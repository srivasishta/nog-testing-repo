package com.ngl.micro.image;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

public class ImageJob implements Identifiable<UUID> {

	/** Generated. Do not modify. 2016-01-19T10:03:34.530Z */
	public static final class Fields {
		public static final String CREATED_DATE = "createdDate";
		public static final String ID = "id";
		public static final String LARGE = "large";
		public static final String MEDIUM = "medium";
		public static final String ORIGINAL = "original";
		public static final String PARTNER_ID = "partnerId";
		public static final String PROCESSING_STATUS = "processingStatus";
		public static final String PROCESSING_STATUS_REASON = "processingStatusReason";
		public static final String RESOURCE_ID = "resourceId";
		public static final String RESOURCE_TYPE = "resourceType";
		public static final String THUMBNAIL = "thumbnail";
	}

	private UUID id;
	private ResourceType resourceType;
	private UUID resourceId;
	private UUID partnerId;
	private String processingStatus;
	private String processingStatusReason;
	private ZonedDateTime createdDate;
	private ImageMetadata original;
	private ImageMetadata thumbnail;
	private ImageMetadata medium;
	private ImageMetadata large;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public ResourceType getResourceType() {
		return this.resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	public UUID getResourceId() {
		return this.resourceId;
	}

	public void setResourceId(UUID resourceId) {
		this.resourceId = resourceId;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public String getProcessingStatus() {
		return this.processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	public String getProcessingStatusReason() {
		return this.processingStatusReason;
	}

	public void setProcessingStatusReason(String processingStatusReason) {
		this.processingStatusReason = processingStatusReason;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public ImageMetadata getOriginal() {
		return this.original;
	}

	public void setOriginal(ImageMetadata original) {
		this.original = original;
	}

	public ImageMetadata getThumbnail() {
		return this.thumbnail;
	}

	public void setThumbnail(ImageMetadata thumbnail) {
		this.thumbnail = thumbnail;
	}

	public ImageMetadata getMedium() {
		return this.medium;
	}

	public void setMedium(ImageMetadata medium) {
		this.medium = medium;
	}

	public ImageMetadata getLarge() {
		return this.large;
	}

	public void setLarge(ImageMetadata large) {
		this.large = large;
	}

}
