package com.ngl.micro.image;

public enum ResourceType {
	BUSINESS_LOGO,
	CHARITY_LOGO,
	PARTNER_LOGO;
}