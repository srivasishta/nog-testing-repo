package com.ngl.micro.image;

public class ImageMetadata {

	/** Generated. Do not modify. 2016-01-19T10:03:34.444Z */
	public static final class Fields {
		public static final String HEIGHT = "height";
		public static final String URL = "url";
		public static final String WIDTH = "width";
	}


	private String url;
	private Integer width;
	private Integer height;

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getWidth() {
		return this.width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return this.height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

}
