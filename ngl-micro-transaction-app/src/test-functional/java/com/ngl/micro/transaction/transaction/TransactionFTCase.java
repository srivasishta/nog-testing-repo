package com.ngl.micro.transaction.transaction;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.TransactionApplication;
import com.ngl.micro.transaction.TransactionFTCaseConfiguration;
import com.ngl.micro.transaction.client.TransactionClient;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.micro.transaction.test_data.TestAccessTokens;
import com.ngl.micro.transaction.test_data.TestTransactions;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = {
	DefaultDatabaseTestConfiguration.class,
	TransactionFTCaseConfiguration.class
	})
public class TransactionFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Autowired
	private TransactionClient client;

	private TestTransactions testTransactions;

	public TransactionFTCase() {
		super(TransactionApplication.class);
	}

	@Override
	@Before
	public void before() {
		super.before();
		this.testTransactions = new TestTransactions();
	}

	@Test
	public void test_import_transaction_as_partner() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_READ_IMPORT);

		Transaction expected = this.testTransactions.transaction();
		expected.setPartnerId(TestTransactions.PARTNER_5);
		Transaction created = this.client.getTransactionResource().create(expected).getState();
		expected.setId(created.getId());
		expected.setGroupId(created.getGroupId());
		expected.setCreatedDate(created.getCreatedDate());
		expected.setLastModifiedDate(created.getLastModifiedDate());
		expected.setProcessingStatus(ProcessingStatus.PENDING.name());

		assertEquals(expected, created);
	}

	@Test
	public void test_import_transaction_as_system() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Transaction expected = this.testTransactions.transaction();
		expected.setPartnerId(TestTransactions.PARTNER_5);
		Transaction created = this.client.getTransactionResource().create(expected).getState();
		expected.setId(created.getId());
		expected.setGroupId(created.getGroupId());
		expected.setCreatedDate(created.getCreatedDate());
		expected.setLastModifiedDate(created.getLastModifiedDate());
		expected.setProcessingStatus(ProcessingStatus.PENDING.name());

		assertEquals(expected, created);
	}

	@Test
	public void test_create_transaction_for_different_partner() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_6_READ);

		Transaction tx = this.testTransactions.transaction();
		tx.setPartnerId(TestTransactions.PARTNER_5);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getTransactionResource().create(tx);
	}

	@Test
	public void test_create_transaction_with_unauthorised_scope() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_READ);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getTransactionResource().create(this.testTransactions.transaction());
	}

	@Test
	public void test_read_transaction_with_unauthorised_scope() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_BUSINESS_READ);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getTransactionResource().get(Data.ID_1);
	}

	@Test
	public void test_read_transaction_for_different_partner() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_READ_IMPORT);
		this.thrown.expectStatus(ApplicationStatus.NOT_FOUND);

		Transaction tx = this.testTransactions.transaction();
		tx.setPartnerId(TestTransactions.PARTNER_5);
		tx = this.client.getTransactionResource().create(tx).getState();

		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_6_READ);
		this.client.getTransactionResource().get(tx.getId());
	}

	@Test
	public void test_list_partner_transactions() throws Exception {
		this.cmdLoadTestDataSet(this.testTransactions);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_READ);

		List<Transaction> items = this.client.getTransactionResource().list(PageRequest.with(0, 20).build()).getState().getItems();

		Assert.assertTrue(items.size() > 0);
		items.forEach(tx -> assertEquals(TestTransactions.PARTNER_5, tx.getPartnerId()));
	}

	@Test
	public void test_list_partner_transactions_with_unauthorised_scope() throws Exception {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.PARTNER_5_BUSINESS_READ);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getTransactionResource().list(PageRequest.with(0, 20).build()).getState().getItems();
	}

}
