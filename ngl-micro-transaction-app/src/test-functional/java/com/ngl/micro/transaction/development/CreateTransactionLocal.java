package com.ngl.micro.transaction.development;

import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ngl.micro.transaction.TransactionFTCaseConfiguration;
import com.ngl.micro.transaction.client.TransactionClient;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
		TransactionFTCaseConfiguration.class
})
@ActiveProfiles("development")
public class CreateTransactionLocal {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CreateTransactionLocal.class);

	@SuppressWarnings("unused")
	@Autowired
	private TransactionClient client;
	private Random rand = new Random();

	@Test
	@Ignore("Run manually")
	public void run_create_single() throws Exception {
		this.createTransaction(1L, 3L, "100.0");
	}

	public void createTransaction(long membershipId, long storeId, String amount) {
//		Transaction transaction = new Transaction();
//		transaction.setPartnerId(Data.toUUID(1L));
//		transaction.setAmount(amount);
//		transaction.setBaseAmount(amount);
//		transaction.setBaseCurrency(Currency.CAD);
//		transaction.setCurrency(Currency.CAD);
//		transaction.setMembershipId(membershipId);
//		transaction.setStatus(TransactionStatus.SETTLED);
//		transaction.setStoreId(storeId);
//		transaction.setTimezone(ZoneId.of("America/Edmonton"));
//		transaction.setTraceId(UUID.randomUUID().toString());
//		transaction.setTransactionDate(Time.utcNow());
//
//		UUID id = this.client.getTransactionResource().create(transaction).getState().getId();
//
//		log.info("Transaction created: {}", id);
	}

	@Test
	@Ignore("Run manually")
	public void create_multiple_transactions() throws InterruptedException {

		int count = 100;

		int memberMin = 1;
		int memberMax = 10;

		int storeMin = 1;
		int storeMax = 16;

		int minCents = 5;
		int maxCents = 10_000;

		for (int i = 0; i < count; i++) {
			Long membershipId = Long.valueOf(this.range(memberMin, memberMax));
			Long storeId = Long.valueOf(this.range(storeMin, storeMax));
			double amount = this.range(minCents, maxCents) / 100;
			String amountString = String.valueOf(amount);
			this.createTransaction(membershipId, storeId, amountString);
			Thread.sleep(50L);
		}

	}

	private int range(int min, int max) {
		return this.rand.nextInt((max - min) + 1) + min;
	}
}
