package com.ngl.micro.transaction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.transaction.client.TransactionClient;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerConfiguration;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerProperties;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCaseConfiguration;
import com.ngl.middleware.rest.api.page.QueryParameterParser;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.HttpClient;

@Configuration
@EnableMicroserviceJmsClient
@Import(HornetQEmbeddedServerConfiguration.class)
public class TransactionFTCaseConfiguration extends AbstractApplicationFTCaseConfiguration {

	public static final Long DEFAULT_ACCOUNT_ID = 2L;

	private static final String TRANSACTION_SERVICE_URL = "http://localhost:9093/service/transaction/";

	@Bean
	public HornetQEmbeddedServerProperties hornetQEmbeddedServerProperties(
			EnvironmentConfigurationProvider config) {
		return config.getConfiguration(
				HornetQEmbeddedServerProperties.DEFAULT_PREFIX,
				HornetQEmbeddedServerProperties.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public TransactionClient restClient(
			HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(
				TRANSACTION_SERVICE_URL, requestFactory, parser, mapper);
		return new TransactionClient(config);
	}

}
