CREATE TABLE `processing_status_reason` (

    -- Columns
    `id`            BIGINT(20) NOT NULL,
    `value`         VARCHAR(50) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`value`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
