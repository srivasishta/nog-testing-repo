CREATE TABLE `transaction_charity_choices` (

    -- Columns
    `id`                    BIGINT(20) NOT NULL AUTO_INCREMENT,
    `transaction_id`        BINARY(16) NOT NULL,
    `charity_id`            BINARY(16) NOT NULL,
    `charity_allocation`    INTEGER NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`transaction_id`, `charity_id`),
    FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
