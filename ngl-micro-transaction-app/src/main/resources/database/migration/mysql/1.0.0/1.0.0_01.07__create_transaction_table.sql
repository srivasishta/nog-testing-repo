CREATE TABLE `transaction` (

    -- Columns
    `id`                             BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`                    BINARY(16) NOT NULL,
    `group_id`                       BINARY(16) NOT NULL,
    `partner_id`                     BINARY(16) NOT NULL,
    `trace_id`                       VARCHAR(100) NOT NULL,
    `ref_id`                         VARCHAR(100) NOT NULL,
    `membership_id`                  BINARY(16),
    `membership_ref_id`              VARCHAR(100) NOT NULL,
    `business_id`                    BINARY(16),
    `store_id`                       BINARY(16),
    `store_ref_id`                   VARCHAR(100) NOT NULL,
    `transaction_status_id`          BIGINT(20) NOT NULL,
    `transaction_date`               DATETIME NOT NULL,
    `timezone`                       BIGINT(20) NOT NULL,
    `currency`                       BIGINT(20) NOT NULL,
    `amount`                         DECIMAL(15,5) NOT NULL,
    `local_currency`                 BIGINT(20) NOT NULL,
    `local_amount`                   DECIMAL(15,5) NOT NULL,
    `processing_status_id`           BIGINT(20) NOT NULL,
    `processing_status_reason_id`    BIGINT(20) NOT NULL,

    `created_date`                   DATETIME NOT NULL,
    `last_modified_date`             DATETIME NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`),
    UNIQUE KEY (`partner_id`, `trace_id`),
    UNIQUE KEY (`partner_id`, `ref_id`, `transaction_status_id`),
    FOREIGN KEY (`transaction_status_id`) REFERENCES `transaction_status_type` (`id`),
    FOREIGN KEY (`processing_status_id`) REFERENCES `processing_status_type` (`id`),
    FOREIGN KEY (`processing_status_reason_id`) REFERENCES `processing_status_reason` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_t_group_id` ON `transaction` (`group_id`);
CREATE INDEX `idx_t_trace_id` ON `transaction` (`trace_id`);
CREATE INDEX `idx_t_ref_id` ON `transaction` (`ref_id`);
CREATE INDEX `idx_t_membership_id` ON `transaction` (`membership_id`);
CREATE INDEX `idx_t_business_id` ON `transaction` (`business_id`);
CREATE INDEX `idx_t_store_id` ON `transaction` (`store_id`);
CREATE INDEX `idx_t_transaction_date` ON `transaction` (`transaction_date`);
CREATE INDEX `idx_t_created_date` ON `transaction` (`created_date`);
