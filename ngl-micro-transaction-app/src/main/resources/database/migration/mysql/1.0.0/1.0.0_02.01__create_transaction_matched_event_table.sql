CREATE TABLE `transaction_matched_event` (

    -- Columns
    `id`                    BIGINT(20) NOT NULL AUTO_INCREMENT,
    `transaction_id`        BINARY(16) NOT NULL,
    `created_date`          DATETIME NOT NULL,
    `published`             CHAR(1) NOT NULL DEFAULT 'F',

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY(`transaction_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_tme_created_date` ON `transaction_matched_event` (`created_date`);
