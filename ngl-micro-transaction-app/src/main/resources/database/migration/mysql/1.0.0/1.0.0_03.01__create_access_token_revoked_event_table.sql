CREATE TABLE `access_token_revoked_event` (

    -- Columns
    `id`                 BIGINT(20) NOT NULL,
    `token_id`           BINARY(16) NOT NULL,
    `revoked_date`       DATETIME DEFAULT NULL,
    `expiration_date`    DATETIME DEFAULT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`token_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_atre_expiration_date` ON `access_token_revoked_event` (`expiration_date`);
