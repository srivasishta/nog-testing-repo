CREATE TABLE `transaction_demographics` (

    -- Columns
    `id`                   BIGINT(20) NOT NULL AUTO_INCREMENT,
    `transaction_id`       BINARY(16) NOT NULL,
    `gender`               BIGINT(20) NOT NULL,
    `birthdate`            DATE,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`transaction_id`),
    FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
