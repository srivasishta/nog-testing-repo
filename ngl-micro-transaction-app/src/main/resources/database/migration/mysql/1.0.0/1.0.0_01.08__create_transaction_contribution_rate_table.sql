CREATE TABLE `transaction_contribution_rate` (

    -- Columns
    `id`                    BIGINT(20) NOT NULL AUTO_INCREMENT,
    `transaction_id`        BINARY(16) NOT NULL,
    `contribution_rate`     DECIMAL(15,5) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`transaction_id`),
    FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
