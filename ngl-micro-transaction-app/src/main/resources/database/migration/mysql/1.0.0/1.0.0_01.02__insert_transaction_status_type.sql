INSERT INTO `transaction_status_type` (`id`, `value`) VALUES
(1, 'AUTHORIZED'),
(2, 'SETTLED'),
(3, 'CANCELLED'),
(4, 'REVERSED');
