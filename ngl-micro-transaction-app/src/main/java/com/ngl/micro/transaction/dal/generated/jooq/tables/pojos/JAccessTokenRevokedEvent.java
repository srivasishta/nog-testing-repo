/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.transaction.dal.generated.jooq.tables.pojos;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JAccessTokenRevokedEvent implements Serializable {

	private static final long serialVersionUID = -174060040;

	private Long          id;
	private UUID          tokenId;
	private ZonedDateTime revokedDate;
	private ZonedDateTime expirationDate;

	public JAccessTokenRevokedEvent() {}

	public JAccessTokenRevokedEvent(JAccessTokenRevokedEvent value) {
		this.id = value.id;
		this.tokenId = value.tokenId;
		this.revokedDate = value.revokedDate;
		this.expirationDate = value.expirationDate;
	}

	public JAccessTokenRevokedEvent(
		Long          id,
		UUID          tokenId,
		ZonedDateTime revokedDate,
		ZonedDateTime expirationDate
	) {
		this.id = id;
		this.tokenId = tokenId;
		this.revokedDate = revokedDate;
		this.expirationDate = expirationDate;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getTokenId() {
		return this.tokenId;
	}

	public void setTokenId(UUID tokenId) {
		this.tokenId = tokenId;
	}

	public ZonedDateTime getRevokedDate() {
		return this.revokedDate;
	}

	public void setRevokedDate(ZonedDateTime revokedDate) {
		this.revokedDate = revokedDate;
	}

	public ZonedDateTime getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(ZonedDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}
}
