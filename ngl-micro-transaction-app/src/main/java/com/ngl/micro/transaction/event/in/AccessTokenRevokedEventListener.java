package com.ngl.micro.transaction.event.in;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.messaging.core.data.deserialize.JsonMessageDeserializer;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;
import com.ngl.middleware.rest.server.security.oauth2.Revocation;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.util.Assert;

/**
 * Listens for access token revoked events.
 *
 * @author Willy du Preez
 *
 */
public class AccessTokenRevokedEventListener
		extends JsonMessageDeserializer<AccessTokenRevokedEvent>
		implements MessageListener<AccessTokenRevokedEvent> {

	private AccessTokenRevokedEventRepository events;
	private RevocationList revocations;

	public AccessTokenRevokedEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository events,
			RevocationList revocationList) {
		super(objectMapper);

		Assert.notNull(events);
		Assert.notNull(revocationList);

		this.events = events;
		this.revocations = revocationList;
	}

	@Override
	protected Class<? extends AccessTokenRevokedEvent> getEventType(String eventName) {
		if(AccessTokenRevokedEvent.NAME.equals(eventName)) {
			return AccessTokenRevokedEvent.class;
		}
		throw new UnsupportedOperationException("Unknown event type: " + eventName);
	}

	@Override
	public void onMessage(AccessTokenRevokedEvent event) {
		this.revocations.removeExpiredRevocations();
		this.revocations.addRevocation(new Revocation(event.getTokenId(), event.getExpirationDate()));
		this.events.add(event);
	}

}
