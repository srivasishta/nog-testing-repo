package com.ngl.micro.transaction.transaction;

import static com.ngl.micro.shared.contracts.oauth.Scopes.TRANSACTION_IMPORT_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.TRANSACTION_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Transaction endpoint.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Path("transactions")
public interface TransactionEndpoint extends RestEndpoint {

	@GET
	@RequiresScopes(TRANSACTION_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@GET
	@Path("{id}")
	@RequiresScopes(TRANSACTION_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@POST
	@RequiresScopes(TRANSACTION_IMPORT_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(Transaction transaction);

}
