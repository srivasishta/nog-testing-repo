package com.ngl.micro.transaction.policy;

import static com.ngl.micro.transaction.policy.TransactionAuthority.URN_PREFIX;

import java.net.URI;

import com.ngl.middleware.policy.api.ResourceType;
import com.ngl.middleware.util.Uris;

/**
 * Deprecated until more complex needed.
 *
 * @author Willy du Preez
 *
 */
@Deprecated
public enum TransactionResourceType implements ResourceType {

	TRANSACTION,
	TRANSACTION_COLLECTION;

	private final URI resourceType;

	private TransactionResourceType() {
		this.resourceType = Uris.toUri(URN_PREFIX + this.name().toLowerCase().replace('_', '-'));
	}

	@Override
	public URI getUri() {
		return this.resourceType;
	}

}