package com.ngl.micro.transaction.transaction;

import static com.ngl.micro.transaction.dal.generated.jooq.Tables.PROCESSING_STATUS_REASON;
import static com.ngl.micro.transaction.dal.generated.jooq.Tables.PROCESSING_STATUS_TYPE;
import static com.ngl.micro.transaction.dal.generated.jooq.Tables.TRANSACTION;
import static com.ngl.micro.transaction.dal.generated.jooq.Tables.TRANSACTION_STATUS_TYPE;

import java.sql.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.TableOnConditionStep;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionCharityChoicesDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionContributionRateDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionDemographicsDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransactionCharityChoices;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransactionContributionRate;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransactionDemographics;
import com.ngl.micro.transaction.dal.generated.jooq.tables.records.JTransactionRecord;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.micro.transaction.match.ProcessingStatusReason;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.util.Assert;

/**
 * Transaction repository.
 *
 * @author Willy du Preez
 * @author Paco Mendes
 *
 */
@Transactional(rollbackFor = Exception.class)
public class TransactionRepository implements PagingRepository<Transaction, UUID> {

	private DAL support;
	private JooqQuery query;

	private FieldMap<String> fieldMap;

	private JTransactionContributionRateDao rates;
	private JTransactionDemographicsDao demographics;
	private JTransactionCharityChoicesDao choices;

	public TransactionRepository(Configuration config) {
		this.support= new DAL(config);
		this.rates = new JTransactionContributionRateDao(config);
		this.demographics = new JTransactionDemographicsDao(config);
		this.choices = new JTransactionCharityChoicesDao(config);

		this.fieldMap = new FieldMap<String>()
				.bind(Transaction.Fields.ID, new MappedField(TRANSACTION.RESOURCE_ID).searchable(true))
				.bind(Transaction.Fields.GROUP_ID, new MappedField(TRANSACTION.GROUP_ID).searchable(true))
				.bind(Transaction.Fields.TRACE_ID, new MappedField(TRANSACTION.TRACE_ID).searchable(true))
				.bind(Transaction.Fields.REF_ID, new MappedField(TRANSACTION.REF_ID).searchable(true))
				.bind(Transaction.Fields.PARTNER_ID, new MappedField(TRANSACTION.PARTNER_ID).searchable(true))
				.bind(Transaction.Fields.MEMBERSHIP_ID, new MappedField(TRANSACTION.MEMBERSHIP_ID).searchable(true))
				.bind(Transaction.Fields.MEMBERSHIP_REF_ID, new MappedField(TRANSACTION.MEMBERSHIP_REF_ID).searchable(true))
				.bind(Transaction.Fields.BUSINESS_ID, new MappedField(TRANSACTION.BUSINESS_ID).searchable(true))
				.bind(Transaction.Fields.STORE_ID, new MappedField(TRANSACTION.STORE_ID).searchable(true))
				.bind(Transaction.Fields.STORE_REF_ID, new MappedField(TRANSACTION.STORE_REF_ID).searchable(true))
				.bind(Transaction.Fields.TRANSACTION_STATUS, new MappedField(TRANSACTION_STATUS_TYPE.VALUE).searchable(true))
				.bind(Transaction.Fields.TRANSACTION_DATE, new MappedField(TRANSACTION.TRANSACTION_DATE).searchable(true).sortable(true))
				.bind(Transaction.Fields.TIMEZONE, TRANSACTION.TIMEZONE)
				.bind(Transaction.Fields.CURRENCY, TRANSACTION.CURRENCY)
				.bind(Transaction.Fields.AMOUNT, new MappedField(TRANSACTION.AMOUNT).searchable(true))
				.bind(Transaction.Fields.LOCAL_CURRENCY, TRANSACTION.LOCAL_CURRENCY)
				.bind(Transaction.Fields.LOCAL_AMOUNT, TRANSACTION.LOCAL_AMOUNT)
				.bind(Transaction.Fields.PROCESSING_STATUS, new MappedField(PROCESSING_STATUS_TYPE.VALUE).as(Transaction.Fields.PROCESSING_STATUS).searchable(true))
				.bind(Transaction.Fields.PROCESSING_STATUS_REASON, PROCESSING_STATUS_REASON.VALUE.as(Transaction.Fields.PROCESSING_STATUS_REASON))
				.bind(Transaction.Fields.CREATED_DATE, new MappedField(TRANSACTION.CREATED_DATE).searchable(true).sortable(true))
				.bind(Transaction.Fields.LAST_MODIFIED_DATE, TRANSACTION.LAST_MODIFIED_DATE);

		TableOnConditionStep<?> tables = TRANSACTION
				.join(TRANSACTION_STATUS_TYPE).on(TRANSACTION.TRANSACTION_STATUS_ID.eq(TRANSACTION_STATUS_TYPE.ID))
				.join(PROCESSING_STATUS_TYPE).on(TRANSACTION.PROCESSING_STATUS_ID.eq(PROCESSING_STATUS_TYPE.ID))
				.leftOuterJoin(PROCESSING_STATUS_REASON).on(TRANSACTION.PROCESSING_STATUS_REASON_ID.eq(PROCESSING_STATUS_REASON.ID));

		this.query = JooqQuery.builder(this.support, this.fieldMap)
				.from(tables)
				.build();
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public Transaction add(Transaction transaction) {
		JTransactionRecord record = this.support.sql().newRecord(TRANSACTION);
		record.setResourceId(transaction.getId());
		record.setGroupId(transaction.getGroupId());
		record.setRefId(transaction.getRefId());
		record.setTraceId(transaction.getTraceId());
		record.setPartnerId(transaction.getPartnerId());
		record.setMembershipRefId(transaction.getMembershipRefId());
		record.setStoreRefId(transaction.getStoreRefId());
		record.setTransactionStatusId(TransactionStatus.valueOf(transaction.getTransactionStatus()).getId());
		record.setTransactionDate(transaction.getTransactionDate());
		record.setTimezone(TimeZone.forZoneId(transaction.getTimezone()).getId());
		record.setCurrency(Currency.valueOf(transaction.getCurrency()).getId());
		record.setAmount(transaction.getAmount());
		record.setLocalCurrency(Currency.valueOf(transaction.getLocalCurrency()).getId());
		record.setLocalAmount(transaction.getLocalAmount());
		record.setProcessingStatusId(ProcessingStatus.valueOf(transaction.getProcessingStatus()).getId());
		record.setProcessingStatusReasonId(ProcessingStatusReason.idFrom(transaction.getProcessingStatusReason()));
		record.setCreatedDate(transaction.getCreatedDate());
		record.setLastModifiedDate(transaction.getLastModifiedDate());

		// Do not store these fields. They are looked up
		// during the transaction matching process.
//		record.setMembershipId(transaction.getMembershipId());
//		record.setBusinessId(transaction.getBusinessId());
//		record.setStoreId(transaction.getStoreId());

		record.store();
		return this.get(record.getResourceId());
	}

	@Override
	public Transaction get(UUID id) {
		Record transaction = this.query.getRecordWhere(TRANSACTION.RESOURCE_ID.eq(id));
		if (transaction == null) {
			return null;
		} else {
			return this.map(transaction);
		}
	}

	public List<Transaction> getByRefId(String refId) {
		List<Record> transactions = this.query.getRecordsWhere(TRANSACTION.REF_ID.eq(refId));
		return transactions.stream()
				.map(this::map)
				.collect(Collectors.toList());
	}

	@Override
	public Page<Transaction> get(PageRequest pageRequest) {
		return this.query.getPage(pageRequest, this::map);
	}

	private Transaction map(Record record) {
		Transaction tx = new Transaction();
		tx.setId(record.getValue(TRANSACTION.RESOURCE_ID));
		tx.setGroupId(record.getValue(TRANSACTION.GROUP_ID));
		tx.setRefId(record.getValue(TRANSACTION.REF_ID));
		tx.setTraceId(record.getValue(TRANSACTION.TRACE_ID));
		tx.setPartnerId(record.getValue(TRANSACTION.PARTNER_ID));
		tx.setMembershipId(record.getValue(TRANSACTION.MEMBERSHIP_ID));
		tx.setMembershipRefId(record.getValue(TRANSACTION.MEMBERSHIP_REF_ID));
		tx.setBusinessId(record.getValue(TRANSACTION.BUSINESS_ID));
		tx.setStoreId(record.getValue(TRANSACTION.STORE_ID));
		tx.setStoreRefId(record.getValue(TRANSACTION.STORE_REF_ID));
		tx.setTransactionStatus(record.getValue(TRANSACTION_STATUS_TYPE.VALUE));
		tx.setTransactionDate(record.getValue(TRANSACTION.TRANSACTION_DATE));
		tx.setTimezone(TimeZone.forId(record.getValue(TRANSACTION.TIMEZONE)).getZoneId());
		tx.setCurrency(Currency.forId(record.getValue(TRANSACTION.CURRENCY)).name());
		tx.setAmount(record.getValue(TRANSACTION.AMOUNT));
		tx.setLocalCurrency(Currency.forId(record.getValue(TRANSACTION.LOCAL_CURRENCY)).name());
		tx.setLocalAmount(record.getValue(TRANSACTION.LOCAL_AMOUNT));
		tx.setProcessingStatus((String) record.getValue(Transaction.Fields.PROCESSING_STATUS));
		tx.setProcessingStatusReason((String) record.getValue(Transaction.Fields.PROCESSING_STATUS_REASON));
		tx.setCreatedDate(record.getValue(TRANSACTION.CREATED_DATE));
		tx.setLastModifiedDate(record.getValue(TRANSACTION.LAST_MODIFIED_DATE));
		return tx;
	}

	@Override
	public void update(Transaction tx) {
		JTransactionRecord record = this.support.sql().fetchOne(TRANSACTION, TRANSACTION.RESOURCE_ID.eq(tx.getId()));
		record.setBusinessId(tx.getBusinessId());
		record.setStoreId(tx.getStoreId());
		record.setMembershipId(tx.getMembershipId());
		record.setProcessingStatusId(ProcessingStatus.valueOf(tx.getProcessingStatus()).getId());
		record.setProcessingStatusReasonId(ProcessingStatusReason.idFrom(tx.getProcessingStatusReason()));
		record.setLastModifiedDate(tx.getLastModifiedDate());
		record.store();
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException("Transactions cannot be deleted.");
	}

	@Override
	public long size() {
		return this.query.size();
	}

	public TransactionMetadata getMetadata(Transaction transaction) {
		Assert.notNull(transaction);
		Assert.state(transaction.getId() != null);

		TransactionMetadata metadata = new TransactionMetadata();

		JTransactionContributionRate rate = this.rates.fetchOneByJTransactionId(transaction.getId());
		if (rate != null) {
			metadata.setStoreContributionRate(rate.getContributionRate());
		}

		JTransactionDemographics demographics = this.demographics.fetchOneByJTransactionId(transaction.getId());
		if (demographics != null) {
			metadata.setMembershipDemographics(new MembershipDemographics(
					Gender.forId(demographics.getGender()),
					demographics.getBirthdate() == null ? null : demographics.getBirthdate().toLocalDate()));

			// If there are demographics, then there could be choices.
			metadata.setMembershipCharityChoice(this.choices.fetchByJTransactionId(transaction.getId())
					.stream()
					.map(c -> new MembershipCharityChoice(c.getCharityId(), c.getCharityAllocation()))
					.collect(Collectors.toSet()));
		}

		return metadata;
	}

	public void updateStoreMetadata(Transaction tx, TransactionMetadata metadata) {
		JTransactionContributionRate rate = this.rates.fetchOneByJTransactionId(tx.getId());
		if (rate == null) {
			rate = new JTransactionContributionRate(null, tx.getId(), metadata.getStoreContributionRate());
			this.rates.insert(rate);
		}
		else {
			// Past rates cannot change.
			Assert.state(rate.getContributionRate() == metadata.getStoreContributionRate());
		}
	}

	public void updateMembershipMetadata(Transaction tx, TransactionMetadata metadata) {
		JTransactionDemographics demographics = this.demographics.fetchOneByJTransactionId(tx.getId());
		if (demographics == null) {
			MembershipDemographics md = metadata.getMembershipDemographics();
			demographics = new JTransactionDemographics(
					null,
					tx.getId(),
					md.getGender().getId(),
					md.getBirthdate().map(d -> Date.valueOf(d)).orElse(null));
			this.demographics.insert(demographics);

			List<JTransactionCharityChoices> choices = metadata.getMembershipCharityChoice().stream()
					.map(c -> new JTransactionCharityChoices(null, tx.getId(), c.getCharityId(), c.getAllocation()))
					.collect(Collectors.toList());

			if (!choices.isEmpty()) {
				this.choices.insert(choices);
			}
		}
	}

}
