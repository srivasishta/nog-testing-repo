package com.ngl.micro.transaction.transaction;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;

public class TransactionMetadata {

	private UUID transactionId;
	private BigDecimal storeContributionRate;
	private Set<MembershipCharityChoice> membershipCharityChoice = Collections.emptySet();
	private MembershipDemographics membershipDemographics;

	public UUID getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(UUID transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getStoreContributionRate() {
		return this.storeContributionRate;
	}

	public void setStoreContributionRate(BigDecimal storeContributionRate) {
		this.storeContributionRate = storeContributionRate;
	}

	public Set<MembershipCharityChoice> getMembershipCharityChoice() {
		return this.membershipCharityChoice;
	}

	public void setMembershipCharityChoice(Set<MembershipCharityChoice> membershipCharityChoice) {
		this.membershipCharityChoice = membershipCharityChoice;
	}

	public MembershipDemographics getMembershipDemographics() {
		return this.membershipDemographics;
	}

	public void setMembershipDemographics(MembershipDemographics membershipDemographics) {
		this.membershipDemographics = membershipDemographics;
	}

}
