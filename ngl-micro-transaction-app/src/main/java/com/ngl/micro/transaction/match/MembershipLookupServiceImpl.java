package com.ngl.micro.transaction.match;

import org.springframework.util.Assert;

import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Methods;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.util.Uris;

public class MembershipLookupServiceImpl implements MembershipLookupService {

	private HttpClient client;
	private String serviceUrl;

	public MembershipLookupServiceImpl(HttpClient client, String serviceUrl) {
		Assert.notNull(client);
		Uris.toUri(serviceUrl);

		this.client = client;
		this.serviceUrl = serviceUrl;
	}

	@Override
	public MembershipLookupResponse lookup(MembershipLookupRequest request) {
		RestResponse response = this.client.request(this.serviceUrl)
				.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
				.method(Methods.POST)
				.body(request)
				.execute(RestResponse.class);

		if (response.getStatus() != 200) {
			ApplicationError error = response.bodyAsPojo(ApplicationError.class);
			throw new ApplicationException(error, error.getDetail());
		}

		return response.bodyAsPojo(MembershipLookupResponse.class);
	}

}
