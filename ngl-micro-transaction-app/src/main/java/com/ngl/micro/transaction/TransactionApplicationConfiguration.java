package com.ngl.micro.transaction;

import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.PlatformTransactionManager;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.business.StoreLookupService;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.micro.transaction.event.in.AccessTokenRevokedEventListener;
import com.ngl.micro.transaction.event.in.AccessTokenRevokedEventRepository;
import com.ngl.micro.transaction.event.out.TransactionMatchedEventPublishedListener;
import com.ngl.micro.transaction.event.out.TransactionMatchedEventRepository;
import com.ngl.micro.transaction.match.MembershipLookupServiceImpl;
import com.ngl.micro.transaction.match.StoreLookupServiceImpl;
import com.ngl.micro.transaction.match.TransactionMatcher;
import com.ngl.micro.transaction.match.TransactionMatcherProperties;
import com.ngl.micro.transaction.transaction.TransactionEndpoint;
import com.ngl.micro.transaction.transaction.TransactionEndpointImpl;
import com.ngl.micro.transaction.transaction.TransactionRepository;
import com.ngl.micro.transaction.transaction.TransactionService;
import com.ngl.micro.transaction.transaction.TransactionValidator;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.messaging.core.bus.RxLocalMessageBus;
import com.ngl.middleware.messaging.core.data.deserialize.LoggingDeserializationErrorHandler;
import com.ngl.middleware.messaging.core.data.serialize.JsonMessageSerializer;
import com.ngl.middleware.messaging.jms.publish.JmsPublisher;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherConfiguration;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherMetrics;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriber;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberConfiguration;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the transaction application.
 *
 * @author Willy du Preez
 */
@Configuration
@EnableMicroserviceRestServer
@EnableMicroserviceDatabase
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
public class TransactionApplicationConfiguration extends MicroserviceConfiguration {

	@Bean
	public RestEndpoints restEndpoints(
			ObjectMapper mapper, TransactionEndpoint transactionEndpoint, Realm securityRealm) {

		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints("transaction");
		restEndpoints.addEndpoints(transactionEndpoint);
		restEndpoints.setRealm(securityRealm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	/* --------------------------------------------------------
	 *  Transaction
	 * -------------------------------------------------------- */

	@Bean
	@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
	public TransactionRepository transactionRepository(org.jooq.Configuration config) {
		return new TransactionRepository(config);
	}

	@Bean
	public TransactionService transactionService(
			TransactionRepository transactionRepository,
			TransactionMatcher transactionMatcher,
			PlatformTransactionManager platformTransactionManager) {

		return new TransactionService(transactionRepository,
				TransactionValidator.newInstance(),
				transactionMatcher,
				platformTransactionManager);
	}

	@Bean
	public TransactionEndpoint transactionEndpoint(RequestProcessor processor, TransactionService transactionService) {
		return new TransactionEndpointImpl(processor, transactionService);
	}

	@Bean
	public TransactionMatchedEventRepository transactionMatchedEventRepository(org.jooq.Configuration config) {
		return new TransactionMatchedEventRepository(this.transactionRepository(config), config);
	}

	@Bean
	public MessageBus transactionMatchedEventBus() {
		return new RxLocalMessageBus("TransactionMatchedEventBus", 1);
	}

	@Bean
	public TransactionMatcherProperties transactionMatcherProperties(EnvironmentConfigurationProvider config) {
		return config.getConfiguration(TransactionMatcherProperties.DEFAULT_PREFIX, TransactionMatcherProperties.class);
	}

	@Bean
	public TransactionMatcher transactionMatcher(
			TransactionMatcherProperties transactionMatcherProperties,
			TransactionRepository transactionRepository,
			TransactionMatchedEventRepository transactionMatchedEventRepository,
			PlatformTransactionManager txManager,
			StoreLookupService storeLookupService,
			MembershipLookupService membershipLookupService) {

		return new TransactionMatcher(
				transactionRepository,
				transactionMatchedEventRepository,
				this.transactionMatchedEventBus(),
				txManager,
				transactionMatcherProperties,
				storeLookupService,
				membershipLookupService);
	}

	@Bean(destroyMethod = "stop")
	public JmsPublisher transactionMatchedEventPublisher(
			ConnectionFactory connectionFactory,
			ObjectMapper objectMapper,
			TransactionMatchedEventRepository transactionImportedEventRepository,
			MetricRegistry metricRegistry) {

		JmsPublisherConfiguration config = new JmsPublisherConfiguration();
		config.setConnectionFactory(connectionFactory);
		config.setDestination(TransactionMatchedEvent.NAME);
		config.setMessageBus(this.transactionMatchedEventBus());
		config.setProducerCount(20);
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setSerializer(new JsonMessageSerializer(objectMapper));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessagePublishedListener(new TransactionMatchedEventPublishedListener(transactionImportedEventRepository));
		config.setJmsPublisherMetrics(new JmsPublisherMetrics(TransactionMatchedEvent.NAME, metricRegistry));

		JmsPublisher publisher = new JmsPublisher(config);
		publisher.start();
		return publisher;
	}

	/* --------------------------------------------------------
	 *  Lookup Services
	 * -------------------------------------------------------- */

	@Bean
	public HttpClient httpClient(ObjectMapper mapper) {
		return HttpClient.withDefaultTransport()
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.build();
	}

	@Bean
	public MembershipLookupService membershipLookupService(
			HttpClient httpClient,
			TransactionMatcherProperties properties) {
		return new MembershipLookupServiceImpl(httpClient, properties.getMembershipLookupServiceUrl());
	}

	@Bean
	public StoreLookupService storeLookupService(
			HttpClient httpClient,
			TransactionMatcherProperties properties) {
		return new StoreLookupServiceImpl(httpClient, properties.getStoreLookupServiceUrl());
	}

	/* --------------------------------------------------------
	 *  Access Tokens
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration configuration) {
		return new AccessTokenRevokedEventRepository(configuration);
	}

	@Bean
	public RevocationList revocationList(AccessTokenRevokedEventRepository events) {
		return new RevocationList(events.getByExpirationDateGreaterThan(Time.utcNow()));
	}

	@Bean
	public AccessTokenRevokedEventListener accessTokenEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository accessTokenRevokedEventRepository,
			RevocationList revocationList) {
		return new AccessTokenRevokedEventListener(objectMapper, accessTokenRevokedEventRepository, revocationList);
	}

	@Bean
	public Realm securityRealm(RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(
				this.signedEncryptedJwtFactory(),
				revocationList));
	}

	@Bean
	public SignedEncryptedJwtFactory signedEncryptedJwtFactory() {
		return new SignedEncryptedJwtFactory();
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber accessTokenEventSubscriber(
			ConnectionFactory connectionFactory,
			AccessTokenRevokedEventListener accessTokenEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<AccessTokenRevokedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(1);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setSubscription("transaction-" + AccessTokenRevokedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(accessTokenEventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(accessTokenEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

}