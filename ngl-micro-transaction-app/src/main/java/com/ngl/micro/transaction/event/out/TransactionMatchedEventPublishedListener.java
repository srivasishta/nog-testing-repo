package com.ngl.micro.transaction.event.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.messaging.api.Message;
import com.ngl.middleware.messaging.core.publish.MessagePublishedListener;

/**
 * Publishes transaction matched events.
 *
 * @author Willy du Preez
 *
 */
public class TransactionMatchedEventPublishedListener implements MessagePublishedListener {

	private static final Logger log = LoggerFactory.getLogger(TransactionMatchedEventPublishedListener.class);

	private TransactionMatchedEventRepository repository;

	public TransactionMatchedEventPublishedListener(TransactionMatchedEventRepository repository) {
		this.repository = repository;
	}

	@Override
	public void onPublished(Message<?> message) {
		if (message instanceof TransactionMatchedEvent) {
			this.repository.markPublished(((TransactionMatchedEvent) message).getId());
		}
		else {
			log.warn("Unexpected message type received: {}", message == null ? null : message.getClass().getName());
		}
	}

}
