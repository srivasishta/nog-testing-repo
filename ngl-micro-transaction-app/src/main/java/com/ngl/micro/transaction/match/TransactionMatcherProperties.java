package com.ngl.micro.transaction.match;

public class TransactionMatcherProperties {

	public static final String DEFAULT_PREFIX = "transaction.matcher.";

	private int coreThreads = 1;
	private int maxThreads = 10;
	private int maxQueueSize = 10_000;

	private String membershipLookupServiceUrl;
	private String storeLookupServiceUrl;

	public int getCoreThreads() {
		return this.coreThreads;
	}

	public void setCoreThreads(int coreThreads) {
		this.coreThreads = coreThreads;
	}

	public int getMaxThreads() {
		return this.maxThreads;
	}

	public void setMaxThreads(int maxThreads) {
		this.maxThreads = maxThreads;
	}

	public int getMaxQueueSize() {
		return this.maxQueueSize;
	}

	public void setMaxQueueSize(int maxQueueSize) {
		this.maxQueueSize = maxQueueSize;
	}

	public String getMembershipLookupServiceUrl() {
		return this.membershipLookupServiceUrl;
	}

	public void setMembershipLookupServiceUrl(String membershipLookupServiceUrl) {
		this.membershipLookupServiceUrl = membershipLookupServiceUrl;
	}

	public String getStoreLookupServiceUrl() {
		return this.storeLookupServiceUrl;
	}

	public void setStoreLookupServiceUrl(String storeLookupServiceUrl) {
		this.storeLookupServiceUrl = storeLookupServiceUrl;
	}

}
