package com.ngl.micro.transaction.policy;

import java.util.Set;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.policy.api.AccessResponse;
import com.ngl.middleware.policy.api.Decision;
import com.ngl.middleware.policy.api.PolicyEnforcementPoint;
import com.ngl.middleware.util.Collections;

/**
 * Deprecated until more complex needed.
 *
 * @author Willy du Preez
 *
 */
@Deprecated
public class TransactionAuthority {

	static final String URN_PREFIX = "urn:com.ngl.micro.transaction:1.0:";

	public static Set<Resource> getPolicies() {
		return Collections.asSet(
				new ClassPathResource("/policies/transaction.view_transaction.xml", TransactionAuthority.class));
	}

//	private PolicyEnforcementPoint pep;

	public TransactionAuthority(PolicyEnforcementPoint pep) {
//		this.pep = pep;
	}

	public void authorizeList() {

	}

	public AccessResponse authorizeRead(Transaction transaction, String subjectId, Set<String> roles) {
//		AccessRequest.Builder builder = AccessRequest.to(READ, TRANSACTION)
//				.addResourceAttribute(TransactionAttributeId.USER_ID_ATTRIBUTE, String.valueOf(transaction.getUserId()))
//				.addResourceAttribute(TransactionAttributeId.STATUS_ATTRIBUTE, transaction.getStatus())
//				.addSubjectAttribute(StandardSubjectAttributeId.SUBJECT_ROLES_ID, XacmlDataType.STRING, roles);
//
//		if (!isNullOrWhitespace(subjectId)) {
//			builder.addSubjectAttribute(XacmlSubjectAttributeId.SUBJECT_ID, XacmlDataType.STRING, subjectId);
//		}
//
//		AccessRequest request = builder.build();
//
//		return pep.enforce(request);
		AccessResponse response = new AccessResponse();
		response.setDecision(Decision.PERMIT);
		return response;
	}

	public void authorizeCreate() {

	}

}
