package com.ngl.micro.transaction.match;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolProperties;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.StoreLookupRequest;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.micro.shared.contracts.business.StoreLookupService;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.event.out.TransactionMatchedEventRepository;
import com.ngl.micro.transaction.transaction.TransactionMetadata;
import com.ngl.micro.transaction.transaction.TransactionRepository;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * <p>The {@link TransactionMatcher} performs matching of incoming transactions
 * against business rule to determine whether the transaction is imported into
 * the system or rejected.</p>
 *
 * <p>Transactions submitted for matching are wrapped in a task that is in turn
 * submitted to an {@link ExecutorService}. The executor service is created
 * with a {@link BlockingQueue} configured using the {@link TransactionMatcherProperties}
 * maxQueueSize property. Once the queue capacity is reached, the matcher will
 * throw {@link RejectedExecutionException}s until capacity becomes available.</p>
 *
 * <p>Once a task has been submitted it awaits execution. Upon execution the task
 * will perform the following assuming a successful import:</p>
 * <ol>
 *   <li>Lookup the store using the store reference number</li>
 *   <li>Lookup the membership using the membership reference number</li>
 *   <li>Raise a transaction matched event</li>
 * </ol>
 *
 * <p><b>The store lookup</b> first checks the database to determine if a
 * remote store lookup has been performed for the transaction. If so, then
 * the cached data is used, otherwise a remote store lookup is performed.
 * </p>
 *
 * <p>If the store was active at the time of the transaction, then a
 * the <b>membership lookup</b> is performed. The membership lookup first checks
 * the database to determine if a remote membership lookup has been performed
 * for the transaction. If so, then the cached data is used, otherwise a remote
 * membership lookup is performed.
 * </p>
 *
 * <p>If the membership was active at the time of the transaction, then a
 * transaction matched event is raised.</p>
 *
 * </p><b>Remote lookup error handling and Hystrix implementation</b></p>
 *
 * <p>Hystrix is used as the circuit breaker pattern implementation for remote
 * service lookups. The Hystrix thread pool is configured to be the same size
 * as the {@link ExecutorService} thread pool. The default is 10 which should
 * be sufficient for most environments. The Hystrix queue size is set to -1 to
 * ensure that a SynchronousQueue is used. The synchronous queue is a hand-over
 * queue, meaning that an executor thread will hand over a task to a Hystrix
 * thread. The Hystrix command is then executed synchronously, meaning that the
 * executor thread will wait for the Hystrix command to finish before continuing
 * execution.</p>
 *
 * <p>In the case of remote service lookup failure, i.e. a timeout or HTTP call
 * interruption, the hystrix command will throw an exception. The the executor
 * thread (in the matching task) will sleep for 100ms and retry the command.
 * For continuous failed commands (i.e. the service is down), the Hytsrix
 * circuit breaker will take over and handle retries.</p>
 *
 * @author Willy du Preez
 *
 */
public class TransactionMatcher {

	private static final String MEMBERSHIP_GROUP = "MembershipLookupGroup_" + UUIDS.type4Uuid().toString();
	private static final String STORE_GROUP = "StoreLookupGroup_" + UUIDS.type4Uuid().toString();

	private static final Logger log = LoggerFactory.getLogger(TransactionMatcher.class);

	private TransactionRepository transactions;
	private TransactionMatchedEventRepository events;

	private MembershipLookupService memberships;
	private StoreLookupService stores;

	private MessageBus messageBus;

	private PlatformTransactionManager txManager;
	private TransactionDefinition txDefinition;

	private ExecutorService executioner;

	private Setter storeLookupConfig;
	private Setter membershipLookupConfig;

	public TransactionMatcher(
			TransactionRepository transactionRepository,
			TransactionMatchedEventRepository transactionMatchedEventRepository,
			MessageBus eventBus,
			PlatformTransactionManager txManager,
			TransactionMatcherProperties properties,
			StoreLookupService stores,
			MembershipLookupService memberships) {

		Assert.notNull(transactionRepository);
		Assert.notNull(transactionMatchedEventRepository);
		Assert.notNull(eventBus);
		Assert.notNull(txManager);
		Assert.notNull(properties);
		Assert.notNull(stores);
		Assert.notNull(memberships);

		this.transactions = transactionRepository;
		this.events = transactionMatchedEventRepository;

		this.messageBus = eventBus;

		this.txManager = txManager;
		this.txDefinition = new DefaultTransactionDefinition();

		this.stores = stores;
		this.memberships = memberships;

		this.executioner = new ThreadPoolExecutor(
				properties.getCoreThreads(),
				properties.getMaxThreads(),
				60, TimeUnit.SECONDS,
				new ArrayBlockingQueue<>(properties.getMaxQueueSize()));

		// Hystrix Config: Set the core pool size the
		// same as the executioner's core pool size.
		// Max queue size -1 to use SynchronousQueue.

		this.membershipLookupConfig = Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(MEMBERSHIP_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

		this.storeLookupConfig = Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(STORE_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

	}

	/**
	 * Submits a transaction for matching. Throws an exception if the matcher
	 * capacity has been reached.
	 *
	 * If the transaction has already been matched, then a completed future is
	 * returned.
	 *
	 * @param transaction the transaction to match
	 * @return the future with the matching result
	 * @throws RejectedExecutionException if the transaction matcher capacity has
	 * been reached
	 */
	public Future<Transaction> submit(Transaction transaction) throws RejectedExecutionException {
		log.debug("Submitting transaction for matching: {}", transaction.getId());
		return this.executioner.submit(new TransactionMatchingTask(transaction));
	}

	/**
	 * Submits a transaction for asynchronous matching. No exceptions are thrown
	 * if the transaction matcher capacity has been reached.
	 *
	 * @param transaction the transaction to submit for matching
	 */
	public void submitSafely(Transaction transaction) {
		try {
			this.submit(transaction);
		} catch (RejectedExecutionException e) {
			log.warn("Transaction matcher capacity reached. Submit rejected.", e);
		}
	}

	private class TransactionMatchingTask implements Callable<Transaction> {

		private Transaction transaction;
		private TransactionMetadata metadata;

		private TransactionMatchingTask(Transaction transaction) {
			this.transaction = transaction;
		}

		@Override
		public Transaction call() throws InterruptedException {
			log.debug("Starting transaction matching for transaction: {}", this.transaction.getId());
			try {
				this.metadata = TransactionMatcher.this.transactions.getMetadata(this.transaction);
				if (ProcessingStatus.PENDING.name().equals(this.transaction.getProcessingStatus())
						&& this.transaction.getStoreId() == null) {
					log.debug("Store metadata not present, looking up for transaction: {}", this.transaction.getId());
					this.lookupStore();
				}
				if (ProcessingStatus.PENDING.name().equals(this.transaction.getProcessingStatus())) {
					log.debug("Membership metadata not present, looking up for transaction: {}", this.transaction.getId());
					this.lookupMembership();
				}
				if (ProcessingStatus.MATCHED.name().equals(this.transaction.getProcessingStatus())) {
					log.debug("Raising matched event for transaction with ID: {}", this.transaction.getId());
					this.raiseTransactionMatchedEvent();
				}
				else {
					log.debug("Transaction not matched: id={};status={};reason={}",
							this.transaction.getId(),
							this.transaction.getProcessingStatus(),
							this.transaction.getProcessingStatusReason());
				}
			} catch (Exception e) {
				log.error("Transaction matching process failed for transaction: {}", this.transaction.getId(), e);
				throw e;
			}
			return this.transaction;
		}

		private void lookupStore() throws InterruptedException {
			StoreLookupResponse store = this.executeStoreLookup();

			if (!store.isMatched()) {
				this.transaction.setProcessingStatus(ProcessingStatus.REJECTED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.STORE_NOT_FOUND.name());
			}
			else if (ResourceStatus.INACTIVE.equals(store.getBusinessStatus())) {
				this.transaction.setProcessingStatus(ProcessingStatus.REJECTED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.BUSINESS_INACTIVE.name());
			}
			else if (ResourceStatus.INACTIVE.equals(store.getStoreStatus())) {
				this.transaction.setProcessingStatus(ProcessingStatus.REJECTED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.STORE_INACTIVE.name());
			}
			else {
				this.transaction.setBusinessId(store.getBusinessId());
				this.transaction.setStoreId(store.getStoreId());
				this.metadata.setStoreContributionRate(store.getContributionRate());
				TransactionMatcher.this.transactions.updateStoreMetadata(this.transaction, this.metadata);
			}

			this.transaction.setLastModifiedDate(Time.utcNow());
			TransactionMatcher.this.transactions.update(this.transaction);
		}

		private StoreLookupResponse executeStoreLookup() throws InterruptedException {
			StoreLookupRequest request = new StoreLookupRequest();
			request.setStoreRefId(this.transaction.getStoreRefId());
			request.setTransactionDate(this.transaction.getTransactionDate());

			while (true) {
				try {
					return new StoreLookupCommand(
							TransactionMatcher.this.storeLookupConfig,
							TransactionMatcher.this.stores,
							request)
							.execute();
				} catch (HystrixRuntimeException e) {
					// Execution failed or circuit is most likely open.
					log.warn("Failed to execute store lookup command, retrying in 100 ms.", e);
					// Wait a while to give the service a chance to
					// recover. Instead of implementing a back-off
					// strategy, let Hystrix deal with service retries
					// and make the sleep just long enough to avoid
					// CPU starvation.
					Thread.sleep(100);
				}
			}
		}

		private void lookupMembership() throws InterruptedException {
			MembershipLookupResponse membership = this.executeMembershipLookup();

			if (!membership.isMatched()) {
				this.transaction.setProcessingStatus(ProcessingStatus.REJECTED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.MEMBERSHIP_NOT_FOUND.name());
			}
			else if (ResourceStatus.INACTIVE.equals(membership.getStatus())) {
				this.transaction.setProcessingStatus(ProcessingStatus.REJECTED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.MEMBERSHIP_INACTIVE.name());
			}
			else {
				this.transaction.setMembershipId(membership.getMembershipId());
				this.transaction.setProcessingStatus(ProcessingStatus.MATCHED.name());
				this.transaction.setProcessingStatusReason(ProcessingStatusReason.MATCHED.name());
				this.metadata.setMembershipDemographics(membership.getDemographics());
				this.metadata.setMembershipCharityChoice(membership.getCharityChoice());
				TransactionMatcher.this.transactions.updateMembershipMetadata(this.transaction, this.metadata);
			}

			this.transaction.setLastModifiedDate(Time.utcNow());
			TransactionMatcher.this.transactions.update(this.transaction);
		}

		private MembershipLookupResponse executeMembershipLookup() throws InterruptedException {
			MembershipLookupRequest request = new MembershipLookupRequest();
			request.setMembershipRefId(this.transaction.getMembershipRefId());
			request.setPartnerId(this.transaction.getPartnerId());
			request.setTransactionDate(this.transaction.getTransactionDate());

			while (true) {
				try {
					return new MembershipLookupCommand(
							TransactionMatcher.this.membershipLookupConfig,
							TransactionMatcher.this.memberships,
							request)
							.execute();
				} catch (HystrixRuntimeException e) {
					// Execution failed or circuit is most likely open.
					log.warn("Failed to execute membership lookup command, retrying in 100 ms.", e);
					// Wait a while to give the service a chance to
					// recover. Instead of implementing a back-off
					// strategy, let Hystrix deal with service retries
					// and make the sleep just long enough to avoid
					// CPU starvation.
					Thread.sleep(100);
				}
			}
		}

		private void raiseTransactionMatchedEvent() {
			org.springframework.transaction.TransactionStatus status =
					TransactionMatcher.this.txManager.getTransaction(TransactionMatcher.this.txDefinition);
			TransactionMatchedEvent event;
			try {
				event = this.createTransactionMatchedEvent();
			} catch (Exception e) {
				TransactionMatcher.this.txManager.rollback(status);
				throw e;
			}
			TransactionMatcher.this.txManager.commit(status);
			TransactionMatcher.this.messageBus.send(event);
		}

		private TransactionMatchedEvent createTransactionMatchedEvent() {
			return TransactionMatcher.this.events.getByTransactionId(this.transaction.getId()).orElseGet(() -> {
				TransactionMatchedEvent event = TransactionMatchedEvent.builder(0L, Time.utcNow())
						.businessId(this.transaction.getBusinessId())
						.businessStoreContributionRate(this.metadata.getStoreContributionRate())
						.businessStoreId(this.transaction.getStoreId())
						.membershipCharityChoices(this.metadata.getMembershipCharityChoice())
						.membershipDemographics(this.metadata.getMembershipDemographics())
						.membershipId(this.transaction.getMembershipId())
						.partnerId(this.transaction.getPartnerId())
						.transactionId(this.transaction.getId())
						.transactionAmount(this.transaction.getAmount())
						.transactionCurrency(Currency.valueOf(this.transaction.getCurrency()))
						.transactionDate(this.transaction.getTransactionDate())
						.transactionGroupId(this.transaction.getGroupId())
						.transactionStatus(TransactionStatus.valueOf(this.transaction.getTransactionStatus()))
						.transactionTimezone(TimeZone.forZoneId(this.transaction.getTimezone()))
						.build();
				return TransactionMatcher.this.events.addEvent(event);
			});
		}
	}

}
