package com.ngl.micro.transaction.event.out;

import static com.ngl.micro.transaction.dal.generated.jooq.Tables.TRANSACTION_MATCHED_EVENT;

import java.util.Optional;
import java.util.UUID;

import org.jooq.Configuration;
import org.springframework.util.Assert;

import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionMatchedEventDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransactionMatchedEvent;
import com.ngl.micro.transaction.dal.generated.jooq.tables.records.JTransactionMatchedEventRecord;
import com.ngl.micro.transaction.transaction.TransactionMetadata;
import com.ngl.micro.transaction.transaction.TransactionRepository;
import com.ngl.middleware.dal.repository.PublishableEventRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.portability.SQLBooleans;

public class TransactionMatchedEventRepository
		implements PublishableEventRepository<TransactionMatchedEvent, Long> {

	private DAL support;
	private JTransactionMatchedEventDao events;
	private TransactionRepository transactions;

	public TransactionMatchedEventRepository(TransactionRepository transactions, Configuration config) {
		this.transactions = transactions;

		this.support= new DAL(config);
		this.events = new JTransactionMatchedEventDao(config);
	}

	@Override
	public TransactionMatchedEvent addEvent(TransactionMatchedEvent event) {
		Assert.notNull(event);
		Assert.state(event.getId() == 0L);

		JTransactionMatchedEventRecord record = this.support.sql().newRecord(TRANSACTION_MATCHED_EVENT);
		record.setCreatedDate(event.getCreated());
		record.setTransactionId(event.getTransactionId());

		record.store();

		return this.getEvent(record.getId()).get();
	}

//	@Override
//	public TransactionMatchedEvent getEvent(Long id) {
//		return null;
//		return this.support.sql().select()
//				.from(TRANSACTION_MATCHED_EVENT)
//				.where(TRANSACTION_MATCHED_EVENT.ID.eq(id))
//				.fetchOne()
//				.map(this::map);
//	}

//	private TransactionMatchedEvent map(Record record) {
//		TransactionMatchedEvent event = new TransactionMatchedEvent(record.getValue(TRANSACTION_MATCHED_EVENT.ID),
//				record.getValue(TRANSACTION_MATCHED_EVENT.GROUP_ID),
//				record.getValue(TRANSACTION_MATCHED_EVENT.CREATED_DATE));
//
//		event.setTransactionId(record.getValue(TRANSACTION_MATCHED_EVENT.TRANSACTION_ID));
//		event.setTransactionStatus(TransactionStatus.valueOf(record.getValue(TRANSACTION_MATCHED_EVENT.TRANSACTION_STATUS)));
//		event.setTransactionAmount(amountString(record.getValue(TRANSACTION_MATCHED_EVENT.TRANSACTION_AMOUNT)));
//		event.setTransactionDate(record.getValue(TRANSACTION_MATCHED_EVENT.TRANSACTION_DATE));
//
//		event.setMembershipId(record.getValue(TRANSACTION_MATCHED_EVENT.MEMBERSHIP_ID));
//		event.setStoreId(record.getValue(TRANSACTION_MATCHED_EVENT.STORE_ID));
//
//		return event;
//	}

//	public List<TransactionMatchedEvent> getGroup(UUID groupId) {
//		Result<Record> result = this.support.sql().select()
//				.from(TRANSACTION_MATCHED_EVENT)
//				.where(TRANSACTION_MATCHED_EVENT.GROUP_ID.eq(groupId))
//				.fetch();
//
//		return result.map(this::map);
//		return null;
//	}

	@Override
	public void markPublished(Long eventId) {
		Assert.notNull(eventId);

		this.support.sql().update(TRANSACTION_MATCHED_EVENT)
				.set(TRANSACTION_MATCHED_EVENT.PUBLISHED, SQLBooleans.toString(true))
				.where(TRANSACTION_MATCHED_EVENT.ID.eq(eventId))
				.execute();
	}

	@Override
	public Optional<TransactionMatchedEvent> getEvent(Long id) {
		Assert.notNull(id);
		Assert.state(id > 0);
		return this.mapToEvent(this.events.fetchOneByJId(id));
	}

	public Optional<TransactionMatchedEvent> getByTransactionId(UUID id) {
		Assert.notNull(id);
		return this.mapToEvent(this.events.fetchOneByJTransactionId(id));
	}

	private Optional<TransactionMatchedEvent> mapToEvent(JTransactionMatchedEvent record) {
		if (record == null) {
			return Optional.empty();
		}
		else {
			Transaction tx = this.transactions.get(record.getTransactionId());
			TransactionMetadata metadata = this.transactions.getMetadata(tx);
			TransactionMatchedEvent event = TransactionMatchedEvent.builder(record.getId(), record.getCreatedDate())
					.businessId(tx.getBusinessId())
					.businessStoreContributionRate(metadata.getStoreContributionRate())
					.businessStoreId(tx.getStoreId())
					.membershipCharityChoices(metadata.getMembershipCharityChoice())
					.membershipDemographics(metadata.getMembershipDemographics())
					.membershipId(tx.getMembershipId())
					.partnerId(tx.getPartnerId())
					.transactionAmount(tx.getAmount())
					.transactionCurrency(Currency.valueOf(tx.getCurrency()))
					.transactionDate(tx.getTransactionDate())
					.transactionGroupId(tx.getGroupId())
					.transactionId(tx.getId())
					.transactionStatus(TransactionStatus.valueOf(tx.getTransactionStatus()))
					.transactionTimezone(TimeZone.forZoneId(tx.getTimezone()))
					.build();
			return Optional.of(event);
		}
	}

}
