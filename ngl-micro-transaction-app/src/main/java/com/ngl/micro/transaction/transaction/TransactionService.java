package com.ngl.micro.transaction.transaction;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.micro.transaction.match.ProcessingStatusReason;
import com.ngl.micro.transaction.match.TransactionMatcher;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Collections;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The transaction service.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Transactional(rollbackFor = Exception.class)
public class TransactionService {

	// Allows for time discrepancy between server times of up to 15mins which still
	// prevents datetimes based on forward timezones from incorrectly being imported.
	private static final int TX_TIME_DISCREPANCY_ALLOWANCE_MINS = 15;

	private static final Logger log = LoggerFactory.getLogger(TransactionService.class);

	private TransactionRepository transactions;
	private TransactionValidator validator;
	private TransactionMatcher matcher;

	private PlatformTransactionManager txManager;
	private TransactionDefinition txDefinition;

	public TransactionService(
			TransactionRepository transactionRepository,
			TransactionValidator transactionValidator,
			TransactionMatcher matcher,
			PlatformTransactionManager txManager) {

		this.transactions = transactionRepository;
		this.validator = transactionValidator;

		this.matcher = matcher;

		this.txManager = txManager;
		this.txDefinition = new DefaultTransactionDefinition();
	}

	@Transactional(readOnly = true)
	public Page<Transaction> getTransactions(PageRequest pageRequest) {
		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			pageRequest.filter(SearchQueries.and(Transaction.Fields.PARTNER_ID).eq(partnerId));
		}
		return this.transactions.get(pageRequest);
	}

	@Transactional(readOnly = true)
	public Transaction getTransactionById(UUID id) {
		Transaction transaction = this.transactions.get(id);
		UUID partnerId = Subjects.getSubject();
		if (transaction == null || !ApiPolicy.isAccessibleByPartner(partnerId, transaction.getPartnerId())) {
			throw ApplicationError.resourceNotFound()
					.setTitle("Transaction Not Found")
					.setDetail("No transaction found for the ID: " + id)
					.asException();
		}
		return transaction;
	}

	@Transactional(propagation = Propagation.NEVER)
	public Transaction importTransaction(Transaction transaction) {

		if (transaction.getTransactionDate().isAfter(Time.utcNow().plusMinutes(TX_TIME_DISCREPANCY_ALLOWANCE_MINS))) {
			throw ApplicationError.validationError()
				.setTitle("Invalid Transaction Date.")
				.setDetail("The transaction date of the provided transaction is in the future.")
				.asException();
		}

		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId) && !transaction.getPartnerId().equals(partnerId)) {
			log.warn("Partner tried to import a transaction with an incorrect partner ID.");
			throw ApplicationError.forbidden()
					.setTitle("Unauthorized API Usage")
					.setDetail("The provided token does not have sufficient privileges to use this API.")
					.asException();
		}

		org.springframework.transaction.TransactionStatus status = this.txManager.getTransaction(this.txDefinition);
		try {
			transaction = this.createTransaction(transaction);
			log.debug("Created transaction: id={}, traceId={}", transaction.getId(), transaction.getRefId());
		} catch (Exception e) {
			this.txManager.rollback(status);
			e.printStackTrace();
			throw e;
		}
		this.txManager.commit(status);

		if (TransactionStatus.SETTLED.name().equals(transaction.getTransactionStatus())) {
			this.matcher.submitSafely(transaction);
		}

		return transaction;
	}

	private Transaction createTransaction(Transaction transaction) {
		List<Transaction> group = this.transactions.getByRefId(transaction.getRefId());
		if (group.isEmpty()) {
			transaction.setGroupId(UUIDS.type4Uuid());
		} else {
			this.validator.validateStatusTransition(transaction, group);
			transaction.setGroupId(Collections.getFirst(group).getGroupId());
		}
		transaction.setId(UUIDS.type4Uuid());
		transaction.setMembershipId(null);
		transaction.setStoreId(null);
		transaction.setProcessingStatus(ProcessingStatus.PENDING.name());
		transaction.setProcessingStatusReason(ProcessingStatusReason.PENDING.name());
		transaction.setCreatedDate(Time.utcNow());
		transaction.setLastModifiedDate(transaction.getCreatedDate());
		this.validator.validate(transaction);
		return this.transactions.add(transaction);
	}

}
