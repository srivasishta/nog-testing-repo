package com.ngl.micro.transaction.policy;

import static com.ngl.micro.transaction.policy.TransactionAuthority.URN_PREFIX;

import java.net.URI;

import com.ngl.middleware.policy.api.AttributeId;
import com.ngl.middleware.util.Uris;

/**
 * Deprecated until more complex needed.
 *
 * @author Willy du Preez
 *
 */
@Deprecated
public enum TransactionAttributeId implements AttributeId {

	USER_ID_ATTRIBUTE(URN_PREFIX + "transaction:user-id"),
	STATUS_ATTRIBUTE(URN_PREFIX + "transaction:status");

	private URI uri;

	private TransactionAttributeId(String uriString) {
		this.uri = Uris.toUri(uriString);
	}

	@Override
	public URI getUri() {
		return this.uri;
	}

}