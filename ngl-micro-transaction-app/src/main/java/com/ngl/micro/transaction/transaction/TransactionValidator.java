package com.ngl.micro.transaction.transaction;

import static com.ngl.middleware.rest.api.error.ValidationErrorCode.ALREADY_EXISTS;
import static java.lang.annotation.ElementType.FIELD;

import java.util.List;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ValidationError;
import com.ngl.middleware.rest.server.validation.ResourceValidator;
import com.ngl.middleware.rest.server.validation.constraint.RestrictedZoneIdDef;

public class TransactionValidator extends ResourceValidator<Transaction> {

	public static TransactionValidator newInstance() {
		return ResourceValidator.configurator(new TransactionValidator()).configure();
	}

	private TransactionValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Transaction.class)
			.property("partnerId", FIELD)
				.constraint(new NotNullDef())
			.property("traceId", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(100))
			.property("refId", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(100))
			.property("membershipRefId", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(100))
			.property("storeRefId", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(100))
			.property("transactionStatus", FIELD)
				.constraint(new NotNullDef())
			.property("transactionDate", FIELD)
				.constraint(new NotNullDef())
			.property("timezone", FIELD)
				.constraint(new RestrictedZoneIdDef().allowed(TimeZone.ALLOWED_ZONES))
			.property("amount", FIELD)
				.constraint(new NotNullDef())
			.property("currency", FIELD)
				.constraint(new NotNullDef())
			.property("localAmount", FIELD)
				.constraint(new NotNullDef())
			.property("localCurrency", FIELD)
				.constraint(new NotNullDef())
			.property("createdDate", FIELD)
				.constraint(new NotNullDef())
			.property("lastModifiedDate", FIELD)
				.constraint(new NotNullDef());

	}

	public void validateStatusTransition(Transaction transaction, List<Transaction> transactions) {
		Boolean duplicate = transactions.stream()
				.filter((tx) -> tx.getTransactionStatus().equals(transaction.getTransactionStatus()))
				.count() > 0;

		if (duplicate) {
			String duplicateMessage = String.format("'%s' transaction for trace '%s' already exists",
					transaction.getTransactionStatus(),
					transaction.getTraceId());

			throw ApplicationError.validationError()
					.setTitle("Transaction Status Validation Error")
					.setDetail("Invalid transaction state transition detected.")
					.addValidationErrors(new ValidationError(Transaction.Fields.TRANSACTION_STATUS, ALREADY_EXISTS, duplicateMessage))
					.asException();
		}
	}

}
