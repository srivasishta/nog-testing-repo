package com.ngl.micro.transaction.match;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.hystrix.HystrixCommand;
import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.middleware.util.Assert;

public class MembershipLookupCommand extends HystrixCommand<MembershipLookupResponse> {

	private static final Logger log = LoggerFactory.getLogger(MembershipLookupCommand.class);

	private MembershipLookupService service;
	private MembershipLookupRequest request;

	protected MembershipLookupCommand(Setter config, MembershipLookupService service, MembershipLookupRequest request) {
		super(config);

		Assert.notNull(config);
		Assert.notNull(service);
		Assert.notNull(request);

		this.service = service;
		this.request = request;
	}

	@Override
	protected MembershipLookupResponse run() throws Exception {
		log.debug("Command executing for transaction: {}", this.request.getTransactionDate());
		return this.service.lookup(this.request);
	}

}
