package com.ngl.micro.transaction.match;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

public enum ProcessingStatusReason {

	PENDING(1),
	MATCHED(2),

	STORE_NOT_FOUND(1001),
	STORE_INACTIVE(1002),
	BUSINESS_INACTIVE(1003),

	MEMBERSHIP_NOT_FOUND(2001),
	MEMBERSHIP_INACTIVE(2002);

	public static ProcessingStatusReason forId(Long id) {
		Assert.notNull(id);

		for (ProcessingStatusReason reason : ProcessingStatusReason.values()) {
			if (reason.id == id) {
				return reason;
			}
		}
		throw new NoSuchElementException("No NotMatchedReason with ID: " + id);
	}

	public static String nameForId(Long id) {
		return ProcessingStatusReason.forId(id).name();
	}

	public static Long idFrom(String name) {
		Assert.notNull(name);
		return ProcessingStatusReason.valueOf(name).getId();
	}

	private long id;

	private ProcessingStatusReason(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
