package com.ngl.micro.transaction.match;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

public enum ProcessingStatus {

	PENDING(1L),
	MATCHED(2L),
	REJECTED(3L);

	public static ProcessingStatus forId(Long id) {
		Assert.notNull(id);

		for (ProcessingStatus status : ProcessingStatus.values()) {
			if (status.id == id) {
				return status;
			}
		}
		throw new NoSuchElementException("No ProcessingStatus with ID: " + id);
	}

	private long id;

	private ProcessingStatus(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
