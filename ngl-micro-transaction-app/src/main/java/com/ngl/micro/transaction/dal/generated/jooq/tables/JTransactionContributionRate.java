/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.transaction.dal.generated.jooq.tables;


import com.ngl.micro.transaction.dal.generated.jooq.JNglMicroTransaction;
import com.ngl.micro.transaction.dal.generated.jooq.Keys;
import com.ngl.micro.transaction.dal.generated.jooq.tables.records.JTransactionContributionRateRecord;
import com.ngl.middleware.database.mapping.extension.UUIDTypeConverter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JTransactionContributionRate extends TableImpl<JTransactionContributionRateRecord> {

	private static final long serialVersionUID = 380903774;

	/**
	 * The reference instance of <code>ngl_micro_transaction.transaction_contribution_rate</code>
	 */
	public static final JTransactionContributionRate TRANSACTION_CONTRIBUTION_RATE = new JTransactionContributionRate();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JTransactionContributionRateRecord> getRecordType() {
		return JTransactionContributionRateRecord.class;
	}

	/**
	 * The column <code>ngl_micro_transaction.transaction_contribution_rate.id</code>.
	 */
	public final TableField<JTransactionContributionRateRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_transaction.transaction_contribution_rate.transaction_id</code>.
	 */
	public final TableField<JTransactionContributionRateRecord, UUID> TRANSACTION_ID = createField("transaction_id", org.jooq.impl.SQLDataType.BINARY.length(16).nullable(false), this, "", new UUIDTypeConverter());

	/**
	 * The column <code>ngl_micro_transaction.transaction_contribution_rate.contribution_rate</code>.
	 */
	public final TableField<JTransactionContributionRateRecord, BigDecimal> CONTRIBUTION_RATE = createField("contribution_rate", org.jooq.impl.SQLDataType.DECIMAL.precision(15, 5).nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_transaction.transaction_contribution_rate</code> table reference
	 */
	public JTransactionContributionRate() {
		this("transaction_contribution_rate", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_transaction.transaction_contribution_rate</code> table reference
	 */
	public JTransactionContributionRate(String alias) {
		this(alias, TRANSACTION_CONTRIBUTION_RATE);
	}

	private JTransactionContributionRate(String alias, Table<JTransactionContributionRateRecord> aliased) {
		this(alias, aliased, null);
	}

	private JTransactionContributionRate(String alias, Table<JTransactionContributionRateRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroTransaction.NGL_MICRO_TRANSACTION, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JTransactionContributionRateRecord, Long> getIdentity() {
		return Keys.IDENTITY_TRANSACTION_CONTRIBUTION_RATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JTransactionContributionRateRecord> getPrimaryKey() {
		return Keys.KEY_TRANSACTION_CONTRIBUTION_RATE_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JTransactionContributionRateRecord>> getKeys() {
		return Arrays.<UniqueKey<JTransactionContributionRateRecord>>asList(Keys.KEY_TRANSACTION_CONTRIBUTION_RATE_PRIMARY, Keys.KEY_TRANSACTION_CONTRIBUTION_RATE_TRANSACTION_ID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JTransactionContributionRateRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JTransactionContributionRateRecord, ?>>asList(Keys.TRANSACTION_CONTRIBUTION_RATE_IBFK_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JTransactionContributionRate as(String alias) {
		return new JTransactionContributionRate(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JTransactionContributionRate rename(String name) {
		return new JTransactionContributionRate(name, null);
	}
}
