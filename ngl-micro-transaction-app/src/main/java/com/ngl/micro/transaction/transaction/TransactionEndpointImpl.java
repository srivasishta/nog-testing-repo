package com.ngl.micro.transaction.transaction;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;

/**
 * Transaction Endpoint Implementation.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class TransactionEndpointImpl implements TransactionEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private TransactionService transactionService;

	public TransactionEndpointImpl(RequestProcessor processor, TransactionService accountService) {
		this.processor = processor;
		this.transactionService = accountService;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Transaction transaction = this.transactionService.getTransactionById(id);

		Resource resource = HAL.resource(transaction)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Transaction> page = this.transactionService.getTransactions(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Transaction transaction) {
		Transaction imported = this.transactionService.importTransaction(transaction);
		Resource resource = HAL.resource(imported).build();
		return Response.ok(resource).build();
	}

}
