package com.ngl.micro.transaction.match;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.hystrix.HystrixCommand;
import com.ngl.micro.shared.contracts.business.StoreLookupRequest;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.micro.shared.contracts.business.StoreLookupService;
import com.ngl.middleware.util.Assert;

public class StoreLookupCommand extends HystrixCommand<StoreLookupResponse> {

	private static final Logger log = LoggerFactory.getLogger(StoreLookupResponse.class);

	private StoreLookupService service;
	private StoreLookupRequest request;

	protected StoreLookupCommand(Setter config, StoreLookupService service, StoreLookupRequest request) {
		super(config);

		Assert.notNull(config);
		Assert.notNull(service);
		Assert.notNull(request);

		this.service = service;
		this.request = request;
	}

	@Override
	protected StoreLookupResponse run() throws Exception {
		log.debug("Command executing for transaction: {}", this.request.getTransactionDate());
		return this.service.lookup(this.request);
	}

}
