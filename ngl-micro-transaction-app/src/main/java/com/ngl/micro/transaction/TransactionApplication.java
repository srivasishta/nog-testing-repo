package com.ngl.micro.transaction;

import static com.ngl.micro.transaction.dal.generated.jooq.JNglMicroTransaction.NGL_MICRO_TRANSACTION;

import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The transaction application entry-point.
 *
 * @author Willy du Preez
 *
 */
public class TransactionApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new TransactionApplication().development();
	}

	public TransactionApplication() {
		super(TransactionApplicationConfiguration.class, NGL_MICRO_TRANSACTION);
	}

}