package com.ngl.micro.transaction;

import java.time.ZoneId;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.transaction.test_data.TestTransactions;
import com.ngl.micro.transaction.transaction.TransactionValidator;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.util.Time;

public class TransactionValidatorTest {

	private static DataFactory testData;

	private TransactionValidator validator;
	private TestTransactions testTransactions;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@BeforeClass
	public static void beforeClass() {
		TransactionValidatorTest.testData = new DataFactory();
	}

	@Before
	public void before() {
		this.testTransactions = new TestTransactions();
		this.validator = TransactionValidator.newInstance();
	}

	@Test
	public void test_validate_transaction_resource() {
		Transaction transaction = this.testTransactions.transaction();
		transaction.setCreatedDate(Time.utcNow());
		this.validator.validate(transaction);
	}

	@Test
	public void test_fail_null_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("traceId", ValidationErrorCode.INVALID)
				.expectValidationError("refId", ValidationErrorCode.INVALID)
				.expectValidationError("membershipRefId", ValidationErrorCode.INVALID)
				.expectValidationError("partnerId", ValidationErrorCode.INVALID)
				.expectValidationError("storeRefId", ValidationErrorCode.INVALID)
				.expectValidationError("transactionStatus", ValidationErrorCode.INVALID)
				.expectValidationError("createdDate", ValidationErrorCode.INVALID)
				.expectValidationError("lastModifiedDate", ValidationErrorCode.INVALID)
				.expectValidationError("transactionDate", ValidationErrorCode.INVALID)
				.expectValidationError("timezone", ValidationErrorCode.INVALID)
				.expectValidationError("amount", ValidationErrorCode.INVALID)
				.expectValidationError("currency", ValidationErrorCode.INVALID)
				.expectValidationError("localAmount", ValidationErrorCode.INVALID)
				.expectValidationError("localCurrency", ValidationErrorCode.INVALID);

		this.validator.validate(new Transaction());
	}

	@Test
	public void test_fail_ids_too_long() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("traceId", ValidationErrorCode.INVALID)
				.expectValidationError("refId", ValidationErrorCode.INVALID)
				.expectValidationError("membershipRefId", ValidationErrorCode.INVALID)
				.expectValidationError("storeRefId", ValidationErrorCode.INVALID);

		Transaction transaction = this.testTransactions.transaction();
		transaction.setCreatedDate(Time.utcNow());
		transaction.setTraceId(testData.getRandomChars(101));
		transaction.setRefId(testData.getRandomChars(101));
		transaction.setStoreRefId(testData.getRandomChars(101));
		transaction.setMembershipRefId(testData.getRandomChars(101));

		this.validator.validate(transaction);
	}

	@Test
	public void test_fail_trace_id_too_short() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("traceId", ValidationErrorCode.INVALID);

		Transaction transaction = this.testTransactions.transaction();
		transaction.setCreatedDate(Time.utcNow());
		transaction.setTraceId("");

		this.validator.validate(transaction);
	}

	@Test
	public void test_fail_unsupported_zone() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("timezone", ValidationErrorCode.INVALID);

		Transaction transaction = this.testTransactions.transaction();
		transaction.setTimezone(ZoneId.of("Africa/Johannesburg"));

		this.validator.validate(transaction);
	}

}