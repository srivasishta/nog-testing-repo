package com.ngl.micro.transaction.tools;

import java.util.Collections;

import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields for transaction microservice.
 *
 * @author Paco Mendes
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Collections.emptyList()).generate(Transaction.class);
	}

}
