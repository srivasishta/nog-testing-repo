//package com.ngl.micro.transaction.policy;
//
//import static com.ngl.micro.transaction.TransactionTestObjects.DEFAULT_USER_ID;
//import static com.ngl.middleware.util.Collections.asSet;
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.Assert.assertThat;
//
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import com.ngl.micro.transaction.Transaction;
//import com.ngl.micro.transaction.TransactionTestObjects;
//import com.ngl.micro.transaction.transaction.TransactionAuthority;
//import com.ngl.middleware.policy.api.AccessResponse;
//import com.ngl.middleware.policy.api.Decision;
//import com.ngl.middleware.policy.api.PolicyEnforcementPoint;
//import com.ngl.middleware.policy.provider.balana.BalanaPepBuilder;
//
//public class TransactionAuthority_ReadTest {
//
//	private static TransactionAuthority transactionAuthority;
//
//	@BeforeClass
//	public static void beforeClass() {
//		PolicyEnforcementPoint pep = new BalanaPepBuilder()
//				.addResources(TransactionAuthority.getPolicies())
//				.build();
//		transactionAuthority = new TransactionAuthority(pep);
//	}
//
//	@Test
//	public void test_read_deny_public() {
//		Transaction transaction = TransactionTestObjects.defaultTransaction();
//		AccessResponse response = transactionAuthority.authorizeRead(transaction,
//				String.valueOf(DEFAULT_USER_ID), asSet("user"));
//
//		System.out.println(response.getObligations());
//		System.out.println(response.getAdvices());
//
//		assertThat(response.getDecision(), is(Decision.PERMIT));
//	}
//
//	@Test
//	public void test_read_deny_member_not_owner() {
//		Transaction transaction = TransactionTestObjects.defaultTransaction();
//		AccessResponse response = transactionAuthority.authorizeRead(transaction,
//				String.valueOf(DEFAULT_USER_ID + 1), asSet("user", "member"));
//
//		System.out.println(response.getObligations());
//		System.out.println(response.getAdvices());
//
//		assertThat(response.getDecision(), is(Decision.DENY));
//	}
//
//	@Test
//	public void test_read_permit_member() {
//		Transaction transaction = TransactionTestObjects.defaultTransaction();
//		AccessResponse response = transactionAuthority.authorizeRead(transaction,
//				String.valueOf(DEFAULT_USER_ID), asSet("user", "member"));
//
//		System.out.println(response.getObligations());
//		System.out.println(response.getAdvices());
//
//		assertThat(response.getDecision(), is(Decision.PERMIT));
//	}
//
//}
