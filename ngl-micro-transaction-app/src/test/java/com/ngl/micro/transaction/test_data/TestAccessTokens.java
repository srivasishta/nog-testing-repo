package com.ngl.micro.transaction.test_data;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.util.Collections;

/**
 * Access tokens used for testing.
 *
 * @author Willy du Preez
 *
 */
public class TestAccessTokens {

	public static final TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);

	public static final TestAccessToken PARTNER_5_BUSINESS_READ = TestAccessToken.newBasic()
			.subjectClaim(TestTransactions.PARTNER_5)
			.clientIdClaim(TestTransactions.PARTNER_5)
			.scope(Collections.asSet(Scopes.BUSINESS_READ_SCOPE));

	public static final TestAccessToken PARTNER_5_READ = TestAccessToken.newBasic()
			.subjectClaim(TestTransactions.PARTNER_5)
			.clientIdClaim(TestTransactions.PARTNER_5)
			.scope(Collections.asSet(Scopes.TRANSACTION_READ_SCOPE));

	public static final TestAccessToken PARTNER_5_READ_IMPORT = TestAccessToken.newBasic()
			.subjectClaim(TestTransactions.PARTNER_5)
			.clientIdClaim(TestTransactions.PARTNER_5)
			.scope(Collections.asSet(Scopes.TRANSACTION_READ_SCOPE, Scopes.TRANSACTION_IMPORT_SCOPE));

	public static final TestAccessToken PARTNER_6_READ = TestAccessToken.newBasic()
			.subjectClaim(TestTransactions.PARTNER_6)
			.clientIdClaim(TestTransactions.PARTNER_6)
			.scope(Collections.asSet(Scopes.TRANSACTION_READ_SCOPE));

}
