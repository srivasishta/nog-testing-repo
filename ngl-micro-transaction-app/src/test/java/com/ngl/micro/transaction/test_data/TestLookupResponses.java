package com.ngl.micro.transaction.test_data;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.request.ImmutableResponse;
import com.ngl.middleware.test.data.Data;

public class TestLookupResponses {

	public ImmutableResponse membershipLookupResponse(ObjectMapper mapper) throws Exception  {
		Set<MembershipCharityChoice> choices = new HashSet<>();
		choices.add(new MembershipCharityChoice(Data.ID_9, 100));

		MembershipDemographics demographics = new MembershipDemographics(Gender.FEMALE, LocalDate.now().minusYears(30));

		MembershipLookupResponse response = new MembershipLookupResponse();
		response.setCharityChoice(choices);
		response.setDemographics(demographics);
		response.setMatched(true);
		response.setMembershipId(Data.ID_1);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse storeLookupResponse(ObjectMapper mapper) throws Exception {
		StoreLookupResponse response = new StoreLookupResponse();
		response.setBusinessId(Data.ID_2);
		response.setBusinessStatus(ResourceStatus.ACTIVE);
		response.setContributionRate(Data.AMOUNT_1_00);
		response.setMatched(true);
		response.setStoreId(Data.ID_3);
		response.setStoreStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

}
