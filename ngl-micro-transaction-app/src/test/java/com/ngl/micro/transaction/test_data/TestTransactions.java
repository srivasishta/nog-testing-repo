package com.ngl.micro.transaction.test_data;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransaction;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.micro.transaction.match.ProcessingStatusReason;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

/**
 * Transaction test data.
 *
 * @author Willy du Preez
 *
 */
public class TestTransactions implements JooqTestDataSet {

	public static final String DEFAULT_TRACE_ID = Data.TYPE4_UUID_1_VAL;
	public static final String DEFAULT_REF_ID = Data.TYPE4_UUID_2_VAL;

	public static final UUID DEFAULT_ID = Data.ID_3;
	public static final UUID DEFAULT_GROUP_ID = Data.ID_4;

	public static final UUID DEFAULT_PARTNER_ID = Data.ID_2;
	public static final String DEFAULT_MEMBERSHIP_REF_ID = Data.NAME_A;
	public static final String DEFAULT_STORE_REF_ID = Data.NAME_B;

	public static final BigDecimal DEFAULT_AMOUNT = Data.AMOUNT_10_00;
	public static final Currency DEFAULT_CURRENCY = Currency.CAD;

	public static final TransactionStatus DEFAULT_STATUS = TransactionStatus.SETTLED;
	public static final ZoneId DEFAULT_ZONE_ID = TimeZone.AMERICA__EDMONTON.getZoneId();
	public static final ZonedDateTime DEFAULT_TRANSACTION_DATE = Data.T_MINUS_10d;

	public static final UUID PARTNER_5 = Data.ID_5;
	public static final UUID PARTNER_6 = Data.ID_6;

	public Transaction transaction() {
		Transaction transaction = new Transaction();
		transaction.setId(DEFAULT_ID);
		transaction.setGroupId(DEFAULT_GROUP_ID);
		transaction.setPartnerId(DEFAULT_PARTNER_ID);
		transaction.setTraceId(DEFAULT_TRACE_ID);
		transaction.setRefId(DEFAULT_REF_ID);

		transaction.setMembershipRefId(DEFAULT_MEMBERSHIP_REF_ID);
		transaction.setStoreRefId(DEFAULT_STORE_REF_ID);

		transaction.setAmount(DEFAULT_AMOUNT);
		transaction.setCurrency(DEFAULT_CURRENCY.name());
		transaction.setLocalAmount(DEFAULT_AMOUNT);
		transaction.setLocalCurrency(DEFAULT_CURRENCY.name());

		transaction.setTransactionStatus(DEFAULT_STATUS.name());
		transaction.setTimezone(DEFAULT_ZONE_ID);
		transaction.setTransactionDate(DEFAULT_TRANSACTION_DATE);

		transaction.setProcessingStatus(ProcessingStatus.PENDING.name());
		transaction.setProcessingStatusReason(ProcessingStatusReason.PENDING.name());

		transaction.setCreatedDate(DEFAULT_TRANSACTION_DATE.minusDays(1L));
		transaction.setLastModifiedDate(transaction.getCreatedDate());

		return transaction;
	}

	public Transaction transaction(String refId, String traceId) {
		Transaction transaction = this.transaction();
		transaction.setTraceId(traceId);
		transaction.setRefId(refId);
		return transaction;
	}

	@Override
	public void loadData(DSLContext sql) {
		try {
			JTransactionDao transactionDao = new JTransactionDao(sql.configuration());
			transactionDao.insert(
					this.transaction(100L, Data.TYPE4_UUID_1, DEFAULT_PARTNER_ID, TransactionStatus.AUTHORIZED),
					this.transaction(102L, Data.TYPE4_UUID_2, DEFAULT_PARTNER_ID, TransactionStatus.REVERSED),
					this.transaction(103L, Data.TYPE4_UUID_3, DEFAULT_PARTNER_ID, TransactionStatus.SETTLED),
					this.transaction(104L, Data.TYPE4_UUID_4, DEFAULT_PARTNER_ID, TransactionStatus.SETTLED),
					this.transaction(105L, Data.TYPE4_UUID_5, DEFAULT_PARTNER_ID, TransactionStatus.SETTLED),
					this.transaction(106L, Data.TYPE4_UUID_6, PARTNER_5, TransactionStatus.SETTLED),
					this.transaction(107L, Data.TYPE4_UUID_7, DEFAULT_PARTNER_ID, TransactionStatus.SETTLED),
					this.transaction(108L, Data.TYPE4_UUID_8, DEFAULT_PARTNER_ID, TransactionStatus.SETTLED),
					this.transaction(109L, Data.TYPE4_UUID_9, PARTNER_5, TransactionStatus.SETTLED));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private JTransaction transaction(
			Long id,
			UUID groupId,
			UUID partnerId,
			TransactionStatus status) {

		JTransaction transaction = new JTransaction();
		transaction.setId(id);
		transaction.setResourceId(Data.toUUID(id));
		transaction.setGroupId(groupId);
		transaction.setTraceId("TRACE-" + groupId.toString());
		transaction.setRefId("REF-" + groupId.toString());
		transaction.setPartnerId(partnerId);

		transaction.setMembershipId(Data.ID_8);
		transaction.setMembershipRefId("REF-" + Data.ID_8);
		transaction.setStoreId(Data.ID_9);
		transaction.setStoreRefId("REF-" + Data.ID_9);

		transaction.setTransactionStatusId(status.getId());
		transaction.setTransactionDate(DEFAULT_TRANSACTION_DATE.minusDays(id));
		transaction.setTimezone(TimeZone.AMERICA__EDMONTON.getId());

		transaction.setProcessingStatusId(ProcessingStatus.MATCHED.getId());
		transaction.setProcessingStatusReasonId(ProcessingStatusReason.MATCHED.getId());

		transaction.setCurrency(DEFAULT_CURRENCY.getId());
		transaction.setAmount(DEFAULT_AMOUNT);
		transaction.setLocalCurrency(DEFAULT_CURRENCY.getId());
		transaction.setLocalAmount(DEFAULT_AMOUNT);

		transaction.setCreatedDate(transaction.getTransactionDate().plusDays(id));
		transaction.setLastModifiedDate(transaction.getTransactionDate().plusDays(id));

		return transaction;
	}

}