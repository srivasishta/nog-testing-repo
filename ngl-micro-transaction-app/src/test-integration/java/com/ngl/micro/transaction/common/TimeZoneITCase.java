package com.ngl.micro.transaction.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTimezoneDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTimezone;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class TimeZoneITCase extends AbstractDatabaseITCase {

	private JTimezoneDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JTimezoneDao(this.sqlContext);
	}

	@Test
	public void test_timezone_mapping() throws Exception {
		JTimezone status;
		assertEquals("Expect equal status type count.", TimeZone.values().length, this.statusDao.count());
		for (TimeZone type : TimeZone.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
