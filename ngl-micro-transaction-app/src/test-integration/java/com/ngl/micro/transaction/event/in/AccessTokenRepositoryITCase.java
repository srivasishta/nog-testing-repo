//package com.ngl.micro.transaction.event.in;
//
//import static com.ngl.micro.transaction.Transaction.STATUS
//import static com.ngl.micro.transaction.TransactionStatus.AUTHORIZED
//import static org.junit.Assert.*
//
//import java.time.ZonedDateTime
//
//import org.junit.Before
//import org.junit.Test
//import org.springframework.test.context.ContextConfiguration
//
//import com.ngl.micro.transaction.test_data.Tokens_TestDataSet
//import com.ngl.middleware.database.test.AbstractDatabaseITCase
//import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration
//
//@ContextConfiguration(classes = [DefaultDatabaseTestConfiguration.class])
//public class AccessTokenRepositoryITCase extends AbstractDatabaseITCase {
//
//	private AccessTokenEventRepository tokenRepo;
//	private Tokens_TestDataSet tokensDataset;
//
//	@Before
//	public void setup() {
//		tokenRepo = new AccessTokenEventRepository(this.sqlContext);
//		tokensDataset = new Tokens_TestDataSet(this.sqlContext);
//	}
//
//	@Test
//	public void "add access token"() {
//		AccessToken issued = issuedToken();
//		AccessToken token = tokenRepo.add(issued.getId(), issued.getIssuedDate(), issued.getToken());
//		assertEquals(issued, token);
//		assertTrue(token.isActive());
//		assertNull(token.getRevokedDate());
//	}
//
//	@Test
//	public void "get by key"() {
//		tokensDataset.loadData();
//		AccessToken issued = issuedToken();
//		tokenRepo.add(issued.getId(), issued.getIssuedDate(), issued.getToken());
//		assertEquals(issued, tokenRepo.getByKey(issued.getToken()));
//	}
//
//	@Test
//	public void "get non cached"() {
//		AccessToken issued = issuedToken();
//		tokenRepo.add(issued.getId(), issued.getIssuedDate(), issued.getToken());
//		tokenRepo.clearCache();
//		assertEquals(issued, tokenRepo.getByKey(issued.getToken()));
//	}
//
//	@Test
//	public void "get non existing by key"() {
//		assertEquals(null, tokenRepo.getByKey("nothing to find"));
//	}
//
//	@Test
//	public void "update access token"() {
//		AccessToken issued = issuedToken();
//		AccessToken token = tokenRepo.add(issued.getId(), issued.getIssuedDate(), issued.getToken());
//		ZonedDateTime revokedDate = issued.getIssuedDate().plusMonths(1);
//		AccessToken revoked = tokenRepo.update(issued.getId(), revokedDate);
//		assertEquals(revokedDate, revoked.getRevokedDate());
//		assertFalse(revoked.isActive());
//	}
//
//	@Test
//	public void "update none"() {
//		ZonedDateTime revokedDate = Time.utcNow();
//		AccessToken revoked = tokenRepo.update(1L, revokedDate);
//		assertNull(revoked);
//	}
//
//	private AccessToken issuedToken() {
//		AccessToken token = new AccessToken();
//		token.setId(26L);
//		token.setIssuedDate(ZonedDateTime.of(2015,  12, 1, 13, 10 ,1));
//		token.setToken(UUID.randomUUID().toString());
//		return token;
//	}
//
//
//}