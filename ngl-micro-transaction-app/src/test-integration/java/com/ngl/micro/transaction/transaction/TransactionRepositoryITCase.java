package com.ngl.micro.transaction.transaction;

import static com.ngl.micro.transaction.Transaction.Fields.TRANSACTION_STATUS;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.micro.transaction.test_data.TestTransactions;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = {
	DefaultDatabaseTestConfiguration.class,
})
public class TransactionRepositoryITCase extends AbstractDatabaseITCase {

	private TransactionRepository repo;
	private TestTransactions testTransactions;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new TransactionRepository(this.sqlContext);
		this.testTransactions = new TestTransactions();
	}

	@Test
	public void test_create_transaction() throws JsonProcessingException {
		Transaction tx = this.testTransactions.transaction();
		tx.setCreatedDate(Data.T_MINUS_5d);
		tx.setTransactionDate(Data.T_MINUS_10d);

		tx = this.repo.add(tx);

		assertEquals(TestTransactions.DEFAULT_ID, tx.getId());
		assertEquals(TestTransactions.DEFAULT_GROUP_ID, tx.getGroupId());
		assertEquals(TestTransactions.DEFAULT_TRACE_ID, tx.getTraceId());
		assertEquals(TestTransactions.DEFAULT_PARTNER_ID, tx.getPartnerId());
		assertEquals(TestTransactions.DEFAULT_MEMBERSHIP_REF_ID, tx.getMembershipRefId());
		assertEquals(TestTransactions.DEFAULT_STORE_REF_ID, tx.getStoreRefId());
		assertEquals(TestTransactions.DEFAULT_STATUS.name(), tx.getTransactionStatus());
		assertEquals(Data.T_MINUS_5d, tx.getCreatedDate());
		assertEquals(Data.T_MINUS_10d, tx.getTransactionDate());
		assertEquals(TestTransactions.DEFAULT_ZONE_ID, tx.getTimezone());
		assertEquals(TestTransactions.DEFAULT_CURRENCY.name(), tx.getCurrency());
		assertEquals(TestTransactions.DEFAULT_AMOUNT, tx.getAmount());
		assertEquals(TestTransactions.DEFAULT_CURRENCY.name(), tx.getLocalCurrency());
		assertEquals(TestTransactions.DEFAULT_AMOUNT, tx.getLocalAmount());
	}

	@Test
	public void test_list_transactions() {
		this.cmdLoadTestDataSet(this.testTransactions);

		Page<Transaction> transactions = this.repo.get(PageRequest.with(0,5)
				.search(SearchQueries.q(Transaction.Fields.PROCESSING_STATUS).eq(ProcessingStatus.MATCHED).getSearchQuery())
				.build());

		assertEquals(0, transactions.getPageNumber());
		assertEquals(9, transactions.getTotalItems());
		assertEquals(5, transactions.getPageSize());
	}

	@Test
	public void test_list_filter_transactions() {
		this.cmdLoadTestDataSet(this.testTransactions);

		PageRequest page = PageRequest.with(0, 5)
				.search(SearchQueries.q(TRANSACTION_STATUS).eq(TransactionStatus.AUTHORIZED).getSearchQuery())
				.build();

		Page<Transaction> transactions = this.repo.get(page);
		assertEquals(1, transactions.getTotalItems());
		transactions.getItems().forEach((tx) -> assertEquals(TransactionStatus.AUTHORIZED.name(), tx.getTransactionStatus()));
	}

}