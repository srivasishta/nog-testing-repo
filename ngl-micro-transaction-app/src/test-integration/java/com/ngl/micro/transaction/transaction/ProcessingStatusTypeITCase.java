package com.ngl.micro.transaction.transaction;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JProcessingStatusTypeDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JProcessingStatusType;
import com.ngl.micro.transaction.match.ProcessingStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ProcessingStatusTypeITCase extends AbstractDatabaseITCase {

	private JProcessingStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JProcessingStatusTypeDao(this.sqlContext);
	}

	@Test
	public void test_processing_status_type_mapping() throws Exception {
		JProcessingStatusType status;
		assertEquals("Expect equal status type count.", ProcessingStatus.values().length, this.statusDao.count());
		for (ProcessingStatus type : ProcessingStatus.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
