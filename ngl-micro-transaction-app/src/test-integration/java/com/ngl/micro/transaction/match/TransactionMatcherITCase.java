package com.ngl.micro.transaction.match;

import java.util.Arrays;
import java.util.concurrent.RejectedExecutionException;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.business.StoreLookupService;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.micro.transaction.Transaction;
import com.ngl.micro.transaction.event.out.TransactionMatchedEventRepository;
import com.ngl.micro.transaction.test_data.TestAccessTokens;
import com.ngl.micro.transaction.test_data.TestLookupResponses;
import com.ngl.micro.transaction.test_data.TestTransactions;
import com.ngl.micro.transaction.transaction.TransactionRepository;
import com.ngl.micro.transaction.transaction.TransactionService;
import com.ngl.micro.transaction.transaction.TransactionValidator;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.messaging.core.bus.RxLocalMessageBus;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.request.FakeTransport;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.json.ObjectMapperFactory;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class
})
public class TransactionMatcherITCase extends AbstractDatabaseITCase {

	private static final Logger log = LoggerFactory.getLogger(TransactionMatcherITCase.class);

	private static ObjectMapper mapper;

	private FakeTransport fakeTransport;
	private HttpClient httpClient;
	private TransactionMatcher matcher;
	private TransactionService service;
	private TransactionRepository transactions;
	private TestTransactions testTransactions;
	private TestLookupResponses testLookupResponses;
	private MembershipLookupService memberships;
	private StoreLookupService stores;
	private TransactionMatchedEventRepository events;

	@Autowired
	private PlatformTransactionManager txManager;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void beforeClass() {
		mapper = new ObjectMapperFactory().getInstance();
	}

	@Override
	@Before
	public void before() {
		super.before();

		this.fakeTransport = new FakeTransport();
		this.httpClient = HttpClient.withTransport(this.fakeTransport)
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.build();

		this.memberships = new MembershipLookupServiceImpl(this.httpClient, "");
		this.stores = new StoreLookupServiceImpl(this.httpClient, "");
		this.transactions = new TransactionRepository(this.sqlContext);
		this.events = new TransactionMatchedEventRepository(this.transactions, this.sqlContext);
		this.matcher = new TransactionMatcher(
				this.transactions,
				this.events,
				new RxLocalMessageBus(TransactionMatchedEvent.NAME, 1),
				this.txManager,
				new TransactionMatcherProperties(),
				this.stores,
				this.memberships);
		this.service = new TransactionService(
				this.transactions, TransactionValidator.newInstance(), this.matcher, this.txManager);
		this.testTransactions = new TestTransactions();
		this.testLookupResponses = new TestLookupResponses();

		TestSubjects.login(TestAccessTokens.SYSTEM);
	}

	@After
	public void after() {
		TestSubjects.logout();
	}

	@Test
	public void test_submit_transaction() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.storeLookupResponse(mapper),
				this.testLookupResponses.membershipLookupResponse(mapper)));
		this.fakeTransport.setLatency(20);

		Transaction imported = this.service.importTransaction(this.testTransactions.transaction());

		log.info("Asserting pending status.");
		imported = this.transactions.get(imported.getId());
		Assert.assertThat(imported.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(500);
		log.info("Asserting matched status.");
		imported = this.transactions.get(imported.getId());
		Assert.assertThat(imported.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.MATCHED.name()));
		Assert.assertThat(this.events.getByTransactionId(imported.getId()).orElse(null), CoreMatchers.notNullValue());
	}

	@Test
	@Ignore("Tasks get rejected, but service uses submitSafely")
	public void test_submit__thread_pool_exhausted() {
		this.thrown.expect(RejectedExecutionException.class);
		this.fakeTransport.setLatency(10_000);

		for (int idx = 0; idx < 21; idx++) {
			this.service.importTransaction(this.testTransactions.transaction("" + idx, "" + idx));
		}

	}

}
