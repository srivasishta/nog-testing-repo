package com.ngl.micro.transaction.transaction;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JProcessingStatusReasonDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JProcessingStatusReason;
import com.ngl.micro.transaction.match.ProcessingStatusReason;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ProcessingStatusReasonITCase extends AbstractDatabaseITCase {

	private JProcessingStatusReasonDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JProcessingStatusReasonDao(this.sqlContext);
	}

	@Test
	public void test_processing_status_reason_mapping() throws Exception {
		JProcessingStatusReason status;
		assertEquals("Expect equal status type count.", ProcessingStatusReason.values().length, this.statusDao.count());
		for (ProcessingStatusReason type : ProcessingStatusReason.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
