package com.ngl.micro.transaction.transaction;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.transaction.dal.generated.jooq.tables.daos.JTransactionStatusTypeDao;
import com.ngl.micro.transaction.dal.generated.jooq.tables.pojos.JTransactionStatusType;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class TransactionStatusTypeITCase extends AbstractDatabaseITCase {

	private JTransactionStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JTransactionStatusTypeDao(this.sqlContext);
	}

	@Test
	public void test_transaction_status_type_mapping() throws Exception {
		JTransactionStatusType status;
		assertEquals("Expect equal status type count.", TransactionStatus.values().length, this.statusDao.count());
		for (TransactionStatus type : TransactionStatus.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
