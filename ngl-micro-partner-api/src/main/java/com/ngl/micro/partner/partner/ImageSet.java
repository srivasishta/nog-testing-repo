package com.ngl.micro.partner.partner;

import static com.ngl.middleware.util.EqualsUtils.hashValue;

import com.ngl.middleware.util.EqualsUtils;

/**
 * A set of image URLs for standard image sizes.
 *
 * @author Willy du Preez
 *
 */
public class ImageSet {

	private String original;
	private String thumbnail;
	private String medium;
	private String large;

	public String getOriginal() {
		return this.original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getThumbnail() {
		return this.thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getMedium() {
		return this.medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getLarge() {
		return this.large;
	}

	public void setLarge(String large) {
		this.large = large;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += hashValue(this.original);
		result += hashValue(this.thumbnail);
		result += hashValue(this.medium);
		result += hashValue(this.large);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ImageSet)) {
			return false;
		}

		ImageSet other = (ImageSet) obj;

		return EqualsUtils.areEqual(this.original, other.original)
				&& EqualsUtils.areEqual(this.thumbnail, other.thumbnail)
				&& EqualsUtils.areEqual(this.medium, other.medium)
				&& EqualsUtils.areEqual(this.large, other.large);
	}

}
