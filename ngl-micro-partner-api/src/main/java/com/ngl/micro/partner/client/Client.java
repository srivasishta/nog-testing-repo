package com.ngl.micro.partner.client;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * An partner client to access the network of giving with an oauth access token and scope.
 *
 * @author Paco Mendes
 */
public class Client implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-27T09:35:24.276Z */
	public static final class Fields {
		public static final String ACCESS_TOKEN_EXPIRES_IN = "accessTokenExpiresIn";
		public static final String CLIENT_ID_ISSUED_AT = "clientIdIssuedAt";
		public static final String CLIENT_LAST_MODIFIED_AT = "clientLastModifiedAt";
		public static final String CLIENT_NAME = "clientName";
		public static final String CLIENT_SECRET = "clientSecret";
		public static final String CLIENT_SECRET_EXPIRES_AT = "clientSecretExpiresAt";
		public static final String ID = "id";
		public static final String PARTNER_ID = "partnerId";
		public static final String SCOPE = "scope";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
	}

	public enum ClientStatus {
		ACTIVE,
		INACTIVE;
	}

	// TODO JsonProperty client_id
	private UUID id;
	private UUID partnerId;
	private String clientName;
	private String clientSecret;
	private ClientStatus status;
	private String statusReason = "";
	private Set<String> scope = new HashSet<>();
	private Long accessTokenExpiresIn;
	private ZonedDateTime clientIdIssuedAt;
	private ZonedDateTime clientLastModifiedAt;
	private ZonedDateTime clientSecretExpiresAt;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public String getClientName() {
		return this.clientName;
	}

	public void setClientName(String name) {
		this.clientName = name;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public ClientStatus getStatus() {
		return this.status;
	}

	public void setStatus(ClientStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public Set<String> getScope() {
		return this.scope;
	}

	public void setScope(Set<String> scope) {
		this.scope = scope;
	}

	public ZonedDateTime getClientIdIssuedAt() {
		return this.clientIdIssuedAt;
	}

	public void setClientIdIssuedAt(ZonedDateTime clientIdIssuedAt) {
		this.clientIdIssuedAt = clientIdIssuedAt;
	}


	public ZonedDateTime getClientLastModifiedAt() {
		return this.clientLastModifiedAt;
	}

	public void setClientLastModifiedAt(ZonedDateTime clientLastModifiedAt) {
		this.clientLastModifiedAt = clientLastModifiedAt;
	}

	public ZonedDateTime getClientSecretExpiresAt() {
		return this.clientSecretExpiresAt;
	}

	public void setClientSecretExpiresAt(ZonedDateTime clientSecretExpiresAt) {
		this.clientSecretExpiresAt = clientSecretExpiresAt;
	}

	public Long getAccessTokenExpiresIn() {
		return this.accessTokenExpiresIn;
	}

	public void setAccessTokenExpiresIn(Long accessTokenExpiresIn) {
		this.accessTokenExpiresIn = accessTokenExpiresIn;
	}

	@Override
	public String toString() {
		return "Partner Client[id=" + this.id + ", status=" + this.status + "]";
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Client)) {
			return false;
		}

		Client other = (Client) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.clientName, other.clientName) &&
				areEqual(this.clientSecret, other.clientSecret) &&
				areEqual(this.status, other.status) &&
				areEqual(this.statusReason, other.statusReason) &&
				areEqual(this.scope, other.scope) &&
				areEqual(this.clientIdIssuedAt, other.clientIdIssuedAt) &&
				areEqual(this.clientLastModifiedAt, other.clientLastModifiedAt) &&
				areEqual(this.clientSecretExpiresAt, other.clientSecretExpiresAt) &&
				areEqual(this.accessTokenExpiresIn, other.accessTokenExpiresIn);
	}
}
