package com.ngl.micro.partner.client;

import static com.ngl.middleware.util.EqualsUtils.areEqual;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

public class AccessToken implements Identifiable<UUID> {

	private UUID id;
	private ZonedDateTime issuedDate;
	private ZonedDateTime revokedDate;
	private ZonedDateTime expiresDate;

	@Override
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public ZonedDateTime getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(ZonedDateTime issuedDate) {
		this.issuedDate = issuedDate;
	}

	public ZonedDateTime getRevokedDate() {
		return revokedDate;
	}

	public void setRevokedDate(ZonedDateTime revokedDate) {
		this.revokedDate = revokedDate;
	}

	public ZonedDateTime getExpiresDate() {
		return expiresDate;
	}

	public void setExpiresDate(ZonedDateTime expiryDate) {
		this.expiresDate = expiryDate;
	}

	@Override
	public String toString() {
		return "Access Token [id=" + this.id + "]";
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AccessToken)) {
			return false;
		}

		AccessToken other = (AccessToken) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.issuedDate, other.issuedDate) &&
				areEqual(this.revokedDate, other.revokedDate) &&
				areEqual(this.expiresDate, other.expiresDate);
	}

}
