package com.ngl.micro.partner.partner;

import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.EqualsUtils;

/**
 * An account to manage partner clients accessing the network of giving.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class Partner implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-27T09:35:24.214Z */
	public static final class Fields {
		public static final String CLIENTS = "clients";
		public static final String ID = "id";
		public static final String LOGO_IMAGES = "logoImages";
		public static final String LOGO_IMAGES_LARGE = "logoImages.large";
		public static final String LOGO_IMAGES_MEDIUM = "logoImages.medium";
		public static final String LOGO_IMAGES_ORIGINAL = "logoImages.original";
		public static final String LOGO_IMAGES_THUMBNAIL = "logoImages.thumbnail";
		public static final String NAME = "name";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
	}

	public enum PartnerStatus {
		ACTIVE,
		INACTIVE;
	}

	private UUID id;
	private String name;
	private PartnerStatus status;
	private String statusReason = "";
	private ImageSet logoImages = new ImageSet();
	private Set<UUID> clients = new HashSet<>();

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PartnerStatus getStatus() {
		return this.status;
	}

	public void setStatus(PartnerStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public ImageSet getLogoImages() {
		return this.logoImages;
	}

	public void setLogoImages(ImageSet logoImages) {
		this.logoImages = logoImages;
	}

	public Set<UUID> getClients() {
		return this.clients;
	}

	public void setClients(Set<UUID> clients) {
		this.clients = clients;
	}

	@Override
	public String toString() {
		return "Partner Account [id=" + this.id + ", name=" + this.name+ ", status=" + this.status + "]";
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.id);
		result += hashValue(this.name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Partner)) {
			return false;
		}

		Partner other = (Partner) obj;

		return  EqualsUtils.areEqual(this.id, other.id)
				&& EqualsUtils.areEqual(this.name, other.name)
				&& EqualsUtils.areEqual(this.status, other.status)
				&& EqualsUtils.areEqual(this.statusReason, other.statusReason)
				&& EqualsUtils.areEqual(this.logoImages, other.logoImages)
				&& EqualsUtils.areEqual(this.clients, other.clients);
	}
}
