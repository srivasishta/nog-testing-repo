package com.ngl.micro.partner.client;

public final class Scopes {

	private Scopes() {
	}

	public static final String BUSINESS_READ_SCOPE 	= "business.read";
	public static final String BUSINESS_WRITE_SCOPE = "business.write";
	public static final String STORE_READ_SCOPE 	= "store.read";
	public static final String STORE_WRITE_SCOPE	= "store.write";
	public static final String SALE_READ_SCOPE 		= "sale.read";

	public static final String CHARITY_READ_SCOPE 	= "charity.read";
	public static final String CONTRIBUTION_READ_SCOPE 	= "contribution.read";

	public static final String IMAGE_UPLOAD_SCOPE = "image.upload";
	public static final String IMAGE_READ_SCOPE = "image.read";

	public static final String MEMBER_READ_SCOPE 	= "membership.read";
	public static final String MEMBER_WRITE_SCOPE 	= "membership.write";
	public static final String PURCHASE_READ_SCOPE 	= "purchase.read";

	public static final String PARTNER_READ_SCOPE 	= "partner.read";
	public static final String PARTNER_UPDATE_SCOPE = "partner.update";

	public static final String TRANSACTION_READ_SCOPE 	= "transaction.read";
	public static final String TRANSACTION_IMPORT_SCOPE = "transaction.import";

	/* --------------------------------------------------------------------
	 *  System only scopes
	 * -------------------------------------------------------------------- */
	public static final String SYS_PARTNER_WRITE_SCOPE = "system.partner.write";

	public static final String SYS_CLIENT_READ_SCOPE = "system.client.read";
	public static final String SYS_CLIENT_WRITE_SCOPE = "system.client.write";
	public static final String SYS_CLIENT_UPDATE_SCOPE = "system.client.update";

	public static final String SYS_CHARITY_WRITE_SCOPE = "system.charity.write";

}
