package com.ngl.micro.charity;

import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.EqualsUtils;

public class Charity implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-27T09:34:19.738Z */
	public static final class Fields {
		public static final String CATEGORIES = "categories";
		public static final String DESCRIPTION = "description";
		public static final String ID = "id";
		public static final String LOGO_IMAGES = "logoImages";
		public static final String LOGO_IMAGES_LARGE = "logoImages.large";
		public static final String LOGO_IMAGES_MEDIUM = "logoImages.medium";
		public static final String LOGO_IMAGES_ORIGINAL = "logoImages.original";
		public static final String LOGO_IMAGES_THUMBNAIL = "logoImages.thumbnail";
		public static final String NAME = "name";
		public static final String REACH = "reach";
		public static final String REACH_AREAS = "reach.areas";
		public static final String REACH_TYPE = "reach.type";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
	}

	public static final class ReachSearchFields {
		public static final String COUNTRY = "country";
		public static final String REGION = "region";
	}

	public enum CharityStatus {
		ACTIVE,
		INACTIVE;
	}

	public enum CharityCategory {
		HEALTH,
		INCLUSION,
		EDUCATION,
		ENVIRONMENT;
	}

	private UUID id;
	private String name;
	private String description;
	private CharityStatus status;
	private String statusReason = "";
	private Reach reach;
	private ImageSet logoImages = new ImageSet();
	private Set<CharityCategory> categories = new HashSet<>();

	public Charity() {
	}

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CharityStatus getStatus() {
		return this.status;
	}

	public void setStatus(CharityStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public Reach getReach() {
		return this.reach;
	}

	public void setReach(Reach reach) {
		this.reach = reach;
	}

	public ImageSet getLogoImages() {
		return this.logoImages;
	}

	public void setLogoImages(ImageSet logoImages) {
		this.logoImages = logoImages;
	}

	public Set<CharityCategory> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<CharityCategory> categories) {
		this.categories = categories;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.id);
		result += hashValue(this.name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Charity)) {
			return false;
		}

		Charity other = (Charity) obj;

		return EqualsUtils.areEqual(this.id, other.id)
				&& EqualsUtils.areEqual(this.name, other.name)
				&& EqualsUtils.areEqual(this.description, other.description)
				&& EqualsUtils.areEqual(this.status, other.status)
				&& EqualsUtils.areEqual(this.statusReason, other.statusReason)
				&& EqualsUtils.areEqual(this.reach, other.reach)
				&& EqualsUtils.areEqual(this.logoImages, other.logoImages)
				&& EqualsUtils.areEqual(this.categories, other.categories);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
