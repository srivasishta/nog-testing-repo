package com.ngl.micro.charity;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.Set;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * A geographic area represents the areas that a charity operates in.
 *
 * @author Cameron Waldron
 */
public class Area implements Identifiable<Country> {

	private Country country;
	private Set<String> regions;

	public Area() {
	}

	public Area(Country country, Set<String> regions) {
		this.country = country;
		this.regions = regions;
	}

	@Override
	public Country getId() {
		return country;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Set<String> getRegions() {
		return regions;
	}

	public void setRegions(Set<String> regions) {
		this.regions = regions;
	}

	@Override
	public int hashCode() {
		int result = 29;
		result += hashValue(this.country);
		result += hashValue(this.regions);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Area)) {
			return false;
		}

		Area other = (Area) obj;

		return  areEqual(this.country, other.country) &&
				areEqual(this.regions, other.regions);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.country, this.regions);
	}
}