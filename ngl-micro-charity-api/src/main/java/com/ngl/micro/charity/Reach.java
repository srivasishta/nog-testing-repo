package com.ngl.micro.charity;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents the geographic reach of a Charity.
 *
 * @author Paco Mendes
 */
public class Reach {

	public static enum ReachType {
		GLOBAL,
		INTERNATIONAL,
		NATIONAL,
		REGIONAL,
	}

	private ReachType type;
	private Set<Area> areas = new HashSet<>();

	public Reach() {
	}

	public Reach(ReachType type, Set<Area> areas) {
		this.type = type;
		this.areas = areas;
	}

	public ReachType getType() {
		return type;
	}

	public void setType(ReachType type) {
		this.type = type;
	}

	public Set<Area> getAreas() {
		return areas;
	}

	public void setAreas(Set<Area> areas) {
		this.areas = areas;
	}

	@Override
	public int hashCode() {
		int result = 29;
		result += hashValue(this.type);
		result += hashValue(this.areas);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Reach)) {
			return false;
		}

		Reach other = (Reach) obj;

		return  areEqual(this.type, other.type) &&
				areEqual(this.areas, other.areas);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.type, this.areas);
	}
}