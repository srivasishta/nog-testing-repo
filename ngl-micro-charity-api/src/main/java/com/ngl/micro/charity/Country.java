/*
 *
 */
package com.ngl.micro.charity;

import java.util.Locale;

/**
 * 2 letter ISO 3166 country codes taken from {@link Locale}.
 *
 *TODO merge into shared library
 */
public enum Country {
	CA,  // Canada
	US;  // United States of America
}
