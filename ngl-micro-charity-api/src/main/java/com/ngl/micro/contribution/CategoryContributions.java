package com.ngl.micro.contribution;

import java.math.BigDecimal;

import com.ngl.micro.charity.Charity.CharityCategory;

public class CategoryContributions {

	private BigDecimal total;
	private CharityCategory category;

	public CategoryContributions() {
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public CharityCategory getCategory() {
		return this.category;
	}

	public void setCategory(CharityCategory category) {
		this.category = category;
	}

}
