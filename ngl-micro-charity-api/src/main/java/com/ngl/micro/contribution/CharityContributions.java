package com.ngl.micro.contribution;

import java.math.BigDecimal;

public class CharityContributions {

	private BigDecimal total;

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
