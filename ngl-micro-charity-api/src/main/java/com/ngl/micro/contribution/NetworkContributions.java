package com.ngl.micro.contribution;

import java.math.BigDecimal;
import java.util.Set;

public class NetworkContributions {

	private BigDecimal total;
	private Set<CategoryContributions> categoryContributions;

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Set<CategoryContributions> getCategoryContributions() {
		return this.categoryContributions;
	}

	public void setCategoryContributions(Set<CategoryContributions> categoryContributions) {
		this.categoryContributions = categoryContributions;
	}

}
