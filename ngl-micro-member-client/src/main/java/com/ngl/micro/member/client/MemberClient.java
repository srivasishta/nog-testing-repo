package com.ngl.micro.member.client;

import java.util.UUID;

import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.hal.TypedResource;

/**
 * The Member Service Client.
 *
 * @author Willy du Preez
 */
public class MemberClient extends AbstractRestClient {

	public class MembershipResource extends ResourceTemplate<Membership, UUID> {

		private MembershipResource() {
			super(Membership.class, "memberships/");
		}

		public TypedResource<Page<Purchase>> purchases(UUID membershipId, PageRequest page) {
			Request request = MemberClient.this.http.request(this.actionPath() + membershipId + "/purchases/")
					.queryParams(MemberClient.this.qpParser.asQueryParameters(page));

			return MemberClient.this.executePagedRequest(Purchase.class, request);
		}

		public TypedResource<PurchaseSummary> purchaseSummary(UUID membershipId) {
			Request request = MemberClient.this.http.request(this.actionPath() + membershipId + "/purchases/summary");

			return MemberClient.this.executeRequest(PurchaseSummary.class, request);
		}

		public TypedResource<PurchaseSummary> purchaseSummary(UUID membershipId, SearchQuery query) {
			Request request = MemberClient.this.http
					.request(this.actionPath() + membershipId + "/purchases/summary")
					.queryParams(MemberClient.this.qpParser.asQueryParameters(query));

			return MemberClient.this.executeRequest(PurchaseSummary.class, request);
		}

	}

	private MembershipResource membershipResource;


	public MemberClient(RestClientConfig config) {
		super(config);

		this.membershipResource = new MembershipResource();
	}

	public MembershipResource getMembershipResource() {
		return this.membershipResource;
	}

}
