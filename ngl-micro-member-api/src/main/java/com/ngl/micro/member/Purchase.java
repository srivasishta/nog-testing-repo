package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * Represents a purchase made by a member at a store with a resulting donation.
 */
public class Purchase implements Identifiable<UUID>{

	/** Generated. Do not modify. 2015-10-02T07:22:18.450Z */
	public static final class Fields {
		public static final String BUSINESS_ID = "businessId";
		public static final String CONTRIBUTION_AMOUNT = "contributionAmount";
		public static final String ID = "id";
		public static final String PURCHASE_AMOUNT = "purchaseAmount";
		public static final String PURCHASE_DATE = "purchaseDate";
		public static final String STORE_ID = "storeId";
	}



	private UUID id;
	private UUID businessId;
	private UUID storeId;
	private ZonedDateTime purchaseDate;
	private BigDecimal purchaseAmount;
	private BigDecimal contributionAmount;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(UUID businessId) {
		this.businessId = businessId;
	}

	public UUID getStoreId() {
		return this.storeId;
	}

	public void setStoreId(UUID storeId) {
		this.storeId = storeId;
	}

	public ZonedDateTime getPurchaseDate() {
		return this.purchaseDate;
	}

	public void setPurchaseDate(ZonedDateTime date) {
		this.purchaseDate = date;
	}

	public BigDecimal getPurchaseAmount() {
		return this.purchaseAmount;
	}

	public void setPurchaseAmount(BigDecimal purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public BigDecimal getContributionAmount() {
		return this.contributionAmount;
	}

	public void setContributionAmount(BigDecimal contributionAmount) {
		this.contributionAmount = contributionAmount;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.id);
		result += hashValue(this.storeId);
		result += hashValue(this.purchaseDate);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Purchase)) {
			return false;
		}

		Purchase other = (Purchase) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.storeId, other.storeId) &&
				areEqual(this.purchaseDate, other.purchaseDate) &&
				areEqual(this.purchaseAmount, other.purchaseAmount) &&
				areEqual(this.contributionAmount, other.contributionAmount);
	}

	@Override
	public String toString() {
		return String.format("%s %s", purchaseDate, purchaseAmount);
	}
}
