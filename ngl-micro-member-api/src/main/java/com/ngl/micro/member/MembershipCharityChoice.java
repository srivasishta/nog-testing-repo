package com.ngl.micro.member;

import java.util.UUID;

public class MembershipCharityChoice {
	private UUID membershipId;
	private UUID charityId;
	private Integer allocation;

	public UUID getMembershipId() {
		return membershipId;
	}

	public void setMembershipId(UUID memberId) {
		this.membershipId = memberId;
	}

	public UUID getCharityId() {
		return charityId;
	}

	public void setCharityId(UUID charityId) {
		this.charityId = charityId;
	}

	public Integer getAllocation() {
		return allocation;
	}

	public void setAllocation(Integer allocation) {
		this.allocation = allocation;
	}
}
