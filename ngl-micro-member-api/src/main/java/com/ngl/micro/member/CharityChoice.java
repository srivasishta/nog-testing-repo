package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * Represents the charity choice for a Membership over a given time frame.
 * Charity allocations are represented as an {@link Integer} for 1:100
 *
 * @author Paco Mendes
 */
public class CharityChoice implements Identifiable<UUID> {

	private UUID charityId;
	private Integer allocation;

	public CharityChoice() {
	}

	public CharityChoice(UUID charityId, Integer allocation) {
		this.charityId = charityId;
		this.allocation = allocation;
	}

	@Override
	public UUID getId() {
		return charityId;
	}

	public UUID getCharityId() {
		return charityId;
	}

	public void setCharityId(UUID charityId) {
		this.charityId = charityId;
	}

	public Integer getAllocation() {
		return allocation;
	}

	public void setAllocation(Integer allocation) {
		this.allocation = allocation;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += hashValue(this.charityId);
		result += hashValue(this.allocation);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CharityChoice)) {
			return false;
		}

		CharityChoice other = (CharityChoice) obj;

		return  areEqual(this.charityId, other.charityId) &&
				areEqual(this.allocation, other.allocation);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.charityId, this.allocation);
	}

}
