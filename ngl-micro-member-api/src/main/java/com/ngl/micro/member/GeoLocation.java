package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

/**
 * lat-lon geo location using decimal degrees.
 *
 * @author Paco Mendes
 */
public class GeoLocation {

	private Double lat;
	private Double lon;

	/**
	 * Instantiate a location using a given lat lon in decimal degrees.
	 *
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @return the geo location
	 */
	public static GeoLocation of(Double latitude, Double longitude) {
		GeoLocation location = new GeoLocation();
		location.setLat(latitude);
		location.setLon(longitude);
		return location;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double latitude) {
		this.lat = latitude;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double longitude) {
		this.lon = longitude;
	}

	@Override
	public String toString() {
		return String.format("[%s, %s]", lat, lon);
	}

	@Override
	public int hashCode() {
		int result = 31;
		result += hashValue(this.lat);
		result += hashValue(this.lon);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GeoLocation)) {
			return false;
		}

		GeoLocation other = (GeoLocation) obj;

		return  areEqual(this.lat, other.lat) &&
				areEqual(this.lon, other.lon);
	}

}