package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;

/**
 * Represents a summary of purchases made by a member over all time.
 */
public class PurchaseSummary {

	/** Generated. Do not modify. 2015-10-02T07:22:18.467Z */
	public static final class Fields {
		public static final String TOTAL_AMOUNT = "totalAmount";
		public static final String TOTAL_CONTRIBUTED = "totalContributed";
		public static final String TOTAL_PURCHASES = "totalPurchases";
	}

	private Integer totalPurchases;
	private BigDecimal totalAmount;
	private BigDecimal totalContributed;

	public Integer getTotalPurchases() {
		return this.totalPurchases;
	}

	public void setTotalPurchases(Integer totalPurchases) {
		this.totalPurchases = totalPurchases;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalContributed() {
		return this.totalContributed;
	}

	public void setTotalContributed(BigDecimal totalDonated) {
		this.totalContributed = totalDonated;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.totalPurchases);
		result += hashValue(this.totalAmount);
		result += hashValue(this.totalContributed);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PurchaseSummary)) {
			return false;
		}

		PurchaseSummary other = (PurchaseSummary) obj;

		return  areEqual(this.totalPurchases, other.totalPurchases) &&
				areEqual(this.totalAmount, other.totalAmount) &&
				areEqual(this.totalContributed, other.totalContributed);
	}
}
