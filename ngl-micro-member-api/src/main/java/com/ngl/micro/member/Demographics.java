package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.time.LocalDate;

public class Demographics {

	public static enum Gender {
		MALE, FEMALE, UNSPECIFIED;
	}

	private Gender gender = Gender.UNSPECIFIED;
	private LocalDate birthdate;
	private Country country;
	private String region;
	private GeoLocation geoLocation;

	public Gender getGender() {
		return this.gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public GeoLocation getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.gender);
		result += hashValue(this.birthdate);
		result += hashValue(this.country);
		result += hashValue(this.region);
		result += hashValue(this.geoLocation);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Demographics)) {
			return false;
		}

		Demographics other = (Demographics) obj;

		return  areEqual(this.gender, other.gender) &&
				areEqual(this.birthdate, other.birthdate) &&
				areEqual(this.country, other.country) &&
				areEqual(this.region, other.region) &&
				areEqual(this.geoLocation, other.geoLocation);
	}

	@Override
	public String toString() {
		return String.format("%s:%s:%s:%s", this.gender, this.birthdate, this.country, this.region);
	}
}
