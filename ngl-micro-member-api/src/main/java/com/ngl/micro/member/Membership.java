package com.ngl.micro.member;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

public class Membership implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-26T09:09:31.773Z */
	public static final class Fields {
		public static final String CHARITY_CHOICES = "charityChoices";
		public static final String CHARITY_CHOICES_ALLOCATION = "charityChoices.allocation";
		public static final String CHARITY_CHOICES_CHARITY_ID = "charityChoices.charityId";
		public static final String DEMOGRAPHICS = "demographics";
		public static final String DEMOGRAPHICS_BIRTHDATE = "demographics.birthdate";
		public static final String DEMOGRAPHICS_COUNTRY = "demographics.country";
		public static final String DEMOGRAPHICS_GENDER = "demographics.gender";
		public static final String DEMOGRAPHICS_GEO_LOCATION = "demographics.geoLocation";
		public static final String DEMOGRAPHICS_GEO_LOCATION_LAT = "demographics.geoLocation.lat";
		public static final String DEMOGRAPHICS_GEO_LOCATION_LON = "demographics.geoLocation.lon";
		public static final String DEMOGRAPHICS_REGION = "demographics.region";
		public static final String EXTERNAL_ID = "externalId";
		public static final String ID = "id";
		public static final String PARTNER_ID = "partnerId";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
	}

	private UUID id;
	private UUID partnerId;
	private String externalId;
	private MembershipStatus status;
	private String statusReason = "";
	private Demographics demographics;
	private Set<CharityChoice> charityChoices = new HashSet<>();

	public enum MembershipStatus {
		ACTIVE,
		INACTIVE;
	}

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public String getExternalId() {
		return this.externalId;
	}

	public void setExternalId(String identifier) {
		this.externalId = identifier;
	}

	public MembershipStatus getStatus() {
		return this.status;
	}

	public void setStatus(MembershipStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public Demographics getDemographics() {
		return this.demographics;
	}

	public void setDemographics(Demographics demographics) {
		this.demographics = demographics;
	}

	public Set<CharityChoice> getCharityChoices() {
		return this.charityChoices;
	}

	public void setCharityChoices(Set<CharityChoice> charityChoices) {
		this.charityChoices = charityChoices;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += hashValue(this.id);
		result += hashValue(this.partnerId);
		result += hashValue(this.externalId);
		result += hashValue(this.status);
		result += hashValue(this.statusReason);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Membership)) {
			return false;
		}

		Membership other = (Membership) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.partnerId, other.partnerId) &&
				areEqual(this.externalId, other.externalId) &&
				areEqual(this.status, other.status) &&
				areEqual(this.statusReason, other.statusReason) &&
				areEqual(this.demographics, other.demographics) &&
				areEqual(this.charityChoices, other.charityChoices);
	}

	@Override
	public String toString() {
		return String.format("%s:%s:%s", this.id, this.partnerId, this.externalId);
	}

}
