package com.ngl.micro.charity.client;

import java.util.UUID;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.contribution.CharityContributions;
import com.ngl.micro.contribution.NetworkContributions;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.hal.TypedResource;

/**
 * The Charity Service Client.
 *
 * @author Cameron Waldron
 */
public class CharityClient extends AbstractRestClient {

	public class CharityResource extends ResourceTemplate<Charity, UUID> {
		private CharityResource() {
			super(Charity.class, "charities/");
		}

		public TypedResource<CharityContributions> charityContributions(UUID charityId, SearchQuery searchQuery) {
			Request request = CharityClient.this.http
					.request(this.actionPath() + charityId + "/contributions/summary")
					.queryParams(CharityClient.this.qpParser.asQueryParameters(searchQuery));

			return CharityClient.this.executeRequest(CharityContributions.class, request);
		}

		public TypedResource<CharityContributions> charityContributions(UUID charityId) {
			return this.charityContributions(charityId, new SearchQuery());
		}

	}

	public class ContributionResource {

		private String relativePath = "contributions/";

		public String actionPath(){
			return CharityClient.this.serviceUrl + this.relativePath;
		}

		public TypedResource<NetworkContributions> networkContributions(SearchQuery searchQuery) {
			Request request = CharityClient.this.http
					.request(this.actionPath() + "summary")
					.queryParams(CharityClient.this.qpParser.asQueryParameters(searchQuery));

			return CharityClient.this.executeRequest(NetworkContributions.class, request);
		}

		/**
		 * Gets the total network contributions over all time.
		 *
		 * @return the typed resource
		 */
		public TypedResource<NetworkContributions> networkContributions() {
			return this.networkContributions(new SearchQuery());
		}

	}

	private CharityResource charityResource;
	private ContributionResource contributionResource;

	public CharityClient(RestClientConfig config) {
		super(config);
		this.contributionResource = new ContributionResource();
		this.charityResource = new CharityResource();
	}

	public CharityResource getCharityResource() {
		return this.charityResource;
	}

	public ContributionResource getContributionResource() {
		return this.contributionResource;
	}

}
