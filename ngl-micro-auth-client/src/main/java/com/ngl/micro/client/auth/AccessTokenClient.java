package com.ngl.micro.client.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Methods;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Uris;

/**
 * An access token client capable of requesting and revoking OAuth 2.0 tokens.
 *
 * @author Willy du Preez
 *
 */
public class AccessTokenClient {

	private static final Logger log = LoggerFactory.getLogger(AccessTokenClient.class);

	private static final String CLIENT_ID = "client_id";
	private static final String CLIENT_SECRET = "client_secret";
	private static final String GRANT_TYPE = "grant_type";
	private static final String CLIENT_CREDENTIALS_GRANT = "client_credentials";
	private static final String REVOKED_TOKEN_ID = "token";
	private static final String SCOPE = "scope";

	public static Builder builder(HttpClient httpClient) {
		return new Builder(httpClient);
	}

	public static class Builder {

		private AccessTokenClient client;

		private Builder(HttpClient httpClient) {
			Assert.notNull(httpClient);
			this.client = new AccessTokenClient(httpClient);
		}

		public Builder withTokenUrl(String tokenUrl) {
			Uris.toUri(tokenUrl);
			this.client.tokenUrl = tokenUrl;
			return this;
		}

		public Builder withRevocationUrl(String revocationUrl) {
			Uris.toUri(revocationUrl);
			this.client.revocationUrl = revocationUrl;
			return this;
		}

		public AccessTokenClient build() {
			return this.client;
		}

	}

	private String tokenUrl = "http://localhost:9096/service/partner/token";
	private String revocationUrl = "http://localhost:9096/service/partner/revoke";
	private HttpClient httpClient;

	private AccessTokenClient(HttpClient httpClient) {
		Assert.notNull(httpClient);
		this.httpClient = httpClient;
	}

	public AccessTokenResponse issueToken(AccessTokenRequest request) {
		String body = this.requestBody(request);
		RestResponse response = this.httpClient.request(this.tokenUrl)
				.method(Methods.POST)
				.header(HttpHeaders.CONTENT_TYPE, MediaTypes.APPLICATION_FORM_URLENCODED)
				.body(body)
				.execute(RestResponse.class);

		log.debug("Issue token response: {}", response.toString());

		if (response.getStatus() == HttpStatus.SC_OK) {
			return response.bodyAsResource(AccessTokenResponse.class).getState();
		}
		else if (response.getStatus() == HttpStatus.SC_UNAUTHORIZED) {
			throw ApplicationError.notAuthorized().asException();
		}
		else if (response.getStatus() == HttpStatus.SC_BAD_REQUEST) {
			JsonNode json = response.bodyAsJsonNode();
			String message = json.has("error") ? json.get("error").asText() : "";
			throw ApplicationError.notAuthorized().asException(message);
		}else {
			throw new RuntimeException(response.toString());
		}
	}

	private String requestBody(AccessTokenRequest request) {
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair(GRANT_TYPE, CLIENT_CREDENTIALS_GRANT));
		urlParameters.add(new BasicNameValuePair(CLIENT_ID, request.getClientId()));
		urlParameters.add(new BasicNameValuePair(CLIENT_SECRET, request.getClientSecret()));
		if (request.getScopes() != null && !request.getScopes().isEmpty()) {
			urlParameters.add(new BasicNameValuePair(SCOPE, String.join(" ", request.getScopes())));
		}

		try {
			HttpEntity entity = new UrlEncodedFormEntity(urlParameters);
			return EntityUtils.toString(entity);
		} catch (IOException e) {
			throw new RuntimeException("Failed to create request body", e);
		}
	}

	public RevokeAccessTokenResponse revokeToken(RevokeAccessTokenRequest request) {
		String body = this.revocationBody(request);
		RestResponse response = this.httpClient.request(this.revocationUrl)
				.method(Methods.POST)
				.header(HttpHeaders.CONTENT_TYPE, MediaTypes.APPLICATION_FORM_URLENCODED)
				.body(body)
				.execute(RestResponse.class);

		log.debug("Revoke token response: {}", response.toString());

		if (response.getStatus() == HttpStatus.SC_OK) {
			return new RevokeAccessTokenResponse();
		}
		else if (response.getStatus() == HttpStatus.SC_UNAUTHORIZED) {
			throw ApplicationError.notAuthorized().asException("Failed to revoke token.");
		}
		else if (response.getStatus() == HttpStatus.SC_BAD_REQUEST) {
			throw ApplicationError.badRequest().asException();
		}
		else {
			throw new RuntimeException(response.toString());
		}
	}

	private String revocationBody(RevokeAccessTokenRequest request) {
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair(GRANT_TYPE, CLIENT_CREDENTIALS_GRANT));
		urlParameters.add(new BasicNameValuePair(CLIENT_ID, request.getClientId()));
		urlParameters.add(new BasicNameValuePair(CLIENT_SECRET, request.getClientSecret()));
		urlParameters.add(new BasicNameValuePair(REVOKED_TOKEN_ID, request.getAccessToken()));

		try {
			HttpEntity entity = new UrlEncodedFormEntity(urlParameters);
			return EntityUtils.toString(entity);
		} catch (IOException e) {
			throw new RuntimeException("Failed to create request body", e);
		}
	}

}
