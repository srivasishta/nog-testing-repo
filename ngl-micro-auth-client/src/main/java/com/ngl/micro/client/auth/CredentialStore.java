package com.ngl.micro.client.auth;

/**
 * A {@link CredentialStore} provides the client ID and secret.
 *
 * @author Willy du Preez
 *
 */
public interface CredentialStore {

	String getClientId();
	String getClientSecret();

}
