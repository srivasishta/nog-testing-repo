package com.ngl.micro.client.auth;

/**
 * Properties that could be passed to the {@link CredentialFactory} to
 * customize the creation of a {@link Credential}.
 *
 * @author Willy du Preez
 *
 */
public class CredentialProperties implements CredentialStore {

	public static final String DEFAULT_PREFIX = "client.auth.";

	private String clientId;
	private String clientSecret;
	private String tokenUrl;
	private String revocationUrl;
	private long intialDelayMillis = 10_000;

	@Override
	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getTokenUrl() {
		return this.tokenUrl;
	}

	public void setTokenUrl(String tokenUrl) {
		this.tokenUrl = tokenUrl;
	}

	public String getRevocationUrl() {
		return this.revocationUrl;
	}

	public void setRevocationUrl(String revocationUrl) {
		this.revocationUrl = revocationUrl;
	}

	public long getIntialDelayMillis() {
		return this.intialDelayMillis;
	}

	public void setIntialDelayMillis(long intialDelayMillis) {
		this.intialDelayMillis = intialDelayMillis;
	}

}
