package com.ngl.micro.client.auth;

/**
 * An access token revocation request.
 *
 * @author Willy du Preez
 *
 */
public class RevokeAccessTokenRequest {

	private String clientId;
	private String clientSecret;
	private String accessToken;

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
