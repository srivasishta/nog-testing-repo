package com.ngl.micro.client.auth;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.client.http.RequestFilter;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.BackOff;
import com.ngl.middleware.util.backoff.LinearBackOff;

/**
 * Manages a REST API credential. The {@link Credential} will automatically
 * request new access tokens a configurable amount of time before the
 * existing token expires, revoke access tokens once a new token has been
 * issued and backs off in case of failure to request or revoke access
 * tokens.
 *
 * @author Willy du Preez
 *
 */
public class Credential implements RequestFilter {

	private static final Logger log = LoggerFactory.getLogger(Credential.class);

	private static final String DEFAULT_AUTHORIZATION_TYPE = "Bearer";

	private static final long ONE_MINUTE_IN_MILLIS = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);

	private static final long DEFAULT_INITIAL_DELAY = 10_000;
	private static final long DEFAULT_REISSUE_BEFORE_EXPIRY = ONE_MINUTE_IN_MILLIS * 10;
	private static final BackOff DEFAULT_REISSUE_BACK_OFF = new LinearBackOff(30_000, 3_000, 3_000);

	private static final long DEFAULT_REVOCATION_DELAY = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
	private static final BackOff DEFAULT_REVOCATION_BACK_OFF = new LinearBackOff(30_000, 3_000, 3_000);
	private static final int DEFAULT_REVOCATION_RETRIES = 3;

	public static Builder builder(AccessTokenClient client, CredentialStore store) {
		return new Builder(client, store);
	}

	public static class Builder {

		private Credential credential;

		private Builder(AccessTokenClient client, CredentialStore store) {
			Assert.notNull(client);
			Assert.notNull(store);
			this.credential = new Credential();
			this.credential.client = client;
			this.credential.credentialStore = store;
		}

		public Builder withReissueBackOff(BackOff backOff) {
			Assert.notNull(backOff);
			this.credential.reissueBackOff = backOff;
			return this;
		}

		public Builder withInitialDelayMillis(long delayMillis) {
			Assert.isTrue(delayMillis > 0);
			this.credential.initialDelayMillis = delayMillis;
			return this;
		}

		public Builder withInitialDelay(long delay, TimeUnit unit) {
			Assert.isTrue(delay > 0);
			this.credential.initialDelayMillis = unit.toMillis(delay);
			return this;
		}

		public Builder reissueBeforeExpiry(long duration, TimeUnit unit) {
			Assert.isTrue(duration > 0);
			this.credential.reissueMillisBeforeExpiry = unit.toMillis(duration);
			return this;
		}

		public Builder reissueBeforeExpiryMillis(long durationMillis) {
			Assert.isTrue(durationMillis > 0);
			this.credential.reissueMillisBeforeExpiry = durationMillis;
			return this;
		}

		public Builder withRevocationBackOff(BackOff backOff) {
			Assert.notNull(backOff);
			this.credential.revocationBackOff = backOff;
			return this;
		}

		public Builder withRevocationDelay(long delay, TimeUnit unit) {
			Assert.isTrue(delay > 0);
			this.credential.revocationDelayMillis = unit.toMillis(delay);
			return this;
		}

		public Builder withRevocationDelayMillis(long delayMillis) {
			Assert.isTrue(delayMillis > 0);
			this.credential.revocationDelayMillis = delayMillis;
			return this;
		}

		public Builder withRevocationRetries(int count) {
			Assert.isTrue(count >= 0);
			this.credential.revocationRetries = count;
			return this;
		}

		public Credential build() {
			this.credential.revocationExecutor = Executors.newSingleThreadScheduledExecutor();
			this.credential.reissueExecutor = Executors.newSingleThreadScheduledExecutor();
			return this.credential;
		}

	}

	private Lock lock = new ReentrantLock();

	private long initialDelayMillis = DEFAULT_INITIAL_DELAY;
	private BackOff reissueBackOff = DEFAULT_REISSUE_BACK_OFF;
	private long reissueMillisBeforeExpiry = DEFAULT_REISSUE_BEFORE_EXPIRY;
	private ScheduledExecutorService reissueExecutor;

	private long revocationRetries = DEFAULT_REVOCATION_RETRIES;
	private BackOff revocationBackOff = DEFAULT_REVOCATION_BACK_OFF;
	private long revocationDelayMillis = DEFAULT_REVOCATION_DELAY;
	private ScheduledExecutorService revocationExecutor;

	private AccessTokenClient client;
	private CredentialStore credentialStore;
	private String accessToken;

	private Credential() {
	}

	@Override
	public void filter(Request request) {
		if (!Strings.isNullOrWhitespace(this.accessToken)) {
			request.header(Headers.CLIENT_AUTHORIZATION, DEFAULT_AUTHORIZATION_TYPE + " " + this.getAccessToken());
		}
	}

	public void init() {
		this.reissueExecutor.schedule(
				new RequestTokenTask(),
				this.initialDelayMillis,
				TimeUnit.MILLISECONDS);
	}

	public void close() {
		this.reissueExecutor.shutdownNow();
		this.revocationExecutor.shutdownNow();
	}

	private String getAccessToken() {
		this.lock.lock();
		try {
			return this.accessToken;
		} finally {
			this.lock.unlock();
		}
	}

	private void setAccessToken(String accessToken) {
		this.lock.lock();
		try {
			this.accessToken = accessToken;
		} finally {
			this.lock.unlock();
		}
	}

	private class RequestTokenTask implements Runnable {

		@Override
		public void run() {
			try {
				AccessTokenResponse response = this.requestToken();
				this.scheduleRevocation(Credential.this.getAccessToken());
				this.scheduleReissue(response.getExpiresAt());
				Credential.this.setAccessToken(response.getAccessToken());
				Credential.this.reissueBackOff.reset();
			} catch (Exception e) {
				long backOff = Credential.this.reissueBackOff.nextBackOff();
				log.warn("Failed to issue access token. Trying again in {}ms", backOff, e);
				Credential.this.reissueExecutor.schedule(new RequestTokenTask(), backOff, TimeUnit.MILLISECONDS);
			}
		}

		private AccessTokenResponse requestToken() {
			AccessTokenRequest request = new AccessTokenRequest();
			request.setClientId(Credential.this.credentialStore.getClientId());
			request.setClientSecret(Credential.this.credentialStore.getClientSecret());
			return Credential.this.client.issueToken(request);
		}

		private void scheduleRevocation(String accessToken) {
			if (!Strings.isNullOrWhitespace(accessToken)) {
				RevokeTokenTask revocation = new RevokeTokenTask(accessToken);
				Credential.this.revocationExecutor.schedule(revocation,
						Credential.this.revocationDelayMillis,
						TimeUnit.MILLISECONDS);
			}
		}

		private void scheduleReissue(ZonedDateTime expiresAtUtc) {
			ZonedDateTime reissueAtUtc = expiresAtUtc.minus(
					Credential.this.reissueMillisBeforeExpiry,
					ChronoUnit.MILLIS);
			Duration delay = Duration.between(Time.utcNow(), reissueAtUtc);
			if (delay.isNegative()) {
				delay = Duration.of(Credential.this.initialDelayMillis, ChronoUnit.MILLIS);
			}
			Credential.this.reissueExecutor.schedule(new RequestTokenTask(), delay.toMillis(), TimeUnit.MILLISECONDS);
		}

	}

	private class RevokeTokenTask implements Runnable {

		private String tokenToRevoke;
		private int retries;

		public RevokeTokenTask(String tokenToRevoke) {
			this.tokenToRevoke = tokenToRevoke;
		}

		@Override
		public void run() {
			try {
				RevokeAccessTokenRequest request = new RevokeAccessTokenRequest();
				request.setAccessToken(this.tokenToRevoke);
				request.setClientId(Credential.this.credentialStore.getClientId());
				request.setClientSecret(Credential.this.credentialStore.getClientSecret());
				Credential.this.client.revokeToken(request);
				Credential.this.revocationBackOff.reset();
			} catch (Exception e) {
				if (this.retries < Credential.this.revocationRetries) {
					this.retries++;
					long backOff = Credential.this.revocationBackOff.nextBackOff();
					log.warn("Failed to revoke access token [{} of {}]. Trying again in {}ms",
							this.retries, Credential.this.revocationRetries, backOff, e);
					Credential.this.revocationExecutor.schedule(this, backOff, TimeUnit.MILLISECONDS);
				}
				else {
					log.warn("Failed to revoke access token. Maximum retries of {} reached.",
							Credential.this.revocationRetries, e);
				}
			}
		}

	}

}
