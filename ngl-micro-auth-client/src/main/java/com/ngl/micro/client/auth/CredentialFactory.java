package com.ngl.micro.client.auth;

import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.request.ApacheHttpTransport;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.json.ObjectMapperFactory;

/**
 * A factory that creates a sensibly configured default {@link Credential}.
 *
 * @author Willy du Preez
 *
 */
public class CredentialFactory {

	public Credential newInstance(CredentialProperties properties) {
		ObjectMapper mapper = new ObjectMapperFactory().getInstance();

		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(2);
		cm.setDefaultMaxPerRoute(1);
		cm.setValidateAfterInactivity(1);

		CloseableHttpClient http = HttpClients.custom()
				.setConnectionManager(cm)
				.setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
				.build();

		HttpClient httpClient = HttpClient.withTransport(new ApacheHttpTransport(http))
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.build();

		AccessTokenClient client = AccessTokenClient.builder(httpClient)
				.withRevocationUrl(properties.getRevocationUrl())
				.withTokenUrl(properties.getTokenUrl())
				.build();

		return Credential.builder(client, properties)
				.withInitialDelayMillis(properties.getIntialDelayMillis())
				.build();
	}

}
