package com.ngl.micro.client.auth;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.middleware.util.Collections;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.Time;

/**
 * An access token response. Note that expires at is in UTC.
 *
 * @author Willy du Preez
 *
 */
public class AccessTokenResponse {

	private Set<String> approvedScopes;
	private ZonedDateTime expiresAt;
	private String accessToken;
	private String accessTokenType;

	@JsonCreator
	public AccessTokenResponse(
			@JsonProperty("approvedScope") String approvedScope,
			@JsonProperty("expiresIn") long expiresIn,
			@JsonProperty("issuedAt") long issuedAt,
			@JsonProperty("tokenKey") String tokenKey,
			@JsonProperty("tokenType") String tokenType) {

		this.approvedScopes = this.parseScope(approvedScope);
		this.accessToken = tokenKey;
		this.accessTokenType = tokenType;
		this.expiresAt = Time.utcNow().plusSeconds(expiresIn);
	}

	private Set<String> parseScope(String approvedScope) {
		if (Strings.isNullOrWhitespace(approvedScope)) {
			return new HashSet<>();
		}
		else {
			return Collections.asSet(approvedScope.split(Strings.SPACE));
		}
	}

	public Set<String> getApprovedScopes() {
		return this.approvedScopes;
	}

	public void setApprovedScopes(Set<String> approvedScopes) {
		this.approvedScopes = approvedScopes;
	}

	public ZonedDateTime getExpiresAt() {
		return this.expiresAt;
	}

	public void setExpiresAt(ZonedDateTime expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessTokenType() {
		return this.accessTokenType;
	}

	public void setAccessTokenType(String accessTokenType) {
		this.accessTokenType = accessTokenType;
	}

}
