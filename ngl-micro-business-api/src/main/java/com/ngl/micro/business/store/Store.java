package com.ngl.micro.business.store;

import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.TradeCategory;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.EqualsUtils;

/**
 * A business store with a physical address.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class Store implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-20T14:37:40.029Z */
	public static final class Fields {
		public static final String ADDRESS = "address";
		public static final String ADDRESS_COUNTRY = "address.country";
		public static final String ADDRESS_ID = "address.id";
		public static final String ADDRESS_LOCALITY = "address.locality";
		public static final String ADDRESS_POSTAL_CODE = "address.postalCode";
		public static final String ADDRESS_REGION = "address.region";
		public static final String ADDRESS_STREET_ADDRESS = "address.streetAddress";
		public static final String BUSINESS_CATEGORIES = "businessCategories";
		public static final String BUSINESS_ID = "businessId";
		public static final String BUSINESS_KEYWORDS = "businessKeywords";
		public static final String CONTRIBUTION_RATE = "contributionRate";
		public static final String DESCRIPTION = "description";
		public static final String EMAIL_ADDRESS = "emailAddress";
		public static final String EXTERNAL_IDENTIFIERS = "externalIdentifiers";
		public static final String EXTERNAL_IDENTIFIERS_END_DATE = "externalIdentifiers.endDate";
		public static final String EXTERNAL_IDENTIFIERS_ID = "externalIdentifiers.id";
		public static final String EXTERNAL_IDENTIFIERS_KEY = "externalIdentifiers.key";
		public static final String EXTERNAL_IDENTIFIERS_START_DATE = "externalIdentifiers.startDate";
		public static final String GEO_LOCATION = "geoLocation";
		public static final String GEO_LOCATION_LAT = "geoLocation.lat";
		public static final String GEO_LOCATION_LON = "geoLocation.lon";
		public static final String ID = "id";
		public static final String LOGO_IMAGES = "logoImages";
		public static final String LOGO_IMAGES_LARGE = "logoImages.large";
		public static final String LOGO_IMAGES_MEDIUM = "logoImages.medium";
		public static final String LOGO_IMAGES_ORIGINAL = "logoImages.original";
		public static final String LOGO_IMAGES_THUMBNAIL = "logoImages.thumbnail";
		public static final String NAME = "name";
		public static final String PHONE_NUMBERS = "phoneNumbers";
		public static final String PHONE_NUMBERS_AREA_CODE = "phoneNumbers.areaCode";
		public static final String PHONE_NUMBERS_COUNTRY_CODE = "phoneNumbers.countryCode";
		public static final String PHONE_NUMBERS_EXTENSION = "phoneNumbers.extension";
		public static final String PHONE_NUMBERS_ID = "phoneNumbers.id";
		public static final String PHONE_NUMBERS_NUMBER = "phoneNumbers.number";
		public static final String PHONE_NUMBERS_TYPE = "phoneNumbers.type";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
		public static final String STORE_CATEGORIES = "storeCategories";
		public static final String STORE_KEYWORDS = "storeKeywords";
		public static final String TIMEZONE = "timezone";
		public static final String TRADING_HOURS = "tradingHours";
		public static final String WEBSITE = "website";
	}

	public static final class LocationFields {
		public static final String LAT = "lat";
		public static final String LON = "lon";
		public static final String RADIUS = "radius";
	}

	public enum StoreStatus {
		ACTIVE,
		INACTIVE;
	}

	private UUID id;
	private UUID businessId;
	private String name;
	private String description;
	private StoreStatus status;
	private String statusReason = "";
	private String website;
	private ImageSet logoImages = new ImageSet();
	private String emailAddress;
	private Address address;
	private GeoLocation geoLocation = new GeoLocation();
	private ZoneId timezone;
	private BigDecimal contributionRate;
	private Set<PhoneNumber> phoneNumbers = new HashSet<>();
	private Set<ExternalIdentifier> externalIdentifiers = new HashSet<>();
	private Set<TradeCategory> businessCategories = new HashSet<>();
	private Set<TradeCategory> storeCategories = new HashSet<>();
	private Set<String> businessKeywords = new HashSet<>();
	private Set<String> storeKeywords = new HashSet<>();
	private Set<TradingHours> tradingHours = new HashSet<>();

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(UUID businessId) {
		this.businessId = businessId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StoreStatus getStatus() {
		return this.status;
	}

	public void setStatus(StoreStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public ImageSet getLogoImages() {
		return this.logoImages;
	}

	public void setLogoImages(ImageSet logoImages) {
		this.logoImages = logoImages;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public GeoLocation getGeoLocation() {
		return this.geoLocation;
	}

	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}

	public ZoneId getTimezone() {
		return this.timezone;
	}

	public void setTimezone(ZoneId timezone) {
		this.timezone = timezone;
	}

	public BigDecimal getContributionRate() {
		return this.contributionRate;
	}

	public void setContributionRate(BigDecimal contributionRate) {
		this.contributionRate = contributionRate;
	}

	public Set<PhoneNumber> getPhoneNumbers() {
		return this.phoneNumbers;
	}

	public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public Set<ExternalIdentifier> getExternalIdentifiers() {
		return this.externalIdentifiers;
	}

	public void setExternalIdentifiers(Set<ExternalIdentifier> identifiers) {
		this.externalIdentifiers = identifiers;
	}

	public Set<TradeCategory> getBusinessCategories() {
		return this.businessCategories;
	}

	public void setBusinessCategories(Set<TradeCategory> businessCategories) {
		this.businessCategories = businessCategories;
	}

	public Set<TradeCategory> getStoreCategories() {
		return this.storeCategories;
	}

	public void setStoreCategories(Set<TradeCategory> storeCategories) {
		this.storeCategories = storeCategories;
	}

	public Set<String> getBusinessKeywords() {
		return this.businessKeywords;
	}

	public void setBusinessKeywords(Set<String> businessKeywords) {
		this.businessKeywords = businessKeywords;
	}

	public Set<String> getStoreKeywords() {
		return this.storeKeywords;
	}

	public void setStoreKeywords(Set<String> keywords) {
		this.storeKeywords = keywords;
	}

	public Set<TradingHours> getTradingHours() {
		return this.tradingHours;
	}

	public void setTradingHours(Set<TradingHours> tradingHours) {
		this.tradingHours = tradingHours;
	}

	public void addTradingHours(TradingHours tradingHours) {
		this.tradingHours.add(tradingHours);
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += hashValue(this.id);
		result += hashValue(this.businessId);
		result += hashValue(this.description);
		result += hashValue(this.emailAddress);
		result += hashValue(this.name);
		result += hashValue(this.status);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Store)) {
			return false;
		}

		Store other = (Store) obj;

		return EqualsUtils.areEqual(this.id, other.id)
				&& EqualsUtils.areEqual(this.businessId, other.businessId)
				&& EqualsUtils.areEqual(this.status, other.status)
				&& EqualsUtils.areEqual(this.statusReason, other.statusReason)
				&& EqualsUtils.areEqual(this.name, other.name)
				&& EqualsUtils.areEqual(this.description, other.description)
				&& EqualsUtils.areEqual(this.emailAddress, other.emailAddress)
				&& EqualsUtils.areEqual(this.website, other.website)
				&& EqualsUtils.areEqual(this.logoImages, other.logoImages)
				&& EqualsUtils.areEqual(this.address, other.address)
				&& EqualsUtils.areEqual(this.timezone, other.timezone)
				&& EqualsUtils.areEqual(this.storeKeywords, other.storeKeywords)
				&& EqualsUtils.areEqual(this.businessKeywords, other.businessKeywords)
				&& EqualsUtils.areEqual(this.externalIdentifiers, other.externalIdentifiers)
				&& EqualsUtils.areEqual(this.phoneNumbers, other.phoneNumbers)
				&& EqualsUtils.areEqual(this.businessCategories, other.businessCategories)
				&& EqualsUtils.areEqual(this.storeCategories, other.storeCategories)
				&& EqualsUtils.areEqual(this.tradingHours, other.tradingHours);
	}

}
