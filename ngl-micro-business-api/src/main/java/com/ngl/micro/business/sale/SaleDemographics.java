package com.ngl.micro.business.sale;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.time.LocalDate;

import com.ngl.middleware.util.Assert;

/**
 * {@link SaleDemographics}.
 *
 * @author Willy du Preez
 *
 */
public class SaleDemographics {

	private String gender;
	private LocalDate birthDate;

	public SaleDemographics() {
	}

	public SaleDemographics(String gender, LocalDate birthDate) {
		Assert.notNull(gender);

		this.gender = gender;
		this.birthDate = birthDate;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return this.gender;
	}

	public LocalDate getBirthDate() {
		return this.birthDate;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.gender);
		result += hashValue(this.birthDate);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Sale)) {
			return false;
		}

		SaleDemographics other = (SaleDemographics) obj;

		return areEqual(this.gender, other.gender)
				&& areEqual(this.birthDate, other.birthDate);
	}

}
