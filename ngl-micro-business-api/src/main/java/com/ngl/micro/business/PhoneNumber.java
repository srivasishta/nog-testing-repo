package com.ngl.micro.business;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.middleware.rest.api.Identifiable;

/**
 * A business or store phone number.
 */
public class PhoneNumber implements Identifiable<PhoneNumberType> {

	public static enum PhoneNumberType {
		TEL, FAX;
	}

	private PhoneNumberType type;
	private String countryCode;
    private String areaCode;
    private String number;
    private String extension;

	@Override
	public PhoneNumberType getId() {
		return type;
	}

    public PhoneNumberType getType() {
		return type;
	}

	public void setType(PhoneNumberType type) {
		this.type = type;
	}

	public String getCountryCode() {
		return countryCode;
	}

    public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

    public String getAreaCode() {
		return areaCode;
	}

    public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

    public String getNumber() {
		return number;
	}

    public void setNumber(String number) {
		this.number = number;
	}

    public String getExtension() {
		return extension;
	}

    public void setExtension(String extension) {
		this.extension = extension;
	}

    @Override
    public String toString() {
    	return String.format("%s: +%s %s %s ext: %s", type, countryCode, areaCode, number, extension);
    }

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.type);
		result += hashValue(this.countryCode);
		result += hashValue(this.areaCode);
		result += hashValue(this.number);
		result += hashValue(this.extension);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PhoneNumber)) {
			return false;
		}

		PhoneNumber other = (PhoneNumber) obj;

		return  areEqual(this.type, other.type) &&
				areEqual(this.countryCode, other.countryCode) &&
				areEqual(this.areaCode, other.areaCode) &&
				areEqual(this.number, other.number) &&
				areEqual(this.extension, other.extension);
	}

}
