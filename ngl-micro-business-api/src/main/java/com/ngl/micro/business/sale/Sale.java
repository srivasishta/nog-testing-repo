package com.ngl.micro.business.sale;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * Represents a sale made by a store.
 */
public class Sale implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-20T14:37:40.101Z */
	public static final class Fields {
		public static final String CONTRIBUTION_AMOUNT = "contributionAmount";
		public static final String DEMOGRAPHICS = "demographics";
		public static final String DEMOGRAPHICS_BIRTH_DATE = "demographics.birthDate";
		public static final String DEMOGRAPHICS_GENDER = "demographics.gender";
		public static final String ID = "id";
		public static final String SALE_AMOUNT = "saleAmount";
		public static final String SALE_DATE = "saleDate";
		public static final String STORE_ID = "storeId";
	}

	private UUID id;
	private UUID storeId;
	private ZonedDateTime saleDate;
	private BigDecimal saleAmount;
	private BigDecimal contributionAmount;
	private SaleDemographics demographics;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getStoreId() {
		return this.storeId;
	}

	public void setStoreId(UUID storeId) {
		this.storeId = storeId;
	}

	public ZonedDateTime getSaleDate() {
		return this.saleDate;
	}

	public void setSaleDate(ZonedDateTime saleDate) {
		this.saleDate = saleDate;
	}

	public BigDecimal getSaleAmount() {
		return this.saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public BigDecimal getContributionAmount() {
		return this.contributionAmount;
	}

	public void setContributionAmount(BigDecimal contributionAmount) {
		this.contributionAmount = contributionAmount;
	}

	public SaleDemographics getDemographics() {
		return this.demographics;
	}

	public void setDemographics(SaleDemographics demographics) {
		this.demographics = demographics;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Sale)) {
			return false;
		}

		Sale other = (Sale) obj;

		return areEqual(this.id, other.id)
				&& areEqual(this.storeId, other.storeId)
				&& areEqual(this.saleDate, other.saleDate)
				&& areEqual(this.saleAmount, other.saleAmount)
				&& areEqual(this.contributionAmount, other.contributionAmount)
				&& areEqual(this.demographics, other.demographics);
	}

	@Override
	public String toString() {
		return String.format("%s : %s", this.saleDate, this.saleAmount);
	}

}
