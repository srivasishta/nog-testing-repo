package com.ngl.micro.business.store;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.time.ZonedDateTime;

import com.ngl.middleware.rest.api.Identifiable;

public class ExternalIdentifier implements Identifiable<Long> {

	public static final String ID = "id";
	public static final String IDENTIFIER = "key";
	public static final String START_DATE = "startDate";
	public static final String END_DATE = "endDate";

	private Long id;
	private String key;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;

	public ExternalIdentifier() {
	}

	public ExternalIdentifier(ExternalIdentifier id) {
		this.id = id.id;
		this.key = id.key;
		this.startDate = id.startDate;
		this.endDate = id.endDate;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		int result = 31;
		result += hashValue(this.id);
		result += hashValue(this.key);
		result += hashValue(this.startDate);
		result += hashValue(this.endDate);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ExternalIdentifier)) {
			return false;
		}

		ExternalIdentifier other = (ExternalIdentifier) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.key, other.key) &&
				areEqual(this.startDate, other.startDate) &&
				areEqual(this.endDate, other.endDate);
	}

	@Override
	public String toString() {
		return String.format("id:%s,key:%s", id, key);
	}
}