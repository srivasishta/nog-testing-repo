package com.ngl.micro.business.store;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.time.DayOfWeek;
import java.time.LocalTime;

import com.ngl.middleware.rest.api.Identifiable;

public class TradingHours implements Identifiable<Long>{

	private Long id;
	private DayOfWeek startDay;
	private DayOfWeek endDay;
	private LocalTime openTime;
	private LocalTime closeTime;

	@Override
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DayOfWeek getStartDay() {
		return startDay;
	}

	public void setStartDay(DayOfWeek startDay) {
		this.startDay = startDay;
	}

	public DayOfWeek getEndDay() {
		return endDay;
	}

	public void setEndDay(DayOfWeek endDay) {
		this.endDay = endDay;
	}

	public LocalTime getOpenTime() {
		return openTime;
	}

	public void setOpenTime(LocalTime openTime) {
		this.openTime = openTime;
	}

	public LocalTime getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(LocalTime closeTime) {
		this.closeTime = closeTime;
	}

	@Override
	public String toString() {
		return String.format("%s:%s-%s %s-%s", id, startDay, endDay, openTime, closeTime);
	}

	@Override
	public int hashCode() {
		int result = 31;
		result += hashValue(this.id);
		result += hashValue(this.startDay);
		result += hashValue(this.endDay);
		result += hashValue(this.openTime);
		result += hashValue(this.closeTime);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TradingHours)) {
			return false;
		}

		TradingHours other = (TradingHours) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.startDay, other.startDay) &&
				areEqual(this.endDay, other.endDay) &&
				areEqual(this.openTime, other.openTime) &&
				areEqual(this.closeTime, other.closeTime);
	}
}
