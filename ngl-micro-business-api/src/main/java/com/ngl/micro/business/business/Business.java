package com.ngl.micro.business.business;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.TradeCategory;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.EqualsUtils;

/**
 * A {@link Business} represents a trading merchant with one or multiple stores.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class Business implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-11-20T14:37:39.941Z */
	public static final class Fields {
		public static final String CATEGORIES = "categories";
		public static final String DESCRIPTION = "description";
		public static final String EMAIL_ADDRESS = "emailAddress";
		public static final String ID = "id";
		public static final String KEYWORDS = "keywords";
		public static final String LOGO_IMAGES = "logoImages";
		public static final String LOGO_IMAGES_LARGE = "logoImages.large";
		public static final String LOGO_IMAGES_MEDIUM = "logoImages.medium";
		public static final String LOGO_IMAGES_ORIGINAL = "logoImages.original";
		public static final String LOGO_IMAGES_THUMBNAIL = "logoImages.thumbnail";
		public static final String NAME = "name";
		public static final String PARTNER_ID = "partnerId";
		public static final String PHONE_NUMBERS = "phoneNumbers";
		public static final String PHONE_NUMBERS_AREA_CODE = "phoneNumbers.areaCode";
		public static final String PHONE_NUMBERS_COUNTRY_CODE = "phoneNumbers.countryCode";
		public static final String PHONE_NUMBERS_EXTENSION = "phoneNumbers.extension";
		public static final String PHONE_NUMBERS_ID = "phoneNumbers.id";
		public static final String PHONE_NUMBERS_NUMBER = "phoneNumbers.number";
		public static final String PHONE_NUMBERS_TYPE = "phoneNumbers.type";
		public static final String STATUS = "status";
		public static final String STATUS_REASON = "statusReason";
		public static final String WEBSITE = "website";
	}

	public enum BusinessStatus {
		ACTIVE,
		INACTIVE;
	}

	private UUID id;
	private UUID partnerId;
	private BusinessStatus status;
	private String statusReason = "";
	private String name;
	private String description;
	private String website;
	private ImageSet logoImages = new ImageSet();
	private String emailAddress;
	private Set<PhoneNumber> phoneNumbers = new HashSet<>();
	private Set<String> keywords = new HashSet<>();
	private Set<TradeCategory> categories = new HashSet<>();

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public BusinessStatus getStatus() {
		return this.status;
	}

	public void setStatus(BusinessStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ImageSet getLogoImages() {
		return this.logoImages;
	}

	public void setLogoImages(ImageSet logoImages) {
		this.logoImages = logoImages;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Set<PhoneNumber> getPhoneNumbers() {
		return this.phoneNumbers;
	}

	public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public Set<String> getKeywords() {
		return this.keywords;
	}

	public void setKeywords(Set<String> keywords) {
		this.keywords = keywords;
	}

	public Set<TradeCategory> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<TradeCategory> tradeCategories) {
		this.categories = tradeCategories;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Business)) {
			return false;
		}

		Business other = (Business) obj;

		return EqualsUtils.areEqual(this.id, other.id)
				&& EqualsUtils.areEqual(this.partnerId, other.partnerId)
				&& EqualsUtils.areEqual(this.status, other.status)
				&& EqualsUtils.areEqual(this.statusReason, other.statusReason)
				&& EqualsUtils.areEqual(this.name, other.name)
				&& EqualsUtils.areEqual(this.description, other.description)
				&& EqualsUtils.areEqual(this.emailAddress, other.emailAddress)
				&& EqualsUtils.areEqual(this.website, other.website)
				&& EqualsUtils.areEqual(this.logoImages, other.logoImages)
				&& EqualsUtils.areEqual(this.phoneNumbers, other.phoneNumbers)
				&& EqualsUtils.areEqual(this.keywords, other.keywords)
				&& EqualsUtils.areEqual(this.categories, other.categories);
	}

}
