package com.ngl.micro.business.sale;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;

/**
 * Represents a summary of sales made by a business store over all time.
 */
public class SaleSummary {

	/** Generated. Do not modify. 2015-11-20T14:37:40.121Z */
	public static final class Fields {
		public static final String TOTAL_AMOUNT = "totalAmount";
		public static final String TOTAL_CONTRIBUTED = "totalContributed";
		public static final String TOTAL_SALES = "totalSales";
	}

	private Integer totalSales;
	private BigDecimal totalAmount;
	private BigDecimal totalContributed;
	private CustomerSummary customerSummary;

	public Integer getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Integer totalSales) {
		this.totalSales = totalSales;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalContributed() {
		return totalContributed;
	}

	public void setTotalContributed(BigDecimal totalContributed) {
		this.totalContributed = totalContributed;
	}

	public CustomerSummary getCustomerSummary() {
		return customerSummary;
	}

	public void setCustomerSummary(CustomerSummary customerSummary) {
		this.customerSummary = customerSummary;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.totalSales);
		result += hashValue(this.totalAmount);
		result += hashValue(this.totalContributed);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SaleSummary)) {
			return false;
		}

		SaleSummary other = (SaleSummary) obj;

		return  areEqual(this.totalSales, other.totalSales) &&
				areEqual(this.totalAmount, other.totalAmount) &&
				areEqual(this.totalContributed, other.totalContributed) &&
				areEqual(this.customerSummary, other.customerSummary);
	}

}
