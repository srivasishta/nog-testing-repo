package com.ngl.micro.business.sale;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

/**
 * Customer demographic summary totals.
 *
 * @author Paco Mendes
 */
public class CustomerSummary {

	private Integer totalCustomers;
	private Integer totalMale;
	private Integer totalFemale;
	private Integer totalUnspecified;
	private Integer totalReturning;

	public Integer getTotalCustomers() {
		return totalCustomers;
	}

	public void setTotalCustomers(Integer totalCustomers) {
		this.totalCustomers = totalCustomers;
	}

	public Integer getTotalMale() {
		return totalMale;
	}

	public void setTotalMale(Integer totalMale) {
		this.totalMale = totalMale;
	}

	public Integer getTotalFemale() {
		return totalFemale;
	}

	public void setTotalFemale(Integer totalFemale) {
		this.totalFemale = totalFemale;
	}

	public Integer getTotalUnspecified() {
		return totalUnspecified;
	}

	public void setTotalUnspecified(Integer totalUnspecified) {
		this.totalUnspecified = totalUnspecified;
	}

	public Integer getTotalReturning() {
		return totalReturning;
	}

	public void setTotalReturning(Integer totalReturning) {
		this.totalReturning = totalReturning;
	}

	@Override
	public int hashCode() {
		int result = 23;
		result += hashValue(this.totalCustomers);
		result += hashValue(this.totalReturning);
		result += hashValue(this.totalMale);
		result += hashValue(this.totalFemale);
		result += hashValue(this.totalUnspecified);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CustomerSummary)) {
			return false;
		}

		CustomerSummary other = (CustomerSummary) obj;

		return  areEqual(this.totalCustomers, other.totalCustomers) &&
				areEqual(this.totalReturning, other.totalReturning) &&
				areEqual(this.totalMale, other.totalMale) &&
				areEqual(this.totalFemale, other.totalFemale) &&
				areEqual(this.totalUnspecified, other.totalUnspecified);
	}
}
