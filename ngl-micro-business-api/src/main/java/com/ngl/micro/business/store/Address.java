package com.ngl.micro.business.store;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * A store address
 *
 * @author Paco Mendes
 */
public class Address implements Identifiable<Long>{

	private Long id;
	private String streetAddress;
	private String locality;
	private String region;
	private String postalCode;
	private Country country;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public int hashCode() {
		int result = 7;
		result += hashValue(this.id);
		result += hashValue(this.streetAddress);
		result += hashValue(this.locality);
		result += hashValue(this.region);
		result += hashValue(this.postalCode);
		result += hashValue(this.country);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Address)) {
			return false;
		}

		Address other = (Address) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.streetAddress, other.streetAddress) &&
				areEqual(this.locality, other.locality) &&
				areEqual(this.region, other.region) &&
				areEqual(this.postalCode, other.postalCode) &&
				areEqual(this.country, other.country);
	}
}