CREATE TABLE `charity_choice` (

    -- Columns
    `id`                   BIGINT(20) NOT NULL AUTO_INCREMENT,
    `membership_id`        BIGINT(20) NOT NULL,
    `start_date`           DATETIME NOT NULL,
    `end_date`             DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`membership_id`) REFERENCES `membership` (`id`) ON DELETE CASCADE

) ENGINE = InnoDB DEFAULT CHARSET = utf8;