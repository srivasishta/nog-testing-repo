CREATE TABLE `contribution_recorded_event` (

    -- Columns
    `id`                             BIGINT(20) NOT NULL,
    `created_date`                   DATETIME NOT NULL,
    `contribution_document_id`       BINARY(16) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_cre_created_date` ON `contribution_recorded_event` (`created_date`);
