CREATE TABLE `charity_allocation` (

    -- Columns
    `id`                   BIGINT(20) NOT NULL AUTO_INCREMENT,
    `charity_choice_id`    BIGINT(20) NOT NULL,
    `charity_id`           BINARY(16) NOT NULL,
    `allocation`           INTEGER NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`charity_choice_id`) REFERENCES `charity_choice` (`id`) ON DELETE CASCADE

) ENGINE = InnoDB DEFAULT CHARSET = utf8;