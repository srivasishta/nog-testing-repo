CREATE TABLE `membership_revision` (

	-- Columns
	`id`                    BIGINT(20) NOT NULL AUTO_INCREMENT,
	`resource_id`           BINARY(16) NOT NULL,
	`revised_resource_id`   BINARY(16) NOT NULL,
	`revised_date`          DATETIME NOT NULL,
	`revised_by_client`     BINARY(16) NOT NULL,
	`revised_by_partner`    BINARY(16) NOT NULL,
	`revised_by_user`       VARCHAR(50),
	`comment`               VARCHAR(255),
	`applied_patch`         TEXT NOT NULL,
	`revert_patch`          TEXT NOT NULL,

	-- Constraints
	PRIMARY KEY (`id`),
	UNIQUE KEY (`resource_id`),
	UNIQUE KEY (`revised_date`, `revised_resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_membership_revision__revised_resource_id` ON `membership_revision` (`revised_resource_id`);

