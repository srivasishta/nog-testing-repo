CREATE TABLE `membership_demographics` (

    -- Columns
    `id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `membership_id`     BIGINT(20) NOT NULL,
    `birthdate`         DATE DEFAULT NULL,
    `gender`            VARCHAR(12) DEFAULT 'UNSPECIFIED',
    `country`           VARCHAR(2) NOT NULL,
    `region`            VARCHAR(50) NOT NULL,
    `lat`               DOUBLE,
    `lon`               DOUBLE,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY(`membership_id`),
    FOREIGN KEY (`membership_id`) REFERENCES `membership` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
