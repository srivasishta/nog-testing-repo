CREATE TABLE `purchase` (

    -- Columns
    `id`                            BIGINT(20) NOT NULL AUTO_INCREMENT,
    `contribution_document_id`      BINARY(16) NOT NULL,
    `purchase_date`                 DATETIME NOT NULL,
    `purchase_amount`               DECIMAL(15,5) NOT NULL,
    `contribution_amount`           DECIMAL(15,5) NOT NULL,
    `membership_id`                 BINARY(16) NOT NULL,
    `store_id`                      BINARY(16) NOT NULL,
    `business_id`                   BINARY(16) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`contribution_document_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_purchase__purchase_date` ON `purchase` (`purchase_date`);
CREATE INDEX `idx_purchase__membership_id` ON `purchase` (`membership_id`);
CREATE INDEX `idx_purchase__store_id` ON `purchase` (`store_id`);
CREATE INDEX `idx_purchase__business_id` ON `purchase` (`business_id`);
