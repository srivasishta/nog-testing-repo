CREATE TABLE `membership` (

    -- Columns
    `id`             BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`    BINARY(16) NOT NULL,
    `partner_id`     BINARY(16) NOT NULL,
    `external_id`    VARCHAR(100) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`),
    UNIQUE KEY (`partner_id`, `external_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
