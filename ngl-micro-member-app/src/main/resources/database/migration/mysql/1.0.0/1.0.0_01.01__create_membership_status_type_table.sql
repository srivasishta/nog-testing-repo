CREATE TABLE `membership_status_type` (

    -- Columns
    `id`        BIGINT(20) NOT NULL,
    `value`     VARCHAR(20) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE (`value`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
