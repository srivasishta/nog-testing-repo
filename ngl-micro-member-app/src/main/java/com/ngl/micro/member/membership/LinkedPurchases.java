package com.ngl.micro.member.membership;

import static com.ngl.micro.member.dal.generated.jooq.Tables.PURCHASE;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.sum;

import java.math.BigDecimal;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.Record3;

import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JPurchaseRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQueries;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;

public class LinkedPurchases {

	private static final String MEMBERSHIP_ID_FILTER = "membershipId";

	private DAL support;
	private FieldMap<String> fieldMap;
	private JooqQuery query;

	public LinkedPurchases(Configuration config) {
		this.support = new DAL(config);

		this.fieldMap = new FieldMap<String>()
				.bind(Purchase.Fields.ID, new MappedField(PURCHASE.CONTRIBUTION_DOCUMENT_ID).searchable(true))
				.bind(Purchase.Fields.PURCHASE_DATE, new MappedField(PURCHASE.PURCHASE_DATE).searchable(true).sortable(true))
				.bind(Purchase.Fields.PURCHASE_AMOUNT, PURCHASE.PURCHASE_AMOUNT)
				.bind(Purchase.Fields.CONTRIBUTION_AMOUNT, PURCHASE.CONTRIBUTION_AMOUNT)
				.bind(Purchase.Fields.STORE_ID, new MappedField(PURCHASE.STORE_ID).searchable(true))
				.bind(Purchase.Fields.BUSINESS_ID, new MappedField(PURCHASE.BUSINESS_ID).searchable(true))
				.bind(MEMBERSHIP_ID_FILTER, new MappedField(PURCHASE.MEMBERSHIP_ID).searchable(true));

		this.query = JooqQuery.builder(this.support, this.fieldMap)
				.from(PURCHASE)
				.build();
	}

	public Purchase addPurchase(UUID membershipId, Purchase purchase) {
		JPurchaseRecord record = this.support.sql().newRecord(PURCHASE);
		record.setContributionDocumentId(purchase.getId());
		record.setPurchaseDate(purchase.getPurchaseDate());
		record.setPurchaseAmount(purchase.getPurchaseAmount());
		record.setContributionAmount(purchase.getContributionAmount());
		record.setMembershipId(membershipId);
		record.setStoreId(purchase.getStoreId());
		record.setBusinessId(purchase.getBusinessId());
		record.store();
		return this.getByPurchaseId(record.getId());
	}

	private Purchase getByPurchaseId(Long purchaseId) {
		Record record = this.query.getRecordWhere(PURCHASE.ID.eq(purchaseId));
		if (record == null) {
			return null;
		} else {
			return this.map(record);
		}
	}

	private Purchase map(Record record) {
		Purchase purchase = new Purchase();
		purchase.setId(record.getValue(PURCHASE.CONTRIBUTION_DOCUMENT_ID));
		purchase.setPurchaseDate(record.getValue(PURCHASE.PURCHASE_DATE));
		purchase.setPurchaseAmount(record.getValue(PURCHASE.PURCHASE_AMOUNT));
		purchase.setContributionAmount(record.getValue(PURCHASE.CONTRIBUTION_AMOUNT));
		purchase.setStoreId(record.getValue(PURCHASE.STORE_ID));
		return purchase;
	}

	public Page<Purchase> getByMembershipId(UUID membershipId, PageRequest pageRequest) {
		pageRequest.filter(SearchQueries.and(MEMBERSHIP_ID_FILTER).eq(membershipId));
		return this.query.getPage(pageRequest, this::map);
	}

	public PurchaseSummary getPurchaseSummaryByMembershipId(UUID membershipId, SearchQuery query) {
		org.jooq.Condition condition = JooqQueries.mapSearch(this.fieldMap, query)
				.map(c -> c.and(PURCHASE.MEMBERSHIP_ID.eq(membershipId)))
				.orElse(PURCHASE.MEMBERSHIP_ID.eq(membershipId));

		Record3<Integer, BigDecimal, BigDecimal> result = this.support.sql().select(
				count(),
				sum(PURCHASE.PURCHASE_AMOUNT),
				sum(PURCHASE.CONTRIBUTION_AMOUNT))
				.from(PURCHASE)
				.where(condition)
				.fetchOne();

		PurchaseSummary summary = new PurchaseSummary();
		summary.setTotalPurchases(result.value1());
		summary.setTotalAmount(result.value2() == null ? new BigDecimal(0) : result.value2());
		summary.setTotalContributed(result.value3() == null ? new BigDecimal(0) : result.value3());
		return summary;
	}

}
