package com.ngl.micro.member.management;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ngl.micro.member.Membership;
import com.ngl.micro.member.membership.MembershipRepository;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.micro.shared.contracts.member.MembershipLookupService;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.annotation.PermitAll;

/**
 * Management endpoint implementation.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Path("management/membership-lookup")
public class JsonMembershipLookupService implements MembershipLookupService, RestEndpoint {

	private MembershipRepository memberships;

	public JsonMembershipLookupService(MembershipRepository membershipRepository) {
		this.memberships = membershipRepository;
	}

	@Override
	@POST
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MembershipLookupResponse lookup(MembershipLookupRequest request) {
		Membership membership = this.memberships.getByExternalIdAt(
				request.getPartnerId(),
				request.getMembershipRefId(),
				request.getTransactionDate());
		return membership == null ? this.unmatched() : this.matched(membership);
	}

	private MembershipLookupResponse unmatched() {
		MembershipLookupResponse response = new MembershipLookupResponse();
		response.setMatched(false);
		return response;
	}

	private MembershipLookupResponse matched(Membership membership) {
		Gender gender = Gender.forEnum(membership.getDemographics().getGender());
		LocalDate birthdate = membership.getDemographics().getBirthdate();

		Set<MembershipCharityChoice> choices = membership.getCharityChoices().stream()
				.map(c -> new MembershipCharityChoice(c.getCharityId(), c.getAllocation()))
				.collect(Collectors.toSet());

		MembershipLookupResponse response = new MembershipLookupResponse();
		response.setMembershipId(membership.getId());
		response.setMatched(true);
		response.setStatus(ResourceStatus.forEnum(membership.getStatus()));
		response.setDemographics(new MembershipDemographics(gender, birthdate));
		response.setCharityChoice(choices);
		return response;
	}

}
