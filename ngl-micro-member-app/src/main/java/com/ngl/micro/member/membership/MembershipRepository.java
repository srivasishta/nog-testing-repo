package com.ngl.micro.member.membership;

import static com.ngl.micro.member.dal.generated.jooq.Tables.CHARITY_ALLOCATION;
import static com.ngl.micro.member.dal.generated.jooq.Tables.CHARITY_CHOICE;
import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP;
import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP_DEMOGRAPHICS;
import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP_STATUS_HISTORY;
import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP_STATUS_TYPE;

import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Country;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.Demographics.Gender;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JMembershipRecord;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Time;

/**
 * Membership repository.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class MembershipRepository implements PagingRepository<Membership, UUID> {

	private static final String PRIMARY_KEY = "pk";

	private DAL dal;
	private TableOnConditionStep<?> tables;
	private JooqQuery query;
	private FieldMap<String> fieldMap;
	private FieldMap<String> linkedFieldMap;

	private LinkedCharityChoices linkedCharityChoice;
	private LinkedDemographics linkedDemographics;
	private LinkedPurchases linkedPurchases;
	private StatusHistoryRepository statusHistory;

	public MembershipRepository(Configuration config) {
		this.dal= new DAL(config);

		this.linkedCharityChoice = new LinkedCharityChoices(config);
		this.linkedPurchases = new LinkedPurchases(config);
		this.linkedDemographics = new LinkedDemographics(config);
		this.statusHistory = new StatusHistoryRepository(config);

		this.fieldMap = new FieldMap<String>()
				.bind(Membership.Fields.ID, new MappedField(MEMBERSHIP.RESOURCE_ID).searchable(true))
				.bind(Membership.Fields.PARTNER_ID, new MappedField(MEMBERSHIP.PARTNER_ID).searchable(true))
				.bind(Membership.Fields.EXTERNAL_ID, new MappedField(MEMBERSHIP.EXTERNAL_ID).searchable(true))
				.bind(Membership.Fields.STATUS, new MappedField(MEMBERSHIP_STATUS_TYPE.VALUE).searchable(true))
				.bind(Membership.Fields.STATUS_REASON, MEMBERSHIP_STATUS_HISTORY.STATUS_REASON)
				.bind(Membership.Fields.DEMOGRAPHICS_BIRTHDATE, new MappedField(MEMBERSHIP_DEMOGRAPHICS.BIRTHDATE).searchable(true))
				.bind(Membership.Fields.DEMOGRAPHICS_GENDER, new MappedField(MEMBERSHIP_DEMOGRAPHICS.GENDER).searchable(true))
				.bind(Membership.Fields.DEMOGRAPHICS_COUNTRY, new MappedField(MEMBERSHIP_DEMOGRAPHICS.COUNTRY).searchable(true))
				.bind(Membership.Fields.DEMOGRAPHICS_REGION, new MappedField(MEMBERSHIP_DEMOGRAPHICS.REGION).searchable(true))
				.bind(Membership.Fields.DEMOGRAPHICS_GEO_LOCATION_LAT, MEMBERSHIP_DEMOGRAPHICS.LAT)
				.bind(Membership.Fields.DEMOGRAPHICS_GEO_LOCATION_LON, MEMBERSHIP_DEMOGRAPHICS.LON)
				.bind(PRIMARY_KEY, new MappedField(MEMBERSHIP.ID));

		this.linkedFieldMap = new FieldMap<String>()
				.bindAll(this.fieldMap)
				.bind(Membership.Fields.CHARITY_CHOICES_CHARITY_ID, new MappedField(CHARITY_ALLOCATION.CHARITY_ID).searchable(true));

		this.tables = MEMBERSHIP
				.join(MEMBERSHIP_DEMOGRAPHICS).on(MEMBERSHIP.ID.eq(MEMBERSHIP_DEMOGRAPHICS.MEMBERSHIP_ID))
				.join(MEMBERSHIP_STATUS_HISTORY).on(MEMBERSHIP.ID.eq(MEMBERSHIP_STATUS_HISTORY.MEMBERSHIP_ID))
				.join(MEMBERSHIP_STATUS_TYPE).on(MEMBERSHIP_STATUS_TYPE.ID.eq(MEMBERSHIP_STATUS_HISTORY.STATUS_ID))
				.and(MEMBERSHIP_STATUS_HISTORY.END_DATE.isNull());

		this.query = JooqQuery.builder(this.dal, this.fieldMap)
				.from(this.tables)
				.build();
	}

	@Override
	public Membership add(Membership membership) {
		JMembershipRecord record = this.dal.sql().newRecord(MEMBERSHIP);
		record.setPartnerId(membership.getPartnerId());
		record.setResourceId(membership.getId());
		record.setExternalId(membership.getExternalId().trim());
		record.store();

		Long membershipId = record.getId();
		ZonedDateTime lastUpdated = Time.utcNow();

		this.statusHistory.setMembershipStatus(membershipId, membership.getStatus(), membership.getStatusReason(), lastUpdated);
		this.linkedDemographics.setMembershipDemographics(membershipId, membership.getDemographics());
		this.linkedCharityChoice.setMembershipCharityChoice(membershipId, membership.getCharityChoices(), lastUpdated);

		return this.get(membership.getId());
	}

	@Override
	public Membership get(UUID id) {
		Record record = this.query.getRecordWhere(MEMBERSHIP.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			Membership membership = this.map(record);
			ZonedDateTime lookupDate = Time.utcNow();
			membership.setCharityChoices(this.linkedCharityChoice.getByMembershipIdAt(record.getValue(MEMBERSHIP.ID), lookupDate));
			return membership;
		}
	}

	public Membership getByExternalIdAt(UUID partnerId, String externalId, ZonedDateTime date) {

		Record record = this.query.getRecordWhere(
				MEMBERSHIP.EXTERNAL_ID.eq(externalId)
				.and(MEMBERSHIP.PARTNER_ID.eq(partnerId)));
		if (record == null) {
			return null;
		} else {
			Membership membership = this.map(record);
			membership.setCharityChoices(this.linkedCharityChoice.getByMembershipIdAt(record.getValue(MEMBERSHIP.ID), date));

			MembershipStatusRecord status = this.statusHistory.getByMembershipIdAt(record.getValue(MEMBERSHIP.ID), date);
			membership.setStatus(status.getStatus());
			membership.setStatusReason(status.getStatusReason());

			return membership;
		}
	}

	@Override
	public Page<Membership> get(PageRequest pageRequest) {
		List<String> props = pageRequest.getSearchQuery().getConditions()
				.stream()
				.map(c -> c.getProperty())
				.filter(p -> p.equals(Membership.Fields.CHARITY_CHOICES_CHARITY_ID))
				.collect(Collectors.toList());

		Page<Membership> resultPage;
		if (props.isEmpty()) {
			resultPage = this.query.getPage(pageRequest, this::map);
		}
		else {
			ZonedDateTime nowDate = ZonedDateTime.now();
			TableOnConditionStep<?> from = this.tables
				.leftOuterJoin(CHARITY_CHOICE).on(
						MEMBERSHIP.ID.eq(CHARITY_CHOICE.MEMBERSHIP_ID)
						.and(CHARITY_CHOICE.START_DATE.le(nowDate)
						.and(CHARITY_CHOICE.END_DATE.isNull().or(CHARITY_CHOICE.END_DATE.ge(nowDate)))))
				.leftOuterJoin(CHARITY_ALLOCATION).on(CHARITY_ALLOCATION.CHARITY_CHOICE_ID.eq(CHARITY_CHOICE.ID));

			resultPage = this.query.getPage(
					pageRequest,
					() -> this.linkedFieldMap,
					() -> from,
					q -> {
						q.addGroupBy(MEMBERSHIP.ID);
					},
					this::map);
		}

		this.injectCharityChoices(resultPage);

		return resultPage;
	}

	private void injectCharityChoices(Page<Membership> membershipPage) {
			Map<UUID, Set<CharityChoice>> resultPageChoices = this.linkedCharityChoice.list(membershipPage);
			membershipPage.getItems()
				.stream()
				.forEach(c -> { c.setCharityChoices(resultPageChoices.get(c.getId())); });
	}

	private Membership map(Record record) {
		Membership membership = new Membership();
		membership.setId(record.getValue(MEMBERSHIP.RESOURCE_ID));
		membership.setPartnerId(record.getValue(MEMBERSHIP.PARTNER_ID));
		membership.setExternalId(record.getValue(MEMBERSHIP.EXTERNAL_ID));
		membership.setStatus(MembershipStatus.valueOf(record.getValue(MEMBERSHIP_STATUS_TYPE.VALUE)));
		membership.setStatusReason(record.getValue(MEMBERSHIP_STATUS_HISTORY.STATUS_REASON));

		Gender gender = Gender.valueOf(record.getValue(MEMBERSHIP_DEMOGRAPHICS.GENDER));
		Date birthdate = record.getValue(MEMBERSHIP_DEMOGRAPHICS.BIRTHDATE);
		Demographics demographics = new Demographics();
		demographics.setGender(gender);
		demographics.setBirthdate(birthdate == null ? null : birthdate.toLocalDate());
		demographics.setCountry(Country.valueOf(record.getValue(MEMBERSHIP_DEMOGRAPHICS.COUNTRY)));
		demographics.setRegion(record.getValue(MEMBERSHIP_DEMOGRAPHICS.REGION));
		Double lat = record.getValue(MEMBERSHIP_DEMOGRAPHICS.LAT);
		Double lon = record.getValue(MEMBERSHIP_DEMOGRAPHICS.LON);
		demographics.setGeoLocation(GeoLocation.of(lat, lon));

		membership.setDemographics(demographics);

		return membership;
	}

	@Override
	public void update(Membership membership) {
		ZonedDateTime lastUpdated = Time.utcNow();

		JMembershipRecord record = this.dal.sql().fetchOne(MEMBERSHIP, MEMBERSHIP.RESOURCE_ID.eq(membership.getId()));
		record.setExternalId(membership.getExternalId().trim());
		record.store();

		Long membershipId = record.getValue(MEMBERSHIP.ID);

		this.linkedDemographics.setMembershipDemographics(membershipId, membership.getDemographics());
		this.linkedCharityChoice.setMembershipCharityChoice(membershipId, membership.getCharityChoices(), lastUpdated);
		this.statusHistory.setMembershipStatus(membershipId, membership.getStatus(), membership.getStatusReason(), lastUpdated);
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException("Delete operations not allowed");
	}

	@Override
	public long size() {
		return this.query.size();
	}

	public Purchase addPurchase(UUID partnerId, UUID membershipId, Purchase purchase) {
		Assert.notNull(partnerId);
		Assert.notNull(membershipId);
		Assert.notNull(purchase);

		UUID actual = this.dal.sql()
				.select(MEMBERSHIP.PARTNER_ID)
				.from(MEMBERSHIP)
				.where(MEMBERSHIP.RESOURCE_ID.eq(membershipId))
				.fetchOne().value1();

		Assert.state(partnerId.equals(actual), "Membership [%s] does not belong to partner [%s]", membershipId, partnerId);

		return this.linkedPurchases.addPurchase(membershipId, purchase);
	}

	public Page<Purchase> getPurchases(UUID membershipId, PageRequest page) {
		return this.linkedPurchases.getByMembershipId(membershipId, page);
	}

	public PurchaseSummary getPurchaseSummary(UUID membershipId, SearchQuery query) {
		return this.linkedPurchases.getPurchaseSummaryByMembershipId(membershipId, query);
	}

}
