package com.ngl.micro.member.membership.constraint;

import org.hibernate.validator.cfg.ConstraintDef;

public class CharityChoicesDef extends ConstraintDef<CharityChoicesDef, ValidCharityChoices> {

	public CharityChoicesDef() {
		super(ValidCharityChoices.class);
	}

}
