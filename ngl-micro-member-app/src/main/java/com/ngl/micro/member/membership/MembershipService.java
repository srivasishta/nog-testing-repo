package com.ngl.micro.member.membership;

import static com.ngl.middleware.rest.api.error.ValidationErrorCode.ALREADY_EXISTS;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.revision.VersionControlService;
import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ValidationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The membership service.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Transactional(rollbackOn = Exception.class)
public class MembershipService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MembershipService.class);

	private MembershipRepository memberships;
	private MembershipValidator validator;
	private Merge merge;
	private VersionControlService<Membership, UUID> membershipVc;

	public MembershipService(MembershipRepository memberships,
			MembershipValidator validator,
			VersionControlService<Membership, UUID> membershipVc) {
		this.memberships = memberships;
		this.validator = validator;
		this.merge = new Merge();
		this.membershipVc = membershipVc;
	}

	public Membership createMembership(Membership membership) {
		ZonedDateTime createdDate = Time.utcNow();
		UUID partnerId = Subjects.getSubject();
		// If it is not a system account, set the partner ID to
		// that in the token. If it is a system account and no
		// ID is specified, then set it to the system account's ID.
		// Else if the system is creating the membership on behalf
		// of a partner, leave the ID unchanged.
		if(!ApiPolicy.isSystemAccount(partnerId) || membership.getPartnerId() == null) {
			membership.setPartnerId(partnerId);
		}
		this.validator.validate(membership);
		this.validateUniqueReferenceId(membership);
		membership.setId(UUIDS.type4Uuid());

		Membership created = this.memberships.add(membership);

		this.membershipVc.revise(
				createdDate,
				new Revision(),
				new Membership(),
				created,
				Subjects.getClientId(),
				Subjects.getSubject());

		return created;
	}

	public Membership getMembership(UUID id) {
		UUID partnerId = Subjects.getSubject();
		Membership membership = this.memberships.get(id);
		if (membership == null || !ApiPolicy.isAccessibleByPartner(partnerId, membership.getPartnerId())) {
			throw ApplicationError.resourceNotFound()
					.setTitle("Membership Not Found")
					.setDetail("No membership found for the ID: " + id)
					.asException();
		}
		return membership;
	}

	public Page<Membership> getMemberships(PageRequest pageRequest) {
		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			pageRequest.filter(SearchQueries.and(Membership.Fields.PARTNER_ID).eq(partnerId));
		}
		return this.memberships.get(pageRequest);
	}

	public Membership updateMembership(UUID id, Patch patch) {
		ZonedDateTime updatedDate = Time.utcNow();
		Membership base = this.getMembership(id);
		Membership working = this.getMembership(id);
		UUID partnerId = working.getPartnerId();
		String externalId = working.getExternalId();
		this.merge.merge(patch, working);
		// Read only fields
		working.setPartnerId(partnerId);
		working.setId(id);
		this.validator.validate(working);
		if (!working.getExternalId().equals(externalId)) {
			this.validateUniqueReferenceId(working);
		}
		this.memberships.update(working);
		Membership updated = this.getMembership(id);

		this.membershipVc.revise(
				updatedDate,
				new Revision(),
				base,
				updated,
				Subjects.getClientId(),
				Subjects.getSubject());

		return updated;
	}

	private void validateUniqueReferenceId(Membership membership) {
		PageRequest request = PageRequest.with(0, 1).search(SearchQueries
				.q(Membership.Fields.PARTNER_ID).eq(membership.getPartnerId())
				.and(Membership.Fields.EXTERNAL_ID).eq(membership.getExternalId()).getSearchQuery())
				.build();

		Page<Membership> partnerIdentifiers = this.memberships.get(request);
		if (partnerIdentifiers.hasItems()) {
			throw ApplicationError.validationError()
					.setTitle("Duplicate partner identifier.")
					.setDetail("A matching identifier has already been allocated for this partner.")
					.addValidationErrors(
							new ValidationError(Membership.Fields.EXTERNAL_ID, ALREADY_EXISTS, "Exists for partner."))
					.asException();
		}
	}

	public void recordPurchase(ContributionDocument document) {
		Purchase purchase = new Purchase();
		purchase.setBusinessId(document.getBusinessId());
		purchase.setContributionAmount(document.getContributionAmount());
		purchase.setId(document.getId());
		purchase.setPurchaseAmount(document.getTransactionAmount());
		purchase.setPurchaseDate(document.getTransactionDate());
		purchase.setStoreId(document.getBusinessStoreId());

		this.memberships.addPurchase(document.getPartnerId(), document.getMembershipId(), purchase);
	}

	public Page<Purchase> getPurchases(UUID membershipId, PageRequest pageRequest) {
		this.getMembership(membershipId);
		return this.memberships.getPurchases(membershipId, pageRequest);
	}

	public PurchaseSummary getPurchaseSummary(UUID membershipId, SearchQuery query) {
		this.getMembership(membershipId);
		return this.memberships.getPurchaseSummary(membershipId, query);
	}

}
