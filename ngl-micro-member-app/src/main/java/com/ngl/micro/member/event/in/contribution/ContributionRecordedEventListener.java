package com.ngl.micro.member.event.in.contribution;

import javax.transaction.Transactional;

import com.ngl.micro.member.membership.MembershipService;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;
import com.ngl.middleware.util.Assert;

/**
 * Listens for {@link ContributionRecordedEvent}s.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackOn = Exception.class)
public class ContributionRecordedEventListener implements MessageListener<ContributionRecordedEvent> {

	private ContributionRecordedEventRepository events;
	private MembershipService memberships;

	public ContributionRecordedEventListener(
			ContributionRecordedEventRepository events,
			MembershipService memberships) {

		Assert.notNull(events);
		Assert.notNull(memberships);

		this.events = events;
		this.memberships = memberships;
	}

	@Override
	public void onMessage(ContributionRecordedEvent event) {
		this.memberships.recordPurchase(event.getDocument());
		this.events.addEvent(event);
	}

}
