package com.ngl.micro.member;

import java.util.UUID;
import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.member.dal.generated.jooq.JNglMicroMember;
import com.ngl.micro.member.event.in.contribution.ContributionRecordedEventDeserializer;
import com.ngl.micro.member.event.in.contribution.ContributionRecordedEventListener;
import com.ngl.micro.member.event.in.contribution.ContributionRecordedEventRepository;
import com.ngl.micro.member.event.in.token.AccessTokenRevokedEventListener;
import com.ngl.micro.member.event.in.token.AccessTokenRevokedEventRepository;
import com.ngl.micro.member.management.JsonMembershipLookupService;
import com.ngl.micro.member.membership.MembershipEndpoint;
import com.ngl.micro.member.membership.MembershipEndpointImpl;
import com.ngl.micro.member.membership.MembershipRepository;
import com.ngl.micro.member.membership.MembershipService;
import com.ngl.micro.member.membership.MembershipValidator;
import com.ngl.micro.member.revision.VersionControlService;
import com.ngl.micro.member.revision.VersionControlledEndpointDelegate;
import com.ngl.micro.member.revision.jooq.JVersionControlService;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.messaging.core.data.deserialize.LoggingDeserializationErrorHandler;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriber;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberConfiguration;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the transaction application.
 *
 * @author Willy du Preez
 */
@Configuration
@EnableMicroserviceDatabase
@EnableMicroserviceRestServer
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
public class MemberApplicationConfiguration extends MicroserviceConfiguration {

	private static final String MEMBERS_ENDPOINT = "member";

	@Bean
	public RestEndpoints restEndpoints(
			ObjectMapper mapper,
			MembershipEndpoint membershipEndpoint,
			JsonMembershipLookupService membershipLookupService,
			Realm securityRealm) {

		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints(MEMBERS_ENDPOINT);
		restEndpoints.addEndpoints(membershipEndpoint, membershipLookupService);
		restEndpoints.setRealm(securityRealm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	/* --------------------------------------------------------
	 *  Membership
	 * -------------------------------------------------------- */

	@Bean
	public MembershipRepository memberRepository(org.jooq.Configuration config) {
		return new MembershipRepository(config);
	}

	@Bean
	public MembershipValidator membershipValidator() {
		return MembershipValidator.newInstance();
	}

	@Bean
	public VersionControlService<Membership, UUID> membershipVersionControlService(DAL dal, ObjectMapper mapper) {
		return new JVersionControlService<>(
				mapper,
				JNglMicroMember.NGL_MICRO_MEMBER.getName(),
				"membership_revision",
				dal);
	}

	@Bean
	public MembershipService memberService(
			MembershipRepository memberRepository,
			MembershipValidator membershipValidator,
			VersionControlService<Membership, UUID> membershipVersionControlService) {
		return new MembershipService(
				memberRepository,
				membershipValidator,
				membershipVersionControlService);
	}

	@Bean
	public MembershipEndpoint memberEndpoint(
			RequestProcessor processor,
			MembershipService memberService,
			VersionControlService<Membership, UUID> membershipVersionControlService) {
		return new MembershipEndpointImpl(
				processor,
				memberService,
				new VersionControlledEndpointDelegate<>(membershipVersionControlService)
				);
	}

	/* --------------------------------------------------------
	 *  Management
	 * -------------------------------------------------------- */

	@Bean
	public JsonMembershipLookupService membershipLookupService(MembershipRepository memberRepository) {
		return new JsonMembershipLookupService(memberRepository);
	}

	/* --------------------------------------------------------
	 *  Access Tokens
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration configuration) {
		return new AccessTokenRevokedEventRepository(configuration);
	}

	@Bean
	public RevocationList revocationList(AccessTokenRevokedEventRepository events) {
		return new RevocationList(events.getByExpirationDateGreaterThan(Time.utcNow()));
	}

	@Bean
	public AccessTokenRevokedEventListener accessTokenEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository accessTokenRevokedEventRepository,
			RevocationList revocationList) {
		return new AccessTokenRevokedEventListener(objectMapper, accessTokenRevokedEventRepository, revocationList);
	}

	@Bean
	public Realm securityRealm(RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(
				this.signedEncryptedJwtFactory(),
				revocationList));
	}

	@Bean
	public SignedEncryptedJwtFactory signedEncryptedJwtFactory() {
		return new SignedEncryptedJwtFactory();
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber accessTokenEventSubscriber(
			ConnectionFactory connectionFactory,
			AccessTokenRevokedEventListener accessTokenEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<AccessTokenRevokedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(1);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setSubscription("member-" + AccessTokenRevokedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(accessTokenEventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(accessTokenEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

	/* --------------------------------------------------------
	 *  Events
	 * -------------------------------------------------------- */

	@Bean
	public ContributionRecordedEventRepository contributionRecordedEventRepository(org.jooq.Configuration config) {
		return new ContributionRecordedEventRepository(config);
	}

	@Bean
	public MessageListener<ContributionRecordedEvent> contributionRecordedEventListener(
			ContributionRecordedEventRepository contributionRecordedEventRepository,
			MembershipService membershipService) {
		return new ContributionRecordedEventListener(contributionRecordedEventRepository, membershipService);
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber contributionRecordedEventSubscriber(
			ObjectMapper objectMapper,
			ConnectionFactory connectionFactory,
			MessageListener<ContributionRecordedEvent> contributionRecordedEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<ContributionRecordedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(10);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(ContributionRecordedEvent.NAME);
		config.setSubscription("member-" + ContributionRecordedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(new ContributionRecordedEventDeserializer(objectMapper));
		config.setMetrics(new SpringJmsSubscriberMetrics(ContributionRecordedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(contributionRecordedEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

}
