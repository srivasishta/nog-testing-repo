package com.ngl.micro.member.revision;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.util.Beta;

@Beta(description =  "To be moved into middleware.")
public interface VersionControlService<R extends Identifiable<RID>, RID extends Serializable> {

	Revision getLatestRevision(RID resourceId);
	Revision getRevision(RID resourceId, UUID revisionId);

	Page<Revision> getRevisions(RID resourceId, PageRequest request);

	Optional<Revision> revise(
			ZonedDateTime revisedDate,
			Revision revision,
			R previous,
			R revised,
			UUID clientId,
			UUID partnerId);

}
