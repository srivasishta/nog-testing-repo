package com.ngl.micro.member.membership.constraint;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ngl.micro.member.CharityChoice;

/**
 * A validator of charity allocations to ensure that total allocations sum up to 100.
 *
 * @author Paco Mendes
 */
public class CharityChoicesValidator implements ConstraintValidator<ValidCharityChoices, Set<CharityChoice>> {

	@Override
	public void initialize(ValidCharityChoices choice) {
		choice.getClass();
	}

	@Override
	public boolean isValid(Set<CharityChoice> choices, ConstraintValidatorContext context) {
		if (choices == null) {
			failNull(context);
			return false;
		}
		if (choices.size() == 0) {
			return true;
		}

		Set<UUID> charities = new HashSet<>();
		Integer total = 0;
		boolean validation = true;

		for (CharityChoice choice : choices) {
			if (charities.contains(choice.getCharityId())) {
				failDuplicateCharities(context);
				validation = false;
			}
			charities.add(choice.getCharityId());
			total += choice.getAllocation();
		}

		if(!total.equals(100)) {
			failSum(context, total);
			validation = false;
		}
		return validation;
	}

	private void failNull(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("Choices may not be null.").addConstraintViolation();
	}

	private void failDuplicateCharities(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("Selected charities must be distinct.").addConstraintViolation();
	}

	private void failSum(ConstraintValidatorContext context, Integer total) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("Sum of all allocations must be 100. " + total + " provided.").addConstraintViolation();
	}
}