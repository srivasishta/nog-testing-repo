package com.ngl.micro.member.revision.jooq;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import org.jooq.Record;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.member.revision.VersionControlService;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.util.Beta;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.UUIDS;

@Transactional(rollbackFor = Exception.class)
@Beta(description =  "To be moved into middleware.")
public class JVersionControlService<R extends Identifiable<UUID>>
		implements VersionControlService<R, UUID> {

	private ObjectMapper mapper;
//	private Diff differ = new Diff();
	private JRevision table;
	private DAL dal;
	private FieldMap<String> fieldMap;
	private JooqQuery query;

	public JVersionControlService(ObjectMapper mapper, String schemaName, String tableName, DAL dal) {
		Assert.notNull(mapper);
		Assert.notNull(schemaName);
		Assert.notNull(tableName);
		Assert.notNull(dal);

		this.mapper = mapper;
		this.dal = dal;
		this.table = JRevision.createInstance(schemaName, tableName);

		this.fieldMap = new FieldMap<String>()
				.bind(Revision.Fields.ID, new MappedField(this.table.RESOURCE_ID).searchable(true))
				.bind(Revision.Fields.APPLIED_PATCH, this.table.APPLIED_PATCH)
				.bind(Revision.Fields.COMMENT, this.table.COMMENT)
				.bind(Revision.Fields.REVERT_PATCH, this.table.REVERT_PATCH)
				.bind(Revision.Fields.REVISED_BY_CLIENT, this.table.REVISED_BY_CLIENT)
				.bind(Revision.Fields.REVISED_BY_PARTNER, this.table.REVISED_BY_PARTNER)
				.bind(Revision.Fields.REVISED_BY_USER, this.table.REVISED_BY_USER)
				.bind(Revision.Fields.REVISED_DATE, new MappedField(this.table.REVISED_DATE).searchable(true).sortable(true))
				.bind(Revision.Fields.REVISED_RESOURCE_ID, new MappedField(this.table.REVISED_RESOURCE_ID).searchable(true));

		this.query = JooqQuery.builder(this.dal, this.fieldMap)
				.from(this.table)
				.build();
	}

	@Override
	@Transactional(readOnly = true)
	public Revision getLatestRevision(UUID resourceId) {
		Record record = this.dal.sql()
				.select(this.table.fields())
				.from(this.table)
				.where(this.table.REVISED_RESOURCE_ID.eq(resourceId))
				.orderBy(this.table.REVISED_DATE.desc())
				.limit(1)
				.fetchOne();

		return this.mapNullableRecord(resourceId, record);
	}

	@Override
	@Transactional(readOnly = true)
	public Revision getRevision(UUID resourceId, UUID revisionId) {
		Record record = this.dal.sql()
				.select(this.table.fields())
				.from(this.table)
				.where(this.table.RESOURCE_ID.eq(revisionId)
						.and(this.table.REVISED_RESOURCE_ID.eq(resourceId)))
				.fetchOne();

		return this.mapNullableRecord(resourceId, record);
	}

	private Revision mapNullableRecord(UUID resourceId, Record record) {
		if (record == null) {
			throw ApplicationError.resourceNotFound()
					.setTitle("Revision Not Found")
					.setDetail("No revision found for resource with ID: " + resourceId)
					.asException();
		}
		return this.mapRecord(record);
	}

	private Revision mapRecord(Record record) {
		try {
			Revision revision = new Revision();
			revision.setAppliedPatch(this.mapper.readValue(record.getValue(this.table.APPLIED_PATCH), Patch.class));
			revision.setComment(record.getValue(this.table.COMMENT));
			revision.setId(record.getValue(this.table.RESOURCE_ID));
			revision.setRevertPatch(this.mapper.readValue(record.getValue(this.table.REVERT_PATCH), Patch.class));
			revision.setRevisedByClient(record.getValue(this.table.REVISED_BY_CLIENT));
			revision.setRevisedByPartner(record.getValue(this.table.REVISED_BY_PARTNER));
			revision.setRevisedByUser(record.getValue(this.table.REVISED_BY_USER));
			revision.setRevisedDate(record.getValue(this.table.REVISED_DATE));
			revision.setRevisedResourceId(record.getValue(this.table.REVISED_RESOURCE_ID));
			return revision;
		} catch (IOException e) {
			throw new ApplicationError.ApplicationErrorBuilder(ApplicationStatus.INTERNAL_SERVER_ERROR)
					.setTitle("Internal Server Error")
					.setDetail("Failed to get patch for revision with ID: " + record.getValue(this.table.RESOURCE_ID))
					.asException(e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Revision> getRevisions(UUID resourceId, PageRequest request) {
		request.filter(SearchQueries.q(Revision.Fields.REVISED_RESOURCE_ID).eq(resourceId));
		return this.query.getPage(request, this::mapRecord);
	}

	@Override
	public Optional<Revision> revise(
			ZonedDateTime revisedDate,
			Revision revision,
			R previous,
			R revised,
			UUID clientId,
			UUID partnerId) {

		String appliedPatch = this.patchToText(revised.getId(), revised, previous);
		if (Strings.isNullOrWhitespace(appliedPatch)) {
			return Optional.empty();
		}
		this.validateRevisedDate(revisedDate, revised.getId());
		String revertPatch = this.patchToText(revised.getId(), previous, revised);

		JRevisionRecord record = new JRevisionRecord(this.table);
		record.setAppliedPatch(appliedPatch);
		record.setComment(revision.getComment());
		record.setResourceId(UUIDS.type4Uuid());
		record.setRevertPatch(revertPatch);
		record.setRevisedByClient(clientId);
		record.setRevisedByPartner(partnerId);
		record.setRevisedByUser(revision.getRevisedByUser());
		record.setRevisedDate(revisedDate);
		record.setRevisedResourceId(revised.getId());
		this.dal.sql().insertInto(this.table).set(record).execute();

		return Optional.of(this.mapRecord(record));
	}

	private void validateRevisedDate(ZonedDateTime revisedDate, UUID resourceId) {
		Record latest = this.dal.sql()
				.select(this.table.REVISED_DATE)
				.from(this.table)
				.where(this.table.REVISED_RESOURCE_ID.eq(resourceId))
				.orderBy(this.table.REVISED_DATE.desc())
				.limit(1)
				.fetchOne();
		if (latest != null
				&& !revisedDate.isAfter(latest.getValue(this.table.REVISED_DATE))) {
			throw ApplicationError.validationError()
					.setTitle("Validation Error")
					.setDetail("Resource out of date. Latest available revision ID: " + resourceId)
					.asException();
		}
	}

	private String patchToText(UUID resourceId, R working, R base) {
		try {
			// TODO CORE-1426: Middleware: Third-party ObjectDiffer used for
			// Patches has concurrency issues on compare(working, base) method
			Patch patch = new Diff().diff(working, base);
			if (patch.getOperations().isEmpty()) {
				return Strings.EMPTY;
			}
			return this.mapper.writeValueAsString(patch);
		} catch (IOException e) {
			throw new ApplicationError.ApplicationErrorBuilder(ApplicationStatus.INTERNAL_SERVER_ERROR)
					.setTitle("Internal Server Error")
					.setDetail("Failed to generate patch for resource with ID: " + resourceId)
					.asException(e);
		}
	}

}
