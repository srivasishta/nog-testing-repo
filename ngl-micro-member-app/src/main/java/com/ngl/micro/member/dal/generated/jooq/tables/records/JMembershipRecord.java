/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq.tables.records;


import com.ngl.micro.member.dal.generated.jooq.tables.JMembership;

import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JMembershipRecord extends UpdatableRecordImpl<JMembershipRecord> implements Record4<Long, UUID, UUID, String> {

	private static final long serialVersionUID = -2016632073;

	/**
	 * Setter for <code>ngl_micro_member.membership.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_member.membership.resource_id</code>.
	 */
	public void setResourceId(UUID value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership.resource_id</code>.
	 */
	public UUID getResourceId() {
		return (UUID) getValue(1);
	}

	/**
	 * Setter for <code>ngl_micro_member.membership.partner_id</code>.
	 */
	public void setPartnerId(UUID value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership.partner_id</code>.
	 */
	public UUID getPartnerId() {
		return (UUID) getValue(2);
	}

	/**
	 * Setter for <code>ngl_micro_member.membership.external_id</code>.
	 */
	public void setExternalId(String value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership.external_id</code>.
	 */
	public String getExternalId() {
		return (String) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, UUID, UUID, String> fieldsRow() {
		return (Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, UUID, UUID, String> valuesRow() {
		return (Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JMembership.MEMBERSHIP.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<UUID> field2() {
		return JMembership.MEMBERSHIP.RESOURCE_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<UUID> field3() {
		return JMembership.MEMBERSHIP.PARTNER_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field4() {
		return JMembership.MEMBERSHIP.EXTERNAL_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UUID value2() {
		return getResourceId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UUID value3() {
		return getPartnerId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value4() {
		return getExternalId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipRecord value2(UUID value) {
		setResourceId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipRecord value3(UUID value) {
		setPartnerId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipRecord value4(String value) {
		setExternalId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipRecord values(Long value1, UUID value2, UUID value3, String value4) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JMembershipRecord
	 */
	public JMembershipRecord() {
		super(JMembership.MEMBERSHIP);
	}

	/**
	 * Create a detached, initialised JMembershipRecord
	 */
	public JMembershipRecord(Long id, UUID resourceId, UUID partnerId, String externalId) {
		super(JMembership.MEMBERSHIP);

		setValue(0, id);
		setValue(1, resourceId);
		setValue(2, partnerId);
		setValue(3, externalId);
	}
}
