/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq.tables;


import com.ngl.micro.member.dal.generated.jooq.JNglMicroMember;
import com.ngl.micro.member.dal.generated.jooq.Keys;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JMembershipStatusHistoryRecord;
import com.ngl.middleware.database.mapping.extension.ZonedDateTimeConverter;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JMembershipStatusHistory extends TableImpl<JMembershipStatusHistoryRecord> {

	private static final long serialVersionUID = 891724322;

	/**
	 * The reference instance of <code>ngl_micro_member.membership_status_history</code>
	 */
	public static final JMembershipStatusHistory MEMBERSHIP_STATUS_HISTORY = new JMembershipStatusHistory();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JMembershipStatusHistoryRecord> getRecordType() {
		return JMembershipStatusHistoryRecord.class;
	}

	/**
	 * The column <code>ngl_micro_member.membership_status_history.id</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_status_history.membership_id</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, Long> MEMBERSHIP_ID = createField("membership_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_status_history.status_id</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, Long> STATUS_ID = createField("status_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_status_history.status_reason</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, String> STATUS_REASON = createField("status_reason", org.jooq.impl.SQLDataType.VARCHAR.length(50).nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_status_history.start_date</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, ZonedDateTime> START_DATE = createField("start_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new ZonedDateTimeConverter());

	/**
	 * The column <code>ngl_micro_member.membership_status_history.end_date</code>.
	 */
	public final TableField<JMembershipStatusHistoryRecord, ZonedDateTime> END_DATE = createField("end_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "", new ZonedDateTimeConverter());

	/**
	 * Create a <code>ngl_micro_member.membership_status_history</code> table reference
	 */
	public JMembershipStatusHistory() {
		this("membership_status_history", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_member.membership_status_history</code> table reference
	 */
	public JMembershipStatusHistory(String alias) {
		this(alias, MEMBERSHIP_STATUS_HISTORY);
	}

	private JMembershipStatusHistory(String alias, Table<JMembershipStatusHistoryRecord> aliased) {
		this(alias, aliased, null);
	}

	private JMembershipStatusHistory(String alias, Table<JMembershipStatusHistoryRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroMember.NGL_MICRO_MEMBER, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JMembershipStatusHistoryRecord, Long> getIdentity() {
		return Keys.IDENTITY_MEMBERSHIP_STATUS_HISTORY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JMembershipStatusHistoryRecord> getPrimaryKey() {
		return Keys.KEY_MEMBERSHIP_STATUS_HISTORY_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JMembershipStatusHistoryRecord>> getKeys() {
		return Arrays.<UniqueKey<JMembershipStatusHistoryRecord>>asList(Keys.KEY_MEMBERSHIP_STATUS_HISTORY_PRIMARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JMembershipStatusHistoryRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JMembershipStatusHistoryRecord, ?>>asList(Keys.MEMBERSHIP_STATUS_HISTORY_IBFK_2, Keys.MEMBERSHIP_STATUS_HISTORY_IBFK_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipStatusHistory as(String alias) {
		return new JMembershipStatusHistory(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JMembershipStatusHistory rename(String name) {
		return new JMembershipStatusHistory(name, null);
	}
}
