/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq.tables.records;


import com.ngl.micro.member.dal.generated.jooq.tables.JMembershipStatusType;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JMembershipStatusTypeRecord extends UpdatableRecordImpl<JMembershipStatusTypeRecord> implements Record2<Long, String> {

	private static final long serialVersionUID = -1094030795;

	/**
	 * Setter for <code>ngl_micro_member.membership_status_type.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership_status_type.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_member.membership_status_type.value</code>.
	 */
	public void setValue(String value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_member.membership_status_type.value</code>.
	 */
	public String getValue() {
		return (String) getValue(1);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record2 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, String> fieldsRow() {
		return (Row2) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, String> valuesRow() {
		return (Row2) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JMembershipStatusType.MEMBERSHIP_STATUS_TYPE.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field2() {
		return JMembershipStatusType.MEMBERSHIP_STATUS_TYPE.VALUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value2() {
		return getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipStatusTypeRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipStatusTypeRecord value2(String value) {
		setValue(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipStatusTypeRecord values(Long value1, String value2) {
		value1(value1);
		value2(value2);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JMembershipStatusTypeRecord
	 */
	public JMembershipStatusTypeRecord() {
		super(JMembershipStatusType.MEMBERSHIP_STATUS_TYPE);
	}

	/**
	 * Create a detached, initialised JMembershipStatusTypeRecord
	 */
	public JMembershipStatusTypeRecord(Long id, String value) {
		super(JMembershipStatusType.MEMBERSHIP_STATUS_TYPE);

		setValue(0, id);
		setValue(1, value);
	}
}
