package com.ngl.micro.member.membership;

import java.sql.Date;
import java.time.LocalDate;

import org.jooq.Configuration;

import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JMembershipDemographicsDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics;

/**
 * Demographics linked to a membership.
 *
 * @author Paco Mendes
 */
public class LinkedDemographics {

	private JMembershipDemographicsDao demographicsDao;

	public LinkedDemographics(Configuration configuration) {
		this.demographicsDao = new JMembershipDemographicsDao(configuration);
	}

	public void setMembershipDemographics(Long membershipId, Demographics demographics) {
		JMembershipDemographics record = demographicsDao.fetchOneByJMembershipId(membershipId);
		if (record == null) {
			record = new JMembershipDemographics();
			record.setMembershipId(membershipId);
			demographicsDao.insert(map(record, demographics));
		} else {
			demographicsDao.update(map(record, demographics));
		}
	}

	private JMembershipDemographics map(JMembershipDemographics record, Demographics demographics) {
		record.setGender(demographics.getGender().name());
		record.setBirthdate(optionalBirthdate(demographics.getBirthdate()));
		record.setCountry(demographics.getCountry().name());
		record.setRegion(demographics.getRegion());
		mapOptionalGeoLocation(record, demographics.getGeoLocation());
		return record;
	}

	private Date optionalBirthdate(LocalDate date) {
		return date == null ? null : Date.valueOf(date);
	}

	private void mapOptionalGeoLocation(JMembershipDemographics record, GeoLocation geoLocation) {
		if (geoLocation == null) {
			record.setLat(null);
			record.setLon(null);
		} else {
			record.setLat(geoLocation.getLat());
			record.setLon(geoLocation.getLon());
		}

	}

}
