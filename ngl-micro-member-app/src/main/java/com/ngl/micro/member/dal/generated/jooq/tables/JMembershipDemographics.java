/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq.tables;


import com.ngl.micro.member.dal.generated.jooq.JNglMicroMember;
import com.ngl.micro.member.dal.generated.jooq.Keys;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JMembershipDemographicsRecord;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JMembershipDemographics extends TableImpl<JMembershipDemographicsRecord> {

	private static final long serialVersionUID = 1285443308;

	/**
	 * The reference instance of <code>ngl_micro_member.membership_demographics</code>
	 */
	public static final JMembershipDemographics MEMBERSHIP_DEMOGRAPHICS = new JMembershipDemographics();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JMembershipDemographicsRecord> getRecordType() {
		return JMembershipDemographicsRecord.class;
	}

	/**
	 * The column <code>ngl_micro_member.membership_demographics.id</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.membership_id</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, Long> MEMBERSHIP_ID = createField("membership_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.birthdate</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, Date> BIRTHDATE = createField("birthdate", org.jooq.impl.SQLDataType.DATE, this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.gender</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, String> GENDER = createField("gender", org.jooq.impl.SQLDataType.VARCHAR.length(12).defaulted(true), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.country</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, String> COUNTRY = createField("country", org.jooq.impl.SQLDataType.VARCHAR.length(2).nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.region</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, String> REGION = createField("region", org.jooq.impl.SQLDataType.VARCHAR.length(50).nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.lat</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, Double> LAT = createField("lat", org.jooq.impl.SQLDataType.DOUBLE, this, "");

	/**
	 * The column <code>ngl_micro_member.membership_demographics.lon</code>.
	 */
	public final TableField<JMembershipDemographicsRecord, Double> LON = createField("lon", org.jooq.impl.SQLDataType.DOUBLE, this, "");

	/**
	 * Create a <code>ngl_micro_member.membership_demographics</code> table reference
	 */
	public JMembershipDemographics() {
		this("membership_demographics", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_member.membership_demographics</code> table reference
	 */
	public JMembershipDemographics(String alias) {
		this(alias, MEMBERSHIP_DEMOGRAPHICS);
	}

	private JMembershipDemographics(String alias, Table<JMembershipDemographicsRecord> aliased) {
		this(alias, aliased, null);
	}

	private JMembershipDemographics(String alias, Table<JMembershipDemographicsRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroMember.NGL_MICRO_MEMBER, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JMembershipDemographicsRecord, Long> getIdentity() {
		return Keys.IDENTITY_MEMBERSHIP_DEMOGRAPHICS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JMembershipDemographicsRecord> getPrimaryKey() {
		return Keys.KEY_MEMBERSHIP_DEMOGRAPHICS_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JMembershipDemographicsRecord>> getKeys() {
		return Arrays.<UniqueKey<JMembershipDemographicsRecord>>asList(Keys.KEY_MEMBERSHIP_DEMOGRAPHICS_PRIMARY, Keys.KEY_MEMBERSHIP_DEMOGRAPHICS_MEMBERSHIP_ID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JMembershipDemographicsRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JMembershipDemographicsRecord, ?>>asList(Keys.MEMBERSHIP_DEMOGRAPHICS_IBFK_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JMembershipDemographics as(String alias) {
		return new JMembershipDemographics(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JMembershipDemographics rename(String name) {
		return new JMembershipDemographics(name, null);
	}
}
