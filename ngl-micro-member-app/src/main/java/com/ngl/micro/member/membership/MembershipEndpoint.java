package com.ngl.micro.member.membership;

import static com.ngl.micro.shared.contracts.oauth.Scopes.MEMBER_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.MEMBER_WRITE_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.PURCHASE_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.member.Membership;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.http.PATCH;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Membership endpoint.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Path("memberships")
public interface MembershipEndpoint extends RestEndpoint {

	@POST
	@RequiresScopes(MEMBER_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(Membership member);

	@GET
	@Path("{id}")
	@RequiresScopes(MEMBER_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@GET
	@RequiresScopes(MEMBER_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@PATCH
	@Path("{id}")
	@RequiresScopes(MEMBER_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response update(@PathParam("id") UUID id, Patch patch);

	@GET
	@Path("{id}/purchases")
	@RequiresScopes(PURCHASE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response purchases(@PathParam("id") UUID id);

	@GET
	@Path("{id}/purchases/summary")
	@RequiresScopes(PURCHASE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response summary(@PathParam("id") UUID id);

}
