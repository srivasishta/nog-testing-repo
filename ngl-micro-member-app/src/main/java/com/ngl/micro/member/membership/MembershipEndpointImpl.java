package com.ngl.micro.member.membership;

import static com.ngl.micro.shared.contracts.oauth.Scopes.MEMBER_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.revision.VersionControlledEndpoint;
import com.ngl.micro.member.revision.VersionControlledEndpointDelegate;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Membership endpoint implementation.
 *
 * @author Willy du Preez
 *
 */
public class MembershipEndpointImpl implements MembershipEndpoint, VersionControlledEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private MembershipService membershipService;

	private VersionControlledEndpointDelegate<Membership, UUID> vc;

	public MembershipEndpointImpl(RequestProcessor processor,
			MembershipService membershipService,
			VersionControlledEndpointDelegate<Membership, UUID> vc) {
		this.processor = processor;
		this.membershipService = membershipService;
		this.vc = vc;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Membership membership = this.membershipService.getMembership(id);
		Resource resource = HAL.resource(membership)
				.fieldFilter(fields)
				.build();
		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Membership> page = this.membershipService.getMemberships(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Membership member) {
		Membership membership = this.membershipService.createMembership(member);
		Resource resource = HAL.resource(membership).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response update(UUID id, Patch patch) {
		Membership membership = this.membershipService.updateMembership(id, patch);
		Resource resource = HAL.resource(membership).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response purchases(UUID membershipId) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Purchase> page = this.membershipService.getPurchases(membershipId, pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response summary(UUID membershipId) {
		SearchQuery query = this.processor.pageRequest(this.msgCtx).getSearchQuery();
		PurchaseSummary summary = this.membershipService.getPurchaseSummary(membershipId, query);
		Resource resource = HAL.resource(summary).build();
		return Response.ok(resource).build();
	}

	@Override
	@RequiresScopes(MEMBER_READ_SCOPE)
	public Response getRevisions(UUID resourceId) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		return this.vc.getRevisions(resourceId, pageRequest);
	}

	@Override
	@RequiresScopes(MEMBER_READ_SCOPE)
	public Response getRevision(UUID resourceId, UUID revisionId) {
		return this.vc.getRevision(resourceId, revisionId);
	}

	@Override
	@RequiresScopes(MEMBER_READ_SCOPE)
	public Response getLatestRevision(UUID resourceId) {
		return this.vc.getLatestRevision(resourceId);
	}

}
