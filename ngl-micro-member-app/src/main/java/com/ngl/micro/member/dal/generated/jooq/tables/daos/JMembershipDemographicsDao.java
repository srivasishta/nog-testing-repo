/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq.tables.daos;


import com.ngl.micro.member.dal.generated.jooq.tables.JMembershipDemographics;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JMembershipDemographicsRecord;

import java.sql.Date;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JMembershipDemographicsDao extends DAOImpl<JMembershipDemographicsRecord, com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics, Long> {

	/**
	 * Create a new JMembershipDemographicsDao without any configuration
	 */
	public JMembershipDemographicsDao() {
		super(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS, com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics.class);
	}

	/**
	 * Create a new JMembershipDemographicsDao with an attached configuration
	 */
	public JMembershipDemographicsDao(Configuration configuration) {
		super(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS, com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJId(Long... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics fetchOneByJId(Long value) {
		return fetchOne(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.ID, value);
	}

	/**
	 * Fetch records that have <code>membership_id IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJMembershipId(Long... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.MEMBERSHIP_ID, values);
	}

	/**
	 * Fetch a unique record that has <code>membership_id = value</code>
	 */
	public com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics fetchOneByJMembershipId(Long value) {
		return fetchOne(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.MEMBERSHIP_ID, value);
	}

	/**
	 * Fetch records that have <code>birthdate IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJBirthdate(Date... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.BIRTHDATE, values);
	}

	/**
	 * Fetch records that have <code>gender IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJGender(String... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.GENDER, values);
	}

	/**
	 * Fetch records that have <code>country IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJCountry(String... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.COUNTRY, values);
	}

	/**
	 * Fetch records that have <code>region IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJRegion(String... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.REGION, values);
	}

	/**
	 * Fetch records that have <code>lat IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJLat(Double... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.LAT, values);
	}

	/**
	 * Fetch records that have <code>lon IN (values)</code>
	 */
	public List<com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics> fetchByJLon(Double... values) {
		return fetch(JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS.LON, values);
	}
}
