package com.ngl.micro.member;

import static com.ngl.micro.member.dal.generated.jooq.JNglMicroMember.NGL_MICRO_MEMBER;

import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The member application entry-point.
 *
 * @author Willy du Preez
 *
 */
public class MemberApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new MemberApplication().development();
	}

	public MemberApplication() {
		super(MemberApplicationConfiguration.class, NGL_MICRO_MEMBER);
	}

}