package com.ngl.micro.member.membership;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.MaxDef;
import org.hibernate.validator.cfg.defs.MinDef;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.membership.constraint.CharityChoicesDef;
import com.ngl.middleware.rest.server.validation.ResourceValidator;
import com.ngl.middleware.rest.server.validation.constraint.CoordinateDef;

public class MembershipValidator extends ResourceValidator<Membership> {

	public static MembershipValidator newInstance() {
		return ResourceValidator.configurator(new MembershipValidator()).configure();
	}

	private MembershipValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Membership.class)
			.property("partnerId", FIELD)
				.constraint(new NotNullDef())
			.property("status", FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("externalId", FIELD)
				.constraint(new NotNullDef())
			.property("demographics", FIELD)
				.constraint(new NotNullDef())
				.valid()
			.property("charityChoices", FIELD)
				.constraint(new CharityChoicesDef())
				.valid()
		.type(Demographics.class)
			.property("gender", FIELD)
				.constraint(new NotNullDef())
			.property("country", FIELD)
				.constraint(new NotNullDef())
			.property("region", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("geoLocation", FIELD)
				.valid()
			.property("birthdate", FIELD)
		.type(GeoLocation.class)
			.property("lat", FIELD)
				.constraint(new CoordinateDef().lat().radians().nullable())
			.property("lon", FIELD)
				.constraint(new CoordinateDef().lon().radians().nullable())
		.type(CharityChoice.class)
			.property("charityId", FIELD)
				.constraint(new NotNullDef())
			.property("allocation", FIELD)
				.constraint(new MinDef().value(1))
				.constraint(new MaxDef().value(100));
	}

}
