package com.ngl.micro.member.event.in.contribution;

import static com.ngl.micro.member.dal.generated.jooq.Tables.CONTRIBUTION_RECORDED_EVENT;

import java.util.Optional;
import java.util.UUID;

import org.springframework.util.Assert;

import com.ngl.micro.member.dal.generated.jooq.tables.records.JContributionRecordedEventRecord;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.dal.repository.EventRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

/**
 * Manages {@link ContributionRecordedEvent}s received.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEventRepository implements EventRepository<ContributionRecordedEvent, Long> {

	private DAL dal;

	public ContributionRecordedEventRepository(org.jooq.Configuration config) {
		this.dal = new DAL(config);
	}

	@Override
	public ContributionRecordedEvent addEvent(ContributionRecordedEvent event) {
		Assert.notNull(event);

		JContributionRecordedEventRecord record = this.dal.sql().newRecord(CONTRIBUTION_RECORDED_EVENT);
		record.setId(event.getId());
		record.setCreatedDate(event.getCreated());
		record.setContributionDocumentId(event.getDocument().getId());
		record.store();

		return event;
	}

	@Override
	public Optional<ContributionRecordedEvent> getEvent(Long id) {
		throw new UnsupportedOperationException("Use ContributionRecordedEventRepository.getEventHistory()");
	}

	public Optional<ContributionRecordedEventHistory> getEventHistory(Long id) {
		Assert.notNull(id);

		JContributionRecordedEventRecord record = this.dal.sql()
				.selectFrom(CONTRIBUTION_RECORDED_EVENT)
				.where(CONTRIBUTION_RECORDED_EVENT.ID.eq(id))
				.fetchOne();

		if (record == null) {
			return Optional.empty();
		}

		UUID documentId = record.getValue(CONTRIBUTION_RECORDED_EVENT.CONTRIBUTION_DOCUMENT_ID);

		return Optional.of(new ContributionRecordedEventHistory(
				id,
				record.getValue(CONTRIBUTION_RECORDED_EVENT.CREATED_DATE),
				documentId));
	}

}
