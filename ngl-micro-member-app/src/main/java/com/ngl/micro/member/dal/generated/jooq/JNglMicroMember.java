/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.member.dal.generated.jooq;


import com.ngl.micro.member.dal.generated.jooq.tables.JAccessTokenRevokedEvent;
import com.ngl.micro.member.dal.generated.jooq.tables.JCharityAllocation;
import com.ngl.micro.member.dal.generated.jooq.tables.JCharityChoice;
import com.ngl.micro.member.dal.generated.jooq.tables.JContributionRecordedEvent;
import com.ngl.micro.member.dal.generated.jooq.tables.JGender;
import com.ngl.micro.member.dal.generated.jooq.tables.JMembership;
import com.ngl.micro.member.dal.generated.jooq.tables.JMembershipDemographics;
import com.ngl.micro.member.dal.generated.jooq.tables.JMembershipStatusHistory;
import com.ngl.micro.member.dal.generated.jooq.tables.JMembershipStatusType;
import com.ngl.micro.member.dal.generated.jooq.tables.JPurchase;
import com.ngl.micro.member.dal.generated.jooq.tables.JSchemaVersion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JNglMicroMember extends SchemaImpl {

	private static final long serialVersionUID = 690407023;

	/**
	 * The reference instance of <code>ngl_micro_member</code>
	 */
	public static final JNglMicroMember NGL_MICRO_MEMBER = new JNglMicroMember();

	/**
	 * No further instances allowed
	 */
	private JNglMicroMember() {
		super("ngl_micro_member");
	}

	@Override
	public final List<Table<?>> getTables() {
		List result = new ArrayList();
		result.addAll(getTables0());
		return result;
	}

	private final List<Table<?>> getTables0() {
		return Arrays.<Table<?>>asList(
			JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT,
			JCharityAllocation.CHARITY_ALLOCATION,
			JCharityChoice.CHARITY_CHOICE,
			JContributionRecordedEvent.CONTRIBUTION_RECORDED_EVENT,
			JGender.GENDER,
			JMembership.MEMBERSHIP,
			JMembershipDemographics.MEMBERSHIP_DEMOGRAPHICS,
			JMembershipStatusHistory.MEMBERSHIP_STATUS_HISTORY,
			JMembershipStatusType.MEMBERSHIP_STATUS_TYPE,
			JPurchase.PURCHASE,
			JSchemaVersion.SCHEMA_VERSION);
	}
}
