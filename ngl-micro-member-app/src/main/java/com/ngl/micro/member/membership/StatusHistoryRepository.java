package com.ngl.micro.member.membership;

import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP_STATUS_HISTORY;
import static java.util.stream.Collectors.toList;

import java.time.ZonedDateTime;
import java.util.List;

import org.jooq.Configuration;
import org.jooq.Result;

import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.dal.generated.jooq.Tables;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JMembershipStatusHistoryRecord;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.util.Experimental;

/**
 * Tracked member status history.
 *
 * @author Paco Mendes
 */
public class StatusHistoryRepository {

	private DAL support;

	public StatusHistoryRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public void setMembershipStatus(Long MembershipId, MembershipStatus status, String reason, ZonedDateTime updatedDate) {
		JMembershipStatusHistoryRecord currentStatus = this.getStatusRecordAt(MembershipId, updatedDate);
		if (currentStatus == null) {
			this.addStatusRecord(MembershipId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(MembershipId, status, reason, updatedDate);
		}
	}

	private boolean updated(JMembershipStatusHistoryRecord currentStatus, MembershipStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void addStatusRecord(Long MembershipId, MembershipStatus status, String reason, ZonedDateTime startDate) {
		JMembershipStatusHistoryRecord record =  this.support.sql().newRecord(MEMBERSHIP_STATUS_HISTORY);
		record.setMembershipId(MembershipId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(startDate);
		record.store();
	}

	private void expireStatusRecord(JMembershipStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public MembershipStatusRecord getByMembershipIdAt(Long membershipId, ZonedDateTime updatedDate) {
		JMembershipStatusHistoryRecord record = this.getStatusRecordAt(membershipId, updatedDate);
		return record == null ? null : this.map(record);
	}

	private JMembershipStatusHistoryRecord getStatusRecordAt(Long membershipId, ZonedDateTime updatedDate) {
		return this.support.sql()
				.selectFrom(MEMBERSHIP_STATUS_HISTORY)
				.where(MEMBERSHIP_STATUS_HISTORY.MEMBERSHIP_ID.eq(membershipId))
				.and(MEMBERSHIP_STATUS_HISTORY.START_DATE.lessOrEqual(updatedDate))
				.and(MEMBERSHIP_STATUS_HISTORY.END_DATE.greaterThan(updatedDate)
						.or(MEMBERSHIP_STATUS_HISTORY.END_DATE.isNull()))
				.fetchOne();
	}

	@Experimental
	public List<MembershipStatusRecord> list(Long membershipId) {
		Result<JMembershipStatusHistoryRecord> records = this.support.sql()
				.selectFrom(MEMBERSHIP_STATUS_HISTORY)
				.where(Tables.MEMBERSHIP_STATUS_HISTORY.MEMBERSHIP_ID.eq(membershipId))
				.fetch();

		return records.stream().map(this::map).collect(toList());
	}

	private MembershipStatusRecord map(JMembershipStatusHistoryRecord record) {
		MembershipStatusRecord status = new MembershipStatusRecord();
		status.setId(record.getId());
		status.setStatus(MembershipStatus.valueOf(ResourceStatus.forId(record.getStatusId()).name()));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}

}
