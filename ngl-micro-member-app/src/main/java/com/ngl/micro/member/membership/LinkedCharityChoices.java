package com.ngl.micro.member.membership;

import static com.ngl.micro.member.dal.generated.jooq.Tables.CHARITY_ALLOCATION;
import static com.ngl.micro.member.dal.generated.jooq.Tables.CHARITY_CHOICE;
import static com.ngl.micro.member.dal.generated.jooq.Tables.MEMBERSHIP;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Condition;
import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.MembershipCharityChoice;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JCharityAllocationDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JCharityAllocation;
import com.ngl.micro.member.dal.generated.jooq.tables.records.JCharityChoiceRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;

public class LinkedCharityChoices {

	private static final String CHARITY_ID = "charity_id";
	private static final String ALLOCATION = "allocation";

	private DAL support;
	private JCharityAllocationDao allocationsDao;
	private JooqQuery query;
	private TableOnConditionStep<?> tables;
	private FieldMap<String> fieldMap;

	public LinkedCharityChoices(Configuration configuration) {
		this.support = new DAL(configuration);
		this.allocationsDao = new JCharityAllocationDao(configuration);

		this.fieldMap = new FieldMap<String>()
							.bind(Membership.Fields.ID, new MappedField(MEMBERSHIP.RESOURCE_ID).searchable(true))
							.bind(CHARITY_ID, new MappedField(CHARITY_ALLOCATION.CHARITY_ID))
							.bind(ALLOCATION, new MappedField(CHARITY_ALLOCATION.ALLOCATION));

		ZonedDateTime nowDate = ZonedDateTime.now();
		this.tables = MEMBERSHIP
				.leftOuterJoin(CHARITY_CHOICE).on(MEMBERSHIP.ID.eq(CHARITY_CHOICE.MEMBERSHIP_ID)
						.and(CHARITY_CHOICE.START_DATE.le(nowDate)
						.and(CHARITY_CHOICE.END_DATE.isNull().or(CHARITY_CHOICE.END_DATE.ge(nowDate)))
						))
				.leftOuterJoin(CHARITY_ALLOCATION).on(CHARITY_ALLOCATION.CHARITY_CHOICE_ID.eq(CHARITY_CHOICE.ID));

		this.query = JooqQuery
				.builder(this.support, this.fieldMap)
				.from(tables)
				.build();
	}

	public Map<UUID, Set<CharityChoice>> list(Page<Membership> membershipPage) {

		List<Record> charityChoiceRecords = this.query.getRecordsWhere(
				MEMBERSHIP.RESOURCE_ID.in(
						membershipPage.getItems()
							.stream()
							.map(m -> m.getId())
							.collect(Collectors.toList())
				)
		);

		return charityChoiceRecords.stream()
			.map(this::map)
			.collect(
				Collectors.groupingBy(MembershipCharityChoice::getMembershipId,
						Collectors.mapping(this::map,
						Collectors.toSet())
				)
			);
	}

	private CharityChoice map(MembershipCharityChoice membershipCharityChoice) {
		CharityChoice charityChoice = new CharityChoice();
		charityChoice.setAllocation(membershipCharityChoice.getAllocation());
		charityChoice.setCharityId(membershipCharityChoice.getCharityId());
		return charityChoice;
	}

	private MembershipCharityChoice map(Record record) {
		MembershipCharityChoice membershipCharityChoice = new MembershipCharityChoice();
		membershipCharityChoice.setMembershipId(record.getValue(MEMBERSHIP.RESOURCE_ID));
		membershipCharityChoice.setCharityId(record.getValue(CHARITY_ALLOCATION.CHARITY_ID));
		membershipCharityChoice.setAllocation(record.getValue(CHARITY_ALLOCATION.ALLOCATION));
		return membershipCharityChoice;
	}


	public Set<CharityChoice> setMembershipCharityChoice(Long membershipId, Set<CharityChoice> choices, ZonedDateTime updatedDate) {
		Set<CharityChoice> activeChoices = getByMembershipIdAt(membershipId, updatedDate);
		if (activeChoices.equals(choices)) {
			return activeChoices;
		}
		JCharityChoiceRecord activeRecord = this.getRecord(this.active(membershipId));
		if (activeRecord != null) {
			this.expire(activeRecord, updatedDate);

		}
		return this.addChoices(membershipId, choices, updatedDate);
	}

	private Set<CharityChoice> addChoices(Long membershipId, Set<CharityChoice> choices, ZonedDateTime updatedDate) {
		if (choices.isEmpty()) {
			return new HashSet<>();
		}
		JCharityChoiceRecord choiceRecord = this.support.sql().newRecord(CHARITY_CHOICE);
		choiceRecord.setMembershipId(membershipId);
		choiceRecord.setStartDate(updatedDate);
		choiceRecord.setEndDate(null);
		choiceRecord.store();
		this.allocationsDao.insert(this.map(choiceRecord.getId(), choices));
		return this.getByMembershipId(membershipId);
	}

	private void expire(JCharityChoiceRecord record, ZonedDateTime expiry) {
		record.setEndDate(expiry);
		record.store();
	}

	private Set<JCharityAllocation> map(Long choiceId, Set<CharityChoice> choices) {
		Set<JCharityAllocation> allocationRecords = new HashSet<>();
		choices.forEach(c ->
			allocationRecords.add(new JCharityAllocation(null, choiceId, c.getCharityId(), c.getAllocation()))
		);
		return allocationRecords;
	}

	public Set<CharityChoice> getByMembershipId(Long membershipId) {
		return this.getChoices(this.active(membershipId));
	}

	public Set<CharityChoice> getByMembershipIdAt(Long membershipId, ZonedDateTime date) {
		return this.getChoices(this.effective(membershipId, date));
	}

	private Condition active(Long membershipId) {
		return CHARITY_CHOICE.MEMBERSHIP_ID.eq(membershipId)
				.and(CHARITY_CHOICE.END_DATE.isNull());
	}
	private Condition effective(Long membershipId, ZonedDateTime date) {
		return CHARITY_CHOICE.MEMBERSHIP_ID.eq(membershipId)
				.and(CHARITY_CHOICE.START_DATE.lessOrEqual(date))
				.and(CHARITY_CHOICE.END_DATE.isNull()
					.or(CHARITY_CHOICE.END_DATE.greaterThan(date)));
	}

	private Set<CharityChoice> getChoices(Condition condition) {
		JCharityChoiceRecord record = this.getRecord(condition);
		if (record == null) {
			return new HashSet<>();
		}
		return this.getChoices(record.getId());
	}

	private JCharityChoiceRecord getRecord(Condition condition) {
		return this.support.sql()
				.selectFrom(CHARITY_CHOICE)
				.where(condition)
				.fetchOne();
	}

	private Set<CharityChoice> getChoices(Long choiceId) {
		return this.allocationsDao.fetchByJCharityChoiceId(choiceId)
				.stream()
				.map(c -> {
					CharityChoice choice = new CharityChoice();
					choice.setCharityId(c.getCharityId());
					choice.setAllocation(c.getAllocation());
					return choice;
				})
				.collect(Collectors.toSet());
	}

}