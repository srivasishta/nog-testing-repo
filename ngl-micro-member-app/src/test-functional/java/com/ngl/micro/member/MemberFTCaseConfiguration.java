package com.ngl.micro.member;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.member.client.MemberClient;
import com.ngl.micro.member.management.ManagementClient;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerConfiguration;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerProperties;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCaseConfiguration;
import com.ngl.middleware.rest.api.page.QueryParameterParser;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.HttpClient;

@Configuration
@EnableMicroserviceJmsClient
@Import(HornetQEmbeddedServerConfiguration.class)
public class MemberFTCaseConfiguration extends AbstractApplicationFTCaseConfiguration {

	public static final Long DEFAULT_PARTNER_ID = 2L;

	private static final String MEMBER_SERVICE_URL = "http://localhost:9095/service/member/";

	@Bean
	public HornetQEmbeddedServerProperties hornetQEmbeddedServerProperties(
			EnvironmentConfigurationProvider config) {
		return config.getConfiguration(
				HornetQEmbeddedServerProperties.DEFAULT_PREFIX,
				HornetQEmbeddedServerProperties.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public MemberClient restClient(
			HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(MEMBER_SERVICE_URL, requestFactory, parser, mapper);
		return new MemberClient(config);
	}

	@Bean
	public ManagementClient managementClient(HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(MEMBER_SERVICE_URL + "management/", requestFactory, parser, mapper);
		return new ManagementClient(config);
	}

}
