package com.ngl.micro.member.member;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Set;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Country;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.Demographics.Gender;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.MemberApplication;
import com.ngl.micro.member.MemberFTCaseConfiguration;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.client.MemberClient;
import com.ngl.micro.member.test_data.TestAccessTokens;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.micro.member.test_data.TestPurchases;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		MemberFTCaseConfiguration.class
})
public class MemberFTCase extends AbstractApplicationFTCase {

	private static final String MEMBERSHIP_ID_FILTER = "membershipId";

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestMemberships testMemberships;
	private TestPurchases testPurchases;

	@Autowired
	private MemberClient client;

	public MemberFTCase() {
		super(MemberApplication.class);
		this.testMemberships = new TestMemberships();
		this.testPurchases = new TestPurchases();
	}

	@Test
	public void test_create_membership_as_system() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Membership member= this.testMemberships.membership();
		member.setExternalId(Data.STRING_1);
		member.setPartnerId(Data.ID_4);
		member = this.client.getMembershipResource().create(member).getState();

		assertEquals(Data.ID_4, member.getPartnerId());

		Membership systemMember= this.testMemberships.membership();
		systemMember.setExternalId(Data.STRING_2);
		systemMember.setPartnerId(null);
		systemMember = this.client.getMembershipResource().create(systemMember).getState();

		assertEquals(ApiPolicy.SYSTEM_ACCOUNT_ID, systemMember.getPartnerId());
	}

	@Test
	public void test_membership_create_read_update() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_full_service_access());

		String providedIdentifier = " identifier ";
		String expectedIdentifier = "identifier";
		String createdStatusReason = "created";
		String updatedStatusReason = "suspended";

		Demographics baseDemographics = this.testMemberships.demographics(null, Gender.UNSPECIFIED, Country.US, "", new GeoLocation());
		Demographics updatedDemographics = this.testMemberships.demographics(
				LocalDate.of(1982, 2, 2),
				Gender.FEMALE,
				Country.CA, "AB",
				GeoLocation.of(-1.230123, 1.02120));
		Membership base = this.testMemberships.membership(providedIdentifier);
		base.setDemographics(baseDemographics);
		base.setStatus(MembershipStatus.ACTIVE);
		base.setStatusReason(createdStatusReason);
		Membership membership = this.client.getMembershipResource()
				.create(base)
				.getState();

		assertNotNull(membership.getId());
		assertEquals(expectedIdentifier, membership.getExternalId());
		assertEquals(baseDemographics, membership.getDemographics());
		assertEquals(MembershipStatus.ACTIVE, membership.getStatus());
		assertEquals(createdStatusReason, membership.getStatusReason());

		Set<CharityChoice> choices = Collections.asSet(
				new CharityChoice(Data.ID_2, 10),
				new CharityChoice(Data.ID_3, 20),
				new CharityChoice(Data.ID_4, 70));
		Membership working = this.client.getMembershipResource().get(membership.getId()).getState();
		working.setExternalId("Updated Identifier");
		working.setStatus(MembershipStatus.INACTIVE);
		working.setStatusReason(updatedStatusReason);
		working.setDemographics(updatedDemographics);
		working.setCharityChoices(choices);
		Patch patch = new Diff().diff(working, membership);
		Membership updated = this.client.getMembershipResource().patch(membership.getId(), patch).getState();

		assertEquals(MembershipStatus.INACTIVE, updated.getStatus());
		assertEquals(updatedStatusReason, updated.getStatusReason());
		assertEquals(updatedDemographics, updated.getDemographics());
		assertEquals(choices, updated.getCharityChoices());
	}

	@Test
	public void test_list_memberships() {
		this.test_membership_create_read_update();
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_full_service_access());

		Page<Membership> page = this.client.getMembershipResource().list(PageRequest.with(0, 10).build()).getState();

		Assert.assertThat(page.getNumberOfItems(), CoreMatchers.is(1));
		Assert.assertThat(page.getPageNumber(), CoreMatchers.is(0));
		Assert.assertThat(page.getPageSize(), CoreMatchers.is(10));
		Assert.assertThat(page.getTotalItems(), CoreMatchers.is(1L));
		Assert.assertThat(page.getTotalPages(), CoreMatchers.is(1));
		Assert.assertThat(page.getItems().size(), CoreMatchers.is(1));
	}

	@Test
	public void test_unauthorised_partner_read() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_3_member_read());

		this.thrown.expectStatus(ApplicationStatus.NOT_FOUND);

		this.client.getMembershipResource().get(Data.toUUID(TestMemberships.PARTNER_2_MEMBER_1_ID));
	}

	@Test
	public void test_unauthorised_read_scope() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_3_member_read());

		this.thrown.expectStatus(ApplicationStatus.NOT_FOUND);

		this.client.getMembershipResource().get(Data.toUUID(TestMemberships.PARTNER_2_MEMBER_1_ID));
	}

	@Test
	public void test_create_with_authorised_partner_id() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_full_service_access());

		Membership member = this.testMemberships.membership();
		member.setPartnerId(Data.toUUID(TestMemberships.PARTNER_3_PK));
		member = this.client.getMembershipResource().create(member).getState();

		assertEquals(TestMemberships.PARTNER_2, member.getPartnerId());
	}

	@Test
	public void test_unauthorised_create_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_member_read());

		Membership member = this.testMemberships.membership();
		member.setPartnerId(Data.toUUID(TestMemberships.PARTNER_2_PK));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getMembershipResource().create(member);
	}

	@Test
	public void test_unauthorised_update() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_member_read());

		Membership base = this.client.getMembershipResource().get(Data.toUUID(TestMemberships.PARTNER_2_MEMBER_1_ID)).getState();

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getMembershipResource().patch(base.getId(), new Patch(new ArrayList<>()));
	}

	@Test
	public void test_get_purchases() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_purchase_read());

		Page<Purchase> purchases = this.client.getMembershipResource().purchases(Data.toUUID(TestMemberships.PARTNER_2_MEMBER_1_ID), PageRequest.with(0, 100).build()).getState();
		assertTrue(purchases.getItems().size() > 0);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_3_purchase_read());
		purchases = this.client.getMembershipResource().purchases(Data.toUUID(TestMemberships.PARTNER_3_MEMBER_5_ID), PageRequest.with(0, 100).build()).getState();
		assertTrue("no purchases for other partner", purchases.getItems().isEmpty());
	}

	@Test
	public void test_get_purchase_summary() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_full_service_access());

		this.client.getMembershipResource().create(this.testMemberships.membership()).getState();
		PurchaseSummary summary = this.client.getMembershipResource().purchaseSummary(TestMemberships.PARTNER_2).getState();

		assertNotNull(summary);
	}

	@Test
	public void test_get_purchase_summary_filter() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_full_service_access());

		SearchQuery query = SearchQueries
			.q(Purchase.Fields.STORE_ID).eq(Data.ID_5)
			.and(Purchase.Fields.BUSINESS_ID).eq(Data.ID_6)
			.and(Purchase.Fields.CONTRIBUTION_AMOUNT).ge(Data.AMOUNT_10_00)
			.and(Purchase.Fields.PURCHASE_DATE).ge(Data.T_0.minusDays(3))
			.getSearchQuery();

		PurchaseSummary summary = this.client.getMembershipResource().purchaseSummary(Data.ID_1, query).getState();
		assertNotNull(summary);
		assertTrue(summary.getTotalPurchases() >= 1);
		assertEquals(summary.getTotalContributed().toString(), "10.95400");
		assertEquals(summary.getTotalAmount().toString(), "109.54000");
	}

	@Test
	public void test_unauthorised_purchase_read() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_member_read());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getMembershipResource().purchases(TestMemberships.PARTNER_2, PageRequest.with(0, 1).build());
	}

	@Test
	public void test_unauthorised_purchase_summary_read() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.partner_id_2_member_read());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getMembershipResource().purchaseSummary(TestMemberships.PARTNER_2);
	}

}
