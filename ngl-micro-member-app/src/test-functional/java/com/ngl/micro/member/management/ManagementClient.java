package com.ngl.micro.member.management;

import static com.ngl.middleware.rest.client.http.MediaTypes.APPLICATION_JSON;
import static com.ngl.middleware.rest.client.http.Methods.POST;

import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.client.http.response.RestResponse;

/**
 * The Member Service Client.
 *
 * @author Paco Mendes
 */
public class ManagementClient extends AbstractRestClient {

	public ManagementClient(RestClientConfig config) {
		super(config);
	}

	public MembershipLookupResponse lookup(MembershipLookupRequest lookup) {
		Request request = ManagementClient.this.http.request(this.serviceUrl + "lookup")
				.method(POST)
				.header(Headers.CONTENT_TYPE, APPLICATION_JSON)
				.body(lookup);

		RestResponse response = request.execute(RestResponse.class);

		if (response.getStatus() == ApplicationStatus.OK.code()) {
			return response.bodyAsPojo(MembershipLookupResponse.class);
		} else {
			ApplicationError error = response.bodyAsPojo(ApplicationError.class);
			throw new ApplicationException(error, error.getDetail());
		}
	}

}
