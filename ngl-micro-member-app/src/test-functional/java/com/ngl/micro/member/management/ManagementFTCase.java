package com.ngl.micro.member.management;

import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_2;
import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_2_MEMBER_1_EXTERNAL_ID;
import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_3;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.MemberApplication;
import com.ngl.micro.member.MemberFTCaseConfiguration;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.member.MembershipLookupRequest;
import com.ngl.micro.shared.contracts.member.MembershipLookupResponse;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		MemberFTCaseConfiguration.class
})
@Ignore
public class ManagementFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestMemberships testMemberships;

	@Autowired
	private ManagementClient client;

	public ManagementFTCase() {
		super(MemberApplication.class);
		this.testMemberships = new TestMemberships();
	}

	@Test
	public void test_lookup_membership() {
		this.cmdLoadTestDataSet(this.testMemberships);

		MembershipLookupResponse response = this.client.lookup(new MembershipLookupRequest(PARTNER_2, PARTNER_2_MEMBER_1_EXTERNAL_ID, Data.T_0));

		assertEquals(true, response.isMatched());
		assertEquals(TestMemberships.PARTNER_2_MEMBER_1, response.getMembershipId());
		assertEquals(ResourceStatus.ACTIVE, response.getStatus());
		assertEquals(Gender.MALE, response.getDemographics().getGender());
		assertEquals(LocalDate.of(1980,  12, 1), response.getDemographics().getBirthdate().get());
		assertEquals(4, response.getCharityChoice().size());

		response = this.client.lookup(new MembershipLookupRequest(PARTNER_2, PARTNER_2_MEMBER_1_EXTERNAL_ID, Data.T_MINUS_5M));
		assertEquals(true, response.isMatched());
		assertEquals(TestMemberships.PARTNER_2_MEMBER_1, response.getMembershipId());
		assertEquals(ResourceStatus.INACTIVE, response.getStatus());
		assertEquals(1, response.getCharityChoice().size());

		response = this.client.lookup(new MembershipLookupRequest(PARTNER_3, PARTNER_2_MEMBER_1_EXTERNAL_ID, Data.T_0));
		assertEquals("Unmatched partner", false, response.isMatched());

		response = this.client.lookup(new MembershipLookupRequest(PARTNER_2, "not matching", Data.T_0));
		assertEquals("Unmatched external id", false, response.isMatched());
	}

}
