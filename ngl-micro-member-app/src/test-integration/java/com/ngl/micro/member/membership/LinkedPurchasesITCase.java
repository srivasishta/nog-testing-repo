package com.ngl.micro.member.membership;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.micro.member.test_data.TestPurchases;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class LinkedPurchasesITCase extends AbstractDatabaseITCase {

	private LinkedPurchases repo;
	private TestMemberships testMemberships;
	private TestPurchases testPurchases;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new LinkedPurchases(this.sqlContext);
		this.testMemberships = new TestMemberships();
		this.testPurchases = new TestPurchases();
	}

	@Test
	public void test_record_purchase() {
		this.cmdLoadTestDataSet(this.testMemberships);

		Purchase purchase = this.testPurchases.purchase(1L);
		Purchase created = this.repo.addPurchase(Data.toUUID(1L), purchase);

		assertEquals(purchase, created);
	}

	@Test
	public void test_get_purchases() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);

		Page<Purchase> purchases = this.repo.getByMembershipId(Data.ID_1, PageRequest.with(0, 1000).build());
		BigDecimal totalPurchases = purchases.getItems().stream().map(p -> p.getPurchaseAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal totalContributions = purchases.getItems().stream().map(p -> p.getContributionAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);

		PurchaseSummary summary = this.repo.getPurchaseSummaryByMembershipId(Data.ID_1, new SearchQuery());

		assertTrue(summary.getTotalPurchases() > 0);
		assertEquals(summary.getTotalPurchases().longValue(), purchases.getTotalItems());
		assertEquals(summary.getTotalAmount(), totalPurchases);
		assertEquals(summary.getTotalContributed(), totalContributions);

	}

	@Test
	public void test_get_filtered_purchases() {
		this.cmdLoadTestDataSet(this.testMemberships);
		this.cmdLoadTestDataSet(this.testPurchases);

		SearchQuery q = SearchQueries
				.q(Purchase.Fields.STORE_ID).eq(Data.ID_5)
				.and(Purchase.Fields.PURCHASE_DATE).gt(Data.T_0.minusDays(6))
				.getSearchQuery();

		PageRequest request = PageRequest.with(0, 1000).search(q).build();
		Page<Purchase> purchases = this.repo.getByMembershipId(Data.ID_1, request);
		BigDecimal totalPurchases = purchases.getItems().stream().map(p -> p.getPurchaseAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal totalContributions = purchases.getItems().stream().map(p -> p.getContributionAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);

		PurchaseSummary summary = this.repo.getPurchaseSummaryByMembershipId(Data.ID_1, q);

		assertTrue(summary.getTotalPurchases() > 0);
		assertEquals(summary.getTotalPurchases().longValue(), purchases.getTotalItems());
		assertEquals(summary.getTotalAmount(), totalPurchases);
		assertEquals(summary.getTotalContributed(), totalContributions);
	}

}
