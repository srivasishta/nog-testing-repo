package com.ngl.micro.member.membership;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class LinkedCharityChoiceRepositoryITCase extends AbstractDatabaseITCase {

	private MembershipRepository membershipsRepo;
	private LinkedCharityChoices choicesRepo;

	private TestMemberships testMemberships;

	@Override
	@Before
	public void before() {
		super.before();
		this.membershipsRepo = new MembershipRepository(this.sqlContext);
		this.choicesRepo = new LinkedCharityChoices(this.sqlContext);
		this.testMemberships = new TestMemberships();
	}

	@Test
	public void test_add_choices() {
		Set<CharityChoice> choices = Collections.asSet(
				new CharityChoice(Data.ID_1, 40),
				new CharityChoice(Data.ID_3, 60));
		Membership membership = this.testMemberships.membership();
		membership.setCharityChoices(choices);
		membership = this.membershipsRepo.add(membership);

		assertEquals(choices, this.choicesRepo.getByMembershipId(Data.toLong(membership.getId())));
	}

	@Test
	public void test_get_previous_and_active_charity_choices() {
		Membership membership = this.testMemberships.membership();
		membership.setCharityChoices(new HashSet<>());
		membership = this.membershipsRepo.add(membership);

		ZonedDateTime previousDate = Data.T_MINUS_10d;
		ZonedDateTime currentDate = Data.T_0;
		Set<CharityChoice> previousChoices = Collections.asSet(new CharityChoice(Data.ID_1, 40), new CharityChoice(Data.ID_3, 60));
		Set<CharityChoice> currentChoices = Collections.asSet(new CharityChoice(Data.ID_2, 60), new CharityChoice(Data.ID_3, 40));
		Long membershipId = Data.toLong(membership.getId());
		this.choicesRepo.setMembershipCharityChoice(membershipId, previousChoices, previousDate);
		this.choicesRepo.setMembershipCharityChoice(membershipId, currentChoices, currentDate);

		assertTrue(this.choicesRepo.getByMembershipIdAt(membershipId, previousDate.minusDays(1)).isEmpty());
		assertEquals(previousChoices, this.choicesRepo.getByMembershipIdAt(membershipId, previousDate));
		assertEquals(previousChoices, this.choicesRepo.getByMembershipIdAt(membershipId, previousDate.plusDays(1L)));
		assertEquals(currentChoices, this.choicesRepo.getByMembershipIdAt(membershipId, currentDate));
		assertEquals(currentChoices, this.choicesRepo.getByMembershipIdAt(membershipId, currentDate.plusDays(1L)));
		assertEquals(currentChoices, this.choicesRepo.getByMembershipId(membershipId));
	}

}