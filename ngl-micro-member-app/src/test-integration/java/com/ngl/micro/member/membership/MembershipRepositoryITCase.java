package com.ngl.micro.member.membership;

import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_2;
import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_2_MEMBER_1_EXTERNAL_ID;
import static com.ngl.micro.member.test_data.TestMemberships.PARTNER_2_MEMBER_1_ID;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Country;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.Demographics.Gender;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class MembershipRepositoryITCase extends AbstractDatabaseITCase {

	private MembershipRepository repo;
	private StatusHistoryRepository statusHistory;
	private TestMemberships testMemberships;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new MembershipRepository(this.sqlContext);
		this.statusHistory = new StatusHistoryRepository(this.sqlContext);
		this.testMemberships = new TestMemberships();
	}

	@Test
	public void test_create_membership() {
		Membership reference = this.testMemberships.membership();
		Membership membership = this.repo.add(reference);

		assertNotNull(membership.getId());
		assertEquals(reference.getDemographics(), membership.getDemographics());
		assertEquals(reference.getCharityChoices(), membership.getCharityChoices());
	}

	@Test
	public void test_create_optional_membership() {
		Membership reference = this.testMemberships.membership();
		reference.getDemographics().setGeoLocation(null);
		reference.getDemographics().setBirthdate(null);
		reference.setCharityChoices(new HashSet<>());

		Membership membership = this.repo.add(reference);

		assertEquals(null, membership.getDemographics().getBirthdate());
		assertEquals(new GeoLocation(), membership.getDemographics().getGeoLocation());
		assertTrue(membership.getCharityChoices().isEmpty());
	}

	@Test
	public void test_get_membership() {
		Membership reference = this.testMemberships.membership();
		Membership created = this.repo.add(reference);
		Membership membership = this.repo.get(created.getId());

		assertNotNull(membership.getId());
		assertEquals(reference.getDemographics(), membership.getDemographics());
		assertEquals(reference.getCharityChoices(), membership.getCharityChoices());
	}

	@Test
	public void test_get_non_existing_membership() {
		UUID nonExisting = Data.toUUID(100L);
		assertNull(this.repo.get(nonExisting));
	}

	@Test
	public void test_list_memberships() {
		this.cmdLoadTestDataSet(this.testMemberships);

		Page<Membership> members = this.repo.get(PageRequest.with(3, 3).build());

		assertEquals(3, members.getPageNumber());
		assertEquals(10, members.getTotalItems());
		assertEquals(3, members.getPageSize());
		assertEquals(1, members.getNumberOfItems());
	}

	@Test
	public void test_test_search_by_status() {
		this.cmdLoadTestDataSet(this.testMemberships);

		Page<Membership> members = this.repo.get(PageRequest.with(0, 5).build()
				.filter(SearchQueries.q(Membership.Fields.STATUS).eq(MembershipStatus.INACTIVE)));

		assertEquals(4, members.getNumberOfItems());
		for (Membership member : members.getItems()) {
			Assert.assertTrue(member.getStatus().equals(MembershipStatus.INACTIVE));
		}
	}

	@Test
	public void test_search_by_demographics() {
		this.cmdLoadTestDataSet(this.testMemberships);

		Page<Membership> members = this.repo.get(PageRequest.with(0, 5).build()
				.filter(SearchQueries
						.q(Membership.Fields.DEMOGRAPHICS_COUNTRY).eq(Country.CA)
						.and(Membership.Fields.DEMOGRAPHICS_BIRTHDATE).eq(LocalDate.of(1981, 12, 1))
						.and(Membership.Fields.DEMOGRAPHICS_REGION).eq("AB")
						.and(Membership.Fields.DEMOGRAPHICS_GENDER).eq(Gender.FEMALE)));

		assertEquals(1, members.getNumberOfItems());

		members.getItems().forEach(m -> {
			Demographics demo = m.getDemographics();
			Assert.assertEquals(Country.CA, demo.getCountry());
			Assert.assertEquals("AB", demo.getRegion());
			Assert.assertEquals(Gender.FEMALE, demo.getGender());
		});
	}


	@Test
	public void test_search_by_charity() {
		this.cmdLoadTestDataSet(this.testMemberships);

		SearchQuery query = SearchQueries.q(Membership.Fields.CHARITY_CHOICES_CHARITY_ID).eq(Data.ID_2).getSearchQuery();
		Fields fields = new Fields();
		fields.include(Membership.Fields.ID);
		Page<Membership> memberships = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(memberships.hasItems());
		for (Membership m : memberships.getItems()) {
			Set<CharityChoice> charityChoices = this.repo.get(m.getId()).getCharityChoices();
			Assert.assertTrue(charityChoices.removeIf(x -> x.getCharityId().equals(Data.ID_2)));
		}
	}

	@Test
	@Ignore("Passes on MYSQL, Fails on H2")
	public void test_search_by_charities() {
		this.cmdLoadTestDataSet(this.testMemberships);

		List<UUID> charityFilter = Arrays.asList(Data.ID_1, Data.ID_5);

		SearchQuery query = SearchQueries.q(Membership.Fields.CHARITY_CHOICES_CHARITY_ID).in(charityFilter).getSearchQuery();
		Page<Membership> memberships = this.repo.get(PageRequest.with(0, 5).search(query).build());
		Assert.assertTrue(memberships.hasItems());
		for (Membership m : memberships.getItems()) {
			Set<CharityChoice> charityChoices = this.repo.get(m.getId()).getCharityChoices();
			Assert.assertTrue(charityChoices.removeIf(x -> x.getCharityId().equals(Data.ID_1))
					|| charityChoices.removeIf(x -> x.getCharityId().equals(Data.ID_5)));
		}
	}









	@Test
	public void test_update_membership() {
		String loadedReason = "loaded";
		String activatedReason = "activated";
		Membership base = this.testMemberships.membership();
		base.setStatus(MembershipStatus.INACTIVE);
		base.setStatusReason(loadedReason);
		base = this.repo.add(base);

		assertThat(base.getStatus(), is(MembershipStatus.INACTIVE));
		assertThat(base.getStatusReason(), is(loadedReason));

		base.setStatus(MembershipStatus.ACTIVE);
		base.setStatusReason(activatedReason);
		Demographics demographics = this.testMemberships.demographics(
				LocalDate.of(1980, 1, 1),
				Gender.FEMALE,
				Country.CA,
				"AB",
				GeoLocation.of(-33.0, 18.0));
		base.setDemographics(demographics);
		this.repo.update(base);

		Membership updated = this.repo.get(base.getId());
		assertThat(updated.getStatus(), is(MembershipStatus.ACTIVE));
		assertThat(base.getStatusReason(), is(activatedReason));
		assertThat(this.statusHistory.list(Data.toLong(base.getId())).size(), is(2));
		assertEquals(demographics, updated.getDemographics());
	}
	@Test
	public void test_update_membership_charity_choice() {
		Membership base = this.testMemberships.membership();
		base = this.repo.add(base);

		this.repo.update(base);

		Membership updated = this.repo.get(base.getId());
		assertEquals(base.getCharityChoices(), updated.getCharityChoices());

		Set<CharityChoice> updatedChoice = Collections.asSet(
				new CharityChoice(Data.ID_5, 20),
				new CharityChoice(Data.ID_7, 80));

		base.setCharityChoices(updatedChoice);
		this.repo.update(base);
		updated = this.repo.get(base.getId());
		assertEquals(updatedChoice, updated.getCharityChoices());

	}

	@Test
	public void test_update_optional_membership_fields() {
		Membership base = this.testMemberships.membership();
		base = this.repo.add(base);

		base.getDemographics().setGeoLocation(new GeoLocation());
		this.repo.update(base);

		Membership updated = this.repo.get(base.getId());
		assertEquals(base.getDemographics(), updated.getDemographics());
	}

	@Test
	public void test_update_membership_status_unchanged() {
		Membership base = this.testMemberships.membership();
		base.setStatus(MembershipStatus.INACTIVE);
		base = this.repo.add(base);

		assertThat(base.getStatus(), is(MembershipStatus.INACTIVE));

		base.setStatus(MembershipStatus.INACTIVE);
		this.repo.update(base);

		Membership updated = this.repo.get(base.getId());
		assertThat(updated.getStatus(), is(MembershipStatus.INACTIVE));
		assertThat(this.statusHistory.list(Data.toLong(base.getId())).size(), is(1));
	}

	@Test
	public void get_membership_by_identifier() {
		this.cmdLoadTestDataSet(this.testMemberships);
		assertNull(this.repo.getByExternalIdAt(PARTNER_2, "should be null", Data.T_0));

		UUID memberId = Data.toUUID(PARTNER_2_MEMBER_1_ID);
		String externalId = PARTNER_2_MEMBER_1_EXTERNAL_ID;
		assertEquals(
				this.repo.get(memberId),
				this.repo.getByExternalIdAt(PARTNER_2, externalId, Data.T_0));

		Membership previous = this.repo.getByExternalIdAt(PARTNER_2, externalId, Data.T_MINUS_1M);
		Membership current = this.repo.getByExternalIdAt(PARTNER_2, externalId, Data.T_0);
		assertTrue(!previous.equals(current));
	}

}
