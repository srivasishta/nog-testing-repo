package com.ngl.micro.member.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.dal.generated.jooq.tables.daos.JGenderDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JGender;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class GenderITCase extends AbstractDatabaseITCase {

	private JGenderDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JGenderDao(this.sqlContext);
	}

	@Test
	public void test_gender_mapping() throws Exception {
		JGender status;
		assertEquals("Expect equal status type count.", Gender.values().length, this.statusDao.count());
		for (Gender type : Gender.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
