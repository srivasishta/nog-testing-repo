package com.ngl.micro.member.membership;

import static com.ngl.micro.member.Membership.MembershipStatus.ACTIVE;
import static com.ngl.micro.member.Membership.MembershipStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private StatusHistoryRepository repo;
	private TestMemberships testMemberships;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new StatusHistoryRepository(this.sqlContext);
		this.testMemberships = new TestMemberships();
	}

	@Test
	public void test_get_membership_status() {
		assertEquals(null, this.repo.getByMembershipIdAt(1L, Data.T_0));

		this.cmdLoadTestDataSet(this.testMemberships);

		assertEquals(null, this.repo.getByMembershipIdAt(1L, Data.T_MINUS_1y));
		assertEquals(ACTIVE, this.repo.getByMembershipIdAt(1L, Data.T_MINUS_10M).getStatus());
		assertEquals(INACTIVE, this.repo.getByMembershipIdAt(1L, Data.T_MINUS_5M).getStatus());
		assertEquals(INACTIVE, this.repo.getByMembershipIdAt(1L, Data.T_MINUS_1M.minusSeconds(1)).getStatus());
		assertEquals(ACTIVE, this.repo.getByMembershipIdAt(1L, Data.T_MINUS_1M).getStatus());
		assertEquals(ACTIVE, this.repo.getByMembershipIdAt(1L, Data.T_0).getStatus());
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.testMemberships);

		Long memberId = TestMemberships.PARTNER_2_MEMBER_1_ID;

		MembershipStatusRecord status = this.repo.getByMembershipIdAt(memberId, Data.T_0);
		int records = this.repo.list(memberId).size();

		assertEquals(ACTIVE, status.getStatus());
		assertEquals(Data.REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setMembershipStatus(memberId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByMembershipIdAt(memberId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(records, this.repo.list(memberId).size());

		//Status reason update
		this.repo.setMembershipStatus(memberId, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByMembershipIdAt(memberId, ZonedDateTime.now());
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(memberId).size());

		//Status only
		this.repo.setMembershipStatus(memberId, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByMembershipIdAt(memberId, ZonedDateTime.now());
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(memberId).size());

		//Reason only
		this.repo.setMembershipStatus(memberId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByMembershipIdAt(memberId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(memberId).size());

		//Empty reason
		this.repo.setMembershipStatus(memberId, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getByMembershipIdAt(memberId, ZonedDateTime.now());
		assertEquals("", status.getStatusReason());
		assertEquals(++records, this.repo.list(memberId).size());
	}

}