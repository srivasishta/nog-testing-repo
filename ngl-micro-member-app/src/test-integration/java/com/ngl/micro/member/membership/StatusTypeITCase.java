package com.ngl.micro.member.membership;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JMembershipStatusTypeDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipStatusType;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StatusTypeITCase extends AbstractDatabaseITCase {

	private JMembershipStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		statusDao = new JMembershipStatusTypeDao(sqlContext);
	}

	@Test
	public void test_status_mapping() throws Exception {
		assertEquals(ResourceStatus.ACTIVE.name(), MembershipStatus.ACTIVE.name());
		assertEquals(ResourceStatus.INACTIVE.name(), MembershipStatus.INACTIVE.name());
		JMembershipStatusType status;
		assertEquals("Expect equal status type count.", ResourceStatus.values().length, statusDao.count());
		for (ResourceStatus type : ResourceStatus.values()) {
			status = statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
