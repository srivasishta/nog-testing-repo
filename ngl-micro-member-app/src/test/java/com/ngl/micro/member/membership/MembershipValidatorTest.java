package com.ngl.micro.member.membership;

import static com.ngl.middleware.util.Collections.asSet;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.test_data.TestMemberships;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

public class MembershipValidatorTest {

	private MembershipValidator validator;
	private TestMemberships testMemberships;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Before
	public void before() {
		this.validator = MembershipValidator.newInstance();
		this.testMemberships = new TestMemberships();
	}

	@Test
	public void test_validate_membership_resource() {
		this.validator.validate(this.testMemberships.membership(Data.STRING_1));
	}

	@Test
	public void test_fail_null_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("partnerId", ValidationErrorCode.INVALID)
				.expectValidationError("status", ValidationErrorCode.INVALID)
				.expectValidationError("statusReason", ValidationErrorCode.INVALID)
				.expectValidationError("externalId", ValidationErrorCode.INVALID)
				.expectValidationError("demographics", ValidationErrorCode.INVALID)
				.expectValidationError("charityChoices", ValidationErrorCode.INVALID);

		Membership member = new Membership();
		member.setStatusReason(null);
		member.setCharityChoices(null);
		this.validator.validate(member);
	}

	@Test
	public void test_validate_invalid_geo_max() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
		.expectStatus(ApplicationStatus.BAD_REQUEST)
		.expectValidationError("demographics.geoLocation.lat", ValidationErrorCode.INVALID)
		.expectValidationError("demographics.geoLocation.lon", ValidationErrorCode.INVALID);

		Membership membership = this.testMemberships.membership(Data.STRING_1);
		membership.getDemographics().setGeoLocation(GeoLocation.of(
				com.ngl.middleware.util.geo.GeoLocation.MAX_LAT_RAD + 0.1,
				com.ngl.middleware.util.geo.GeoLocation.MAX_LON_RAD + 0.1));
		this.validator.validate(membership);
	}

	@Test
	public void test_validate_invalid_geo_min() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
		.expectStatus(ApplicationStatus.BAD_REQUEST)
		.expectValidationError("demographics.geoLocation.lat", ValidationErrorCode.INVALID)
		.expectValidationError("demographics.geoLocation.lon", ValidationErrorCode.INVALID);

		Membership membership = this.testMemberships.membership(Data.STRING_1);
		membership.getDemographics().setGeoLocation(GeoLocation.of(
				com.ngl.middleware.util.geo.GeoLocation.MIN_LAT_RAD -0.1,
				com.ngl.middleware.util.geo.GeoLocation.MIN_LON_RAD -0.1));
		this.validator.validate(membership);
	}

	@Test
	public void test_validate_nullable_lat_lon() {
		Membership membership = this.testMemberships.membership(Data.STRING_1);
		membership.getDemographics().setGeoLocation(GeoLocation.of(null, null));
		this.validator.validate(membership);
	}

	@Test
	public void test_validate_empty_charity_choice() {
		Membership membership = this.testMemberships.membership(Data.STRING_1);
		this.validator.validate(membership);
	}

	@Test
	public void test_fail_invalid_charity_choice() {

		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
		.expectStatus(ApplicationStatus.BAD_REQUEST)
		.expectValidationError("charityChoices", ValidationErrorCode.INVALID, "Sum of all allocations must be 100. 111 provided.")
		.expectValidationError("charityChoices", ValidationErrorCode.INVALID, "Selected charities must be distinct.")
		.expectValidationError("charityChoices.allocation", ValidationErrorCode.INVALID, "must be greater than or equal to 1")
		.expectValidationError("charityChoices.allocation", ValidationErrorCode.INVALID, "must be less than or equal to 100");

		Membership member = this.testMemberships.membership();
		member.setCharityChoices(asSet(
			new CharityChoice(Data.ID_7, 0),
			new CharityChoice(Data.ID_7, 101),
			new CharityChoice(Data.ID_9, 10)));

		this.validator.validate(member);
	}

}