package com.ngl.micro.member.test_data;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.util.Collections;

public class TestAccessTokens {

	public static final TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);

	public static TestAccessToken partner_id_2_member_read() {
		return TestAccessToken.newBasic()
				.subjectClaim(TestMemberships.PARTNER_2)
				.clientIdClaim(TestMemberships.PARTNER_2)
				.scope(Collections.asSet(
						Scopes.MEMBER_READ_SCOPE));
	}

	public static TestAccessToken partner_id_2_purchase_read() {
		return TestAccessToken.newBasic()
				.subjectClaim(TestMemberships.PARTNER_2)
				.clientIdClaim(TestMemberships.PARTNER_2)
				.scope(Collections.asSet(
						Scopes.PURCHASE_READ_SCOPE));
	}

	public static TestAccessToken partner_id_2_full_service_access() {
		return TestAccessToken.newBasic()
				.subjectClaim(TestMemberships.PARTNER_2)
				.clientIdClaim(TestMemberships.PARTNER_2)
				.scope(Collections.asSet(
						Scopes.MEMBER_READ_SCOPE,
						Scopes.MEMBER_WRITE_SCOPE,
						Scopes.PURCHASE_READ_SCOPE));
	}

	public static TestAccessToken partner_id_3_member_read() {
		return TestAccessToken.newBasic()
				.subjectClaim(TestMemberships.PARTNER_3)
				.clientIdClaim(TestMemberships.PARTNER_3)
				.scope(Collections.asSet(
						Scopes.MEMBER_READ_SCOPE));
	}

	public static TestAccessToken partner_id_3_purchase_read() {
		return TestAccessToken.newBasic()
				.subjectClaim(TestMemberships.PARTNER_3)
				.clientIdClaim(TestMemberships.PARTNER_3)
				.scope(Collections.asSet(
						Scopes.PURCHASE_READ_SCOPE));
	}

}
