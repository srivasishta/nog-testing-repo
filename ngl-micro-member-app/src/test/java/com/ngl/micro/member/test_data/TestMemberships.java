package com.ngl.micro.member.test_data;

import static com.ngl.micro.member.Demographics.Gender.FEMALE;
import static com.ngl.micro.member.Demographics.Gender.MALE;
import static com.ngl.micro.member.Demographics.Gender.UNSPECIFIED;
import static com.ngl.micro.shared.contracts.ResourceStatus.ACTIVE;
import static com.ngl.micro.shared.contracts.ResourceStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_CREATED;
import static com.ngl.middleware.test.data.Data.REASON_DEACTIVATED;
import static java.time.LocalDate.of;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Country;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.Demographics.Gender;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Membership.MembershipStatus;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JCharityAllocationDao;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JCharityChoiceDao;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JMembershipDao;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JMembershipDemographicsDao;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JMembershipStatusHistoryDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JCharityAllocation;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JCharityChoice;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembership;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipDemographics;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JMembershipStatusHistory;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

/**
 * Membership test data.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
public class TestMemberships implements JooqTestDataSet {

	public static final UUID PARTNER_2 = Data.ID_2;
	public static final UUID PARTNER_3 = Data.ID_3;

	public static final Long PARTNER_2_PK = 2L;
	public static final Long PARTNER_3_PK = 3L;

	public static final UUID PARTNER_2_MEMBER_1 = Data.ID_1;
	public static final UUID PARTNER_3_MEMBER_5 = Data.ID_5;

	public static final Long PARTNER_2_MEMBER_1_ID = 1L;
	public static final Long PARTNER_3_MEMBER_5_ID = 5L;

	public static final String PARTNER_2_MEMBER_1_EXTERNAL_ID = "2_1";

	public Membership membership() {
		return this.membership(Data.STRING_3);
	}

	public Membership membership(final String identifier) {
		Membership membership = new Membership();
		membership.setId(Data.ID_1);
		membership.setStatus(MembershipStatus.ACTIVE);
		membership.setStatusReason(Data.REASON_CREATED);
		membership.setPartnerId(Data.toUUID(2L));
		membership.setExternalId(identifier);
		membership.setDemographics(this.demographics());
		membership.setCharityChoices(this.charityChoice());
		return membership;
	}

	public Demographics demographics() {
		return this.demographics(
				LocalDate.of(1980,1,2),
				Gender.MALE,
				Country.CA,
				"AB",
				GeoLocation.of(1.123, -0.123));
	}

	public Demographics demographics(LocalDate dob, Gender gender, Country country, String region, GeoLocation geoLocation) {
		Demographics demo = new Demographics();
		demo.setBirthdate(dob);
		demo.setGender(gender);
		demo.setCountry(country);
		demo.setRegion(region);
		demo.setGeoLocation(geoLocation);
		return demo;
	}

	public Set<CharityChoice> charityChoice() {
		return Collections.asSet(
			new CharityChoice(Data.ID_7, 10),
			new CharityChoice(Data.ID_8, 20),
			new CharityChoice(Data.ID_9, 70));
	}

	@Override
	public void loadData(DSLContext sql) {
			JMembershipDao memberDao = new JMembershipDao(sql.configuration());
			JMembershipDemographicsDao demographicsDao = new JMembershipDemographicsDao(sql.configuration());
			JMembershipStatusHistoryDao statusHistory = new JMembershipStatusHistoryDao(sql.configuration());
			JCharityChoiceDao choiceDao = new JCharityChoiceDao(sql.configuration());
			JCharityAllocationDao allocationDao = new JCharityAllocationDao(sql.configuration());

			memberDao.insert(
					this.membership(PARTNER_2_MEMBER_1_ID,  PARTNER_2, PARTNER_2_MEMBER_1_EXTERNAL_ID),
					this.membership(2L,                     PARTNER_2, "2_2"),
					this.membership(3L,                     PARTNER_2, "2_3"),
					this.membership(4L,                     PARTNER_2, "2_4"),
					this.membership(PARTNER_3_MEMBER_5_ID,  PARTNER_3, "3_5"),
					this.membership(6L,                     PARTNER_3, "3_6"),
					this.membership(7L,                     PARTNER_3, "3_7"),
					this.membership(8L,                     PARTNER_3, "3_8"),
					this.membership(9L,                     PARTNER_3, "3_9"),
					this.membership(10L,                    PARTNER_3, "3_10"));

			demographicsDao.insert(
					this.demographics(PARTNER_2_MEMBER_1_ID,  of(1980, 12, 1), MALE       , Country.CA, "AB", 53.1, -113.1),
					this.demographics(2L,                     of(1981, 12, 1), FEMALE     , Country.CA, "AB", 53.2, -113.2),
					this.demographics(3L,                     of(1982, 12, 1), UNSPECIFIED, Country.CA, "AB", 53.3, -113.3),
					this.demographics(4L,                     of(1983, 12, 1), UNSPECIFIED, Country.CA, "AB", 53.4, -113.4),
					this.demographics(5L,                     of(1984, 12, 1), UNSPECIFIED, Country.US, "CA", 37.5, -122.5),
					this.demographics(6L,                     of(1985, 12, 1), MALE       , Country.US, "CA", 37.6, -122.6),
					this.demographics(7L,                     of(1986, 12, 1), FEMALE     , Country.US, "CA", 37.7, -122.7),
					this.demographics(8L,                     of(1987, 12, 1), UNSPECIFIED, Country.US, "CA", 37.8, -122.8),
					this.demographics(9L,                     of(1988, 12, 1), UNSPECIFIED, Country.US, "CA", 37.9, -122.9),
					this.demographics(10L,                    of(1989, 12, 1), UNSPECIFIED, Country.US, "CA", null, null));

			statusHistory.insert(
					this.status(PARTNER_2_MEMBER_1_ID,  ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_10M, Data.T_MINUS_5M),
					this.status(PARTNER_2_MEMBER_1_ID,  INACTIVE.getId(), REASON_DEACTIVATED, Data.T_MINUS_5M,  Data.T_MINUS_1M),
					this.status(PARTNER_2_MEMBER_1_ID,  ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null),
					this.status(2L,                     INACTIVE.getId(), REASON_CREATED,     Data.T_MINUS_1M,  null),
					this.status(3L,                     ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null),
					this.status(4L,                     INACTIVE.getId(), REASON_CREATED,     Data.T_MINUS_1M,  null),
					this.status(5L,                     INACTIVE.getId(), REASON_CREATED,     Data.T_MINUS_1M,  null),
					this.status(6L,                     INACTIVE.getId(), REASON_CREATED,     Data.T_MINUS_1M,  null),
					this.status(7L,                     ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null),
					this.status(8L,                     ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null),
					this.status(9L,                     ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null),
					this.status(10L,                    ACTIVE.getId(),   REASON_ACTIVATED,   Data.T_MINUS_1M,  null));

			choiceDao.insert(
					this.choice(Data.PK_1, 1L, Data.T_MINUS_10M, Data.T_MINUS_1M),
					this.choice(Data.PK_2, 1L, Data.T_MINUS_1M,  Data.T_MINUS_10d),
					this.choice(Data.PK_3, 1L, Data.T_MINUS_10d, Data.T_MINUS_1d),
					this.choice(Data.PK_4, 1L, Data.T_MINUS_1d,  null),
					this.choice(Data.PK_5, 2L, Data.T_MINUS_1d,  null));

			allocationDao.insert(
					this.allocation(Data.FK_1, Data.ID_5, 100),
					this.allocation(Data.FK_2, Data.ID_4, 60),
					this.allocation(Data.FK_2, Data.ID_5, 40),
					this.allocation(Data.FK_3, Data.ID_5, 15),
					this.allocation(Data.FK_3, Data.ID_6, 25),
					this.allocation(Data.FK_3, Data.ID_4, 60),
					this.allocation(Data.FK_4, Data.ID_4, 25),
					this.allocation(Data.FK_4, Data.ID_6, 25),
					this.allocation(Data.FK_4, Data.ID_5, 20),
					this.allocation(Data.FK_4, Data.ID_2, 30),
					this.allocation(Data.FK_5, Data.ID_1, 100));
	}

	private JMembership membership(Long id, UUID partnerId, String externalId) {
		JMembership member = new JMembership();
		member.setId(id);
		member.setResourceId(Data.toUUID(id));
		member.setPartnerId(partnerId);
		member.setExternalId(externalId);
		return member;
	}

	private JMembershipDemographics demographics(Long membershipId, LocalDate birthdate, Gender gender, Country country, String region, Double lat, Double lon) {
		JMembershipDemographics demo = new JMembershipDemographics();
		demo.setMembershipId(membershipId);
		demo.setBirthdate(Date.valueOf(birthdate));
		demo.setGender(gender.name());
		demo.setCountry(country.name());
		demo.setRegion(region);
		demo.setLat(lat);
		demo.setLon(lon);
		return demo;
	}

	private JMembershipStatusHistory status(Long membershipId, Long statusId, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JMembershipStatusHistory status = new JMembershipStatusHistory();
		status.setMembershipId(membershipId);
		status.setStatusId(statusId);
		status.setStatusReason(reason);
		status.setStartDate(startDate);
		status.setEndDate(endDate);
		return status;
	}

	private JCharityChoice choice(long id, long membershipId, ZonedDateTime startDate, ZonedDateTime endDate) {
		JCharityChoice choice = new JCharityChoice();
		choice.setMembershipId(membershipId);
		choice.setStartDate(startDate);
		choice.setEndDate(endDate);
		return choice;
	}

	private JCharityAllocation allocation(long choiceId, UUID charityId, int allocation) {
		JCharityAllocation charityAllocation = new JCharityAllocation();
		charityAllocation.setCharityChoiceId(choiceId);
		charityAllocation.setCharityId(charityId);
		charityAllocation.setAllocation(allocation);
		return charityAllocation;
	}

}
