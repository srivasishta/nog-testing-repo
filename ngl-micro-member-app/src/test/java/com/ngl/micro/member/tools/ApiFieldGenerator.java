package com.ngl.micro.member.tools;

import java.util.Arrays;
import java.util.Collections;

import com.ngl.micro.member.CharityChoice;
import com.ngl.micro.member.Demographics;
import com.ngl.micro.member.GeoLocation;
import com.ngl.micro.member.Membership;
import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.PurchaseSummary;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields.
 *
 * @author Willy du Preez
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Arrays.asList(
				Demographics.class,
				GeoLocation.class,
				CharityChoice.class))
 		.generate(Membership.class);

		new ApiFieldsGenerator(Collections.emptyList()).generate(Purchase.class);
		new ApiFieldsGenerator(Collections.emptyList()).generate(PurchaseSummary.class);
	}

}
