package com.ngl.micro.member.tools;

import com.ngl.middleware.database.DatabaseProperties;
import com.ngl.middleware.database.mapping.DatabaseMappingGenerator;
import com.ngl.middleware.database.mapping.DatabaseMappingGeneratorProperties;

/**
 * Schema generator for transaction service tables.
 *
 * @author Willy du Preez
 */
public class SchemaGenerator {

	public static void main(String[] args) {
		DatabaseMappingGeneratorProperties mappingProps = new DatabaseMappingGeneratorProperties();
		mappingProps.setGenerateWithClassPrefix(true);
		mappingProps.setGenerateTargetPackage("com.ngl.micro.member.dal.generated.jooq");
		mappingProps.setGenerateWithExcludes("");
		mappingProps.setGenerateWithIncludes(".*");

		DatabaseProperties dbProps = new DatabaseProperties();
		dbProps.setUsername("root");
		dbProps.setPassword("Admin123");
		dbProps.setSchema("ngl_micro_member");

		new DatabaseMappingGenerator().generateFromSchema(mappingProps, dbProps);
	}

}
