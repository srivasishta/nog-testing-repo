package com.ngl.micro.member.test_data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.jooq.DSLContext;

import com.ngl.micro.member.Purchase;
import com.ngl.micro.member.dal.generated.jooq.tables.daos.JPurchaseDao;
import com.ngl.micro.member.dal.generated.jooq.tables.pojos.JPurchase;
import com.ngl.middleware.dal.DataAccessException;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Time;

/**
 * Purchases test data.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class TestPurchases implements JooqTestDataSet {

	public Purchase purchase() {
		return this.purchase(345L);
	}

	public Purchase purchase(Long purchaseId) {
		Purchase purchase = new Purchase();
		purchase.setId(Data.toUUID(purchaseId));
		purchase.setStoreId(Data.toUUID(54L));
		purchase.setBusinessId(Data.toUUID(55L));
		purchase.setPurchaseDate(Time.utcNow().minusDays(2));
		purchase.setPurchaseAmount(new BigDecimal("23.50000"));
		purchase.setContributionAmount(new BigDecimal("1.15250"));
		return purchase;
	}

	@Override
	public void loadData(DSLContext sql) {
		try {
			JPurchaseDao purchaseDao = new JPurchaseDao(sql.configuration());

			purchaseDao.insert(
					this.purchase(1L,  1L, 5L, Data.T_0.minusDays(9), "14.71" , "1.471"),
					this.purchase(2L,  2L, 5L, Data.T_0.minusDays(8), "25.45" , "2.545"),
					this.purchase(3L,  3L, 5L, Data.T_0.minusDays(7), "4.57"  , "0.457"),
					this.purchase(4L,  4L, 5L, Data.T_0.minusDays(6), "1.61"  , "0.161"),
					this.purchase(5L,  1L, 5L, Data.T_0.minusDays(5), "280.52", "28.052"),
					this.purchase(6L,  1L, 4L, Data.T_0.minusDays(4), "2.99"  , "0.299"),
					this.purchase(7L,  1L, 5L, Data.T_0.minusDays(3), "109.54", "10.954"),
					this.purchase(8L,  6L, 6L, Data.T_0.minusDays(2), "13.14" , "1.314"),
					this.purchase(9L,  7L, 6L, Data.T_0.minusDays(1), "49.52" , "4.952"),
					this.purchase(10L, 8L, 9L, Data.T_0				, "12.41" , "1.241"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException("Failed to insert purchase test data", e);
		}
	}

	private JPurchase purchase(Long id,
			Long membershipId, Long storeId,
			ZonedDateTime purchaseDate, String purchaseAmount, String donationAmount) {

		JPurchase purchase = new JPurchase();
		purchase.setId(id);
		purchase.setContributionDocumentId(Data.toUUID(id));
		purchase.setPurchaseDate(purchaseDate);
		purchase.setPurchaseAmount(new BigDecimal(purchaseAmount));
		purchase.setContributionAmount(new BigDecimal(donationAmount));
		purchase.setMembershipId(Data.toUUID(membershipId));
		purchase.setStoreId(Data.toUUID(storeId));
		purchase.setBusinessId(Data.toUUID(storeId + 1));
		return purchase;
	}

}