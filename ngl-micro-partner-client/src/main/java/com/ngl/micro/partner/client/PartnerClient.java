package com.ngl.micro.partner.client;

import java.util.UUID;

import com.ngl.micro.partner.partner.Partner;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Methods;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.hal.TypedResource;

/**
 *  The partner client for system and partner management.
 */
public class PartnerClient extends AbstractRestClient {

	public class PartnerManagementResource extends ResourceTemplate<Partner, UUID> {

		private PartnerManagementResource() {
			super(Partner.class, "partners/");
		}
	}

	public class ClientManagementResource extends ResourceTemplate<Client, UUID> {
		private ClientManagementResource() {
			super(Client.class, "clients/");
		}

		public TypedResource<Client> secret(UUID clientId) {
			Request request = PartnerClient.this.http.request(PartnerClient.this.serviceUrl + "clients/" + clientId + "/secret")
					.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
					.method(Methods.POST);
			return PartnerClient.this.executeRequest(Client.class, request);
		}
	}

	public class ClientResource {
		private String clientPath;
		private String secretPath;

		private ClientResource() {
			this.clientPath = PartnerClient.this.serviceUrl + "client";
			this.secretPath = PartnerClient.this.serviceUrl + "client/secret";
		}

		public TypedResource<Client> get() {
			Request request = PartnerClient.this.http.request(this.clientPath);
			return PartnerClient.this.executeRequest(Client.class, request);
		}

		public TypedResource<Client> secret(String secret) {
			Request request = PartnerClient.this.http.request(this.secretPath)
					.method(Methods.POST)
					.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
					.body(secret);
			return PartnerClient.this.executeRequest(Client.class, request);
		}
	}

	private PartnerManagementResource partnerManagementResource;
	private ClientManagementResource clientManagementResource;
	private ClientResource clientResource;

	public PartnerClient(RestClientConfig config) {
		super(config);
		this.partnerManagementResource = new PartnerManagementResource();
		this.clientManagementResource = new ClientManagementResource();
		this.clientResource = new ClientResource();
	}

	public PartnerManagementResource getPartnerManagementResource() {
		return this.partnerManagementResource;
	}

	public ClientManagementResource getClientManagementResource() {
		return this.clientManagementResource;
	}

	public ClientResource getClientResource() {
		return this.clientResource;
	}

}