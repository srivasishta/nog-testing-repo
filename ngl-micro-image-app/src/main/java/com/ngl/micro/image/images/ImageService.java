package com.ngl.micro.image.images;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ImageMetadata;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.integration.batch.AsyncPipeline;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Streams;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The image service servicing the REST end point.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackFor = Exception.class)
public class ImageService {

	private static final Logger log = LoggerFactory.getLogger(ImageService.class);

	private ImageJobRepository jobs;
	private ImageRepository images;
	private ImageJobValidator validator;

	private AsyncPipeline<ImageJobContext> pipeline;

	private PlatformTransactionManager txManager;
	private DefaultTransactionDefinition txDefinition;

	public ImageService(
			ImageJobRepository imageJobRepository,
			ImageRepository imageDataRepository,
			ImageJobValidator imageValidator,
			AsyncPipeline<ImageJobContext> pipeline,
			PlatformTransactionManager txManager) {

		Assert.notNull(imageJobRepository);
		Assert.notNull(imageDataRepository);
		Assert.notNull(imageValidator);
		Assert.notNull(pipeline);
		Assert.notNull(txManager);

		this.jobs = imageJobRepository;
		this.images = imageDataRepository;
		this.validator = imageValidator;

		this.pipeline = pipeline;

		this.txManager = txManager;
		this.txDefinition = new DefaultTransactionDefinition();
	}

	public ImageJob getImageProcessingJob(UUID id) {
		ImageJob job = this.jobs.get(id);
		UUID partnerId = Subjects.getSubject();
		if (job == null || !ApiPolicy.isAccessibleByPartner(partnerId, job.getPartnerId())) {
			throw ApplicationError.resourceNotFound()
					.setTitle("Image Job Not Found")
					.setDetail("No iamge job found for the ID: " + id)
					.asException();
		}
		return job;
	}

	public Page<ImageJob> getImageProcessingJob(PageRequest pageRequest) {
		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			pageRequest.filter(SearchQueries.and(ImageJob.Fields.PARTNER_ID).eq(partnerId));
		}
		return this.jobs.get(pageRequest);
	}

	public ImageJob uploadImage(InputStream image, ImageJob upload) throws IOException {
		this.validator.validate(upload);
		this.preAuthorizeUpload(upload.getPartnerId());

		UUID id = UUIDS.type4Uuid();
		ImageJob job;

		org.springframework.transaction.TransactionStatus status = this.txManager.getTransaction(this.txDefinition);
		try {
			try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				Streams.copyLarge(image, out);
				this.images.addImage(id, ImageSize.ORIGINAL, new ImageMetadata(), out.toByteArray());
			}
			job = new ImageJob();
			job.setId(id);
			job.setResourceId(upload.getResourceId());
			job.setPartnerId(upload.getPartnerId());
			job.setResourceType(upload.getResourceType());
			job.setProcessingStatus(ProcessingStatus.PENDING.name());
			job.setProcessingStatusReason(ProcessingStatusReason.PENDING.name());
			job.setCreatedDate(Time.utcNow());
			job = this.jobs.add(job);
			log.debug("Created image processing job: id={}", job.getId());
		} catch (Exception e) {
			log.error("Failed to create image processing job.", e);
			this.txManager.rollback(status);
			throw e;
		}
		this.txManager.commit(status);

		this.pipeline.submitSafely(new ImageJobContext(this.jobs, job));

		return job;
	}

	private void preAuthorizeUpload(UUID partnerId) {
		if (!ApiPolicy.isAccessibleByPartner(Subjects.getSubject(), partnerId)) {
			throw ApplicationError.validationError()
					.setTitle("Validation Error")
					.setDetail("Invalid partner ID specified.")
					.asException();
		}
	}

}
