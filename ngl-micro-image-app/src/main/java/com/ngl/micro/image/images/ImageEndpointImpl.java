package com.ngl.micro.image.images;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ResourceType;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Strings;

/**
 * Image endpoint implementation.
 *
 * @author Willy du Preez
 *
 */
public class ImageEndpointImpl implements ImageEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private ImageService images;

	public ImageEndpointImpl(RequestProcessor processor, ImageService images) {
		this.processor = processor;
		this.images = images;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		ImageJob job = this.images.getImageProcessingJob(id);
		Resource resource = HAL.resource(job)
				.fieldFilter(fields)
				.build();
		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<ImageJob> page = this.images.getImageProcessingJob(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response upload(HttpServletRequest request) throws ServletException, IOException {
		ImageJob job = new ImageJob();
		String partnerId = request.getParameter("partnerId");
		if (Strings.isNullOrWhitespace(partnerId)) {
			job.setPartnerId(Subjects.getSubject());
		}
		else {
			job.setPartnerId(UUID.fromString(partnerId));
		}
		job.setResourceId(UUID.fromString(request.getParameter("resourceId")));
		job.setResourceType(ResourceType.valueOf(request.getParameter("resourceType")));
	    Part filePart = request.getPart("image");
	    try (InputStream image = filePart.getInputStream()) {
	    	job = this.images.uploadImage(image, job);
	    	return Response.ok(job).build();
	    }
	}

	@Override
	public Response status(UUID id) {
		ImageJob job = new ImageJob();
		job.setId(id);
		return Response.ok(job).build();
	}

}
