/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.image.dal.generated.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JImageSize implements Serializable {

	private static final long serialVersionUID = 670451777;

	private Long   id;
	private String size;

	public JImageSize() {}

	public JImageSize(JImageSize value) {
		this.id = value.id;
		this.size = value.size;
	}

	public JImageSize(
		Long   id,
		String size
	) {
		this.id = id;
		this.size = size;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("JImageSize (");

		sb.append(id);
		sb.append(", ").append(size);

		sb.append(")");
		return sb.toString();
	}
}
