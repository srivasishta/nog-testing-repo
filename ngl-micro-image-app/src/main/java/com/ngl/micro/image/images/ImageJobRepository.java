package com.ngl.micro.image.images;

import static com.ngl.micro.image.dal.generated.jooq.Tables.IMAGE_JOB;
import static com.ngl.micro.image.dal.generated.jooq.Tables.PROCESSING_STATUS;
import static com.ngl.micro.image.dal.generated.jooq.Tables.PROCESSING_STATUS_REASON;
import static com.ngl.micro.image.dal.generated.jooq.Tables.RESOURCE_LOOKUP_TYPE;

import java.util.UUID;

import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ResourceType;
import com.ngl.micro.image.dal.generated.jooq.tables.daos.JImageJobDao;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JImageJob;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.util.Assert;

/**
 * A repository for managing {@link ImageJob}s.
 *
 * @author Willy du Preez
 *
 */
public class ImageJobRepository implements PagingRepository<ImageJob, UUID> {

	private DAL dal;
	private JImageJobDao dao;
	private JooqQuery query;
	private TableOnConditionStep<?> tables;
	private FieldMap<String> fieldMap;
	private ImageRepository images;

	public ImageJobRepository(DAL dal) {
		Assert.notNull(dal);

		this.dal = dal;
		this.dao = new JImageJobDao(this.dal.config());
		this.images = new ImageRepository(this.dal);

		this.fieldMap = new FieldMap<String>()
				.bind(ImageJob.Fields.ID, new MappedField(IMAGE_JOB.RESOURCE_ID).searchable(true))
				.bind(ImageJob.Fields.PARTNER_ID, new MappedField(IMAGE_JOB.PARTNER_ID).searchable(true))
				.bind(ImageJob.Fields.PROCESSING_STATUS, new MappedField(PROCESSING_STATUS.STATUS).searchable(true))
				.bind(ImageJob.Fields.PROCESSING_STATUS_REASON, new MappedField(PROCESSING_STATUS_REASON.REASON).searchable(true))
				.bind(ImageJob.Fields.RESOURCE_ID, IMAGE_JOB.RESOURCE_LOOKUP_ID)
				.bind(ImageJob.Fields.CREATED_DATE,  new MappedField(IMAGE_JOB.CREATED_DATE).searchable(true).sortable(true))
				.bind(ImageJob.Fields.RESOURCE_TYPE, new MappedField(RESOURCE_LOOKUP_TYPE.TYPE).searchable(true));

		this.tables = IMAGE_JOB
				.join(PROCESSING_STATUS).on(PROCESSING_STATUS.ID.eq(IMAGE_JOB.PROCESSING_STATUS_ID))
				.join(PROCESSING_STATUS_REASON).on(PROCESSING_STATUS_REASON.ID.eq(IMAGE_JOB.PROCESSING_STATUS_REASON_ID))
				.join(RESOURCE_LOOKUP_TYPE).on(RESOURCE_LOOKUP_TYPE.ID.eq(IMAGE_JOB.RESOURCE_LOOKUP_TYPE_ID));

		this.query = JooqQuery
				.builder(this.dal, this.fieldMap)
				.from(this.tables)
				.build();
	}

	@Override
	public ImageJob add(ImageJob item) {
		Assert.notNull(item);
		Assert.state(item.getId() != null);

		JImageJob record = new JImageJob();
		record.setResourceId(item.getId());
		record.setPartnerId(item.getPartnerId());
		record.setResourceLookupId(item.getResourceId());
		record.setCreatedDate(item.getCreatedDate());
		record.setResourceLookupTypeId(ResourceLookupType.forEnum(item.getResourceType()).getId());
		record.setProcessingStatusId(ProcessingStatus.valueOf(item.getProcessingStatus()).getId());
		record.setProcessingStatusReasonId(ProcessingStatusReason.valueOf(item.getProcessingStatusReason()).getId());
		this.dao.insert(record);

		return this.get(item.getId());
	}

	@Override
	public ImageJob get(UUID id) {
		Assert.notNull(id);

		Record record = this.query.getRecordWhere(IMAGE_JOB.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			ImageJob item = this.mapRecord(record);
			item.setOriginal(this.images.getImageMetadataBySize(id, ImageSize.ORIGINAL));
			item.setThumbnail(this.images.getImageMetadataBySize(id, ImageSize.THUMBNAIL));
			item.setMedium(this.images.getImageMetadataBySize(id, ImageSize.MEDIUM));
			item.setLarge(this.images.getImageMetadataBySize(id, ImageSize.LARGE));
			return item;
		}
	}

	@Override
	public Page<ImageJob> get(PageRequest pageRequest) {
		return this.query.getPage(pageRequest, this::mapRecord);
	}

	private ImageJob mapRecord(Record record) {
		ImageJob item = new ImageJob();
		item.setId(record.getValue(IMAGE_JOB.RESOURCE_ID));
		item.setPartnerId(record.getValue(IMAGE_JOB.PARTNER_ID));
		item.setResourceId(record.getValue(IMAGE_JOB.RESOURCE_LOOKUP_ID));
		item.setCreatedDate(record.getValue(IMAGE_JOB.CREATED_DATE));
		item.setResourceType(ResourceType.valueOf(record.getValue(RESOURCE_LOOKUP_TYPE.TYPE)));
		item.setProcessingStatus(record.getValue(PROCESSING_STATUS.STATUS));
		item.setProcessingStatusReason(record.getValue(PROCESSING_STATUS_REASON.REASON));
		return item;
	}

	@Override
	public void update(ImageJob item) {
		Assert.notNull(item);
		Assert.state(item.getId() != null);

		JImageJob record = this.dao.fetchOneByJResourceId(item.getId());
		record.setProcessingStatusId(ProcessingStatus.valueOf(item.getProcessingStatus()).getId());
		record.setProcessingStatusReasonId(ProcessingStatusReason.valueOf(item.getProcessingStatusReason()).getId());
		this.dao.update(record);
	}

	@Override
	public long size() {
		return this.dao.count();
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException();
	}

}
