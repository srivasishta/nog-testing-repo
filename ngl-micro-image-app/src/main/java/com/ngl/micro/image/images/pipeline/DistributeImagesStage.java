package com.ngl.micro.image.images.pipeline;

import java.util.UUID;

import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ImageMetadata;
import com.ngl.micro.image.distribute.ImageDistributionCommand;
import com.ngl.micro.image.distribute.ImageDistributionNetwork;
import com.ngl.micro.image.distribute.ImageDistributionRequest;
import com.ngl.micro.image.distribute.ImageDistributionResponse;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ImageRepository;
import com.ngl.micro.image.images.ImageSize;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.middleware.integration.breaker.HystrixTrap;
import com.ngl.middleware.util.Strings;

/**
 * A stage in the pipeline that distributes the images to the configured CDN.
 *
 * @author Willy du Preez
 *
 */
public class DistributeImagesStage extends AbstractStage {

	private HystrixTrap trap;
	private Setter imageDistributionConfig;
	private ImageDistributionNetwork cdn;
	private ImageRepository data;

	public DistributeImagesStage(
			HystrixTrap trap,
			ImageRepository data,
			Setter distributionBreaker,
			ImageDistributionNetwork cdn) {

		Assert.notNull(trap);
		Assert.notNull(data);
		Assert.notNull(distributionBreaker);
		Assert.notNull(cdn);

		this.trap = trap;
		this.imageDistributionConfig = distributionBreaker;
		this.cdn = cdn;
		this.data = data;
	}

	@Override
	public void process(ImageJobContext context) throws InterruptedException {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_IMAGE_DISTRIBUTION)) {
			this.distributeImages(context);
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_RESOURCE_UPDATE);
		}
	}

	private void distributeImages(ImageJobContext context) throws InterruptedException {
		ImageJob job = context.getJob();

		if (Strings.isNullOrWhitespace(job.getOriginal().getUrl())) {
			String url = this.distributeImage(
					job.getId(), job.getOriginal(), this.data.getImageDataBySize(job.getId(), ImageSize.ORIGINAL));
			ImageMetadata metadata = this.data.getImageMetadataBySize(job.getId(), ImageSize.ORIGINAL);
			metadata.setUrl(url);
			this.data.updateImageMetadata(job.getId(), ImageSize.ORIGINAL, metadata);
		}

		if (Strings.isNullOrWhitespace(job.getThumbnail().getUrl())) {
			String url = this.distributeImage(
					job.getId(), job.getThumbnail(), this.data.getImageDataBySize(job.getId(), ImageSize.THUMBNAIL));
			ImageMetadata metadata = this.data.getImageMetadataBySize(job.getId(), ImageSize.THUMBNAIL);
			metadata.setUrl(url);
			this.data.updateImageMetadata(job.getId(), ImageSize.THUMBNAIL, metadata);
		}

		if (Strings.isNullOrWhitespace(job.getMedium().getUrl())) {
			String url = this.distributeImage(
					job.getId(), job.getMedium(), this.data.getImageDataBySize(job.getId(), ImageSize.MEDIUM));
			ImageMetadata metadata = this.data.getImageMetadataBySize(job.getId(), ImageSize.MEDIUM);
			metadata.setUrl(url);
			this.data.updateImageMetadata(job.getId(), ImageSize.MEDIUM, metadata);
		}

		if (Strings.isNullOrWhitespace(job.getLarge().getUrl())) {
			String url = this.distributeImage(
					job.getId(), job.getLarge(), this.data.getImageDataBySize(job.getId(), ImageSize.LARGE));
			ImageMetadata metadata = this.data.getImageMetadataBySize(job.getId(), ImageSize.LARGE);
			metadata.setUrl(url);
			this.data.updateImageMetadata(job.getId(), ImageSize.LARGE, metadata);
		}

		context.refreshJob();
	}

	private String distributeImage(UUID jobId, ImageMetadata metadata, byte[] data) throws InterruptedException {
		ImageDistributionRequest request = new ImageDistributionRequest();
		request.setImageJobId(jobId);
		request.setMetadata(metadata);
		request.setData(data);
		ImageDistributionResponse response = this.trap.funnel(() -> new ImageDistributionCommand(
				this.imageDistributionConfig,
				this.cdn,
				request));
		return response.getImageUrl();
	}

	@Override
	public String getName() {
		return "[distribute-images]";
	}

}
