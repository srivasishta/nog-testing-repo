/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.image.dal.generated.jooq;


import com.ngl.micro.image.dal.generated.jooq.tables.JAccessTokenRevokedEvent;
import com.ngl.micro.image.dal.generated.jooq.tables.JImageData;
import com.ngl.micro.image.dal.generated.jooq.tables.JImageJob;
import com.ngl.micro.image.dal.generated.jooq.tables.JImageMetadata;
import com.ngl.micro.image.dal.generated.jooq.tables.JImageSize;
import com.ngl.micro.image.dal.generated.jooq.tables.JProcessingStatus;
import com.ngl.micro.image.dal.generated.jooq.tables.JProcessingStatusReason;
import com.ngl.micro.image.dal.generated.jooq.tables.JResourceLookupType;
import com.ngl.micro.image.dal.generated.jooq.tables.JSchemaVersion;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in ngl_micro_image
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

	/**
	 * The table ngl_micro_image.access_token_revoked_event
	 */
	public static final JAccessTokenRevokedEvent ACCESS_TOKEN_REVOKED_EVENT = com.ngl.micro.image.dal.generated.jooq.tables.JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT;

	/**
	 * The table ngl_micro_image.image_data
	 */
	public static final JImageData IMAGE_DATA = com.ngl.micro.image.dal.generated.jooq.tables.JImageData.IMAGE_DATA;

	/**
	 * The table ngl_micro_image.image_job
	 */
	public static final JImageJob IMAGE_JOB = com.ngl.micro.image.dal.generated.jooq.tables.JImageJob.IMAGE_JOB;

	/**
	 * The table ngl_micro_image.image_metadata
	 */
	public static final JImageMetadata IMAGE_METADATA = com.ngl.micro.image.dal.generated.jooq.tables.JImageMetadata.IMAGE_METADATA;

	/**
	 * The table ngl_micro_image.image_size
	 */
	public static final JImageSize IMAGE_SIZE = com.ngl.micro.image.dal.generated.jooq.tables.JImageSize.IMAGE_SIZE;

	/**
	 * The table ngl_micro_image.processing_status
	 */
	public static final JProcessingStatus PROCESSING_STATUS = com.ngl.micro.image.dal.generated.jooq.tables.JProcessingStatus.PROCESSING_STATUS;

	/**
	 * The table ngl_micro_image.processing_status_reason
	 */
	public static final JProcessingStatusReason PROCESSING_STATUS_REASON = com.ngl.micro.image.dal.generated.jooq.tables.JProcessingStatusReason.PROCESSING_STATUS_REASON;

	/**
	 * The table ngl_micro_image.resource_lookup_type
	 */
	public static final JResourceLookupType RESOURCE_LOOKUP_TYPE = com.ngl.micro.image.dal.generated.jooq.tables.JResourceLookupType.RESOURCE_LOOKUP_TYPE;

	/**
	 * The table ngl_micro_image.schema_version
	 */
	public static final JSchemaVersion SCHEMA_VERSION = com.ngl.micro.image.dal.generated.jooq.tables.JSchemaVersion.SCHEMA_VERSION;
}
