package com.ngl.micro.image.process;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;
import org.springframework.util.Assert;

public final class ImageProcessor {

	private static final Color TRANSPARENT = new Color(0f, 0f, 0f, 0f);

	public static BufferedImage toBufferedImage(byte[] data) throws IOException {
		Assert.notNull(data);
		try (ByteArrayInputStream in = new ByteArrayInputStream(data)) {
			return ImageIO.read(in);
		}
	}

	public static byte[] toBytes(BufferedImage image) throws IOException {
		Assert.notNull(image);
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			ImageIO.write(image, "png", out);
			return out.toByteArray();
		}
	}

	public static BufferedImage resize(BufferedImage image, int width, int height) {
		Assert.notNull(image);
		return Scalr.resize(image, Mode.FIT_EXACT, width, height);
	}

	public static BufferedImage pad(BufferedImage image, int aspectRatioX, int aspectRatioY) throws IOException {
		Assert.notNull(image);
		int srcWidth  =  image.getWidth();
		int srcHeight = image.getHeight();
		double car = (double) srcWidth / (double) srcHeight;
		double dar = (double) aspectRatioX / (double) aspectRatioY;
		int paddedWidth, paddedHeight, x = 0, y = 0;
		if (car > dar) {
			paddedWidth = srcWidth;
			paddedHeight = (int) ((double) srcHeight / dar * car);
			y = (paddedHeight - srcHeight) / 2;
		}
		else if (car < dar){
			paddedWidth = (int) ((double) srcWidth / car * dar);
			paddedHeight = srcHeight;
			x = (paddedWidth - srcWidth) / 2;
		}
		else {
			return toBufferedImage(toBytes(image));
		}

		BufferedImage result = new BufferedImage(paddedWidth, paddedHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics g = result.getGraphics();

		// "Clear" the background of the new image first.
		g.setColor(TRANSPARENT);
		g.fillRect(0, 0, paddedWidth, paddedHeight);

		// Draw the image into the center of the new padded image.
		g.drawImage(image, x, y, null);
		g.dispose();

		return result;
	}

	private ImageProcessor() {
	}

}
