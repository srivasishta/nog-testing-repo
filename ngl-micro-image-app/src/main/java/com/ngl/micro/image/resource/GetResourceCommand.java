package com.ngl.micro.image.resource;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.hystrix.HystrixCommand;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.Assert;

/**
 * A Hystrix command used to look up resources over the network. The command delegates to
 * a {@link ResourceService} to perform the actual network call which is managed
 * by the circuit breaker.
 *
 * @author Willy du Preez
 *
 * @param <T> the resource type
 */
public class GetResourceCommand<T extends Identifiable<UUID>> extends HystrixCommand<T> {

	private static final Logger log = LoggerFactory.getLogger(GetResourceCommand.class);

	private ResourceService<T> service;
	private UUID resourceId;

	public GetResourceCommand(Setter config, ResourceService<T> service, UUID resourceId) {
		super(config);

		Assert.notNull(config);
		Assert.notNull(service);
		Assert.notNull(resourceId);

		this.service = service;
		this.resourceId = resourceId;
	}

	@Override
	protected T run() throws Exception {
		log.debug("Lookup command executing for resource with ID: {}", this.resourceId);
		return this.service.getResource(this.resourceId);
	}

}
