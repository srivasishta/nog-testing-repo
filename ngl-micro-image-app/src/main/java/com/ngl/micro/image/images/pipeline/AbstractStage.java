package com.ngl.micro.image.images.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.middleware.integration.pipeline.Stage;

/**
 * An abstract stage for common functionality.
 *
 * @author Willy du Preez
 *
 */
public abstract class AbstractStage implements Stage<ImageJobContext> {

	private static final Logger log = LoggerFactory.getLogger(AbstractStage.class);

	protected void updateState(
			ImageJobContext context,
			ProcessingStatus newStatus,
			ProcessingStatusReason newStatusReason) {

		ImageJob job = context.getJob();
		log.debug("Image processing job [{}] changing state: [{}] {} -> [{}] {}",
				job.getId(),
				job.getProcessingStatus(), job.getProcessingStatusReason(),
				newStatus, newStatusReason);

		job.setProcessingStatus(newStatus.name());
		job.setProcessingStatusReason(newStatusReason.name());
		context.getJobs().update(job);
	}

}
