package com.ngl.micro.image.images.pipeline;

import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;

/**
 * A first stage in the pipeline that initializes the job.
 *
 * @author Willy du Preez
 *
 */
public class InitJobStage extends AbstractStage {

	@Override
	public void process(ImageJobContext context) {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.PENDING)) {
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_RESOURCE_AUTH);
		}
	}

	@Override
	public String getName() {
		return "[init-job]";
	}

}
