package com.ngl.micro.image.images;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * The reason for the current processing status.
 *
 * @author Willy du Preez
 *
 */
public enum ProcessingStatusReason {

	PENDING(1L),
	SUCCESS(2L),

	AWAITING_RESOURCE_AUTH(1001L),
	AWAITING_IMAGE_PROCESSING(1002L),
	AWAITING_IMAGE_AUTH(1003L),
	AWAITING_IMAGE_DISTRIBUTION(1004L),
	AWAITING_RESOURCE_UPDATE(1005L),
	AWAITING_FINALIZATION(1006L),

	PROCESSING_ERROR_UNKNOWN(2001L),
	PROCESSING_ERROR_ORIGINAL_DIMENSIONS_TOO_SMALL(2002L),

	BUSINESS_NOT_FOUND(3001L),
	BUSINESS_AUTHORIZATION_FAILED(3002L),

	CHARITY_NOT_FOUND(4001L),
	CHARITY_AUTHORIZATION_FAILED(4002L),

	PARTNER_AUTHORIZATION_FAILED(5001L);

	public static ProcessingStatusReason forId(Long id) {
		Assert.notNull(id);

		for (ProcessingStatusReason reason : ProcessingStatusReason.values()) {
			if (reason.id == id) {
				return reason;
			}
		}
		throw new NoSuchElementException("No ProcessingStatusReason with ID: " + id);
	}

	public static String nameForId(Long id) {
		return ProcessingStatusReason.forId(id).name();
	}

	public static Long idFrom(String name) {
		Assert.notNull(name);
		return ProcessingStatusReason.valueOf(name).getId();
	}

	private long id;

	private ProcessingStatusReason(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
