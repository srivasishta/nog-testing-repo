package com.ngl.micro.image.images;

import java.util.UUID;

import com.ngl.micro.image.ImageJob;
import com.ngl.middleware.integration.pipeline.PipelineContext;

/**
 * A context for an image job that is processed through the image job pipeline.
 *
 * @author Willy du Preez
 *
 */
public class ImageJobContext implements PipelineContext<UUID> {

	private ImageJobRepository jobs;
	private ImageJob job;

	public ImageJobContext(ImageJobRepository jobs, ImageJob job) {
		this.jobs = jobs;
		this.job = job;
	}

	public ImageJobRepository getJobs() {
		return this.jobs;
	}

	public ImageJob getJob() {
		return this.job;
	}

	public void refreshJob() {
		this.job = this.jobs.get(this.job.getId());
	}

	public boolean isPendingWithReason(ImageJob job, ProcessingStatusReason reason) {
		return ProcessingStatus.PENDING.name().equals(job.getProcessingStatus())
				&& reason.name().equals(job.getProcessingStatusReason());
	}

	@Override
	public UUID getId() {
		return this.job.getId();
	}

	@Override
	public String toString() {
		return "[job=" + this.job.getId().toString()
				+ ", status=" + this.job.getProcessingStatus()
				+ ", reason=" + this.job.getProcessingStatusReason() + "]";
	}

}
