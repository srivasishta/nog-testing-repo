package com.ngl.micro.image.images;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Image sizes DB lookup.
 *
 * @author Willy du Preez
 *
 */
public enum ResourceLookupType {

	BUSINESS_LOGO(1),
	CHARITY_LOGO(2),
	PARTNER_LOGO(3);

	public static ResourceLookupType forId(Long id) {
		Assert.notNull(id);

		for (ResourceLookupType size : ResourceLookupType.values()) {
			if (size.id == id) {
				return size;
			}
		}
		throw new NoSuchElementException("No ResourceLookupType with ID: " + id);
	}

	public static ResourceLookupType forEnum(Enum<?> named) {
		return ResourceLookupType.valueOf(named.name());
	}

	private long id;

	private ResourceLookupType(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
