/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.image.dal.generated.jooq.tables.records;


import com.ngl.micro.image.dal.generated.jooq.tables.JImageData;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JImageDataRecord extends UpdatableRecordImpl<JImageDataRecord> implements Record3<Long, Long, byte[]> {

	private static final long serialVersionUID = -1740467805;

	/**
	 * Setter for <code>ngl_micro_image.image_data.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.image_data.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_image.image_data.metadata_id</code>.
	 */
	public void setMetadataId(Long value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.image_data.metadata_id</code>.
	 */
	public Long getMetadataId() {
		return (Long) getValue(1);
	}

	/**
	 * Setter for <code>ngl_micro_image.image_data.data</code>.
	 */
	public void setData(byte[] value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.image_data.data</code>.
	 */
	public byte[] getData() {
		return (byte[]) getValue(2);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record3 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Long, Long, byte[]> fieldsRow() {
		return (Row3) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Long, Long, byte[]> valuesRow() {
		return (Row3) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JImageData.IMAGE_DATA.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field2() {
		return JImageData.IMAGE_DATA.METADATA_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<byte[]> field3() {
		return JImageData.IMAGE_DATA.DATA;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value2() {
		return getMetadataId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] value3() {
		return getData();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JImageDataRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JImageDataRecord value2(Long value) {
		setMetadataId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JImageDataRecord value3(byte[] value) {
		setData(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JImageDataRecord values(Long value1, Long value2, byte[] value3) {
		value1(value1);
		value2(value2);
		value3(value3);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JImageDataRecord
	 */
	public JImageDataRecord() {
		super(JImageData.IMAGE_DATA);
	}

	/**
	 * Create a detached, initialised JImageDataRecord
	 */
	public JImageDataRecord(Long id, Long metadataId, byte[] data) {
		super(JImageData.IMAGE_DATA);

		setValue(0, id);
		setValue(1, metadataId);
		setValue(2, data);
	}
}
