package com.ngl.micro.image.event.in.token;

import static com.ngl.micro.image.dal.generated.jooq.Tables.ACCESS_TOKEN_REVOKED_EVENT;

import java.time.ZonedDateTime;
import java.util.List;

import org.jooq.Configuration;

import com.ngl.micro.image.dal.generated.jooq.tables.daos.JAccessTokenRevokedEventDao;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JAccessTokenRevokedEvent;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.rest.server.security.oauth2.Revocation;

/**
 * Repository for managing persistence of access token revoked events.
 *
 * @author Willy du Preez
 *
 */
public class AccessTokenRevokedEventRepository {

	private DAL dal;
	private JAccessTokenRevokedEventDao dao;

	public AccessTokenRevokedEventRepository(Configuration config) {
		this.dal = new DAL(config);
		this.dao = new JAccessTokenRevokedEventDao(config);
	}

	public void add(AccessTokenRevokedEvent event) {
		JAccessTokenRevokedEvent record = new JAccessTokenRevokedEvent();
		record.setId(event.getId());
		record.setTokenId(event.getTokenId());
		record.setRevokedDate(event.getCreated());
		record.setExpirationDate(event.getExpirationDate());
		this.dao.insert(record);
	}

	public List<Revocation> getByExpirationDateGreaterThan(ZonedDateTime date) {
		return this.dal.sql().selectFrom(ACCESS_TOKEN_REVOKED_EVENT)
				.where(ACCESS_TOKEN_REVOKED_EVENT.EXPIRATION_DATE.greaterThan(date))
				.fetch()
				.map(e -> new Revocation(e.getTokenId(), e.getExpirationDate()));
	}

}
