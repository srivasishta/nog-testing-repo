package com.ngl.micro.image.resource;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.hystrix.HystrixCommand;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.util.Assert;

/**
 * A Hystrix command used to patch resources over the network. The command delegates to
 * a {@link ResourceService} to perform the actual network call which is managed
 * by the circuit breaker.
 *
 * @author Willy du Preez
 *
 * @param <T> the resource type
 */
public class PatchResourceCommand<T extends Identifiable<UUID>> extends HystrixCommand<T> {

	private static final Logger log = LoggerFactory.getLogger(PatchResourceCommand.class);

	private ResourceService<T> service;
	private T resource;

	public PatchResourceCommand(Setter config, ResourceService<T> service, T resource) {
		super(config);

		Assert.notNull(config);
		Assert.notNull(service);
		Assert.notNull(resource);
		Assert.state(resource.getId() != null);

		this.service = service;
		this.resource = resource;
	}

	@Override
	protected T run() throws Exception {
		log.debug("Patch command executing for resource with ID: {}", this.resource.getId());
		return this.service.patchResource(this.resource);
	}

}
