package com.ngl.micro.image.distribute;

import java.io.IOException;

/**
 * Thrown if an error occurs interacting with the CDN.
 *
 * @author Willy du Preez
 *
 */
public class ImageDistributionException extends IOException {

	private static final long serialVersionUID = 1L;

	public ImageDistributionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageDistributionException(String message) {
		super(message);
	}

}
