package com.ngl.micro.image.images;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Image endpoint.
 *
 * @author Willy du Preez
 *
 */
@Path("images")
public interface ImageEndpoint extends RestEndpoint {

	@GET
	@Path("{id}")
	@RequiresScopes(Scopes.IMAGE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@GET
	@RequiresScopes(Scopes.IMAGE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@POST
	@RequiresScopes(Scopes.IMAGE_UPLOAD_SCOPE)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	Response upload(@Context HttpServletRequest request) throws ServletException, IOException;

	@GET
	@RequiresScopes(Scopes.IMAGE_UPLOAD_SCOPE)
	@Path("{id}/status")
	@Produces(MediaType.APPLICATION_JSON)
	Response status(UUID id);

}
