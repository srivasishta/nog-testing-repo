package com.ngl.micro.image.resource.type;

import com.ngl.micro.image.resource.LogoResource;

/**
 * A partial representation of the charity resource used to look up
 * and patch charities for logo updates.
 *
 * @author Willy du Preez
 *
 */
public class CharityLogoResource extends LogoResource {

}
