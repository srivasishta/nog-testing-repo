package com.ngl.micro.image.distribute;

import com.ngl.middleware.daemon.ApplicationEnvironment;

/**
 * Local filesystem CDN configuration.
 *
 * @author Willy du Preez
 *
 */
public class LocalHostedNetworkProperties {

	public static final String DEFAULT_PREFIX = "image.distribution.local.";

	private String baseUrl = "http://localhost/";
	private String path = ApplicationEnvironment.newInstance(this.getClass()).getTmpPath();

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
