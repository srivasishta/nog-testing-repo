package com.ngl.micro.image.resource;

import java.util.UUID;

import org.springframework.util.Assert;

import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Methods;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.util.Uris;

/**
 * A base implementation to look up and patch resources.
 *
 * @author Willy du Preez
 *
 * @param <T> the resource type
 */
public abstract class AbstractResourceService<T extends Identifiable<UUID>> implements ResourceService<T> {

	private Class<T> resourceType;
	private HttpClient client;
	private String resourceUrl;
	private Diff diff;

	public AbstractResourceService(Class<T> resourceType, HttpClient client, String resourceUrl) {
		Assert.notNull(resourceType);
		Assert.notNull(client);
		Uris.toUri(resourceUrl);

		this.client = client;
		if (!resourceUrl.endsWith("/")) {
			resourceUrl += "/";
		}
		this.resourceUrl = resourceUrl;
		this.resourceType = resourceType;
		this.diff = new Diff();
	}

	@Override
	public T getResource(UUID id) {
		RestResponse response = this.client.request(this.resourceUrl + id)
				.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
				.method(Methods.GET)
				.execute(RestResponse.class);

		if (response.getStatus() != 200) {
			ApplicationError error = response.bodyAsPojo(ApplicationError.class);
			if (ApplicationStatus.NOT_FOUND.equals(error.getStatus())) {
				return null;
			}
			throw new ApplicationException(error, error.getDetail());
		}

		return response.bodyAsPojo(this.resourceType);
	}

	@Override
	public T patchResource(T resource) {
		T base = this.getResource(resource.getId());
		Patch patch = this.calculatePatch(this.diff, base, resource);

		RestResponse response = this.client.request(this.resourceUrl + resource.getId())
				.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
				.method(Methods.PATCH)
				.body(patch)
				.execute(RestResponse.class);

		if (response.getStatus() != 200) {
			ApplicationError error = response.bodyAsPojo(ApplicationError.class);
			throw new ApplicationException(error, error.getDetail());
		}

		return response.bodyAsPojo(this.resourceType);
	}

	public Class<T> getResourceType() {
		return this.resourceType;
	}

	protected abstract Patch calculatePatch(Diff diff, T base, T working);

}
