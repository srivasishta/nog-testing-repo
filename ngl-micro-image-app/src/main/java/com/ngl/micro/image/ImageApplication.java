package com.ngl.micro.image;

import com.ngl.micro.image.dal.generated.jooq.JNglMicroImage;
import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The image application entry-point.
 *
 * @author Willy du Preez
 *
 */
public class ImageApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new ImageApplication().development();
	}

	public ImageApplication() {
		super(ImageApplicationConfiguration.class, JNglMicroImage.NGL_MICRO_IMAGE);
	}

}