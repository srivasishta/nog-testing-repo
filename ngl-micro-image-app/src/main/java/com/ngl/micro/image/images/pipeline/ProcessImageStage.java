package com.ngl.micro.image.images.pipeline;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.UUID;

import com.ngl.micro.image.ImageMetadata;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ImageRepository;
import com.ngl.micro.image.images.ImageSize;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.micro.image.process.ImageProcessor;

/**
 * A stage in the pipeline that processes an image into different sizes.
 *
 * @author Willy du Preez
 *
 */
public class ProcessImageStage extends AbstractStage {

	private static final int ASPECT_RATIO_X = 4;
	private static final int ASPECT_RATIO_Y = 3;

	private static final int T_WIDTH = 160;
	private static final int T_HEIGHT = 120;

	private static final int M_WIDTH = 320;
	private static final int M_HEIGHT = 240;

	private static final int L_WIDTH = 640;
	private static final int L_HEIGHT = 480;

	private static final int MIN_WIDTH = L_WIDTH;
	private static final int MIN_HEIGHT = L_HEIGHT;

	private ImageRepository images;

	public ProcessImageStage(ImageRepository images) {
		this.images = images;
	}

	@Override
	public void process(ImageJobContext context) {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_IMAGE_PROCESSING)) {
			try {
				this.processImages(context);
			} catch (Exception e) {
				this.updateState(context, ProcessingStatus.FAILED, ProcessingStatusReason.PROCESSING_ERROR_UNKNOWN);
			}
		}
	}

	private void processImages(ImageJobContext context) throws IOException {
		byte[] originalRaw = this.images.getImageDataBySize(context.getId(), ImageSize.ORIGINAL);
		BufferedImage originalImg = null;
		BufferedImage paddedImg = null;
		try {
			originalImg = ImageProcessor.toBufferedImage(originalRaw);
			paddedImg = ImageProcessor.pad(originalImg, ASPECT_RATIO_X, ASPECT_RATIO_Y);
			this.updateOriginalMetadata(context.getId(), originalImg);
			if (this.hasValidOriginalDimensions(originalImg.getWidth(), originalImg.getHeight())) {
				this.processImages(context, paddedImg);
			}
			else {
				this.updateState(
						context,
						ProcessingStatus.FAILED,
						ProcessingStatusReason.PROCESSING_ERROR_ORIGINAL_DIMENSIONS_TOO_SMALL);
				return;
			}
		} finally {
			if (originalImg != null) {
				originalImg.flush();
			}
			if (paddedImg != null) {
				paddedImg.flush();
			}
		}
	}

	private void updateOriginalMetadata(UUID jobId, BufferedImage originalImg) {
		ImageMetadata original = this.images.getImageMetadataBySize(jobId, ImageSize.ORIGINAL);
		original.setWidth(originalImg.getWidth());
		original.setHeight(originalImg.getHeight());
		this.images.updateImageMetadata(jobId, ImageSize.ORIGINAL, original);
	}

	private void processImages(ImageJobContext context, BufferedImage padded) throws IOException {
		ImageMetadata thumb = new ImageMetadata();
		thumb.setWidth(T_WIDTH);
		thumb.setHeight(T_HEIGHT);
		BufferedImage thumbImg = ImageProcessor.resize(padded, T_WIDTH, T_HEIGHT);
		try {
			this.images.addImage(context.getId(), ImageSize.THUMBNAIL, thumb, ImageProcessor.toBytes(thumbImg));
		}
		finally {
			thumbImg.flush();
		}

		ImageMetadata medium = new ImageMetadata();
		medium.setWidth(M_WIDTH);
		medium.setHeight(M_HEIGHT);
		BufferedImage mediumImg = ImageProcessor.resize(padded, M_WIDTH, M_HEIGHT);
		try {
			this.images.addImage(context.getId(), ImageSize.MEDIUM, medium, ImageProcessor.toBytes(mediumImg));
		}
		finally {
			thumbImg.flush();
		}

		ImageMetadata large = new ImageMetadata();
		large.setWidth(L_WIDTH);
		large.setHeight(L_HEIGHT);
		BufferedImage largeImg = ImageProcessor.resize(padded, L_WIDTH, L_HEIGHT);
		try {
			this.images.addImage(context.getId(), ImageSize.LARGE, large, ImageProcessor.toBytes(largeImg));
		}
		finally {
			largeImg.flush();
		}

		context.refreshJob();
		this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_IMAGE_AUTH);
	}

	private boolean hasValidOriginalDimensions(int width, int height) {
		return width >= MIN_WIDTH || height >= MIN_HEIGHT;
	}

	@Override
	public String getName() {
		return "[start]";
	}

}
