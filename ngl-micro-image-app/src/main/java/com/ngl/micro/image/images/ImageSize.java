package com.ngl.micro.image.images;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Image sizes DB lookup.
 *
 * @author Willy du Preez
 *
 */
public enum ImageSize {

	ORIGINAL(1),
	THUMBNAIL(2),
	MEDIUM(3),
	LARGE(4);

	public static ImageSize forId(Long id) {
		Assert.notNull(id);

		for (ImageSize size : ImageSize.values()) {
			if (size.id == id) {
				return size;
			}
		}
		throw new NoSuchElementException("No ImageSize with ID: " + id);
	}

	public static ImageSize forEnum(Enum<?> named) {
		return ImageSize.valueOf(named.name());
	}

	private long id;

	private ImageSize(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
