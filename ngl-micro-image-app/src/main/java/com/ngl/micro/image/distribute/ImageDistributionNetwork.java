package com.ngl.micro.image.distribute;

/**
 * CDN abstraction.
 *
 * @author Willy du Preez
 *
 */
public interface ImageDistributionNetwork {

	void init();
	ImageDistributionResponse distribute(ImageDistributionRequest request) throws ImageDistributionException;

}
