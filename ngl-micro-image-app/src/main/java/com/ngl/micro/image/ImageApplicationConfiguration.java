package com.ngl.micro.image;

import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.client.auth.Credential;
import com.ngl.micro.client.auth.CredentialFactory;
import com.ngl.micro.client.auth.CredentialProperties;
import com.ngl.micro.image.distribute.ImageDistributionNetwork;
import com.ngl.micro.image.distribute.LocalHostedNetwork;
import com.ngl.micro.image.event.in.token.AccessTokenRevokedEventListener;
import com.ngl.micro.image.event.in.token.AccessTokenRevokedEventRepository;
import com.ngl.micro.image.images.ImageEndpoint;
import com.ngl.micro.image.images.ImageEndpointImpl;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ImageJobPipelineFactory;
import com.ngl.micro.image.images.ImageJobPipelineProperties;
import com.ngl.micro.image.images.ImageJobRepository;
import com.ngl.micro.image.images.ImageJobValidator;
import com.ngl.micro.image.images.ImageRepository;
import com.ngl.micro.image.images.ImageService;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.type.BusinessLogoResource;
import com.ngl.micro.image.resource.type.CharityLogoResource;
import com.ngl.micro.image.resource.type.PartnerLogoResource;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.integration.batch.AsyncPipeline;
import com.ngl.middleware.messaging.core.data.deserialize.LoggingDeserializationErrorHandler;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriber;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberConfiguration;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the image application.
 *
 * @author Willy du Preez
 */
@Configuration
@EnableMicroserviceDatabase
@EnableMicroserviceRestServer
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
public class ImageApplicationConfiguration extends MicroserviceConfiguration {

	private static final String IMAGE_ENDPOINT = "image";

	@Bean
	public RestEndpoints restEndpoints(
			ObjectMapper mapper,
			ImageEndpoint membershipEndpoint,
			Realm securityRealm) {

		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints(IMAGE_ENDPOINT);
		restEndpoints.addEndpoints(membershipEndpoint);
		restEndpoints.setRealm(securityRealm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	/* --------------------------------------------------------
	 *  Image
	 * -------------------------------------------------------- */

	@Bean
	public ImageRepository imageDataRepository(DAL dal) {
		return new ImageRepository(dal);
	}

	@Bean
	public ImageJobRepository imageJobRepository(DAL dal) {
		return new ImageJobRepository(dal);
	}

	@Bean
	public ImageJobValidator imageValidator() {
		return ImageJobValidator.newInstance();
	}

	@Bean
	public ImageJobPipelineProperties imageJobProcessorProperties(EnvironmentConfigurationProvider config) {
		return config.getConfiguration(ImageJobPipelineProperties.DEFAULT_PREFIX, ImageJobPipelineProperties.class);
	}

	@Bean
	public HttpClient httpClient(ObjectMapper mapper, Credential credential) {
		return HttpClient.withDefaultTransport()
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.register(credential)
				.build();
	}

	@Bean(initMethod = "init")
	public ImageDistributionNetwork imageDistributionNetwork(EnvironmentConfigurationProvider config) {
		return new LocalHostedNetwork(config);
	}

	@Bean
	public AsyncPipeline<ImageJobContext> imageJobPipeline(
			ImageJobRepository imageJobRepository,
			ImageRepository imageDataRepository,
			PlatformTransactionManager txManager,
			ImageJobPipelineProperties properties,
			ImageDistributionNetwork imageDistributionNetwork,
			HttpClient httpClient) {

		LogoResourceService<BusinessLogoResource> businesses = new LogoResourceService<>(
				BusinessLogoResource.class, httpClient, properties.getBusinessResourceUrl());

		LogoResourceService<CharityLogoResource> charities = new LogoResourceService<>(
				CharityLogoResource.class, httpClient, properties.getCharityResourceUrl());

		LogoResourceService<PartnerLogoResource> partner = new LogoResourceService<>(
				PartnerLogoResource.class, httpClient, properties.getPartnerResourceUrl());

		return new ImageJobPipelineFactory().newInstance(
				properties,
				txManager,
				imageJobRepository,
				imageDataRepository,
				imageDistributionNetwork,
				businesses,
				charities,
				partner);
	}

	@Bean
	public ImageService imageService(
			ImageJobRepository imageJobRepository,
			ImageRepository imageDataRepository,
			ImageJobValidator imageValidator,
			AsyncPipeline<ImageJobContext> imageJobPipeline,
			PlatformTransactionManager txManager) {
		return new ImageService(
				imageJobRepository,
				imageDataRepository,
				imageValidator,
				imageJobPipeline,
				txManager);
	}

	@Bean
	public ImageEndpoint imageEndpoint(RequestProcessor processor, ImageService imageService) {
		return new ImageEndpointImpl(processor, imageService);
	}

	/* --------------------------------------------------------
	 *  Access Tokens
	 * -------------------------------------------------------- */

    @Bean(initMethod = "init", destroyMethod = "close")
    public Credential credential(ObjectMapper objectMapper, EnvironmentConfigurationProvider config) {
    	CredentialProperties props = config.getConfiguration(
    			CredentialProperties.DEFAULT_PREFIX, CredentialProperties.class);

    	return new CredentialFactory().newInstance(props);
    }

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration configuration) {
		return new AccessTokenRevokedEventRepository(configuration);
	}

	@Bean
	public RevocationList revocationList(AccessTokenRevokedEventRepository events) {
		return new RevocationList(events.getByExpirationDateGreaterThan(Time.utcNow()));
	}

	@Bean
	public AccessTokenRevokedEventListener accessTokenEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository accessTokenRevokedEventRepository,
			RevocationList revocationList) {
		return new AccessTokenRevokedEventListener(objectMapper, accessTokenRevokedEventRepository, revocationList);
	}

	@Bean
	public Realm securityRealm(RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(
				this.signedEncryptedJwtFactory(),
				revocationList));
	}

	@Bean
	public SignedEncryptedJwtFactory signedEncryptedJwtFactory() {
		return new SignedEncryptedJwtFactory();
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber accessTokenEventSubscriber(
			ConnectionFactory connectionFactory,
			AccessTokenRevokedEventListener accessTokenEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<AccessTokenRevokedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(1);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setSubscription("image-" + AccessTokenRevokedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(accessTokenEventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(accessTokenEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

}
