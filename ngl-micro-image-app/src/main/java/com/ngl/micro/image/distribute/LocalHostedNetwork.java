package com.ngl.micro.image.distribute;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Uris;

/**
 * Local filesystem CDN.
 *
 * @author Willy du Preez
 *
 */
public class LocalHostedNetwork implements ImageDistributionNetwork {

	private static final Logger log = LoggerFactory.getLogger(LocalHostedNetwork.class);

	private EnvironmentConfigurationProvider config;
	private LocalHostedNetworkProperties properties;
	private String baseUrl;
	private File directory;

	public LocalHostedNetwork(EnvironmentConfigurationProvider config) {
		Assert.notNull(config);
		this.config = config;
	}

	public LocalHostedNetwork(LocalHostedNetworkProperties properties) {
		Assert.notNull(properties);
		this.properties = properties;
	}

	@Override
	public void init() {
		if (this.properties == null) {
			this.properties = this.config.getConfiguration(
					LocalHostedNetworkProperties.DEFAULT_PREFIX, LocalHostedNetworkProperties.class);
		}

		Assert.notNull(this.properties.getBaseUrl());
		Assert.notNull(this.properties.getPath());
		this.directory = new File(this.properties.getPath());
		if (!this.directory.exists()) {
			this.directory.mkdirs();
		}
		Assert.state(this.directory.exists() && this.directory.isDirectory(), this.directory.getAbsolutePath());
		this.baseUrl = this.properties.getBaseUrl();
		Uris.toUri(this.baseUrl);
		if (!this.baseUrl.endsWith("/")) {
			this.baseUrl = this.baseUrl + "/";
		}
	}

	@Override
	public ImageDistributionResponse distribute(ImageDistributionRequest request) throws ImageDistributionException {
		String filename = String.format("%s-%dx%d.png",
				request.getImageJobId(), request.getMetadata().getWidth(), request.getMetadata().getHeight());
		File file = new File(this.directory, filename);
		log.debug("Distributing image to local filesystem: " + file.getAbsolutePath());
		try (FileOutputStream out = new FileOutputStream(file)) {
			out.write(request.getData());
		} catch (IOException e) {
			throw new ImageDistributionException(
					"Failed to distribute image to local file: " + file.getAbsolutePath(), e);
		}
		ImageDistributionResponse response = new ImageDistributionResponse();
		response.setImageUrl(this.baseUrl + filename);
		return response;
	}

}
