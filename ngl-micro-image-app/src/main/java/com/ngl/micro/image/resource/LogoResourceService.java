package com.ngl.micro.image.resource;

import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.patch.diff.Diff;

/**
 * A default implementation of {@link ResourceService} which assumes the resource
 * format to follow the {@link LogoResource} format.
 *
 * @author Willy du Preez
 *
 * @param <T> resource type
 */
public class LogoResourceService<T extends LogoResource> extends AbstractResourceService<T> {

	public LogoResourceService(Class<T> resourceType, HttpClient client, String resourceUrl) {
		super(resourceType, client, resourceUrl);
	}

	@Override
	protected Patch calculatePatch(Diff diff, T base, T working) {
		LogoResource narrowBase = new LogoResource();
		narrowBase.setLogoImages(base.getLogoImages());
		LogoResource narrowWorking = new LogoResource();
		narrowWorking.setLogoImages(working.getLogoImages());
		return diff.diff(narrowWorking, narrowBase);
	}

}
