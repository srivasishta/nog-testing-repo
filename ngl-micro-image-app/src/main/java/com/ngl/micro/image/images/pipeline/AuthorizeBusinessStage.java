package com.ngl.micro.image.images.pipeline;

import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.GetResourceCommand;
import com.ngl.micro.image.resource.type.BusinessLogoResource;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.integration.breaker.HystrixTrap;

/**
 * Authorizes business logo uploads.
 *
 * @author Willy du Preez
 *
 */
public class AuthorizeBusinessStage extends AbstractStage {

	private HystrixTrap trap;
	private LogoResourceService<BusinessLogoResource> businesses;
	private Setter businessBreaker;

	public AuthorizeBusinessStage(
			HystrixTrap trap,
			LogoResourceService<BusinessLogoResource> businesses,
			Setter businessBreaker) {
		Assert.notNull(trap);
		Assert.notNull(businesses);
		Assert.notNull(businessBreaker);

		this.trap = trap;
		this.businesses = businesses;
		this.businessBreaker = businessBreaker;
	}

	@Override
	public void process(ImageJobContext context) throws InterruptedException {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_RESOURCE_AUTH)) {
			this.authorizeResource(context);
		}
	}

	private void authorizeResource(ImageJobContext context) throws InterruptedException {
		ImageJob job = context.getJob();
		BusinessLogoResource business = this.trap.funnel(() -> new GetResourceCommand<>(
				this.businessBreaker,
				this.businesses,
				job.getResourceId()));
		if (business == null) {
			this.updateState(context, ProcessingStatus.FAILED, ProcessingStatusReason.BUSINESS_NOT_FOUND);
		}
		else if (!ApiPolicy.isAccessibleByPartner(job.getPartnerId(), business.getPartnerId())) {
			this.updateState(context, ProcessingStatus.FAILED, ProcessingStatusReason.BUSINESS_AUTHORIZATION_FAILED);
		}
		else {
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_IMAGE_PROCESSING);
		}
	}

	@Override
	public String getName() {
		return "[auth-business-resource]";
	}

}
