package com.ngl.micro.image.distribute;

/**
 * Response from the CDN for an image distribution request.
 *
 * @author Willy du Preez
 *
 */
public class ImageDistributionResponse {

	private String imageUrl;

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}
