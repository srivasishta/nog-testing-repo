package com.ngl.micro.image.resource;

import java.util.UUID;

import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.rest.api.Identifiable;

/**
 * A resource with a logo as a first level property called "logoImages".
 *
 * @author Willy du Preez
 *
 */
public class LogoResource implements Identifiable<UUID> {

	private UUID id;
	private ResourceStatus status;
	private ImageSet logoImages;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public ResourceStatus getStatus() {
		return this.status;
	}

	public void setStatus(ResourceStatus status) {
		this.status = status;
	}

	public ImageSet getLogoImages() {
		return this.logoImages;
	}

	public void setLogoImages(ImageSet logoImages) {
		this.logoImages = logoImages;
	}

}
