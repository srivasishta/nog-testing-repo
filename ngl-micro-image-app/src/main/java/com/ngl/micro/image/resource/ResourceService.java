package com.ngl.micro.image.resource;

import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

/**
 * A resource service interface used to look up and patch resources.
 *
 * @author Willy du Preez
 *
 * @param <T> the resource type
 */
public interface ResourceService<T extends Identifiable<UUID>> {

	T getResource(UUID id);
	T patchResource(T resource);

}
