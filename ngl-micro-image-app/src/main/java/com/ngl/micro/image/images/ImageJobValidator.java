package com.ngl.micro.image.images;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;

import com.ngl.micro.image.ImageJob;
import com.ngl.middleware.rest.server.validation.ResourceValidator;

/**
 * Validates {@link ImageJob}s.
 *
 * @author Willy du Preez
 *
 */
public class ImageJobValidator extends ResourceValidator<ImageJob> {

	public static ImageJobValidator newInstance() {
		return ResourceValidator.configurator(new ImageJobValidator()).configure();
	}

	private ImageJobValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(ImageJob.class)
			.property(ImageJob.Fields.RESOURCE_TYPE, FIELD)
				.constraint(new NotNullDef())
			.property(ImageJob.Fields.RESOURCE_ID, FIELD)
				.constraint(new NotNullDef())
			.property(ImageJob.Fields.PARTNER_ID, FIELD)
				.constraint(new NotNullDef());
	}

}
