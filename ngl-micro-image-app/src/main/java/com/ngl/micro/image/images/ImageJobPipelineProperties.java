package com.ngl.micro.image.images;

import com.ngl.middleware.integration.batch.AsyncPipelineProperties;

/**
 * Image job pipeline configuration options.
 *
 * @author Willy du Preez
 *
 */
public class ImageJobPipelineProperties extends AsyncPipelineProperties {

	public static final String DEFAULT_PREFIX = "image.job.processor.";

	private String businessResourceUrl;
	private String charityResourceUrl;
	private String partnerResourceUrl;

	public String getBusinessResourceUrl() {
		return this.businessResourceUrl;
	}

	public void setBusinessResourceUrl(String businessResourceUrl) {
		this.businessResourceUrl = businessResourceUrl;
	}

	public String getCharityResourceUrl() {
		return this.charityResourceUrl;
	}

	public void setCharityResourceUrl(String charityResourceUrl) {
		this.charityResourceUrl = charityResourceUrl;
	}

	public String getPartnerResourceUrl() {
		return this.partnerResourceUrl;
	}

	public void setPartnerResourceUrl(String partnerResourceUrl) {
		this.partnerResourceUrl = partnerResourceUrl;
	}

}
