package com.ngl.micro.image.images.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;

/**
 * Authorizes partner logo uploads.
 *
 * @author Willy du Preez
 *
 */
public class AuthorizePartnerStage extends AbstractStage {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AuthorizeBusinessStage.class);

	public AuthorizePartnerStage() {
	}

	@Override
	public void process(ImageJobContext context) throws InterruptedException {
		// Note: Partner ID authorization is done on the REST end point.
		// Reaching this stage means that the partner uploaded it's own
		// logo or that the system account uploaded a logo on behalf of
		// the partner.
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_RESOURCE_AUTH)) {
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_IMAGE_PROCESSING);
		}
	}

	@Override
	public String getName() {
		return "[auth-partner-resource]";
	}

}
