package com.ngl.micro.image.resource.type;

import java.util.UUID;

import com.ngl.micro.image.resource.LogoResource;

/**
 * A partial representation of the business resource used to look up
 * and patch businesses for logo updates.
 *
 * @author Willy du Preez
 *
 */
public class BusinessLogoResource extends LogoResource {

	private UUID partnerId;

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

}
