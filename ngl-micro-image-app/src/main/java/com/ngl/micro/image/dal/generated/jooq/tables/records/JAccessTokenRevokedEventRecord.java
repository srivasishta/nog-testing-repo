/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.image.dal.generated.jooq.tables.records;


import com.ngl.micro.image.dal.generated.jooq.tables.JAccessTokenRevokedEvent;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JAccessTokenRevokedEventRecord extends UpdatableRecordImpl<JAccessTokenRevokedEventRecord> implements Record4<Long, UUID, ZonedDateTime, ZonedDateTime> {

	private static final long serialVersionUID = 1664165870;

	/**
	 * Setter for <code>ngl_micro_image.access_token_revoked_event.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.access_token_revoked_event.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_image.access_token_revoked_event.token_id</code>.
	 */
	public void setTokenId(UUID value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.access_token_revoked_event.token_id</code>.
	 */
	public UUID getTokenId() {
		return (UUID) getValue(1);
	}

	/**
	 * Setter for <code>ngl_micro_image.access_token_revoked_event.revoked_date</code>.
	 */
	public void setRevokedDate(ZonedDateTime value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.access_token_revoked_event.revoked_date</code>.
	 */
	public ZonedDateTime getRevokedDate() {
		return (ZonedDateTime) getValue(2);
	}

	/**
	 * Setter for <code>ngl_micro_image.access_token_revoked_event.expiration_date</code>.
	 */
	public void setExpirationDate(ZonedDateTime value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>ngl_micro_image.access_token_revoked_event.expiration_date</code>.
	 */
	public ZonedDateTime getExpirationDate() {
		return (ZonedDateTime) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, UUID, ZonedDateTime, ZonedDateTime> fieldsRow() {
		return (Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, UUID, ZonedDateTime, ZonedDateTime> valuesRow() {
		return (Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<UUID> field2() {
		return JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT.TOKEN_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<ZonedDateTime> field3() {
		return JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT.REVOKED_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<ZonedDateTime> field4() {
		return JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT.EXPIRATION_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UUID value2() {
		return getTokenId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ZonedDateTime value3() {
		return getRevokedDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ZonedDateTime value4() {
		return getExpirationDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JAccessTokenRevokedEventRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JAccessTokenRevokedEventRecord value2(UUID value) {
		setTokenId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JAccessTokenRevokedEventRecord value3(ZonedDateTime value) {
		setRevokedDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JAccessTokenRevokedEventRecord value4(ZonedDateTime value) {
		setExpirationDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JAccessTokenRevokedEventRecord values(Long value1, UUID value2, ZonedDateTime value3, ZonedDateTime value4) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JAccessTokenRevokedEventRecord
	 */
	public JAccessTokenRevokedEventRecord() {
		super(JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT);
	}

	/**
	 * Create a detached, initialised JAccessTokenRevokedEventRecord
	 */
	public JAccessTokenRevokedEventRecord(Long id, UUID tokenId, ZonedDateTime revokedDate, ZonedDateTime expirationDate) {
		super(JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT);

		setValue(0, id);
		setValue(1, tokenId);
		setValue(2, revokedDate);
		setValue(3, expirationDate);
	}
}
