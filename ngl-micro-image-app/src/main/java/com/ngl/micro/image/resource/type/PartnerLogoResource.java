package com.ngl.micro.image.resource.type;

import com.ngl.micro.image.resource.LogoResource;

/**
 * A partial representation of the partner resource used to look up
 * and patch partners for logo updates.
 *
 * @author Willy du Preez
 *
 */
public class PartnerLogoResource extends LogoResource {

}
