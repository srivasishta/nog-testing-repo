package com.ngl.micro.image.images;

import static com.ngl.micro.image.dal.generated.jooq.Tables.IMAGE_DATA;
import static com.ngl.micro.image.dal.generated.jooq.Tables.IMAGE_METADATA;

import java.util.UUID;

import org.jooq.Record;
import org.springframework.util.Assert;

import com.ngl.micro.image.ImageMetadata;
import com.ngl.micro.image.dal.generated.jooq.tables.daos.JImageDataDao;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JImageData;
import com.ngl.micro.image.dal.generated.jooq.tables.records.JImageDataRecord;
import com.ngl.micro.image.dal.generated.jooq.tables.records.JImageMetadataRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

/**
 * A repository to manage image data and metadata.
 *
 * @author Willy du Preez
 *
 */
public class ImageRepository {

	private DAL dal;
	private JImageDataDao data;

	public ImageRepository(DAL dal) {
		Assert.notNull(dal);
		this.dal = dal;
		this.data = new JImageDataDao(this.dal.config());
	}

	public byte[] getImageDataBySize(UUID id, ImageSize size) {
		Assert.notNull(id);
		Record record = this.dal.sql()
				.select(IMAGE_METADATA.ID)
				.from(IMAGE_METADATA)
				.where(IMAGE_METADATA.RESOURCE_ID.eq(id)
						.and(IMAGE_METADATA.IMAGE_SIZE_ID.eq(size.getId())))
				.fetchOne();

		if (record != null) {
			Long metadataId = record.getValue(IMAGE_METADATA.ID);
			JImageData image = this.data.fetchOneByJMetadataId(metadataId);
			return image.getData();
		}
		return null;
	}

	public ImageMetadata getImageMetadataBySize(UUID id, ImageSize size) {
		JImageMetadataRecord record = this.getImageMetadataRecordBySize(id, size);

		if (record == null) {
			return new ImageMetadata();
		}

		ImageMetadata metadata = new ImageMetadata();
		metadata.setHeight(record.getHeight());
		metadata.setWidth(record.getWidth());
		metadata.setUrl(record.getUrl());
		return metadata;
	}

	private JImageMetadataRecord getImageMetadataRecordBySize(UUID id, ImageSize size) {
		JImageMetadataRecord record = this.dal.sql()
				.selectFrom(IMAGE_METADATA)
				.where(IMAGE_METADATA.RESOURCE_ID.eq(id)
						.and(IMAGE_METADATA.IMAGE_SIZE_ID.eq(size.getId())))
				.fetchOne();
		return record;
	}

	public void addImage(UUID resourceId, ImageSize size, ImageMetadata metadata, byte[] data) {
		Assert.notNull(data);
		Assert.notNull(resourceId);
		Assert.notNull(metadata);
		Assert.notNull(size);

		JImageMetadataRecord record = this.dal.sql().newRecord(IMAGE_METADATA);
		record.setHeight(metadata.getHeight());
		record.setWidth(metadata.getWidth());
		record.setImageSizeId(size.getId());
		record.setResourceId(resourceId);
		record.setUrl(metadata.getUrl());
		record.insert();

		JImageDataRecord dataRecord = this.dal.sql().newRecord(IMAGE_DATA);
		dataRecord.setMetadataId(record.getId());
		dataRecord.setData(data);
		dataRecord.insert();
	}

	public void updateImageMetadata(UUID resourceId, ImageSize size, ImageMetadata metadata) {
		Assert.notNull(resourceId);
		Assert.notNull(metadata);
		Assert.notNull(size);

		JImageMetadataRecord record = this.getImageMetadataRecordBySize(resourceId, size);
		record.setHeight(metadata.getHeight());
		record.setWidth(metadata.getWidth());
		record.setUrl(metadata.getUrl());
		record.update();
	}

}
