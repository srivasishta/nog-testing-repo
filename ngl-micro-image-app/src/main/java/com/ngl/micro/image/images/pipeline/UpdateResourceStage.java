package com.ngl.micro.image.images.pipeline;

import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.micro.image.resource.LogoResource;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.ImageSet;
import com.ngl.micro.image.resource.PatchResourceCommand;
import com.ngl.middleware.integration.breaker.HystrixTrap;
import com.ngl.middleware.util.Types;

/**
 * A stage in the pipeline that updates the resource.
 *
 * @author Willy du Preez
 *
 * @param <T> the resource type
 */
public class UpdateResourceStage<T extends LogoResource> extends AbstractStage {

	private HystrixTrap trap;
	private LogoResourceService<T> resources;
	private Setter resourceBreaker;

	public UpdateResourceStage(HystrixTrap trap, LogoResourceService<T> resources, Setter resourceBreaker) {
		Assert.notNull(trap);
		Assert.notNull(resources);
		Assert.notNull(resourceBreaker);
		this.trap = trap;
		this.resources = resources;
		this.resourceBreaker = resourceBreaker;
	}

	@Override
	public void process(ImageJobContext context) throws InterruptedException {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_RESOURCE_UPDATE)) {
			this.updateBusiness(context);
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_FINALIZATION);
		}
	}

	private T updateBusiness(ImageJobContext context) throws InterruptedException {
		ImageJob job = context.getJob();
		ImageSet logos = new ImageSet();
		logos.setOriginal(job.getOriginal().getUrl());
		logos.setThumbnail(job.getThumbnail().getUrl());
		logos.setMedium(job.getMedium().getUrl());
		logos.setLarge(job.getLarge().getUrl());
		T resource = Types.newInstance(this.resources.getResourceType());
		resource.setId(job.getResourceId());
		resource.setLogoImages(logos);
		return this.trap.funnel(() -> new PatchResourceCommand<>(
				this.resourceBreaker,
				this.resources,
				resource));
	}

	@Override
	public String getName() {
		return "[update-resource]";
	}

}
