package com.ngl.micro.image.images.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;

/**
 * A final stage in the pipeline that completes the job.
 *
 * @author Willy du Preez
 *
 */
public class CompleteJobStage extends AbstractStage {

	private static final Logger log = LoggerFactory.getLogger(CompleteJobStage.class);

	@Override
	public void process(ImageJobContext context) {
		ImageJob job = context.getJob();

		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_FINALIZATION)) {
			// TODO: Create image resource - copy from job details.
			this.updateState(context, ProcessingStatus.SUCCESS, ProcessingStatusReason.SUCCESS);
		}

		if (ProcessingStatus.SUCCESS.name().equals(job.getProcessingStatus())) {
			log.debug("Image processing job completed sucessfully: id={}; status={}; reason={}",
					job.getId(),
					job.getProcessingStatus(),
					job.getProcessingStatusReason());
		}
		else {
			log.debug("Image processing job failed: id={}; status={}; reason={}",
					job.getId(),
					job.getProcessingStatus(),
					job.getProcessingStatusReason());
		}
	}

	@Override
	public String getName() {
		return "[complete-job]";
	}

}
