package com.ngl.micro.image.images;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolProperties;
import com.ngl.micro.image.ResourceType;
import com.ngl.micro.image.distribute.ImageDistributionNetwork;
import com.ngl.micro.image.images.pipeline.AuthorizeBusinessStage;
import com.ngl.micro.image.images.pipeline.AuthorizeCharityStage;
import com.ngl.micro.image.images.pipeline.AuthorizeImagesStage;
import com.ngl.micro.image.images.pipeline.AuthorizePartnerStage;
import com.ngl.micro.image.images.pipeline.CompleteJobStage;
import com.ngl.micro.image.images.pipeline.DistributeImagesStage;
import com.ngl.micro.image.images.pipeline.InitJobStage;
import com.ngl.micro.image.images.pipeline.ProcessImageStage;
import com.ngl.micro.image.images.pipeline.UpdateResourceStage;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.type.BusinessLogoResource;
import com.ngl.micro.image.resource.type.CharityLogoResource;
import com.ngl.micro.image.resource.type.PartnerLogoResource;
import com.ngl.middleware.integration.batch.AsyncPipeline;
import com.ngl.middleware.integration.breaker.HystrixTrap;
import com.ngl.middleware.integration.pipeline.Branch;
import com.ngl.middleware.integration.pipeline.Condition;
import com.ngl.middleware.integration.pipeline.Pipeline;
import com.ngl.middleware.util.UUIDS;
import com.ngl.middleware.util.backoff.LinearBackOff;

/**
 * A factory used to create an image job pipeline.
 *
 * @author Willy du Preez
 *
 */
public class ImageJobPipelineFactory {

	private static final String BUSINESS_LOOKUP_GROUP = "BusinessLookupGroup_" + UUIDS.type4Uuid().toString();
	private static final String CHARITY_LOOKUP_GROUP = "CharityLookupGroup_" + UUIDS.type4Uuid().toString();
	private static final String PARTNER_LOOKUP_GROUP = "PartnerLookupGroup_" + UUIDS.type4Uuid().toString();
	private static final String IMAGE_DISTRIBUTION_GROUP = "ImageDistributionGroup_" + UUIDS.type4Uuid().toString();

	private static Condition<ImageJobContext> isBusinessResource() {
		return (c) -> ResourceType.BUSINESS_LOGO.equals(c.getJob().getResourceType());
	}

	private static Condition<ImageJobContext> isCharityResource() {
		return (c) -> ResourceType.CHARITY_LOGO.equals(c.getJob().getResourceType());
	}

	private static Condition<ImageJobContext> isPartnerResource() {
		return (c) -> ResourceType.PARTNER_LOGO.equals(c.getJob().getResourceType());
	}

	public ImageJobPipelineFactory() {
	}

	public AsyncPipeline<ImageJobContext> newInstance(
			ImageJobPipelineProperties properties,
			PlatformTransactionManager txManager,
			ImageJobRepository jobs,
			ImageRepository images,
			ImageDistributionNetwork cdn,
			LogoResourceService<BusinessLogoResource> businesses,
			LogoResourceService<CharityLogoResource> charities,
			LogoResourceService<PartnerLogoResource> partners) {

		Assert.notNull(properties);
		Assert.notNull(txManager);
		Assert.notNull(jobs);
		Assert.notNull(images);
		Assert.notNull(cdn);
		Assert.notNull(businesses);
		Assert.notNull(charities);
		Assert.notNull(partners);

//		TransactionDefinition txDefinition = new DefaultTransactionDefinition();

		// Hystrix Config: Set the core pool size the
		// same as the executioner's core pool size.
		// Max queue size -1 to use SynchronousQueue.

		Setter businessBreaker = Setter
				.withGroupKey(HystrixCommandGroupKey.Factory.asKey(BUSINESS_LOOKUP_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

		Setter charityBreaker = Setter
				.withGroupKey(HystrixCommandGroupKey.Factory.asKey(CHARITY_LOOKUP_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

		Setter partnerBreaker = Setter
				.withGroupKey(HystrixCommandGroupKey.Factory.asKey(PARTNER_LOOKUP_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

		Setter distributionBreaker = Setter
				.withGroupKey(HystrixCommandGroupKey.Factory.asKey(IMAGE_DISTRIBUTION_GROUP))
				.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
						.withCoreSize(properties.getMaxThreads())
						.withMaxQueueSize(-1));

		Pipeline<ImageJobContext> pipeline = this.createPipeline(
				businesses,
				businessBreaker,
				charities,
				charityBreaker,
				partners,
				partnerBreaker,
				images,
				distributionBreaker,
				cdn);

		return AsyncPipeline.builder(pipeline)
				.withCoreThreads(properties.getCoreThreads())
				.withMaxQueueSize(properties.getMaxQueueSize())
				.withMaxThreads(properties.getMaxThreads())
				.build();
	}

	private Pipeline<ImageJobContext> createPipeline(
			LogoResourceService<BusinessLogoResource> businesses,
			Setter businessBreaker,
			LogoResourceService<CharityLogoResource> charities,
			Setter charityBreaker,
			LogoResourceService<PartnerLogoResource> partners,
			Setter partnerBreaker,
			ImageRepository images,
			Setter distributionBreaker,
			ImageDistributionNetwork cdn) {

		HystrixTrap businessTrap = new HystrixTrap(new LinearBackOff(30_000, 3_000, 1_000));
		HystrixTrap charityTrap = new HystrixTrap(new LinearBackOff(30_000, 3_000, 1_000));
		HystrixTrap partnerTrap = new HystrixTrap(new LinearBackOff(30_000, 3_000, 1_000));

		InitJobStage initJob = new InitJobStage();

		AuthorizeBusinessStage authBusiness = new AuthorizeBusinessStage(businessTrap, businesses, businessBreaker);
		AuthorizeCharityStage authCharity = new AuthorizeCharityStage(charityTrap, charities, charityBreaker);
		AuthorizePartnerStage authPartner = new AuthorizePartnerStage();

		ProcessImageStage processImage = new ProcessImageStage(images);
		AuthorizeImagesStage authImages = new AuthorizeImagesStage();
		DistributeImagesStage distributeImages = new DistributeImagesStage(charityTrap, images, distributionBreaker, cdn);

		UpdateResourceStage<BusinessLogoResource> updateBusiness = new UpdateResourceStage<>(businessTrap, businesses, businessBreaker);
		UpdateResourceStage<CharityLogoResource> updateCharity = new UpdateResourceStage<>(charityTrap, charities, charityBreaker);
		UpdateResourceStage<PartnerLogoResource> updatePartner = new UpdateResourceStage<>(partnerTrap, partners, partnerBreaker);

		CompleteJobStage completeJob = new CompleteJobStage();

		Pipeline<ImageJobContext> pipeline = Pipeline.start(initJob)
				.branch(
						Branch.when(isBusinessResource()).toPath(
								Pipeline.start(authBusiness).end()),
						Branch.when(isCharityResource()).toPath(
								Pipeline.start(authCharity).end()),
						Branch.when(isPartnerResource()).toPath(
								Pipeline.start(authPartner).end()))
				.pipe(processImage)
				.pipe(authImages)
				.pipe(distributeImages)
				.branch(
						Branch.when(isBusinessResource()).toPath(
								Pipeline.start(updateBusiness).end()),
						Branch.when(isCharityResource()).toPath(
								Pipeline.start(updateCharity).end()),
						Branch.when(isPartnerResource()).toPath(
								Pipeline.start(updatePartner).end()))
				.end(completeJob);

		return pipeline;
	}

}
