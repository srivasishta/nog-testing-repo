package com.ngl.micro.image.images;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * The processing status.
 *
 * @author Willy du Preez
 *
 */
public enum ProcessingStatus {

	PENDING(1L),
	SUCCESS(2L),
	FAILED(3L);

	public static ProcessingStatus forId(Long id) {
		Assert.notNull(id);

		for (ProcessingStatus status : ProcessingStatus.values()) {
			if (status.id == id) {
				return status;
			}
		}
		throw new NoSuchElementException("No ProcessingStatus with ID: " + id);
	}


	public static ProcessingStatus forEnum(Enum<?> named) {
		return ProcessingStatus.valueOf(named.name());
	}

	private long id;

	private ProcessingStatus(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
