package com.ngl.micro.image.distribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.hystrix.HystrixCommand;
import com.ngl.middleware.util.Assert;

/**
 * A Hystrix command used to wrap image distribution to a CDN in a circuit breaker.
 *
 * @author Willy du Preez
 *
 */
public class ImageDistributionCommand extends HystrixCommand<ImageDistributionResponse> {

	private static final Logger log = LoggerFactory.getLogger(ImageDistributionCommand.class);

	private ImageDistributionNetwork cdn;
	private ImageDistributionRequest request;

	public ImageDistributionCommand(Setter config, ImageDistributionNetwork cdn, ImageDistributionRequest request) {
		super(config);

		Assert.notNull(config);
		Assert.notNull(cdn);
		Assert.notNull(request);

		this.cdn = cdn;
		this.request = request;
	}

	@Override
	protected ImageDistributionResponse run() throws Exception {
		log.debug("Distribution command executing for job with ID: {}", this.request.getImageJobId());
		return this.cdn.distribute(this.request);
	}

}
