package com.ngl.micro.image.images.pipeline;

import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;

/**
 * A stage in the pipeline that authorize processed image content and
 * ensures quality.
 *
 * @author Willy du Preez
 *
 */
public class AuthorizeImagesStage extends AbstractStage {

	@Override
	public void process(ImageJobContext context) {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_IMAGE_AUTH)) {
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_IMAGE_DISTRIBUTION);
		}
	}

	@Override
	public String getName() {
		return "[auth-images]";
	}

}
