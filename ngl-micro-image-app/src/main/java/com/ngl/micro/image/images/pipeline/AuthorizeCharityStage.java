package com.ngl.micro.image.images.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.images.ImageJobContext;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.micro.image.resource.LogoResource;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.GetResourceCommand;
import com.ngl.micro.image.resource.type.CharityLogoResource;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.integration.breaker.HystrixTrap;

/**
 * Authorizes charity logo uploads.
 *
 * @author Willy du Preez
 *
 */
public class AuthorizeCharityStage extends AbstractStage {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AuthorizeBusinessStage.class);

	private HystrixTrap trap;
	private LogoResourceService<CharityLogoResource> charities;
	private Setter charitiesBreaker;

	public AuthorizeCharityStage(
			HystrixTrap trap,
			LogoResourceService<CharityLogoResource> charities,
			Setter charityBreaker) {
		Assert.notNull(trap);
		Assert.notNull(charities);
		Assert.notNull(charityBreaker);

		this.trap = trap;
		this.charities = charities;
		this.charitiesBreaker = charityBreaker;
	}

	@Override
	public void process(ImageJobContext context) throws InterruptedException {
		if (context.isPendingWithReason(context.getJob(), ProcessingStatusReason.AWAITING_RESOURCE_AUTH)) {
			this.authorizeResource(context);
		}
	}

	private void authorizeResource(ImageJobContext context) throws InterruptedException {
		ImageJob job = context.getJob();
		if (!ApiPolicy.isSystemAccount(job.getPartnerId())) {
			this.updateState(context, ProcessingStatus.FAILED, ProcessingStatusReason.CHARITY_AUTHORIZATION_FAILED);
			return;
		}

		LogoResource charity = this.trap.funnel(() -> new GetResourceCommand<>(
				this.charitiesBreaker,
				this.charities,
				job.getResourceId()));
		if (charity == null) {
			this.updateState(context, ProcessingStatus.FAILED, ProcessingStatusReason.CHARITY_NOT_FOUND);
		}
		else {
			this.updateState(context, ProcessingStatus.PENDING, ProcessingStatusReason.AWAITING_IMAGE_PROCESSING);
		}
	}

	@Override
	public String getName() {
		return "[auth-charity-resource]";
	}

}
