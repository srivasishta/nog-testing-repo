package com.ngl.micro.image.distribute;

import java.util.UUID;

import com.ngl.micro.image.ImageMetadata;

/**
 * Request to distribute images to a CDN.
 *
 * @author Willy du Preez
 *
 */
public class ImageDistributionRequest {

	private UUID imageJobId;
	private ImageMetadata metadata;
	private byte[] data;

	public UUID getImageJobId() {
		return this.imageJobId;
	}

	public void setImageJobId(UUID imageJobId) {
		this.imageJobId = imageJobId;
	}

	public ImageMetadata getMetadata() {
		return this.metadata;
	}

	public void setMetadata(ImageMetadata metadata) {
		this.metadata = metadata;
	}

	public byte[] getData() {
		return this.data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
