CREATE TABLE `processing_status_reason` (

	-- Columns
	`id`            BIGINT(20) NOT NULL,
	`reason`        VARCHAR(50) NOT NULL,

	-- Constraints
	PRIMARY KEY (`id`),
	UNIQUE KEY (`reason`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
