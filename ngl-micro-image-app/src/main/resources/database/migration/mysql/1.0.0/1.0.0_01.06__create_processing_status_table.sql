CREATE TABLE `processing_status` (

	-- Columns
	`id`            BIGINT(20) NOT NULL,
	`status`        VARCHAR(50) NOT NULL,

	-- Constraints
	PRIMARY KEY (`id`),
	UNIQUE KEY (`status`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
