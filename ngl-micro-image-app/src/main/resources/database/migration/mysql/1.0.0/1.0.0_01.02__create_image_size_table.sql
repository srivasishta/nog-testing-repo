CREATE TABLE `image_size` (

	-- Columns
	`id`            BIGINT(20) NOT NULL,
	`size`         VARCHAR(50) NOT NULL,

	-- Constraints
	PRIMARY KEY (`id`),
	UNIQUE KEY (`size`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
