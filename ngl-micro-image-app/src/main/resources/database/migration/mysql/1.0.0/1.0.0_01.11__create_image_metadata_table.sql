CREATE TABLE `image_metadata` (

    -- Columns
    `id`                          BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`                 BINARY(16) NOT NULL,
    `image_size_id`               BIGINT(20) NOT NULL,
    `width`                       INT,
    `height`                      INT,
    `url`                         VARCHAR(255),

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`, `image_size_id`),
    FOREIGN KEY (`image_size_id`) REFERENCES `image_size` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
