CREATE TABLE `image_job` (

    -- Columns
    `id`                             BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`                    BINARY(16) NOT NULL,
    `resource_lookup_id`             BINARY(16) NOT NULL,
    `resource_lookup_type_id`        BIGINT(20) NOT NULL,
    `partner_id`                     BINARY(16) NOT NULL,
    `processing_status_id`           BIGINT(20) NOT NULL,
    `processing_status_reason_id`    BIGINT(20) NOT NULL,
    `created_date`                   DATETIME NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`),
    FOREIGN KEY (`resource_lookup_type_id`) REFERENCES `resource_lookup_type` (`id`),
    FOREIGN KEY (`processing_status_id`) REFERENCES `processing_status` (`id`),
    FOREIGN KEY (`processing_status_reason_id`) REFERENCES `processing_status_reason` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_ij_created_date` ON `image_job` (`created_date`);
CREATE INDEX `idx_ij_partner_id` ON `image_job` (`partner_id`);
CREATE INDEX `idx_ij_resource_lookup_id` ON `image_job` (`resource_lookup_id`);
