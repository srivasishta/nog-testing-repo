CREATE TABLE `resource_lookup_type` (

	-- Columns
	`id`            BIGINT(20) NOT NULL,
	`type`          VARCHAR(50) NOT NULL,

	-- Constraints
	PRIMARY KEY (`id`),
	UNIQUE KEY (`type`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
