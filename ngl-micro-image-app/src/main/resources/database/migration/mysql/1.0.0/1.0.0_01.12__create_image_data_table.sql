CREATE TABLE `image_data` (

    -- Columns
    `id`                 BIGINT(20) NOT NULL AUTO_INCREMENT,
    `metadata_id`        BIGINT(20) NOT NULL,
    `data`               MEDIUMBLOB NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`metadata_id`),
    FOREIGN KEY (`metadata_id`) REFERENCES `image_metadata` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
