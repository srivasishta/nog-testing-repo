package com.ngl.micro.image.images;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.image.ImageApplication;
import com.ngl.micro.image.ImageFTCaseConfiguration;
import com.ngl.micro.image.client.ImageClient;
import com.ngl.micro.image.test_data.TestAccessTokens;
import com.ngl.micro.image.test_data.TestImages;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.util.UUIDS;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		ImageFTCaseConfiguration.class
})
public class ImageSecurityFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Autowired
	private ImageClient client;

	private TestImages testImages;

	public ImageSecurityFTCase() {
		super(ImageApplication.class);
		this.testImages = new TestImages();
	}

	@Test
	public void test_authorization() {
		// It is not considered best practice to test multiple
		// cases in one test method. However, FT cases take
		// longer, there are a lot of authorization tests, and
		// we specifically want to test through the end point.
		this.cmdLoadTestDataSet(this.testImages);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.unauthorized());

		try {
			this.client.getImageResource().upload(
					TestImages.class.getResourceAsStream(TestImages.TEST_IMAGE_RESOURCE), this.testImages.defaultImageProcessingJob());
			Assert.fail("Expecting unauthorized upload to fail (missing scope).");
		} catch (ApplicationException e) {
			Assert.assertTrue(ApplicationStatus.FORBIDDEN.equals(e.getError().getStatus()));
		}

		try {
			this.client.getImageResource().get(UUIDS.type4Uuid());
			Assert.fail("Expecting unauthorized get to fail (missing scope).");
		} catch (ApplicationException e) {
			Assert.assertTrue(ApplicationStatus.FORBIDDEN.equals(e.getError().getStatus()));
		}

		try {
			this.client.getImageResource().list(PageRequest.with(0, 20).build());
			Assert.fail("Expecting unauthorized list to fail (missing scope).");
		} catch (ApplicationException e) {
			Assert.assertTrue(ApplicationStatus.FORBIDDEN.equals(e.getError().getStatus()));
		}

		try {
			this.client.getImageResource().status(UUIDS.type4Uuid());
			Assert.fail("Expecting unauthorized status to fail (missing scope).");
		} catch (ApplicationException e) {
			Assert.assertTrue(ApplicationStatus.FORBIDDEN.equals(e.getError().getStatus()));
		}
	}

}
