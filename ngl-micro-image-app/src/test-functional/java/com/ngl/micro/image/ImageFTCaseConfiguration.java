package com.ngl.micro.image;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.image.client.ImageClient;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerConfiguration;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerProperties;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCaseConfiguration;
import com.ngl.middleware.rest.api.page.QueryParameterParser;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.HttpClient;

@Configuration
@EnableMicroserviceJmsClient
@Import(HornetQEmbeddedServerConfiguration.class)
public class ImageFTCaseConfiguration extends AbstractApplicationFTCaseConfiguration {

	private static final String IMAGE_SERVICE_URL = "http://localhost:9099/service/image/";

	@Bean
	public HornetQEmbeddedServerProperties hornetQEmbeddedServerProperties(
			EnvironmentConfigurationProvider config) {
		return config.getConfiguration(
				HornetQEmbeddedServerProperties.DEFAULT_PREFIX,
				HornetQEmbeddedServerProperties.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ImageClient restClient(
			HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(IMAGE_SERVICE_URL, requestFactory, parser, mapper);
		return new ImageClient(config);
	}

}
