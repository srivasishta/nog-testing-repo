package com.ngl.micro.image.images;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.image.ImageApplication;
import com.ngl.micro.image.ImageFTCaseConfiguration;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.client.ImageClient;
import com.ngl.micro.image.test_data.TestAccessTokens;
import com.ngl.micro.image.test_data.TestImages;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.test.api.ExpectedApplicationException;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		ImageFTCaseConfiguration.class
})
public class ImageFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Autowired
	private ImageClient client;

	private TestImages testImages;

	public ImageFTCase() {
		super(ImageApplication.class);
		this.testImages = new TestImages();
	}

	@Test
	public void test_upload() {
		this.cmdLoadTestDataSet(this.testImages);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.system());

		ImageJob upload = this.testImages.defaultImageProcessingJob();

		ImageJob job = this.client.getImageResource().upload(
				TestImages.class.getResourceAsStream(TestImages.TEST_IMAGE_RESOURCE), upload)
				.getState();

		Assert.assertThat(job.getId(), CoreMatchers.notNullValue());
		Assert.assertThat(job.getPartnerId(), CoreMatchers.is(upload.getPartnerId()));
		Assert.assertThat(job.getResourceId(), CoreMatchers.is(upload.getResourceId()));
		Assert.assertThat(job.getResourceType(), CoreMatchers.is(upload.getResourceType()));
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Assert.assertNotNull(job.getCreatedDate());

	}

}
