package com.ngl.micro.image.images;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.RejectedExecutionException;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ResourceType;
import com.ngl.micro.image.distribute.ImageDistributionNetwork;
import com.ngl.micro.image.distribute.LocalHostedNetwork;
import com.ngl.micro.image.distribute.LocalHostedNetworkProperties;
import com.ngl.micro.image.resource.LogoResourceService;
import com.ngl.micro.image.resource.type.BusinessLogoResource;
import com.ngl.micro.image.resource.type.CharityLogoResource;
import com.ngl.micro.image.resource.type.PartnerLogoResource;
import com.ngl.micro.image.test_data.TestAccessTokens;
import com.ngl.micro.image.test_data.TestImages;
import com.ngl.micro.image.test_data.TestLookupResponses;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DatabaseTestContext;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.integration.batch.AsyncPipeline;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.request.FakeTransport;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.json.ObjectMapperFactory;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class
})
public class ImageJobPipelineITCase extends AbstractDatabaseITCase {

	private static final int DEFAULT_TRANSPORT_LATENCY = 20;
	private static final int DEFAULT_TEST_WAIT_TIME = 1_500;

	private static final Logger log = LoggerFactory.getLogger(ImageJobPipelineITCase.class);

	private static ObjectMapper mapper;

	private FakeTransport fakeTransport;
	private HttpClient httpClient;
	private AsyncPipeline<ImageJobContext> pipeline;
	private ImageService service;
	private ImageJobRepository jobs;
	private ImageRepository data;
	private TestImages testImages;
	private TestLookupResponses testLookupResponses;
	private LogoResourceService<BusinessLogoResource> businesses;
	private LogoResourceService<CharityLogoResource> charities;
	private LogoResourceService<PartnerLogoResource> partners;
	private ImageDistributionNetwork cdn;

	@Autowired
	private PlatformTransactionManager txManager;

	@SuppressWarnings("unused")
	@Autowired
	private EnvironmentConfigurationProvider config;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@BeforeClass
	public static void beforeClass() {
		mapper = new ObjectMapperFactory().getInstance();
	}

	@Override
	@Before
	public void before() {
		super.before();

		this.fakeTransport = new FakeTransport();
		this.httpClient = HttpClient.withTransport(this.fakeTransport)
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.build();

		this.businesses = new LogoResourceService<BusinessLogoResource>(BusinessLogoResource.class, this.httpClient, "");
		this.charities = new LogoResourceService<CharityLogoResource>(CharityLogoResource.class, this.httpClient, "");
		this.partners = new LogoResourceService<PartnerLogoResource>(PartnerLogoResource.class, this.httpClient, "");
		this.jobs = new ImageJobRepository(new DAL(this.sqlContext));
		this.data = new ImageRepository(new DAL(this.sqlContext));
		LocalHostedNetworkProperties properties = new LocalHostedNetworkProperties();
		properties.setPath(DatabaseTestContext.getInstance().getBackupDir().toAbsolutePath().toString());
		this.cdn = new LocalHostedNetwork(properties);
		this.cdn.init();
		this.pipeline = new ImageJobPipelineFactory().newInstance(
				new ImageJobPipelineProperties(),
				this.txManager,
				this.jobs, this.data, this.cdn,
				this.businesses,
				this.charities,
				this.partners);
		this.service = new ImageService(
				this.jobs, this.data, ImageJobValidator.newInstance(), this.pipeline, this.txManager);
		this.testImages= new TestImages();
		this.testLookupResponses = new TestLookupResponses();

		TestSubjects.login(TestAccessTokens.system());
	}

	@After
	public void after() {
		TestSubjects.logout();
	}

	@Test
	public void test_upload_fail__original_too_small() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.businessLookupResponse(mapper),
				this.testLookupResponses.businessLookupResponse(mapper),
				this.testLookupResponses.businessPatchResponse(mapper)));
		this.fakeTransport.setLatency(DEFAULT_TRANSPORT_LATENCY);

		ImageJob job = this.service.uploadImage(
				this.testImages.defaultImageStream(), this.testImages.imageProcessingJob(
						TestLookupResponses.BUSINESS_ID, ResourceType.BUSINESS_LOGO));

		log.info("Asserting pending status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(DEFAULT_TEST_WAIT_TIME);
		log.info("Asserting final status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.FAILED.name()));
		Assert.assertThat(job.getProcessingStatusReason(), CoreMatchers.is(ProcessingStatusReason.PROCESSING_ERROR_ORIGINAL_DIMENSIONS_TOO_SMALL.name()));
	}

	@Test
	public void test_upload_business_logo() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.businessLookupResponse(mapper),
				this.testLookupResponses.businessLookupResponse(mapper),
				this.testLookupResponses.businessPatchResponse(mapper)));
		this.fakeTransport.setLatency(DEFAULT_TRANSPORT_LATENCY);

		ImageJob job = this.service.uploadImage(
				this.testImages.imageStream(TestImages.TEST_IMAGE_PAD_HEIGHT), this.testImages.imageProcessingJob(
						TestLookupResponses.BUSINESS_ID, ResourceType.BUSINESS_LOGO));

		log.info("Asserting pending status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(DEFAULT_TEST_WAIT_TIME);
		log.info("Asserting final status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.SUCCESS.name()));
		Assert.assertThat(job.getProcessingStatusReason(), CoreMatchers.is(ProcessingStatusReason.SUCCESS.name()));
	}

	@Test
	public void test_upload_charity_logo_fail__uploaded_by_non_system_account() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.charityLookupResponse(mapper),
				this.testLookupResponses.charityLookupResponse(mapper),
				this.testLookupResponses.charityPatchResponse(mapper)));
		this.fakeTransport.setLatency(DEFAULT_TRANSPORT_LATENCY);

		ImageJob job = this.service.uploadImage(
				this.testImages.defaultImageStream(), this.testImages.imageProcessingJob(
						TestLookupResponses.CHARITY_ID, ResourceType.CHARITY_LOGO));

		log.info("Asserting pending status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(DEFAULT_TEST_WAIT_TIME);
		log.info("Asserting final status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.FAILED.name()));
		Assert.assertThat(job.getProcessingStatusReason(), CoreMatchers.is(ProcessingStatusReason.CHARITY_AUTHORIZATION_FAILED.name()));
	}

	@Test
	public void test_upload_charity_logo() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.charityLookupResponse(mapper),
				this.testLookupResponses.charityLookupResponse(mapper),
				this.testLookupResponses.charityPatchResponse(mapper)));
		this.fakeTransport.setLatency(DEFAULT_TRANSPORT_LATENCY);

		ImageJob job = this.testImages.imageProcessingJob(TestLookupResponses.CHARITY_ID, ResourceType.CHARITY_LOGO);
		job.setPartnerId(TestAccessTokens.system().getSubjectClaim());

		job = this.service.uploadImage(this.testImages.imageStream(TestImages.TEST_IMAGE_PAD_HEIGHT), job);

		log.info("Asserting pending status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(DEFAULT_TEST_WAIT_TIME);
		log.info("Asserting final status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.SUCCESS.name()));
		Assert.assertThat(job.getProcessingStatusReason(), CoreMatchers.is(ProcessingStatusReason.SUCCESS.name()));
	}

	@Test
	public void test_upload_partner_logo() throws Exception {
		this.fakeTransport.addResponses(Arrays.asList(
				this.testLookupResponses.partnerLookupResponse(mapper),
				this.testLookupResponses.partnerPatchResponse(mapper)));
		this.fakeTransport.setLatency(DEFAULT_TRANSPORT_LATENCY);

		ImageJob job = this.service.uploadImage(
				this.testImages.imageStream(TestImages.TEST_IMAGE_PAD_HEIGHT), this.testImages.imageProcessingJob(
						TestLookupResponses.PARTNER_ID, ResourceType.PARTNER_LOGO));

		log.info("Asserting pending status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.PENDING.name()));
		Thread.sleep(DEFAULT_TEST_WAIT_TIME);
		log.info("Asserting final status.");
		job = this.jobs.get(job.getId());
		Assert.assertThat(job.getProcessingStatus(), CoreMatchers.is(ProcessingStatus.SUCCESS.name()));
		Assert.assertThat(job.getProcessingStatusReason(), CoreMatchers.is(ProcessingStatusReason.SUCCESS.name()));
	}

	@Test
	@Ignore("Tasks get rejected, but service uses submitSafely")
	public void test_submit_thread_pool_exhausted() throws IOException {
		this.thrown.expect(RejectedExecutionException.class);
		this.fakeTransport.setLatency(10_000);

		for (int idx = 0; idx < 21; idx++) {
			this.service.uploadImage(this.testImages.defaultImageStream(), this.testImages.imageProcessingJob(
					TestLookupResponses.CHARITY_ID, ResourceType.CHARITY_LOGO));
		}

	}

}
