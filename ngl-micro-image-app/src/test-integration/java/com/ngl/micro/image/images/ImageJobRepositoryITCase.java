package com.ngl.micro.image.images;
import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.test_data.TestImages;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.UUIDS;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ImageJobRepositoryITCase extends AbstractDatabaseITCase {

	private ImageJobRepository repo;
	private TestImages testImages;

	@Before
	public void setup() {
		this.testImages = new TestImages();
		this.repo = new ImageJobRepository(new DAL(this.sqlContext));
	}

	@Test
	public void test_create_job() {
		UUID newId = UUIDS.type4Uuid();
		ImageJob job = this.testImages.defaultImageProcessingJob();
		job.setId(newId);
		job.setProcessingStatus(ProcessingStatus.PENDING.name());
		job.setProcessingStatusReason(ProcessingStatusReason.PENDING.name());
		job.setCreatedDate(Data.T_0);

		job = this.repo.add(job);

		assertEquals(newId, job.getId());
		assertEquals(TestImages.DEFAULT_PARTNER_ID, job.getPartnerId());
		assertEquals(TestImages.DEFAULT_RESOURCE_ID, job.getResourceId());
		assertEquals(TestImages.DEFAULT_RESOURCE_TYPE, job.getResourceType());
		assertEquals(ProcessingStatus.PENDING.name(), job.getProcessingStatus());
		assertEquals(ProcessingStatusReason.PENDING.name(), job.getProcessingStatusReason());
		assertEquals(Data.T_0, job.getCreatedDate());
	}

	@Test
	public void test_get_job() {
		this.cmdLoadTestDataSet(this.testImages);
		ImageJob job = this.repo.get(TestImages.JOB_1_ID);

		assertEquals(TestImages.JOB_1_ID, job.getId());
		assertEquals(TestImages.DEFAULT_PARTNER_ID, job.getPartnerId());
		assertEquals(TestImages.DEFAULT_RESOURCE_ID, job.getResourceId());
		assertEquals(TestImages.DEFAULT_RESOURCE_TYPE, job.getResourceType());
		assertEquals(TestImages.JOB_1_STATUS.name(), job.getProcessingStatus());
		assertEquals(TestImages.JOB_1_STATUS_REASON.name(), job.getProcessingStatusReason());
	}

	@Test
	public void test_get_missing_job() {
		Assert.assertNull(this.repo.get(Data.TYPE4_UUID_1));
	}

	@Test
	public void test_list_jobs() {
		this.cmdLoadTestDataSet(this.testImages);
		Page<ImageJob> jobs = this.repo.get(PageRequest.with(0, 5).build());
		Assert.assertEquals(1, jobs.getItems().size());
	}

//
//	@Test
//	public void test_update_charity() {
//		Charity reference = this.testCharities.newCharity();
//		reference.setStatus(CharityStatus.ACTIVE);
//		Charity working = this.repo.add(reference);
//		working.setName("Updated Name");
//		working.setDescription("Updated Description");
//		working.setStatus(CharityStatus.INACTIVE);
//		working.setReach(new Reach(ReachType.NATIONAL, asSet(new Area(Country.ZA, asSet("GA", "KZ", "WC")))));
//		working.setCategories(asSet(CharityCategory.EDUCATION, CharityCategory.ENVIRONMENT));
//		this.repo.update(working);
//
//		assertEquals(working, this.repo.get(working.getId()));
//	}
//

}