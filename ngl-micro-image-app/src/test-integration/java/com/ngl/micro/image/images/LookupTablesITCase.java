package com.ngl.micro.image.images;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.image.dal.generated.jooq.tables.daos.JImageSizeDao;
import com.ngl.micro.image.dal.generated.jooq.tables.daos.JProcessingStatusDao;
import com.ngl.micro.image.dal.generated.jooq.tables.daos.JProcessingStatusReasonDao;
import com.ngl.micro.image.dal.generated.jooq.tables.daos.JResourceLookupTypeDao;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JImageSize;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JProcessingStatus;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JProcessingStatusReason;
import com.ngl.micro.image.dal.generated.jooq.tables.pojos.JResourceLookupType;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class LookupTablesITCase extends AbstractDatabaseITCase {

	private JImageSizeDao sizeDao;
	private JProcessingStatusDao statusDao;
	private JProcessingStatusReasonDao reasonDao;
	private JResourceLookupTypeDao typeDao;

	@Override
	public void before() {
		super.before();
		this.sizeDao = new JImageSizeDao(this.sqlContext);
		this.statusDao = new JProcessingStatusDao(this.sqlContext);
		this.reasonDao = new JProcessingStatusReasonDao(this.sqlContext);
		this.typeDao = new JResourceLookupTypeDao(this.sqlContext);
	}

	@Test
	public void test_size_mapping() throws Exception {
		JImageSize value;
		assertEquals("Expect equal count.", ImageSize.values().length, this.sizeDao.count());
		for (ImageSize type : ImageSize.values()) {
			value = this.sizeDao.fetchOneByJSize(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), value.getSize());
			assertEquals("Expect type id equals persisted id: ", type.getId(), value.getId().longValue());
		}
	}

	@Test
	public void test_status_mapping() throws Exception {
		JProcessingStatus value;
		assertEquals("Expect equal count.", ProcessingStatus.values().length, this.statusDao.count());
		for (ProcessingStatus type : ProcessingStatus.values()) {
			value = this.statusDao.fetchOneByJStatus(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), value.getStatus());
			assertEquals("Expect type id equals persisted id: ", type.getId(), value.getId().longValue());
		}
	}

	@Test
	public void test_reason_mapping() throws Exception {
		JProcessingStatusReason value;
		assertEquals("Expect equal count.", ProcessingStatusReason.values().length, this.reasonDao.count());
		for (ProcessingStatusReason type : ProcessingStatusReason.values()) {
			value = this.reasonDao.fetchOneByJReason(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), value.getReason());
			assertEquals("Expect type id equals persisted id: ", type.getId(), value.getId().longValue());
		}
	}

	@Test
	public void test_type_mapping() throws Exception {
		JResourceLookupType value;
		assertEquals("Expect equal count.", ResourceLookupType.values().length, this.typeDao.count());
		for (ResourceLookupType type : ResourceLookupType.values()) {
			value = this.typeDao.fetchOneByJType(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), value.getType());
			assertEquals("Expect type id equals persisted id: ", type.getId(), value.getId().longValue());
		}
	}
}
