package com.ngl.micro.image.test_data;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.image.resource.type.BusinessLogoResource;
import com.ngl.micro.image.resource.type.CharityLogoResource;
import com.ngl.micro.image.resource.type.PartnerLogoResource;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.request.ImmutableResponse;
import com.ngl.middleware.test.data.Data;

public class TestLookupResponses {

	public static final UUID BUSINESS_ID = Data.ID_6;
	public static final UUID CHARITY_ID = Data.ID_7;
	public static final UUID PARTNER_ID = Data.ID_2;

	public ImmutableResponse businessLookupResponse(ObjectMapper mapper) throws Exception {
		BusinessLogoResource response = new BusinessLogoResource();
		response.setId(BUSINESS_ID);
		response.setPartnerId(PARTNER_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse businessPatchResponse(ObjectMapper mapper) throws Exception {
		BusinessLogoResource response = new BusinessLogoResource();
		response.setId(BUSINESS_ID);
		response.setPartnerId(PARTNER_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse charityLookupResponse(ObjectMapper mapper) throws Exception {
		CharityLogoResource response = new CharityLogoResource();
		response.setId(CHARITY_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse charityPatchResponse(ObjectMapper mapper) throws Exception {
		CharityLogoResource response = new CharityLogoResource();
		response.setId(CHARITY_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse partnerLookupResponse(ObjectMapper mapper) throws Exception {
		PartnerLogoResource response = new PartnerLogoResource();
		response.setId(PARTNER_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

	public ImmutableResponse partnerPatchResponse(ObjectMapper mapper) throws Exception {
		PartnerLogoResource response = new PartnerLogoResource();
		response.setId(PARTNER_ID);
		response.setStatus(ResourceStatus.ACTIVE);

		return new ImmutableResponse(200, "OK", new Headers(), mapper.writeValueAsBytes(response));
	}

}
