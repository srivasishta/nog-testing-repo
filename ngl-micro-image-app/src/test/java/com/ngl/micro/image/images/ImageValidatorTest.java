package com.ngl.micro.image.images;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.test_data.TestImages;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class ImageValidatorTest {

	private ImageJobValidator validator;
	private TestImages testImages;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Before
	public void before() {
		this.validator = ImageJobValidator.newInstance();
		this.testImages = new TestImages();
	}

	@Test
	public void test_validate_image_resource() {
		this.validator.validate(this.testImages.defaultImageProcessingJob());
	}

	@Test
	public void test_fail_null_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError(ImageJob.Fields.RESOURCE_ID, ValidationErrorCode.INVALID)
				.expectValidationError(ImageJob.Fields.RESOURCE_TYPE, ValidationErrorCode.INVALID)
				.expectValidationError(ImageJob.Fields.PARTNER_ID, ValidationErrorCode.INVALID);

		this.validator.validate(new ImageJob());
	}

}