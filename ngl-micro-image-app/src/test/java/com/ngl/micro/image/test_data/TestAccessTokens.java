package com.ngl.micro.image.test_data;

import java.util.UUID;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.util.Collections;
import com.ngl.middleware.util.UUIDS;

public class TestAccessTokens {

	private static final TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);

	public static final UUID UNAUTHORIZED_ID = UUIDS.type4Uuid();

	public static TestAccessToken system() {
		return TestAccessToken.newBasic()
				.issuedDate(SYSTEM.getIssuedDate())
				.expiresDate(SYSTEM.getExpiresDate())
				.subjectClaim(SYSTEM.getSubjectClaim())
				.clientIdClaim(SYSTEM.getClientIdClaim())
				.scope(SYSTEM.getScope());
	}

	public static TestAccessToken unauthorized() {
		return TestAccessToken.newBasic()
				.issuedDate(SYSTEM.getIssuedDate())
				.expiresDate(SYSTEM.getExpiresDate())
				.subjectClaim(UNAUTHORIZED_ID)
				.clientIdClaim(UNAUTHORIZED_ID)
				.scope(Collections.asSet(Scopes.BUSINESS_READ_SCOPE));
	}

}
