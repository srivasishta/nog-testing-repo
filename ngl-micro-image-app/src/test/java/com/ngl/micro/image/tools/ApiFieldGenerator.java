package com.ngl.micro.image.tools;

import java.util.Collections;

import com.ngl.micro.image.ImageMetadata;
import com.ngl.micro.image.ImageJob;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields.
 *
 * @author Willy du Preez
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Collections.emptyList()).generate(ImageMetadata.class);
		new ApiFieldsGenerator(Collections.emptyList()).generate(ImageJob.class);
	}

}
