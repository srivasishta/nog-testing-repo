package com.ngl.micro.image.test_data;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.UUID;

import org.jooq.DSLContext;
import org.springframework.util.Assert;

import com.ngl.micro.image.ImageJob;
import com.ngl.micro.image.ResourceType;
import com.ngl.micro.image.dal.generated.jooq.Tables;
import com.ngl.micro.image.images.ProcessingStatus;
import com.ngl.micro.image.images.ProcessingStatusReason;
import com.ngl.micro.image.images.ResourceLookupType;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Streams;

public class TestImages implements JooqTestDataSet {

	public static final String TEST_IMAGE_RESOURCE = "/image/image.png";
	public static final String TEST_IMAGE_PAD_WIDTH = "/image/pad_width.png";
	public static final String TEST_IMAGE_PAD_HEIGHT = "/image/pad_height.png";
	public static final String TEST_IMAGE_FORMAT_PNG = "/image/format_png.png";
	public static final String TEST_IMAGE_FORMAT_JPG = "/image/format_jpeg.jpg";

	public static final UUID JOB_1_ID = Data.ID_1;
	public static final ProcessingStatus JOB_1_STATUS = ProcessingStatus.SUCCESS;
	public static final ProcessingStatusReason JOB_1_STATUS_REASON = ProcessingStatusReason.SUCCESS;

	public static final UUID DEFAULT_RESOURCE_ID = TestLookupResponses.BUSINESS_ID;
	public static final ResourceType DEFAULT_RESOURCE_TYPE = ResourceType.BUSINESS_LOGO;
	public static final UUID DEFAULT_PARTNER_ID = TestLookupResponses.PARTNER_ID;

	public ImageJob defaultImageProcessingJob() {
		ImageJob job = new ImageJob();
		job.setResourceId(DEFAULT_RESOURCE_ID);
		job.setResourceType(DEFAULT_RESOURCE_TYPE);
		job.setPartnerId(DEFAULT_PARTNER_ID);
		job.setCreatedDate(Data.T_MINUS_1H);
		return job;
	}

	public ImageJob imageProcessingJob(UUID resourceId, ResourceType resourceType) {
		ImageJob job = new ImageJob();
		job.setResourceId(resourceId);
		job.setResourceType(resourceType);
		job.setPartnerId(DEFAULT_PARTNER_ID);
		job.setCreatedDate(Data.T_MINUS_1H);
		return job;
	}

	public InputStream defaultImageStream() {
		InputStream is = TestImages.class.getResourceAsStream(TEST_IMAGE_RESOURCE);
		Assert.notNull(is);
		return is;
	}

	public InputStream imageStream(String resource) {
		InputStream is = TestImages.class.getResourceAsStream(resource);
		Assert.notNull(is);
		return is;
	}

	public byte[] imageBytes(String resource) {
		try (InputStream in = TestImages.class.getResourceAsStream(resource);
				ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			Assert.notNull(in);
			Streams.copyLarge(in, out);
			return out.toByteArray();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void loadData(DSLContext sql) {
		sql.insertInto(Tables.IMAGE_JOB)
				.set(Tables.IMAGE_JOB.RESOURCE_ID, JOB_1_ID)
				.set(Tables.IMAGE_JOB.PARTNER_ID, DEFAULT_PARTNER_ID)
				.set(Tables.IMAGE_JOB.RESOURCE_LOOKUP_TYPE_ID, ResourceLookupType.forEnum(DEFAULT_RESOURCE_TYPE).getId())
				.set(Tables.IMAGE_JOB.RESOURCE_LOOKUP_ID, DEFAULT_RESOURCE_ID)
				.set(Tables.IMAGE_JOB.PROCESSING_STATUS_ID, JOB_1_STATUS.getId())
				.set(Tables.IMAGE_JOB.PROCESSING_STATUS_REASON_ID, JOB_1_STATUS_REASON.getId())
				.set(Tables.IMAGE_JOB.CREATED_DATE, Data.T_MINUS_1H)
				.execute();
	}

}
