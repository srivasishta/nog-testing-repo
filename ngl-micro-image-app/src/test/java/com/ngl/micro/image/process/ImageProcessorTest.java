package com.ngl.micro.image.process;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.ngl.micro.image.test_data.TestImages;

@Ignore
public class ImageProcessorTest {

	private TestImages testImages;

	@Before
	public void before() {
		this.testImages = new TestImages();
	}

	@Test
	public void test_read() throws Exception {
		byte[] dataJpg = this.testImages.imageBytes(TestImages.TEST_IMAGE_FORMAT_JPG);
		byte[] dataPng = this.testImages.imageBytes(TestImages.TEST_IMAGE_FORMAT_PNG);
		BufferedImage imageJpg = ImageProcessor.toBufferedImage(dataJpg);
		BufferedImage imagePng = ImageProcessor.toBufferedImage(dataPng);
		imageJpg.flush();
		imagePng.flush();
	}

	@Test
	public void test_padding() throws Exception {
		byte[] data = this.testImages.imageBytes(TestImages.TEST_IMAGE_PAD_WIDTH);
		BufferedImage image = ImageProcessor.toBufferedImage(data);
		BufferedImage padded = ImageProcessor.pad(image, 4, 3);
		BufferedImage resized = ImageProcessor.resize(padded, 640, 480);
		this.view(resized);
		Thread.sleep(10000);
		image.flush();
		padded.flush();
		resized.flush();
	}

	@Test
	public void test_resize() throws Exception {
		byte[] data = this.testImages.imageBytes(TestImages.TEST_IMAGE_PAD_WIDTH);
		BufferedImage image = ImageProcessor.toBufferedImage(data);
		BufferedImage resized = ImageProcessor.resize(image, 640, 480);
		image.flush();
		resized.flush();
	}

	private void view(BufferedImage image) {
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(image)));
		frame.pack();
		frame.setVisible(true);
	}

}
