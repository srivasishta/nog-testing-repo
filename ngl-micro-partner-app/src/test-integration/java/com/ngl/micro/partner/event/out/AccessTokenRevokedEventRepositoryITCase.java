package com.ngl.micro.partner.event.out;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

/**
 * Tests for revoked access token events.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class AccessTokenRevokedEventRepositoryITCase extends AbstractDatabaseITCase {

	private AccessTokenRevokedEventRepository repo;

	private TestPartners testPartners;
	private TestClients testClients;
	private TestAccessTokens testTokens;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new AccessTokenRevokedEventRepository(sqlContext);
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
		this.testTokens = new TestAccessTokens();
	}

	@Test
	public void test_create_token_revoked_event() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);
		this.cmdLoadTestDataSet(this.testTokens);

		AccessTokenRevokedEvent event = this.repo.add(
				TestAccessTokens.TOKEN_ID,
				Data.T_PLUS_10m,
				Data.T_PLUS_1H);

		assertEquals(TestAccessTokens.TOKEN_ID, event.getTokenId());
		assertEquals(Data.T_PLUS_10m, event.getCreated());
		assertEquals(Data.T_PLUS_1H, event.getExpirationDate());
	}

}