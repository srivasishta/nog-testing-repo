package com.ngl.micro.partner.partner;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class PartnerServiceITCase extends AbstractDatabaseITCase {

	private PartnerService service;
	private PartnerRepository repo;
	private TestPartners testPartners;

	@Before
	public void setup() {
		this.testPartners = new TestPartners();
		this.repo = new PartnerRepository(this.sqlContext);
		this.service = new PartnerService(PartnerValidator.newInstance(), this.repo, null);
	}

	@Test
	public void test_update_Partner_logo_as_system() {
		TestSubjects.login(TestAccessTokens.SYSTEM);
		Partner base = this.service.createPartner(this.testPartners.partner());
		Partner working = this.service.getPartnerById(base.getId());

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.service.updatePartner(base.getId(), patch);
		Partner patched = this.service.getPartnerById(base.getId());

		Assert.assertThat(patched, CoreMatchers.is(working));
	}

	@Test
	public void test_update_Partner_logo_as_partner() {
		TestSubjects.login(TestAccessTokens.full());
		this.cmdLoadTestDataSet(this.testPartners);
		Partner base = this.service.getPartnerById(TestPartners.PARTNER_ID);
		Partner working = this.service.getPartnerById(TestPartners.PARTNER_ID);

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.service.updatePartner(base.getId(), patch);
		Partner patched = this.service.getPartnerById(base.getId());

		Assert.assertThat(patched, CoreMatchers.is(base));
	}

}