package com.ngl.micro.partner.partner;

import static com.ngl.micro.partner.partner.Partner.PartnerStatus.ACTIVE;
import static com.ngl.micro.partner.partner.Partner.PartnerStatus.INACTIVE;
import static com.ngl.micro.partner.test_data.TestPartners.PARTNER_PK;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

/**
 * The Class PartnerStatusHistoryRepositoryITCase.
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class PartnerStatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private PartnerStatusHistoryRepository repo;
	private TestPartners partners;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new PartnerStatusHistoryRepository(this.sqlContext);
		this.partners = new TestPartners();
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.partners);

		PartnerStatusRecord status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setStatus(PARTNER_PK, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Status reason update
		this.repo.setStatus(PARTNER_PK, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());

		//Status only
		this.repo.setStatus(PARTNER_PK, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());

		//Reason only
		this.repo.setStatus(PARTNER_PK, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Empty reason
		this.repo.setStatus(PARTNER_PK, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getPartnerStatus(PARTNER_PK);
		assertEquals("", status.getStatusReason());
	}

}