package com.ngl.micro.partner.token;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.client.AccessToken;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

/**
 * The Class AccessTokenRepositoryITCase.
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class AccessTokenRepositoryITCase extends AbstractDatabaseITCase {

	private AccessTokenRepository repo;
	private TestPartners testPartners;
	private TestClients testClients;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new AccessTokenRepository(sqlContext);
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
	}

	@Test
	public void test_save_token() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);

		AccessToken accessToken = this.repo.add(
				Data.ID_3,
				Data.ID_7,
				Data.T_0,
				Data.T_PLUS_1H);

		assertNotNull(accessToken.getId());
		assertEquals(Data.ID_7, accessToken.getId());
		assertEquals(Data.T_0, accessToken.getIssuedDate());
		assertEquals(Data.T_PLUS_1H, accessToken.getExpiresDate());
		assertEquals(null, accessToken.getRevokedDate());
	}

	@Test
	public void test_update_revoked_date() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);

		AccessToken accessToken = this.repo.add(Data.ID_3, Data.ID_7, Data.T_0, Data.T_PLUS_1H);
		assertTrue(this.repo.revoke(accessToken.getId()).isPresent());
		accessToken = this.repo.getById(accessToken.getId());

		assertTrue(accessToken.getRevokedDate().isAfter(Data.T_MINUS_1s));
		assertTrue(accessToken.getRevokedDate().isBefore(Data.T_PLUS_10M));

		assertFalse(this.repo.revoke(accessToken.getId()).isPresent());
	}

}