package com.ngl.micro.partner.partner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.partner.Partner.PartnerStatus;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Time;

/**
 * The Class PartnerRepositoryITCase.
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class PartnerRepositoryITCase extends AbstractDatabaseITCase {

	private PartnerRepository repo;
	private TestPartners partners;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new PartnerRepository(this.sqlContext);
		this.partners = new TestPartners();
	}

	@Test
	public void test_create_partner() {
		Partner partner = this.partners.partner();
		Partner created = this.repo.add(partner, Time.utcNow());
		partner.setLogoImages(new ImageSet());
		Assert.assertEquals(partner, created);
	}

	@Test
	public void test_get_partner() {
		this.cmdLoadTestDataSet(this.partners);
		Partner fetched = this.repo.get(TestPartners.PARTNER_ID);
		assertEquals(TestPartners.PARTNER_ID, fetched.getId());
		assertEquals(TestPartners.PARTNER_NAME, fetched.getName());
		assertEquals(PartnerStatus.ACTIVE, fetched.getStatus());
		assertEquals(Data.REASON_ACTIVATED, fetched.getStatusReason());
	}

	@Test
	public void test_get_non_existing_partner() {
		assertNull(this.repo.get(Data.ID_9));
	}

	@Test
	public void test_list_partners() {
		this.cmdLoadTestDataSet(this.partners);
		Page<Partner> partners = this.repo.get(PageRequest.with(1, 2).build());
		assertEquals(1, partners.getPageNumber());
		assertEquals(5, partners.getTotalItems());
		assertEquals(2, partners.getPageSize());
		assertEquals(2, partners.getNumberOfItems());
	}

	@Test
	public void test_update_partner() {
		this.cmdLoadTestDataSet(this.partners);
		Partner base = this.repo.get(TestPartners.PARTNER_ID);
		base.setName(Data.NAME_9);
		base.setStatus(PartnerStatus.INACTIVE);
		base.setStatusReason(Data.REASON_DEACTIVATED);
		base.setLogoImages(this.partners.partner().getLogoImages());
		this.repo.update(base, Data.T_PLUS_1m);
		Partner updated = this.repo.get(TestPartners.PARTNER_ID);
		assertEquals(Data.NAME_9, updated.getName());
		assertEquals(PartnerStatus.INACTIVE, updated.getStatus());
		assertEquals(Data.REASON_DEACTIVATED, updated.getStatusReason());
		assertEquals(this.partners.partner().getLogoImages(), updated.getLogoImages());
	}

}