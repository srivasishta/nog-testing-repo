package com.ngl.micro.partner.client;

import static com.ngl.micro.partner.client.Client.ClientStatus.ACTIVE;
import static com.ngl.micro.partner.client.Client.ClientStatus.INACTIVE;
import static com.ngl.micro.partner.test_data.TestClients.CLIENT_PK;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

/**
 * The Class PartnerStatusHistoryRepositoryITCase.
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ClientStatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private ClientStatusHistoryRepository repo;
	private TestPartners partners;
	private TestClients clients;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new ClientStatusHistoryRepository(this.sqlContext);
		this.partners = new TestPartners();
		this.clients = new TestClients();
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.partners);
		this.cmdLoadTestDataSet(this.clients);

		ClientStatusRecord status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setStatus(CLIENT_PK, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Status reason update
		this.repo.setStatus(CLIENT_PK, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());

		//Status only
		this.repo.setStatus(CLIENT_PK, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());

		//Reason only
		this.repo.setStatus(CLIENT_PK, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Empty reason
		this.repo.setStatus(CLIENT_PK, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getClientStatus(CLIENT_PK);
		assertEquals("", status.getStatusReason());
	}

}