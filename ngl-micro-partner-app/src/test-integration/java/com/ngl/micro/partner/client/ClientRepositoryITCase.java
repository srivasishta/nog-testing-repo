package com.ngl.micro.partner.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.test.data.Data;

/**
 * Client repo integration tests.
 *
 * TODO Test clients for partner id and updating client statuses
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ClientRepositoryITCase extends AbstractDatabaseITCase {

	private TestPartners testPartners;
	private TestClients testClients;

	private ClientRepository repo;

	@Override
	@Before
	public void before() {
		super.before();
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
		this.repo = new ClientRepository(this.sqlContext);
	}

	@Test
	public void test_create_client() {
		this.cmdLoadTestDataSet(this.testPartners);

		Client client = this.testClients.newClient();
		Client created = this.repo.addClient(client);
		client.setId(created.getId());
		created.setClientSecret(this.repo.getSecret(created.getId()));

		assertEquals(client, created);
	}

	@Test
	public void test_get_by_client_id() {
		this.cmdLoadTestDataSet(this.testPartners);

		Client client = this.repo.addClient(this.testClients.newClient());
		Client fetched = this.repo.getClient(client.getId());

		assertEquals(client, fetched);
	}

	@Test
	public void test_get_non_existing_account() {
		assertNull(this.repo.getClient(Data.ID_9));
	}

	@Test
	public void test_list_clients() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);

		Page<Client> page = this.repo.getClients(PageRequest.with(1, 2).build());
		assertEquals(1, page.getPageNumber());
		assertEquals(5, page.getTotalItems());
		assertEquals(2, page.getPageSize());
		assertEquals(2, page.getNumberOfItems());
	}

	@Test
	public void test_update_client() {
		this.cmdLoadTestDataSet(this.testPartners);

		Client base = this.testClients.newClient();

		Client working = this.repo.addClient(base);
		working.setClientSecret(Data.STRING_6);
		working.setStatus(ClientStatus.INACTIVE);
		working.setStatusReason(Data.REASON_DEACTIVATED);

		working.getScope().add(Scopes.PARTNER_READ_SCOPE);
		working.getScope().remove(Scopes.BUSINESS_WRITE_SCOPE);

		this.repo.updateClient(working);
		Client updated = this.repo.getClient(working.getId());
		assertEquals(working.getClientName(), updated.getClientName());

		assertEquals(working.getStatus(), updated.getStatus());
		assertEquals(working.getStatusReason(), updated.getStatusReason());

		assertEquals(working.getScope(), updated.getScope());
		assertEquals(working.getClientSecretExpiresAt(), updated.getClientSecretExpiresAt());
		assertEquals(working.getClientLastModifiedAt(), updated.getClientLastModifiedAt());
		assertEquals(working.getAccessTokenExpiresIn(), updated.getAccessTokenExpiresIn());

		//Unchanged
		assertEquals(null, updated.getClientSecret());
		assertEquals(base.getPartnerId(), updated.getPartnerId());
		assertEquals(base.getClientIdIssuedAt(), updated.getClientIdIssuedAt());
	}

}