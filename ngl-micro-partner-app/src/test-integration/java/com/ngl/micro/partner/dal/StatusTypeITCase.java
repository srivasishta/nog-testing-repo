//package com.ngl.micro.partner.dal;
//
//import static org.junit.Assert.assertEquals;
//
//import javax.ws.rs.core.Response.StatusType;
//
//import org.junit.Test;
//import org.springframework.test.context.ContextConfiguration;
//
//import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JStatusTypeDao;
//import com.ngl.micro.shared.contracts.ResourceStatus;
//import com.ngl.middleware.database.test.AbstractDatabaseITCase;
//import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
//
//@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
//public class StatusTypeITCase extends AbstractDatabaseITCase {
//
//	private JStatusTypeDao statusDao;
//
//	@Override
//	public void before() {
//		super.before();
//		statusDao = new JStatusTypeDao(sqlContext);
//	}
//
//	@Test
//	public void test_valid_mapping() throws Exception {
//		com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JStatusType status;
//		assertEquals("Expect equal status type count.", ResourceStatus.values().length, statusDao.count());
//		for (ResourceStatus type : ResourceStatus.values()) {
//			status = statusDao.fetchOneByJStatus(type.name());
//			assertEquals("Expect type name equals persisted name", type.name(), status.getStatus());
//			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId());
//			assertEquals("Expect id mapping.", type.getId(), ResourceStatus.getIdFrom(status.getStatus()));
//			assertEquals("Expect name mapping.", type.name(), ResourceStatus.getNameFrom(type.getId()));
//		}
//		assertEquals("Expect null mapping.", null, ResourceStatus.getNameFrom(3L));
//	}
//}
