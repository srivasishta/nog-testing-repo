/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables;


import com.ngl.micro.partner.dal.generated.jooq.JNglMicroPartner;
import com.ngl.micro.partner.dal.generated.jooq.Keys;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JClientRecord;
import com.ngl.middleware.database.mapping.extension.UUIDTypeConverter;
import com.ngl.middleware.database.mapping.extension.ZonedDateTimeConverter;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JClient extends TableImpl<JClientRecord> {

	private static final long serialVersionUID = -1278249582;

	/**
	 * The reference instance of <code>ngl_micro_partner.client</code>
	 */
	public static final JClient CLIENT = new JClient();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JClientRecord> getRecordType() {
		return JClientRecord.class;
	}

	/**
	 * The column <code>ngl_micro_partner.client.id</code>.
	 */
	public final TableField<JClientRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client.partner_id</code>.
	 */
	public final TableField<JClientRecord, Long> PARTNER_ID = createField("partner_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client.client_id</code>.
	 */
	public final TableField<JClientRecord, UUID> CLIENT_ID = createField("client_id", org.jooq.impl.SQLDataType.BINARY.length(16).nullable(false), this, "", new UUIDTypeConverter());

	/**
	 * The column <code>ngl_micro_partner.client.client_name</code>.
	 */
	public final TableField<JClientRecord, String> CLIENT_NAME = createField("client_name", org.jooq.impl.SQLDataType.VARCHAR.length(50).nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client.client_secret</code>.
	 */
	public final TableField<JClientRecord, String> CLIENT_SECRET = createField("client_secret", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client.client_id_issued_at</code>.
	 */
	public final TableField<JClientRecord, ZonedDateTime> CLIENT_ID_ISSUED_AT = createField("client_id_issued_at", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new ZonedDateTimeConverter());

	/**
	 * The column <code>ngl_micro_partner.client.client_last_modified_at</code>.
	 */
	public final TableField<JClientRecord, ZonedDateTime> CLIENT_LAST_MODIFIED_AT = createField("client_last_modified_at", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new ZonedDateTimeConverter());

	/**
	 * The column <code>ngl_micro_partner.client.client_secret_expires_at</code>.
	 */
	public final TableField<JClientRecord, ZonedDateTime> CLIENT_SECRET_EXPIRES_AT = createField("client_secret_expires_at", org.jooq.impl.SQLDataType.TIMESTAMP, this, "", new ZonedDateTimeConverter());

	/**
	 * The column <code>ngl_micro_partner.client.access_token_expires_in</code>.
	 */
	public final TableField<JClientRecord, Long> ACCESS_TOKEN_EXPIRES_IN = createField("access_token_expires_in", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_partner.client</code> table reference
	 */
	public JClient() {
		this("client", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_partner.client</code> table reference
	 */
	public JClient(String alias) {
		this(alias, CLIENT);
	}

	private JClient(String alias, Table<JClientRecord> aliased) {
		this(alias, aliased, null);
	}

	private JClient(String alias, Table<JClientRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroPartner.NGL_MICRO_PARTNER, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JClientRecord, Long> getIdentity() {
		return Keys.IDENTITY_CLIENT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JClientRecord> getPrimaryKey() {
		return Keys.KEY_CLIENT_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JClientRecord>> getKeys() {
		return Arrays.<UniqueKey<JClientRecord>>asList(Keys.KEY_CLIENT_PRIMARY, Keys.KEY_CLIENT_CLIENT_ID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JClientRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JClientRecord, ?>>asList(Keys.CLIENT_IBFK_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClient as(String alias) {
		return new JClient(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JClient rename(String name) {
		return new JClient(name, null);
	}
}
