package com.ngl.micro.partner.partner;

import java.time.ZonedDateTime;

import com.ngl.micro.partner.partner.Partner.PartnerStatus;

public class PartnerStatusRecord {

	private Long id;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;
	private PartnerStatus status;
	private String statusReason;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public PartnerStatus getStatus() {
		return this.status;
	}

	public void setStatus(PartnerStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
}