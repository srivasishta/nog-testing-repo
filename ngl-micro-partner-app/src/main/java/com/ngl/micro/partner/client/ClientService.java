package com.ngl.micro.partner.client;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.UUID;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.security.crypto.secret.SecretHashStrategy;
import com.ngl.middleware.security.oauth2.client.ClientIdentifierStrategy;
import com.ngl.middleware.security.oauth2.client.ClientSecretStrategy;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.Time;

/**
 * The partner service.
 *
 * @author Willy du Preez
 * @author Paco Mendes
 *
 */
@Transactional(rollbackOn = Exception.class)
public class ClientService {

	public static final Logger log = LoggerFactory.getLogger(ClientService.class);

	private ClientIdentifierStrategy identifierStrategy;
	private ClientSecretStrategy secretStrategy;
	private SecretHashStrategy hashStrategy;

	private ClientRepository clients;
	private ClientValidator validator;
	private Merge mergeHandler;

	public ClientService(
			ClientRepository clients,
			ClientValidator validator,
			ClientIdentifierStrategy identifierStrategy,
			ClientSecretStrategy secretStrategy,
			SecretHashStrategy hashStrategy) {

		Assert.notNull(identifierStrategy);
		Assert.notNull(secretStrategy);
		Assert.notNull(hashStrategy);

		this.clients = clients;
		this.validator = validator;
		this.mergeHandler = new Merge();

		this.identifierStrategy = identifierStrategy;
		this.secretStrategy = secretStrategy;
		this.hashStrategy = hashStrategy;
	}

	public Client createClient(Client client) {
		this.validator.validate(client);
		log.debug("Creating client: partnerId={},clientName={}", client.getPartnerId(), client.getClientName());
		UUID generatedIdentifier = this.identifierStrategy.newIdentifier(new HashMap<>());
		String generatedSecret = this.secretStrategy.newSecret();
		String hashedSecret = this.hashStrategy.createHash(generatedSecret.toCharArray());
		ZonedDateTime createdDate = Time.utcNow();
		log.debug("Client ID and secret generated: partnerId={},clientName={},clientId={}",
				client.getPartnerId(), client.getClientName(), generatedIdentifier);

		Client created = new Client();
		created.setId(generatedIdentifier);
		created.setPartnerId(client.getPartnerId());
		created.setClientName(client.getClientName());
		created.setClientIdIssuedAt(createdDate);
		created.setClientLastModifiedAt(createdDate);
		created.setClientSecret(hashedSecret);
		created.setClientSecretExpiresAt(client.getClientSecretExpiresAt());
		created.setScope(client.getScope());
		created.setAccessTokenExpiresIn(client.getAccessTokenExpiresIn());
		created.setStatus(client.getStatus());
		created.setStatusReason(client.getStatusReason());

		created = this.clients.addClient(created);
		created.setClientSecret(generatedSecret);
		return created;
	}

	public Page<Client> getClients(PageRequest page) {
		return this.clients.getClients(page);
	}

	public Client getClient(UUID id) {
		Client client = this.clients.getClient(id);
		if (client == null) {
			ApplicationError error = ApplicationError.resourceNotFound().build();
			throw new ApplicationException(error, "No client found with ID: " + id);
		}
		return client;
	}

	public Client updateClient(UUID id, Patch patch) {
		Client working = this.clients.getClient(id);
		UUID oldPartnerId = working.getPartnerId();

		this.mergeHandler.merge(patch, working);
		this.validator.validate(working);

		working.setId(id);
		working.setPartnerId(oldPartnerId);
		working.setClientLastModifiedAt(Time.utcNow());

		this.clients.updateClient(working);
		return this.getClient(id);
	}

	public Client changeSecret(UUID clientId, String currentSecret) {
		this.authenticate(clientId, currentSecret);
		return this.changeSecret(clientId);
	}

	private void authenticate(UUID clientId, String secret) {
		String correctHash = this.clients.getSecret(clientId);
		if (!this.hashStrategy.validateSecret(secret.toCharArray(), correctHash)) {
			ApplicationError error = ApplicationError.notAuthorized().build();
			throw new ApplicationException(error, "Invalid credentials.");
		}
	}

	public Client changeSecret(UUID clientId) {
		log.debug("Changing client secret: clientId={}", clientId);
		String newSecret = this.secretStrategy.newSecret();
		String newSecretHash = this.hashStrategy.createHash(newSecret.toCharArray());
		log.debug("New client secret generated: clientId={}", clientId);

		this.clients.updateSecret(clientId, newSecretHash);

		Client client = this.clients.getClient(clientId);
		client.setClientLastModifiedAt(Time.utcNow());
		this.clients.updateClient(client);
		client = this.clients.getClient(clientId);
		client.setClientSecret(newSecret);
		return client;
	}

}
