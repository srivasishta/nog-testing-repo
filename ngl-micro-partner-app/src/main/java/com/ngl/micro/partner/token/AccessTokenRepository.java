package com.ngl.micro.partner.token;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.ACCESS_TOKEN;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT;
import static java.util.stream.Collectors.toSet;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Configuration;
import org.jooq.Record;

import com.ngl.micro.partner.client.AccessToken;
import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JAccessTokenDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JAccessToken;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JAccessTokenRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.rest.server.security.oauth2.Revocation;
import com.ngl.middleware.util.Time;

/**
 * Repository for access tokens. Only Bearer token types currently supported.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class AccessTokenRepository {

	private DAL support;
	private JAccessTokenDao tokenDao;

	public AccessTokenRepository(Configuration config) {
		this.support = new DAL(config);
		this.tokenDao = new JAccessTokenDao(config);
	}

	public AccessToken add(UUID clientId, UUID tokenId, ZonedDateTime issuedDate, ZonedDateTime expiryDate) {
		Long fk = this.support.sql().select(CLIENT.ID)
				.from(CLIENT)
				.where(CLIENT.CLIENT_ID.eq(clientId))
				.fetchOne().value1();

		JAccessTokenRecord record = this.support.sql().newRecord(ACCESS_TOKEN);
		record.setClientId(fk);
		record.setTokenId(tokenId);
		record.setIssuedDate(issuedDate);
		record.setExpiresDate(expiryDate);
		record.store();

		return this.getById(record.getTokenId());
	}

	public AccessToken getById(UUID tokenId) {
		JAccessToken token = this.tokenDao.fetchOneByJTokenId(tokenId);
		return token == null ? null : this.map(token);
	}

	public Set<AccessToken> getByClientIdAndActive(UUID clientId) {
		Long fk = this.support.sql().select(CLIENT.ID)
				.from(CLIENT)
				.where(CLIENT.CLIENT_ID.eq(clientId))
				.fetchOne().value1();

		return this.support.sql().select()
				.from(ACCESS_TOKEN)
				.where(ACCESS_TOKEN.CLIENT_ID.eq(fk))
						.and(ACCESS_TOKEN.EXPIRES_DATE.greaterThan(Time.utcNow())
						.and(ACCESS_TOKEN.REVOKED_DATE.isNull()))
				.fetch()
				.stream()
				.map(this::map)
				.collect(toSet());
	}

	public Optional<AccessToken> revoke(UUID tokenId) {
		JAccessToken record = this.tokenDao.fetchOneByJTokenId(tokenId);
		if (record == null || record.getRevokedDate() != null) {
			return Optional.empty();
		} else {
			record.setRevokedDate(Time.utcNow());
			this.tokenDao.update(record);
			return Optional.of(this.getById(tokenId));
		}
	}

	private AccessToken map(Record record) {
		AccessToken token = new AccessToken();
		token.setId(record.getValue(ACCESS_TOKEN.TOKEN_ID));
		token.setIssuedDate(record.getValue(ACCESS_TOKEN.ISSUED_DATE));
		token.setExpiresDate(record.getValue(ACCESS_TOKEN.EXPIRES_DATE));
		token.setRevokedDate(record.getValue(ACCESS_TOKEN.REVOKED_DATE));
		return token;
	}

	private AccessToken map(JAccessToken record) {
		AccessToken token = new AccessToken();
		token.setId(record.getTokenId());
		token.setIssuedDate(record.getIssuedDate());
		token.setExpiresDate(record.getExpiresDate());
		token.setRevokedDate(record.getRevokedDate());
		return token;
	}

	public List<Revocation> getByRevokedAndExpirationDateAfter(ZonedDateTime after) {
		return this.support.sql().select()
				.from(ACCESS_TOKEN)
				.where(ACCESS_TOKEN.EXPIRES_DATE.greaterThan(after))
						.and(ACCESS_TOKEN.REVOKED_DATE.isNotNull())
				.fetch()
				.stream()
				.map(a -> new Revocation(a.getValue(ACCESS_TOKEN.TOKEN_ID), a.getValue(ACCESS_TOKEN.EXPIRES_DATE)))
				.collect(Collectors.toList());
	}

}
