package com.ngl.micro.partner.client;

import java.time.ZonedDateTime;

import com.ngl.micro.partner.client.Client.ClientStatus;

public class ClientStatusRecord {

	private Long id;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;
	private ClientStatus status;
	private String statusReason;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public ClientStatus getStatus() {
		return this.status;
	}

	public void setStatus(ClientStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

}