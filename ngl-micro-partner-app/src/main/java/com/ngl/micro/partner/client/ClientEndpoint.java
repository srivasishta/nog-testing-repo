package com.ngl.micro.partner.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.middleware.rest.server.RestEndpoint;

/**
 * Partner client endpoint.
 *
 * @author Paco Mendes
 *
 */
@Path("client")
public interface ClientEndpoint extends RestEndpoint {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	Response get();

	@POST
	@Path("secret")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response secret(String currentSecret);

}
