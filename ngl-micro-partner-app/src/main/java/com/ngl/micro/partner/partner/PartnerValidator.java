package com.ngl.micro.partner.partner;

import java.lang.annotation.ElementType;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.middleware.rest.server.validation.ResourceValidator;

public class PartnerValidator extends ResourceValidator<Partner> {

	public static PartnerValidator newInstance() {
		return ResourceValidator.configurator(new PartnerValidator()).configure();
	}

	private PartnerValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Partner.class)
			.property("id", ElementType.FIELD)
			.property("name", ElementType.FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(50))
			.property("status", ElementType.FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", ElementType.FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("clients", ElementType.FIELD);
	}

}
