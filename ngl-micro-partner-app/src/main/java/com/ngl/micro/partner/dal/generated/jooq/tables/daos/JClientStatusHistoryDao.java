/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables.daos;


import com.ngl.micro.partner.dal.generated.jooq.tables.JClientStatusHistory;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JClientStatusHistoryRecord;

import java.time.ZonedDateTime;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JClientStatusHistoryDao extends DAOImpl<JClientStatusHistoryRecord, com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory, Long> {

	/**
	 * Create a new JClientStatusHistoryDao without any configuration
	 */
	public JClientStatusHistoryDao() {
		super(JClientStatusHistory.CLIENT_STATUS_HISTORY, com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory.class);
	}

	/**
	 * Create a new JClientStatusHistoryDao with an attached configuration
	 */
	public JClientStatusHistoryDao(Configuration configuration) {
		super(JClientStatusHistory.CLIENT_STATUS_HISTORY, com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJId(Long... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory fetchOneByJId(Long value) {
		return fetchOne(JClientStatusHistory.CLIENT_STATUS_HISTORY.ID, value);
	}

	/**
	 * Fetch records that have <code>client_id IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJClientId(Long... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.CLIENT_ID, values);
	}

	/**
	 * Fetch records that have <code>status_id IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJStatusId(Long... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.STATUS_ID, values);
	}

	/**
	 * Fetch records that have <code>status_reason IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJStatusReason(String... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.STATUS_REASON, values);
	}

	/**
	 * Fetch records that have <code>start_date IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJStartDate(ZonedDateTime... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.START_DATE, values);
	}

	/**
	 * Fetch records that have <code>end_date IN (values)</code>
	 */
	public List<com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory> fetchByJEndDate(ZonedDateTime... values) {
		return fetch(JClientStatusHistory.CLIENT_STATUS_HISTORY.END_DATE, values);
	}
}
