package com.ngl.micro.partner.event.out;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.messaging.api.Message;
import com.ngl.middleware.messaging.core.publish.MessagePublishedListener;

public class AccessTokenEventPublishedListener implements MessagePublishedListener {

	private static final Logger log = LoggerFactory.getLogger(AccessTokenEventPublishedListener.class);

	private AccessTokenRevokedEventRepository events;

	public AccessTokenEventPublishedListener(AccessTokenRevokedEventRepository tokenRevokedRepository) {
		this.events = tokenRevokedRepository;
	}

	@Override
	public void onPublished(Message<?> message) {
		if (message instanceof AccessTokenRevokedEvent) {
			this.events.markPublished(((AccessTokenRevokedEvent) message).getId());
		}
		else {
			log.warn("Unexpected message type received: {}", message == null ? null : message.getClass().getName());
		}
	}

}
