package com.ngl.micro.partner.client;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT_STATUS_HISTORY;

import java.time.ZonedDateTime;

import org.jooq.Configuration;

import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JClientStatusHistoryRecord;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

/**
 * Tracked status history repo for partner clients.
 *
 * @author Paco Mendes
 */
public class ClientStatusHistoryRepository {

	private DAL support;

	public ClientStatusHistoryRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public void setStatus(Long clientId, ClientStatus status, String reason, ZonedDateTime updatedDate) {
		JClientStatusHistoryRecord currentStatus = this.getStatusRecord(clientId);
		if (currentStatus == null) {
			this.addStatusRecord(clientId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(clientId, status, reason, updatedDate);
		}
	}

	private void addStatusRecord(Long clientId, ClientStatus status, String reason, ZonedDateTime updatedDate) {
		JClientStatusHistoryRecord record =  this.support.sql().newRecord(CLIENT_STATUS_HISTORY);
		record.setClientId(clientId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(updatedDate);
		record.store();
	}

	private boolean updated(JClientStatusHistoryRecord currentStatus, ClientStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void expireStatusRecord(JClientStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public ClientStatusRecord getClientStatus(Long clientId) {
		JClientStatusHistoryRecord record = this.getStatusRecord(clientId);
		return record == null ? null : this.map(record);
	}

	private JClientStatusHistoryRecord getStatusRecord(Long clientId) {
		return this.support.sql()
				.selectFrom(CLIENT_STATUS_HISTORY)
				.where(CLIENT_STATUS_HISTORY.CLIENT_ID.eq(clientId))
				.and(CLIENT_STATUS_HISTORY.END_DATE.isNull())
				.fetchOne();
	}

	private ClientStatusRecord map(JClientStatusHistoryRecord record) {
		ClientStatusRecord status = new ClientStatusRecord();
		status.setId(record.getId());
		status.setStatus(ClientStatus.valueOf(ResourceStatus.nameForId(record.getStatusId())));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}

}
