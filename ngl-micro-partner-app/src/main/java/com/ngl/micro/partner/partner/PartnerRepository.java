package com.ngl.micro.partner.partner;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.PARTNER;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.PARTNER_STATUS_HISTORY;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.STATUS_TYPE;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.Table;

import com.ngl.micro.partner.dal.generated.jooq.tables.records.JPartnerRecord;
import com.ngl.micro.partner.partner.Partner.PartnerStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.util.Strings;

/**
 * Repository for partner accounts.
 *
 * @author Paco Mendes
 */
public class PartnerRepository {

	private static final String PRIMARY_KEY = "pk";

	private DAL support;
	private FieldMap<String> fieldMap;
	private JooqQuery query;
	private PartnerStatusHistoryRepository statuses;

	public PartnerRepository(Configuration config) {

		this.support= new DAL(config);

		this.statuses = new PartnerStatusHistoryRepository(config);

		this.fieldMap = new FieldMap<String>()
				.bind(Partner.Fields.ID, new MappedField(PARTNER.RESOURCE_ID).searchable(true))
				.bind(Partner.Fields.NAME, new MappedField(PARTNER.NAME).searchable(true).sortable(true))
				.bind(Partner.Fields.STATUS, new MappedField(STATUS_TYPE.VALUE).searchable(true))
				.bind(Partner.Fields.STATUS_REASON, new MappedField(PARTNER_STATUS_HISTORY.STATUS_REASON))
				.bind(Partner.Fields.LOGO_IMAGES_ORIGINAL, PARTNER.LOGO_ORIGINAL)
				.bind(Partner.Fields.LOGO_IMAGES_THUMBNAIL, PARTNER.LOGO_SMALL)
				.bind(Partner.Fields.LOGO_IMAGES_MEDIUM, PARTNER.LOGO_MEDIUM)
				.bind(Partner.Fields.LOGO_IMAGES_LARGE, PARTNER.LOGO_LARGE)
				.bind(PRIMARY_KEY, new MappedField(PARTNER.ID));

		Table<? extends Record> tables = PARTNER
				.join(PARTNER_STATUS_HISTORY).on(PARTNER.ID.eq(PARTNER_STATUS_HISTORY.PARTNER_ID))
				.join(STATUS_TYPE).on(STATUS_TYPE.ID.eq(PARTNER_STATUS_HISTORY.STATUS_ID))
				.and(PARTNER_STATUS_HISTORY.END_DATE.isNull());

		this.query = JooqQuery.builder(this.support, this.fieldMap)
				.from(tables)
				.build();
	}

	public Partner add(Partner partner, ZonedDateTime createdDate) {
		JPartnerRecord record = this.support.sql().newRecord(PARTNER);
		record.setResourceId(partner.getId());
		record.setName(partner.getName());
		record.store();
		Long partnerId = record.getId();
		this.statuses.setStatus(partnerId, partner.getStatus(), partner.getStatusReason(), createdDate);
		return this.get(partner.getId());
	}

	public Partner get(UUID id) {
		Record record = this.query.getRecordWhere(PARTNER.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		}
		else {
			Set<UUID> clients = new HashSet<>(this.getClients(record.getValue(PARTNER.ID)));
			Partner partner = this.map(record);
			partner.setClients(clients);
			return partner;
		}
	}

	private List<UUID> getClients(Long id) {
		return this.support.sql().select(CLIENT.CLIENT_ID)
				.from(CLIENT)
				.where(CLIENT.PARTNER_ID.eq(id))
				.fetch()
				.map(r -> r.getValue(CLIENT.CLIENT_ID));
	}

	public Page<Partner> get(PageRequest pageRequest) {
		return this.query.getPage(pageRequest, this::map);
	}

	private Partner map(Record record) {
		Partner partner = new Partner();
		partner.setId(record.getValue(PARTNER.RESOURCE_ID));
		partner.setName(record.getValue(PARTNER.NAME));
		String status = record.getValue(STATUS_TYPE.VALUE);
		partner.setStatus(PartnerStatus.valueOf(status));
		partner.setStatusReason(record.getValue(PARTNER_STATUS_HISTORY.STATUS_REASON));
		String logoOriginal = record.getValue(PARTNER.LOGO_ORIGINAL);
		String logoSmall = record.getValue(PARTNER.LOGO_SMALL);
		String logoMedium = record.getValue(PARTNER.LOGO_MEDIUM);
		String logoLarge = record.getValue(PARTNER.LOGO_LARGE);
		if (!Strings.isNullOrWhitespace(logoOriginal)
				|| !Strings.isNullOrWhitespace(logoSmall)
				|| !Strings.isNullOrWhitespace(logoMedium)
				|| !Strings.isNullOrWhitespace(logoLarge)) {
			ImageSet logos = new ImageSet();
			logos.setOriginal(logoOriginal);
			logos.setThumbnail(logoSmall);
			logos.setLarge(logoLarge);
			logos.setMedium(logoMedium);
			partner.setLogoImages(logos);
		}
		return partner;
	}

	public void update(Partner partner, ZonedDateTime updatedDate) {
		JPartnerRecord record = this.support.sql().fetchOne(PARTNER, PARTNER.RESOURCE_ID.eq(partner.getId()));
		record.setName(partner.getName());
		ImageSet logos = partner.getLogoImages() == null ? new ImageSet() : partner.getLogoImages();
		record.setLogoOriginal(logos.getOriginal());
		record.setLogoSmall(logos.getThumbnail());
		record.setLogoMedium(logos.getMedium());
		record.setLogoLarge(logos.getLarge());
		record.store();
		this.statuses.setStatus(
				record.getId(),
				partner.getStatus(),
				partner.getStatusReason(),
				updatedDate);
	}

}