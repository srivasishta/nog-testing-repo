//package com.ngl.micro.partner.dal;
//
///**
// * An enum mapping status types.
// *
// * @author Paco Mendes
// */
//public enum StatusType {
//
//	ACTIVE(1L),
//	INACTIVE(2L);
//
//	private Long id;
//
//	private StatusType(Long id) {
//		this.id = id;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public static Long getIdFrom(String name) {
//		return StatusType.valueOf(name).getId();
//	}
//
//	public static String getNameFrom(Long statusId) {
//		if (ACTIVE.getId().equals(statusId)) {
//			return ACTIVE.name();
//		}
//		if (INACTIVE.getId().equals(statusId)) {
//			return INACTIVE.name();
//		}
//		return null;
//	}
//}
