package com.ngl.micro.partner.partner;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.PARTNER_STATUS_HISTORY;

import java.time.ZonedDateTime;

import org.jooq.Configuration;

import com.ngl.micro.partner.dal.generated.jooq.tables.records.JPartnerStatusHistoryRecord;
import com.ngl.micro.partner.partner.Partner.PartnerStatus;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

/**
 * Tracked status history repo for partner status.
 *
 * @author Paco Mendes
 */
public class PartnerStatusHistoryRepository {

	private DAL support;

	public PartnerStatusHistoryRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public void setStatus(Long accountId, PartnerStatus status, String reason, ZonedDateTime updatedDate) {
		JPartnerStatusHistoryRecord currentStatus = this.getStatusRecord(accountId);
		if (currentStatus == null) {
			this.addStatusRecord(accountId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(accountId, status, reason, updatedDate);
		}
	}

	private void addStatusRecord(Long accountId, PartnerStatus status, String reason, ZonedDateTime updatedDate) {
		JPartnerStatusHistoryRecord record =  this.support.sql().newRecord(PARTNER_STATUS_HISTORY);
		record.setPartnerId(accountId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(updatedDate);
		record.store();
	}

	private boolean updated(JPartnerStatusHistoryRecord currentStatus, PartnerStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void expireStatusRecord(JPartnerStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public PartnerStatusRecord getPartnerStatus(Long partnerId) {
		JPartnerStatusHistoryRecord record = this.getStatusRecord(partnerId);
		return record == null ? null : this.map(record);
	}

	private JPartnerStatusHistoryRecord getStatusRecord(Long partnerId) {
		return this.support.sql()
				.selectFrom(PARTNER_STATUS_HISTORY)
				.where(PARTNER_STATUS_HISTORY.PARTNER_ID.eq(partnerId))
				.and(PARTNER_STATUS_HISTORY.END_DATE.isNull())
				.fetchOne();
	}

	private PartnerStatusRecord map(JPartnerStatusHistoryRecord record) {
		PartnerStatusRecord status = new PartnerStatusRecord();
		status.setId(record.getId());
		status.setStatus(PartnerStatus.valueOf(ResourceStatus.nameForId(record.getStatusId())));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}

}
