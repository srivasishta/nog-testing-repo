package com.ngl.micro.partner;

import java.util.List;
import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.cxf.rs.security.oauth2.grants.clientcred.ClientCredentialsGrantHandler;
import org.apache.cxf.rs.security.oauth2.services.AccessTokenService;
import org.apache.cxf.rs.security.oauth2.services.TokenRevocationService;
import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.partner.client.ClientEndpoint;
import com.ngl.micro.partner.client.ClientEndpointImpl;
import com.ngl.micro.partner.client.ClientManagementEndpoint;
import com.ngl.micro.partner.client.ClientManagementEndpointImpl;
import com.ngl.micro.partner.client.ClientRepository;
import com.ngl.micro.partner.client.ClientService;
import com.ngl.micro.partner.client.ClientValidator;
import com.ngl.micro.partner.event.out.AccessTokenEventPublishedListener;
import com.ngl.micro.partner.event.out.AccessTokenRevokedEventRepository;
import com.ngl.micro.partner.partner.PartnerManagementEndpoint;
import com.ngl.micro.partner.partner.PartnerManagementEndpointImpl;
import com.ngl.micro.partner.partner.PartnerRepository;
import com.ngl.micro.partner.partner.PartnerService;
import com.ngl.micro.partner.partner.PartnerValidator;
import com.ngl.micro.partner.token.AccessTokenRepository;
import com.ngl.micro.partner.token.PartnerCredentialsDataProvider;
import com.ngl.micro.partner.token.PartnerSecretVerifier;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.messaging.core.bus.RxLocalMessageBus;
import com.ngl.middleware.messaging.core.data.serialize.JsonMessageSerializer;
import com.ngl.middleware.messaging.jms.publish.JmsPublisher;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherConfiguration;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.EnableMicroserviceSecurityExtensions;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.CxfAccessTokenEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.Revocation;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtFactory;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.security.SecurityStrategyFactory;
import com.ngl.middleware.security.SecurityStrategyProperties;
import com.ngl.middleware.security.crypto.secret.SecretHashStrategy;
import com.ngl.middleware.security.oauth2.client.ClientIdentifierStrategy;
import com.ngl.middleware.security.oauth2.client.ClientSecretStrategy;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the partner application.
 *
 * @author Willy du Preez
 * @author Paco Mendes
 */
@Configuration
@EnableMicroserviceDatabase
@EnableMicroserviceRestServer
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
@EnableMicroserviceSecurityExtensions
public class PartnerApplicationConfiguration extends MicroserviceConfiguration {

	private static final String PARTNERS_ENDPOINT = "partner";

	@Bean
	public RestEndpoints restEndpoints(
			ObjectMapper mapper,
			PartnerManagementEndpoint partnerManagementEndpoint,
			ClientManagementEndpoint clientManagementEndpoint,
			ClientEndpoint clientEndpoint,
			AccessTokenEndpoint accessTokenEndpoint,
			Realm securityRealm) {

		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints(PARTNERS_ENDPOINT);
		restEndpoints.addEndpoints(partnerManagementEndpoint, clientManagementEndpoint, clientEndpoint, accessTokenEndpoint);
		restEndpoints.setRealm(securityRealm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	@Bean
	public SecretHashStrategy secretHashStrategy(
			SecurityStrategyProperties security,
			EnvironmentConfigurationProvider config) {

		return SecurityStrategyFactory.secretHashStrategy(config, security.getHash());
	}

	@Bean
	public ClientSecretStrategy clientSecretStrategy(
			SecurityStrategyProperties security,
			EnvironmentConfigurationProvider config) {

		return SecurityStrategyFactory.clientSecretStrategy(config, security.getClientSecret());
	}

	@Bean
	public ClientIdentifierStrategy clientIdentifierStrategy(
			SecurityStrategyProperties security,
			EnvironmentConfigurationProvider config) {

		return SecurityStrategyFactory.clientIdentifierStrategy(config, security.getClientIdentifier());
	}

	/* --------------------------------------------------------
	 *  Partner
	 * -------------------------------------------------------- */

	@Bean
	public PartnerRepository partnerAccountRepository(org.jooq.Configuration config) {
		return new PartnerRepository(config);
	}

	@Bean
	public PartnerValidator partnerAccountValidator() {
		return PartnerValidator.newInstance();
	}

	@Bean
	public PartnerService partnerAccountService(
			PartnerValidator partnerValidator,
			PartnerRepository partnerRepository,
			ClientRepository clientRepository) {
		return new PartnerService(partnerValidator, partnerRepository, clientRepository);
	}

	@Bean
	public PartnerManagementEndpoint partnerManagementEndpoint(RequestProcessor processor, PartnerService partnerService) {
		return new PartnerManagementEndpointImpl(processor, partnerService);
	}

	/* --------------------------------------------------------
	 *  Client
	 * -------------------------------------------------------- */

	@Bean
	public ClientRepository clientRepository(org.jooq.Configuration config) {
		return new ClientRepository(config);
	}

	@Bean
	public ClientValidator clientValidator() {
		return ClientValidator.newInstance();
	}

	@Bean
	public ClientService clientService(
			ClientRepository clientRepository,
			ClientValidator clientValidator,
			ClientIdentifierStrategy identifierStrategy,
			ClientSecretStrategy secretStrategy,
			SecretHashStrategy hashStrategy) {
		return new ClientService(clientRepository,
				clientValidator,
				identifierStrategy,
				secretStrategy,
				hashStrategy);
	}

	@Bean
	public ClientManagementEndpoint clientManagementEndpoint(RequestProcessor processor, ClientService partnerService) {
		return new ClientManagementEndpointImpl(processor, partnerService);
	}

	@Bean
	public ClientEndpoint clientEndpoint(RequestProcessor processor, ClientService clientService) {
		return new ClientEndpointImpl(processor, clientService);
	}

	@Bean
	public AccessTokenRepository accessTokenRepository(org.jooq.Configuration config) {
		return new AccessTokenRepository(config);
	}

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration config) {
		return new AccessTokenRevokedEventRepository(config);
	}

	/* --------------------------------------------------------
	 *  OAuth
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenEndpoint accessTokenEndpoint(
			PlatformTransactionManager txManager,
			MessageBus messageBus,
			ClientRepository clientRepository,
			AccessTokenRepository accessTokenRepository,
			RevocationList revocationList,
			AccessTokenRevokedEventRepository revokedTokenEventRepository,
			SecretHashStrategy secretHashStrategy) throws Exception {

		PartnerCredentialsDataProvider oAuthDataProvider = new PartnerCredentialsDataProvider(
				txManager,
				messageBus,
				clientRepository,
				accessTokenRepository,
				revocationList,
				revokedTokenEventRepository);

		ClientCredentialsGrantHandler handler = new ClientCredentialsGrantHandler();
//			handler.setCanSupportPublicClients(false);
//			handler.setPartialMatchScopeValidation(false);
		handler.setDataProvider(oAuthDataProvider);

		AccessTokenService ats = new AccessTokenService();
		ats.setBlockUnsecureRequests(false);
//			ats.setCanSupportPublicClients(true);
//			ats.setClientIdProvider(clientIdProvider);
		ats.setDataProvider(oAuthDataProvider);
		PartnerSecretVerifier verifier = new PartnerSecretVerifier(secretHashStrategy);
		ats.setClientSecretVerifier(verifier);

		ats.setGrantHandler(handler);

		TokenRevocationService trs = new TokenRevocationService();
		trs.setDataProvider(oAuthDataProvider);
		trs.setClientSecretVerifier(verifier);

		CxfAccessTokenEndpoint endpoint = new CxfAccessTokenEndpoint();
		endpoint.setAccessTokenService(ats);
		endpoint.setTokenRevocationService(trs);
		return endpoint;
	}

	@Bean
	public RevocationList revocationList(AccessTokenRepository repository) {
		List<Revocation> revocations = repository.getByRevokedAndExpirationDateAfter(Time.utcNow());
		return new RevocationList(revocations);
	}

	@Bean
	public Realm securityRealm(JwtFactory tokenFactory, RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(tokenFactory, revocationList));
	}

	@Bean
	public JwtFactory tokenFactory() {
		return new SignedEncryptedJwtFactory();
	}

	/* --------------------------------------------------------
	 *  Events
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenEventPublishedListener accessTokenEventPublishedListener(
			AccessTokenRevokedEventRepository tokenRevokedEventRepository) {
		return new AccessTokenEventPublishedListener(tokenRevokedEventRepository);
	}

	@Bean(destroyMethod = "stop")
	public JmsPublisher tokenEventPublisher(ConnectionFactory connectionFactory,
			ObjectMapper objectMapper,
			AccessTokenEventPublishedListener accessTokenEventListener,
			MessageBus messageBus,
			MetricRegistry metricRegistry) {

		JmsPublisherConfiguration config = new JmsPublisherConfiguration();
		config.setConnectionFactory(connectionFactory);
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setMessageBus(this.messageBus());
		config.setProducerCount(1);
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setSerializer(new JsonMessageSerializer(objectMapper));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessagePublishedListener(accessTokenEventListener);
		config.setJmsPublisherMetrics(new JmsPublisherMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));

		JmsPublisher publisher = new JmsPublisher(config);
		publisher.start();
		return publisher;
	}

	@Bean
	public MessageBus messageBus() {
		return new RxLocalMessageBus();
	}

}