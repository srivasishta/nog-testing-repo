/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables.records;


import com.ngl.micro.partner.dal.generated.jooq.tables.JClientStatusHistory;

import java.time.ZonedDateTime;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JClientStatusHistoryRecord extends UpdatableRecordImpl<JClientStatusHistoryRecord> implements Record6<Long, Long, Long, String, ZonedDateTime, ZonedDateTime> {

	private static final long serialVersionUID = 1499950575;

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.client_id</code>.
	 */
	public void setClientId(Long value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.client_id</code>.
	 */
	public Long getClientId() {
		return (Long) getValue(1);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.status_id</code>.
	 */
	public void setStatusId(Long value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.status_id</code>.
	 */
	public Long getStatusId() {
		return (Long) getValue(2);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.status_reason</code>.
	 */
	public void setStatusReason(String value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.status_reason</code>.
	 */
	public String getStatusReason() {
		return (String) getValue(3);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.start_date</code>.
	 */
	public void setStartDate(ZonedDateTime value) {
		setValue(4, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.start_date</code>.
	 */
	public ZonedDateTime getStartDate() {
		return (ZonedDateTime) getValue(4);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_status_history.end_date</code>.
	 */
	public void setEndDate(ZonedDateTime value) {
		setValue(5, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_status_history.end_date</code>.
	 */
	public ZonedDateTime getEndDate() {
		return (ZonedDateTime) getValue(5);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record6 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row6<Long, Long, Long, String, ZonedDateTime, ZonedDateTime> fieldsRow() {
		return (Row6) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row6<Long, Long, Long, String, ZonedDateTime, ZonedDateTime> valuesRow() {
		return (Row6) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field2() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.CLIENT_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field3() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.STATUS_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field4() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.STATUS_REASON;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<ZonedDateTime> field5() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.START_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<ZonedDateTime> field6() {
		return JClientStatusHistory.CLIENT_STATUS_HISTORY.END_DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value2() {
		return getClientId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value3() {
		return getStatusId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value4() {
		return getStatusReason();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ZonedDateTime value5() {
		return getStartDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ZonedDateTime value6() {
		return getEndDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value2(Long value) {
		setClientId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value3(Long value) {
		setStatusId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value4(String value) {
		setStatusReason(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value5(ZonedDateTime value) {
		setStartDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord value6(ZonedDateTime value) {
		setEndDate(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistoryRecord values(Long value1, Long value2, Long value3, String value4, ZonedDateTime value5, ZonedDateTime value6) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		value5(value5);
		value6(value6);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JClientStatusHistoryRecord
	 */
	public JClientStatusHistoryRecord() {
		super(JClientStatusHistory.CLIENT_STATUS_HISTORY);
	}

	/**
	 * Create a detached, initialised JClientStatusHistoryRecord
	 */
	public JClientStatusHistoryRecord(Long id, Long clientId, Long statusId, String statusReason, ZonedDateTime startDate, ZonedDateTime endDate) {
		super(JClientStatusHistory.CLIENT_STATUS_HISTORY);

		setValue(0, id);
		setValue(1, clientId);
		setValue(2, statusId);
		setValue(3, statusReason);
		setValue(4, startDate);
		setValue(5, endDate);
	}
}
