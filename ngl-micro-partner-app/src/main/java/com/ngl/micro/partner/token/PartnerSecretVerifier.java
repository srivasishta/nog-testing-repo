package com.ngl.micro.partner.token;

import static com.ngl.middleware.util.Strings.isNullOrWhitespace;

import org.apache.cxf.rs.security.oauth2.common.Client;
import org.apache.cxf.rs.security.oauth2.provider.ClientSecretVerifier;

import com.ngl.middleware.security.crypto.secret.SecretHashStrategy;
import com.ngl.middleware.util.Assert;

/**
 * Verifies a clients secret.
 *
 * @author Willy du Preez
 *
 */
public class PartnerSecretVerifier implements ClientSecretVerifier {

	private SecretHashStrategy secretHashStrategy;

	public PartnerSecretVerifier(SecretHashStrategy secretHashStrategy) {
		Assert.notNull(secretHashStrategy);

		this.secretHashStrategy = secretHashStrategy;
	}

	@Override
	public boolean validateClientSecret(Client client, String clientSecret) {
		if (isNullOrWhitespace(clientSecret)) {
			return false;
		}
		else {
			return this.secretHashStrategy.validateSecret(clientSecret.toCharArray(), client.getClientSecret());
		}
	}

}
