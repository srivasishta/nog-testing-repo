package com.ngl.micro.partner.client;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;

/**
 * Partner client info and management endpoint.
 * Provides info and management available to an authorised partner client.
 *
 * @author Paco Mendes
 *
 */
public class ClientEndpointImpl implements ClientEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private ClientService clientService;

	public ClientEndpointImpl(RequestProcessor processor, ClientService clientService) {
		this.processor = processor;
		this.clientService = clientService;
	}

	@Override
	public Response get() {
		Fields fields = this.processor.fields(this.msgCtx);
		UUID clientId = Subjects.getClientId();

		Client client = this.clientService.getClient(clientId);

		Resource resource = HAL.resource(client)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();

	}

	@Override
	public Response secret(String currentSecret) {
		Fields fields = this.processor.fields(this.msgCtx);

		UUID clientId = Subjects.getClientId();
		Client client = this.clientService.changeSecret(clientId, currentSecret);

		Resource resource = HAL.resource(client)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

}
