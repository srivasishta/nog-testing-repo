package com.ngl.micro.partner.partner;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;

/**
 * Partner management endpoint implementation.
 *
 * @author Paco Mendes
 */
public class PartnerManagementEndpointImpl implements PartnerManagementEndpoint {

	@Context
	private MessageContext msgCtx;
	private RequestProcessor processor;

	private PartnerService partnerService;

	public PartnerManagementEndpointImpl(RequestProcessor processor, PartnerService partnerService) {
		this.processor = processor;
		this.partnerService = partnerService;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Partner partner = this.partnerService.getPartnerById(id);

		Resource resource = HAL.resource(partner)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Partner> page = this.partnerService.getPartners(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Partner partner) {
		Partner created = this.partnerService.createPartner(partner);
		Resource resource = HAL.resource(created).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response update(UUID id, Patch patch) {
		Partner updated = this.partnerService.updatePartner(id, patch);
		Resource resource = HAL.resource(updated).build();
		return Response.ok(resource).build();
	}

}
