package com.ngl.micro.partner.client;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT_SCOPE;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.SCOPE;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JClientScopeDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JScopeDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientScope;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ValidationError;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;

/**
 * Partner client repository.
 *
 * @author Paco Mendes
 *
 */
public class LinkedClientScopes {

	private DAL support;
	private JScopeDao scopeDao;
	private JClientScopeDao clientScopeDao;

	public LinkedClientScopes(Configuration config) {

		this.support= new DAL(config);
		this.scopeDao = new JScopeDao(config);
		this.clientScopeDao = new JClientScopeDao(config);
	}

	public void add(Long clientId, Set<String> scopes) {
		Map<String, Long> scopeIds = this.scopeIds();
		for (String scope : scopes) {
			JClientScope s = new JClientScope();
			s.setClientId(clientId);
			Long scopeId = scopeIds.get(scope);
			if (scopeId == null) {
				this.invalidScope(scope);
			} else {
				s.setScopeId(scopeId);
			}
			this.clientScopeDao.insert(s);
		}
	}

	public Set<String> get(Long clientId) {
		return this.support.sql()
				.select(SCOPE.VALUE)
				.from(CLIENT_SCOPE)
				.join(SCOPE).on(CLIENT_SCOPE.SCOPE_ID.eq(SCOPE.ID))
				.where(CLIENT_SCOPE.CLIENT_ID.eq(clientId))
				.fetch()
				.stream()
				.map(s -> s.getValue(SCOPE.VALUE))
				.collect(toSet());
	}

	public void update(Long clientId, Set<String> scopes) {
		Set<String> currentScopes = this.get(clientId);
		if (!currentScopes.equals(scopes)) {
			this.delete(clientId);
			this.add(clientId, scopes);
		}
	}

	private Map<String, Long> scopeIds() {
		return this.scopeDao.findAll().stream()
				.collect(toMap(s -> s.getValue(), s -> s.getId()));
	}

	private void invalidScope(String scopeName) {
		ApplicationError.validationError()
			.addValidationErrors(new ValidationError(Client.Fields.SCOPE, ValidationErrorCode.INVALID, "Unknown scope: " + scopeName))
			.asException();
	}

	private void delete(Long clientId) {
		List<JClientScope> scopes = this.clientScopeDao.fetchByJClientId(clientId);
		if (!scopes.isEmpty()){
			this.clientScopeDao.delete(scopes);
		}
	}

}