package com.ngl.micro.partner.token;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.cxf.rs.security.oauth2.common.AccessTokenRegistration;
import org.apache.cxf.rs.security.oauth2.common.Client;
import org.apache.cxf.rs.security.oauth2.common.OAuthPermission;
import org.apache.cxf.rs.security.oauth2.common.ServerAccessToken;
import org.apache.cxf.rs.security.oauth2.common.UserSubject;
import org.apache.cxf.rs.security.oauth2.provider.OAuthDataProvider;
import org.apache.cxf.rs.security.oauth2.provider.OAuthServiceException;
import org.apache.cxf.rs.security.oauth2.tokens.bearer.BearerAccessToken;
import org.apache.cxf.rs.security.oauth2.utils.OAuthConstants;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ngl.micro.partner.client.ClientRepository;
import com.ngl.micro.partner.event.out.AccessTokenRevokedEventRepository;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.server.security.oauth2.Revocation;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtClaims;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtFactory;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.rest.server.security.oauth2.util.JwtConstants;
import com.ngl.middleware.rest.server.security.oauth2.util.OAuth2Constants;
import com.ngl.middleware.security.SecurityException;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * Provides authentication data for the OAuth 2.0 client credentials grant
 * type used to issue access tokens.
 *
 * @author Willy du Pree
 * @author Paco Mendes
 *
 */
public class PartnerCredentialsDataProvider implements OAuthDataProvider {

	private final List<String> allowedGrantTypes = Collections.singletonList(OAuthConstants.CLIENT_CREDENTIALS_GRANT);
	private final JwtFactory tokenFactory = new SignedEncryptedJwtFactory();

	private PlatformTransactionManager txManager;
	private TransactionDefinition txDefinition;

	private MessageBus messageBus;

	private ClientRepository clientRepo;
	private AccessTokenRepository accessTokenRepo;
	private RevocationList revocationList;
	private AccessTokenRevokedEventRepository revokedTokenEventRepo;

	public PartnerCredentialsDataProvider(
			PlatformTransactionManager txManager,
			MessageBus messageBus,
			ClientRepository clientRepo,
			AccessTokenRepository accessTokenRepo,
			RevocationList revocationList,
			AccessTokenRevokedEventRepository revokedTokenEventRepo) {

		this.txManager = txManager;
		this.txDefinition = new DefaultTransactionDefinition();

		this.messageBus = messageBus;

		this.clientRepo = clientRepo;
		this.revocationList = revocationList;
		this.accessTokenRepo = accessTokenRepo;
		this.revokedTokenEventRepo = revokedTokenEventRepo;
	}

	@Override
	public Client getClient(String clientId) throws OAuthServiceException {

		com.ngl.micro.partner.client.Client partnerClient = this.clientRepo.getClient(this.checkedUuid(clientId));
		if (partnerClient == null) {
			return null;
		}
		String secret = this.clientRepo.getSecret(partnerClient.getId());

		Map<String, String> props = new HashMap<>();
		props.put(JwtConstants.SUBJECT, partnerClient.getPartnerId().toString());
		props.put(OAuth2Constants.ACCESS_TOKEN_EXPIRES_IN, String.valueOf(partnerClient.getAccessTokenExpiresIn()));

		Client client = new Client();
		client.setClientId(clientId);
		client.setClientSecret(secret);
		client.setConfidential(true);
		client.setAllowedGrantTypes(this.allowedGrantTypes);
		client.setRegisteredScopes(new ArrayList<>(partnerClient.getScope()));
		client.setProperties(props);

		return client;
	}

	@Override
	public ServerAccessToken createAccessToken(AccessTokenRegistration registration) throws OAuthServiceException {
		ZonedDateTime issuedDate = Time.utcNow();

		List<String> approved = approveScope(
				registration.getClient().getRegisteredScopes(),
				registration.getRequestedScope());

		registration.setApprovedScope(approved);

		JwtClaims claims = this.createAccessTokensClaims(registration, issuedDate);
		ServerAccessToken token = this.createServerAccessToken(registration, claims);

		TransactionStatus status = this.txManager.getTransaction(this.txDefinition);
		try {
			this.accessTokenRepo.add(claims.getClientId(), claims.getJwtId(), issuedDate, claims.getExpiration());
		} catch (Exception e) {
			txManager.rollback(status);
			throw e;
		}
		this.txManager.commit(status);

		return token;
	}

	private List<String> approveScope(List<String> registered, List<String> requested) {
		if (requested.isEmpty()) {
			return new ArrayList<>(registered);
		}
		return requested.stream().filter(s -> registered.contains(s)).collect(Collectors.toList());
	}

	private JwtClaims createAccessTokensClaims(AccessTokenRegistration registration, ZonedDateTime issuedDate) {
		Client client = registration.getClient();
		Map<String, String> extra = client.getProperties();

		JwtClaims claims = new JwtClaims();

		claims.setIssuer("https://www.trustedinnovators.com/");
		claims.setAudience(Arrays.asList("https://www.trustedinnovators.com/"));
		claims.setJwtId(UUIDS.type4Uuid());

		claims.setSubject(this.checkedUuid(extra.get(JwtConstants.SUBJECT)));
		claims.setClientId(this.checkedUuid(client.getClientId()));
		claims.setScopes(new HashSet<>(registration.getApprovedScope()));

		claims.setIssuedAt(issuedDate);
		claims.setExpiration(issuedDate.plusSeconds(Long.valueOf(extra.get(OAuth2Constants.ACCESS_TOKEN_EXPIRES_IN))));

		return claims;
	}

	private ServerAccessToken createServerAccessToken(AccessTokenRegistration registration, JwtClaims claims) {
		List<OAuthPermission> permissions = this.convertScopeToPermissions(
				registration.getClient(),
				registration.getApprovedScope());

		String tokenKey = this.tokenFactory.produce(claims.getAllClaims());
		Long lifetime = claims.getExpiration().toEpochSecond() - claims.getIssuedAt().toEpochSecond();

		ServerAccessToken server = new BearerAccessToken(
				registration.getClient(),
				tokenKey,
				lifetime,
				claims.getIssuedAt().toEpochSecond());
		server.setAudience(registration.getAudience());
		server.setGrantType(registration.getGrantType());
		server.setScopes(permissions);
		server.setSubject(registration.getSubject());

		return server;
	}

	@Override
	public List<OAuthPermission> convertScopeToPermissions(Client client, List<String> requestedScope) {
		return requestedScope.stream().map(s -> new OAuthPermission(s, s)).collect(Collectors.toList());
	}

	@Override
	public void revokeToken(Client client, String accessTokenKey, String tokenTypeHint) throws OAuthServiceException {
		JwtClaims claims;
		try {
			claims = new JwtClaims(this.tokenFactory.consume(accessTokenKey));
		} catch (SecurityException ex) {
			throw ApplicationError.badRequest().asException("Failed to consume access token.", ex);
		}

		UUID tokenId = claims.getJwtId();
		ZonedDateTime expiration = claims.getExpiration();
		if (Time.utcNow().isAfter(expiration)) {
			return;
		}

		TransactionStatus status = this.txManager.getTransaction(txDefinition);
		AccessTokenRevokedEvent event;
		try {
			event = this.accessTokenRepo.revoke(tokenId)
					.map(t -> this.revokedTokenEventRepo.add(tokenId, t.getRevokedDate(), t.getExpiresDate()))
					.orElse(null);
		}
		catch (Exception e) {
			this.txManager.rollback(status);
			throw e;
		}
		this.txManager.commit(status);

		if (event != null) {
			this.revocationList.addRevocation(new Revocation(claims.getJwtId(), expiration));
			this.revocationList.removeExpiredRevocations();
			this.messageBus.send(event);
		}
	}

	@Override
	public ServerAccessToken getPreauthorizedToken(Client client, List<String> requestedScopes, UserSubject subject,
			String grantType) throws OAuthServiceException {
		return null;
	}

	@Override
	public ServerAccessToken getAccessToken(String accessToken) throws OAuthServiceException {
		throw new UnsupportedOperationException("Not supported");
	}

	@Override
	public void removeAccessToken(ServerAccessToken accessToken) throws OAuthServiceException {
		throw new UnsupportedOperationException("Not supported");
	}

	@Override
	public ServerAccessToken refreshAccessToken(Client client, String refreshToken, List<String> requestedScopes)
			throws OAuthServiceException {
		throw new UnsupportedOperationException("Not supported");
	}

	private UUID checkedUuid(String id) {
		try {
			return UUID.fromString(id);
		} catch(IllegalArgumentException ex) {
			throw ApplicationError.badRequest().asException("Invalid id format.");
		}
	}

}
