package com.ngl.micro.partner.partner;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.http.PATCH;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Partner management endpoint.
 *
 * @author Paco Mendes
 */
@Path("partners")
public interface PartnerManagementEndpoint extends RestEndpoint {

	@POST
	@RequiresScopes(Scopes.SYS_PARTNER_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(Partner partner);

	@GET
	@Path("{id}")
	@RequiresScopes(Scopes.PARTNER_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@GET
	@RequiresScopes(Scopes.PARTNER_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@PATCH
	@Path("{id}")
	@RequiresScopes(Scopes.PARTNER_UPDATE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response update(@PathParam("id") UUID id, Patch patch);

}
