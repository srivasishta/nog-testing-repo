/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JScope implements Serializable {

	private static final long serialVersionUID = -481198202;

	private Long   id;
	private String value;

	public JScope() {}

	public JScope(JScope value) {
		this.id = value.id;
		this.value = value.value;
	}

	public JScope(
		Long   id,
		String value
	) {
		this.id = id;
		this.value = value;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
