package com.ngl.micro.partner.partner;

import static com.ngl.micro.partner.partner.Partner.PartnerStatus.INACTIVE;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.transaction.Transactional;

import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.client.ClientRepository;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * Service for managing partners.
 *
 * @author Paco Mendes
 */
@Transactional(rollbackOn = Exception.class)
public class PartnerService {

	private PartnerValidator validator;
	private Merge mergeHandler;

	private PartnerRepository partners;
	private ClientRepository clients;

	public PartnerService(PartnerValidator validator, PartnerRepository partnerRepo, ClientRepository clientRepo) {
		this.validator = validator;
		this.partners = partnerRepo;
		this.clients = clientRepo;
		this.mergeHandler = new Merge();
	}

	public Partner createPartner(Partner partner) {
		this.validator.validate(partner);
		ZonedDateTime createdDate = Time.utcNow();
		partner.setId(UUIDS.type4Uuid());
		return this.partners.add(partner, createdDate);
	}

	public Partner getPartnerById(UUID id) {
		Partner partner = this.partners.get(id);
		if (partner == null) {
			ApplicationError.resourceNotFound().asException();
		}
		return partner;
	}

	public Page<Partner> getPartners(PageRequest pageRequest) {
		return this.partners.get(pageRequest);
	}

	public Partner updatePartner(UUID id, Patch patch) {
		UUID partnerId = Subjects.getSubject();

		Partner working = this.getPartnerById(id);
		ImageSet beforeImages = this.beforeImages(working.getLogoImages());

		this.mergeHandler.merge(patch, working);

		working.setId(id);
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			working.setLogoImages(beforeImages);
		}

		this.validator.validate(working);
		ZonedDateTime updatedDate = Time.utcNow();
		this.partners.update(working, updatedDate);
		if (this.isInactive(working)) {
			this.clients.updateClientStatuses(
					working.getId(),
					ClientStatus.INACTIVE,
					working.getStatusReason(),
					updatedDate);
		}
		return this.getPartnerById(working.getId());
	}

	private ImageSet beforeImages(ImageSet logoImages) {
		if (logoImages == null) {
			return null;
		}
		ImageSet copy = new ImageSet();
		copy.setOriginal(logoImages.getOriginal());
		copy.setThumbnail(logoImages.getThumbnail());
		copy.setMedium(logoImages.getMedium());
		copy.setLarge(logoImages.getLarge());
		return copy;
	}

	private boolean isInactive(Partner partner) {
		return partner.getStatus().equals(INACTIVE);
	}

}