/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables;


import com.ngl.micro.partner.dal.generated.jooq.JNglMicroPartner;
import com.ngl.micro.partner.dal.generated.jooq.Keys;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JClientStatusHistoryRecord;
import com.ngl.middleware.database.mapping.extension.ZonedDateTimeConverter;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JClientStatusHistory extends TableImpl<JClientStatusHistoryRecord> {

	private static final long serialVersionUID = -413373687;

	/**
	 * The reference instance of <code>ngl_micro_partner.client_status_history</code>
	 */
	public static final JClientStatusHistory CLIENT_STATUS_HISTORY = new JClientStatusHistory();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JClientStatusHistoryRecord> getRecordType() {
		return JClientStatusHistoryRecord.class;
	}

	/**
	 * The column <code>ngl_micro_partner.client_status_history.id</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client_status_history.client_id</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, Long> CLIENT_ID = createField("client_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client_status_history.status_id</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, Long> STATUS_ID = createField("status_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_partner.client_status_history.status_reason</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, String> STATUS_REASON = createField("status_reason", org.jooq.impl.SQLDataType.VARCHAR.length(50).nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>ngl_micro_partner.client_status_history.start_date</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, ZonedDateTime> START_DATE = createField("start_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new ZonedDateTimeConverter());

	/**
	 * The column <code>ngl_micro_partner.client_status_history.end_date</code>.
	 */
	public final TableField<JClientStatusHistoryRecord, ZonedDateTime> END_DATE = createField("end_date", org.jooq.impl.SQLDataType.TIMESTAMP, this, "", new ZonedDateTimeConverter());

	/**
	 * Create a <code>ngl_micro_partner.client_status_history</code> table reference
	 */
	public JClientStatusHistory() {
		this("client_status_history", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_partner.client_status_history</code> table reference
	 */
	public JClientStatusHistory(String alias) {
		this(alias, CLIENT_STATUS_HISTORY);
	}

	private JClientStatusHistory(String alias, Table<JClientStatusHistoryRecord> aliased) {
		this(alias, aliased, null);
	}

	private JClientStatusHistory(String alias, Table<JClientStatusHistoryRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroPartner.NGL_MICRO_PARTNER, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JClientStatusHistoryRecord, Long> getIdentity() {
		return Keys.IDENTITY_CLIENT_STATUS_HISTORY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JClientStatusHistoryRecord> getPrimaryKey() {
		return Keys.KEY_CLIENT_STATUS_HISTORY_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JClientStatusHistoryRecord>> getKeys() {
		return Arrays.<UniqueKey<JClientStatusHistoryRecord>>asList(Keys.KEY_CLIENT_STATUS_HISTORY_PRIMARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JClientStatusHistoryRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JClientStatusHistoryRecord, ?>>asList(Keys.CLIENT_STATUS_HISTORY_IBFK_2, Keys.CLIENT_STATUS_HISTORY_IBFK_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientStatusHistory as(String alias) {
		return new JClientStatusHistory(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JClientStatusHistory rename(String name) {
		return new JClientStatusHistory(name, null);
	}
}
