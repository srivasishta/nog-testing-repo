package com.ngl.micro.partner.client;

import java.lang.annotation.ElementType;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.MinDef;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.middleware.rest.server.validation.ResourceValidator;
import com.ngl.middleware.rest.server.validation.constraint.UniqueKeySetDef;

public class ClientValidator extends ResourceValidator<Client> {

	public static ClientValidator newInstance() {
		return ResourceValidator.configurator(new ClientValidator()).configure();
	}

	private ClientValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		configureClientMapping(mapping);;
	}

	public static void configureClientMapping(ConstraintMapping mapping) {
		mapping.type(Client.class)
			.property("partnerId", ElementType.FIELD)
				.constraint(new NotNullDef())
			.property("clientName", ElementType.FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(50))
			.property("clientSecret", ElementType.FIELD)
			.property("status", ElementType.FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", ElementType.FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("scope", ElementType.FIELD)
				.constraint(new UniqueKeySetDef().min(1).max(255))
			.property("accessTokenExpiresIn", ElementType.FIELD)
				.constraint(new NotNullDef())
				.constraint(new MinDef().value(0))
			.property("clientIdIssuedAt", ElementType.FIELD)
			.property("clientLastModifiedAt", ElementType.FIELD)
			.property("clientSecretExpiresAt", ElementType.FIELD);
	}
}
