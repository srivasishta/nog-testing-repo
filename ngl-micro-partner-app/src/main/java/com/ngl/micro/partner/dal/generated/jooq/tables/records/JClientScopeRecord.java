/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.partner.dal.generated.jooq.tables.records;


import com.ngl.micro.partner.dal.generated.jooq.tables.JClientScope;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.6.4"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JClientScopeRecord extends UpdatableRecordImpl<JClientScopeRecord> implements Record2<Long, Long> {

	private static final long serialVersionUID = 1695567954;

	/**
	 * Setter for <code>ngl_micro_partner.client_scope.client_id</code>.
	 */
	public void setClientId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_scope.client_id</code>.
	 */
	public Long getClientId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_partner.client_scope.scope_id</code>.
	 */
	public void setScopeId(Long value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_partner.client_scope.scope_id</code>.
	 */
	public Long getScopeId() {
		return (Long) getValue(1);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record2<Long, Long> key() {
		return (Record2) super.key();
	}

	// -------------------------------------------------------------------------
	// Record2 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, Long> fieldsRow() {
		return (Row2) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, Long> valuesRow() {
		return (Row2) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JClientScope.CLIENT_SCOPE.CLIENT_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field2() {
		return JClientScope.CLIENT_SCOPE.SCOPE_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getClientId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value2() {
		return getScopeId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientScopeRecord value1(Long value) {
		setClientId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientScopeRecord value2(Long value) {
		setScopeId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JClientScopeRecord values(Long value1, Long value2) {
		value1(value1);
		value2(value2);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JClientScopeRecord
	 */
	public JClientScopeRecord() {
		super(JClientScope.CLIENT_SCOPE);
	}

	/**
	 * Create a detached, initialised JClientScopeRecord
	 */
	public JClientScopeRecord(Long clientId, Long scopeId) {
		super(JClientScope.CLIENT_SCOPE);

		setValue(0, clientId);
		setValue(1, scopeId);
	}
}
