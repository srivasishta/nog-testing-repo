package com.ngl.micro.partner.client;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.CLIENT_STATUS_HISTORY;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.PARTNER;
import static com.ngl.micro.partner.dal.generated.jooq.Tables.STATUS_TYPE;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.Table;

import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.dal.generated.jooq.tables.records.JClientRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;

/**
 * Partner client repository.
 *
 * @author Paco Mendes
 *
 */
public class ClientRepository {

	private static final String PRIMARY_KEY = "pk";

	private DAL support;
	private FieldMap<String> fieldMap;
	private JooqQuery query;

	private LinkedClientScopes scopes;
	private ClientStatusHistoryRepository statuses;

	public ClientRepository(Configuration config) {

		this.support= new DAL(config);

		this.fieldMap = new FieldMap<String>()
				.bind(Client.Fields.ID, new MappedField(CLIENT.CLIENT_ID).searchable(true))
				.bind(Client.Fields.PARTNER_ID, new MappedField(PARTNER.RESOURCE_ID))
				.bind(Client.Fields.CLIENT_NAME, new MappedField(CLIENT.CLIENT_NAME).searchable(true).sortable(true))
				.bind(Client.Fields.STATUS, new MappedField(STATUS_TYPE.VALUE).searchable(true))
				.bind(Client.Fields.STATUS_REASON, new MappedField(CLIENT_STATUS_HISTORY.STATUS_REASON))
				.bind(Client.Fields.CLIENT_ID_ISSUED_AT, new MappedField(CLIENT.CLIENT_ID_ISSUED_AT))
				.bind(Client.Fields.CLIENT_LAST_MODIFIED_AT, new MappedField(CLIENT.CLIENT_LAST_MODIFIED_AT))
				.bind(Client.Fields.CLIENT_SECRET_EXPIRES_AT, new MappedField(CLIENT.CLIENT_SECRET_EXPIRES_AT))
				.bind(Client.Fields.ACCESS_TOKEN_EXPIRES_IN, new MappedField(CLIENT.ACCESS_TOKEN_EXPIRES_IN))
				.bind(PRIMARY_KEY, new MappedField(CLIENT.ID));

		Table<? extends Record> tables = CLIENT
				.join(CLIENT_STATUS_HISTORY)
						.on(CLIENT_STATUS_HISTORY.CLIENT_ID.eq(CLIENT.ID))
						.and(CLIENT_STATUS_HISTORY.END_DATE.isNull())
				.join(STATUS_TYPE).on(STATUS_TYPE.ID.eq(CLIENT_STATUS_HISTORY.STATUS_ID))
				.join(PARTNER).on(PARTNER.ID.eq(CLIENT.PARTNER_ID));

		this.query = JooqQuery.builder(this.support, this.fieldMap)
				.from(tables)
				.build();

		this.scopes = new LinkedClientScopes(config);
		this.statuses = new ClientStatusHistoryRepository(config);

	}

	public Client addClient(Client client) {
		Long partnerId = this.support.sql().select(PARTNER.ID)
				.from(PARTNER)
				.where(PARTNER.RESOURCE_ID.eq(client.getPartnerId()))
				.fetchOne().value1();

		ZonedDateTime updatedDate = client.getClientIdIssuedAt();
		JClientRecord record = this.support.sql().newRecord(CLIENT);
		record.setPartnerId(partnerId);
		record.setClientName(client.getClientName());
		record.setClientId(client.getId());
		record.setClientSecret(client.getClientSecret());
		record.setClientIdIssuedAt(client.getClientIdIssuedAt());
		record.setClientLastModifiedAt(client.getClientLastModifiedAt());
		record.setClientSecretExpiresAt(client.getClientSecretExpiresAt());
		record.setAccessTokenExpiresIn(client.getAccessTokenExpiresIn());
		record.store();

		Long clientId = record.getId();
		this.statuses.setStatus(clientId, client.getStatus(), client.getStatusReason(), updatedDate);
		this.scopes.add(clientId, client.getScope());

		return this.getClient(client.getId());
	}

	public Page<Client> getClients(PageRequest pageRequest) {
		return this.query.getPage(pageRequest, this::map);
	}

	public Client getClient(UUID id) {
		Record record = this.query.getRecordWhere(CLIENT.CLIENT_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			Client client = this.map(record);
			client.setScope(this.scopes.get(record.getValue(CLIENT.ID)));
			return client;
		}
	}

	private Client map(Record record) {
		Client client = new Client();
		client.setId(record.getValue(CLIENT.CLIENT_ID));
		client.setPartnerId(record.getValue(PARTNER.RESOURCE_ID));
		client.setClientIdIssuedAt(record.getValue(CLIENT.CLIENT_ID_ISSUED_AT));
		client.setClientLastModifiedAt(record.getValue(CLIENT.CLIENT_LAST_MODIFIED_AT));
		client.setClientName(record.getValue(CLIENT.CLIENT_NAME));
		client.setClientSecretExpiresAt(record.getValue(CLIENT.CLIENT_SECRET_EXPIRES_AT));
		client.setStatus(ClientStatus.valueOf(record.getValue(STATUS_TYPE.VALUE)));
		client.setStatusReason(record.getValue(CLIENT_STATUS_HISTORY.STATUS_REASON));
		client.setAccessTokenExpiresIn(record.getValue(CLIENT.ACCESS_TOKEN_EXPIRES_IN));
		return client;
	}

	public void updateClient(Client client) {
		ZonedDateTime modifiedDate = client.getClientLastModifiedAt();
		JClientRecord record = this.getClientRecord(client.getId());
		record.setClientName(client.getClientName());
		record.setClientLastModifiedAt(modifiedDate);
		record.setClientSecretExpiresAt(client.getClientSecretExpiresAt());
		record.setAccessTokenExpiresIn(client.getAccessTokenExpiresIn());
		record.store();

		Long clientId = record.getId();
		this.statuses.setStatus(clientId, client.getStatus(), client.getStatusReason(), modifiedDate);
		this.scopes.update(clientId, client.getScope());
	}

	public String getSecret(UUID clientId) {
		JClientRecord record = this.getClientRecord(clientId);
		return record == null ? null : record.getClientSecret();
	}

	public void updateSecret(UUID clientId, String secret) {
		JClientRecord record = this.getClientRecord(clientId);
		record.setClientSecret(secret);
		record.store();
	}

	private JClientRecord getClientRecord(UUID clientId) {
		return this.support.sql().fetchOne(CLIENT, CLIENT.CLIENT_ID.eq(clientId));
	}

	public void updateClientStatuses(UUID partnerId, ClientStatus status, String reason, ZonedDateTime updatedDate) {
		Long partner = this.support.sql().select(PARTNER.ID)
				.from(PARTNER)
				.where(PARTNER.RESOURCE_ID.eq(partnerId))
				.fetchOne().value1();

		this.support.sql().selectFrom(CLIENT)
				.where(CLIENT.PARTNER_ID.eq(partner))
				.fetch()
				.forEach(c -> this.statuses.setStatus(c.getId(), status, reason, updatedDate));
	}

}