package com.ngl.micro.partner.client;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;

/**
 * Partner client endpoint implementation.
 *
 * @author Paco Mendes
 */
public class ClientManagementEndpointImpl implements ClientManagementEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private ClientService clientService;

	public ClientManagementEndpointImpl(RequestProcessor processor, ClientService clientService) {
		this.processor = processor;
		this.clientService = clientService;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Client client = this.clientService.getClient(id);

		Resource resource = HAL.resource(client)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Client> page = this.clientService.getClients(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Client client) {
		Client created = this.clientService.createClient(client);
		Resource resource = HAL.resource(created).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response update(UUID id, Patch patch) {
		Client client = this.clientService.updateClient(id, patch);
		Resource resource = HAL.resource(client).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response secret(UUID clientId) {
		Client client = this.clientService.changeSecret(clientId);
		Resource resource = HAL.resource(client).build();
		return Response.ok(resource).build();
	}

}
