package com.ngl.micro.partner;

import static com.ngl.micro.partner.dal.generated.jooq.JNglMicroPartner.NGL_MICRO_PARTNER;

import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The partner application entry-point.
 *
 * @author Willy du Preez
 *
 */
public class PartnerApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new PartnerApplication().development();
	}

	public PartnerApplication() {
		super(PartnerApplicationConfiguration.class, NGL_MICRO_PARTNER);
	}

}
