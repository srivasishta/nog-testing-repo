package com.ngl.micro.partner.event.out;

import static com.ngl.micro.partner.dal.generated.jooq.Tables.ACCESS_TOKEN_REVOKED_EVENT;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import org.jooq.Configuration;

import com.ngl.micro.partner.dal.generated.jooq.tables.records.JAccessTokenRevokedEventRecord;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.dal.repository.PublishableEventRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.portability.SQLBooleans;

/**
 * Repository to manage {@link AccessTokenRevokedEvent}s.
 *
 * @author Willy du Preez
 *
 */
public class AccessTokenRevokedEventRepository
		implements PublishableEventRepository<AccessTokenRevokedEvent, Long> {

	private DAL support;

	public AccessTokenRevokedEventRepository(Configuration config) {
		 this.support= new DAL(config);
	}

	public AccessTokenRevokedEvent add(UUID tokenId, ZonedDateTime revokedDate, ZonedDateTime expirationDate) {
		JAccessTokenRevokedEventRecord record = this.support.sql().newRecord(ACCESS_TOKEN_REVOKED_EVENT);
		record.setCreatedDate(revokedDate);
		record.setTokenId(tokenId);
		record.setExpirationDate(expirationDate);
		record.store();

		return this.getEvent(record.getId()).get();
	}

	@Override
	public AccessTokenRevokedEvent addEvent(AccessTokenRevokedEvent event) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public Optional<AccessTokenRevokedEvent> getEvent(Long id) {
		AccessTokenRevokedEvent event = this.support.sql()
				.fetchOne(ACCESS_TOKEN_REVOKED_EVENT, ACCESS_TOKEN_REVOKED_EVENT.ID.eq(id))
				.map(r -> {
					return r == null ? null : new AccessTokenRevokedEvent(
							r.getValue(ACCESS_TOKEN_REVOKED_EVENT.ID),
							r.getValue(ACCESS_TOKEN_REVOKED_EVENT.CREATED_DATE),
							r.getValue(ACCESS_TOKEN_REVOKED_EVENT.TOKEN_ID),
							r.getValue(ACCESS_TOKEN_REVOKED_EVENT.EXPIRATION_DATE));
				});

		return Optional.ofNullable(event);
	}

	@Override
	public void markPublished(Long eventId) {
		this.support.sql().update(ACCESS_TOKEN_REVOKED_EVENT)
				.set(ACCESS_TOKEN_REVOKED_EVENT.PUBLISHED, SQLBooleans.toString(true))
				.where(ACCESS_TOKEN_REVOKED_EVENT.ID.eq(eventId))
				.execute();
	}

}
