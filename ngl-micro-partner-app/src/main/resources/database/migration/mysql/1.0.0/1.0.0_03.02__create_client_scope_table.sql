CREATE TABLE `client_scope` (

    -- Columns
    `client_id`               BIGINT(20) NOT NULL,
    `scope_id`                BIGINT(20) NOT NULL,

    -- Constraints
    PRIMARY KEY  (`client_id`, `scope_id`),
    FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
    FOREIGN KEY (`scope_id`) REFERENCES `scope` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
