CREATE TABLE `partner` (

    -- Columns
    `id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`       BINARY(16) NOT NULL,
    `name`              VARCHAR(50) NOT NULL,
    `logo_original`     VARCHAR(255),
    `logo_small`        VARCHAR(255),
    `logo_medium`       VARCHAR(255),
    `logo_large`        VARCHAR(255),

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`),
    UNIQUE KEY (`name`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;