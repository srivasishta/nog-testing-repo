CREATE TABLE `access_token_revoked_event` (

    -- Columns
    `id`                    BIGINT(20) NOT NULL AUTO_INCREMENT,
    `created_date`          DATETIME NOT NULL,
    `token_id`              BINARY(16) NOT NULL,
    `expiration_date`       DATETIME NOT NULL,
    `published`             CHAR(1) NOT NULL DEFAULT 'F',

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`token_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_atre_created_date` ON `access_token_revoked_event` (`created_date`);
CREATE INDEX `idx_atre_expiration_date` ON `access_token_revoked_event` (`expiration_date`);
