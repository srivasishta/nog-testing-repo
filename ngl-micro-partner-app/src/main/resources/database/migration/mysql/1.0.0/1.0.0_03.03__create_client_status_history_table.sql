CREATE TABLE `client_status_history` (

    -- Columns
    `id`              BIGINT(20) NOT NULL AUTO_INCREMENT,
    `client_id`       BIGINT(20) NOT NULL,
    `status_id`       BIGINT(20) NOT NULL,
    `status_reason`   VARCHAR(50) NOT NULL DEFAULT '',
    `start_date`      DATETIME NOT NULL,
    `end_date`        DATETIME,

    -- Columns
    PRIMARY KEY (`id`),
    FOREIGN KEY (`status_id`) REFERENCES `status_type` (`id`),
    FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;