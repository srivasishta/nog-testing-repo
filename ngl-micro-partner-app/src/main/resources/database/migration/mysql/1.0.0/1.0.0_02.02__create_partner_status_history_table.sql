CREATE TABLE `partner_status_history` (

    -- Columns
    `id`              BIGINT(20) NOT NULL AUTO_INCREMENT,
    `partner_id`      BIGINT(20) NOT NULL,
    `status_id`       BIGINT(20) NOT NULL,
    `status_reason`   VARCHAR(50) NOT NULL DEFAULT '',
    `start_date`      DATETIME NOT NULL,
    `end_date`        DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`status_id`) REFERENCES `status_type` (`id`),
    FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;