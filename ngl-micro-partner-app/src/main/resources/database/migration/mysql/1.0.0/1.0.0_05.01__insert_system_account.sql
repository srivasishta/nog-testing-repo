-- Default password: password
INSERT INTO `partner` (`id`, `resource_id`, `name`) VALUES (1, X'00000000000000000000000000000001', 'System');

INSERT INTO `client` (
		`id`,
		`partner_id`,
		`client_name`,
		`client_id`,
		`client_secret`,
		`client_id_issued_at`,
		`client_secret_expires_at`,
		`client_last_modified_at`,
		`access_token_expires_in`)
		VALUES (
		1,
		1,
		'System Client',
		X'00000000000000000000000000000001',
		'40000:b341ff1dcca02b77797442bbd36c2db40dcc93cac739bdba0f09204fbde4f5005f3c3039058d0d55fc1316162f6b96f83c0dd2f16d2d88dd62bc510d0c2b40d12f3bbc30e2a3d8b353b47814719ca5fe484b06a68c9f4dd77c9098e424302d088ca8ca6a48e2de309f32c12d1809fd96c2dd7c750c3ec598f75708b1eda2fca12674227f1fe005524cd0b8c66c7721f24568d852928dbd8c668d1be0e15852ad48982b85a3a17a2839b5abe93efd630bd6e670a84eb14d107fe61765261925159eb3e09946fc7b3c69a1178ad127c84b84f29e01c4b86f448e8e2d654d9b4751dd8a5389466d3ba9a725c7ddd3c805495187bb5f9d7fb95b7050cfccf3e543ee:4a617c5bead1fdd57e30188e0d19289f6d5c8d908ec0c1c5ce6d5b423d107a587a772ca75b543b20c4122e50007b615dfd0bde7e8b7566314ebf4eadd5930c1c656d184aabb3c9a630fe8706400cfc939a05622f924086965a0f653bb4431e6e82dbda5baea0484837257f1afa6a9edd8b7478a62c0d856544ec3c4575d0863f5a8668ca8ae704540dc265dd04406ee23e6ed74b85b509c1b2a6c7e1bf5bc611fe3fd7e3b82bf330981bad65e2402c3bfb18b4de805b2a36e8e5943a66e57a49cab9d57e33c2661909920ce6b4a9e8574f920f9bae042820eb8218b419e234e4abedff94cd776dc151cecf3b50a115f12c8cbfd31ea5b42dab3b23cdc85715f3',
		NOW(),
		null,
		NOW(),
		3600
		);

INSERT INTO `partner_status_history` (`partner_id`, `status_id`, `status_reason`, `start_date`) VALUES
(1, 1, 'Created system account', NOW());

INSERT INTO `client_status_history` (`client_id`, `status_id`, `status_reason`, `start_date`) VALUES
(1, 1, 'Created system client', NOW());

INSERT INTO `client_scope` (`client_id`, `scope_id`) SELECT 1, `scope`.`id` FROM `scope`;
