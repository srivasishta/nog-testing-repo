CREATE TABLE `access_token` (

    -- Columns
    `id`                     BIGINT(20) NOT NULL AUTO_INCREMENT,
    `client_id`              BIGINT(20) NOT NULL,
    `token_id`               BINARY(16) NOT NULL,
    `issued_date`            DATETIME NOT NULL,
    `expires_date`           DATETIME NOT NULL,
    `revoked_date`           DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
    UNIQUE KEY (`token_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_at_expires_date` ON `access_token` (`expires_date`);
