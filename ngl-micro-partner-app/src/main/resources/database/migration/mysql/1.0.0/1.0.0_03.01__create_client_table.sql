CREATE TABLE `client` (

    -- Columns
    `id`                       BIGINT(20) NOT NULL AUTO_INCREMENT,
    `partner_id`               BIGINT(20) NOT NULL,
    `client_id`                BINARY(16) NOT NULL,
    `client_name`              VARCHAR(50) NOT NULL,
    `client_secret`            TEXT NOT NULL,
    `client_id_issued_at`      DATETIME NOT NULL,
    `client_last_modified_at`  DATETIME NOT NULL,
    `client_secret_expires_at` DATETIME,
    `access_token_expires_in`  BIGINT(20) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`),
    UNIQUE KEY (`client_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
