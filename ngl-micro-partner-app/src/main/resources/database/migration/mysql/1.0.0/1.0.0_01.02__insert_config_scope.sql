INSERT INTO `scope` (`value`) VALUES
('business.read'),
('business.write'),
('store.read'),
('store.write'),
('sale.read'),

('charity.read'),
('contribution.read'),

('membership.read'),
('membership.write'),
('purchase.read'),

('partner.read'),
('partner.update'),

('transaction.read'),
('transaction.import'),

('image.read'),
('image.upload'),

('system.charity.write'),

('system.partner.write'),

('system.client.read'),
('system.client.write'),
('system.client.update'),

('system.image.update');