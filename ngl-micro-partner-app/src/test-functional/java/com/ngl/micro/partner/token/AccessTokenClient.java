//package com.ngl.micro.partner.token;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.UUID;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpHeaders;
//import org.apache.http.HttpStatus;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
//import org.apache.http.message.BasicHeader;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.ngl.middleware.rest.api.ApplicationStatus;
//import com.ngl.middleware.rest.api.error.ApplicationError;
//import com.ngl.middleware.rest.api.error.ApplicationError.ApplicationErrorBuilder;
//import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
//import com.ngl.middleware.rest.api.error.ApplicationException;
//import com.ngl.middleware.rest.client.http.MediaTypes;
//import com.ngl.middleware.util.Strings;
//
///**
// * Client to issue and revoking access tokens.
// *
// * @author Paco Mendes
// */
//public class AccessTokenClient {
//
//    public static final String CLIENT_ID = "client_id";
//    public static final String CLIENT_SECRET = "client_secret";
//    public static final String GRANT_TYPE = "grant_type";
//    public static final String CLIENT_CREDENTIALS_GRANT = "client_credentials";
//
//    public static final String SCOPE = "scope";
//    public static final String REVOKED_TOKEN_ID = "token";
//
//    public static final String ISSUE_TOKEN_ENDPOINT = "token";
//    public static final String REVOKE_TOKEN_ENDPOINT = "revoke";
//
//    private String parterServiceUrl;
//
//	private CloseableHttpClient http;
//	private ObjectMapper mapper;
//
//	public AccessTokenClient(String parterServiceUrl, ObjectMapper mapper) {
//		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
//		cm.setMaxTotal(1);
//		cm.setDefaultMaxPerRoute(1);
//		cm.setValidateAfterInactivity(1);
//
//		this.http = HttpClients.custom()
//				.setConnectionManager(cm)
////				.setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
//				.build();
//
//		this.mapper = mapper;
//		this.parterServiceUrl = parterServiceUrl;
//	}
//
//	public AccessTokenHolder issueToken(UUID clientId, String clientSecret, Set<String> scopes) throws Exception {
//		HttpPost accessTokenRequest = new HttpPost(this.parterServiceUrl + ISSUE_TOKEN_ENDPOINT);
//		accessTokenRequest.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MediaTypes.APPLICATION_FORM_URLENCODED));
//
//		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//		urlParameters.add(new BasicNameValuePair(GRANT_TYPE, CLIENT_CREDENTIALS_GRANT));
//		urlParameters.add(new BasicNameValuePair(CLIENT_ID, clientId.toString()));
//		urlParameters.add(new BasicNameValuePair(CLIENT_SECRET, clientSecret));
//		urlParameters.add(new BasicNameValuePair(SCOPE, String.join(" ", scopes)));
//
//		HttpEntity entity = new UrlEncodedFormEntity(urlParameters);
//		accessTokenRequest.setEntity(entity);
//
//		try {
//			CloseableHttpResponse response = this.http.execute(accessTokenRequest);
//			String body = EntityUtils.toString(response.getEntity());
//			response.close();
//
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
//				throw ApplicationError.notAuthorized().asException();
//			}
//
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
//				JsonNode json = this.mapper.readTree(body);
//				String message = "";
//				if (json.has("error")) {
//					message = json.get("error").asText();
//				}
//				throw ApplicationError.notAuthorized().asException(message);
//			}
//
//			if (!Strings.isNullOrWhitespace(body)) {
//				JsonNode json = this.mapper.readTree(body);
//				AccessTokenHolder token = new AccessTokenHolder();
//				token.setToken(json.get("tokenKey").asText());
//				List<String> registeredScopes = Arrays.asList(json.get("approvedScope").asText().split(" "));
//				token.setScopes(new HashSet<>(registeredScopes));
//				return token;
//			}
//		} catch (ApplicationException ae) {
//			throw ae;
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new ApplicationErrorBuilder(ApplicationStatus.INTERNAL_SERVER_ERROR).setCode(ApplicationErrorCode.UNEXPECTED_SERVER_ERROR).asException();
//		} finally {
//			accessTokenRequest.releaseConnection();
//		}
//		throw new ApplicationErrorBuilder(ApplicationStatus.INTERNAL_SERVER_ERROR).setCode(ApplicationErrorCode.UNEXPECTED_SERVER_ERROR).asException();
//	}
//
//	public void revokeToken(UUID clientId, String clientSecret, String token) throws Exception {
//		HttpPost accessTokenRequest = new HttpPost(this.parterServiceUrl + REVOKE_TOKEN_ENDPOINT);
//
//		accessTokenRequest.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MediaTypes.APPLICATION_FORM_URLENCODED));
//
//		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//		urlParameters.add(new BasicNameValuePair(GRANT_TYPE, CLIENT_CREDENTIALS_GRANT));
//		urlParameters.add(new BasicNameValuePair(CLIENT_ID, clientId.toString()));
//		urlParameters.add(new BasicNameValuePair(CLIENT_SECRET, clientSecret));
//		urlParameters.add(new BasicNameValuePair(REVOKED_TOKEN_ID, token));
//
//		HttpEntity entity = new UrlEncodedFormEntity(urlParameters);
//
//		accessTokenRequest.setEntity(entity);
//
//		try {
//			CloseableHttpResponse response = this.http.execute(accessTokenRequest);
//			EntityUtils.consume(response.getEntity());
//			response.close();
//
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//				return;
//			}
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
//				ApplicationError.notAuthorized().asException("Failed to revoke token.");
//			}
//		} catch (ApplicationException ae) {
//			throw ae;
//		} catch (Exception e) {
//			ApplicationError.badRequest().asException("Failed to revoke token.");
//		} finally {
//			accessTokenRequest.releaseConnection();
//		}
//	}
//
//	public void close() {
//		try {
//			this.http.close();
//		} catch (IOException e) {
//		}
//	}
//
//}
