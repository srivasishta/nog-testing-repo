package com.ngl.micro.partner.partner;

import static com.ngl.micro.partner.client.Scopes.PARTNER_READ_SCOPE;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.partner.AbstractPartnerFTCase;
import com.ngl.micro.partner.client.Client;
import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.partner.Partner.PartnerStatus;
import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

/**
 * The Class PartnerAccountFTCase.
 *
 * TODO test deactivating partner and all clients
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class PartnerManagementFTCase extends AbstractPartnerFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestPartners testPartners;
	private TestClients testClients;

	public PartnerManagementFTCase() {
		super();
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
	}

	@Test
	public void test_list_partners() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);
		this.cmdLoadTestDataSet(this.testPartners);

		PageRequest page = PageRequest.with(0, 2).build().filter(SearchQueries.and(Partner.Fields.STATUS).eq(PartnerStatus.ACTIVE));
		Page<Partner> partners = this.serviceClient.getPartnerManagementResource().list(page).getState();

		assertEquals(2, partners.getItems().size());
	}

	@Test
	public void test_partner_create_read_update() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Partner partner = this.testPartners.partner();
		partner.setLogoImages(new ImageSet());
		Partner created = this.serviceClient.getPartnerManagementResource()
				.create(partner)
				.getState();

		UUID partnerId = created.getId();
		partner.setId(partnerId);
		assertEquals(partner, created);

		Partner working = this.serviceClient.getPartnerManagementResource().get(partnerId).getState();
		working.setName(Data.NAME_A);
		working.setStatus(PartnerStatus.INACTIVE);
		working.setStatusReason(Data.REASON_DEACTIVATED);

		Patch firstPatch = new Diff().diff(working, partner);
		Partner updated = this.serviceClient.getPartnerManagementResource().patch(partnerId, firstPatch).getState();

		assertEquals(working, updated);

		working.setName(Data.NAME_B);
		working.setStatus(PartnerStatus.ACTIVE);
		working.setStatusReason(Data.REASON_ACTIVATED);

		Patch patch = new Diff().diff(working, updated);

		updated = this.serviceClient.getPartnerManagementResource().patch(partnerId, patch).getState();

		assertEquals(working, updated);

		Client client = this.testClients.client(null, partnerId, "partrner client");
		client = this.serviceClient.getClientManagementResource().create(client).getState();

		updated = this.serviceClient.getPartnerManagementResource().get(partnerId).getState();
		assertEquals(1, updated.getClients().size());
	}

	@Test
	public void test_deactivate_partner() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Partner working = this.testPartners.partner();
		working.setLogoImages(new ImageSet());
		Partner base = this.testPartners.partner();
		base.setLogoImages(new ImageSet());
		working = this.serviceClient.getPartnerManagementResource()
				.create(working)
				.getState();
		UUID partnerId = working.getId();

		Client client1 = this.testClients.client(null, partnerId, "client1");
		Client client2 = this.testClients.client(null, partnerId, "client2");

		client1 = this.serviceClient.getClientManagementResource().create(client1).getState();
		client2 = this.serviceClient.getClientManagementResource().create(client2).getState();

		assertEquals(ClientStatus.ACTIVE, client1.getStatus());
		assertEquals(ClientStatus.ACTIVE, client2.getStatus());

		String deactivatedReason = "partner deactivated";
		working.setStatus(PartnerStatus.INACTIVE);
		working.setStatusReason(deactivatedReason);

		Patch patch = new Diff().diff(working, base);
		working = this.serviceClient.getPartnerManagementResource().patch(partnerId, patch).getState();
		assertEquals(PartnerStatus.INACTIVE, working.getStatus());
		assertEquals(deactivatedReason, working.getStatusReason());
		assertEquals(2, working.getClients().size());

		base = working;
		working = this.serviceClient.getPartnerManagementResource().get(base.getId()).getState();
		working.getClients().containsAll(Collections.asSet(client1.getId(), client2.getId()));

		String reactivatedReason = "partner reactivated";
		working.setStatus(PartnerStatus.ACTIVE);
		working.setStatusReason(reactivatedReason);

		patch = new Diff().diff(working, base);
		working = this.serviceClient.getPartnerManagementResource().patch(partnerId, patch).getState();
		assertEquals(PartnerStatus.ACTIVE, working.getStatus());
		assertEquals(reactivatedReason, working.getStatusReason());

		working.getClients().containsAll(Collections.asSet(client1.getId(), client2.getId()));

	}

	@Test
	public void test_list_partners_with_unauthorised_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(new HashSet<>()));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getPartnerManagementResource().list(PageRequest.with(0, 1).build());
	}

	@Test
	public void test_read_partner_with_unauthorised_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(new HashSet<>()));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getPartnerManagementResource().get(Data.ID_1);
	}

	@Test
	public void test_create_partner_with_unauthorised_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(Collections.asSet(PARTNER_READ_SCOPE)));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getPartnerManagementResource().create(new Partner());
	}

	@Test
	public void test_update_partner_with_unauthorised_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(Collections.asSet(PARTNER_READ_SCOPE)));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getPartnerManagementResource().patch(Data.ID_1, new Patch(java.util.Collections.emptyList()));
	}

	@Test
	public void test_patch_partner_with_unauthorised_scope() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(Collections.asSet(PARTNER_READ_SCOPE)));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getPartnerManagementResource().patch(Data.ID_1, new Patch(new ArrayList<>()));
	}

}
