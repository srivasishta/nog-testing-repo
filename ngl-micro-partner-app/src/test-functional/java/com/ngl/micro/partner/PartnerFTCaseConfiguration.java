package com.ngl.micro.partner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.client.auth.AccessTokenClient;
import com.ngl.micro.partner.client.PartnerClient;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerConfiguration;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerProperties;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCaseConfiguration;
import com.ngl.middleware.rest.api.page.QueryParameterParser;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.HttpClient;
import com.ngl.middleware.rest.client.http.jackson.JacksonJsonMessageBodyWriter;
import com.ngl.middleware.rest.client.http.response.RestResponse;
import com.ngl.middleware.rest.json.ObjectMapperFactory;

@Configuration
@EnableMicroserviceJmsClient
@Import(HornetQEmbeddedServerConfiguration.class)
public class PartnerFTCaseConfiguration extends AbstractApplicationFTCaseConfiguration {

	public static final String PARTNER_SERVICE_URL = "http://localhost:9096/service/partner/";

	@Bean
	public HornetQEmbeddedServerProperties hornetQEmbeddedServerProperties(
			EnvironmentConfigurationProvider config) {
		return config.getConfiguration(
				HornetQEmbeddedServerProperties.DEFAULT_PREFIX,
				HornetQEmbeddedServerProperties.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Bean
	public PartnerClient restClient(
			HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(PARTNER_SERVICE_URL, requestFactory, parser, mapper);
		return new PartnerClient(config);
	}

	@Bean
	public AccessTokenClient accessTokenClient() {
		ObjectMapper mapper = new ObjectMapperFactory().getInstance();

		HttpClient httpClient = HttpClient.withDefaultTransport()
				.register(new JacksonJsonMessageBodyWriter(mapper))
				.register(RestResponse.factory(mapper))
				.build();

		return AccessTokenClient.builder(httpClient).build();
	}

}
