package com.ngl.micro.partner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.partner.client.PartnerClient;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;

@ContextConfiguration(classes = {
	DefaultDatabaseTestConfiguration.class,
	PartnerFTCaseConfiguration.class
})
public abstract class AbstractPartnerFTCase extends AbstractApplicationFTCase {

	@Autowired
	protected PartnerClient serviceClient;

	public AbstractPartnerFTCase() {
		super(PartnerApplication.class);
	}

}