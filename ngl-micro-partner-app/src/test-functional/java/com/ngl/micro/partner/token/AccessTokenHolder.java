package com.ngl.micro.partner.token;

import java.util.HashSet;

import com.ngl.micro.partner.client.AccessToken;

public class AccessTokenHolder extends AccessToken {

	private String token;
	private HashSet<String> scopes;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public HashSet<String> getScopes() {
		return scopes;
	}

	public void setScopes(HashSet<String> scopes) {
		this.scopes = scopes;
	}

}
