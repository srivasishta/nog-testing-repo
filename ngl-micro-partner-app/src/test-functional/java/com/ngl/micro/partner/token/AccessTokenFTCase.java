package com.ngl.micro.partner.token;

import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_WRITE_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.PARTNER_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.PARTNER_UPDATE_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.SYS_CLIENT_READ_SCOPE;
import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ngl.micro.client.auth.AccessTokenClient;
import com.ngl.micro.client.auth.AccessTokenRequest;
import com.ngl.micro.client.auth.AccessTokenResponse;
import com.ngl.micro.client.auth.RevokeAccessTokenRequest;
import com.ngl.micro.partner.AbstractPartnerFTCase;
import com.ngl.micro.partner.client.Client;
import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.util.Collections;

/**
 * Test issuing and revoking of tokens.
 *
 * @author Paco Mendes
 */
public class AccessTokenFTCase extends AbstractPartnerFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	//TODO get client without access tokens
	// Update client scope for issued tokens

	@Autowired
	private AccessTokenClient tokenClient;

	private TestPartners testPartners;
	private TestClients testClients;

	public AccessTokenFTCase() {
		super();
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
	}

	@Test
	public void test_get_access_tokens_for_client() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(Collections.asSet(SYS_CLIENT_READ_SCOPE));

		client = this.serviceClient.getClientManagementResource().create(client).getState();

		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), new HashSet<>()));

		Assert.assertNotNull(token.getAccessToken());
		Assert.assertNotNull(token.getApprovedScopes());
		Assert.assertTrue(token.getApprovedScopes().containsAll(Collections.asSet(SYS_CLIENT_READ_SCOPE)));
	}

	@Test
	public void test_issue_all_registered_scope_tokens() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Set<String> availableScopes = asSet(
			PARTNER_READ_SCOPE,
			PARTNER_UPDATE_SCOPE,
			BUSINESS_READ_SCOPE,
			BUSINESS_WRITE_SCOPE);
		Client client = this.testClients.newClient();
		client.setScope(availableScopes);
		client = this.serviceClient.getClientManagementResource().create(client).getState();
		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), new HashSet<>()));

		assertNotNull(token.getAccessToken());
		assertEquals(availableScopes, token.getApprovedScopes());
	}

	@Test
	public void test_issue_access_token_with_unauthorized_scopes() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(Collections.asSet(
			PARTNER_READ_SCOPE,
			BUSINESS_READ_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();
		Set<String> unauthorizedScopes = Collections.asSet(PARTNER_UPDATE_SCOPE);

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);

		this.tokenClient.issueToken(this.tokenRequest(client.getId(), client.getClientSecret(), unauthorizedScopes));
	}

	@Test
	public void test_issue_access_token_with_invalid_credentials() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(Collections.asSet(
			PARTNER_READ_SCOPE,
			BUSINESS_READ_SCOPE));

		client = this.serviceClient.getClientManagementResource().create(client).getState();

		Set<String> unauthorizedScopes = Collections.asSet(PARTNER_UPDATE_SCOPE);

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);
		this.tokenClient.issueToken(this.tokenRequest(client.getId(), "bad secret", unauthorizedScopes));
	}

	@Test
	public void test_issue_access_token() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(Collections.asSet(
			SYS_CLIENT_READ_SCOPE,
			BUSINESS_READ_SCOPE,
			BUSINESS_WRITE_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();

		Set<String> issuedScopes = Collections.asSet(SYS_CLIENT_READ_SCOPE, BUSINESS_READ_SCOPE);
		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), issuedScopes));

		assertNotNull(token.getAccessToken());
		assertEquals(issuedScopes, token.getApprovedScopes());
	}
	@Test
	public void test_revoke_access_token() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(Collections.asSet(SYS_CLIENT_READ_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();
		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), asSet()));
		this.accessTokenProvider.setAccessToken(token.getAccessToken());
		//Access token valid
		this.serviceClient.getClientManagementResource().get(client.getId()).getState();
		this.tokenClient.revokeToken(
				this.revokeRequest(client.getId().toString(), client.getClientSecret(), token.getAccessToken()));

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);
		//Access token no longer valid
		this.serviceClient.getClientManagementResource().get(client.getId()).getState();
	}

	@Test
	public void test_revoke_access_token_with_invalid_credentials() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(asSet(SYS_CLIENT_READ_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();

		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), asSet()));

		this.accessTokenProvider.setAccessToken(token.getAccessToken());

		this.serviceClient.getClientManagementResource().get(client.getId()).getState();

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);
		this.tokenClient.revokeToken(this.revokeRequest(client.getId().toString(), "bad credentials", token.getAccessToken()));
	}

	@Test
	public void test_revoke_access_token_with_invalid_token() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(asSet(SYS_CLIENT_READ_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();

		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), asSet()));

		this.accessTokenProvider.setAccessToken(token.getAccessToken());

		this.serviceClient.getClientManagementResource().get(client.getId()).getState();

		this.thrown.expectStatus(ApplicationStatus.BAD_REQUEST);
		this.tokenClient.revokeToken(this.revokeRequest(client.getId().toString(), client.getClientSecret(), ""));
	}

	@Test
	public void test_revoke_access_token_with_invalid_client_id() throws Exception {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client client = this.testClients.newClient();
		client.setScope(asSet(SYS_CLIENT_READ_SCOPE));
		client = this.serviceClient.getClientManagementResource().create(client).getState();

		AccessTokenResponse token = this.tokenClient.issueToken(
				this.tokenRequest(client.getId(), client.getClientSecret(), asSet()));

		this.accessTokenProvider.setAccessToken(token.getAccessToken());

		this.serviceClient.getClientManagementResource().get(client.getId()).getState();

		this.thrown.expectStatus(ApplicationStatus.BAD_REQUEST);
		this.tokenClient.revokeToken(this.revokeRequest("123", client.getClientSecret(), token.getAccessToken()));
	}

	private RevokeAccessTokenRequest revokeRequest(String clientId, String clientSecret, String token) {
		RevokeAccessTokenRequest request = new RevokeAccessTokenRequest();
		request.setClientId(clientId);
		request.setClientSecret(clientSecret);
		request.setAccessToken(token);
		return request;
	}

	private AccessTokenRequest tokenRequest(UUID clientId, String clientSecret, Set<String> scopes) {
		AccessTokenRequest request = new AccessTokenRequest();
		request.setClientId(clientId.toString());
		request.setClientSecret(clientSecret);
		request.setScopes(scopes);
		return request;
	}

}
