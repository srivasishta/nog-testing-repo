package com.ngl.micro.partner.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.partner.AbstractPartnerFTCase;
import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

/**
 * Client REST API tests.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class ClientFTCase extends AbstractPartnerFTCase {

	private TestPartners testPartners;
	private TestClients testClients;
	private TestAccessTokens testAccessTokens;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	public ClientFTCase() {
		super();
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
		this.testAccessTokens = new TestAccessTokens();
	}

	@Test
	public void test_read_authenticated_client_info() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);
		this.cmdLoadTestDataSet(this.testAccessTokens);

		Client client = this.serviceClient.getClientResource().get().getState();
		assertEquals(ApiPolicy.SYSTEM_CLIENT_ID, client.getId());
		assertEquals(ApiPolicy.SYSTEM_ACCOUNT_ID, client.getPartnerId());
		assertNotNull(client.getId());
		assertNotNull(client.getAccessTokenExpiresIn());
		assertNotNull(client.getClientIdIssuedAt());
		assertNotNull(client.getClientLastModifiedAt());
		assertNotNull(client.getClientName());
		assertNotNull(client.getStatus());
		assertNotNull(client.getStatusReason());
		assertNull(client.getClientSecret());
	}

	@Test
	public void test_reset_client_password() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);
		this.cmdLoadTestDataSet(this.testPartners);

		Client client = this.serviceClient.getClientManagementResource().create(this.testClients.newClient()).getState();
		String currentSecret = client.getClientSecret();

		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID
				.subjectClaim(client.getPartnerId())
				.clientIdClaim(client.getId()));

		Client updatedClient = this.serviceClient.getClientResource().secret(currentSecret).getState();
		String updatedSecret = updatedClient.getClientSecret();

		assertNotNull(currentSecret);
		assertNotNull(updatedSecret);
		assertFalse(currentSecret.equals(updatedSecret));
		assertTrue(updatedClient.getClientLastModifiedAt().isAfter(client.getClientLastModifiedAt()));
		client.setClientSecret(updatedSecret);
		client.setClientLastModifiedAt(updatedClient.getClientLastModifiedAt());
		assertEquals("Rest of client must be unchanged", client, updatedClient);
	}

	@Test
	public void test_reset_password_unauthorized_secret() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);

		Client client = this.serviceClient.getClientManagementResource()
				.create(this.testClients.newClient()).getState();

		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID
				.subjectClaim(client.getPartnerId())
				.clientIdClaim(client.getId())
				.scope(Scopes.VALUES));

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);
		this.serviceClient.getClientResource().secret(Data.STRING_9).getState();
	}

	@Test
	public void test_reset_password_unauthorized_token() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.BASIC_VALID.scope(new HashSet<>()));
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);

		this.thrown.expectStatus(ApplicationStatus.NOT_AUTHORIZED);
		this.serviceClient.getClientResource().secret(Data.STRING_1).getState();
	}

}
