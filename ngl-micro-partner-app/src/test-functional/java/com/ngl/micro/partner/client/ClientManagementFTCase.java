package com.ngl.micro.partner.client;

import static com.ngl.micro.partner.client.Client.ClientStatus.ACTIVE;
import static com.ngl.micro.partner.client.Client.ClientStatus.INACTIVE;
import static com.ngl.micro.partner.client.Scopes.SYS_CLIENT_READ_SCOPE;
import static com.ngl.micro.partner.client.Scopes.SYS_CLIENT_UPDATE_SCOPE;
import static com.ngl.micro.partner.client.Scopes.SYS_CLIENT_WRITE_SCOPE;
import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.partner.AbstractPartnerFTCase;
import com.ngl.micro.partner.test_data.TestAccessTokens;
import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;
import com.ngl.middleware.util.Time;

/**
 * @author Paco Mendes
 */
@Ignore
public class ClientManagementFTCase extends AbstractPartnerFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestPartners testPartners;
	private TestClients testClients;

	public ClientManagementFTCase() {
		super();
		this.testPartners = new TestPartners();
		this.testClients = new TestClients();
	}

	// reset password
	// reset pass unauth

	@Test
	public void test_client_listing() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.cmdLoadTestDataSet(this.testClients);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		int pageSize = 2;
		PageRequest page = PageRequest.with(0, pageSize).build().filter(SearchQueries.q(Client.Fields.STATUS).eq(ACTIVE));
		Page<Client> clients= this.serviceClient.getClientManagementResource().list(page).getState();
		assertEquals(pageSize, clients.getItems().size());
	}

	@Test
	public void test_client_create_read_update() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		String ignoredId = "ignored id";
		String ignoredSecret = "ignored secret";
		Set<String> expectedScope = Collections.asSet(SYS_CLIENT_READ_SCOPE, SYS_CLIENT_UPDATE_SCOPE);

		Client client = new Client();
		client.setId(null);
		client.setPartnerId(TestPartners.PARTNER_ID);
		client.setClientName("created client name");
		client.setClientSecret(ignoredSecret);
		client.setClientSecretExpiresAt(null);

		client.setScope(expectedScope);
		client.setAccessTokenExpiresIn(3600L);
		client.setStatus(INACTIVE);
		client.setStatusReason("loaded" );

		Client created = this.serviceClient.getClientManagementResource().create(client).getState();

		//Validate generated fields
		assertNotNull(created.getId());
		assertNotNull(created.getClientSecret());
		assertFalse(created.getId().equals(ignoredId));
		assertFalse(created.getClientSecret().equals(ignoredSecret));

		assertNotNull(created.getClientIdIssuedAt());
		assertNotNull(created.getClientLastModifiedAt());
		assertEquals(created.getClientIdIssuedAt(), created.getClientLastModifiedAt());

		//Validate created fields
		assertEquals(TestPartners.PARTNER_ID, created.getPartnerId());
		assertEquals("created client name", created.getClientName());
		assertEquals(Long.valueOf(3600L), created.getAccessTokenExpiresIn());
		assertEquals(INACTIVE, created.getStatus());
		assertEquals("loaded", created.getStatusReason());
		assertEquals(expectedScope, created.getScope());
		assertNull("expect no secret expiry", created.getClientSecretExpiresAt());

		//Update client
		ZonedDateTime updatedSecretExpiry = Time.utcNow().plusYears(1L);
		Set<String> updatedScope = asSet(SYS_CLIENT_READ_SCOPE, SYS_CLIENT_WRITE_SCOPE);

		Client working = this.serviceClient.getClientManagementResource().get(created.getId()).getState();
		working.setPartnerId(Data.ID_3);
		working.setClientIdIssuedAt(working.getClientIdIssuedAt().plusSeconds(1L));
		working.setClientName("updated client name");
		working.setClientSecret(ignoredSecret);
		working.setClientSecretExpiresAt(updatedSecretExpiry);
		working.setScope(updatedScope);
		working.setAccessTokenExpiresIn(7200L);
		working.setStatus(ACTIVE);
		working.setStatusReason("activated");

		Patch patch = new Diff().diff(working, created);
		Client updated = this.serviceClient.getClientManagementResource().patch(working.getId(), patch).getState();

		//Validate unchanged fields
		assertEquals(created.getId(), updated.getId());
		assertEquals(created.getClientIdIssuedAt(), updated.getClientIdIssuedAt());
		assertEquals(created.getPartnerId(), updated.getPartnerId());
		assertNull(updated.getClientSecret());

		//Validate updated fields
		assertTrue(updated.getClientLastModifiedAt().isAfter(created.getClientIdIssuedAt()));
		assertEquals("updated client name", updated.getClientName());
		assertEquals(Long.valueOf(7200L), updated.getAccessTokenExpiresIn());
		assertEquals(ACTIVE, updated.getStatus());
		assertEquals("activated", updated.getStatusReason());
		assertEquals(updatedScope, updated.getScope());
		assertEquals(updatedSecretExpiry, updated.getClientSecretExpiresAt());

		//Patch update
		working.setClientName("patched client name");
		working.getScope().add(Scopes.PARTNER_READ_SCOPE);

		patch = new Diff().diff(working, updated);
		Client patched = this.serviceClient.getClientManagementResource().patch(working.getId(), patch).getState();

		assertEquals("patched client name", patched.getClientName());
		assertEquals(updated.getScope().size() + 1, patched.getScope().size());
	}

	@Test
	public void test_update_client_scope() {
		//TODO update scope of issued access tokens
//		Test_Partners.using(sqlContext).loadData();
//		this.provideAccessToken();
//
//		String ignoredId = "ignored id";
//		String ignoredSecret = "ignored secret";
//		Set<String> expectedScope = asSet(CLIENT_READ_SCOPE, CLIENT_UPDATE_SCOPE);
//
//		Client client = new Client();
//		client.setId(null);
//		client.setPartnerId(PARTNER_ID);
//		client.setClientName("created client name");
//		client.setClientId(ignoredId);
//		client.setClientSecret(ignoredSecret);
//		client.setClientSecretExpiresAt(null);
//
//		client.setScope(expectedScope);
//		client.setAccessTokenExpiresIn(3600L);
//		client.setStatus(INACTIVE);
//		client.setStatusReason("loaded" );
//
//		Client created = serviceClient.getClientManagementResource().create(client).getState();
//
//		//Validate generated fields
//		assertNotNull(created.getId());
//		assertNotNull(created.getClientId());
//		assertNotNull(created.getClientSecret());
//		assertFalse(created.getClientId().equals(ignoredId));
//		assertFalse(created.getClientSecret().equals(ignoredSecret));
//
//		assertNotNull(created.getClientIdIssuedAt());
//		assertNotNull(created.getClientLastModifiedAt());
//		assertEquals(created.getClientIdIssuedAt(), created.getClientLastModifiedAt());
//
//		//Validate created fields
//		assertEquals(PARTNER_ID, created.getPartnerId());
//		assertEquals("created client name", created.getClientName());
//		assertEquals(Long.valueOf(3600L), created.getAccessTokenExpiresIn());
//		assertEquals(INACTIVE, created.getStatus());
//		assertEquals("loaded", created.getStatusReason());
//		assertEquals(expectedScope, created.getScope());
//		assertNull("expect no secret expiry", created.getClientSecretExpiresAt());
//
	}

	@Test
	public void test_reset_client_secret() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Client created = this.serviceClient.getClientManagementResource().create(this.testClients.newClient()).getState();
		Client updated = this.serviceClient.getClientManagementResource().secret(created.getId()).getState();

		assertNotNull(created.getClientSecret());
		assertNotNull(updated.getClientSecret());
		assertFalse(created.getClientSecret().equals(updated.getClientSecret()));
		assertTrue(updated.getClientLastModifiedAt().isAfter(created.getClientIdIssuedAt()));
	}

	@Test
	public void test_reset_client_secret_with_unauthorised_scope() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM_WITH_BUSINESS_READ_SCOPE);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getClientManagementResource().secret(TestPartners.PARTNER_ID);
	}

	@Test
	public void test_get_client_with_unauthorised_scope() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM_WITH_BUSINESS_READ_SCOPE);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getClientManagementResource().get(TestPartners.PARTNER_ID);
	}

	@Test
	public void test_create_client_with_unauthorised_scope() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM_WITH_BUSINESS_READ_SCOPE);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getClientManagementResource().create(new Client());
	}

	@Test
	public void test_patch_client_with_unauthorised_scope() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM_WITH_BUSINESS_READ_SCOPE);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getClientManagementResource().patch(TestPartners.PARTNER_ID, new Patch(new ArrayList<>()));
	}

	@Test
	public void test_list_client_with_unauthorised_scope() {
		this.cmdLoadTestDataSet(this.testPartners);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM_WITH_BUSINESS_READ_SCOPE);
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.serviceClient.getClientManagementResource().list(PageRequest.with(0,1).build());
	}

}
