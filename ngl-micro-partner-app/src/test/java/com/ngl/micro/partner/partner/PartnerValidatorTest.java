package com.ngl.micro.partner.partner;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.partner.test_data.TestPartners;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class PartnerValidatorTest {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestPartners builder;
	private PartnerValidator validator;
	private DataFactory dataFactory;

	@Before
	public void before() {
		this.builder = new TestPartners();
		this.validator = PartnerValidator.newInstance();
		this.dataFactory = new DataFactory();
	}

	@Test
	public void test_valid_account() {
		this.validator.validate(this.builder.partner());
	}

	@Test
	public void test_null_account_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("name", ValidationErrorCode.INVALID)
					.expectValidationError("status", ValidationErrorCode.INVALID)
					.expectValidationError("statusReason", ValidationErrorCode.INVALID);


		Partner partner = new Partner();
		partner.setClients(null);
		partner.setStatusReason(null);
		this.validator.validate(partner);
	}

	@Test
	public void test_max_field_length() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("name", ValidationErrorCode.INVALID)
					.expectValidationError("statusReason", ValidationErrorCode.INVALID);

		Partner partner = this.builder.partner();
		partner.setName(this.dataFactory.getRandomText(51));
		partner.setStatusReason(this.dataFactory.getRandomText(51));
		this.validator.validate(partner);
	}

	@Test
	public void test_min_field_length() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("name", ValidationErrorCode.INVALID);


		Partner partner = this.builder.partner();
		partner.setName("");
		this.validator.validate(partner);
	}
}
