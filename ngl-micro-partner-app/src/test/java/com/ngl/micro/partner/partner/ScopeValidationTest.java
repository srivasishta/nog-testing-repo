package com.ngl.micro.partner.partner;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.ngl.micro.shared.contracts.oauth.Scopes;

public class ScopeValidationTest {

	@Test
	public void test_validate_scopes() throws Exception {
		List<Field> apiFields = asList(com.ngl.micro.partner.client.Scopes.class.getDeclaredFields());
		apiFields = filterConstants(apiFields);
		for (Field apiField : apiFields) {
			String contactValue = (String) Scopes.class.getDeclaredField(apiField.getName()).get(String.class);
			assertThat(contactValue, CoreMatchers.is(apiField.get(String.class)));
		}

	}

	private List<Field> filterConstants(List<Field> unfiltered) {
		return unfiltered.stream().filter(f -> {
			int mod = f.getModifiers();
			return Modifier.isPublic(mod) && Modifier.isStatic(mod) && Modifier.isFinal(mod);
		}).collect(Collectors.toList());
	}

}
