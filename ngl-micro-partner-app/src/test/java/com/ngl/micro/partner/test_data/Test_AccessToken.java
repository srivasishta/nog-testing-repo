//package com.ngl.micro.partner.test_data;
//
//import java.util.Map;
//import java.util.UUID;
//
//import org.jooq.Configuration;
//import org.jooq.DSLContext;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JAccessTokenDao;
//import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JClientDao;
//import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JAccessToken;
//import com.ngl.micro.shared.contracts.oauth.Claims;
//import com.ngl.middleware.microservice.test.TestTokenDataSet;
//import com.ngl.middleware.rest.client.auth.AccessTokenProvider;
//import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtFactory;
//import com.ngl.middleware.test.data.DataProvider;
//import com.ngl.middleware.util.Time;
//
//public class Test_AccessToken extends TestTokenDataSet {
//
//	private static final Logger log = LoggerFactory.getLogger(Test_AccessToken.class);
//
//	private AccessTokenProvider provider;
//
//	private JwtFactory jwtFactory;
//
//	public Test_AccessToken(AccessTokenProvider provider, Configuration sqlContext, JwtFactory jwtFactory) {
//		super(sqlContext);
//		this.provider = provider;
//		this.jwtFactory = jwtFactory;
//	}
//
//	@Override
//	protected void loadData(DSLContext sql) {
//		try {
//			JAccessTokenDao dao = new JAccessTokenDao(sql.configuration());
//			JClientDao clientDao = new JClientDao(sql.configuration());
//			Map<String, Object> token = jwtFactory.consume(provider.getAccessToken());
//			UUID clientId = UUID.fromString((String) token.get(Claims.CLIENT_ID_CLAIM));
//			dao.insert(token(dao.count() + 1, clientDao.fetchOneByJClientId(clientId).getId(), provider.getAccessToken()));
//		} catch (Exception e) {
//			log.error("Failed to create token.", e);
//		}
//	}
//
//	private JAccessToken token(Long id, Long clientId, String tokenValue) {
//		JAccessToken token = new JAccessToken();
//		token.setId(id);
//		token.setClientId(clientId);
//		token.setToken(tokenValue);
//		token.setIssuedDate(Time.utcNow());
//		return token;
//	}
//
//}
