//package com.ngl.micro.partner.test_data;
//
//import static com.ngl.micro.partner.client.Scopes.CLIENT_READ_SCOPE;
//import static com.ngl.micro.partner.client.Scopes.CLIENT_UPDATE_SCOPE;
//import static com.ngl.micro.partner.client.Scopes.CLIENT_WRITE_SCOPE;
//import static com.ngl.micro.partner.client.Scopes.PARTNER_WRITE_SCOPE;
//import static com.ngl.micro.shared.contracts.oauth.Claims.PARTNER_ID_CLAIM;
//import static com.ngl.micro.shared.contracts.oauth.Claims.SCOPES_CLAIM;
//import static com.ngl.micro.shared.contracts.oauth.Scopes.PARTNER_READ_SCOPE;
//import static com.ngl.micro.shared.contracts.oauth.Scopes.PARTNER_UPDATE_SCOPE;
//import static com.ngl.middleware.microservice.test.OAuthTestUtils.scopes;
//
//import org.jooq.Configuration;
//import org.jooq.DSLContext;
//
//import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JAccessTokenDao;
//import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JAccessToken;
//import com.ngl.micro.shared.contracts.oauth.Claims;
//import com.ngl.middleware.database.test.AbstractJooqTestDataSet;
//import com.ngl.middleware.microservice.test.TestTokenBuilder;
//import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtFactory;
//import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
//import com.ngl.middleware.test.data.DataProvider;
//import com.ngl.middleware.util.Time;
//
///**
// * Access token dataset.
// *
// * @author Paco Mendes
// */
//public class Test_AccessTokens extends AbstractJooqTestDataSet {
//
//	public static Test_AccessTokens using(Configuration sqlContext) {
//		return new Test_AccessTokens(sqlContext);
//	}
//
//	private JwtFactory jwtFactory;
//
//	private Test_AccessTokens(Configuration sqlContext) {
//		super(sqlContext);
//		this.jwtFactory = new SignedEncryptedJwtFactory();
//	}
//
//	@Override
//	protected void loadData(DSLContext sql) {
//
//		JAccessTokenDao dao = new JAccessTokenDao(sql.configuration());
//
//		dao.insert(
//				token(2L, 2L, 2L),
//				token(3L, 2L, 3L),
//				token(4L, 3L, 4L),
//				token(5L, 3L, 5L),
//				token(6L, 4L, 6L)
//				);
//	}
//
//	private JAccessToken token(Long id, Long partnerId, Long clientId) {
//
//		String token = TestTokenBuilder.using(jwtFactory)
//				.withClaim(PARTNER_ID_CLAIM, DataProvider.toUUID(partnerId))
//				.withClaim(Claims.CLIENT_ID_CLAIM, DataProvider.toUUID(clientId))
//				.withClaim(SCOPES_CLAIM, scopes(
//						PARTNER_READ_SCOPE,
//						PARTNER_WRITE_SCOPE,
//						PARTNER_UPDATE_SCOPE,
//						CLIENT_READ_SCOPE,
//						CLIENT_WRITE_SCOPE,
//						CLIENT_UPDATE_SCOPE))
//				.build();
//
//		JAccessToken accessToken = new JAccessToken();
//		accessToken.setId(id);
//		accessToken.setClientId(clientId);
//		accessToken.setToken(token);
//		accessToken.setIssuedDate(Time.utcNow());
//		return accessToken;
//	}
//}
