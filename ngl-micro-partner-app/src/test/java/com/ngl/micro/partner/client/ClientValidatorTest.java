package com.ngl.micro.partner.client;

import static com.ngl.middleware.util.Collections.asSet;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.partner.test_data.TestClients;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class ClientValidatorTest {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestClients builder;
	private ClientValidator validator;
	private DataFactory dataFactory;

	@Before
	public void before() {
		this.builder = new TestClients();
		this.validator = ClientValidator.newInstance();
		this.dataFactory = new DataFactory();
	}

	@Test
	public void test_valid_client() {
		this.validator.validate(this.builder.newClient());
	}

	@Test
	public void test_null_client_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("partnerId", ValidationErrorCode.INVALID)
					.expectValidationError("clientName", ValidationErrorCode.INVALID)
					.expectValidationError("status", ValidationErrorCode.INVALID)
					.expectValidationError("statusReason", ValidationErrorCode.INVALID)
					.expectValidationError("scope", ValidationErrorCode.INVALID)
					.expectValidationError("accessTokenExpiresIn", ValidationErrorCode.INVALID);

		Client client = new Client();
		client.setStatusReason(null);
		client.setScope(null);
		this.validator.validate(client);
	}

	@Test
	public void test_max_field_length() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("clientName", ValidationErrorCode.INVALID)
					.expectValidationError("statusReason", ValidationErrorCode.INVALID)
					.expectValidationError("scope", ValidationErrorCode.INVALID);

		Client client = this.builder.newClient();
		client.setClientName(this.dataFactory.getRandomText(51));
		client.setStatusReason(this.dataFactory.getRandomText(51));
		client.setScope(asSet(this.dataFactory.getRandomText(256)));
		this.validator.validate(client);
	}

	@Test
	public void test_min_field_length() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("clientName", ValidationErrorCode.INVALID)
					.expectValidationError("accessTokenExpiresIn", ValidationErrorCode.INVALID)
					.expectValidationError("scope", ValidationErrorCode.INVALID);


		Client client = this.builder.newClient();
		client.setClientName("");
		client.setAccessTokenExpiresIn(-1L);
		client.setScope(asSet(""));
		this.validator.validate(client);
	}
}
