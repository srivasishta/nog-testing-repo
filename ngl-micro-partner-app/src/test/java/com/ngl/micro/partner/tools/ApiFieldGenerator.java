package com.ngl.micro.partner.tools;

import java.util.Arrays;
import java.util.Collections;

import com.ngl.micro.partner.client.Client;
import com.ngl.micro.partner.partner.ImageSet;
import com.ngl.micro.partner.partner.Partner;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields.
 *
 * @author Willy du Preez
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Arrays.asList(ImageSet.class)).generate(Partner.class);
		new ApiFieldsGenerator(Collections.emptyList()).generate(Client.class);
	}

}
