package com.ngl.micro.partner.test_data;

import static com.ngl.micro.partner.client.Scopes.BUSINESS_READ_SCOPE;
import static com.ngl.micro.partner.client.Scopes.BUSINESS_WRITE_SCOPE;
import static com.ngl.micro.partner.test_data.TestPartners.PARTNER_ID;
import static com.ngl.middleware.util.Collections.asSet;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.partner.client.Client;
import com.ngl.micro.partner.client.Client.ClientStatus;
import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JClientDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JClientStatusHistoryDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClient;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JClientStatusHistory;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.config.DefaultEnvironmentConfigurationProvider;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.security.EnvironmentAware;
import com.ngl.middleware.security.SecurityStrategyFactory;
import com.ngl.middleware.security.SecurityStrategyProperties;
import com.ngl.middleware.security.crypto.secret.SecretHashStrategy;
import com.ngl.middleware.test.data.Data;

public class TestClients implements JooqTestDataSet {

	public static UUID CLIENT_ID = Data.ID_4;
	public static Long CLIENT_PK = Data.PK_4;
	public static Long CLIENT_PARTNER_FK = Data.PK_3;

	public static UUID NEW_CLIENT_ID = Data.ID_8;
	public static Long NEW_CLIENT_PK = Data.PK_8;
	public static String NEW_CLIENT_NAME = Data.NAME_I;

	private static final SecretHashStrategy hash;

	static {
		EnvironmentConfigurationProvider config = new DefaultEnvironmentConfigurationProvider(new String[] { "test" });
		SecurityStrategyProperties security =
				config.getConfiguration(SecurityStrategyProperties.DEFAULT_PREFIX, SecurityStrategyProperties.class);
		hash = SecurityStrategyFactory.secretHashStrategy(config, security.getHash());
		if (hash instanceof EnvironmentAware) {
			((EnvironmentAware) hash).init(config);
		}
	}

	public Client newClient() {
		return this.client(NEW_CLIENT_ID, PARTNER_ID, NEW_CLIENT_NAME);
	}

	public Client client(UUID id, UUID partnerId, String name) {
		Client client = new Client();
		client.setId(id);
		client.setPartnerId(partnerId);
		client.setClientName(name);
		client.setStatus(ClientStatus.ACTIVE);
		client.setStatusReason(Data.REASON_CREATED);
		client.setClientSecret(Data.STRING_5);
		client.setClientIdIssuedAt(Data.T_MINUS_1M);
		client.setClientLastModifiedAt(Data.T_MINUS_10d);
		client.setClientSecretExpiresAt(Data.T_PLUS_1y);
		client.setAccessTokenExpiresIn(Data.SECONDS_1H);
		client.setScope(this.scopes());
		return client;
	}

	public Set<String> scopes() {
		return asSet(
				BUSINESS_READ_SCOPE,
				BUSINESS_WRITE_SCOPE);
	}

	@Override
	public void loadData(DSLContext sql) {

		JClientDao clientDao = new JClientDao(sql.configuration());
		JClientStatusHistoryDao statusDao = new JClientStatusHistoryDao(sql.configuration());

		clientDao.insert(
				this.client(Data.PK_2, TestPartners.PARTNER_PK, Data.NAME_A, Data.STRING_2),
				this.client(Data.PK_3, TestPartners.PARTNER_PK, Data.NAME_B, Data.STRING_3),
				this.client(CLIENT_PK, CLIENT_PARTNER_FK, Data.NAME_C, Data.STRING_4),
				this.client(Data.PK_5, Data.FK_3, Data.NAME_D, Data.STRING_5),
				this.client(Data.PK_6, Data.FK_4, Data.NAME_E, Data.STRING_6),
				this.client(Data.PK_7, Data.FK_5, Data.NAME_F, Data.STRING_7));

		statusDao.insert(
				this.status(TestPartners.PARTNER_PK, ResourceStatus.INACTIVE, Data.REASON_CREATED,   Data.T_MINUS_10d, Data.T_MINUS_5d),
				this.status(TestPartners.PARTNER_PK, ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_5d,  null),
				this.status(Data.FK_3,               ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null),
				this.status(Data.FK_4,               ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null),
				this.status(Data.FK_5,               ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null));
	}

	private JClient client(Long id, Long partnerId, String name, String clientSecret) {
		JClient client = new JClient();
		client.setId(id);
		client.setPartnerId(partnerId);
		client.setClientName(name);
		client.setClientId(Data.toUUID(id));
		client.setClientSecret(hash.createHash(clientSecret.toCharArray()));
		client.setClientIdIssuedAt(Data.T_0);
		client.setClientLastModifiedAt(Data.T_0);
		client.setClientSecretExpiresAt(null);
		client.setAccessTokenExpiresIn(Data.SECONDS_1H);
		return client;
	}

	private JClientStatusHistory status(long clientId, ResourceStatus status, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JClientStatusHistory history = new JClientStatusHistory();
		history.setClientId(clientId);
		history.setStatusId(status.getId());
		history.setStatusReason(reason);
		history.setStartDate(startDate);
		history.setEndDate(endDate);
		return history;
	}

}