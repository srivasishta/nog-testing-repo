package com.ngl.micro.partner.test_data;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JPartnerDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JPartnerStatusHistoryDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JPartner;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JPartnerStatusHistory;
import com.ngl.micro.partner.partner.Partner;
import com.ngl.micro.partner.partner.Partner.PartnerStatus;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

public class TestPartners implements JooqTestDataSet {

	public static UUID PARTNER_ID = Data.ID_2;
	public static Long PARTNER_PK = Data.PK_2;
	public static String PARTNER_NAME = Data.NAME_2;

	public Partner partner() {
		return this.partner(Data.toLong(PARTNER_ID));
	}

	public Partner partner(Long id) {
		Partner partner = new Partner();
		partner.setId(Data.toUUID(id));
		partner.setName(PARTNER_NAME);
		partner.setStatus(PartnerStatus.ACTIVE);
		partner.setStatusReason(Data.REASON_ACTIVATED);
		partner.setClients(new HashSet<>());
		partner.getLogoImages().setOriginal("http://cdn.images/orignal.jpg");
		partner.getLogoImages().setLarge("http://cdn.images/large.jpg");
		return partner;
	}

	@Override
	public void loadData(DSLContext sql) {

		JPartnerDao accountDao = new JPartnerDao(sql.configuration());
		JPartnerStatusHistoryDao statusDao = new JPartnerStatusHistoryDao(sql.configuration());

		accountDao.insert(
				this.account(PARTNER_PK, PARTNER_NAME),
				this.account(Data.PK_3, Data.NAME_3),
				this.account(Data.PK_4, Data.NAME_4),
				this.account(Data.PK_5, Data.NAME_5));

		statusDao.insert(
				this.status(PARTNER_PK, ResourceStatus.INACTIVE, Data.REASON_CREATED,   Data.T_MINUS_1M,  Data.T_MINUS_10d),
				this.status(PARTNER_PK, ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null),
				this.status(Data.FK_3,  ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null),
				this.status(Data.FK_4,  ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null),
				this.status(Data.FK_5,  ResourceStatus.ACTIVE,   Data.REASON_ACTIVATED, Data.T_MINUS_10d, null));
	}

	private JPartner account(Long id, String name) {
		JPartner partner = new JPartner();
		partner.setId(id);
		partner.setResourceId(Data.toUUID(id));
		partner.setName(name);
		return partner;
	}

	private JPartnerStatusHistory status(Long accountId, ResourceStatus status, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JPartnerStatusHistory history = new JPartnerStatusHistory();
		history.setPartnerId(accountId);
		history.setStatusId(status.getId());
		history.setStatusReason(reason);
		history.setStartDate(startDate);
		history.setEndDate(endDate);
		return history;
	}

}