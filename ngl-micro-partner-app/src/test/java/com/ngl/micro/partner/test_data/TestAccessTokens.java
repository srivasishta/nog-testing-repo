package com.ngl.micro.partner.test_data;

import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.partner.dal.generated.jooq.tables.daos.JAccessTokenDao;
import com.ngl.micro.partner.dal.generated.jooq.tables.pojos.JAccessToken;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

public class TestAccessTokens implements JooqTestDataSet {

	public static final UUID TOKEN_ID = Data.ID_1;

	public static TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);
	public static TestAccessToken BASIC_VALID = TestAccessToken.BASIC_VALID;
	public static TestAccessToken SYSTEM_WITH_BUSINESS_READ_SCOPE = new TestAccessToken()
			.subjectClaim(ApiPolicy.SYSTEM_ACCOUNT_ID)
			.clientIdClaim(ApiPolicy.SYSTEM_CLIENT_ID)
			.issuedDate(Data.T_0)
			.expiresDate(Data.T_PLUS_10H)
			.scope(Collections.asSet(Scopes.BUSINESS_READ_SCOPE));

	public static TestAccessToken full() {
		return new TestAccessToken()
				.subjectClaim(TestPartners.PARTNER_ID)
				.clientIdClaim(TestPartners.PARTNER_ID)
				.issuedDate(Data.T_0)
				.expiresDate(Data.T_PLUS_10H)
				.scope(Scopes.VALUES);
	}

	@Override
	public void loadData(DSLContext sql) {
		JAccessTokenDao dao = new JAccessTokenDao(sql.configuration());
		dao.insert(this.token(TOKEN_ID, TestClients.CLIENT_PARTNER_FK, TestClients.CLIENT_PK));
	}

	private JAccessToken token(UUID id, Long partnerId, Long clientId) {
		JAccessToken accessToken = new JAccessToken();
		accessToken.setId(Data.toLong(TOKEN_ID));
		accessToken.setClientId(clientId);
		accessToken.setTokenId(TOKEN_ID);
		accessToken.setIssuedDate(Data.T_0);
		accessToken.setExpiresDate(Data.T_PLUS_1H);
		return accessToken;
	}

}
