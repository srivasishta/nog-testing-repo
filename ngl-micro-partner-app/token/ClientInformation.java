package com.ngl.micro.partner.client;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import com.ngl.middleware.rest.api.Identifiable;

public class ClientInformation implements Identifiable<String> {

	private String id;
	private String clientSecret;
	private long clientIdIssuedAt;
	private long clientSecretExpiresAt;

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public long getClientIdIssuedAt() {
		return clientIdIssuedAt;
	}

	public void setClientIdIssuedAt(long clientIdIssuedAt) {
		this.clientIdIssuedAt = clientIdIssuedAt;
	}

	public long getClientSecretExpiresAt() {
		return clientSecretExpiresAt;
	}

	public void setClientSecretExpiresAt(long clientSecretExpiresAt) {
		this.clientSecretExpiresAt = clientSecretExpiresAt;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClientInformation)) {
			return false;
		}

		ClientInformation other = (ClientInformation) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.clientSecret, other.clientSecret) &&
				areEqual(this.clientIdIssuedAt, other.clientIdIssuedAt) &&
				areEqual(this.clientSecretExpiresAt, other.clientSecretExpiresAt);
	}

}
