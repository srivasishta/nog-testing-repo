package com.ngl.micro.partner.client;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import com.ngl.middleware.rest.api.Identifiable;

public class ClientRegistration implements Identifiable<String> {

	private String token;
	private long issuedAt;
	private long expiresIn;
	private long revokedAt;

	@Override
	public String getId() {
		return this.token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(long issuedAt) {
		this.issuedAt = issuedAt;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public long getRevokedAt() {
		return revokedAt;
	}

	public void setRevokedAt(long revokedAt) {
		this.revokedAt = revokedAt;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += hashValue(this.token);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClientRegistration)) {
			return false;
		}

		ClientRegistration other = (ClientRegistration) obj;

		return  areEqual(this.token, other.token) &&
				areEqual(this.issuedAt, other.issuedAt) &&
				areEqual(this.expiresIn, other.expiresIn) &&
				areEqual(this.revokedAt, other.revokedAt);
	}

}
