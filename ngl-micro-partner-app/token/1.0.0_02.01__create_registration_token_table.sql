CREATE TABLE `registration_token` (

    -- Columns
    `id`                     BIGINT(20) NOT NULL AUTO_INCREMENT,
    `account_id`             BIGINT(20) NOT NULL,
    `token`                  VARCHAR(255) NOT NULL,
    `issued_at`              BIGINT(20) NOT NULL,
    `expires_in`             BIGINT(20) NOT NULL,
    `revoked_at`             BIGINT(20) NOT NULL DEFAULT 0,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
    UNIQUE KEY (`token`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
