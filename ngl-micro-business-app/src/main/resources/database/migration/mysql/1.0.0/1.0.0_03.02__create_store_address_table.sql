CREATE TABLE `store_address` (

    -- Columns
    `id`                      BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`                BIGINT(20) NOT NULL,
    `street_address`          VARCHAR(255) NOT NULL,
    `locality`                VARCHAR(100) NOT NULL,
    `region`                  VARCHAR(50) NOT NULL,
    `postal_code`             VARCHAR(10) NOT NULL,
    `country`                 VARCHAR(2) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`store_id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

