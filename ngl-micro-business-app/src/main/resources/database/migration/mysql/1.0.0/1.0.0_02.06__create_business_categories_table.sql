CREATE TABLE `business_categories` (

    -- Columns
    `business_id`    BIGINT(20) NOT NULL,
    `category_id`    BIGINT(20) NOT NULL,

    -- Constraints
    PRIMARY KEY (`business_id`, `category_id`),
    FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
    FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
