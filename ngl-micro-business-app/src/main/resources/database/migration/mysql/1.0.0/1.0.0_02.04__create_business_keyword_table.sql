CREATE TABLE `business_keyword` (

    -- Columns
    `business_id`     BIGINT(20) NOT NULL,
    `keyword`         VARCHAR(50) NOT NULL,

    -- Constraints
    PRIMARY KEY (`business_id`, `keyword`),
    FOREIGN KEY (`business_id`) REFERENCES `business` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
