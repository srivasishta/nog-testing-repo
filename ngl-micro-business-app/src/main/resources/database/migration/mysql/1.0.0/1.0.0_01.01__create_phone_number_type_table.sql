CREATE TABLE `phone_number_type` (

    -- Columns
    `id`                      BIGINT(20) NOT NULL,
    `value`                   VARCHAR(50) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE(`value`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

