CREATE TABLE `sales` (

    -- Columns
    `id`                                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `contribution_document_id`          BINARY(16) NOT NULL,
    `business_id`                       BINARY(16) NOT NULL,
    `store_id`                          BINARY(16) NOT NULL,
    `customer_id`                       BINARY(16) NOT NULL,
    `sale_date`                         DATETIME NOT NULL,
    `sale_amount`                       DECIMAL(15,5) NOT NULL,
    `contribution_amount`               DECIMAL(15,5) NOT NULL,
    `demographics_gender`               BIGINT(20) NOT NULL,
    `demographics_birthdate`            DATE,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`contribution_document_id`),
    FOREIGN KEY (`business_id`) REFERENCES `business` (`resource_id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_sales__customer_id` ON `sales` (`customer_id`);
CREATE INDEX `idx_sales__sale_date` ON `sales` (`sale_date`);
CREATE INDEX `idx_sales__demographics_gender` ON `sales` (`demographics_gender`);
CREATE INDEX `idx_sales__demographics_birthdate` ON `sales` (`demographics_birthdate`);
