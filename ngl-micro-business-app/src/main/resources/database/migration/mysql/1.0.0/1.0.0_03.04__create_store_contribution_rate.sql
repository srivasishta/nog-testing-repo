CREATE TABLE `store_contribution_rate` (

    -- Columns
    `id`                   BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`             BIGINT(20) NOT NULL,
    `rate`                 DECIMAL(6,5) NOT NULL,
    `start_date`           DATETIME NOT NULL,
    `end_date`             DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE CASCADE

) ENGINE = InnoDB DEFAULT CHARSET = utf8;