CREATE TABLE `store_identifier` (

    -- Columns
    `id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`          BIGINT(20) NOT NULL,
    `external_id`       VARCHAR(50) NOT NULL,
    `start_date`        DATETIME NOT NULL,
    `end_date`          DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
