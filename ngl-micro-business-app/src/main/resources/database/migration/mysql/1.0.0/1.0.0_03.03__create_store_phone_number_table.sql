CREATE TABLE `store_phone_number` (

    -- Columns
    `id`                      BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`                BIGINT(20) NOT NULL,
    `phone_number_type_id`    BIGINT(20) NOT NULL,
    `country_code`            VARCHAR(5) NOT NULL,
    `area_code`               VARCHAR(8),
    `number`                  VARCHAR(20) NOT NULL,
    `extension`               VARCHAR(8),

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`store_id`, `phone_number_type_id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
    FOREIGN KEY (`phone_number_type_id`) REFERENCES `phone_number_type` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

