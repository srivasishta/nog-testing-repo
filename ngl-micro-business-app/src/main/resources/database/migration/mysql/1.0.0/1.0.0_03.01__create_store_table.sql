CREATE TABLE `store` (

		-- Columns
		`id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
		`resource_id`       BINARY(16) NOT NULL,
		`business_id`       BINARY(16) NOT NULL,
		`name`              VARCHAR(60) NOT NULL,
		`description`       VARCHAR(255),
		`website`           VARCHAR(255),
		`email_address`     VARCHAR(255),
		`timezone`          VARCHAR(50) NOT NULL,
		`lat`               DOUBLE,
		`lon`               DOUBLE,

		-- Constraints
		PRIMARY KEY (`id`),
		UNIQUE KEY (`resource_id`),
		FOREIGN KEY (`business_id`) REFERENCES `business` (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
