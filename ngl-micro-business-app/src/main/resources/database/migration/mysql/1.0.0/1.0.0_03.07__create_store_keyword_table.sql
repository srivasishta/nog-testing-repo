CREATE TABLE `store_keyword` (

    -- Columns
    `store_id`        BIGINT(20) NOT NULL,
    `keyword`         VARCHAR(50) NOT NULL,

    -- Constraints
    PRIMARY KEY (`store_id`, `keyword`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
