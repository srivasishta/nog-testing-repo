CREATE TABLE `store_status_type` (

    -- Columns
    `id`        BIGINT(20) NOT NULL,
    `value`     VARCHAR(8) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`value`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
