CREATE TABLE `business` (

    -- Columns
    `id`               BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`      BINARY(16) NOT NULL,
    `partner_id`       BINARY(16) NOT NULL,
    `name`             VARCHAR(60) NOT NULL,
    `description`      VARCHAR(255),
    `email_address`    VARCHAR(255),
    `website`          VARCHAR(255),
    `logo_original`    VARCHAR(255),
    `logo_small`       VARCHAR(255),
    `logo_medium`      VARCHAR(255),
    `logo_large`       VARCHAR(255),

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
