CREATE TABLE `business_phone_number` (

    -- Columns
    `id`                      BIGINT(20) NOT NULL AUTO_INCREMENT,
    `business_id`             BIGINT(20) NOT NULL,
    `phone_number_type_id`    BIGINT(20) NOT NULL,
    `country_code`            VARCHAR(5) NOT NULL,
    `area_code`               VARCHAR(8),
    `number`                  VARCHAR(20) NOT NULL,
    `extension`               VARCHAR(8),

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY (`business_id`, `phone_number_type_id`),
    FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
    FOREIGN KEY (`phone_number_type_id`) REFERENCES `phone_number_type` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

