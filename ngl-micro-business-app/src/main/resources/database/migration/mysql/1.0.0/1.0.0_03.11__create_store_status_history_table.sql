CREATE TABLE `store_status_history` (

    -- Columns
    `id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`          BIGINT(20) NOT NULL,
    `status_id`         BIGINT(20) NOT NULL,
    `status_reason`     VARCHAR(50) NOT NULL DEFAULT '',
    `start_date`        DATETIME NOT NULL,
    `end_date`          DATETIME,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
    FOREIGN KEY (`status_id`) REFERENCES `store_status_type` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;