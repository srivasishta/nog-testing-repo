CREATE TABLE `store_categories` (

    -- Columns
    `store_id`       BIGINT(20) NOT NULL,
    `category_id`    BIGINT(20) NOT NULL,

    -- Constraints
    PRIMARY KEY (`store_id`, `category_id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
    FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
