CREATE TABLE `store_trading_hours` (

    -- Columns
    `id`              BIGINT(20) NOT NULL AUTO_INCREMENT,
    `store_id`        BIGINT(20) NOT NULL,
    `start_day`       VARCHAR(10) NOT NULL,
    `end_day`         VARCHAR(10) NOT NULL,
    `opening_time`    TIME NOT NULL,
    `closing_time`    TIME NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
