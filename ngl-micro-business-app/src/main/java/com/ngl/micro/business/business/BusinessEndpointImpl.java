package com.ngl.micro.business.business;

import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.business.revision.VersionControlledEndpoint;
import com.ngl.micro.business.revision.VersionControlledEndpointDelegate;
import com.ngl.micro.business.sale.Sale;
import com.ngl.micro.business.sale.SaleService;
import com.ngl.micro.business.sale.SaleSummary;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Business Endpoint Implementation.
 *
 * @author Paco Mendes
 */
public class BusinessEndpointImpl implements BusinessEndpoint, VersionControlledEndpoint {

	@Context
	private MessageContext msgCtx;
	private RequestProcessor processor;

	private BusinessService businesses;
	private SaleService sales;
	private VersionControlledEndpointDelegate<Business, UUID> vc;

	public BusinessEndpointImpl(
			RequestProcessor processor,
			BusinessService businessService,
			SaleService sales,
			VersionControlledEndpointDelegate<Business, UUID> vc) {

		this.processor = processor;
		this.businesses = businessService;
		this.sales = sales;
		this.vc = vc;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Business business = this.businesses.getBusinessById(id);

		Resource resource = HAL.resource(business)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Business> page = this.businesses.getBusinesses(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Business business) {
		business = this.businesses.createBusiness(business);
		Resource resource = HAL.resource(business).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response update(UUID id, Patch patch) {
		this.businesses.updateBusiness(id, patch);
		return this.get(id);
	}

	@Override
	public Response sales(UUID id) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Sale> page = this.sales.getBusinessSales(id, pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}


	@Override
	public Response summary(UUID id) {
		SearchQuery query = this.processor.pageRequest(this.msgCtx).getSearchQuery();
		SaleSummary summary = this.sales.getBusinessSalesSummary(id, query);
		Resource resource = HAL.resource(summary).build();
		return Response.ok(resource).build();
	}

	@Override
	@RequiresScopes(BUSINESS_READ_SCOPE)
	public Response getRevisions(UUID resourceId) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		return this.vc.getRevisions(resourceId, pageRequest);
	}

	@Override
	@RequiresScopes(BUSINESS_READ_SCOPE)
	public Response getRevision(UUID resourceId, UUID revisionId) {
		return this.vc.getRevision(resourceId, revisionId);
	}

	@Override
	@RequiresScopes(BUSINESS_READ_SCOPE)
	public Response getLatestRevision(UUID resourceId) {
		return this.vc.getLatestRevision(resourceId);
	}

}
