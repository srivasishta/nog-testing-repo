package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_PHONE_NUMBER;

import java.util.HashSet;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStorePhoneNumberDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStorePhoneNumber;
import com.ngl.micro.business.shared.LinkedPhoneNumberType;
import com.ngl.middleware.dal.repository.AbstractLinkedDataSet;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedStorePhoneNumber extends AbstractLinkedDataSet<PhoneNumber, PhoneNumberType, Long> {

	private DAL support;
	private JStorePhoneNumberDao phoneNumberDao;

	public LinkedStorePhoneNumber(Configuration config) {
		this.support = new DAL(config);
		this.phoneNumberDao = new JStorePhoneNumberDao(config);
	}

	@Override
	public void add(Long businessId, Set<PhoneNumber> phoneNumbers) {
		Set<JStorePhoneNumber> numbers = new HashSet<>();

		for (PhoneNumber number : phoneNumbers) {
			JStorePhoneNumber numberRecord = new JStorePhoneNumber();
			numberRecord.setStoreId(businessId);
			numberRecord.setCountryCode(number.getCountryCode());
			numberRecord.setAreaCode(number.getAreaCode());
			numberRecord.setNumber(number.getNumber());
			numberRecord.setExtension(number.getExtension());
			numberRecord.setPhoneNumberTypeId(this.numberTypeId(number.getType()));
			numbers.add(numberRecord);
		}

		this.phoneNumberDao.insert(numbers);
	}

	@Override
	public Set<PhoneNumber> list(Long storeId) {
		HashSet<PhoneNumber> phoneNumbers = new HashSet<>();
		this.phoneNumberDao.fetchByJStoreId(storeId)
			.forEach((number) -> phoneNumbers.add(this.map(number)));;
		return phoneNumbers;
	}

	private PhoneNumber map(JStorePhoneNumber phoneNumber) {
		PhoneNumber number = new PhoneNumber();
		number.setType(this.numberType(phoneNumber.getPhoneNumberTypeId()));
		number.setCountryCode(phoneNumber.getCountryCode());
		number.setAreaCode(phoneNumber.getAreaCode());
		number.setNumber(phoneNumber.getNumber());
		number.setExtension(phoneNumber.getExtension());
		return number;
	}

	private PhoneNumberType numberType(long id) {
		return PhoneNumberType.valueOf(LinkedPhoneNumberType.forId(id).name());
	}

	private Long numberTypeId(PhoneNumberType type) {
		return LinkedPhoneNumberType.forEnum(type).getId();
	}

	@Override
	public void update(Long storeId, Set<PhoneNumber> phoneNumbers) {
		Set<PhoneNumber> currentNumbers = this.list(storeId);
		if (currentNumbers.equals(phoneNumbers)) {
			return;
		}
		if (!currentNumbers.isEmpty()) {
			this.deleteAll(storeId);
		}
		this.add(storeId, phoneNumbers);
	}

	private void deleteAll(Long storeId) {
		this.support.sql()
				.delete(STORE_PHONE_NUMBER)
				.where(STORE_PHONE_NUMBER.STORE_ID.eq(storeId))
				.execute();
	}

	@Override
	public PhoneNumber add(Long parentId, PhoneNumber items) {
		throw new UnsupportedOperationException();
	}

	@Override
	public PhoneNumber get(Long parentId, PhoneNumberType childId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Long parentId, PhoneNumber item) {
		throw new UnsupportedOperationException();
	}

}