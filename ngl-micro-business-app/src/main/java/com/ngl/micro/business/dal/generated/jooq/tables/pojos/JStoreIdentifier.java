/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables.pojos;


import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JStoreIdentifier implements Serializable {

	private static final long serialVersionUID = -1282716417;

	private Long          id;
	private Long          storeId;
	private String        externalId;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;

	public JStoreIdentifier() {}

	public JStoreIdentifier(JStoreIdentifier value) {
		this.id = value.id;
		this.storeId = value.storeId;
		this.externalId = value.externalId;
		this.startDate = value.startDate;
		this.endDate = value.endDate;
	}

	public JStoreIdentifier(
		Long          id,
		Long          storeId,
		String        externalId,
		ZonedDateTime startDate,
		ZonedDateTime endDate
	) {
		this.id = id;
		this.storeId = storeId;
		this.externalId = externalId;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getExternalId() {
		return this.externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public ZonedDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("JStoreIdentifier (");

		sb.append(id);
		sb.append(", ").append(storeId);
		sb.append(", ").append(externalId);
		sb.append(", ").append(startDate);
		sb.append(", ").append(endDate);

		sb.append(")");
		return sb.toString();
	}
}
