package com.ngl.micro.business.shared;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Persisted phone number types.
 *
 * @author Paco Mendes
 */
public enum LinkedPhoneNumberType {

	TEL(1L),
	FAX(2L);

	public static LinkedPhoneNumberType forId(Long id) {
		Assert.notNull(id);

		for (LinkedPhoneNumberType number : LinkedPhoneNumberType.values()) {
			if (number.id == id) {
				return number;
			}
		}
		throw new NoSuchElementException("No phone number type with ID: " + id);
	}

	public static LinkedPhoneNumberType forEnum(Enum<?> named) {
		String name = named.name();
		for (LinkedPhoneNumberType number : LinkedPhoneNumberType.values()) {
			if (number.name().equals(name)) {
				return number;
			}
		}
		throw new NoSuchElementException("No Phone number for enum: " + named);
	}

	private long id;

	private LinkedPhoneNumberType(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
