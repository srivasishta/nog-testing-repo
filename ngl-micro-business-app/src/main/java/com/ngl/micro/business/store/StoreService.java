package com.ngl.micro.business.store;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.business.BusinessRepository;
import com.ngl.micro.business.revision.VersionControlService;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.error.ValidationError;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.rest.patch.util.PatchedSets;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The Store service.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
@Transactional(rollbackOn = Exception.class)
public class StoreService {

	private BusinessRepository businesses;
	private StoreRepository stores;
	private VersionControlService<Store, UUID> storeVc;

	private StoreValidator storeValidator;
	private ExternalIdentifierValidator identifierValidator;
	private StoreSearchValidator searchValidator;

	private Merge mergeHandler;

	public StoreService(
			BusinessRepository businesses,
			StoreRepository stores,
			VersionControlService<Store, UUID> storeVc,
			StoreValidator storeValidator) {

		this.businesses = businesses;
		this.stores = stores;
		this.storeValidator = storeValidator;
		this.identifierValidator = new ExternalIdentifierValidator();
		this.searchValidator = new StoreSearchValidator();
		this.mergeHandler = new Merge();
		this.storeVc = storeVc;
	}

	public Store create(Store store) {
		ZonedDateTime createdDate = Time.utcNow();
		this.storeValidator.validate(store);
		Business business = this.getRequiredBusiness(store.getBusinessId());

		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isAccessibleByPartner(partnerId, business.getPartnerId())) {
			throw ApplicationError.forbidden()
				.setTitle("Unauthorized to Create Store")
				.setDetail("Stores can only be created by the system or an authorised business partner.")
				.asException();
		}

		store.setId(UUIDS.type4Uuid());
		store.getStoreCategories().removeAll(business.getCategories());
		store.getStoreKeywords().removeAll(business.getKeywords());
		this.enforceValidStatus(business, store);

		Store createdStore = this.stores.add(store);
		Long storeId = this.stores.getInternalId(store.getId());

		List<ValidationError> errors = new ArrayList<>();

		this.addIdentifiers(storeId, store.getExternalIdentifiers(), createdDate, errors);
		if (!errors.isEmpty()) {
			ApplicationError.validationError()
					.setCode(ApplicationErrorCode.VALIDATION_ERROR)
					.addValidationErrors(errors)
					.asException("Invalid store identifiers.");
		}

		createdStore = this.stores.get(createdStore.getId());
		this.storeVc.revise(
				createdDate,
				new Revision(),
				new Store(),
				createdStore,
				Subjects.getClientId(),
				Subjects.getSubject());

		return createdStore;

	}

	private Business getRequiredBusiness(UUID businessId) {
		Business business = this.businesses.get(businessId);
		if (business == null) {
			ApplicationError.validationError()
					.setCode(ApplicationErrorCode.VALIDATION_ERROR)
					.addValidationErrors(new ValidationError(
							Store.Fields.BUSINESS_ID,
							ValidationErrorCode.MISSING,
							"Unknown business."))
					.asException("Invalid business id.");
		}
		return business;
	}

	public Store getStoreById(UUID id) {
		Store store = this.stores.get(id);
		if (store == null) {
			ApplicationError notFound = ApplicationError.resourceNotFound().build();
			throw new ApplicationException(notFound, "No store found for the ID: " + id);
		}
		return store;
	}

	public Page<Store> getStores(PageRequest pageRequest) {
		this.searchValidator.validate(pageRequest.getSearchQuery());
		return this.stores.get(pageRequest);
	}

	public Store updateStore(UUID storeId, Patch patch) {

		Store base = this.getStoreById(storeId);
		Business business = this.businesses.get(base.getBusinessId());

		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isAccessibleByPartner(partnerId, business.getPartnerId())) {
			throw ApplicationError.forbidden()
				.setTitle("Unauthorized to Update Store")
				.setDetail("Stores can only be updated by the system or an authorised business partner.")
				.asException();
		}

		Store working = this.getStoreById(storeId);

		Set<ExternalIdentifier> existingIdentifiers = this.copyOf(working.getExternalIdentifiers());

		this.mergeHandler.merge(patch, working);
		this.storeValidator.validate(working);

		working.setId(storeId);
		working.setBusinessId(base.getBusinessId());
		working.getStoreCategories().removeAll(business.getCategories());
		working.getStoreKeywords().removeAll(business.getKeywords());
		this.enforceValidStatus(business, working);

		ZonedDateTime updatedDate = Time.utcNow();
		this.stores.update(working);

		Long internalId = this.stores.getInternalId(working.getId());

		Set<ExternalIdentifier> patchedIdentifiers = working.getExternalIdentifiers();
		this.updateIdentifiers(updatedDate, internalId, existingIdentifiers, patchedIdentifiers);

		Store updated = this.stores.get(working.getId());
		this.storeVc.revise(
				updatedDate,
				new Revision(),
				base,
				updated,
				Subjects.getClientId(),
				Subjects.getSubject());

		return updated;
	}

	private Set<ExternalIdentifier> copyOf(Set<ExternalIdentifier> identifiers) {
		Set<ExternalIdentifier> copy = new HashSet<>();
		for (ExternalIdentifier id : identifiers) {
			copy.add(new ExternalIdentifier(id));
		}
		return copy;
	}

	private void enforceValidStatus(Business business, Store store) {
		if (business.getStatus() == BusinessStatus.INACTIVE) {
			store.setStatus(StoreStatus.INACTIVE);
		}
	}

	private void updateIdentifiers(
			ZonedDateTime updatedDate,
			Long storeId,
			Set<ExternalIdentifier> referenceIdentifiers,
			Set<ExternalIdentifier> patchedIdentifiers) {

		Set<ExternalIdentifier> removed = PatchedSets.removed(referenceIdentifiers, patchedIdentifiers);
		Set<ExternalIdentifier> updated = PatchedSets.updated(referenceIdentifiers, patchedIdentifiers);
		Set<ExternalIdentifier> added = PatchedSets.added(patchedIdentifiers);

		List<ValidationError> errors = new ArrayList<>();

		this.expireIdentifiers(storeId, removed, updatedDate);
		this.updateIdentifiers(storeId, updated, updatedDate, errors);
		this.addIdentifiers(storeId, added, updatedDate, errors);

		if (!errors.isEmpty()) {
			ApplicationError.validationError()
			.addValidationErrors(errors)
			.asException("Failed to update store identifiers.");
		}
	}

	private void addIdentifiers(Long storeId, Set<ExternalIdentifier> identifiers, ZonedDateTime startDate, List<ValidationError> errors) {
		for (ExternalIdentifier identifier : identifiers) {
			identifier.setStartDate(startDate);
			Set<ExternalIdentifier> existingIdentifiers = this.stores.getExternalIdentifiers(identifier.getKey());
			errors.addAll(this.identifierValidator.validateIdentifier(identifier, existingIdentifiers, startDate));
			this.stores.addExternalIdentifier(storeId, identifier);
		}
	}

	private void updateIdentifiers(Long storeId, Set<ExternalIdentifier> identifiers, ZonedDateTime referenceDate, List<ValidationError> errors) {
		for (ExternalIdentifier identifier : identifiers) {
			Set<ExternalIdentifier> existingIdentifiers = this.stores.getExternalIdentifiers(identifier.getKey());
			errors.addAll(this.identifierValidator.validateIdentifier(identifier, existingIdentifiers, referenceDate));
			this.stores.updateExternalIdentifier(storeId, identifier);
		}
	}

	private void expireIdentifiers(Long storeId,  Set<ExternalIdentifier>  identifiers, ZonedDateTime expiryDate) {
		for (ExternalIdentifier identifier : identifiers) {
			identifier.setEndDate(expiryDate);
			this.stores.updateExternalIdentifier(storeId, identifier);
		}
	}

}