/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables.records;


import com.ngl.micro.business.dal.generated.jooq.tables.JPhoneNumberType;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JPhoneNumberTypeRecord extends UpdatableRecordImpl<JPhoneNumberTypeRecord> implements Record2<Long, String> {

	private static final long serialVersionUID = -1059139557;

	/**
	 * Setter for <code>ngl_micro_business.phone_number_type.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_business.phone_number_type.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_business.phone_number_type.value</code>.
	 */
	public void setValue(String value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_business.phone_number_type.value</code>.
	 */
	public String getValue() {
		return (String) getValue(1);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record2 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, String> fieldsRow() {
		return (Row2) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, String> valuesRow() {
		return (Row2) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JPhoneNumberType.PHONE_NUMBER_TYPE.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field2() {
		return JPhoneNumberType.PHONE_NUMBER_TYPE.VALUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value2() {
		return getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPhoneNumberTypeRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPhoneNumberTypeRecord value2(String value) {
		setValue(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPhoneNumberTypeRecord values(Long value1, String value2) {
		value1(value1);
		value2(value2);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JPhoneNumberTypeRecord
	 */
	public JPhoneNumberTypeRecord() {
		super(JPhoneNumberType.PHONE_NUMBER_TYPE);
	}

	/**
	 * Create a detached, initialised JPhoneNumberTypeRecord
	 */
	public JPhoneNumberTypeRecord(Long id, String value) {
		super(JPhoneNumberType.PHONE_NUMBER_TYPE);

		setValue(0, id);
		setValue(1, value);
	}
}
