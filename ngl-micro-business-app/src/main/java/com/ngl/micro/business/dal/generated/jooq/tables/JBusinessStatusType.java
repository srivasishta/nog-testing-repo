/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables;


import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.dal.generated.jooq.Keys;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JBusinessStatusTypeRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JBusinessStatusType extends TableImpl<JBusinessStatusTypeRecord> {

	private static final long serialVersionUID = 1679101374;

	/**
	 * The reference instance of <code>ngl_micro_business.business_status_type</code>
	 */
	public static final JBusinessStatusType BUSINESS_STATUS_TYPE = new JBusinessStatusType();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JBusinessStatusTypeRecord> getRecordType() {
		return JBusinessStatusTypeRecord.class;
	}

	/**
	 * The column <code>ngl_micro_business.business_status_type.id</code>.
	 */
	public final TableField<JBusinessStatusTypeRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_business.business_status_type.value</code>.
	 */
	public final TableField<JBusinessStatusTypeRecord, String> VALUE = createField("value", org.jooq.impl.SQLDataType.VARCHAR.length(8).nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_business.business_status_type</code> table reference
	 */
	public JBusinessStatusType() {
		this("business_status_type", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_business.business_status_type</code> table reference
	 */
	public JBusinessStatusType(String alias) {
		this(alias, BUSINESS_STATUS_TYPE);
	}

	private JBusinessStatusType(String alias, Table<JBusinessStatusTypeRecord> aliased) {
		this(alias, aliased, null);
	}

	private JBusinessStatusType(String alias, Table<JBusinessStatusTypeRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroBusiness.NGL_MICRO_BUSINESS, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JBusinessStatusTypeRecord> getPrimaryKey() {
		return Keys.KEY_BUSINESS_STATUS_TYPE_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JBusinessStatusTypeRecord>> getKeys() {
		return Arrays.<UniqueKey<JBusinessStatusTypeRecord>>asList(Keys.KEY_BUSINESS_STATUS_TYPE_PRIMARY, Keys.KEY_BUSINESS_STATUS_TYPE_VALUE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JBusinessStatusType as(String alias) {
		return new JBusinessStatusType(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JBusinessStatusType rename(String name) {
		return new JBusinessStatusType(name, null);
	}
}
