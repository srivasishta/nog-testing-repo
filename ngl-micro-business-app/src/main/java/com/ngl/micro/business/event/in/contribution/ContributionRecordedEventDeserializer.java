package com.ngl.micro.business.event.in.contribution;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.messaging.core.data.deserialize.JsonMessageDeserializer;

/**
 * De-serializes {@link ContributionRecordedEvent}s.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEventDeserializer extends JsonMessageDeserializer<ContributionRecordedEvent> {

	public ContributionRecordedEventDeserializer(ObjectMapper objectMapper) {
		super(objectMapper);
	}

	@Override
	protected Class<ContributionRecordedEvent> getEventType(String eventName) {
		if (ContributionRecordedEvent.NAME.equals(eventName)) {
			return ContributionRecordedEvent.class;
		}
		throw new UnsupportedOperationException("Unknown event type: " + eventName);
	}

}
