package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_CONTRIBUTION_RATE;
import static com.ngl.middleware.util.EqualsUtils.areEqual;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.jooq.Condition;
import org.jooq.Configuration;

import com.ngl.micro.business.dal.generated.jooq.tables.records.JStoreContributionRateRecord;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedContributionRate {

	private DAL support;

	public LinkedContributionRate(Configuration configuration) {
		this.support = new DAL(configuration);
	}

	public BigDecimal get(Long storeId) {
		return this.getRate(this.forStore(storeId));
	}

	public BigDecimal getAt(Long storeId, ZonedDateTime date) {
		return this.getRate(this.forStoreAt(storeId, date));
	}

	private Condition forStore(Long storeId) {
		return STORE_CONTRIBUTION_RATE.STORE_ID.eq(storeId)
				.and(STORE_CONTRIBUTION_RATE.END_DATE.isNull());
	}
	private Condition forStoreAt(Long storeId, ZonedDateTime date) {
		return STORE_CONTRIBUTION_RATE.STORE_ID.eq(storeId)
				.and(STORE_CONTRIBUTION_RATE.START_DATE.lessOrEqual(date))
				.and(STORE_CONTRIBUTION_RATE.END_DATE.isNull()
					.or(STORE_CONTRIBUTION_RATE.END_DATE.greaterThan(date)));
	}

	private BigDecimal getRate(Condition condition) {
		JStoreContributionRateRecord record = this.getRecord(condition);
		if (record == null) {
			return BigDecimal.ZERO.setScale(Currency.SCALE, Currency.ROUNDING);
		}
		return record.getRate();
	}

	private JStoreContributionRateRecord getRecord(Condition condition) {
		return this.support.sql()
				.selectFrom(STORE_CONTRIBUTION_RATE)
				.where(condition)
				.fetchOne();
	}

	public BigDecimal set(Long storeId, BigDecimal rate, ZonedDateTime date) {
		JStoreContributionRateRecord record = this.getRecord(this.forStore(storeId));
		rate = rate.setScale(Currency.SCALE, Currency.ROUNDING);
		if (record != null) {
			if (areEqual(rate, record.getRate())) {
				return rate;
			} else {
				this.expire(record, date);
			}
		}
		return this.add(storeId, rate, date);
	}

	private BigDecimal add(Long storeId, BigDecimal rate, ZonedDateTime createdDate) {
		JStoreContributionRateRecord contributionRateRecord = this.support.sql().newRecord(STORE_CONTRIBUTION_RATE);
		contributionRateRecord.setStoreId(storeId);
		contributionRateRecord.setStartDate(createdDate);
		contributionRateRecord.setEndDate(null);
		contributionRateRecord.setRate(rate);
		contributionRateRecord.store();
		return this.get(storeId);
	}

	private void expire(JStoreContributionRateRecord record, ZonedDateTime expiry) {
		record.setEndDate(expiry);
		record.store();
	}

}
