package com.ngl.micro.business.store;

import java.sql.Time;
import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.Configuration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreTradingHoursDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreTradingHours;
import com.ngl.middleware.dal.repository.AbstractLinkedDataSet;

public class LinkedTradingHours extends AbstractLinkedDataSet<TradingHours, Long, Long>{

	private JStoreTradingHoursDao tradingHoursDao;

	public LinkedTradingHours(Configuration config) {
		this.tradingHoursDao = new JStoreTradingHoursDao(config);
	}

	@Override
	public void add(Long storeId, Set<TradingHours> tradingHours) {
		Set<JStoreTradingHours> storeTradingHours = new HashSet<>();
		for (TradingHours tradingHour : tradingHours) {
			JStoreTradingHours hours = new JStoreTradingHours();
			hours.setStoreId(storeId);
			hours.setStartDay(tradingHour.getStartDay().name());
			hours.setEndDay(tradingHour.getEndDay().name());
			hours.setOpeningTime(Time.valueOf(tradingHour.getOpenTime()));
			hours.setClosingTime(Time.valueOf(tradingHour.getCloseTime()));
			storeTradingHours.add(hours);
		}
		this.tradingHoursDao.insert(storeTradingHours);
	}

	@Override
	public Set<TradingHours> list(Long storeId) {
		  return this.map(this.tradingHoursDao.fetchByJStoreId(storeId));
	}

	private Set<TradingHours> map(List<JStoreTradingHours> tradingHours) {
		return tradingHours
				.stream()
				.map(this::map)
				.collect(Collectors.toSet());
	}

	private TradingHours map(JStoreTradingHours storeHours) {
		TradingHours hours = new TradingHours();
		hours.setId(storeHours.getId());
		hours.setStartDay(DayOfWeek.valueOf(storeHours.getStartDay()));
		hours.setEndDay(DayOfWeek.valueOf(storeHours.getEndDay()));
		hours.setOpenTime(storeHours.getOpeningTime().toLocalTime());
		hours.setCloseTime(storeHours.getClosingTime().toLocalTime());
		return hours;
	}

	@Override
	public void update(Long storeId, Set<TradingHours> tradingHours) {
		List<JStoreTradingHours> currentHours = this.tradingHoursDao.fetchByJStoreId(storeId);
		if (this.map(currentHours).equals(tradingHours)) {
			return;
		}
		this.tradingHoursDao.delete(currentHours);
		this.add(storeId, tradingHours);
	}

	@Override
	public TradingHours add(Long parentId, TradingHours items) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TradingHours get(Long parentId, Long childId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Long parentId, TradingHours item) {
		// TODO Auto-generated method stub

	}
}