package com.ngl.micro.business;

import static com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness.NGL_MICRO_BUSINESS;

import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The business application entry-point.
 *
 * @author Paco Mendes
 *
 */
public class BusinessApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new BusinessApplication().development();
	}

	public BusinessApplication() {
		super(BusinessApplicationConfiguration.class, NGL_MICRO_BUSINESS);
	}

}