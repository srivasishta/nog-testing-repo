package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_IDENTIFIER;
import static com.ngl.middleware.util.EqualsUtils.areEqual;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.Configuration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreIdentifierDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreIdentifier;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JStoreIdentifierRecord;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedExternalIdentifier {

	private DAL support;
	private JStoreIdentifierDao identifierDao;

	public LinkedExternalIdentifier(Configuration config) {
		this.support = new DAL(config);
		this.identifierDao = new JStoreIdentifierDao(config);
	}

	public ExternalIdentifier add(Long storeId, ExternalIdentifier identifier) {
		JStoreIdentifierRecord record =  this.support.sql().newRecord(STORE_IDENTIFIER);
		record.setStoreId(storeId);
		record.setStartDate(identifier.getStartDate());
		record.setEndDate(identifier.getEndDate());
		record.setExternalId(identifier.getKey().trim());
		record.store();
		return this.get(record.getId());
	}

	private ExternalIdentifier get(Long id) {
		JStoreIdentifier identifier = this.identifierDao.fetchOneByJId(id);
		if (identifier == null) {
			return null;
		}
		return this.map(identifier);
	}

	public Set<ExternalIdentifier> get(String key) {
		return this.identifierDao.fetchByJExternalId(key)
				.stream()
				.map(this::map)
				.collect(Collectors.toSet());
	}

	public Set<ExternalIdentifier> getByStoreAt(Long storeId, ZonedDateTime date) {
		return this.support.sql()
				.selectFrom(STORE_IDENTIFIER)
				.where(STORE_IDENTIFIER.STORE_ID.eq(storeId))
				.and(STORE_IDENTIFIER.START_DATE.le(date))
				.and(STORE_IDENTIFIER.END_DATE.isNull()
					.or(STORE_IDENTIFIER.END_DATE.gt(date)))
				.fetch()
				.stream()
				.map(this::map)
				.collect(Collectors.toSet());
	}

	public Long getStoreByIdentifierAt(String key, ZonedDateTime date) {
		JStoreIdentifierRecord record = this.support.sql()
				.selectFrom(STORE_IDENTIFIER)
				.where(STORE_IDENTIFIER.EXTERNAL_ID.eq(key))
				.and(STORE_IDENTIFIER.START_DATE.le(date))
				.and(STORE_IDENTIFIER.END_DATE.isNull()
						.or(STORE_IDENTIFIER.END_DATE.gt(date)))
				.fetchOne();
		return record == null ? null : record.getStoreId();
	}

	private ExternalIdentifier map(JStoreIdentifierRecord storeIdentifier) {
		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setId(storeIdentifier.getId());
		identifier.setKey(storeIdentifier.getExternalId());
		identifier.setStartDate(storeIdentifier.getStartDate());
		ZonedDateTime endDate = (storeIdentifier.getEndDate() == null) ? null : storeIdentifier.getEndDate();
		identifier.setEndDate(endDate);
		return identifier;
	}

	private ExternalIdentifier map(JStoreIdentifier storeIdentifier) {
		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setId(storeIdentifier.getId());
		identifier.setKey(storeIdentifier.getExternalId());
		identifier.setStartDate(storeIdentifier.getStartDate());
		ZonedDateTime endDate = (storeIdentifier.getEndDate() == null) ? null : storeIdentifier.getEndDate();
		identifier.setEndDate(endDate);
		return identifier;
	}

	public void update(Long storeId, ExternalIdentifier pendingIdentifier) {
		JStoreIdentifier identifier = this.identifierDao.fetchOneByJId(pendingIdentifier.getId());
		if (identifier.getStoreId().equals(storeId)) {
			if(!areEqual(identifier.getEndDate(), pendingIdentifier.getEndDate())) {
				identifier.setEndDate(pendingIdentifier.getEndDate());
			}
			this.identifierDao.update(identifier);
		}
	}
}