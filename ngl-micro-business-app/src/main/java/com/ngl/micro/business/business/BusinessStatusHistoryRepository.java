package com.ngl.micro.business.business;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_STATUS_HISTORY;
import static java.util.stream.Collectors.toList;

import java.time.ZonedDateTime;
import java.util.List;

import org.jooq.Configuration;
import org.jooq.Result;

import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JBusinessStatusHistoryRecord;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.util.Experimental;

/**
 * Tracks business status history on all updates.
 *
 * @author Paco Mendes
 */
public class BusinessStatusHistoryRepository {

	private DAL support;

	public BusinessStatusHistoryRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public void setBusinessStatus(Long businessId, BusinessStatus status, String reason, ZonedDateTime updatedDate) {
		JBusinessStatusHistoryRecord currentStatus = this.getStatusRecordAt(businessId, updatedDate);
		if (currentStatus == null) {
			this.addStatusRecord(businessId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(businessId, status, reason, updatedDate);
		}
	}

	private boolean updated(JBusinessStatusHistoryRecord currentStatus, BusinessStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void addStatusRecord(Long businessId, BusinessStatus status, String reason, ZonedDateTime startDate) {
		JBusinessStatusHistoryRecord record =  this.support.sql().newRecord(BUSINESS_STATUS_HISTORY);
		record.setBusinessId(businessId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(startDate);
		record.store();
	}

	private void expireStatusRecord(JBusinessStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public BusinessStatusHistory getByBusinessIdAt(Long businessId, ZonedDateTime updatedDate) {
		JBusinessStatusHistoryRecord record = this.getStatusRecordAt(businessId, updatedDate);
		return record == null ? null : this.map(record);
	}

	public JBusinessStatusHistoryRecord getStatusRecordAt(Long businessId, ZonedDateTime updatedDate) {
		return this.support.sql()
				.selectFrom(BUSINESS_STATUS_HISTORY)
				.where(BUSINESS_STATUS_HISTORY.BUSINESS_ID.eq(businessId))
				.and(BUSINESS_STATUS_HISTORY.START_DATE.lessOrEqual(updatedDate))
				.and(BUSINESS_STATUS_HISTORY.END_DATE.greaterThan(updatedDate)
						.or(BUSINESS_STATUS_HISTORY.END_DATE.isNull()))
				.fetchOne();
	}

	@Experimental
	public List<BusinessStatusHistory> list(Long businessId) {
		Result<JBusinessStatusHistoryRecord> records = this.support.sql()
				.selectFrom(BUSINESS_STATUS_HISTORY)
				.where(BUSINESS_STATUS_HISTORY.BUSINESS_ID.eq(businessId))
				.fetch();

		return records.stream().map(this::map).collect(toList());
	}

	private BusinessStatusHistory map(JBusinessStatusHistoryRecord record) {
		BusinessStatusHistory status = new BusinessStatusHistory();
		status.setId(record.getId());
		status.setStatus(BusinessStatus.valueOf(ResourceStatus.forId(record.getStatusId()).name()));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}

}
