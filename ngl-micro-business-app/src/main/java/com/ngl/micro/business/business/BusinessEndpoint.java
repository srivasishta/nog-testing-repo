package com.ngl.micro.business.business;

import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_WRITE_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.SALE_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.http.PATCH;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Business endpoint exposing {@link Business} resource representations.
 *
 * @author Paco Mendes
 */
@Path("businesses")
public interface BusinessEndpoint extends RestEndpoint {

	@POST
	@RequiresScopes(BUSINESS_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(Business business);

	@GET
	@RequiresScopes(BUSINESS_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@GET
	@Path("{id}")
	@RequiresScopes(BUSINESS_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@PATCH
	@Path("{id}")
	@RequiresScopes(BUSINESS_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response update(@PathParam("id") UUID id, Patch patch);

	@GET
	@Path("{id}/sales")
	@RequiresScopes(SALE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response sales(@PathParam("id") UUID id);

	@GET
	@Path("{id}/sales/summary")
	@RequiresScopes(SALE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response summary(@PathParam("id") UUID id);
}
