package com.ngl.micro.business.business;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessKeywordDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessKeyword;

/**
 * Repo for business keywords.
 *
 * @author Paco Mendes
 */
public class LinkedBusinessKeyword {

	private JBusinessKeywordDao businessKeywordDao;

	public LinkedBusinessKeyword(Configuration config) {
		this.businessKeywordDao = new JBusinessKeywordDao(config);
	}

	public void add(Long businessId, Set<String> keywords) {
		Set<JBusinessKeyword> businessKeywords = new HashSet<>();
		for (String keyword : keywords) {
			businessKeywords.add(new JBusinessKeyword(businessId, keyword.toLowerCase()));
		}
		businessKeywordDao.insert(businessKeywords);
	}

	public Set<String> list(Long businessId) {
		return map(businessKeywords(businessId));
	}

	private List<JBusinessKeyword> businessKeywords(Long businessId) {
		return businessKeywordDao.fetchByJBusinessId(businessId);
	}

	private Set<String> map(List<JBusinessKeyword> businessKeywords) {
		return businessKeywords.stream()
				.map((keyword) -> keyword.getKeyword())
				.collect(toSet());
	}

	public void update(Long businessId, Set<String> keywords) {
		List<JBusinessKeyword> currentKeywords = businessKeywords(businessId);
		if (map(currentKeywords).equals(keywords)) {
			return;
		}
		if (!currentKeywords.isEmpty()) {
			businessKeywordDao.delete(currentKeywords);
		}
		add(businessId, keywords);
	}

}