package com.ngl.micro.business.sale;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An internal sale representation.
 *
 * @author Paco Mendes
 */
public class InternalSale extends Sale {

	private UUID customerId;

	@JsonIgnore
	public UUID getCustomerId() {
		return customerId;
	}

	public void setCustomerId(UUID customerId) {
		this.customerId = customerId;
	}
}
