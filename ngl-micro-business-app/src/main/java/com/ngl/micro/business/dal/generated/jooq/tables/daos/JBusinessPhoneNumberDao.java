/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables.daos;


import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessPhoneNumber;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JBusinessPhoneNumberRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JBusinessPhoneNumberDao extends DAOImpl<JBusinessPhoneNumberRecord, com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber, Long> {

	/**
	 * Create a new JBusinessPhoneNumberDao without any configuration
	 */
	public JBusinessPhoneNumberDao() {
		super(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER, com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber.class);
	}

	/**
	 * Create a new JBusinessPhoneNumberDao with an attached configuration
	 */
	public JBusinessPhoneNumberDao(Configuration configuration) {
		super(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER, com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJId(Long... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber fetchOneByJId(Long value) {
		return fetchOne(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.ID, value);
	}

	/**
	 * Fetch records that have <code>business_id IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJBusinessId(Long... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.BUSINESS_ID, values);
	}

	/**
	 * Fetch records that have <code>phone_number_type_id IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJPhoneNumberTypeId(Long... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.PHONE_NUMBER_TYPE_ID, values);
	}

	/**
	 * Fetch records that have <code>country_code IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJCountryCode(String... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.COUNTRY_CODE, values);
	}

	/**
	 * Fetch records that have <code>area_code IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJAreaCode(String... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.AREA_CODE, values);
	}

	/**
	 * Fetch records that have <code>number IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJNumber(String... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.NUMBER, values);
	}

	/**
	 * Fetch records that have <code>extension IN (values)</code>
	 */
	public List<com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber> fetchByJExtension(String... values) {
		return fetch(JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER.EXTENSION, values);
	}
}
