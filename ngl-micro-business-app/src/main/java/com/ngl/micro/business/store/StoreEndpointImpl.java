package com.ngl.micro.business.store;

import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_READ_SCOPE;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.JsonNode;
import com.ngl.micro.business.revision.VersionControlledEndpoint;
import com.ngl.micro.business.revision.VersionControlledEndpointDelegate;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.TypedResource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.jackson.ResourceReader;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Store Endpoint Implementation.
 *
 * @author Paco Mendes
 */
public class StoreEndpointImpl implements StoreEndpoint, VersionControlledEndpoint {

	@Context
	private MessageContext msgCtx;
	private RequestProcessor processor;
	private ResourceReader reader;

	private StoreService stores;
	private VersionControlledEndpointDelegate<Store, UUID> vc;

	public StoreEndpointImpl(
			RequestProcessor processor,
			ResourceReader reader,
			StoreService stores,
			VersionControlledEndpointDelegate<Store, UUID> vc) {

		Assert.notNull(processor);
		Assert.notNull(reader);
		Assert.notNull(stores);
		Assert.notNull(vc);

		this.processor = processor;
		this.reader = reader;
		this.stores = stores;
		this.vc = vc;
	}

	@Override
	public Response create(JsonNode store) throws IOException {
		TypedResource<Store> typed = this.reader.readResource(store, Store.class);
		Store created = this.stores.create(typed.getState());
		Resource resource = HAL.resource(created).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Store store = this.stores.getStoreById(id);

		Resource resource = HAL.resource(store)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Store> page = this.stores.getStores(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response patch(UUID id, Patch patch) {
		Fields fields = this.processor.fields(this.msgCtx);
		Store updated = this.stores.updateStore(id, patch);

		Resource resource = HAL.resource(updated)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	public Response getRevisions(UUID resourceId) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		return this.vc.getRevisions(resourceId, pageRequest);
	}

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	public Response getRevision(UUID resourceId, UUID revisionId) {
		return this.vc.getRevision(resourceId, revisionId);
	}

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	public Response getLatestRevision(UUID resourceId) {
		return this.vc.getLatestRevision(resourceId);
	}
}
