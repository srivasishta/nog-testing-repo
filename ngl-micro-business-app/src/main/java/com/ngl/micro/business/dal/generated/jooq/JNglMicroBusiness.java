/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq;


import com.ngl.micro.business.dal.generated.jooq.tables.JAccessTokenRevokedEvent;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusiness;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessCategories;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessKeyword;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessPhoneNumber;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessStatusHistory;
import com.ngl.micro.business.dal.generated.jooq.tables.JBusinessStatusType;
import com.ngl.micro.business.dal.generated.jooq.tables.JCategory;
import com.ngl.micro.business.dal.generated.jooq.tables.JContributionRecordedEvent;
import com.ngl.micro.business.dal.generated.jooq.tables.JGender;
import com.ngl.micro.business.dal.generated.jooq.tables.JPhoneNumberType;
import com.ngl.micro.business.dal.generated.jooq.tables.JSales;
import com.ngl.micro.business.dal.generated.jooq.tables.JSchemaVersion;
import com.ngl.micro.business.dal.generated.jooq.tables.JStore;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreAddress;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreCategories;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreContributionRate;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreIdentifier;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreKeyword;
import com.ngl.micro.business.dal.generated.jooq.tables.JStorePhoneNumber;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreStatusHistory;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreStatusType;
import com.ngl.micro.business.dal.generated.jooq.tables.JStoreTradingHours;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JNglMicroBusiness extends SchemaImpl {

	private static final long serialVersionUID = -1634282126;

	/**
	 * The reference instance of <code>ngl_micro_business</code>
	 */
	public static final JNglMicroBusiness NGL_MICRO_BUSINESS = new JNglMicroBusiness();

	/**
	 * No further instances allowed
	 */
	private JNglMicroBusiness() {
		super("ngl_micro_business");
	}

	@Override
	public final List<Table<?>> getTables() {
		List result = new ArrayList();
		result.addAll(getTables0());
		return result;
	}

	private final List<Table<?>> getTables0() {
		return Arrays.<Table<?>>asList(
			JAccessTokenRevokedEvent.ACCESS_TOKEN_REVOKED_EVENT,
			JBusiness.BUSINESS,
			JBusinessCategories.BUSINESS_CATEGORIES,
			JBusinessKeyword.BUSINESS_KEYWORD,
			JBusinessPhoneNumber.BUSINESS_PHONE_NUMBER,
			JBusinessStatusHistory.BUSINESS_STATUS_HISTORY,
			JBusinessStatusType.BUSINESS_STATUS_TYPE,
			JCategory.CATEGORY,
			JContributionRecordedEvent.CONTRIBUTION_RECORDED_EVENT,
			JGender.GENDER,
			JPhoneNumberType.PHONE_NUMBER_TYPE,
			JSales.SALES,
			JSchemaVersion.SCHEMA_VERSION,
			JStore.STORE,
			JStoreAddress.STORE_ADDRESS,
			JStoreCategories.STORE_CATEGORIES,
			JStoreContributionRate.STORE_CONTRIBUTION_RATE,
			JStoreIdentifier.STORE_IDENTIFIER,
			JStoreKeyword.STORE_KEYWORD,
			JStorePhoneNumber.STORE_PHONE_NUMBER,
			JStoreStatusHistory.STORE_STATUS_HISTORY,
			JStoreStatusType.STORE_STATUS_TYPE,
			JStoreTradingHours.STORE_TRADING_HOURS);
	}
}
