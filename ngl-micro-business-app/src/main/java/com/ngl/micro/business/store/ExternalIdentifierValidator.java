package com.ngl.micro.business.store;

import static com.ngl.micro.business.store.ExternalIdentifier.END_DATE;
import static com.ngl.micro.business.store.ExternalIdentifier.IDENTIFIER;
import static com.ngl.middleware.rest.api.error.ValidationErrorCode.INVALID;
import static com.ngl.middleware.util.EqualsUtils.areEqual;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.ngl.middleware.rest.api.error.ValidationError;

/**
 * Responsible for validating creating or updating of {@link ExternalIdentifier}.
 * Applied business rules for preventing overlapped or duplicate identifiers.
 *
 * @author Paco Mendes
 */
public class ExternalIdentifierValidator {

	public List<ValidationError> validateIdentifier(ExternalIdentifier identifier, Set<ExternalIdentifier> existingIdentifiers, ZonedDateTime updatedDate) {

		List<ValidationError> errors = new ArrayList<>();

		if (!validEndDate(identifier.getEndDate(), updatedDate)) {
			errors.add(new ValidationError(END_DATE, INVALID, String.format("End date must be null or future dated")));
		}
		if (alreadyAllocated(identifier, existingIdentifiers)) {
			String message = String.format("key '%s' is already allocated between '%s' and '%s'.",
					identifier.getKey(),
					identifier.getStartDate(),
					identifier.getEndDate());
			errors.add(new ValidationError(IDENTIFIER, INVALID, message));
		}
		return errors;
	}

	private boolean validEndDate(ZonedDateTime endDate, ZonedDateTime updatedDate) {
		return (endDate == null) || endDate.isAfter(updatedDate);
	}

	private boolean alreadyAllocated(ExternalIdentifier pendingIdentifier, Set<ExternalIdentifier> existingIdentifiers) {
		return existingIdentifiers
				.stream()
				.anyMatch(existingIdentifier -> isOverlapped(existingIdentifier, pendingIdentifier));
	}

	private boolean isOverlapped(ExternalIdentifier existing, ExternalIdentifier pending) {
		ZonedDateTime existingStart = existing.getStartDate();
		ZonedDateTime existingEnd = existing.getEndDate();
		ZonedDateTime pendingStart = pending.getStartDate();
		ZonedDateTime pendingEnd = pending.getEndDate();

		if (areEqual(existing.getId(), pending.getId())) {
			return false;
		}

		if (!pending.getKey().equals(existing.getKey())) {
			return false;
		}

		if ((existingEnd == null) && (pendingEnd == null)) {
			return true;
		}

		if (existingEnd == null) {
			return pendingEnd.isBefore(existingStart);
		}

		if (pendingEnd == null) {
			return existingEnd.isBefore(pendingStart);
		}
		return 	(pendingStart.isBefore(existingEnd) || pendingStart.equals(existingEnd)) &&
				(pendingEnd.isAfter(existingStart) || pendingEnd.equals(existingStart));
	}

}
