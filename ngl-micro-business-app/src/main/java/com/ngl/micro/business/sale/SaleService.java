package com.ngl.micro.business.sale;

import java.util.UUID;

import org.springframework.transaction.annotation.Transactional;

import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.util.Assert;

/**
 * Provides {@link Sale}s services.
 *
 * @author Willy du Preez
 */
@Transactional(rollbackFor = Exception.class)
public class SaleService {

	private SaleRepository sales;

	public SaleService(SaleRepository sales) {
		this.sales = sales;
	}

	public void recordSale(ContributionDocument document) {
		Assert.notNull(document, "Contribution document cannot be null");

		// Check if the sale has already been recorded.
		if (this.sales.getById(document.getId()).isPresent()) {
			return;
		}

		InternalSale sale = new InternalSale();
		sale.setId(document.getId());
		sale.setStoreId(document.getBusinessStoreId());
		sale.setSaleDate(document.getTransactionDate());
		sale.setSaleAmount(document.getTransactionAmount());
		sale.setContributionAmount(document.getContributionAmount());
		sale.setCustomerId(document.getMembershipId());

		MembershipDemographics demographics = document.getMembershipDemographics();
		sale.setDemographics(new SaleDemographics(
				demographics.getGender().name(),
				demographics.getBirthdate().orElse(null)));

		this.sales.addSale(document.getBusinessId(),sale);
	}

	public Page<Sale> getBusinessSales(UUID businessId, PageRequest request) {
		return this.sales.getSalesByBusinessId(businessId, request);
	}

	public SaleSummary getBusinessSalesSummary(UUID id, SearchQuery query) {
		return this.sales.getSaleSummaryByBusinessId(id, query);
	}

}
