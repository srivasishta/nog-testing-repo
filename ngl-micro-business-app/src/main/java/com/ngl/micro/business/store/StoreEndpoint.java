package com.ngl.micro.business.store;

import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_WRITE_SCOPE;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.ngl.micro.business.revision.VersionControlledEndpoint;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.http.PATCH;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Store endpoint exposing and managing {@link Store} resources.
 *
 * @author Paco Mendes
 */
@Path("stores")
public interface StoreEndpoint extends RestEndpoint, VersionControlledEndpoint {

	@POST
	@RequiresScopes(STORE_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(JsonNode store) throws IOException;

	@GET
	@RequiresScopes(STORE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@GET
	@Path("{id}")
	@RequiresScopes(STORE_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@PATCH
	@Path("{id}")
	@RequiresScopes(STORE_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response patch(@PathParam("id") UUID id, Patch patch);

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	Response getRevisions(UUID resourceId);

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	Response getLatestRevision(UUID resourceId);

	@Override
	@RequiresScopes(STORE_READ_SCOPE)
	Response getRevision(UUID resourceId, UUID revisionId);

}