package com.ngl.micro.business.business;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS;
import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_STATUS_HISTORY;
import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_STATUS_TYPE;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessDao;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JBusinessRecord;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.Time;

/**
 * Root repository for {@link Business} resources.
 *
 * @author Paco Mendes
 */
public class BusinessRepository implements PagingRepository<Business, UUID> {

	private static final String PRIMARY_KEY = "pk";

	private DAL support;
	private FieldMap<String> fieldMap;
	private JooqQuery query;

	private JBusinessDao businessDao;

	private LinkedBusinessPhoneNumber phoneNumbers;
	private LinkedBusinessKeyword keywords;
	private LinkedBusinessCategory categories;
	private BusinessStatusHistoryRepository statusHistory;

	public BusinessRepository(Configuration config) {
		this.support = new DAL(config);
		this.businessDao = new JBusinessDao(config);

		this.phoneNumbers = new LinkedBusinessPhoneNumber(config);
		this.keywords = new LinkedBusinessKeyword(config);
		this.categories = new LinkedBusinessCategory(config);
		this.statusHistory = new BusinessStatusHistoryRepository(config);
		this.fieldMap = new FieldMap<String>()
				.bind(Business.Fields.ID, new MappedField(BUSINESS.RESOURCE_ID).searchable(true))
				.bind(Business.Fields.PARTNER_ID, new MappedField(BUSINESS.PARTNER_ID).searchable(true))
				.bind(Business.Fields.STATUS, new MappedField(BUSINESS_STATUS_TYPE.VALUE).searchable(true))
				.bind(Business.Fields.STATUS_REASON, new MappedField(BUSINESS_STATUS_HISTORY.STATUS_REASON).searchable(true))
				.bind(Business.Fields.NAME, new MappedField(BUSINESS.NAME).searchable(true).sortable(true))
				.bind(Business.Fields.DESCRIPTION, new MappedField(BUSINESS.DESCRIPTION).searchable(true))
				.bind(Business.Fields.EMAIL_ADDRESS, new MappedField(BUSINESS.EMAIL_ADDRESS))
				.bind(Business.Fields.WEBSITE, new MappedField(BUSINESS.WEBSITE))
				.bind(Business.Fields.LOGO_IMAGES_ORIGINAL, BUSINESS.LOGO_ORIGINAL)
				.bind(Business.Fields.LOGO_IMAGES_THUMBNAIL, BUSINESS.LOGO_SMALL)
				.bind(Business.Fields.LOGO_IMAGES_MEDIUM, BUSINESS.LOGO_MEDIUM)
				.bind(Business.Fields.LOGO_IMAGES_LARGE, BUSINESS.LOGO_LARGE)
				.bind(PRIMARY_KEY, BUSINESS.ID);

		TableOnConditionStep<?> tables = BUSINESS
				.join(BUSINESS_STATUS_HISTORY).on(BUSINESS.ID.eq(BUSINESS_STATUS_HISTORY.BUSINESS_ID))
				.join(BUSINESS_STATUS_TYPE).on(BUSINESS_STATUS_HISTORY.STATUS_ID.eq(BUSINESS_STATUS_TYPE.ID)
				.and(BUSINESS_STATUS_HISTORY.END_DATE.isNull()));

		this.query = JooqQuery.builder(this.support, this.fieldMap)
				.from(tables)
				.build();
	}

	@Override
	public Business add(Business business) {
		JBusinessRecord businessRecord = this.support.sql().newRecord(BUSINESS);
		businessRecord.setResourceId(business.getId());
		businessRecord.setPartnerId(business.getPartnerId());
		businessRecord.setName(business.getName());
		businessRecord.setDescription(business.getDescription());
		businessRecord.setEmailAddress(business.getEmailAddress());
		businessRecord.setWebsite(business.getWebsite());
		businessRecord.store();

		Long businessId = businessRecord.getId();
		ZonedDateTime updatedDate = Time.utcNow();

		this.phoneNumbers.add(businessId, business.getPhoneNumbers());
		this.keywords.add(businessId, business.getKeywords());
		this.categories.add(businessId, business.getCategories());
		this.statusHistory.setBusinessStatus(businessId, business.getStatus(), business.getStatusReason(), updatedDate);
		return this.get(business.getId());
	}

	@Override
	public Business get(UUID id) {
		Record record = this.query.getRecordWhere(BUSINESS.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			Business business = this.mapRecord(record);
			Long pk = record.getValue(BUSINESS.ID);
			business.setKeywords(this.keywords.list(pk));
			business.setPhoneNumbers(this.phoneNumbers.list(pk));
			business.setCategories(this.categories.list(pk));
			return business;
		}
	}

	public UUID getIdByName(String name) {
		Record record = this.query.getRecordWhere(BUSINESS.NAME.equalIgnoreCase(name));
		if (record == null) {
			return null;
		} else {
			return record.getValue(BUSINESS.RESOURCE_ID);
		}
	}


	@Override
	public Page<Business> get(PageRequest pageRequest) {
		return this.query.getPage(pageRequest, this::mapRecord);
	}

	private Business mapRecord(Record record) {
		Business business = new Business();
		business.setId(record.getValue(BUSINESS.RESOURCE_ID));
		business.setPartnerId(record.getValue(BUSINESS.PARTNER_ID));
		business.setName(record.getValue(BUSINESS.NAME));
		business.setDescription(record.getValue(BUSINESS.DESCRIPTION));
		business.setWebsite(record.getValue(BUSINESS.WEBSITE));
		business.setEmailAddress(record.getValue(BUSINESS.EMAIL_ADDRESS));
		business.setStatus(BusinessStatus.valueOf(record.getValue(BUSINESS_STATUS_TYPE.VALUE)));
		business.setStatusReason(record.getValue(BUSINESS_STATUS_HISTORY.STATUS_REASON));
		String logoOriginal = record.getValue(BUSINESS.LOGO_ORIGINAL);
		String logoSmall = record.getValue(BUSINESS.LOGO_SMALL);
		String logoMedium = record.getValue(BUSINESS.LOGO_MEDIUM);
		String logoLarge = record.getValue(BUSINESS.LOGO_LARGE);
		if (!Strings.isNullOrWhitespace(logoOriginal)
				|| !Strings.isNullOrWhitespace(logoSmall)
				|| !Strings.isNullOrWhitespace(logoMedium)
				|| !Strings.isNullOrWhitespace(logoLarge)) {
			ImageSet logos = new ImageSet();
			logos.setOriginal(logoOriginal);
			logos.setThumbnail(logoSmall);
			logos.setLarge(logoLarge);
			logos.setMedium(logoMedium);
			business.setLogoImages(logos);
		}
		return business;
	}

	@Override
	public void update(Business business) {
		JBusinessRecord record = this.support.sql().fetchOne(BUSINESS, BUSINESS.RESOURCE_ID.eq(business.getId()));
		record.setName(business.getName());
		record.setDescription(business.getDescription());
		record.setEmailAddress(business.getEmailAddress());
		record.setWebsite(business.getWebsite());
		ImageSet logos = business.getLogoImages() == null ? new ImageSet() : business.getLogoImages();
		record.setLogoOriginal(logos.getOriginal());
		record.setLogoSmall(logos.getThumbnail());
		record.setLogoMedium(logos.getMedium());
		record.setLogoLarge(logos.getLarge());
		record.store();
		Long businessId = record.getId();

		ZonedDateTime updatedDate = Time.utcNow();
		this.phoneNumbers.update(businessId, business.getPhoneNumbers());
		this.keywords.update(businessId, business.getKeywords());
		this.categories.update(businessId, business.getCategories());
		this.statusHistory.setBusinessStatus(businessId, business.getStatus(), business.getStatusReason(), updatedDate);
	}

	@Override
	public long size() {
		return this.businessDao.count();
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException();
	}

}
