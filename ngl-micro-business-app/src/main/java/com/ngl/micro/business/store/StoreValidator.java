package com.ngl.micro.business.store;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.DecimalMaxDef;
import org.hibernate.validator.cfg.defs.DecimalMinDef;
import org.hibernate.validator.cfg.defs.DigitsDef;
import org.hibernate.validator.cfg.defs.EmailDef;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;
import org.hibernate.validator.cfg.defs.URLDef;

import com.ngl.micro.business.shared.ValueTypeValidationMappings;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.middleware.rest.server.validation.ResourceValidator;
import com.ngl.middleware.rest.server.validation.constraint.CoordinateDef;
import com.ngl.middleware.rest.server.validation.constraint.RestrictedZoneIdDef;
import com.ngl.middleware.rest.server.validation.constraint.UniqueKeySetDef;

public class StoreValidator extends ResourceValidator<Store> {

	private static final String MIN_RATE = "0";
	private static final String MAX_RATE = "1";
	private static final int MAX_DIGITS = 6;
	private static final int MAX_PRECISION = 5;

	public static StoreValidator newInstance() {
		return ResourceValidator.configurator(new StoreValidator()).configure();
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Store.class)
			.property("businessId", FIELD)
				.constraint(new NotNullDef())
			.property("name", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(60))
			.property("description", FIELD)
				.constraint(new SizeDef().min(0).max(255))
			.property("status", FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("website", FIELD)
				.constraint(new URLDef())
			.property("emailAddress", FIELD)
				.constraint(new EmailDef())
			.property("address", FIELD)
				.valid()
			.property("geoLocation", FIELD)
				.valid()
			.property("timezone", FIELD)
				.constraint(new RestrictedZoneIdDef().allowed(TimeZone.ALLOWED_ZONES))
			.property("phoneNumbers", FIELD)
				.constraint(new NotNullDef())
				.valid()
			.property("businessCategories", FIELD)
			.property("storeCategories", FIELD)
				.constraint(new NotNullDef())
			.property("businessKeywords", FIELD)
			.property("storeKeywords", FIELD)
				.constraint(new UniqueKeySetDef().min(1).max(50))
			.property("tradingHours", FIELD)
				.constraint(new NotNullDef())
				.valid()
			.property("externalIdentifiers", FIELD)
				.constraint(new NotNullDef())
				.valid()
			.property("contributionRate", FIELD)
				.constraint(new NotNullDef())
				.constraint(new DecimalMaxDef().value(MAX_RATE))
				.constraint(new DecimalMinDef().value(MIN_RATE))
				.constraint(new DigitsDef().integer(MAX_DIGITS).fraction(MAX_PRECISION))
		.type(Address.class)
			.property("streetAddress", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(255))
			.property("locality", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(100))
			.property("region", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("postalCode", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(10))
			.property("country", FIELD)
				.constraint(new NotNullDef())
		.type(GeoLocation.class)
			.property("lat", FIELD)
				.constraint(new CoordinateDef().lat().radians().nullable())
			.property("lon", FIELD)
				.constraint(new CoordinateDef().lon().radians().nullable())
		.type(TradingHours.class)
			.property("startDay", FIELD)
				.constraint(new NotNullDef())
			.property("endDay", FIELD)
				.constraint(new NotNullDef())
			.property("openTime", FIELD)
				.constraint(new NotNullDef())
			.property("closeTime", FIELD)
				.constraint(new NotNullDef())
		.type(ExternalIdentifier.class)
			.property("key", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(50))
			.property("startDate", FIELD)
			.property("endDate", FIELD);

		ValueTypeValidationMappings.configure(mapping);
	}

}
