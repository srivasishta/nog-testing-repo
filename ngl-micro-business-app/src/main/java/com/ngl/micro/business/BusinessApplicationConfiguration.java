package com.ngl.micro.business;

import java.util.UUID;
import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.business.BusinessEndpoint;
import com.ngl.micro.business.business.BusinessEndpointImpl;
import com.ngl.micro.business.business.BusinessRepository;
import com.ngl.micro.business.business.BusinessService;
import com.ngl.micro.business.business.BusinessValidator;
import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.event.in.contribution.ContributionRecordedEventDeserializer;
import com.ngl.micro.business.event.in.contribution.ContributionRecordedEventListener;
import com.ngl.micro.business.event.in.contribution.ContributionRecordedEventRepository;
import com.ngl.micro.business.event.in.token.AccessTokenRevokedEventListener;
import com.ngl.micro.business.event.in.token.AccessTokenRevokedEventRepository;
import com.ngl.micro.business.management.JsonStoreLookupService;
import com.ngl.micro.business.management.StoreLookupRepository;
import com.ngl.micro.business.revision.VersionControlService;
import com.ngl.micro.business.revision.VersionControlledEndpointDelegate;
import com.ngl.micro.business.revision.jooq.JVersionControlService;
import com.ngl.micro.business.sale.SaleRepository;
import com.ngl.micro.business.sale.SaleService;
import com.ngl.micro.business.store.Store;
import com.ngl.micro.business.store.StoreEndpoint;
import com.ngl.micro.business.store.StoreEndpointImpl;
import com.ngl.micro.business.store.StoreRepository;
import com.ngl.micro.business.store.StoreService;
import com.ngl.micro.business.store.StoreValidator;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.messaging.core.data.deserialize.LoggingDeserializationErrorHandler;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriber;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberConfiguration;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.hal.jackson.ResourceReader;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the business application.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
@Configuration
@EnableMicroserviceDatabase
@EnableMicroserviceRestServer
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
public class BusinessApplicationConfiguration extends MicroserviceConfiguration {

	private static final String BUSINESSES_SERVICE_ENDPOINT = "business";

	@Bean
	public RestEndpoints restEndpoints(
			ObjectMapper mapper,
			BusinessEndpoint businessEndpoint,
			StoreEndpoint storeEndpoint,
			JsonStoreLookupService storeLookupService,
			Realm securityRealm) {

		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints(BUSINESSES_SERVICE_ENDPOINT);
		restEndpoints.addEndpoints(businessEndpoint, storeEndpoint, storeLookupService);
		restEndpoints.setRealm(securityRealm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	/* --------------------------------------------------------
	 *  Business
	 * -------------------------------------------------------- */

	@Bean
	public BusinessRepository businessRepository(org.jooq.Configuration config) {
		return new BusinessRepository(config);
	}

	@Bean
	public BusinessValidator businessValidator() {
		return BusinessValidator.newInstance();
	}

	@Bean
	public VersionControlService<Business, UUID> businessVersionControlService(DAL dal, ObjectMapper mapper) {
		return new JVersionControlService<>(
				mapper,
				JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(),
				"business_revision",
				dal);
	}

	@Bean
	public BusinessService businessService(
			BusinessRepository businessRepository,
			StoreRepository storeRepository,
			BusinessValidator businessValidator,
			VersionControlService<Business, UUID> businessVersionControlService) {
		return new BusinessService(businessRepository, storeRepository, businessValidator, businessVersionControlService);
	}

	@Bean
	public BusinessEndpoint businessEndpoint(
			RequestProcessor processor,
			BusinessService businessService,
			SaleService salesService,
			VersionControlService<Business, UUID> businessVersionControlService) {
		return new BusinessEndpointImpl(
				processor, businessService, salesService, new VersionControlledEndpointDelegate<>(businessVersionControlService));
	}

	/* --------------------------------------------------------
	 *  Store
	 * -------------------------------------------------------- */

	@Bean
	public StoreRepository storeRepository(org.jooq.Configuration config) {
		return new StoreRepository(config);
	}

	@Bean
	public VersionControlService<Store, UUID> storeVersionControlService(DAL dal, ObjectMapper mapper) {
		return new JVersionControlService<>(
				mapper,
				JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(),
				"store_revision",
				dal);
	}

	@Bean
	public StoreValidator storeValidator() {
		return StoreValidator.newInstance();
	}

	@Bean
	public StoreService storeService(
			BusinessRepository businessRepository,
			StoreRepository storeRepository,
			VersionControlService<Store, UUID> storeVersionControlService,
			StoreValidator storeValidator) {
		return new StoreService(businessRepository, storeRepository, storeVersionControlService, storeValidator);
	}

	@Bean
	public StoreEndpoint storeEndpoint(
			RequestProcessor processor,
			ResourceReader reader,
			StoreService storeService,
			VersionControlService<Store, UUID> storeVersionControlService) {
		return new StoreEndpointImpl(
				processor, reader, storeService, new VersionControlledEndpointDelegate<>(storeVersionControlService));
	}

	/* --------------------------------------------------------
	 *  Management
	 * -------------------------------------------------------- */


	@Bean
	public StoreLookupRepository storeLookupRepository(org.jooq.Configuration config) {
		return new StoreLookupRepository(config);
	}

	@Bean
	public JsonStoreLookupService storeLookupService(StoreLookupRepository storeRepository) {
		return new JsonStoreLookupService(storeRepository);
	}

	/* --------------------------------------------------------
	 *  Sales
	 * -------------------------------------------------------- */

	@Bean
	public SaleRepository salesRepository(org.jooq.Configuration config) {
		return new SaleRepository(config);
	}

	@Bean
	public SaleService salesService(SaleRepository salesRepository) {
		return new SaleService(salesRepository);
	}

	@Bean
	public ContributionRecordedEventRepository contributionRecorderdEventRepository(org.jooq.Configuration config) {
		return new ContributionRecordedEventRepository(config);
	}

	@Bean
	public MessageListener<ContributionRecordedEvent> contributionRecordedEventListener(
			ContributionRecordedEventRepository contributionRecorderdEventRepository,
			SaleService salesService) {

		return new ContributionRecordedEventListener(contributionRecorderdEventRepository, salesService);
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber contributionRecordedEventSubscriber(
			ObjectMapper objectMapper,
			ConnectionFactory connectionFactory,
			MessageListener<ContributionRecordedEvent> eventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<ContributionRecordedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(10);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(ContributionRecordedEvent.NAME);
		config.setSubscription("business-" + ContributionRecordedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(new ContributionRecordedEventDeserializer(objectMapper));
		config.setMessageListener(eventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(ContributionRecordedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

	/* --------------------------------------------------------
	 *  Access Tokens
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration configuration) {
		return new AccessTokenRevokedEventRepository(configuration);
	}

	@Bean
	public RevocationList revocationList(AccessTokenRevokedEventRepository events) {
		return new RevocationList(events.getByExpirationDateGreaterThan(Time.utcNow()));
	}

	@Bean
	public AccessTokenRevokedEventListener accessTokenEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository accessTokenRevokedEventRepository,
			RevocationList revocationList) {
		return new AccessTokenRevokedEventListener(objectMapper, accessTokenRevokedEventRepository, revocationList);
	}

	@Bean
	public Realm securityRealm(RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(
				this.signedEncryptedJwtFactory(),
				revocationList));
	}

	@Bean
	public SignedEncryptedJwtFactory signedEncryptedJwtFactory() {
		return new SignedEncryptedJwtFactory();
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber accessTokenEventSubscriber(
			ConnectionFactory connectionFactory,
			AccessTokenRevokedEventListener accessTokenRevokedEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<AccessTokenRevokedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(1);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setSubscription("business-" + AccessTokenRevokedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(accessTokenRevokedEventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(accessTokenRevokedEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

}
