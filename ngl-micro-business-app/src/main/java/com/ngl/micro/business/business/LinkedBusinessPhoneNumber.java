package com.ngl.micro.business.business;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_PHONE_NUMBER;

import java.util.HashSet;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessPhoneNumberDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessPhoneNumber;
import com.ngl.micro.business.shared.LinkedPhoneNumberType;
import com.ngl.middleware.dal.repository.AbstractLinkedDataSet;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedBusinessPhoneNumber extends AbstractLinkedDataSet<PhoneNumber, PhoneNumberType, Long> {

	private DAL support;
	private JBusinessPhoneNumberDao phoneNumberDao;

	public LinkedBusinessPhoneNumber(Configuration config) {
		this.support = new DAL(config);
		this.phoneNumberDao = new JBusinessPhoneNumberDao(config);
	}

	@Override
	public void add(Long businessId, Set<PhoneNumber> phoneNumbers) {
		Set<JBusinessPhoneNumber> numbers = new HashSet<>();

		for (PhoneNumber number : phoneNumbers) {
			JBusinessPhoneNumber numberRecord = new JBusinessPhoneNumber();
			numberRecord.setBusinessId(businessId);
			numberRecord.setCountryCode(number.getCountryCode());
			numberRecord.setAreaCode(number.getAreaCode());
			numberRecord.setNumber(number.getNumber());
			numberRecord.setExtension(number.getExtension());
			numberRecord.setPhoneNumberTypeId(this.numberTypeId(number.getType()));
			numbers.add(numberRecord);
		}

		this.phoneNumberDao.insert(numbers);
	}

	@Override
	public Set<PhoneNumber> list(Long businessId) {
		HashSet<PhoneNumber> phoneNumbers = new HashSet<>();
		this.phoneNumberDao.fetchByJBusinessId(businessId)
			.forEach((number) -> phoneNumbers.add(this.map(number)));;
		return phoneNumbers;
	}

	private PhoneNumber map(JBusinessPhoneNumber phoneNumber) {
		PhoneNumber number = new PhoneNumber();
		number.setType(this.numberType(phoneNumber.getPhoneNumberTypeId()));
		number.setCountryCode(phoneNumber.getCountryCode());
		number.setAreaCode(phoneNumber.getAreaCode());
		number.setNumber(phoneNumber.getNumber());
		number.setExtension(phoneNumber.getExtension());
		return number;
	}

	private PhoneNumberType numberType(long id) {
		return PhoneNumberType.valueOf(LinkedPhoneNumberType.forId(id).name());
	}

	private Long numberTypeId(PhoneNumberType type) {
		return LinkedPhoneNumberType.forEnum(type).getId();
	}

	@Override
	public void update(Long businessId, Set<PhoneNumber> phoneNumbers) {
		Set<PhoneNumber> currentNumbers = this.list(businessId);
		if (currentNumbers.equals(phoneNumbers)) {
			return;
		}
		if (!currentNumbers.isEmpty()) {
			this.deleteAll(businessId);
		}
		this.add(businessId, phoneNumbers);
	}

	private void deleteAll(Long businessId) {
		this.support.sql()
				.delete(BUSINESS_PHONE_NUMBER)
				.where(BUSINESS_PHONE_NUMBER.BUSINESS_ID.eq(businessId))
				.execute();
	}

	@Override
	public PhoneNumber add(Long parentId, PhoneNumber items) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public PhoneNumber get(Long parentId, PhoneNumberType childId) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void update(Long parentId, PhoneNumber item) {
		throw new UnsupportedOperationException("Not implemented");
	}
}