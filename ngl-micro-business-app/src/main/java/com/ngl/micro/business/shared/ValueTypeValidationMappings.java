package com.ngl.micro.business.shared;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.PatternDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.micro.business.PhoneNumber;

public class ValueTypeValidationMappings {

	public static void configure(ConstraintMapping mapping) {
		mapping.type(PhoneNumber.class)
			.property("type", FIELD)
				.constraint(new NotNullDef())
			.property("countryCode", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(3))
				.constraint(new PatternDef().regexp("^[0-9]*$"))
			.property("areaCode", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(5))
				.constraint(new PatternDef().regexp("^[0-9]*$"))
			.property("number", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(13))
				.constraint(new PatternDef().regexp("^[0-9]*$"))
			.property("extension", FIELD)
				.constraint(new SizeDef().min(0).max(5))
				.constraint(new PatternDef().regexp("^[0-9]*$"));
	}

}
