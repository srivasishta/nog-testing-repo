package com.ngl.micro.business.management;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.StoreLookupRequest;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.micro.shared.contracts.business.StoreLookupService;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.annotation.PermitAll;

/**
 * Management endpoint implementation.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 *
 */
@Path("management/store-lookup")
public class JsonStoreLookupService implements RestEndpoint, StoreLookupService {

	private StoreLookupRepository stores;

	public JsonStoreLookupService(StoreLookupRepository storeRepository) {
		this.stores = storeRepository;
	}

	@Override
	@POST
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StoreLookupResponse lookup(StoreLookupRequest request) {
		MatchedStore store = this.stores.getByExternalIdAt(request.getStoreRefId(), request.getTransactionDate());
		return store == null ? this.unmatched() : this.matched(store);
	}

	private StoreLookupResponse unmatched() {
		StoreLookupResponse response = new StoreLookupResponse();
		response.setMatched(false);
		return response;
	}

	private StoreLookupResponse matched(MatchedStore store) {
		StoreLookupResponse response = new StoreLookupResponse();
		response.setMatched(true);
		response.setBusinessId(store.getBusinessId());
		response.setBusinessStatus(ResourceStatus.forEnum(store.getBusinessStatus()));
		response.setStoreId(store.getStoreId());
		response.setStoreStatus(ResourceStatus.forEnum(store.getStoreStatus()));
		response.setContributionRate(store.getStoreContributionRate());
		return response;
	}

}
