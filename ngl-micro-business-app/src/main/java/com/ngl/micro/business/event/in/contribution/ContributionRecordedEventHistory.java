package com.ngl.micro.business.event.in.contribution;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.util.Assert;

/**
 * Immutable.
 *
 * {@link ContributionRecordedEvent} history containing only the
 * {@link ContributionDocument} ID and not the document itself.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEventHistory {

	private Long id;
	private ZonedDateTime createdDate;
	private UUID documentId;

	public ContributionRecordedEventHistory(Long id, ZonedDateTime createdDate, UUID documentId) {
		Assert.notNull(id);
		Assert.notNull(createdDate);
		Assert.notNull(documentId);

		this.id = id;
		this.createdDate = createdDate;
		this.documentId = documentId;
	}

	public Long getId() {
		return this.id;
	}

	public ZonedDateTime getCreatedDate() {
		return this.createdDate;
	}

	public UUID getDocumentId() {
		return this.documentId;
	}

}
