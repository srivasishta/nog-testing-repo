package com.ngl.micro.business.sale;

import static com.ngl.micro.business.dal.generated.jooq.Tables.GENDER;
import static com.ngl.micro.business.dal.generated.jooq.Tables.SALES;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record4;
import org.jooq.Record7;
import org.jooq.Table;
import org.jooq.TableOnConditionStep;
import org.jooq.impl.DSL;

import com.ngl.micro.business.dal.generated.jooq.Tables;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JSalesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JSales;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JSalesRecord;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQueries;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.util.Assert;

/**
 * Manages {@link Sale}s.
 *
 * @author Willy du Preez
 *
 */
public class SaleRepository {

	private static final String BUSINESS_ID_FIELD = "businessId";
	private static final String CUSTOMER_ID_FIELD = "customerId";

	private static final String CUSTOMER_SUMMARY_TABLE = "customer_summary_table";
	private static final String CUSTOMER_VISITS = "customer_visits";
	private static final String CUSTOMER_GENDER = "customer_gender";
	private static final String CUSTOMER_PURCHASE_AMOUNT = "customer_purchase_amount";
	private static final String CUSTOMER_CONTRIBUTION_AMOUNT = "customer_contribution_amount";

	private DAL dal;
	private JSalesDao dao;
	private JooqQuery pages;
	private TableOnConditionStep<?> tables;
	private FieldMap<String> fields;

	public SaleRepository(Configuration config) {
		this.dal = new DAL(config);
		this.dao = new JSalesDao(config);

		this.fields = new FieldMap<String>()
				.bind(Sale.Fields.ID, new MappedField(SALES.CONTRIBUTION_DOCUMENT_ID).searchable(true))
				.bind(BUSINESS_ID_FIELD, new MappedField(SALES.BUSINESS_ID).searchable(true))
				.bind(CUSTOMER_ID_FIELD, new MappedField(SALES.CUSTOMER_ID).searchable(true))
				.bind(Sale.Fields.STORE_ID, new MappedField(SALES.STORE_ID).searchable(true))
				.bind(Sale.Fields.SALE_DATE, new MappedField(SALES.SALE_DATE).searchable(true).sortable(true))
				.bind(Sale.Fields.SALE_AMOUNT, SALES.SALE_AMOUNT)
				.bind(Sale.Fields.CONTRIBUTION_AMOUNT, SALES.CONTRIBUTION_AMOUNT)
				.bind(Sale.Fields.DEMOGRAPHICS_BIRTH_DATE, new MappedField(SALES.DEMOGRAPHICS_BIRTHDATE).searchable(true))
				.bind(Sale.Fields.DEMOGRAPHICS_GENDER, new MappedField(GENDER.VALUE).searchable(true));

		this.tables = SALES
				.join(GENDER).on(GENDER.ID.eq(SALES.DEMOGRAPHICS_GENDER));

		this.pages = JooqQuery.builder(this.dal, this.fields)
				.from(tables)
				.build();
	}

	public Sale addSale(UUID businessId, InternalSale sale) {
		Assert.notNull(sale);
		Assert.notNull(sale.getStoreId());

		JSalesRecord record = this.dal.sql().newRecord(Tables.SALES);
		record.setContributionDocumentId(sale.getId());
		record.setBusinessId(businessId);
		record.setStoreId(sale.getStoreId());
		record.setCustomerId(sale.getCustomerId());
		record.setSaleDate(sale.getSaleDate());
		record.setSaleAmount(sale.getSaleAmount());
		record.setContributionAmount(sale.getContributionAmount());
		record.setDemographicsBirthdate(sale.getDemographics().getBirthDate() == null
				? null : Date.valueOf(sale.getDemographics().getBirthDate()));
		record.setDemographicsGender(Gender.valueOf(sale.getDemographics().getGender()).getId());
		record.store();

		return sale;
	}

	public Optional<Sale> getById(UUID id) {
		JSales record = this.dao.fetchOneByJContributionDocumentId(id);

		if (record == null) {
			return Optional.empty();
		}

		SaleDemographics demographics = new SaleDemographics(
				Gender.forId(record.getDemographicsGender()).name(),
				record.getDemographicsBirthdate() == null ? null : record.getDemographicsBirthdate().toLocalDate());

		Sale sale = new Sale();
		sale.setId(id);
		sale.setStoreId(record.getStoreId());
		sale.setSaleDate(record.getSaleDate());
		sale.setSaleAmount(record.getSaleAmount());
		sale.setContributionAmount(record.getContributionAmount());
		sale.setDemographics(demographics);

		return Optional.of(sale);
	}

	public Page<Sale> getSalesByBusinessId(UUID businessId, PageRequest request) {
		request.filter(SearchQueries.and(BUSINESS_ID_FIELD).eq(businessId));
		return this.pages.getPage(request, this::map);
	}

	private Sale map(Record record) {
		Date birthdate = record.getValue(SALES.DEMOGRAPHICS_BIRTHDATE);
		SaleDemographics demographics = new SaleDemographics(
				record.getValue(GENDER.VALUE),
				birthdate == null ? null : birthdate.toLocalDate());

		Sale sale = new Sale();
		sale.setId(record.getValue(SALES.CONTRIBUTION_DOCUMENT_ID));
		sale.setStoreId(record.getValue(SALES.STORE_ID));
		sale.setSaleDate(record.getValue(SALES.SALE_DATE));
		sale.setSaleAmount(record.getValue(SALES.SALE_AMOUNT));
		sale.setContributionAmount(record.getValue(SALES.CONTRIBUTION_AMOUNT));
		sale.setDemographics(demographics);
		return sale;
	}

	public SaleSummary getSaleSummaryByBusinessId(UUID id, SearchQuery query) {
		org.jooq.Condition condition = JooqQueries.mapSearch(this.fields, query)
				.map(c -> c.and(SALES.BUSINESS_ID.eq(id)))
				.orElse(SALES.BUSINESS_ID.eq(id));

		Table<Record4<Long, Integer, BigDecimal, BigDecimal>> customerSales = this.dal.sql().select(
					SALES.DEMOGRAPHICS_GENDER.as(CUSTOMER_GENDER),
					DSL.count(SALES.ID).as(CUSTOMER_VISITS),
					DSL.sum(SALES.SALE_AMOUNT).as(CUSTOMER_PURCHASE_AMOUNT),
					DSL.sum(SALES.CONTRIBUTION_AMOUNT).as(CUSTOMER_CONTRIBUTION_AMOUNT))
				.from(this.tables)
				.where(condition)
				.groupBy(SALES.CUSTOMER_ID)
				.asTable(CUSTOMER_SUMMARY_TABLE);

		Record7<Integer, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal> result =
				this.dal.sql().select(
					DSL.count(),
					DSL.sum(returningCustomer(customerSales)),
					DSL.sum(matchingGender(customerSales, Gender.MALE)),
					DSL.sum(matchingGender(customerSales, Gender.FEMALE)),
					DSL.sum(customerSales.field(CUSTOMER_VISITS, BigDecimal.class)),
					DSL.sum(customerSales.field(CUSTOMER_PURCHASE_AMOUNT, BigDecimal.class)),
					DSL.sum(customerSales.field(CUSTOMER_CONTRIBUTION_AMOUNT, BigDecimal.class)))
				.from(customerSales)
				.fetchOne();

		CustomerSummary customers = new CustomerSummary();
		customers.setTotalCustomers(result.value1());
		customers.setTotalReturning(integerSum(result.value2()));
		customers.setTotalMale(integerSum(result.value3()));
		customers.setTotalFemale(integerSum(result.value4()));
		customers.setTotalUnspecified(customers.getTotalCustomers() - customers.getTotalMale() - customers.getTotalFemale());

		SaleSummary summary = new SaleSummary();
		summary.setTotalSales(integerSum(result.value5()));
		summary.setTotalAmount(decimalSum(result.value6()));
		summary.setTotalContributed(decimalSum(result.value7()));
		summary.setCustomerSummary(customers);

		return summary;
	}

	private Field<Integer> matchingGender(Table<?> table, Gender gender) {
		return DSL.when(table.field(CUSTOMER_GENDER, Long.class).eq(gender.getId()), 1).otherwise(0);
	}

	private Field<Integer> returningCustomer(Table<?> table) {
		return DSL.when(table.field(CUSTOMER_VISITS, BigDecimal.class).gt(new BigDecimal(1)), 1).otherwise(0);
	}

	private Integer integerSum(BigDecimal sum) {
		return sum == null? 0 : sum.intValue();
	}

	private BigDecimal decimalSum(BigDecimal sum) {
		return sum == null? new BigDecimal(0) : sum;
	}
}
