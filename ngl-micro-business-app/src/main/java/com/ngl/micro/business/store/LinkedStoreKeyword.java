package com.ngl.micro.business.store;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreKeywordDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreKeyword;

/**
 * Repo for store keywords.
 *
 * @author Paco Mendes
 */
public class LinkedStoreKeyword {

	private JStoreKeywordDao storeKeywordDao;

	public LinkedStoreKeyword(Configuration config) {
		this.storeKeywordDao = new JStoreKeywordDao(config);
	}

	public void add(Long storeId, Set<String> keywords) {
		Set<JStoreKeyword> storeKeywords = new HashSet<>();
		for (String keyword : keywords) {
			storeKeywords.add(new JStoreKeyword(storeId, keyword.toLowerCase()));
		}
		storeKeywordDao.insert(storeKeywords);
	}

	public Set<String> list(Long storeId) {
		return map(storeKeywords(storeId));
	}

	private List<JStoreKeyword> storeKeywords(Long storeId) {
		return storeKeywordDao.fetchByJStoreId(storeId);
	}

	private Set<String> map(List<JStoreKeyword> storeKeywords) {
		return storeKeywords.stream()
				.map((keyword) -> keyword.getKeyword())
				.collect(toSet());
	}

	public void update(Long storeId, Set<String> keywords) {
		List<JStoreKeyword> currentKeywords = storeKeywords(storeId);
		if (map(currentKeywords).equals(keywords)) {
			return;
		}
		if(!currentKeywords.isEmpty()) {
			storeKeywordDao.delete(currentKeywords);
		}
		add(storeId, keywords);
	}

}