package com.ngl.micro.business.store;

import java.time.ZonedDateTime;

import com.ngl.micro.business.store.Store.StoreStatus;

public class StoreStatusHistory {

	private Long id;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;
	private StoreStatus status;
	private String statusReason;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public StoreStatus getStatus() {
		return this.status;
	}

	public void setStatus(StoreStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

}