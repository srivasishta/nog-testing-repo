package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_CATEGORIES;
import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JCategoryDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreCategoriesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreCategories;
import com.ngl.micro.shared.contracts.business.TradeCategoryType;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedStoreCategory {

	private DAL support;
	private JCategoryDao categoryDao;
	private JStoreCategoriesDao storeCategoryDao;

	public LinkedStoreCategory(Configuration config) {
		this.support = new DAL(config);
		this.categoryDao = new JCategoryDao(config);
		this.storeCategoryDao = new JStoreCategoriesDao(config);
	}

	public void add(Long storeId, Set<TradeCategory> tradeCategories) {
		Set<JStoreCategories> categories = new HashSet<>();
		for (TradeCategory tradeCategory : tradeCategories) {
			JStoreCategories category = new JStoreCategories();
			category.setStoreId(storeId);
			category.setCategoryId(TradeCategoryType.idFrom(tradeCategory));
			categories.add(category);
		}
		this.storeCategoryDao.insert(categories);
	}

	public Set<TradeCategory> list(Long storeId) {
		return this.categoryDao.fetchByJId(this.storeCategoryIds(storeId)).stream()
				.map(c -> TradeCategory.valueOf(c.getValue()))
				.collect(toSet());
	}

	private Long[] storeCategoryIds(Long storeId) {
		List<JStoreCategories> storeCategories = this.storeCategoryDao.fetchByJStoreId(storeId);
		Long[] categoryIds = new Long[storeCategories.size()];
		int index = 0;
		for (JStoreCategories category : storeCategories) {
			categoryIds[index++] = category.getCategoryId();
		}
		return categoryIds;
	}

	public void update(Long storeId, Set<TradeCategory> tradeCategories) {
		Set<TradeCategory> currentCategories = this.list(storeId);
		if (currentCategories.equals(tradeCategories)) {
			return;
		}
		if (!currentCategories.isEmpty()) {
			this.deleteAll(storeId);
		}
		this.add(storeId, tradeCategories);
	}

	private void deleteAll(Long storeId) {
		this.support.sql()
				.delete(STORE_CATEGORIES)
				.where(STORE_CATEGORIES.STORE_ID.eq(storeId))
				.execute();
	}

}