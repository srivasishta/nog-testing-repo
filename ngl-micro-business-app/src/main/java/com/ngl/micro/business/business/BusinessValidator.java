package com.ngl.micro.business.business;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.EmailDef;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;
import org.hibernate.validator.cfg.defs.URLDef;

import com.ngl.micro.business.shared.ValueTypeValidationMappings;
import com.ngl.middleware.rest.server.validation.ResourceValidator;
import com.ngl.middleware.rest.server.validation.constraint.UniqueKeySetDef;

public class BusinessValidator extends ResourceValidator<Business> {

	public static BusinessValidator newInstance() {
		return ResourceValidator.configurator(new BusinessValidator()).configure();
	}

	private BusinessValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Business.class)
			.property("partnerId", FIELD)
				.constraint(new NotNullDef())
			.property("status", FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("name", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(60))
			.property("description", FIELD)
				.constraint(new SizeDef().min(0).max(255))
			.property("emailAddress", FIELD)
				.constraint(new EmailDef())
			.property("website", FIELD)
				.constraint(new URLDef())
			.property("phoneNumbers", FIELD)
				.constraint(new NotNullDef())
				.valid()
			.property("keywords", FIELD)
				.constraint(new UniqueKeySetDef().min(1).max(50))
			.property("categories", FIELD)
				.constraint(new NotNullDef());

		ValueTypeValidationMappings.configure(mapping);
	}
}
