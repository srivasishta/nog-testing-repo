package com.ngl.micro.business.revision;

import java.io.Serializable;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.springframework.util.Assert;

import com.ngl.middleware.rest.api.Identifiable;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.util.Beta;

@Beta(description =  "To be moved into middleware.")
public class VersionControlledEndpointDelegate<R extends Identifiable<RID>, RID extends Serializable> {

	private VersionControlService<R, RID> vc;

	public VersionControlledEndpointDelegate(VersionControlService<R, RID> vc) {
		Assert.notNull(vc);
		this.vc = vc;
	}

	public Response getRevisions(RID resourceId, PageRequest pageRequest) {
		Page<Revision> page = this.vc.getRevisions(resourceId, pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	public Response getLatestRevision(RID resourceId) {
		Revision revision = this.vc.getLatestRevision(resourceId);

		Resource resource = HAL.resource(revision)
				.build();

		return Response.ok(resource).build();
	}

	public Response getRevision(RID resourceId, UUID revisionId) {
		Revision revision = this.vc.getRevision(resourceId, revisionId);

		Resource resource = HAL.resource(revision)
				.build();

		return Response.ok(resource).build();
	}

}
