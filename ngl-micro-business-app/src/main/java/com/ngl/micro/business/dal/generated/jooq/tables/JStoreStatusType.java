/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables;


import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.dal.generated.jooq.Keys;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JStoreStatusTypeRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JStoreStatusType extends TableImpl<JStoreStatusTypeRecord> {

	private static final long serialVersionUID = -586074185;

	/**
	 * The reference instance of <code>ngl_micro_business.store_status_type</code>
	 */
	public static final JStoreStatusType STORE_STATUS_TYPE = new JStoreStatusType();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JStoreStatusTypeRecord> getRecordType() {
		return JStoreStatusTypeRecord.class;
	}

	/**
	 * The column <code>ngl_micro_business.store_status_type.id</code>.
	 */
	public final TableField<JStoreStatusTypeRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_business.store_status_type.value</code>.
	 */
	public final TableField<JStoreStatusTypeRecord, String> VALUE = createField("value", org.jooq.impl.SQLDataType.VARCHAR.length(8).nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_business.store_status_type</code> table reference
	 */
	public JStoreStatusType() {
		this("store_status_type", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_business.store_status_type</code> table reference
	 */
	public JStoreStatusType(String alias) {
		this(alias, STORE_STATUS_TYPE);
	}

	private JStoreStatusType(String alias, Table<JStoreStatusTypeRecord> aliased) {
		this(alias, aliased, null);
	}

	private JStoreStatusType(String alias, Table<JStoreStatusTypeRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroBusiness.NGL_MICRO_BUSINESS, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JStoreStatusTypeRecord> getPrimaryKey() {
		return Keys.KEY_STORE_STATUS_TYPE_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JStoreStatusTypeRecord>> getKeys() {
		return Arrays.<UniqueKey<JStoreStatusTypeRecord>>asList(Keys.KEY_STORE_STATUS_TYPE_PRIMARY, Keys.KEY_STORE_STATUS_TYPE_VALUE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JStoreStatusType as(String alias) {
		return new JStoreStatusType(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JStoreStatusType rename(String name) {
		return new JStoreStatusType(name, null);
	}
}
