/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables.records;


import com.ngl.micro.business.dal.generated.jooq.tables.JStoreCategories;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JStoreCategoriesRecord extends UpdatableRecordImpl<JStoreCategoriesRecord> implements Record2<Long, Long> {

	private static final long serialVersionUID = 507871948;

	/**
	 * Setter for <code>ngl_micro_business.store_categories.store_id</code>.
	 */
	public void setStoreId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_business.store_categories.store_id</code>.
	 */
	public Long getStoreId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_business.store_categories.category_id</code>.
	 */
	public void setCategoryId(Long value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_business.store_categories.category_id</code>.
	 */
	public Long getCategoryId() {
		return (Long) getValue(1);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record2<Long, Long> key() {
		return (Record2) super.key();
	}

	// -------------------------------------------------------------------------
	// Record2 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, Long> fieldsRow() {
		return (Row2) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row2<Long, Long> valuesRow() {
		return (Row2) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JStoreCategories.STORE_CATEGORIES.STORE_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field2() {
		return JStoreCategories.STORE_CATEGORIES.CATEGORY_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getStoreId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value2() {
		return getCategoryId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JStoreCategoriesRecord value1(Long value) {
		setStoreId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JStoreCategoriesRecord value2(Long value) {
		setCategoryId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JStoreCategoriesRecord values(Long value1, Long value2) {
		value1(value1);
		value2(value2);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JStoreCategoriesRecord
	 */
	public JStoreCategoriesRecord() {
		super(JStoreCategories.STORE_CATEGORIES);
	}

	/**
	 * Create a detached, initialised JStoreCategoriesRecord
	 */
	public JStoreCategoriesRecord(Long storeId, Long categoryId) {
		super(JStoreCategories.STORE_CATEGORIES);

		setValue(0, storeId);
		setValue(1, categoryId);
	}
}
