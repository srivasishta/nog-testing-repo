package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS;
import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_CATEGORIES;
import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_KEYWORD;
import static com.ngl.micro.business.dal.generated.jooq.Tables.CATEGORY;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_ADDRESS;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_CATEGORIES;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_CONTRIBUTION_RATE;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_KEYWORD;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_STATUS_HISTORY;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_STATUS_TYPE;
import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static java.util.stream.Collectors.toSet;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.LinkedBusinessCategory;
import com.ngl.micro.business.business.LinkedBusinessKeyword;
import com.ngl.micro.business.dal.generated.jooq.tables.JCategory;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStore;
import com.ngl.micro.business.dal.generated.jooq.tables.records.JStoreRecord;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQueries;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.SearchQuery.Condition;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.Time;

/**
 * Root repository for {@link Store} resources.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class StoreRepository implements PagingRepository<Store, UUID> {

	private static final String STORE_PK = "store_pk";
	private static final String BUSINESS_PK = "business_pk";

	private static final JCategory STORE_CATEGORY_ALIAS = CATEGORY.as("store_category");
	private static final JCategory BUSINESS_CATEGORY_ALIAS = CATEGORY.as("business_category");

	private DAL support;
	private JStoreDao storeDao;
	private JooqQuery query;
	private FieldMap<String> basicSearchFieldMap;
	private FieldMap<String> advancedSearchFieldMap;
	private TableOnConditionStep<?> fromClause;

	private LinkedStoreAddress address;
	private LinkedStorePhoneNumber phoneNumbers;
	private LinkedExternalIdentifier externalIdentifiers;
	private LinkedTradingHours tradingHours;
	private LinkedStoreKeyword storeKeywords;
	private LinkedBusinessCategory businessCategories;
	private LinkedBusinessKeyword businessKeywords;
	private LinkedStoreCategory storeCategories;
	private LinkedContributionRate contributionRate;
	private StoreStatusHistoryRepository status;

	public StoreRepository(Configuration config) {
		this.support = new DAL(config);
		this.storeDao = new JStoreDao(config);

		this.status = new StoreStatusHistoryRepository(config);
		this.phoneNumbers = new LinkedStorePhoneNumber(config);
		this.address = new LinkedStoreAddress(config);
		this.tradingHours = new LinkedTradingHours(config);
		this.storeKeywords = new LinkedStoreKeyword(config);
		this.businessCategories = new LinkedBusinessCategory(config);
		this.businessKeywords = new LinkedBusinessKeyword(config);
		this.storeCategories = new LinkedStoreCategory(config);
		this.externalIdentifiers = new LinkedExternalIdentifier(config);
		this.contributionRate = new LinkedContributionRate(config);

		this.basicSearchFieldMap = new FieldMap<String>()
				.bind(Store.Fields.ID, new MappedField(STORE.RESOURCE_ID).searchable(true))
				.bind(BUSINESS_PK, BUSINESS.ID.as(BUSINESS_PK))
				.bind(Store.Fields.LOGO_IMAGES_ORIGINAL, BUSINESS.LOGO_ORIGINAL)
				.bind(Store.Fields.LOGO_IMAGES_THUMBNAIL, BUSINESS.LOGO_SMALL)
				.bind(Store.Fields.LOGO_IMAGES_MEDIUM, BUSINESS.LOGO_MEDIUM)
				.bind(Store.Fields.LOGO_IMAGES_LARGE, BUSINESS.LOGO_LARGE)
				.bind(Store.Fields.BUSINESS_ID, new MappedField(STORE.BUSINESS_ID).searchable(true))
				.bind(Store.Fields.NAME, new MappedField(STORE.NAME).searchable(true).sortable(true))
				.bind(Store.Fields.DESCRIPTION, new MappedField(STORE.DESCRIPTION).searchable(true))
				.bind(Store.Fields.STATUS, new MappedField(STORE_STATUS_TYPE.VALUE).searchable(true))
				.bind(Store.Fields.STATUS_REASON, new MappedField(STORE_STATUS_HISTORY.STATUS_REASON).searchable(true))
				.bind(Store.Fields.WEBSITE, new MappedField(STORE.WEBSITE))
				.bind(Store.Fields.EMAIL_ADDRESS, new MappedField(STORE.EMAIL_ADDRESS))
				.bind(Store.Fields.GEO_LOCATION_LAT, STORE.LAT)
				.bind(Store.Fields.GEO_LOCATION_LON, STORE.LON)
				.bind(Store.Fields.TIMEZONE, new MappedField(STORE.TIMEZONE).searchable(true))
				.bind(Store.Fields.ADDRESS_STREET_ADDRESS, new MappedField(STORE_ADDRESS.STREET_ADDRESS).searchable(true))
				.bind(Store.Fields.ADDRESS_LOCALITY, new MappedField(STORE_ADDRESS.LOCALITY).searchable(true))
				.bind(Store.Fields.ADDRESS_REGION, new MappedField(STORE_ADDRESS.REGION).searchable(true))
				.bind(Store.Fields.ADDRESS_POSTAL_CODE, new MappedField(STORE_ADDRESS.POSTAL_CODE).searchable(true))
				.bind(Store.Fields.ADDRESS_COUNTRY, new MappedField(STORE_ADDRESS.COUNTRY).searchable(true))
				.bind(Store.Fields.CONTRIBUTION_RATE, new MappedField(STORE_CONTRIBUTION_RATE.RATE).searchable(true).sortable(true))
				.bind(STORE_PK, STORE.ID);

		this.advancedSearchFieldMap = new FieldMap<>();
		this.advancedSearchFieldMap
				.bind(Store.Fields.STORE_CATEGORIES, new MappedField(STORE_CATEGORY_ALIAS.VALUE).as("store_category").searchable(true))
				.bind(Store.Fields.BUSINESS_CATEGORIES, new MappedField(BUSINESS_CATEGORY_ALIAS.VALUE).as("business_category").searchable(true))
				.bind(Store.Fields.STORE_KEYWORDS, new MappedField(STORE_KEYWORD.KEYWORD).as("store_keyword").searchable(true))
				.bind(Store.Fields.BUSINESS_KEYWORDS, new MappedField(BUSINESS_KEYWORD.KEYWORD).as("business_keyword").searchable(true))
				.bindAll(this.basicSearchFieldMap);

		this.fromClause = STORE
				.join(BUSINESS).on(STORE.BUSINESS_ID.eq(BUSINESS.RESOURCE_ID))
				.join(STORE_ADDRESS).on(STORE.ID.eq(STORE_ADDRESS.STORE_ID))
				.join(STORE_STATUS_HISTORY).on(STORE.ID.eq(STORE_STATUS_HISTORY.STORE_ID))
				.join(STORE_STATUS_TYPE).on(STORE_STATUS_HISTORY.STATUS_ID.eq(STORE_STATUS_TYPE.ID)
						.and(STORE_STATUS_HISTORY.END_DATE.isNull()))
				.join(STORE_CONTRIBUTION_RATE).on(STORE.ID.eq(STORE_CONTRIBUTION_RATE.STORE_ID)
						.and(STORE_CONTRIBUTION_RATE.END_DATE.isNull()));

		this.query = JooqQuery.builder(this.support, this.basicSearchFieldMap)
					.from(this.fromClause)
					.build();
	}

	@Override
	public Store add(Store store) {
		JStoreRecord storeRecord = this.support.sql().newRecord(STORE);
		storeRecord.setName(store.getName());
		storeRecord.setResourceId(store.getId());
		storeRecord.setDescription(store.getDescription());
		storeRecord.setBusinessId(store.getBusinessId());
		storeRecord.setEmailAddress(store.getEmailAddress());
		storeRecord.setWebsite(store.getWebsite());
		storeRecord.setTimezone(store.getTimezone().getId());
		storeRecord.setLat(store.getGeoLocation().getLat());
		storeRecord.setLon(store.getGeoLocation().getLon());
		storeRecord.store();

		Long storeId = storeRecord.getId();
		ZonedDateTime updatedDate = Time.utcNow();

		this.phoneNumbers.add(storeId, store.getPhoneNumbers());
		this.address.add(storeId, store.getAddress());
		this.tradingHours.add(storeId, store.getTradingHours());
		this.storeKeywords.add(storeId, store.getStoreKeywords());
		this.storeCategories.add(storeId, store.getStoreCategories());
		this.contributionRate.set(storeId, store.getContributionRate(), updatedDate);
		this.status.setStoreStatus(storeId, store.getStatus(), store.getStatusReason(), updatedDate);

		return this.get(store.getId());
	}

	@Override
	public Store get(UUID id) {
		Record record = this.query.getRecordWhere(STORE.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			Long pk = record.getValue(STORE.ID);
			Store store = this.mapRecord(record);
			store.setAddress(this.address.map(record));
			store.setTradingHours(this.tradingHours.list(pk));
			store.setPhoneNumbers(this.phoneNumbers.list(pk));
			store.setBusinessCategories(this.businessCategories.list(record.getValue(BUSINESS_PK, Long.class)));
			store.setStoreCategories(this.storeCategories.list(pk));
			store.setBusinessKeywords(this.businessKeywords.list(record.getValue(BUSINESS_PK, Long.class)));
			store.setStoreKeywords(this.storeKeywords.list(pk));

			ZonedDateTime lookupDate = Time.utcNow();
			store.setExternalIdentifiers(this.externalIdentifiers.getByStoreAt(pk, lookupDate));
			return store;
		}
	}

	@Override
	public Page<Store> get(PageRequest req) {
		List<Condition> conditions = req.getSearchQuery().getConditions();
		boolean qByCategoriesAndKeywords = this.searchByCategoriesAndKeywords(conditions);
		boolean qByDistance = this.searchByDistance(conditions);

		FieldMap<String> fieldMap;
		TableOnConditionStep<?> from;

		if (qByCategoriesAndKeywords) {
			fieldMap = this.advancedSearchFieldMap;
			from = this.fromClause
					.leftOuterJoin(STORE_CATEGORIES).on(STORE.ID.eq(STORE_CATEGORIES.STORE_ID))
					.leftOuterJoin(BUSINESS_CATEGORIES).on(BUSINESS.ID.eq(BUSINESS_CATEGORIES.BUSINESS_ID))
					.leftOuterJoin(STORE_CATEGORY_ALIAS).on(STORE_CATEGORIES.CATEGORY_ID.eq(STORE_CATEGORY_ALIAS.ID))
					.leftOuterJoin(BUSINESS_CATEGORY_ALIAS).on(BUSINESS_CATEGORIES.CATEGORY_ID.eq(BUSINESS_CATEGORY_ALIAS.ID))
					.leftOuterJoin(STORE_KEYWORD).on(STORE.ID.eq(STORE_KEYWORD.STORE_ID))
					.leftOuterJoin(BUSINESS_KEYWORD).on(BUSINESS.ID.eq(BUSINESS_KEYWORD.BUSINESS_ID));
		}
		else {
			fieldMap = this.basicSearchFieldMap;
			from = this.fromClause;
		}

		if (qByDistance) {
			List<Condition> cleaned = this.removeDistanceFields(conditions);
			req = PageRequest.with(req.getPageNumber(), req.getPageSize())
					.fields(req.getFields())
					.sort(req.getSort())
					.search(new SearchQuery().addConditions(cleaned))
					.build();
		}

		TableOnConditionStep<?> scoped = from;
		return this.query.getPage(
					req,
					() -> fieldMap,
					() -> scoped,
					q -> {
						if (qByDistance) {
							q.addConditions(JooqQueries.coordinatesBoundary(
									com.ngl.middleware.util.geo.GeoLocation.EARTH_RADIUS_IN_KM,
									this.getAsDouble(Store.LocationFields.RADIUS, conditions),
									com.ngl.middleware.util.geo.GeoLocation.fromRadians(
											this.getAsDouble(Store.LocationFields.LAT, conditions),
											this.getAsDouble(Store.LocationFields.LON, conditions)),
									STORE.LAT, STORE.LON));
						}
						if (qByCategoriesAndKeywords) {
							q.addGroupBy(STORE.ID);
						}
					},
					this::mapRecord);
	}

	private boolean searchByCategoriesAndKeywords(List<Condition> conditions) {
		return conditions.stream()
				.map(c -> c.getProperty())
				.anyMatch(p -> p.equals(Store.Fields.BUSINESS_CATEGORIES)
						|| p.equals(Store.Fields.BUSINESS_KEYWORDS)
						|| p.equals(Store.Fields.STORE_CATEGORIES)
						|| p.equals(Store.Fields.STORE_KEYWORDS));
	}

	private boolean searchByDistance(List<Condition> conditions) {
		return conditions.stream()
				.map(c -> c.getProperty())
				.filter(p -> p.equals(Store.LocationFields.LAT)
						|| p.equals(Store.LocationFields.LON)
						|| p.equals(Store.LocationFields.RADIUS))
				.collect(Collectors.counting()) > 0;
	}

	private List<Condition> removeDistanceFields(List<Condition> conditions) {
		return conditions.stream()
				.filter(c -> !c.getProperty().equals(Store.LocationFields.LAT)
						&& !c.getProperty().equals(Store.LocationFields.LON)
						&& !c.getProperty().equals(Store.LocationFields.RADIUS))
				.collect(Collectors.toList());
	}

	private double getAsDouble(String field, List<Condition> conditions) {
		String value = conditions.stream()
				.filter(c -> c.getProperty().equals(field))
				.findFirst()
				.get().getValue();
		// Trim '
		value = value.substring(1, value.length() - 1);
		return Double.valueOf(value);
	}

	private Store mapRecord(Record record) {
		Store store = new Store();
		store.setId(record.getValue(STORE.RESOURCE_ID));
		store.setBusinessId(record.getValue(STORE.BUSINESS_ID));
		store.setName(record.getValue(STORE.NAME));
		store.setDescription(record.getValue(STORE.DESCRIPTION));
		store.setAddress(this.address.map(record));
		store.setTimezone(ZoneId.of(record.getValue(STORE.TIMEZONE)));
		store.setWebsite(record.getValue(STORE.WEBSITE));
		store.setEmailAddress(record.getValue(STORE.EMAIL_ADDRESS));
		store.setStatus(StoreStatus.valueOf(record.getValue(STORE_STATUS_TYPE.VALUE)));
		store.setStatusReason(record.getValue(STORE_STATUS_HISTORY.STATUS_REASON));
		store.setContributionRate(record.getValue(STORE_CONTRIBUTION_RATE.RATE));
		Double lat = record.getValue(STORE.LAT);
		Double lon = record.getValue(STORE.LON);
		store.setGeoLocation(GeoLocation.of(lat, lon));
		String logoOriginal = record.getValue(BUSINESS.LOGO_ORIGINAL);
		String logoSmall = record.getValue(BUSINESS.LOGO_SMALL);
		String logoMedium = record.getValue(BUSINESS.LOGO_MEDIUM);
		String logoLarge = record.getValue(BUSINESS.LOGO_LARGE);
		if (!Strings.isNullOrWhitespace(logoOriginal)
				|| !Strings.isNullOrWhitespace(logoSmall)
				|| !Strings.isNullOrWhitespace(logoMedium)
				|| !Strings.isNullOrWhitespace(logoLarge)) {
			ImageSet logos = new ImageSet();
			logos.setOriginal(logoOriginal);
			logos.setThumbnail(logoSmall);
			logos.setLarge(logoLarge);
			logos.setMedium(logoMedium);
			store.setLogoImages(logos);
		}
		return store;
	}

	@Override
	public void update(Store store) {
		JStore persisted = this.storeDao.fetchOneByJResourceId(store.getId());
		this.update(persisted, store);
		this.storeDao.update(persisted);

		ZonedDateTime updatedDate = Time.utcNow();
		this.address.update(persisted.getId(), store.getAddress());
		this.phoneNumbers.update(persisted.getId(), store.getPhoneNumbers());
		this.tradingHours.update(persisted.getId(), store.getTradingHours());
		this.updateStoreKeywords(persisted.getId(), store.getStoreKeywords());
		this.updateStoreCategories(persisted.getId(), store.getStoreCategories());
		this.contributionRate.set(persisted.getId(), store.getContributionRate(), updatedDate);
		this.status.setStoreStatus(persisted.getId(), store.getStatus(), store.getStatusReason(), updatedDate);
	}

	public void addExternalIdentifier(Long storeId, ExternalIdentifier identifier) {
		this.externalIdentifiers.add(storeId, identifier);
	}

	public Set<ExternalIdentifier> getExternalIdentifiers(String identifier) {
		return this.externalIdentifiers.get(identifier);
	}

	public void updateExternalIdentifier(Long storeId, ExternalIdentifier identifier) {
		this.externalIdentifiers.update(storeId, identifier);
	}

	private void update(JStore persisted, Store store) {
		if(!areEqual(persisted.getName(), store.getName())) {
			persisted.setName(store.getName());
		}
		if(!areEqual(persisted.getDescription(), store.getDescription())) {
			persisted.setDescription(store.getDescription());
		}
		if(!areEqual(persisted.getEmailAddress(), store.getEmailAddress())) {
			persisted.setEmailAddress(store.getEmailAddress());
		}
		if(!areEqual(persisted.getWebsite(), store.getWebsite())) {
			persisted.setWebsite(store.getWebsite());
		}
		if(!areEqual(persisted.getLat(), store.getGeoLocation().getLat())) {
			persisted.setLat(store.getGeoLocation().getLat());
		}
		if(!areEqual(persisted.getLon(), store.getGeoLocation().getLon())) {
			persisted.setLon(store.getGeoLocation().getLon());
		}
		if(!areEqual(persisted.getTimezone(), store.getTimezone())) {
			persisted.setTimezone(store.getTimezone().getId());
		}
	}

	@Override
	public long size() {
		return this.storeDao.count();
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException();
	}

	public Set<Long> getBusinessStoreIds(UUID businessId) {
		return businessId == null ? new HashSet<>() : this.support.sql()
				.select(STORE.ID)
				.from(STORE)
				.where(STORE.BUSINESS_ID.eq(businessId))
				.fetch()
				.stream()
				.map(r -> r.value1())
				.collect(toSet());
	}

	public BigDecimal getContributionRateAt(Long storeId, ZonedDateTime effectiveDate) {
		return this.contributionRate.getAt(storeId, effectiveDate);
	}

	public Set<TradeCategory> getStoreCategories(Long storeId) {
		return this.storeCategories.list(storeId);
	}

	public void updateStoreCategories(Long storeId, Set<TradeCategory> categories) {
		this.storeCategories.update(storeId, categories);
	}

	public Set<String> getStoreKeywords(Long storeId) {
		return this.storeKeywords.list(storeId);
	}

	public void updateStoreKeywords(Long storeId, Set<String> keywords) {
		this.storeKeywords.update(storeId, keywords);
	}

	public void updateStoreStatus(Long storeId, StoreStatus status, String reason, ZonedDateTime updatedDate) {
		this.status.setStoreStatus(storeId, status, reason, updatedDate);
	}

	public Long getInternalId(UUID id) {
		Record1<Long> record = this.support.sql().select(STORE.ID)
				.from(STORE)
				.where(STORE.RESOURCE_ID.eq(id))
				.fetchOne();
		return record == null ? null : record.value1();
	}

}
