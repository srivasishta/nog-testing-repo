package com.ngl.micro.business.business;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.revision.VersionControlService;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.business.store.StoreRepository;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The Business service.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
@Transactional
public class BusinessService {

	private BusinessRepository businesses;
	private StoreRepository stores;
	private VersionControlService<Business, UUID> businessVc;

	private BusinessValidator validator;

	private Merge merge;

	public BusinessService(BusinessRepository businesses,
							StoreRepository stores,
							BusinessValidator validator,
							VersionControlService<Business, UUID> businessVc) {
		this.businesses = businesses;
		this.stores = stores;
		this.validator = validator;
		this.merge = new Merge();
		this.businessVc = businessVc;
	}

	public Business createBusiness(Business business) {
		ZonedDateTime createdDate = Time.utcNow();
		UUID partnerId = Subjects.getSubject();
		// If it is not a system account, set the partner ID to
		// that in the token. If it is a system account and no
		// ID is specified, then set it to the system account's ID.
		// Else if the system is creating the membership on behalf
		// of a partner, leave the ID unchanged.
		if(!ApiPolicy.isSystemAccount(partnerId) || business.getPartnerId() == null) {
			business.setPartnerId(partnerId);
		}
		this.validator.validate(business);
		business.setId(UUIDS.type4Uuid());

		Business createdBusiness = this.businesses.add(business);
		this.businessVc.revise(
				createdDate,
				new Revision(),
				new Business(),
				createdBusiness,
				Subjects.getClientId(),
				Subjects.getSubject());

		return createdBusiness;
	}

	public Business getBusinessById(UUID id) {
		Business business = this.businesses.get(id);
		if (business == null) {
			throw ApplicationError.resourceNotFound().asException("No business found for the ID: " + id);
		}
		return business;
	}

	public Page<Business> getBusinesses(PageRequest pageRequest) {
		return this.businesses.get(pageRequest);
	}

	public void updateBusiness(UUID id, Patch patch) {
		ZonedDateTime updatedDate = Time.utcNow();
		Business base = this.getBusinessById(id);
		Business working = this.getBusinessById(id);

		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isAccessibleByPartner(partnerId, working.getPartnerId())) {
			throw ApplicationError.forbidden()
					.setTitle("Not Authorized")
					.setDetail("Not authorized to update business with ID: " + id)
					.asException();
		}

		BusinessStatus beforeStatus = working.getStatus();
		UUID beforePartnerId = working.getPartnerId();
		ImageSet beforeImages = this.beforeImages(working.getLogoImages());

		this.merge.merge(patch, working);
		working.setId(id);
		working.setPartnerId(beforePartnerId);
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			working.setLogoImages(beforeImages);
		}
		this.validator.validate(working);

		this.businesses.update(working);

		Set<Long> storeIds = this.stores.getBusinessStoreIds(working.getId());
		this.removeStoreCategories(storeIds, working.getCategories());
		this.removeStoreKeywords(storeIds, working.getKeywords());
		if (this.deactivated(beforeStatus, working.getStatus())) {
			this.deactivateStores(storeIds, working.getStatusReason());
		}

		Business updated = this.getBusinessById(id);

		this.businessVc.revise(
				updatedDate,
				new Revision(),
				base,
				updated,
				Subjects.getClientId(),
				Subjects.getSubject());
	}

	private ImageSet beforeImages(ImageSet logoImages) {
		if (logoImages == null) {
			return null;
		}
		ImageSet copy = new ImageSet();
		copy.setOriginal(logoImages.getOriginal());
		copy.setThumbnail(logoImages.getThumbnail());
		copy.setMedium(logoImages.getMedium());
		copy.setLarge(logoImages.getLarge());
		return copy;
	}

	private boolean deactivated(BusinessStatus before, BusinessStatus after) {
		return (before == BusinessStatus.ACTIVE) && (after == BusinessStatus.INACTIVE);
	}

	private void deactivateStores(Set<Long> storeIds, String reason) {
		ZonedDateTime updatedDate = Time.utcNow();
		for (Long storeId : storeIds) {
			this.stores.updateStoreStatus(storeId, StoreStatus.INACTIVE, reason, updatedDate);
		}
	}

	private void removeStoreCategories(Set<Long> storeIds, Set<TradeCategory> categories) {
		for (Long storeId : storeIds) {
			Set<TradeCategory> storeCategories = this.stores.getStoreCategories(storeId);
			if (storeCategories.removeAll(categories)) {
				this.stores.updateStoreCategories(storeId, storeCategories);
			}
		}
	}

	private void removeStoreKeywords(Set<Long> storeIds, Set<String> keywords) {
		for (Long storeId : storeIds) {
			Set<String> storeKeywords = this.stores.getStoreKeywords(storeId);
			if (storeKeywords.removeAll(keywords)) {
				this.stores.updateStoreKeywords(storeId, storeKeywords);
			}
		}
	}

}
