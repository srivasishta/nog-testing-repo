package com.ngl.micro.business.business;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_CATEGORIES;
import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jooq.Configuration;

import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessCategoriesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JCategoryDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessCategories;
import com.ngl.micro.shared.contracts.business.TradeCategoryType;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

public class LinkedBusinessCategory {

	private DAL support;
	private JCategoryDao categoryDao;
	private JBusinessCategoriesDao businessCategoryDao;

	public LinkedBusinessCategory(Configuration config) {
		this.support = new DAL(config);
		this.categoryDao = new JCategoryDao(config);
		this.businessCategoryDao = new JBusinessCategoriesDao(config);
	}

	public void add(Long businessId, Set<TradeCategory> tradeCategories) {
		Set<JBusinessCategories> categories = new HashSet<>();
		for (TradeCategory category : tradeCategories) {
			JBusinessCategories businessCategory = new JBusinessCategories();
			businessCategory.setBusinessId(businessId);
			businessCategory.setCategoryId(TradeCategoryType.forEnum(category).getId());
			categories.add(businessCategory);
		}
		this.businessCategoryDao.insert(categories);
	}

	public Set<TradeCategory> list(Long businessId) {
		return this.categoryDao.fetchByJId(this.businessCategoryIds(businessId)).stream()
			.map(c -> TradeCategory.valueOf(c.getValue()))
			.collect(toSet());
	}

	private Long[] businessCategoryIds(Long businessId) {
		List<JBusinessCategories> businessCategories = this.businessCategoryDao.fetchByJBusinessId(businessId);
		Long[] categoryIds = new Long[businessCategories.size()];
		int index = 0;
		for (JBusinessCategories category : businessCategories) {
			categoryIds[index++] = category.getCategoryId();
		}
		return categoryIds;
	}

	public void update(Long businessId, Set<TradeCategory> tradeCategories) {
		Set<TradeCategory> currentCategories = this.list(businessId);
		if (currentCategories.equals(tradeCategories)) {
			return;
		}
		if (!currentCategories.isEmpty()) {
			this.deleteAll(businessId);
		}
		this.add(businessId, tradeCategories);
	}

	private void deleteAll(Long businessId) {
		this.support.sql()
				.delete(BUSINESS_CATEGORIES)
				.where(BUSINESS_CATEGORIES.BUSINESS_ID.eq(businessId))
				.execute();
	}

}
