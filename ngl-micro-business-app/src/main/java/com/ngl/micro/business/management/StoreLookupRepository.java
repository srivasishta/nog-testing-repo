package com.ngl.micro.business.management;

import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS;
import static com.ngl.micro.business.dal.generated.jooq.Tables.BUSINESS_STATUS_HISTORY;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_CONTRIBUTION_RATE;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_IDENTIFIER;
import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_STATUS_HISTORY;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.Condition;
import org.jooq.Configuration;
import org.jooq.Field;
import org.jooq.Record5;

import com.ngl.micro.business.store.Store;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;

/**
 * Repository for managing {@link Store} resources.
 *
 * @author Paco Mendes
 */
public class StoreLookupRepository {

	private DAL support;

	public StoreLookupRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public MatchedStore getByExternalIdAt(String externalId, ZonedDateTime date) {

		Record5<UUID, UUID, Long, Long, BigDecimal> record = this.support.sql().select(
				STORE.RESOURCE_ID,
				BUSINESS.RESOURCE_ID,
				STORE_STATUS_HISTORY.STATUS_ID,
				BUSINESS_STATUS_HISTORY.STATUS_ID,
				STORE_CONTRIBUTION_RATE.RATE)
			.from(STORE
				.join(STORE_STATUS_HISTORY).on(STORE.ID.eq(STORE_STATUS_HISTORY.STORE_ID))
				.join(STORE_IDENTIFIER).on(STORE.ID.eq(STORE_IDENTIFIER.STORE_ID))
				.join(STORE_CONTRIBUTION_RATE).on(STORE.ID.eq(STORE_CONTRIBUTION_RATE.STORE_ID))
				.join(BUSINESS).on(STORE.BUSINESS_ID.eq(BUSINESS.RESOURCE_ID))
				.join(BUSINESS_STATUS_HISTORY).on(BUSINESS.ID.eq(BUSINESS_STATUS_HISTORY.BUSINESS_ID)))
			.where(STORE_IDENTIFIER.EXTERNAL_ID.eq(externalId))
			.and(inRange(STORE_IDENTIFIER.START_DATE, STORE_IDENTIFIER.END_DATE, date))
			.and(inRange(STORE_STATUS_HISTORY.START_DATE, STORE_STATUS_HISTORY.END_DATE, date))
			.and(inRange(BUSINESS_STATUS_HISTORY.START_DATE, BUSINESS_STATUS_HISTORY.END_DATE, date))
			.and(inRange(STORE_CONTRIBUTION_RATE.START_DATE, STORE_CONTRIBUTION_RATE.END_DATE, date))
			.fetchOne();

		if (record == null) {
			return null;
		} else {
			MatchedStore store = new MatchedStore();
			store.setBusinessId(record.getValue(BUSINESS.RESOURCE_ID));
			store.setBusinessStatus(ResourceStatus.forId(record.getValue(BUSINESS_STATUS_HISTORY.STATUS_ID)));
			store.setStoreId(record.getValue(STORE.RESOURCE_ID));
			store.setStoreStatus(ResourceStatus.forId(record.getValue(STORE_STATUS_HISTORY.STATUS_ID)));
			store.setStoreContributionRate(record.getValue(STORE_CONTRIBUTION_RATE.RATE));
			return store;
		}
	}

	private static Condition inRange(Field<ZonedDateTime> lower, Field<ZonedDateTime> upper, ZonedDateTime date) {
		return lower.lessOrEqual(date)
				.and(upper.greaterThan(date).or(upper.isNull()));
	}

}
