/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.business.dal.generated.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JStoreCategories implements Serializable {

	private static final long serialVersionUID = -1763348183;

	private Long storeId;
	private Long categoryId;

	public JStoreCategories() {}

	public JStoreCategories(JStoreCategories value) {
		this.storeId = value.storeId;
		this.categoryId = value.categoryId;
	}

	public JStoreCategories(
		Long storeId,
		Long categoryId
	) {
		this.storeId = storeId;
		this.categoryId = categoryId;
	}

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("JStoreCategories (");

		sb.append(storeId);
		sb.append(", ").append(categoryId);

		sb.append(")");
		return sb.toString();
	}
}
