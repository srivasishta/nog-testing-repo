package com.ngl.micro.business.event.in.contribution;

import javax.transaction.Transactional;

import com.ngl.micro.business.sale.SaleService;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;

/**
 * Listens for {@link ContributionRecordedEvent}s.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackOn = Exception.class)
public class ContributionRecordedEventListener implements MessageListener<ContributionRecordedEvent> {

	private ContributionRecordedEventRepository events;
	private SaleService sales;

	public ContributionRecordedEventListener(ContributionRecordedEventRepository eventRepository, SaleService sales) {
		this.events = eventRepository;
		this.sales = sales;
	}

	@Override
	public void onMessage(ContributionRecordedEvent event) {
		this.sales.recordSale(event.getDocument());
		this.events.addEvent(event);
	}

}
