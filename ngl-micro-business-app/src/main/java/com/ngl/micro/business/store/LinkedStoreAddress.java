package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_ADDRESS;
import static com.ngl.middleware.util.EqualsUtils.areEqual;

import org.jooq.Configuration;
import org.jooq.Record;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreAddressDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreAddress;

/**
 * DAO to manage a store address. Currently stores only support a single physical address.
 */
public class LinkedStoreAddress {

	private JStoreAddressDao storeAddressDao;

	public LinkedStoreAddress(Configuration config) {
		this.storeAddressDao = new JStoreAddressDao(config);
	}

	public void add(Long storeId, Address address) {
		JStoreAddress storeAddress = new JStoreAddress();
		storeAddress.setStoreId(storeId);
		storeAddress.setStreetAddress(address.getStreetAddress());
		storeAddress.setLocality(address.getLocality());
		storeAddress.setRegion(address.getRegion());
		storeAddress.setPostalCode(address.getPostalCode());
		storeAddress.setCountry(address.getCountry().name());
		storeAddressDao.insert(storeAddress);
	}

	public Address get(Long storeId) {
		JStoreAddress storeAddress = storeAddressDao.fetchOneByJStoreId(storeId);
		return (storeAddress != null) ? map(storeAddress) : null;
	}

	private Address map(JStoreAddress storeAddress) {
		Address address = new Address();
		address.setId(storeAddress.getId());
		address.setStreetAddress(storeAddress.getStreetAddress());
		address.setLocality(storeAddress.getLocality());
		address.setPostalCode(storeAddress.getPostalCode());
		address.setRegion(storeAddress.getRegion());
		address.setCountry(Country.valueOf(storeAddress.getCountry()));
		return address;
	}

	public Address map(Record record) {
		Address address = new Address();
		address.setId(record.getValue(STORE_ADDRESS.ID));
		address.setStreetAddress(record.getValue(STORE_ADDRESS.STREET_ADDRESS));
		address.setLocality(record.getValue(STORE_ADDRESS.LOCALITY));
		address.setRegion(record.getValue(STORE_ADDRESS.REGION));
		address.setPostalCode(record.getValue(STORE_ADDRESS.POSTAL_CODE));
		address.setCountry(Country.valueOf(record.getValue(STORE_ADDRESS.COUNTRY)));
		return address;
	}

	public void update(Long storeId, Address address) {
		JStoreAddress persisted = storeAddressDao.fetchOneByJStoreId(storeId);
		update(persisted, address);
		storeAddressDao.update(persisted);
	}

	private void update(JStoreAddress persisted, Address address) {

		if (!areEqual(persisted.getCountry(), address.getCountry())) {
			persisted.setCountry(address.getCountry().name());
		}
		if(!areEqual(persisted.getLocality(), address.getLocality())) {
			persisted.setLocality(address.getLocality());
		}
		if(!areEqual(persisted.getPostalCode(), address.getPostalCode())) {
			persisted.setPostalCode(address.getPostalCode());
		}
		if(!areEqual(persisted.getRegion(), address.getRegion())) {
			persisted.setRegion(address.getRegion());
		}
		if(!areEqual(persisted.getStreetAddress(), address.getStreetAddress())) {
			persisted.setStreetAddress(address.getStreetAddress());
		}
	}
}