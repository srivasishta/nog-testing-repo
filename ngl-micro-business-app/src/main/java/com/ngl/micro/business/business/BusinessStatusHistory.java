package com.ngl.micro.business.business;

import java.time.ZonedDateTime;

import com.ngl.micro.business.business.Business.BusinessStatus;

public class BusinessStatusHistory {

	private Long id;
	private ZonedDateTime startDate;
	private ZonedDateTime endDate;
	private BusinessStatus status;
	private String statusReason;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public BusinessStatus getStatus() {
		return this.status;
	}

	public void setStatus(BusinessStatus status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String reason) {
		this.statusReason = reason;
	}

}