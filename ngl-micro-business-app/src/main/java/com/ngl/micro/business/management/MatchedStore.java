package com.ngl.micro.business.management;

import java.math.BigDecimal;
import java.util.UUID;

import com.ngl.micro.shared.contracts.ResourceStatus;

/**
 * A matched store lookup.
 *
 * @author Paco Mendes
 *
 */
public class MatchedStore {

	private UUID businessId;
	private ResourceStatus businessStatus;

	private UUID storeId;
	private ResourceStatus storeStatus;

	private BigDecimal storeContributionRate;

	public UUID getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(UUID businessId) {
		this.businessId = businessId;
	}

	public ResourceStatus getBusinessStatus() {
		return this.businessStatus;
	}

	public void setBusinessStatus(ResourceStatus businessStatus) {
		this.businessStatus = businessStatus;
	}

	public UUID getStoreId() {
		return this.storeId;
	}

	public void setStoreId(UUID storeId) {
		this.storeId = storeId;
	}

	public ResourceStatus getStoreStatus() {
		return this.storeStatus;
	}

	public void setStoreStatus(ResourceStatus storeStatus) {
		this.storeStatus = storeStatus;
	}

	public BigDecimal getStoreContributionRate() {
		return this.storeContributionRate;
	}

	public void setStoreContributionRate(BigDecimal contributionRate) {
		this.storeContributionRate = contributionRate;
	}

}
