package com.ngl.micro.business.store;

import static com.ngl.micro.business.dal.generated.jooq.Tables.STORE_STATUS_HISTORY;
import static java.util.stream.Collectors.toList;

import java.time.ZonedDateTime;
import java.util.List;

import org.jooq.Configuration;
import org.jooq.Result;

import com.ngl.micro.business.dal.generated.jooq.tables.records.JStoreStatusHistoryRecord;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.util.Experimental;

/**
 * Tracks store status history on updates.
 *
 * @author Paco Mendes
 */
public class StoreStatusHistoryRepository {

	private DAL support;

	public StoreStatusHistoryRepository(Configuration config) {
		this.support = new DAL(config);
	}

	public void setStoreStatus(Long storeId, StoreStatus status, String reason, ZonedDateTime updatedDate) {
		JStoreStatusHistoryRecord currentStatus = this.getStatusRecordAt(storeId, updatedDate);
		if (currentStatus == null) {
			this.addStatusRecord(storeId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(storeId, status, reason, updatedDate);
		}
	}

	private boolean updated(JStoreStatusHistoryRecord currentStatus, StoreStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void addStatusRecord(Long storeId, StoreStatus status, String reason, ZonedDateTime startDate) {
		JStoreStatusHistoryRecord record =  this.support.sql().newRecord(STORE_STATUS_HISTORY);
		record.setStoreId(storeId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(startDate);
		record.store();
	}

	private void expireStatusRecord(JStoreStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public StoreStatusHistory getByStoreIdAt(Long storeId, ZonedDateTime updatedDate) {
		JStoreStatusHistoryRecord record = this.getStatusRecordAt(storeId, updatedDate);
		return record == null ? null : this.map(record);
	}

	public JStoreStatusHistoryRecord getStatusRecordAt(Long storeId, ZonedDateTime updatedDate) {
		return this.support.sql()
				.selectFrom(STORE_STATUS_HISTORY)
				.where(STORE_STATUS_HISTORY.STORE_ID.eq(storeId))
				.and(STORE_STATUS_HISTORY.START_DATE.lessOrEqual(updatedDate))
				.and(STORE_STATUS_HISTORY.END_DATE.greaterThan(updatedDate)
						.or(STORE_STATUS_HISTORY.END_DATE.isNull()))
				.fetchOne();
	}

	@Experimental
	public List<StoreStatusHistory> list(Long storeId) {
		Result<JStoreStatusHistoryRecord> records = this.support.sql()
				.selectFrom(STORE_STATUS_HISTORY)
				.where(STORE_STATUS_HISTORY.STORE_ID.eq(storeId))
				.fetch();

		return records.stream().map(this::map).collect(toList());
	}

	private StoreStatusHistory map(JStoreStatusHistoryRecord record) {
		StoreStatusHistory status = new StoreStatusHistory();
		status.setId(record.getId());
		status.setStatus(StoreStatus.valueOf(ResourceStatus.forId(record.getStatusId()).name()));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}

}
