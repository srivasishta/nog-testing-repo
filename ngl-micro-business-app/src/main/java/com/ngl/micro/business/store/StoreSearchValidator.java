package com.ngl.micro.business.store;

import java.util.List;
import java.util.stream.Collectors;

import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.SearchQuery.Condition;

public class StoreSearchValidator {

	private static final String DETAIL_MESSAGE = String.format(
			"To perform location based search all search fields [%s, %s, %s] must be present"
			+ " exactly once, use the logical AND operator and the = comparisaon operator.",
			Store.LocationFields.LAT,
			Store.LocationFields.LON,
			Store.LocationFields.RADIUS);

	public void validate(SearchQuery searchQuery) {
		if (searchQuery == null) {
			return;
		}
		List<Condition> conditions = searchQuery.getConditions();

		List<Condition> location = conditions.stream()
				.filter(c -> {
					String p = c.getProperty();
					return Store.LocationFields.LAT.equals(p)
					|| Store.LocationFields.LON.equals(p)
					|| Store.LocationFields.RADIUS.equals(p);
				})
				.collect(Collectors.toList());

		if (location.isEmpty()) {
			return;
		}
		this.validatePresence(location);
		this.validatePosition(conditions, location);
	}

	private void validatePresence(List<Condition> location) {
		long lats = location.stream()
				.filter(c -> Store.LocationFields.LAT.equals(c.getProperty()))
				.collect(Collectors.counting());
		long longs = location.stream()
				.filter(c -> Store.LocationFields.LAT.equals(c.getProperty()))
				.collect(Collectors.counting());
		long radiuses = location.stream()
				.filter(c -> Store.LocationFields.LAT.equals(c.getProperty()))
				.collect(Collectors.counting());

		if (lats != 1 && longs != 1 && radiuses != 1) {
			this.throwInvalidSearchException();
		}
	}

	private void validatePosition(List<Condition> conditions, List<Condition> location) {
		int total = conditions.size();
		if (!location.contains(conditions.get(total - 1))
				|| !location.contains(conditions.get(total - 2))
				|| !location.contains(conditions.get(total - 3))) {
			this.throwInvalidSearchException();
		}
	}

	private void throwInvalidSearchException() {
		throw ApplicationError.validationError()
				.setTitle("Validation Error")
				.setCode(ApplicationErrorCode.VALIDATION_ERROR)
				.setDetail(DETAIL_MESSAGE)
				.asException();
	}

}
