package com.ngl.micro.business.management;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreLookupRepositoryITCase extends AbstractDatabaseITCase {

	private StoreLookupRepository stores;
	private TestBusinesses testBusinesses;
	private TestStores testStores;

	private static final UUID BUSINESS_ID = Data.ID_1;
	private static final UUID STORE_4 = Data.ID_4;
	private static final UUID STORE_5 = Data.ID_5;

	private static final String ID_A = "A";
	private static final String ID_B = "B";
	private static final String ID_C = "C";

	@Override
	public void before() {
		super.before();
		this.stores = new StoreLookupRepository(this.sqlContext);
		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
	}

	@Test
	public void test_unmatched_identifier() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		assertEquals(null,     this.stores.getByExternalIdAt("non existing", Data.T_0));
	}

	@Test
	public void test_match_by_identifier_at_different_points_in_time() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		MatchedStore matchBefore = this.stores.getByExternalIdAt(ID_A, Data.T_MINUS_5M);
		assertEquals(BUSINESS_ID, matchBefore.getBusinessId());
		assertEquals(STORE_4, matchBefore.getStoreId());
		assertEquals(ResourceStatus.ACTIVE, matchBefore.getBusinessStatus());
		assertEquals(ResourceStatus.ACTIVE, matchBefore.getStoreStatus());
		assertEquals(new BigDecimal("0.20000"), matchBefore.getStoreContributionRate());

		MatchedStore matchAfter = this.stores.getByExternalIdAt(ID_A, Data.T_MINUS_1M);
		assertEquals(BUSINESS_ID, matchAfter.getBusinessId());
		assertEquals(STORE_4, matchAfter.getStoreId());
		assertEquals(ResourceStatus.INACTIVE,matchAfter.getBusinessStatus());
		assertEquals(ResourceStatus.INACTIVE,matchAfter.getStoreStatus());
		assertEquals(new BigDecimal("0.00000"),matchAfter.getStoreContributionRate());
	}

	@Test
	public void test_single_identifier_for_store() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		assertEquals(null,     this.stores.getByExternalIdAt(ID_A, Data.T_MINUS_10M.minusSeconds(1)));
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_A, Data.T_MINUS_10M).getStoreId());
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_A, Data.T_0).getStoreId());
	}

	@Test
	public void test_identifier_with_gap() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		assertEquals(null,     this.stores.getByExternalIdAt(ID_B, Data.T_MINUS_1y));
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_B, Data.T_MINUS_10M).getStoreId());
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_B, Data.T_MINUS_5M.minusSeconds(1)).getStoreId());
		assertEquals(null,     this.stores.getByExternalIdAt(ID_B, Data.T_MINUS_5M));
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_B, Data.T_MINUS_1M).getStoreId());
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_B, Data.T_0).getStoreId());
	}

	@Test
	public void test_shared_identifier_between_stores() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		assertEquals(null,     this.stores.getByExternalIdAt(ID_C, Data.T_MINUS_1y));
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_C, Data.T_MINUS_5M).getStoreId());
		assertEquals(STORE_4, this.stores.getByExternalIdAt(ID_C, Data.T_0.minusMonths(3)).getStoreId());
		assertEquals(STORE_5, this.stores.getByExternalIdAt(ID_C, Data.T_MINUS_1M).getStoreId());
		assertEquals(STORE_5, this.stores.getByExternalIdAt(ID_C, Data.T_0).getStoreId());
	}

}
