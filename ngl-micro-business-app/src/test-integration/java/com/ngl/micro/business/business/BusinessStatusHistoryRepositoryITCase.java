package com.ngl.micro.business.business;
import static com.ngl.micro.business.business.Business.BusinessStatus.ACTIVE;
import static com.ngl.micro.business.business.Business.BusinessStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class BusinessStatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private BusinessStatusHistoryRepository repo;
	private TestBusinesses businesses;
	private TestStores stores;

	@Before
	public void setup() {
		this.repo = new BusinessStatusHistoryRepository(this.sqlContext);
		this.businesses = new TestBusinesses();
		this.stores = new TestStores();
	}

	@Test
	public void test_get_business_status() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		Long businessId = TestBusinesses.BUSINESS_PK;
		assertEquals(null, this.repo.getByBusinessIdAt(businessId, Data.T_MINUS_1y));
		assertEquals(ACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_MINUS_10M).getStatus());
		assertEquals(ACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_MINUS_1M.minusSeconds(1)).getStatus());
		assertEquals(INACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_MINUS_1M).getStatus());
		assertEquals(INACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_MINUS_1s).getStatus());
		assertEquals(ACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_0).getStatus());
		assertEquals(ACTIVE, this.repo.getByBusinessIdAt(businessId, Data.T_PLUS_5y).getStatus());
	}

	@Test
	public void test_get_null_status() {
		assertEquals(null, this.repo.getByBusinessIdAt(TestBusinesses.BUSINESS_PK, Data.T_0));
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		Long businessId = TestBusinesses.BUSINESS_PK;

		BusinessStatusHistory status = this.repo.getByBusinessIdAt(businessId, Data.T_0);
		int records = this.repo.list(businessId).size();

		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setBusinessStatus(businessId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByBusinessIdAt(businessId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(records, this.repo.list(businessId).size());

		//Status reason update
		this.repo.setBusinessStatus(businessId, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByBusinessIdAt(businessId, ZonedDateTime.now());
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(businessId).size());

		//Status only
		this.repo.setBusinessStatus(businessId, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByBusinessIdAt(businessId, ZonedDateTime.now());
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(businessId).size());

		//Reason only
		this.repo.setBusinessStatus(businessId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByBusinessIdAt(businessId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(businessId).size());

		//Empty reason
		this.repo.setBusinessStatus(businessId, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getByBusinessIdAt(businessId, ZonedDateTime.now());
		assertEquals("", status.getStatusReason());
		assertEquals(++records, this.repo.list(businessId).size());
	}

}