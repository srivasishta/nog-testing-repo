package com.ngl.micro.business.store;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.revision.jooq.JVersionControlService;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStoreRevisions;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.Sort;
import com.ngl.middleware.rest.api.page.Sort.Direction;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.json.ObjectMapperFactory;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreServiceITCase extends AbstractDatabaseITCase {

	private static ObjectMapper mapper = new ObjectMapperFactory().getInstance();

	private StoreService service;
	private StoreRepository stores;
	private JVersionControlService<Store> storeVc;
	private TestBusinesses testBusinesses;
	private TestStores testStores;
	private TestStoreRevisions testRevisions;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Override
	public void before() {
		super.before();
		this.stores = new StoreRepository(this.sqlContext);
		this.storeVc= new JVersionControlService<>(
				mapper,
				JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(),
				"store_revision",
				new DAL(this.sqlContext));
		this.service = new StoreService(null, this.stores, this.storeVc, StoreValidator.newInstance());
		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
		this.testRevisions = new TestStoreRevisions();
	}

	@Test(expected = ApplicationException.class)
	public void test_list_stores_filter_by_distance_missing_fields() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		SearchQuery query = SearchQueries.q(Store.LocationFields.RADIUS).eq(5)
				.getSearchQuery();
		PageRequest page = PageRequest.with(0, 10).search(query).build();
		this.service.getStores(page);
	}

	@Test
	public void test_list_store_revisions() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);
		this.cmdLoadTestDataSet(this.testRevisions);

		Page<Revision> revisions = this.storeVc.getRevisions(TestStores.STORE_4_ID, PageRequest.with(0, 10).build());
		Assert.assertEquals(3, revisions.getNumberOfItems());
	}

	@Test
	public void test_get_unknown_revision() {
		this.thrown.expectStatus(ApplicationStatus.NOT_FOUND);
		this.storeVc.getRevision(Data.ID_1, Data.ID_1);
	}

	@Test
	public void test_get_revision() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);
		this.cmdLoadTestDataSet(this.testRevisions);

		Page<Revision> revisions = this.storeVc.getRevisions(TestStores.STORE_4_ID, PageRequest.with(0, 10).build());

		Revision revision = this.storeVc.getRevision(TestStores.STORE_4_ID, revisions.getItems().get(1).getId());
		Assert.assertNotNull(revision);
	}

	@Test
	public void test_get_contribution_rate_history() {

		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);
		this.cmdLoadTestDataSet(this.testRevisions);

		UUID storeId = TestStores.STORE_4_ID;

		Store currentStore = this.stores.get(storeId);
		Sort sort = new Sort();
		sort.add(Revision.Fields.REVISED_DATE, Direction.ASC);

		Page<Revision> storeRevisions = this.storeVc.getRevisions(storeId, PageRequest.with(0, 100).sort(sort).build());

		Assert.assertEquals(3, storeRevisions.getTotalItems());

		Merge merge = new Merge();
		Store store = new Store();

		Map<ZonedDateTime, BigDecimal> contributionRateHistory = new TreeMap<>();

		BigDecimal rate = null;
		for (Revision rev : storeRevisions.getItems()) {
			merge.merge(rev.getAppliedPatch(), store);
			if (this.updated(rate, store.getContributionRate())) {
				rate = store.getContributionRate();
				contributionRateHistory.put(rev.getRevisedDate(), rate);
			}
		}

		Assert.assertEquals(currentStore, store);
		Assert.assertEquals(3, contributionRateHistory.size());
	}

	private boolean updated(BigDecimal base, BigDecimal other) {
		return base == null || !base.equals(other);
	}

}
