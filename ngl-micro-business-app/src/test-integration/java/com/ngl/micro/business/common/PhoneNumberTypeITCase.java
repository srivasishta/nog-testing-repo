package com.ngl.micro.business.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JPhoneNumberTypeDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JPhoneNumberType;
import com.ngl.micro.business.shared.LinkedPhoneNumberType;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class PhoneNumberTypeITCase extends AbstractDatabaseITCase {

	private JPhoneNumberTypeDao typeDao;

	@Override
	public void before() {
		super.before();
		this.typeDao = new JPhoneNumberTypeDao(this.sqlContext);
	}

	@Test
	public void test_number_type_mapping() throws Exception {
		JPhoneNumberType type;
		assertEquals("Expect type count.", LinkedPhoneNumberType.values().length, this.typeDao.count());
		for (LinkedPhoneNumberType number : LinkedPhoneNumberType.values()) {
			type = this.typeDao.fetchOneByJValue(number.name());
			assertEquals("Expect type name equals persisted name", number.name(), type.getValue());
			assertEquals("Expect type id equals persisted id: ", number.getId(), type.getId().longValue());
		}
	}
}
