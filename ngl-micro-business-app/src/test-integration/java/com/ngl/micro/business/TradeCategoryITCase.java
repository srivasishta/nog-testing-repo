package com.ngl.micro.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JCategoryDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JCategory;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.shared.contracts.business.TradeCategoryType;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class TradeCategoryITCase extends AbstractDatabaseITCase {

	private JCategoryDao categoryDao;

	@Override
	public void before() {
		super.before();
		this.cmdLoadTestDataSet(new TestBusinesses());
		this.categoryDao = new JCategoryDao(this.sqlContext);
	}

	@Test
	public void test_valid_categories_against_db() throws Exception {
		JCategory category;
		assertEquals("Expect equal category type count.", TradeCategoryType.values().length, this.categoryDao.count());
		for (TradeCategoryType type : TradeCategoryType.values()) {
			category = this.categoryDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), category.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), category.getId().longValue());
		}
	}

	@Test
	public void test_internal_categories_against_external() {
		assertEquals("Expect equal category type count.", TradeCategoryType.values().length, TradeCategory.values().length);
		for (TradeCategory value : TradeCategory.values()) {
			// Throws no such element exception if on failure.
			TradeCategoryType.forEnum(value);
		}
	}
}
