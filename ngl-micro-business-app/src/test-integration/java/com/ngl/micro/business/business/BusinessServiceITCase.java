package com.ngl.micro.business.business;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.revision.jooq.JVersionControlService;
import com.ngl.micro.business.store.StoreRepository;
import com.ngl.micro.business.test_data.TestAccessTokens;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.json.ObjectMapperFactory;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class BusinessServiceITCase extends AbstractDatabaseITCase {

	private static ObjectMapper mapper = new ObjectMapperFactory().getInstance();

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private BusinessService businessService;
	private BusinessRepository businessRepo;
	private StoreRepository storeRepo;
	private TestBusinesses businesses;
	private JVersionControlService<Business> businessVc;

	@Override
	public void before() {
		super.before();
		TestSubjects.login(TestAccessTokens.SYSTEM);
		this.businessRepo = new BusinessRepository(this.sqlContext);
		this.storeRepo = new StoreRepository(this.sqlContext);
		this.businesses = new TestBusinesses();
		this.businessVc= new JVersionControlService<>(
				mapper,
				JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(),
				"store_revision",
				new DAL(this.sqlContext));
		this.businessService = new BusinessService(this.businessRepo, this.storeRepo, BusinessValidator.newInstance(), this.businessVc);
	}

	@Test
	public void test_create_business_with_duplicate_name() {
		this.businessService.createBusiness(this.businesses.business());
		this.businessService.createBusiness(this.businesses.business());
	}

	@Test
	public void test_update_business_with_duplicate_name() {
		Business existing = this.businessService.createBusiness(this.businesses.business());
		Business business = this.businesses.business();
		business.setName("new name");
		business = this.businessService.createBusiness(business);

		Business reference = this.businessService.getBusinessById(business.getId());

		business.setName(existing.getName().toLowerCase());
		Patch patch = new Diff().diff(business, reference);

		this.businessService.updateBusiness(business.getId(), patch);
	}

	@Test
	public void test_update_business_logo_as_system() {
		Business base = this.businessService.createBusiness(this.businesses.business());
		Business working = this.businessService.getBusinessById(base.getId());

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.businessService.updateBusiness(base.getId(), patch);
		Business patched = this.businessService.getBusinessById(base.getId());

		Assert.assertThat(patched, CoreMatchers.is(working));
	}

	@Test
	public void test_update_business_logo_as_partner() {
		TestSubjects.login(TestAccessTokens.full());
		Business base = this.businessService.createBusiness(this.businesses.business());
		Business working = this.businessService.getBusinessById(base.getId());

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.businessService.updateBusiness(base.getId(), patch);
		Business patched = this.businessService.getBusinessById(base.getId());

		Assert.assertThat(patched, CoreMatchers.is(base));
	}

}
