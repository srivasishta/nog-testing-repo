package com.ngl.micro.business.store;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreStatusTypeDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreStatusType;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreStatusTypeITCase extends AbstractDatabaseITCase {

	private JStoreStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JStoreStatusTypeDao(this.sqlContext);
	}

	@Test
	public void test_store_status_mapping() throws Exception {
		assertEquals(ResourceStatus.ACTIVE.name(), StoreStatus.ACTIVE.name());
		assertEquals(ResourceStatus.INACTIVE.name(), StoreStatus.INACTIVE.name());
		JStoreStatusType status;
		assertEquals("Expect equal status type count.", ResourceStatus.values().length, this.statusDao.count());
		for (ResourceStatus type : ResourceStatus.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
