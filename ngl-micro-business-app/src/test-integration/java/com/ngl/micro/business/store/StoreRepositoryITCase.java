package com.ngl.micro.business.store;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.SearchQuery.ComparisonOperator;
import com.ngl.middleware.rest.api.page.SearchQuery.LogicalOperator;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreRepositoryITCase extends AbstractDatabaseITCase {

	private StoreRepository stores;
	private TestBusinesses testBusinesses;
	private TestStores testStores;

	@Override
	public void before() {
		super.before();
		this.stores = new StoreRepository(this.sqlContext);
		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
	}

	@Test
	public void test_create_new_store() throws JsonProcessingException {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.testStores.store();
		Store created = this.stores.add(this.testStores.store());
		assertEquals(store.getTradingHours().size(), created.getTradingHours().size());
		store.setTradingHours(created.getTradingHours());
		// Add the inherited business categories.
		store.setBusinessCategories(Collections.asSet(TradeCategory.DINING__SPORTS_BARS));
		Assert.assertThat(created.getBusinessKeywords().size(), CoreMatchers.is(1));
		store.setBusinessKeywords(created.getBusinessKeywords());
		// Remove categories already on business.
//		store.getStoreCategories().remove(TradeCategory.DINING__SPORTS_BARS);
		assertEquals(store, created);
	}

	@Test
	public void test_get_store() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store1 = this.stores.add(this.testStores.store());
		Store store2 = this.stores.get(store1.getId());
		assertEquals(store1, store2);
	}

	@Test
	public void test_get_missing_store() {
		Store store = this.stores.get(Data.toUUID(1L));
		assertEquals(null, store);
	}

	@Test
	public void test_list_stores() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		PageRequest page = PageRequest.with(1, 3).build();
		Page<Store> pagedStores = this.stores.get(page);
		assertEquals(3, pagedStores.getNumberOfItems());
		assertEquals(2, pagedStores.getTotalPages());
		assertEquals(6, pagedStores.getTotalItems());
	}

	@Test
	public void test_list_stores_filter_by_category() {
		try {
			this.cmdLoadTestDataSet(this.testBusinesses);
			this.cmdLoadTestDataSet(this.testStores);

			SearchQuery query = SearchQueries.q(Store.Fields.STORE_CATEGORIES).eq(TradeCategory.DINING__CASUAL_DINING)
					.or(Store.Fields.BUSINESS_CATEGORIES).eq(TradeCategory.DINING__CASUAL_DINING)
					.getSearchQuery();
			PageRequest page = PageRequest.with(0, 10).search(query).build();
			Page<Store> pagedStores = this.stores.get(page);
			assertEquals(3, pagedStores.getNumberOfItems());
			assertEquals(1, pagedStores.getTotalPages());
			assertEquals(3, pagedStores.getTotalItems());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void test_list_stores_filter_by_categories() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		SearchQuery query = SearchQueries.q(Store.Fields.STORE_CATEGORIES).in(TradeCategory.DINING__CASUAL_DINING, TradeCategory.DINING__QUICK_SERVICE).getSearchQuery();
		PageRequest page = PageRequest.with(0, 10).search(query).build();
		Page<Store> pagedStores = this.stores.get(page);
		for (Store store : pagedStores.getItems()) {
			Set<TradeCategory> categories = this.stores.get(store.getId()).getStoreCategories();
			Assert.assertTrue(categories.contains(TradeCategory.DINING__CASUAL_DINING) || categories.contains(TradeCategory.DINING__QUICK_SERVICE));
		}

	}

	@Test
	public void test_list_stores_filter_by_keyword() {
		try {
			this.cmdLoadTestDataSet(this.testBusinesses);
			this.cmdLoadTestDataSet(this.testStores);

			SearchQuery query = SearchQueries.q(Store.Fields.STORE_KEYWORDS).eq(Data.STRING_1)
					.or(Store.Fields.BUSINESS_KEYWORDS).eq(Data.STRING_1)
					.getSearchQuery();
			PageRequest page = PageRequest.with(0, 10).search(query).build();
			Page<Store> pagedStores = this.stores.get(page);
			assertEquals(3, pagedStores.getNumberOfItems());
			assertEquals(1, pagedStores.getTotalPages());
			assertEquals(3, pagedStores.getTotalItems());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void test_list_stores_filter_by_category_and_keyword() {
		try {
			this.cmdLoadTestDataSet(this.testBusinesses);
			this.cmdLoadTestDataSet(this.testStores);

			SearchQuery query = SearchQueries.q(Store.Fields.STORE_CATEGORIES).eq(TradeCategory.APPAREL__GENERAL)
					.or(Store.Fields.BUSINESS_KEYWORDS).eq(Data.STRING_1)
					.getSearchQuery();
			PageRequest page = PageRequest.with(0, 10).search(query).build();
			Page<Store> pagedStores = this.stores.get(page);
			assertEquals(4, pagedStores.getNumberOfItems());
			assertEquals(1, pagedStores.getTotalPages());
			assertEquals(4, pagedStores.getTotalItems());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void test_list_stores_filter_by_distance() {
		try {
			this.cmdLoadTestDataSet(this.testBusinesses);
			this.cmdLoadTestDataSet(this.testStores);

			SearchQuery query = SearchQueries.q(Store.LocationFields.LAT).eq(TestStores.TIMES_SQUARE_LAT_RAD)
					.and(Store.LocationFields.LON).eq(TestStores.TIMES_SQUARE_LON_RAD)
					.and(Store.LocationFields.RADIUS).eq(5)
					.getSearchQuery();
			PageRequest page = PageRequest.with(0, 10).search(query).build();
			Page<Store> pagedStores = this.stores.get(page);
			assertEquals(3, pagedStores.getNumberOfItems());
			assertEquals(1, pagedStores.getTotalPages());
			assertEquals(3, pagedStores.getTotalItems());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void test_list_stores_filter_by_distance_and_category() {
		try {
			this.cmdLoadTestDataSet(this.testBusinesses);
			this.cmdLoadTestDataSet(this.testStores);

			SearchQuery query = SearchQueries.q(Store.LocationFields.LAT).eq(TestStores.TIMES_SQUARE_LAT_RAD)
					.and(Store.LocationFields.LON).eq(TestStores.TIMES_SQUARE_LON_RAD)
					.and(Store.LocationFields.RADIUS).eq(7)
					.and(Store.Fields.STORE_CATEGORIES).eq(TradeCategory.APPAREL__GENERAL)
					.getSearchQuery();
			PageRequest page = PageRequest.with(0, 10).search(query).build();
			Page<Store> pagedStores = this.stores.get(page);
			assertEquals(1, pagedStores.getNumberOfItems());
			assertEquals(1, pagedStores.getTotalPages());
			assertEquals(1, pagedStores.getTotalItems());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void test_list_stores_for_business() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		SearchQuery query = new SearchQuery();
		query.addConditions(new SearchQuery.Condition(LogicalOperator.AND, "businessId", ComparisonOperator.EQ, "'00000000-0000-0000-0000-000000000001'"));
		PageRequest page = PageRequest.with(0, 10).search(query).build();
		Page<Store> pagedStores = this.stores.get(page);
		assertEquals(3, pagedStores.getTotalItems());
		assertEquals(3, pagedStores.getItems().size());
		assertEquals(Data.ID_1, pagedStores.getItems().get(0).getBusinessId());
		assertEquals(Data.ID_1, pagedStores.getItems().get(1).getBusinessId());
		assertEquals(Data.ID_1, pagedStores.getItems().get(2).getBusinessId());
	}

	@Test
	public void test_list_business_stores() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		Set<Long> businessStores = this.stores.getBusinessStoreIds(Data.ID_1);
		assertTrue(businessStores.size() > 0);

		assertTrue(this.stores.getBusinessStoreIds(Data.toUUID(100L)).isEmpty());
	}

	@Test
	public void test_update_store_profile() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.stores.add(this.testStores.store());
		Store updated = this.testStores.updatedStore(store);
		this.stores.update(updated);
		Store persisted = this.stores.get(store.getId());
		assertEquals(updated.getName(), persisted.getName());
		assertEquals(updated.getDescription(), persisted.getDescription());
		assertEquals(updated.getStatus(), persisted.getStatus());
		assertEquals(updated.getAddress(), persisted.getAddress());
		assertEquals(updated.getGeoLocation(), persisted.getGeoLocation());
		assertEquals(updated.getEmailAddress(), persisted.getEmailAddress());
		assertEquals(updated.getWebsite(), persisted.getWebsite());
		assertEquals(updated.getPhoneNumbers(), persisted.getPhoneNumbers());
	}

	@Test
	public void test_update_store_categories() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.stores.add(this.testStores.store());
		store.setStoreCategories(this.testStores.updatedTradeCategories());
		this.stores.update(store);
		Store persisted = this.stores.get(store.getId());
		assertEquals(store.getStoreCategories(), persisted.getStoreCategories());
	}

	@Test
	public void test_update_keywords() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.stores.add(this.testStores.store());
		store.setStoreKeywords(this.testStores.updatedKeywords());
		this.stores.update(store);
		Store persisted = this.stores.get(store.getId());
		assertEquals(store.getStoreKeywords(), persisted.getStoreKeywords());
	}

	@Test
	public void test_update_trading_hours() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.stores.add(this.testStores.store());
		store.setTradingHours(this.testStores.updatedTradingHours());
		this.stores.update(store);
		Store persisted = this.stores.get(store.getId());
		assertEquals(store.getTradingHours().size(), persisted.getTradingHours().size());
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.testStores.store();
		store.setStatus(StoreStatus.ACTIVE);
		store.setStatusReason("created");
		store = this.stores.add(store);
		assertEquals(StoreStatus.ACTIVE, store.getStatus());
		assertEquals("created", store.getStatusReason());

		store.setStatus(StoreStatus.INACTIVE);
		store.setStatusReason("suspended");
		this.stores.update(store);
		Store persisted = this.stores.get(store.getId());
		assertEquals(StoreStatus.INACTIVE, persisted.getStatus());
		assertEquals("suspended", persisted.getStatusReason());
	}

	@Test
	public void test_manage_store_identifiers() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		Store store = this.stores.add(this.testStores.store());
		UUID storeId = store.getId();
		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setKey("123ABC");
		identifier.setStartDate(Data.T_MINUS_10M);
		identifier.setEndDate(null);
		Long internalId = this.stores.getInternalId(store.getId());
		this.stores.addExternalIdentifier(internalId, identifier);

		store = this.stores.get(storeId);
		assertEquals(1, store.getExternalIdentifiers().size());

		ZonedDateTime updatedDate = Data.T_PLUS_1H;
		identifier = store.getExternalIdentifiers().stream().findFirst().get();
		identifier.setEndDate(updatedDate);
		this.stores.updateExternalIdentifier(internalId, identifier);

		store = this.stores.get(storeId);
		identifier = store.getExternalIdentifiers().stream().findFirst().get();
		assertEquals(updatedDate, identifier.getEndDate());
	}

}
