package com.ngl.micro.business.store;
import static com.ngl.micro.business.store.Store.StoreStatus.ACTIVE;
import static com.ngl.micro.business.store.Store.StoreStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreStatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private StoreStatusHistoryRepository repo;
	private TestBusinesses businesses;
	private TestStores stores;

	@Before
	public void setup() {
		this.repo = new StoreStatusHistoryRepository(this.sqlContext);
		this.businesses = new TestBusinesses();
		this.stores = new TestStores();
	}

	@Test
	public void test_get_store_status() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		Long storeId = TestStores.STORE_4_PK;
		assertEquals(null, this.repo.getByStoreIdAt(storeId, Data.T_MINUS_1y));
		assertEquals(ACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_MINUS_10M).getStatus());
		assertEquals(ACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_MINUS_1M.minusSeconds(1)).getStatus());
		assertEquals(INACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_MINUS_1M).getStatus());
		assertEquals(INACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_MINUS_1s).getStatus());
		assertEquals(ACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_0).getStatus());
		assertEquals(ACTIVE, this.repo.getByStoreIdAt(storeId, Data.T_PLUS_5y).getStatus());
	}

	@Test
	public void test_get_null_status() {
		assertEquals(null, this.repo.getByStoreIdAt(TestStores.STORE_4_PK, Data.T_0));
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		Long storeId = TestStores.STORE_4_PK;

		StoreStatusHistory status = this.repo.getByStoreIdAt(storeId, Data.T_0);
		int records = this.repo.list(storeId).size();

		assertEquals(ACTIVE, status.getStatus());
		assertEquals(Data.REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setStoreStatus(storeId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByStoreIdAt(storeId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(records, this.repo.list(storeId).size());

		//Status reason update
		this.repo.setStoreStatus(storeId, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByStoreIdAt(storeId, ZonedDateTime.now());
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(storeId).size());

		//Status only
		this.repo.setStoreStatus(storeId, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByStoreIdAt(storeId, ZonedDateTime.now());
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(storeId).size());

		//Reason only
		this.repo.setStoreStatus(storeId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByStoreIdAt(storeId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(storeId).size());

		//Empty reason
		this.repo.setStoreStatus(storeId, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getByStoreIdAt(storeId, ZonedDateTime.now());
		assertEquals("", status.getStatusReason());
		assertEquals(++records, this.repo.list(storeId).size());
	}

}