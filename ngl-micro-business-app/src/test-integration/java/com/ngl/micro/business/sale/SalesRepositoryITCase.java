package com.ngl.micro.business.sale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestSales;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class SalesRepositoryITCase extends AbstractDatabaseITCase {

	private static final UUID BUSINESS_ID = Data.ID_1;

	private SaleRepository repo;
	private TestBusinesses businesses;
	private TestStores stores;
	private TestSales sales;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new SaleRepository(this.sqlContext);
		this.businesses = new TestBusinesses();
		this.stores = new TestStores();
		this.sales = new TestSales();
	}

	@Test
	public void test_load_sales(){
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);
	}

	@Test
	public void test_record_sale() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		InternalSale sale = new InternalSale();
		sale.setId(Data.ID_3);
		sale.setStoreId(Data.ID_4);
		sale.setCustomerId(Data.ID_6);
		sale.setSaleAmount(new BigDecimal("26.99"));
		sale.setSaleDate(Data.T_0.minusDays(3));
		sale.setContributionAmount(new BigDecimal("2.699"));
		sale.setDemographics(new SaleDemographics(Gender.MALE.name(), null));

		Sale created = this.repo.addSale(TestBusinesses.BUSINESS_ID, sale);
		assertEquals(sale, created);
	}

	@Test
	public void test_get_sale() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		Long storeId = Data.toLong(TestStores.STORE_4_ID);

		Sale sale = this.repo.getById(Data.ID_1).get();
		assertEquals(new BigDecimal("4.57100"), sale.getContributionAmount());
		assertEquals(new BigDecimal("45.71000"), sale.getSaleAmount());
		assertEquals(Data.ID_1, sale.getId());
		assertEquals(Data.T_0.minusDays(13).toLocalDate(), sale.getSaleDate().toLocalDate());
		assertEquals(Data.toUUID(storeId), sale.getStoreId());
	}

	@Test
	public void test_list_store_sales() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		SearchQuery query = SearchQueries.q(Sale.Fields.STORE_ID).eq(Data.ID_4).getSearchQuery();
		PageRequest page = PageRequest.with(0, 100).search(query).build();

		Page<Sale> storeSales = this.repo.getSalesByBusinessId(BUSINESS_ID, page);
		assertTrue(storeSales.getTotalItems() > 0);
		storeSales.getItems().stream()
			.forEach(s -> Assert.assertEquals(Data.ID_4, s.getStoreId()));
	}

	@Test
	public void test_filter_sales_in_date_range() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		SearchQuery query = SearchQueries.q(Sale.Fields.SALE_DATE).gt(Data.T_MINUS_10d).and(Sale.Fields.SALE_DATE).lt(Data.T_0).getSearchQuery();
		PageRequest page = PageRequest.with(0, 100).search(query).build();

		Page<Sale> storeSales = this.repo.getSalesByBusinessId(BUSINESS_ID, page);
		assertTrue(storeSales.getTotalItems() > 0);
		storeSales.getItems().stream()
			.forEach(s -> Assert.assertTrue(s.getSaleDate().isAfter(Data.T_MINUS_10d) && s.getSaleDate().isBefore(Data.T_0)));
	}

	@Test
	public void test_get_sales() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		SaleSummary summary = this.repo.getSaleSummaryByBusinessId(BUSINESS_ID, new SearchQuery());
		Page<Sale> sales = this.repo.getSalesByBusinessId(BUSINESS_ID, PageRequest.with(0, 1000).build());

		BigDecimal totalSales = sales.getItems().stream().map(p -> p.getSaleAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal totalContributions = sales.getItems().stream().map(p -> p.getContributionAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);

		assertTrue(summary.getTotalSales() > 0);
		assertEquals(summary.getTotalSales().longValue(), sales.getTotalItems());
		assertEquals(summary.getTotalAmount(), totalSales);
		assertEquals(summary.getTotalContributed(), totalContributions);

		CustomerSummary customers = summary.getCustomerSummary();
		Integer totalCustomers = customers.getTotalFemale() +
				customers.getTotalMale() +
				customers.getTotalUnspecified();

		assertEquals(customers.getTotalCustomers(), totalCustomers);
	}

	@Test
	public void test_get_zero_filtered_sales() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		SearchQuery q = SearchQueries
				.q(Sale.Fields.DEMOGRAPHICS_GENDER).eq(Gender.MALE.name())
				.and(Sale.Fields.DEMOGRAPHICS_GENDER).eq(Gender.FEMALE.name())
				.getSearchQuery();

		SaleSummary summary = this.repo.getSaleSummaryByBusinessId(BUSINESS_ID, q);
		assertEquals(0, summary.getTotalSales().intValue());
		assertEquals(0, summary.getTotalAmount().intValue());
		assertEquals(0, summary.getTotalContributed().intValue());

		CustomerSummary customers = summary.getCustomerSummary();
		assertEquals(0, customers.getTotalCustomers().intValue());
		assertEquals(0, customers.getTotalReturning().intValue());
		assertEquals(0, customers.getTotalFemale().intValue());
		assertEquals(0, customers.getTotalMale().intValue());
		assertEquals(0, customers.getTotalUnspecified().intValue());

	}

	@Test
	public void test_get_filtered_sale_summary() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		SearchQuery q = SearchQueries
				.q(Sale.Fields.STORE_ID).eq(Data.ID_4)
				.and(Sale.Fields.SALE_DATE).gt(Data.T_MINUS_1M)
				.and(Sale.Fields.DEMOGRAPHICS_GENDER).eq(Gender.MALE.name())
				.getSearchQuery();

		SaleSummary summary = this.repo.getSaleSummaryByBusinessId(BUSINESS_ID, q);

		assertEquals(2, summary.getTotalSales().intValue());
		assertEquals(new BigDecimal("48.70000"), summary.getTotalAmount());
		assertEquals(new BigDecimal("4.87000"), summary.getTotalContributed());

		CustomerSummary customers = summary.getCustomerSummary();
		assertEquals(1, customers.getTotalCustomers().intValue());
		assertEquals(1, customers.getTotalReturning().intValue());
		assertEquals(0, customers.getTotalFemale().intValue());
		assertEquals(1, customers.getTotalMale().intValue());
		assertEquals(0, customers.getTotalUnspecified().intValue());

	}

}
