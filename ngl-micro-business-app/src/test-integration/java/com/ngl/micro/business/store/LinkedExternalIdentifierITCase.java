package com.ngl.micro.business.store;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.Set;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;


@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class LinkedExternalIdentifierITCase extends AbstractDatabaseITCase {

	private static final String KEY_A = "A";
	private static final String KEY_B = "B";
	private static final String KEY_C = "C";

	private Long STORE_4 = Data.PK_4;
	private Long STORE_5 = Data.PK_5;

	private LinkedExternalIdentifier identifiers;
	private TestBusinesses testBusinesses;
	private TestStores testStores;

	@Override
	public void before() {
		super.before();
		this.identifiers = new LinkedExternalIdentifier(this.sqlContext);
		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
	}

	@Test
	public void test_get_identifiers_by_key() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		this.assertKeys(1, KEY_A, this.identifiers.get(KEY_A));
		this.assertKeys(2, KEY_B, this.identifiers.get(KEY_B));
		this.assertKeys(2, KEY_C, this.identifiers.get(KEY_C));
	}

	private void assertKeys(int count, String key, Set<ExternalIdentifier> ids) {
		assertEquals(count, ids.size());
		ids.forEach(k -> {assertEquals(key, k.getKey());});
	}

	@Test
	public void test_store_identifiers_with_whitespace() {

		final String ADDED_KEY = " X ";
		final String EXPECTED_KEY = "X";

		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		ExternalIdentifier newExternalIdentifier = new ExternalIdentifier();
		newExternalIdentifier.setId(STORE_4);
		newExternalIdentifier.setKey(ADDED_KEY);
		newExternalIdentifier.setStartDate(Data.T_MINUS_10d);
		newExternalIdentifier.setEndDate(Data.T_MINUS_5d);
		this.identifiers.add(this.STORE_4, newExternalIdentifier);

		this.assertExpectedIdentifiers(this.STORE_4, Data.T_MINUS_10d, EXPECTED_KEY, KEY_A, KEY_B);
	}

	@Test
	public void test_get_store_identifiers_at_different_dates() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		this.assertExpectedIdentifiers(this.STORE_4, Data.T_MINUS_1y);
		this.assertExpectedIdentifiers(this.STORE_4, Data.T_MINUS_10M, KEY_A, KEY_B);
		this.assertExpectedIdentifiers(this.STORE_4, Data.T_MINUS_5M.plusSeconds(1), KEY_A, KEY_C);
		this.assertExpectedIdentifiers(this.STORE_4, Data.T_MINUS_1M, KEY_A, KEY_B);
		this.assertExpectedIdentifiers(this.STORE_4, Data.T_0, KEY_A, KEY_B);

	}

	private void assertExpectedIdentifiers(Long storeId, ZonedDateTime date, String... keys) {
		Set<ExternalIdentifier> ids = this.identifiers.getByStoreAt(storeId, date);

		assertEquals("expected matching number of keys", keys.length, ids.size());

		for (String key : keys) {
			assertTrue("contains expected key", ids.stream().anyMatch(id -> id.getKey().equals(key)));
		}

		ids.forEach(id -> {
			assertTrue(id.getStartDate().isBefore(date) || id.getStartDate().equals(date));
			assertTrue(id.getEndDate() == null || id.getEndDate().isAfter(date));
		});
	}

	@Test
	public void test_get_store_by_identifiers_at_different_dates() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		//Basic id
		assertEquals(null,     		this.identifiers.getStoreByIdentifierAt(KEY_A, Data.T_MINUS_1y));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_A, Data.T_MINUS_10M));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_A, Data.T_0));

		//With Gap
		assertEquals(null,     		this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_MINUS_1y));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_MINUS_10M));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_MINUS_5M.minusSeconds(1)));
		assertEquals(null,     		this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_MINUS_5M));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_MINUS_1M));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_B, Data.T_0));

		//Moved Stores
		assertEquals(null,     		this.identifiers.getStoreByIdentifierAt(KEY_C, Data.T_MINUS_1y));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_C, Data.T_MINUS_5M));
		assertEquals(this.STORE_4,  this.identifiers.getStoreByIdentifierAt(KEY_C, Data.T_MINUS_1M.minusSeconds(1)));
		assertEquals(this.STORE_5,  this.identifiers.getStoreByIdentifierAt(KEY_C, Data.T_MINUS_1M));
		assertEquals(this.STORE_5,  this.identifiers.getStoreByIdentifierAt(KEY_C, Data.T_0));
	}

}
