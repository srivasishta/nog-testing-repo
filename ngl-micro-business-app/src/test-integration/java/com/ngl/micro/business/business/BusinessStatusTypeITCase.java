package com.ngl.micro.business.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessStatusTypeDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessStatusType;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class BusinessStatusTypeITCase extends AbstractDatabaseITCase {

	private JBusinessStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JBusinessStatusTypeDao(this.sqlContext);
	}

	@Test
	public void test_business_status_mapping() throws Exception {
		assertEquals(ResourceStatus.ACTIVE.name(), BusinessStatus.ACTIVE.name());
		assertEquals(ResourceStatus.INACTIVE.name(), BusinessStatus.INACTIVE.name());
		JBusinessStatusType status;
		assertEquals("Expect equal status type count.", ResourceStatus.values().length, this.statusDao.count());
		for (ResourceStatus type : ResourceStatus.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
