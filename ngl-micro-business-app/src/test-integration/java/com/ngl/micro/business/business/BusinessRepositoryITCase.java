package com.ngl.micro.business.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.UUIDS;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class BusinessRepositoryITCase extends AbstractDatabaseITCase {

	private static final Long BUSINESS_ID = 1L;

	private BusinessRepository businessRepo;
	private TestBusinesses businesses;

	@Override
	public void before() {
		super.before();
		this.businessRepo = new BusinessRepository(this.sqlContext);
		this.businesses = new TestBusinesses();
	}

	@Test
	public void test_create_business() {
		Business business = this.businessRepo.add(this.businesses.business());
		Business reference = this.businesses.business();
		reference.setLogoImages(new ImageSet());
		this.validate(reference, business);
	}

	@Test
	public void test_get_business() {
		Business reference = this.businessRepo.add(this.businesses.business());
		Business business = this.businessRepo.get(reference.getId());
		assertEquals(reference.getId(), business.getId());
		this.validate(reference, business);
	}

	@Test
	public void test_get_null_business() {
		Business business = this.businessRepo.get(Data.toUUID(BUSINESS_ID));
		assertEquals(null, business);
	}

	//Doesn't break when linked dao deletes empty keyword set (MYSQL vs in memory DB?)
	@Test
	public void test_update_from_empty_linked_resources_business(){
		Business original = this.businesses.business();
		original.setCategories(new HashSet<>());
		original.setKeywords(new HashSet<>());
		original.setPhoneNumbers(new HashSet<>());
		original = this.businessRepo.add(original);
		UUID id = original.getId();
		Business updated = this.updatedBusiness();
		updated.setId(id);
		this.businessRepo.update(updated);
		Business persisted = this.businessRepo.get(id);
		this.validate(updated, persisted);
	}

	@Test
	public void test_update_business() {
		Business original = this.businessRepo.add(this.businesses.business());
		UUID id = original.getId();
		Business updated = this.updatedBusiness();
		updated.setId(id);
		this.businessRepo.update(updated);
		Business persisted = this.businessRepo.get(id);
		this.validate(updated, persisted);
	}

	private Business updatedBusiness() {
		Business reference = this.businesses.business();
		reference.setName("Another Name");
		reference.setEmailAddress("updated@email.com");
		reference.setWebsite("another website");
		reference.setStatus(BusinessStatus.INACTIVE);
		reference.setStatusReason("suspended");
		reference.getLogoImages().setOriginal("http://cdn.images/orignal.jpg");
		reference.getLogoImages().setLarge("http://cdn.images/large.jpg");

		Set<String> updatedKeywords = new HashSet<>(reference.getKeywords());
		updatedKeywords.add("another keyword");
		reference.setKeywords(updatedKeywords);

		Set<TradeCategory> updatedCategories = new HashSet<>(reference.getCategories());
		updatedCategories.add(TradeCategory.DINING__CATERING);
		reference.setCategories(updatedCategories);
		return reference;
	}

	@Test
	public void test_update_status() {
		Business business = this.businesses.business();
		business.setStatus(BusinessStatus.ACTIVE);
		business.setStatusReason("created");
		business = this.businessRepo.add(business);
		assertEquals(BusinessStatus.ACTIVE, business.getStatus());
		assertEquals("created", business.getStatusReason());

		business.setStatus(BusinessStatus.INACTIVE);
		business.setStatusReason("suspended");
		this.businessRepo.update(business);
		Business persisted = this.businessRepo.get(business.getId());
		assertEquals(BusinessStatus.INACTIVE, persisted.getStatus());
		assertEquals("suspended", persisted.getStatusReason());
	}

	@Test
	public void test_list_businesses() {
		Business a = this.businesses.business(Data.ID_1);
		a.setName("Business A");
		Business b = this.businesses.business(Data.ID_2);
		b.setName("Business B");
		Business c = this.businesses.business(Data.ID_3);
		c.setName("Business C");
		Business d = this.businesses.business(Data.ID_4);
		d.setName("Business D");

		this.businessRepo.add(a);
		this.businessRepo.add(b);
		this.businessRepo.add(c);
		this.businessRepo.add(d);

		PageRequest page = PageRequest.with(1, 3).build();
		Page<Business> businesses = this.businessRepo.get(page);
		assertEquals(1, businesses.getNumberOfItems());
		assertEquals(2, businesses.getTotalPages());
		assertEquals(4, businesses.getTotalItems());
		assertEquals(d.getName(), businesses.getItems().get(0).getName());
	}

	@Test
	public void test_list_businesses_filtered_by_partner_id() {
		UUID partnerId = UUIDS.type4Uuid();
		Business a = this.businesses.business(Data.ID_1);
		a.setPartnerId(partnerId);
		a.setName("Business A");

		this.businessRepo.add(a);

		SearchQuery q = SearchQueries.q(Business.Fields.PARTNER_ID).eq(partnerId).getSearchQuery();
		Page<Business> businesses = this.businessRepo.get(PageRequest.with(0, 3).search(q).build());
		assertEquals(1, businesses.getNumberOfItems());
		assertEquals(1, businesses.getTotalPages());
		assertEquals(1, businesses.getTotalItems());
		assertEquals(a.getName(), businesses.getItems().get(0).getName());

		q = SearchQueries.q(Business.Fields.PARTNER_ID).eq(UUIDS.type4Uuid()).getSearchQuery();
		businesses = this.businessRepo.get(PageRequest.with(0, 3).search(q).build());
		assertEquals(0, businesses.getNumberOfItems());
		assertEquals(0, businesses.getTotalPages());
		assertEquals(0, businesses.getTotalItems());
	}

	private void validate(Business reference, Business business) {
		assertNotNull(business.getId());
		assertEquals(reference.getName(), business.getName());
		assertEquals(reference.getEmailAddress(), business.getEmailAddress());
		assertEquals(reference.getWebsite(), business.getWebsite());
		assertEquals(reference.getStatus(), business.getStatus());
		assertEquals(reference.getKeywords().size(), business.getKeywords().size());
		assertEquals(reference.getCategories().size(), business.getCategories().size());
		assertEquals(reference.getPhoneNumbers(), business.getPhoneNumbers());
		assertEquals(reference.getLogoImages(), business.getLogoImages());
	}
}
