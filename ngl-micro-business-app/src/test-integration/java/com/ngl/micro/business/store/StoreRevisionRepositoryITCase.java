package com.ngl.micro.business.store;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.revision.jooq.JVersionControlService;
import com.ngl.micro.business.test_data.TestAccessTokens;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.Sort;
import com.ngl.middleware.rest.api.page.Sort.Direction;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.json.ObjectMapperFactory;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StoreRevisionRepositoryITCase extends AbstractDatabaseITCase {

	private static ObjectMapper mapper = new ObjectMapperFactory().getInstance();

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestBusinesses testBusinesses;
	private TestStores testStores;

	private StoreRepository stores;
	private JVersionControlService<Store> storeVc;
	private Merge merger;

	private Store s1;
	private Store s2;
	private UUID clientId;
	private UUID partnerId;

	@Override
	public void before() {
		super.before();

		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
		this.stores = new StoreRepository(this.sqlContext);

		this.storeVc = new JVersionControlService<>(
				mapper,
				JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(),
				"store_revision",
				new DAL(this.sqlContext));
		this.merger = new Merge();
		this.s1 = new Store();
		this.s1.setName("S1");
		this.s1.setId(UUIDS.type4Uuid());
		this.s2 = new Store();
		this.s2.setName("S2");
		this.s2.setId(this.s1.getId());

		TestAccessToken token = TestAccessTokens.full();
		this.clientId = token.getClientIdClaim();
		this.partnerId = token.getSubjectClaim();
		TestSubjects.login(token);
	}

	@Test
	public void test_add_baseline_revision() throws Exception {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		UUID storeId = TestStores.STORE_4_ID;

		Store store = this.stores.get(storeId);
		this.storeVc.revise(Time.utcNow(), new Revision(), new Store(), store, this.clientId, this.partnerId);

		Page<Revision> storeRevisions = this.storeVc.getRevisions(storeId, PageRequest.with(0, 1).build());

		Revision baseline = storeRevisions.getItems().get(0);
		Store rebuild = new Store();
		new Merge().merge(baseline.getAppliedPatch(), rebuild);

		Assert.assertEquals(store, rebuild);
	}

	@Test
	public void test_add_empty_patch() throws Exception {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		Optional<Revision> revision = this.storeVc.revise(Data.T_0, new Revision(), this.testStores.store(), this.testStores.store(), this.clientId, this.partnerId);
		Assert.assertFalse(revision.isPresent());
		Assert.assertEquals(0, this.storeVc.getRevisions(TestStores.STORE_4_ID, PageRequest.with(0, 1).build()).getTotalItems());
	}

	@Test
	public void test_add_unchanged_patch() throws Exception {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		this.storeVc.revise(Data.T_MINUS_10d, new Revision(), this.s2, this.s1, this.clientId, this.partnerId);
		this.storeVc.revise(Data.T_MINUS_1d, new Revision(), this.s2, this.s2, this.clientId, this.partnerId);

		Assert.assertEquals(1, this.storeVc.getRevisions(this.s1.getId(), PageRequest.with(0, 1).build()).getTotalItems());
	}

	@Test
	public void test_get_revision() throws Exception {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		Revision revision = this.storeVc.revise(Data.T_MINUS_1d, new Revision(), this.s1, this.s2, this.clientId, this.partnerId).get();

		Assert.assertNotNull(this.storeVc.getRevision(this.s1.getId(), revision.getId()));
	}

	@Test
	public void test_add_invalid_revision_date() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		this.thrown.expectStatus(ApplicationStatus.BAD_REQUEST);

		this.storeVc.revise(Data.T_MINUS_1d, new Revision(), this.s1, this.s2, this.clientId, this.partnerId);
		this.storeVc.revise(Data.T_MINUS_10d, new Revision(), this.s2, this.s1, this.clientId, this.partnerId);
	}

	@Test
	public void test_add_equal_revision_date() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);
		this.thrown.expectStatus(ApplicationStatus.BAD_REQUEST);

		this.storeVc.revise(Data.T_MINUS_1d, new Revision(), this.s1, this.s2, this.clientId, this.partnerId);
		this.storeVc.revise(Data.T_MINUS_1d, new Revision(), this.s2, this.s1, this.clientId, this.partnerId);
	}

	@Test
	public void test_list_and_apply_revisions() throws Exception {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		UUID storeId = TestStores.STORE_4_ID;

		Store baseStore = this.stores.get(storeId);

		this.storeVc.revise(Data.T_0.plusSeconds(1), new Revision(), new Store(), baseStore, this.clientId, this.partnerId);

		Store updated = this.updateRate(Data.T_0.plusSeconds(2), baseStore, storeId, "0.2");
		updated = this.updateRate(Data.T_0.plusSeconds(3), updated, storeId, "0.3");
		updated = this.updateRate(Data.T_0.plusSeconds(4), updated, storeId, "0.4");
		updated = this.updateRate(Data.T_0.plusSeconds(5), updated, storeId, "0.5");

		Store currentStore = this.stores.get(storeId);
		Sort sort = new Sort();
		sort.add(Revision.Fields.REVISED_DATE, Direction.ASC);

		Page<Revision> storeRevisions = this.storeVc.getRevisions(storeId, PageRequest.with(0, 100).sort(sort).build());

		Assert.assertEquals(5, storeRevisions.getTotalItems());

		Store rebuiltStore = new Store();
		for (Revision rev : storeRevisions.getItems()) {
			this.merger.merge(rev.getAppliedPatch(), rebuiltStore);
		}
		Assert.assertEquals(currentStore, rebuiltStore);
	}

	private Store updateRate(ZonedDateTime revisionDate, Store base, UUID storeId, String rate) {
		Store updated = this.stores.get(storeId);
		updated.setContributionRate(new BigDecimal(rate));
		this.storeVc.revise(revisionDate, new Revision(), base, updated, this.clientId, this.partnerId);
		return updated;
	}

}
