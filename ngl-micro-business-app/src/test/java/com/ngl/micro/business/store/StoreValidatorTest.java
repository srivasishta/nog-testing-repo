package com.ngl.micro.business.store;

import static com.ngl.middleware.util.Collections.asSet;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class StoreValidatorTest {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private StoreValidator validator;
	private TestStores stores;
	private DataFactory testData;

	@Before
	public void before() {
		this.validator = StoreValidator.newInstance();
		this.stores = new TestStores();
		this.testData = new DataFactory();
	}

	@Test
	public void test_valid_store() {
		this.validator.validate(this.stores.store());
	}

	@Test
	public void test_null_field_validation() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("businessId", ValidationErrorCode.INVALID)
				.expectValidationError("name", ValidationErrorCode.INVALID)
				.expectValidationError("status", ValidationErrorCode.INVALID)
				.expectValidationError("statusReason", ValidationErrorCode.INVALID)
				.expectValidationError("timezone", ValidationErrorCode.INVALID)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID)
				.expectValidationError("address.streetAddress", ValidationErrorCode.INVALID)
				.expectValidationError("address.locality", ValidationErrorCode.INVALID)
				.expectValidationError("address.region", ValidationErrorCode.INVALID)
				.expectValidationError("address.postalCode", ValidationErrorCode.INVALID)
				.expectValidationError("address.country", ValidationErrorCode.INVALID)
				.expectValidationError("tradingHours.startDay", ValidationErrorCode.INVALID)
				.expectValidationError("tradingHours.endDay", ValidationErrorCode.INVALID)
				.expectValidationError("tradingHours.openTime", ValidationErrorCode.INVALID)
				.expectValidationError("tradingHours.closeTime", ValidationErrorCode.INVALID)
				.expectValidationError("externalIdentifiers.key", ValidationErrorCode.INVALID)
				.expectValidationError("storeCategories", ValidationErrorCode.INVALID)
				.expectValidationError("storeKeywords", ValidationErrorCode.INVALID);

		Store store = new Store();
		store.setStatusReason(null);
		store.setAddress(new Address());
		store.setTradingHours(asSet(new TradingHours()));
		store.setExternalIdentifiers(asSet(new ExternalIdentifier()));
		store.setStoreCategories(null);
		store.setStoreKeywords(null);

		this.validator.validate(store);
	}

	@Test
	public void test_null_collection_validation() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("phoneNumbers", ValidationErrorCode.INVALID)
				.expectValidationError("storeCategories", ValidationErrorCode.INVALID)
				.expectValidationError("storeKeywords", ValidationErrorCode.INVALID)
				.expectValidationError("tradingHours", ValidationErrorCode.INVALID)
				.expectValidationError("externalIdentifiers", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setPhoneNumbers(null);
		store.setTradingHours(null);
		store.setExternalIdentifiers(null);
		store.setBusinessCategories(null);
		store.setStoreCategories(null);
		store.setStoreKeywords(null);
		this.validator.validate(store);
	}

	@Test
	public void test_short_field_validation() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("name", ValidationErrorCode.INVALID)
				.expectValidationError("externalIdentifiers.key", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setName("");
		store.setContributionRate(BigDecimal.ZERO);
		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setKey("");
		identifier.setStartDate(ZonedDateTime.of(LocalDateTime.of(2014, 1, 1, 12, 0 ,0), ZoneOffset.UTC));
		store.setExternalIdentifiers(asSet(identifier));
		this.validator.validate(store);
	}

	@Test
	public void test_long_field_validation() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("name", ValidationErrorCode.INVALID)
				.expectValidationError("statusReason", ValidationErrorCode.INVALID)
				.expectValidationError("description", ValidationErrorCode.INVALID)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID)
				.expectValidationError("address.streetAddress", ValidationErrorCode.INVALID)
				.expectValidationError("address.locality", ValidationErrorCode.INVALID)
				.expectValidationError("address.region", ValidationErrorCode.INVALID)
				.expectValidationError("address.postalCode", ValidationErrorCode.INVALID)
				.expectValidationError("externalIdentifiers.key", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setName(this.testData.getRandomWord(61));
		store.setDescription(this.testData.getRandomText(256));
		store.setStatusReason(this.testData.getRandomText(51));
		store.setContributionRate(new BigDecimal("0.000001"));

		Address address = new Address();
		address.setStreetAddress(this.testData.getRandomText(256));
		address.setLocality(this.testData.getRandomText(101));
		address.setRegion(this.testData.getRandomText(51));
		address.setPostalCode(this.testData.getRandomChars(51));
		address.setCountry(Country.CA);
		store.setAddress(address);

		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setKey(this.testData.getRandomChars(51));
		identifier.setStartDate(ZonedDateTime.of(LocalDateTime.of(2014, 1, 1, 12, 0 ,0), ZoneOffset.UTC));
		store.setExternalIdentifiers(asSet(identifier));
		this.validator.validate(store);
	}

	@Test
	public void test_invalidlong_field_validation() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("name", ValidationErrorCode.INVALID)
				.expectValidationError("statusReason", ValidationErrorCode.INVALID)
				.expectValidationError("description", ValidationErrorCode.INVALID)
				.expectValidationError("address.streetAddress", ValidationErrorCode.INVALID)
				.expectValidationError("address.locality", ValidationErrorCode.INVALID)
				.expectValidationError("address.region", ValidationErrorCode.INVALID)
				.expectValidationError("address.postalCode", ValidationErrorCode.INVALID)
				.expectValidationError("externalIdentifiers.key", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setName(this.testData.getRandomWord(61));
		store.setDescription(this.testData.getRandomText(256));
		store.setStatusReason(this.testData.getRandomText(51));

		Address address = new Address();
		address.setStreetAddress(this.testData.getRandomText(256));
		address.setLocality(this.testData.getRandomText(101));
		address.setRegion(this.testData.getRandomText(51));
		address.setPostalCode(this.testData.getRandomChars(51));
		address.setCountry(Country.CA);
		store.setAddress(address);

		ExternalIdentifier identifier = new ExternalIdentifier();
		identifier.setKey(this.testData.getRandomChars(51));
		identifier.setStartDate(ZonedDateTime.of(LocalDateTime.of(2014, 1, 1, 12, 0 ,0), ZoneOffset.UTC));
		store.setExternalIdentifiers(asSet(identifier));
		this.validator.validate(store);
	}

	@Test
	public void test_validate_invalid_geo_max() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
		.expectStatus(ApplicationStatus.BAD_REQUEST)
		.expectValidationError("geoLocation.lat", ValidationErrorCode.INVALID)
		.expectValidationError("geoLocation.lon", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setGeoLocation(GeoLocation.of(
				com.ngl.middleware.util.geo.GeoLocation.MAX_LAT_RAD + 0.1,
				com.ngl.middleware.util.geo.GeoLocation.MAX_LON_RAD + 0.1));
		this.validator.validate(store);
	}

	@Test
	public void test_validate_invalid_geo_min() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
		.expectStatus(ApplicationStatus.BAD_REQUEST)
		.expectValidationError("geoLocation.lat", ValidationErrorCode.INVALID)
		.expectValidationError("geoLocation.lon", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setGeoLocation(GeoLocation.of(
				com.ngl.middleware.util.geo.GeoLocation.MIN_LAT_RAD - 0.1,
				com.ngl.middleware.util.geo.GeoLocation.MIN_LON_RAD - 0.1));
		this.validator.validate(store);
	}

	@Test
	public void test_validate_nullable_lat_lon() {
		Store store = this.stores.store();
		store.setGeoLocation(GeoLocation.of(null, null));
		this.validator.validate(store);
	}

	@Test
	public void test_valid_contribution_range() {
		Store store = this.stores.store();
		store.setContributionRate(new BigDecimal("0.00000"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("0.0"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("0"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("0.99999"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("1.00000"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("1.0"));
		this.validator.validate(store);
		store.setContributionRate(new BigDecimal("1"));
		this.validator.validate(store);
	}

	@Test
	public void test_invalid_max_int() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setContributionRate(new BigDecimal("2"));
		this.validator.validate(store);
	}

	@Test
	public void test_invalid_max_precision() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setContributionRate(new BigDecimal("1.00001"));
		this.validator.validate(store);
	}

	@Test
	public void test_invalid_min_contribution_rate() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setContributionRate(new BigDecimal("-0.00001"));
		this.validator.validate(store);
	}

	@Test
	public void test_invalid_precision() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("contributionRate", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setContributionRate(new BigDecimal("0.000001"));
		this.validator.validate(store);
	}

	@Test
	public void test_fail_unsupported_zone() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("timezone", ValidationErrorCode.INVALID);

		Store store = this.stores.store();
		store.setTimezone(ZoneId.of("Africa/Johannesburg"));

		this.validator.validate(store);
	}

}
