package com.ngl.micro.business.test_data;

import java.util.UUID;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

public class TestAccessTokens {

	public static TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);

	private static final UUID PARTNER_2 = Data.ID_2;

	public static TestAccessToken full() {
		return TestAccessToken.newBasic()
				.subjectClaim(Data.ID_2)
				.clientIdClaim(Data.ID_2)
				.scope(Collections.asSet(
						Scopes.BUSINESS_READ_SCOPE,
						Scopes.BUSINESS_WRITE_SCOPE,
						Scopes.STORE_READ_SCOPE,
						Scopes.STORE_WRITE_SCOPE,
						Scopes.SALE_READ_SCOPE));
	}

	public static TestAccessToken none() {
		return TestAccessToken.newBasic()
				.subjectClaim(PARTNER_2)
				.clientIdClaim(PARTNER_2)
				.scope(Collections.asSet());
	}


	public static TestAccessToken withScope(String... scope) {
		return TestAccessToken.newBasic()
				.subjectClaim(PARTNER_2)
				.clientIdClaim(PARTNER_2)
				.scope(Collections.asSet(scope));
	}

}
