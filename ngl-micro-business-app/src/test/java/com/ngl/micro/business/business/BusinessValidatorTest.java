package com.ngl.micro.business.business;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class BusinessValidatorTest {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestBusinesses businesses;
	private BusinessValidator validator;
	private DataFactory dataFactory;

	@Before
	public void before() {
		this.businesses = new TestBusinesses();
		this.validator = BusinessValidator.newInstance();
		this.dataFactory = new DataFactory();
	}

	@Test
	public void test_valid_business() {
		Business business = this.businesses.business();
		this.validator.validate(business);
	}

	@Test
	public void test_null_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("partnerId", ValidationErrorCode.INVALID)
					.expectValidationError("name", ValidationErrorCode.INVALID)
					.expectValidationError("status", ValidationErrorCode.INVALID)
					.expectValidationError("statusReason", ValidationErrorCode.INVALID)
					.expectValidationError("keywords", ValidationErrorCode.INVALID)
					.expectValidationError("phoneNumbers", ValidationErrorCode.INVALID)
					.expectValidationError("categories", ValidationErrorCode.INVALID);

		Business business = new Business();
		business.setKeywords(null);
		business.setPhoneNumbers(null);
		business.setCategories(null);
		business.setStatusReason(null);
		this.validator.validate(business);
	}

	@Test
	public void test_fields_too_short() {

		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
					.expectStatus(ApplicationStatus.BAD_REQUEST)
					.expectValidationError("name", ValidationErrorCode.INVALID);

		Business business = this.businesses.business();
		business.setName("");
		this.validator.validate(business);
	}

	@Test
	public void test_fields_too_long() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
			.expectStatus(ApplicationStatus.BAD_REQUEST)
			.expectValidationError("name", ValidationErrorCode.INVALID)
			.expectValidationError("description", ValidationErrorCode.INVALID);

		Business business = this.businesses.business();
		business.setName(this.dataFactory.getRandomWord(61));
		business.setDescription(this.dataFactory.getRandomText(256));
		this.validator.validate(business);
	}

	@Test
	public void test_invalid_web_addresses() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
			.expectStatus(ApplicationStatus.BAD_REQUEST)
			.expectValidationError("website", ValidationErrorCode.INVALID)
			.expectValidationError("emailAddress", ValidationErrorCode.INVALID);

		Business business = this.businesses.business();
		business.setWebsite("www.missinghttp.com");
		business.setEmailAddress("invalid#email.com");
		this.validator.validate(business);
	}
}