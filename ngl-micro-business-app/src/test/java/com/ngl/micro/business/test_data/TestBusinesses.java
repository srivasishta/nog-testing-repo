package com.ngl.micro.business.test_data;

import static com.ngl.micro.shared.contracts.ResourceStatus.ACTIVE;
import static com.ngl.micro.shared.contracts.ResourceStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_DEACTIVATED;
import static com.ngl.middleware.util.Collections.asSet;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessCategoriesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessKeywordDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JBusinessStatusHistoryDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusiness;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessCategories;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessKeyword;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JBusinessStatusHistory;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.TradeCategoryType;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

/**
 * Business dataset.
 *
 * @author Paco Mendes
 */
public class TestBusinesses  implements JooqTestDataSet {

	public static final UUID BUSINESS_ID = Data.ID_1;
	public static final Long BUSINESS_PK = Data.PK_1;
	public static final UUID PARTNER_ID = Data.ID_2;

	public Business business() {
		return this.business(BUSINESS_ID);
	}

	public Business business(UUID id) {
		PhoneNumber primary = new PhoneNumber();
		primary.setType(PhoneNumberType.TEL);
		primary.setCountryCode("1");
		primary.setAreaCode("780");
		primary.setNumber("4751201");

		PhoneNumber fax = new PhoneNumber();
		fax.setType(PhoneNumberType.FAX);
		fax.setCountryCode("1");
		fax.setAreaCode("780");
		fax.setNumber("4751202");

		Business business = new Business();
		business.setId(id);
		business.setPartnerId(PARTNER_ID);
		business.setName("Cape Spanish");
		business.setEmailAddress("contact@cape-spanish.com");
		business.setWebsite("http://www.cape-spanish.com");
		business.setStatus(BusinessStatus.ACTIVE);
		business.setStatusReason("enrolled");
		business.setKeywords(asSet("malay", "cuisine"));
		business.setPhoneNumbers(asSet(primary, fax));
		business.setCategories(this.tradeCategories());
		business.getLogoImages().setOriginal("http://cdn.images/orignal.jpg");
		business.getLogoImages().setLarge("http://cdn.images/large.jpg");
		return business;
	}

	private Set<TradeCategory> tradeCategories() {
		return asSet(TradeCategory.DINING__QUICK_SERVICE, TradeCategory.DINING__CASUAL_DINING);
	}

	@Override
	public void loadData(DSLContext sql) {
		JBusinessDao businessDao = new JBusinessDao(sql.configuration());
		JBusinessCategoriesDao businessCategories = new JBusinessCategoriesDao(sql.configuration());
		JBusinessKeywordDao keywords = new JBusinessKeywordDao(sql.configuration());
		JBusinessStatusHistoryDao statusHistory = new JBusinessStatusHistoryDao(sql.configuration());

		businessDao.insert(
				this.business(1L, Data.ID_1, "Beef Palace", "Meat done right", "meat@palace.com", "http://www.meatpalace.com"),
				this.business(2L, Data.ID_2, "Something Fishy", "Too good to be true", "something@fishy.com", "http://www.somethingfishy.com"),
				this.business(3L, Data.ID_3, "Jeans and Shoes", "All accessories below the belt", "below@thebelt.com", "http://www.thebelt.com")
		);

		businessCategories.insert(
				new JBusinessCategories(1L, TradeCategoryType.DINING__SPORTS_BARS.getId()),
				new JBusinessCategories(2L, TradeCategoryType.DINING__CASUAL_DINING.getId())
		);

		keywords.insert(
				new JBusinessKeyword(Data.PK_1, Data.STRING_1),
				new JBusinessKeyword(Data.PK_2, Data.STRING_2)
				);

		statusHistory.insert(
				this.status(1L,  ACTIVE,   REASON_ACTIVATED,   Data.T_MINUS_10M, Data.T_MINUS_1M),
				this.status(1L,  INACTIVE, REASON_DEACTIVATED, Data.T_MINUS_1M, Data.T_0),
				this.status(1L,  ACTIVE,   REASON_ACTIVATED,   Data.T_0, null),
				this.status(2L,  ACTIVE,   REASON_ACTIVATED,   Data.T_0, null),
				this.status(3L,  ACTIVE,   REASON_ACTIVATED,   Data.T_0, null)
				);
	}

	private JBusiness business(Long id, UUID resourceId, String name, String description, String email, String website) {
		JBusiness business = new JBusiness();
		business.setId(id);
		business.setPartnerId(PARTNER_ID);
		business.setResourceId(resourceId);
		business.setName(name);
		business.setDescription(description);
		business.setEmailAddress(email);
		business.setWebsite(website);
		return business;
	}

	private JBusinessStatusHistory status(Long businessId, ResourceStatus status, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JBusinessStatusHistory statusHistory = new JBusinessStatusHistory();
		statusHistory.setBusinessId(businessId);
		statusHistory.setStatusId(status.getId());
		statusHistory.setStatusReason(reason);
		statusHistory.setStartDate(startDate);
		statusHistory.setEndDate(endDate);
		return statusHistory;
	}
}
