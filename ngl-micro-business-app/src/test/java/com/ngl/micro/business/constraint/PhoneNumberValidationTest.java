package com.ngl.micro.business.constraint;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class PhoneNumberValidationTest {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private ValueTypeValidator validator;

	@Before
	public void before() {
		this.validator = ValueTypeValidator.newInstance();
	}

	@Test
	public void test_valid_phone_number_fields() {
		PhoneNumber number = new PhoneNumber();
		number.setType(PhoneNumberType.TEL);
		number.setAreaCode("12345");
		number.setCountryCode("270");
		number.setNumber("1234567890123");
		number.setExtension("12345");
		this.validator.validate(number);
	}

	@Test
	public void test_null_phone_number_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
			.expectStatus(ApplicationStatus.BAD_REQUEST)
			.expectValidationError("type", ValidationErrorCode.INVALID)
			.expectValidationError("countryCode", ValidationErrorCode.INVALID)
			.expectValidationError("areaCode", ValidationErrorCode.INVALID)
			.expectValidationError("number", ValidationErrorCode.INVALID);
		this.validator.validate(new PhoneNumber());
	}

	@Test
	public void test_max_phone_number_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
			.expectStatus(ApplicationStatus.BAD_REQUEST)
			.expectValidationError("countryCode", ValidationErrorCode.INVALID)
			.expectValidationError("areaCode", ValidationErrorCode.INVALID)
			.expectValidationError("number", ValidationErrorCode.INVALID)
			.expectValidationError("extension", ValidationErrorCode.INVALID);

		PhoneNumber number = new PhoneNumber();
		number.setType(PhoneNumberType.TEL);
		number.setCountryCode("0000");
		number.setAreaCode("000000");
		number.setNumber("00000000000000");
		number.setExtension("000000");
		this.validator.validate(number);
	}

	@Test
	public void test_invalid_chars() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
			.expectStatus(ApplicationStatus.BAD_REQUEST)
			.expectValidationError("countryCode", ValidationErrorCode.INVALID)
			.expectValidationError("areaCode", ValidationErrorCode.INVALID)
			.expectValidationError("number", ValidationErrorCode.INVALID)
			.expectValidationError("extension", ValidationErrorCode.INVALID);

		PhoneNumber number = new PhoneNumber();
		number.setType(PhoneNumberType.TEL);
		number.setCountryCode("+01");
		number.setAreaCode("*000#");
		number.setNumber("*0000000000#");
		number.setExtension("*000#");
		this.validator.validate(number);
	}
}