package com.ngl.micro.business.test_data;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.business.dal.generated.jooq.tables.daos.JSalesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JSales;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

/**
 * Sale dataset
 *
 * @author Paco Mendes
 */
public class TestSales implements JooqTestDataSet {

	public static final UUID GROUP_ID = UUID.fromString("79c6f9d9-647e-493f-b3f9-fe89e9993c4c");

	@Override
	public void loadData(DSLContext sql) {
		JSalesDao saleDao = new JSalesDao(sql.configuration());

		saleDao.insert(
				this.sale(1L,  Data.ID_4, Data.ID_2, Data.T_0.minusDays(13), "45.71" , "4.571"  , Gender.MALE        , 18),
				this.sale(2L,  Data.ID_4, Data.ID_3, Data.T_0.minusDays(12), "21.20" , "2.12"   , Gender.FEMALE      , 20),
				this.sale(3L,  Data.ID_4, Data.ID_2, Data.T_0.minusDays(11), "2.99"  , "0.299"  , Gender.MALE        , 18),
				this.sale(4L,  Data.ID_4, Data.ID_4, Data.T_0.minusDays(10), "5.41"  , "0.541"  , Gender.UNSPECIFIED , null),
				this.sale(5L,  Data.ID_5, Data.ID_5, Data.T_0.minusDays(9),  "14.71" , "1.471"  , Gender.UNSPECIFIED , null),
				this.sale(6L,  Data.ID_5, Data.ID_5, Data.T_0.minusDays(8),  "25.45" , "2.545"  , Gender.UNSPECIFIED , null),
				this.sale(7L,  Data.ID_5, Data.ID_5, Data.T_0.minusDays(7),  "4.57"  , "0.457"  , Gender.UNSPECIFIED , null),
				this.sale(8L,  Data.ID_5, Data.ID_5, Data.T_0.minusDays(6),  "1.61"  , "0.161"  , Gender.UNSPECIFIED , null),
				this.sale(9L,  Data.ID_5, Data.ID_5, Data.T_0.minusDays(5),  "280.52", "28.052" , Gender.UNSPECIFIED , null),
				this.sale(10L, Data.ID_5, Data.ID_5, Data.T_0.minusDays(4),  "2.99"  , "0.299"  , Gender.UNSPECIFIED , null),
				this.sale(11L, Data.ID_5, Data.ID_5, Data.T_0.minusDays(3),  "109.54", "10.954" , Gender.UNSPECIFIED , null),
				this.sale(12L, Data.ID_6, Data.ID_6, Data.T_0.minusDays(2),  "13.14" , "1.314"  , Gender.UNSPECIFIED , null),
				this.sale(13L, Data.ID_6, Data.ID_6, Data.T_0.minusDays(1),  "49.52" , "4.952"  , Gender.UNSPECIFIED , null),
				this.sale(14L, Data.ID_9, Data.ID_9, Data.T_0,               "12.41" , "1.241"  , Gender.UNSPECIFIED , null)
		);
	}

	private JSales sale(Long id, UUID storeId, UUID customerId, ZonedDateTime saleDate, String saleAmount, String contributionAmount, Gender gender, Integer age) {
		JSales sale = new JSales();
		sale.setId(id);
		sale.setContributionDocumentId(Data.toUUID(id));
		sale.setBusinessId(TestBusinesses.BUSINESS_ID);
		sale.setStoreId(storeId);
		sale.setCustomerId(customerId);
		sale.setSaleDate(saleDate);
		sale.setSaleAmount(new BigDecimal(saleAmount));
		sale.setContributionAmount(new BigDecimal(contributionAmount));
		sale.setDemographicsBirthdate(age == null? null : Date.valueOf(Data.T_0.minusYears(age).toLocalDate()));
		sale.setDemographicsGender(gender.getId());
		return sale;
	}
}