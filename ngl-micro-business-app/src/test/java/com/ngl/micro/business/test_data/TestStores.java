package com.ngl.micro.business.test_data;

import static com.ngl.micro.shared.contracts.ResourceStatus.ACTIVE;
import static com.ngl.micro.shared.contracts.ResourceStatus.INACTIVE;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_DEACTIVATED;
import static com.ngl.middleware.util.Collections.asSet;
import static java.time.DayOfWeek.SATURDAY;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreAddressDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreCategoriesDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreContributionRateDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreIdentifierDao;
import com.ngl.micro.business.dal.generated.jooq.tables.daos.JStoreStatusHistoryDao;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStore;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreAddress;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreCategories;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreContributionRate;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreIdentifier;
import com.ngl.micro.business.dal.generated.jooq.tables.pojos.JStoreStatusHistory;
import com.ngl.micro.business.store.Address;
import com.ngl.micro.business.store.Country;
import com.ngl.micro.business.store.ExternalIdentifier;
import com.ngl.micro.business.store.GeoLocation;
import com.ngl.micro.business.store.Store;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.business.store.TradingHours;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.TradeCategoryType;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

/**
 * Business dataset.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class TestStores implements JooqTestDataSet {

	// 40.757810, -73.984864
	public static double TIMES_SQUARE_LAT_RAD = 0.7113579804d;
	public static double TIMES_SQUARE_LON_RAD = -1.29127947344d;

	// Empire state: ~1.1km from Times Square
	public static double STORE_4_LAT = 0.711184128154d;
	public static double STORE_4_LON = -1.29129364551d;

	// Statue of Liberty: ~9.11km from Times Square
	public static double STORE_5_LAT = 0.710174088662d;
	public static double STORE_5_LON = -1.29231230693d;

	// Rockefeller: ~0.58km from Times Square
	public static double STORE_6_LAT = 0.711368504736d;
	public static double STORE_6_LON = -1.2911627981765d;

	// Musuem of City of NY: ~4.67km from Times Square (Inactive)
	public static double STORE_7_LAT = 0.71195889726d;
	public static double STORE_7_LON = -1.2906935315005d;

	// UN Headquarters: ~1.73km from Times Square
	public static double STORE_8_LAT = 0.711199521958d;
	public static double STORE_8_LON = -1.29099426918d;

	// Charging Bull: ~6.48km from Times Square
	public static double STORE_9_LAT = 0.71043958815d;
	public static double STORE_9_LON = -1.29181864055d;

	public static UUID STORE_4_ID = Data.ID_4;
	public static Long STORE_4_PK = Data.PK_4;
	public static UUID STORE_4_BUSINESS_ID = Data.ID_1;

	public static String EXTERNAL_ID_A = "A";
	public static String EXTERNAL_ID_B = "B";
	public static String EXTERNAL_ID_C = "C";
	public static String EXTERNAL_ID_D = "D";
	public static String EXTERNAL_ID_E = "E";
	public static String EXTERNAL_ID_F = "F";
	public static String EXTERNAL_ID_G = "G";
	public static String EXTERNAL_ID_H = "H";

	@Override
	public void loadData(DSLContext sql) {

		JStoreDao storeDao = new JStoreDao(sql.configuration());
		JStoreCategoriesDao storeCategoriesDao = new JStoreCategoriesDao(sql.configuration());
		JStoreAddressDao addressDao = new JStoreAddressDao(sql.configuration());
		JStoreStatusHistoryDao statusHistory = new JStoreStatusHistoryDao(sql.configuration());
		JStoreIdentifierDao identifiers = new JStoreIdentifierDao(sql.configuration());
		JStoreContributionRateDao contributionRates = new JStoreContributionRateDao(sql.configuration());

		storeDao.insert(
				this.store(Data.PK_4, Data.ID_4, Data.ID_1, "Empire State Store", "NY's favorite beef stop", "info@meaty.com", "http://www.somethingmeaty.com", "America/Edmonton", STORE_4_LAT, STORE_4_LON),
				this.store(Data.PK_5, Data.ID_5, Data.ID_1, "Statue of Liberty Store", "Liberty's favorite hot dog stop", "info@meaty.com", "http://www.somethingmeaty.com", "America/Edmonton", STORE_5_LAT, STORE_5_LON),
				this.store(Data.PK_6, Data.ID_6, Data.ID_1, "Rockefeller Centre Store", "Rockefeller's favorite downtown joint for beef.", "info@meaty.com", "http://www.somethingmeaty.com", "America/Edmonton", STORE_6_LAT, STORE_6_LON),
				this.store(Data.PK_7, Data.ID_7, Data.ID_2, "Museum of the City of New York", "History for everyone", "something@fishy.com", "http://www.somethingmeaty.com", "America/Edmonton", STORE_7_LAT, STORE_7_LON),
				this.store(Data.PK_8, Data.ID_8, Data.ID_2, "United Nations Headquarters", "Buy a peace", "something@fishy.com", "http://www.somethingmeaty.com", "America/Edmonton", STORE_8_LAT, STORE_8_LON),
				this.store(Data.PK_9, Data.ID_9, Data.ID_3, "Charging Bull", "Money hungry", "below@thebelt.com", "http://www.thebelt.com", "America/Edmonton", STORE_9_LAT, STORE_9_LON)
		);

		storeCategoriesDao.insert(new JStoreCategories(Data.PK_4, TradeCategoryType.DINING__CASUAL_DINING.getId()));
		storeCategoriesDao.insert(new JStoreCategories(Data.PK_9, TradeCategoryType.APPAREL__GENERAL.getId()));

		addressDao.insert(
				this.address(Data.PK_4, "123 McEwan Ave", "Edmonton", "AB", Country.CA, "T6W1W1"),
				this.address(Data.PK_5, "12 Donald Road", "Calgary", "AB", Country.CA, "T6W1W1"),
				this.address(Data.PK_6, "3A Main Street", "Edmonton", "AB", Country.CA, "T6W1W1"),
				this.address(Data.PK_7, "12 Dock Road", "Calgary", "AB", Country.CA, "T6W1W1"),
				this.address(Data.PK_8, "1 Central Street", "Edmonton", "AB", Country.CA, "T6W1W1"),
				this.address(Data.PK_9, "11 Claude Drive", "Beverly Hills", "CA", Country.US, "90210")
		);

		statusHistory.insert(
				this.status(Data.PK_4,  ACTIVE,   REASON_ACTIVATED, Data.T_MINUS_10M, Data.T_MINUS_1M),
				this.status(Data.PK_4,  INACTIVE, REASON_DEACTIVATED, Data.T_MINUS_1M, Data.T_0),
				this.status(Data.PK_4,  ACTIVE,   REASON_ACTIVATED, Data.T_0, null),
				this.status(Data.PK_5,  ACTIVE,   REASON_ACTIVATED, Data.T_MINUS_5M, null),
				this.status(Data.PK_6,  ACTIVE,   REASON_ACTIVATED, Data.T_0, null),
				this.status(Data.PK_7,  ACTIVE,   REASON_ACTIVATED, Data.T_0, null),
				this.status(Data.PK_8,  ACTIVE,   REASON_ACTIVATED, Data.T_0, null),
				this.status(Data.PK_9,  ACTIVE,   REASON_ACTIVATED, Data.T_0, null)
				);

		identifiers.insert(
				this.id(Data.PK_4, EXTERNAL_ID_A ,Data.T_MINUS_10M, null),
				this.id(Data.PK_4, EXTERNAL_ID_B ,Data.T_MINUS_10M, Data.T_MINUS_5M), // Gap between T minus 10M and 5M
				this.id(Data.PK_4, EXTERNAL_ID_B ,Data.T_MINUS_1M, null),
				this.id(Data.PK_4, EXTERNAL_ID_C ,Data.T_MINUS_5M, Data.T_MINUS_1M),//Moved to store 5
				this.id(Data.PK_5, EXTERNAL_ID_C ,Data.T_MINUS_1M, null), //Moved from store 4
				this.id(Data.PK_5, EXTERNAL_ID_D ,Data.T_MINUS_5M, null),
				this.id(Data.PK_6, EXTERNAL_ID_E ,Data.T_MINUS_5M, null),
				this.id(Data.PK_7, EXTERNAL_ID_F ,Data.T_MINUS_5M, null),
				this.id(Data.PK_8, EXTERNAL_ID_G ,Data.T_MINUS_5M, null),
				this.id(Data.PK_9, EXTERNAL_ID_H ,Data.T_MINUS_5M, null)
				);

		contributionRates.insert(
				this.rate(Data.PK_4, "0.2"   ,Data.T_MINUS_10M, Data.T_MINUS_1M),
				this.rate(Data.PK_4, "0.0"   ,Data.T_MINUS_1M, Data.T_0),
				this.rate(Data.PK_4, "0.1"   ,Data.T_0, null),
				this.rate(Data.PK_5, "0.1"   ,Data.T_MINUS_5M, null),
				this.rate(Data.PK_6, "0.1"   ,Data.T_0, null),
				this.rate(Data.PK_7, "0.1"   ,Data.T_0, null),
				this.rate(Data.PK_8, "0.1"   ,Data.T_0, null),
				this.rate(Data.PK_9, "0.1"   ,Data.T_0, null)
				);

	}

	private JStore store(Long id, UUID resourceId, UUID businessId, String name, String description, String email,
			String website, String timezone, Double lat, Double lon) {
		JStore store = new JStore();
		store.setId(id);
		store.setResourceId(resourceId);
		store.setBusinessId(businessId);
		store.setName(name);
		store.setDescription(description);
		store.setEmailAddress(email);
		store.setWebsite(website);
		store.setTimezone(timezone);
		store.setLat(lat);
		store.setLon(lon);
		return store;
	}

	private JStoreAddress address(Long storeId, String streetAddress, String locality, String region,
			Country country, String postalCode) {
		JStoreAddress address = new JStoreAddress();
		address.setStoreId(storeId);
		address.setStreetAddress(streetAddress);
		address.setRegion(region);
		address.setLocality(locality);
		address.setCountry(country.name());
		address.setPostalCode(postalCode);
		return address;
	}

	private JStoreStatusHistory status(Long storeId, ResourceStatus status, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JStoreStatusHistory statusHistory = new JStoreStatusHistory();
		statusHistory.setStoreId(storeId);
		statusHistory.setStatusId(status.getId());
		statusHistory.setStatusReason(reason);
		statusHistory.setStartDate(startDate);
		statusHistory.setEndDate(endDate);
		return statusHistory;
	}

	private JStoreIdentifier id(Long storeId, String id, ZonedDateTime startDate, ZonedDateTime endDate) {
		JStoreIdentifier identifier = new JStoreIdentifier();
		identifier.setStoreId(storeId);
		identifier.setExternalId(id);
		identifier.setStartDate(startDate);
		identifier.setEndDate(endDate);
		return identifier;
	}

	private JStoreContributionRate rate(Long storeId, String rate, ZonedDateTime startDate, ZonedDateTime endDate) {
		JStoreContributionRate record = new JStoreContributionRate();
		record.setStoreId(storeId);
		record.setRate(new BigDecimal(rate));
		record.setStartDate(startDate);
		record.setEndDate(endDate);
		return record;
	}

	public Store store() {
		return this.store(Data.ID_4);
	}

	public Store store(UUID id) {
		Store store = new Store();
		store.setId(id);
		store.setBusinessId(Data.ID_1);
		store.setName("Something Meaty");
		store.setDescription("Children's steakhouse, because you can never be too young, to appreciate a good steak.");
		store.setAddress(this.address());
		store.setGeoLocation(GeoLocation.of(-1.219138,-2.104077));
		store.setEmailAddress("something@meaty.com");
		store.setWebsite("http://www.meaty.com");
		store.setStatus(StoreStatus.ACTIVE);
		store.setStatusReason("created");
		store.setTradingHours(this.tradingHours());
		store.setTimezone(ZoneId.of("America/Edmonton"));
		store.setPhoneNumbers(this.phoneNumbers());
		store.setExternalIdentifiers(new HashSet<>());
		store.setStoreKeywords(this.keywords());
		store.setStoreCategories(this.storeCategories());
		store.setContributionRate(new BigDecimal("0.02500"));
		return store;
	}

	public Store updatedStore(Store store) {

		store.setName("Something Else");
		store.setDescription("Adult steakhouse");
		store.setAddress(this.address());
		store.setGeoLocation(GeoLocation.of(-1.2219138,-2.11464077));
		store.setEmailAddress("something@meaty.com");
		store.setWebsite("http://www.meaty.com");
		store.setStatus(StoreStatus.INACTIVE);
		store.setStatusReason("suspended");
		store.setTimezone(ZoneId.of("America/Edmonton"));
		store.setPhoneNumbers(this.updatedPhoneNumbers());
		store.setContributionRate(new BigDecimal("0.03000"));

		//Unchanged
		store.setTradingHours(this.tradingHours());
		store.setExternalIdentifiers(this.identifiers());
		store.setStoreKeywords(this.keywords());
		store.setBusinessCategories(this.businessCategories());
		store.setStoreCategories(this.storeCategories());
		return store;
	}

	public Set<PhoneNumber> phoneNumbers() {

		PhoneNumber number = new PhoneNumber();
		number.setType(PhoneNumberType.TEL);
		number.setNumber("12345678");
		number.setCountryCode("1");
		number.setAreaCode("780");
		Set<PhoneNumber> phoneNumbers = new HashSet<>();
		phoneNumbers.add(number);
		return phoneNumbers;
	}

	public Set<PhoneNumber> updatedPhoneNumbers() {

		PhoneNumber number = new PhoneNumber();
		number.setType(PhoneNumberType.TEL);
		number.setNumber("22222222");
		number.setCountryCode("2");
		number.setAreaCode("222");
		number.setExtension("22");

		PhoneNumber fax = new PhoneNumber();
		fax.setType(PhoneNumberType.FAX);
		fax.setNumber("3333333");
		fax.setCountryCode("2");
		fax.setAreaCode("333");
		return asSet(number, fax);
	}

	public Address address() {

		Address address = new Address();
		address.setId(1L);
		address.setStreetAddress("245 Munroe Drive");
		address.setLocality("Edmonton");
		address.setRegion("Alberta");
		address.setPostalCode("T6W1W1");
		address.setCountry(Country.CA);
		return address;
	}

	public Address updatedAddress(Address address) {
		address.setStreetAddress("245 Updated Drive");
		address.setLocality("Somewhere");
		address.setRegion("Else");
		address.setPostalCode("T6W1W2");
		address.setCountry(Country.US);
		return address;
	}

	public Set<TradingHours> tradingHours() {

		TradingHours weekday = new TradingHours();
		weekday.setStartDay(DayOfWeek.MONDAY);
		weekday.setEndDay(DayOfWeek.FRIDAY);
		weekday.setOpenTime(LocalTime.of(8, 0));
		weekday.setCloseTime(LocalTime.of(17, 0));

		TradingHours sat = new TradingHours();
		sat.setStartDay(SATURDAY);
		sat.setEndDay(SATURDAY);
		sat.setOpenTime(LocalTime.of(10, 0));
		sat.setCloseTime(LocalTime.of(14, 0));

		Set<TradingHours> hours = new HashSet<>();
		hours.add(weekday);
		hours.add(sat);
		return hours;
	}

	public Set<TradingHours> updatedTradingHours() {

		TradingHours weekday = new TradingHours();
		weekday.setStartDay(DayOfWeek.MONDAY);
		weekday.setEndDay(DayOfWeek.FRIDAY);
		weekday.setOpenTime(LocalTime.of(9, 0));
		weekday.setCloseTime(LocalTime.of(17, 0));

		TradingHours sat = new TradingHours();
		sat.setStartDay(SATURDAY);
		sat.setEndDay(SATURDAY);
		sat.setOpenTime(LocalTime.of(10, 0));
		sat.setCloseTime(LocalTime.of(13, 0));

		TradingHours sun = new TradingHours();
		sun.setStartDay(DayOfWeek.SUNDAY);
		sun.setEndDay(DayOfWeek.SUNDAY);
		sun.setOpenTime(LocalTime.of(10, 0));
		sun.setCloseTime(LocalTime.of(12, 0));

		Set<TradingHours> hours = new HashSet<>();
		hours.add(weekday);
		hours.add(sat);
		return hours;
	}

	public Set<String> keywords() {
		return asSet("steak", "beef", "sirloin", "ribs");
	}

	public Set<String> updatedKeywords() {
		return asSet("steak", "beef", "chops");
	}

	public Set<ExternalIdentifier> identifiers() {

		ExternalIdentifier prevIdentifier = new ExternalIdentifier();
		prevIdentifier.setKey("123ABC");
		prevIdentifier.setStartDate(ZonedDateTime.of(LocalDateTime.of(2014, 8, 21, 11, 23, 04), ZoneOffset.UTC));
		prevIdentifier.setEndDate(ZonedDateTime.of(LocalDateTime.of(2014, 12, 01, 14, 03, 04), ZoneOffset.UTC));

		ExternalIdentifier currentIdentifier = new ExternalIdentifier();
		currentIdentifier.setKey("123ABD");
		currentIdentifier.setStartDate(ZonedDateTime.of(LocalDateTime.of(2014, 12, 01, 14, 03, 04), ZoneOffset.UTC));

		return asSet(prevIdentifier, currentIdentifier);
	}

	public Set<TradeCategory> businessCategories() {
		return asSet(TradeCategory.DINING__CASUAL_DINING, TradeCategory.DINING__QUICK_SERVICE);
	}

	public Set<TradeCategory> storeCategories() {
		return asSet(TradeCategory.DINING__SPORTS_BARS, TradeCategory.DINING__FINE_DINING);
	}

	public Set<TradeCategory> updatedTradeCategories() {
		return asSet(TradeCategory.ENTERTAINMENT__ENTERTAINMENT);
	}
}
