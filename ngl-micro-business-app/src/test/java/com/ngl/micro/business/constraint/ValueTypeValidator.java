package com.ngl.micro.business.constraint;

import org.hibernate.validator.cfg.ConstraintMapping;

import com.ngl.micro.business.shared.ValueTypeValidationMappings;
import com.ngl.middleware.rest.server.validation.ResourceValidator;

/**
 * Test class containing value types for validation.
 */
public class ValueTypeValidator extends ResourceValidator<Object> {

	public static ValueTypeValidator newInstance() {
		return ResourceValidator.configurator(new ValueTypeValidator()).configure();
	}

	private ValueTypeValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		ValueTypeValidationMappings.configure(mapping);
	}
}
