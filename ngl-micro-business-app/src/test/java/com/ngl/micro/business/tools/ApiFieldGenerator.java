package com.ngl.micro.business.tools;

import java.util.Arrays;

import com.ngl.micro.business.ImageSet;
import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.sale.CustomerSummary;
import com.ngl.micro.business.sale.Sale;
import com.ngl.micro.business.sale.SaleDemographics;
import com.ngl.micro.business.sale.SaleSummary;
import com.ngl.micro.business.store.Address;
import com.ngl.micro.business.store.ExternalIdentifier;
import com.ngl.micro.business.store.GeoLocation;
import com.ngl.micro.business.store.Store;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields.
 *
 * @author Willy du Preez
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Arrays.asList(
				PhoneNumber.class,
				ImageSet.class))
		.generate(Business.class);

		new ApiFieldsGenerator(Arrays.asList(
				PhoneNumber.class,
				ExternalIdentifier.class,
				Address.class,
				GeoLocation.class,
				ImageSet.class))
		.generate(Store.class);

		new ApiFieldsGenerator(Arrays.asList(SaleDemographics.class)).generate(Sale.class);
		new ApiFieldsGenerator(Arrays.asList(CustomerSummary.class)).generate(SaleSummary.class);
	}

}
