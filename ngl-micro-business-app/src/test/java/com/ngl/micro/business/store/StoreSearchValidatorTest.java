package com.ngl.micro.business.store;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.SearchQuery.Condition;
import com.ngl.middleware.test.api.ExpectedApplicationException;

public class StoreSearchValidatorTest {

	private static List<Condition> conditions;

	@BeforeClass
	public static void beforeClass() {
		conditions = Collections.unmodifiableList(SearchQueries.q(Store.LocationFields.LAT).eq(0.5)
				.and(Store.LocationFields.LON).eq(0.5)
				.and(Store.LocationFields.RADIUS).eq(7)
				.getConditions());
	}

	private StoreSearchValidator validator;
	private SearchQuery baseQuery;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Before
	public void before() {
		this.validator = new StoreSearchValidator();
		this.baseQuery = SearchQueries.q("a").eq("a")
				.or("b").eq("b")
				.getSearchQuery();
	}

	@Test
	public void test_null() {
		this.validator.validate(null);
	}

	@Test
	public void test_valid() {
		this.validator.validate(this.baseQuery);
		this.validator.validate(this.baseQuery.addConditions(conditions));
		this.validator.validate(new SearchQuery().addConditions(conditions));
	}

	@Test
	public void test_invalid_presence_duplicates() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR);
		this.validator.validate(new SearchQuery().addConditions(conditions).addConditions(conditions.get(0)));
	}

	@Test
	public void test_invalid_presence_missing() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR);
		this.validator.validate(new SearchQuery().addConditions(conditions.get(1)));
	}

	@Test
	public void test_invalid_position() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR);
		this.validator.validate(new SearchQuery().addConditions(conditions).addConditions(this.baseQuery.getConditions()));
	}

}
