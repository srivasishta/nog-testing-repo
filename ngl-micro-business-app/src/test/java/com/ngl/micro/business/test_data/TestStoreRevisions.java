package com.ngl.micro.business.test_data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.DSLContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.business.dal.generated.jooq.JNglMicroBusiness;
import com.ngl.micro.business.revision.jooq.JVersionControlService;
import com.ngl.micro.business.store.Store;
import com.ngl.micro.business.store.StoreRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.json.ObjectMapperFactory;
import com.ngl.middleware.test.data.Data;

/**
 * Store revision dataset
 *
 * @author Paco Mendes
 */
public class TestStoreRevisions implements JooqTestDataSet {

	public static final UUID GROUP_ID = UUID.fromString("79c6f9d9-647e-493f-b3f9-fe89e9993c4c");

	private static ObjectMapper mapper = new ObjectMapperFactory().getInstance();

	private StoreRepository stores;
	private JVersionControlService<Store> revisions;

	@Override
	public void loadData(DSLContext sql) {
		this.stores = new StoreRepository(sql.configuration());
		this.revisions = new JVersionControlService<>(
				mapper, JNglMicroBusiness.NGL_MICRO_BUSINESS.getName(), "store_revision", new DAL(sql.configuration()));

		UUID storeId = TestStores.STORE_4_ID;
		Store baseStore = this.stores.get(storeId);

		TestAccessToken token = TestAccessTokens.full();
		this.revisions.revise(
				Data.T_MINUS_10M,
				new Revision(),
				new Store(),
				baseStore,
				token.getClientIdClaim(),
				token.getSubjectClaim());

		this.updateRate(storeId, "0.0", Data.T_MINUS_1M, token);
		this.updateRate(storeId, "0.1", Data.T_0, token);
	}

	private void updateRate(UUID storeId, String rate, ZonedDateTime date, TestAccessToken token) {
		Store base = this.stores.get(storeId);
		Store updated = this.stores.get(storeId);
		updated.setContributionRate(new BigDecimal(rate));
		this.stores.update(updated);
		this.revisions.revise(
				date,
				new Revision(),
				base,
				updated,
				token.getClientIdClaim(),
				token.getSubjectClaim());
	}
}