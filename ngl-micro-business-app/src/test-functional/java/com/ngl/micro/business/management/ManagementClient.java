package com.ngl.micro.business.management;

import com.ngl.micro.shared.contracts.business.StoreLookupRequest;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Methods;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.client.http.response.RestResponse;

/**
 * The Member Service Client.
 *
 * @author Paco Mendes
 */
public class ManagementClient extends AbstractRestClient {

	public ManagementClient(RestClientConfig config) {
		super(config);
	}

	public StoreLookupResponse lookup(StoreLookupRequest lookup) {

		Request request = ManagementClient.this.http.request(this.serviceUrl + "lookup")
			.method(Methods.POST)
			.header(Headers.CONTENT_TYPE, MediaTypes.APPLICATION_JSON)
			.body(lookup);

		RestResponse response = request.execute(RestResponse.class);

		if (response.getStatus() == ApplicationStatus.OK.code()) {
			return response.bodyAsPojo(StoreLookupResponse.class);
		} else {
			ApplicationError error = response.bodyAsPojo(ApplicationError.class);
			throw new ApplicationException(error, error.getDetail());
		}
	}

}
