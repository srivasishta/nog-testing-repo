package com.ngl.micro.business.store;

import static com.ngl.micro.business.test_data.TestStores.STORE_4_ID;
import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_WRITE_SCOPE;
import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.business.AbstractBusinessFTCase;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.client.BusinessClient.BusinessResource;
import com.ngl.micro.business.client.BusinessClient.StoreResource;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.business.test_data.TestAccessTokens;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

public class StoreFTCase extends AbstractBusinessFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestBusinesses businesses;
	private TestStores stores;

	public StoreFTCase() {
		this.businesses = new TestBusinesses();
		this.stores = new TestStores();
	}

	@Test
	public void test_store_create_read_update() {

		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		Business business = this.client.getBusinessResource().create(this.businesses.business()).getState();

		StoreResource resource = this.client.getStoreResource();

		Store store = this.stores.store(null);
		store.setBusinessId(business.getId());
		store = resource.create(store, new Revision("test-user", "Testing.")).getState();
		UUID storeId = store.getId();
		Assert.assertNotNull(storeId);

		Store reference = resource.get(store.getId()).getState();

		String updatedName = "Updated Name";
		StoreStatus updatedStatus = StoreStatus.INACTIVE;
		String updatedReason = "suspended";

		store.setName(updatedName);
		store.setStatus(updatedStatus);
		store.setStatusReason(updatedReason);

		Patch patch = new Diff().diff(store, reference);

		resource.patch(storeId, patch);

		Store updated = resource.get(storeId).getState();
		assertEquals(updatedName, updated.getName());
		assertEquals(updatedStatus, updated.getStatus());
		assertEquals(updatedReason, updated.getStatusReason());

		SearchQuery query = SearchQueries.q(Store.Fields.BUSINESS_ID).in(store.getBusinessId())
				.and(Store.LocationFields.LAT).eq(TestStores.TIMES_SQUARE_LAT_RAD)
				.and(Store.LocationFields.LON).eq(TestStores.TIMES_SQUARE_LON_RAD)
				.and(Store.LocationFields.RADIUS).eq(5)
				.getSearchQuery();
		PageRequest page = PageRequest.with(0, 10).search(query).build();
		resource.list(page);
	}

	@Test
	public void test_create_store_with_duplicate_identifier() {

		this.cmdLoadTestDataSet(this.businesses);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		StoreResource resource = this.client.getStoreResource();

		Store store4 = this.stores.store(Data.ID_4);
		Store store5 = this.stores.store(Data.ID_5);

		ExternalIdentifier sid = new ExternalIdentifier();
		sid.setStartDate(Data.T_PLUS_1d);
		sid.setKey("abc");
		store4.setExternalIdentifiers(asSet(sid));
		store5.setExternalIdentifiers(asSet(sid));

		resource.create(store4).getState();

		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("key", ValidationErrorCode.INVALID);

		resource.create(store5).getState();
	}

	@Test
	public void test_manage_external_store_identifiers() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		String id1Key = "ID1";
		String id2Key = "ID2";
		String id3Key = "ID3";

		ExternalIdentifier id1 = new ExternalIdentifier();
		id1.setKey(id1Key);
		ExternalIdentifier id2 = new ExternalIdentifier();
		id2.setKey(id2Key);
		id2.setEndDate(Data.T_PLUS_1M);
		ExternalIdentifier id3 = new ExternalIdentifier();
		id3.setKey(id3Key);
		id3.setEndDate(Data.T_PLUS_5M);

		Business business = this.client.getBusinessResource().create(this.businesses.business()).getState();

		Store store = this.stores.store();
		store.setBusinessId(business.getId());
		store.setExternalIdentifiers(asSet(id1));

		//Create with id
		StoreResource resource = this.client.getStoreResource();
		store = resource.create(store).getState();
		UUID storeId = store.getId();

		Set<ExternalIdentifier> ids = store.getExternalIdentifiers();
		assertEquals(1, ids.size());
		this.assertContains(id1Key, ids);
		assertEquals(null, ids.stream().filter(id -> id.getKey().equals(id1Key)).findFirst().get().getEndDate());

		//Add
		Store working = resource.get(store.getId()).getState();
		working.getExternalIdentifiers().addAll(asSet(id2, id3));

		Patch patch = new Diff().diff(working, store);

		resource.patch(storeId, patch);
		store = resource.get(storeId).getState();
		ids = store.getExternalIdentifiers();
		assertEquals(3, ids.size());
		this.assertContains(id1Key, ids);
		this.assertContains(id2Key, ids);
		this.assertContains(id3Key, ids);
		assertEquals(Data.T_PLUS_1M, ids.stream().filter(id -> id.getKey().equals(id2Key)).findFirst().get().getEndDate());
		assertEquals(Data.T_PLUS_5M, ids.stream().filter(id -> id.getKey().equals(id3Key)).findFirst().get().getEndDate());

		//Remove
		working = resource.get(store.getId()).getState();
		working.getExternalIdentifiers().removeIf(i -> i.getKey().equals(id2Key));
		patch = new Diff().diff(working, store);
		resource.patch(storeId, patch);
		store = resource.get(storeId).getState();
		ids = store.getExternalIdentifiers();
		assertEquals(2, ids.size());
		this.assertContains(id1Key, ids);
		this.assertContains(id3Key, ids);

		//Update
		working = resource.get(store.getId()).getState();
		for (ExternalIdentifier id : working.getExternalIdentifiers()) {
			if (id.getKey().equals(id3Key)) {
				id.setEndDate(Data.T_PLUS_1d);
			}
		}
		patch = new Diff().diff(working, store);
		resource.patch(storeId, patch);
		store = resource.get(storeId).getState();
		ids = store.getExternalIdentifiers();
		assertEquals(2, ids.size());
		this.assertContains(id1Key, ids);
		this.assertContains(id3Key, ids);
		assertEquals(Data.T_PLUS_1d, ids.stream().filter(id -> id.getKey().equals(id3Key)).findFirst().get().getEndDate());
	}

	private void assertContains(String key, Set<ExternalIdentifier> identifiers) {
		assertTrue(identifiers.stream().anyMatch(id -> id.getKey().equals(key)));
	}

	@Test
	public void test_manage_store_keywords_categories_and_status() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		BusinessResource businessResource = this.client.getBusinessResource();
		StoreResource storeResource = this.client.getStoreResource();

		Business business = this.businesses.business();
		business.setStatus(BusinessStatus.INACTIVE);
		business = businessResource.create(business).getState();

		Store store = this.stores.store();
		store.setBusinessId(business.getId());
		store.setStatus(StoreStatus.ACTIVE);
		store = storeResource.create(store).getState();

		assertEquals("Equal business categories", business.getCategories(), store.getBusinessCategories());
		assertEquals("Equal business keywords", business.getKeywords(), store.getBusinessKeywords());
		assertEquals("Expect unchanged status", StoreStatus.INACTIVE, store.getStatus());

		UUID storeId = store.getId();

		Store reference = storeResource.get(storeId).getState();
		TradeCategory existingBusinessCategory = business.getCategories().stream().findFirst().get();
		store.getStoreCategories().add(existingBusinessCategory);
		String existingBusinessKeyword = business.getKeywords().stream().findFirst().get();
		store.getStoreKeywords().add(existingBusinessKeyword);

		Patch patch = new Diff().diff(store, reference);
		storeResource.patch(storeId, patch);
		Store updated = storeResource.get(storeId).getState();

		assertEquals("Expected unchanged categories", reference.getStoreCategories(), updated.getStoreCategories());
		assertEquals("Expected unchanged keywords", reference.getStoreKeywords(), updated.getStoreKeywords());
		assertEquals("Expect unchanged status", StoreStatus.INACTIVE, store.getStatus());

		Business referenceBusiness = businessResource.get(business.getId()).getState();
		business.setStatus(BusinessStatus.ACTIVE);
		Patch businessPatch = new Diff().diff(business, referenceBusiness);
		businessResource.patch(business.getId(), businessPatch);

		TradeCategory newCategory = TradeCategory.AUTOMOTIVE__AUTO__REPAIR;
		String newKeyword = "new keyword";
		store.getStoreCategories().add(newCategory);
		store.getStoreKeywords().add(newKeyword);
		store.setStatus(StoreStatus.ACTIVE);

		patch = new Diff().diff(store, reference);
		storeResource.patch(storeId, patch);
		updated = storeResource.get(storeId).getState();

		assertTrue("Expected new category", updated.getStoreCategories().contains(newCategory));
		assertTrue("Expected new keyword", updated.getStoreKeywords().contains(newKeyword));
		assertEquals("Expect updated status", StoreStatus.ACTIVE, updated.getStatus());
	}

	@Test
	public void test_unauthorised_store_create() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_READ_SCOPE));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().create(this.stores.store());
	}

	@Test
	public void test_unauthorised_store_create_for_different_partner() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());
		Business business = this.client.getBusinessResource().create(this.businesses.business()).getState();
		Store store = this.stores.store();
		store.setBusinessId(business.getId());
		store = this.client.getStoreResource().create(store).getState();

		this.accessTokenProvider.setAccessToken(TestAccessTokens.full().subjectClaim(Data.ID_6));
		this.client.getStoreResource().get(store.getId());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().create(store);
	}

	@Test
	public void test_unauthorised_store_update_for_different_partner() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());
		Business business = this.client.getBusinessResource().create(this.businesses.business()).getState();
		Store store = this.stores.store();
		store.setBusinessId(business.getId());
		store = this.client.getStoreResource().create(store).getState();

		this.accessTokenProvider.setAccessToken(TestAccessTokens.full().subjectClaim(Data.ID_6));
		this.client.getStoreResource().get(store.getId());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().patch(store.getId(), new Patch(new ArrayList<>()));
	}

	@Test
	public void test_unauthorised_store_read() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_WRITE_SCOPE));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().get(STORE_4_ID);
	}

	@Test
	public void test_unauthorised_store_update() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_READ_SCOPE));
		this.client.getStoreResource().get(STORE_4_ID).getState();

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().patch(STORE_4_ID, new Patch(new ArrayList<>()));
	}

	@Test
	public void test_store_revisions() {

		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		Business business = this.client.getBusinessResource().create(this.businesses.business()).getState();
		StoreResource resource = this.client.getStoreResource();
		Store store = this.stores.store(null);
		store.setBusinessId(business.getId());
		store = resource.create(store).getState();
		UUID storeId = store.getId();

		Store base = resource.get(storeId).getState();

		store.setName(store.getName() + "1");
		store.setContributionRate(store.getContributionRate().add(new BigDecimal("0.1")));

		Diff diff = new Diff();
		Merge merge = new Merge();

		Patch patch = diff.diff(store, base);

		resource.patch(storeId, patch);

		Page<Revision> revisions = resource
				.getRevisions(storeId, PageRequest.with(0, 100).build())
				.getState();
		Assert.assertEquals(2, revisions.getTotalItems());

		Store rebuild = new Store();
		for (Revision rev : revisions.getItems()) {
			merge.merge(rev.getAppliedPatch(), rebuild);
			Revision fetched = resource.getRevision(storeId, rev.getId()).getState();
			Assert.assertNotNull(fetched);
		}
		Assert.assertEquals(store, rebuild);
	}

	@Test
	public void test_unauthorised_revision_get() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_WRITE_SCOPE));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().getRevision(TestStores.STORE_4_ID, UUID.randomUUID());
	}

	@Test
	public void test_unauthorised_revision_list() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_WRITE_SCOPE));
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getStoreResource().getRevisions(TestStores.STORE_4_ID, PageRequest.with(0, 1).build());
	}

}