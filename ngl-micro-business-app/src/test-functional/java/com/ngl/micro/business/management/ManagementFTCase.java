package com.ngl.micro.business.management;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.math.BigDecimal;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.BusinessApplication;
import com.ngl.micro.business.BusinessFTCaseConfiguration;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.business.StoreLookupRequest;
import com.ngl.micro.shared.contracts.business.StoreLookupResponse;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		BusinessFTCaseConfiguration.class
})
@Ignore
public class ManagementFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestBusinesses testBusinesses;
	private TestStores testStores;

	@Autowired
	private ManagementClient client;

	public ManagementFTCase() {
		super(BusinessApplication.class);
		this.testBusinesses = new TestBusinesses();
		this.testStores = new TestStores();
	}

	@Test
	public void test_lookup_store_by_external_id() {
		this.cmdLoadTestDataSet(this.testBusinesses);
		this.cmdLoadTestDataSet(this.testStores);

		StoreLookupResponse response = this.client.lookup(new StoreLookupRequest(TestStores.EXTERNAL_ID_A, Data.T_0));

		assertEquals(true, response.isMatched());
		assertEquals(TestStores.STORE_4_ID, response.getStoreId());
		assertEquals(ResourceStatus.ACTIVE, response.getStoreStatus());
		assertEquals(TestBusinesses.BUSINESS_ID, response.getBusinessId());
		assertEquals(ResourceStatus.ACTIVE, response.getBusinessStatus());
		assertEquals(new BigDecimal("0.10000"), response.getContributionRate());

		response = this.client.lookup(new StoreLookupRequest(TestStores.EXTERNAL_ID_A, Data.T_MINUS_1M));

		assertEquals(true, response.isMatched());
		assertEquals(TestStores.STORE_4_ID, response.getStoreId());
		assertEquals(ResourceStatus.INACTIVE, response.getStoreStatus());
		assertEquals(TestBusinesses.BUSINESS_ID, response.getBusinessId());
		assertEquals(ResourceStatus.INACTIVE, response.getBusinessStatus());
		assertEquals(new BigDecimal("0.00000"), response.getContributionRate());

		response = this.client.lookup(new StoreLookupRequest("no match", Data.T_0));
		assertFalse("Unmatched external id", response.isMatched());
	}

}
