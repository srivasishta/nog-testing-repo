package com.ngl.micro.business.business;

import static com.ngl.micro.business.test_data.TestBusinesses.BUSINESS_ID;
import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.BUSINESS_WRITE_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.SALE_READ_SCOPE;
import static com.ngl.micro.shared.contracts.oauth.Scopes.STORE_READ_SCOPE;
import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ngl.micro.business.AbstractBusinessFTCase;
import com.ngl.micro.business.PhoneNumber;
import com.ngl.micro.business.PhoneNumber.PhoneNumberType;
import com.ngl.micro.business.TradeCategory;
import com.ngl.micro.business.business.Business.BusinessStatus;
import com.ngl.micro.business.client.BusinessClient;
import com.ngl.micro.business.client.BusinessClient.BusinessResource;
import com.ngl.micro.business.client.BusinessClient.StoreResource;
import com.ngl.micro.business.sale.Sale;
import com.ngl.micro.business.sale.SaleSummary;
import com.ngl.micro.business.store.Store;
import com.ngl.micro.business.store.Store.StoreStatus;
import com.ngl.micro.business.test_data.TestAccessTokens;
import com.ngl.micro.business.test_data.TestBusinesses;
import com.ngl.micro.business.test_data.TestSales;
import com.ngl.micro.business.test_data.TestStores;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

public class BusinessFTCase extends AbstractBusinessFTCase {

	private TestBusinesses businesses;
	private TestStores stores;
	private TestSales sales;

	@Autowired
	private BusinessClient client;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	public BusinessFTCase() {
		this.businesses = new TestBusinesses();
		this.stores = new TestStores();
		this.sales = new TestSales();
	}

	@Test
	public void test_business_create_read_update() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		BusinessResource resource = this.client.getBusinessResource();

		String createdReason = "created";

		Business business = this.businesses.business(null);
		business.setStatus(BusinessStatus.ACTIVE);
		business.setStatusReason(createdReason);

		business = resource.create(business).getState();
		UUID businessId = business.getId();

		assertNotNull(businessId);
		assertEquals(BusinessStatus.ACTIVE, business.getStatus());
		assertEquals(createdReason, business.getStatusReason());

		Business reference = resource.get(business.getId()).getState();

		String updatedName = "Updated Name";
		String updatedReason = "updated";

		business.setName(updatedName);
		business.setStatus(BusinessStatus.INACTIVE);
		business.setStatusReason(updatedReason);

		PhoneNumber tel = new PhoneNumber();
		tel.setType(PhoneNumberType.TEL);
		tel.setCountryCode("1");
		tel.setAreaCode("780");
		tel.setNumber("4751202");
		tel.setExtension("251");
		business.setPhoneNumbers(Collections.asSet(tel));

		Patch patch = new Diff().diff(business, reference);

		resource.patch(businessId, patch);

		Business updated = resource.get(businessId).getState();
		assertEquals(updatedName, updated.getName());
		assertEquals(BusinessStatus.INACTIVE, updated.getStatus());
		assertEquals(updatedReason, updated.getStatusReason());
		assertEquals(Collections.asSet(tel), updated.getPhoneNumbers());
	}

	@Test
	public void test_manage_business_categories() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		BusinessResource businessResource = this.client.getBusinessResource();
		StoreResource storeResource = this.client.getStoreResource();
		Set<TradeCategory> businessCategories = asSet(TradeCategory.APPAREL__CHILDREN, TradeCategory.APPAREL__MENS, TradeCategory.APPAREL__WOMEN);

		Business business = this.businesses.business();
		business.setCategories(businessCategories);
		business = businessResource.create(business).getState();

		Store store = this.stores.store();
		store.setBusinessId(business.getId());
		store.setStoreCategories(businessCategories);
		store = storeResource.create(store).getState();

		assertEquals("Expected no duplicate categories added to store", 0, store.getStoreCategories().size());
		assertEquals("Expected business categories assigned", businessCategories, store.getBusinessCategories());

		Set<TradeCategory> storeCategories = asSet(TradeCategory.APPAREL__GENERAL, TradeCategory.APPAREL__CLEANING, TradeCategory.APPAREL__FOOTWEAR);

		store.setStoreCategories(storeCategories);
		Store refStore = storeResource.get(store.getId()).getState();
		Patch patch = new Diff().diff(store, refStore);
		store = storeResource.patch(store.getId(), patch).getState();

		assertEquals("Expected new categories added to store", storeCategories, store.getStoreCategories());
		assertEquals("Expected business categories still assigned", businessCategories, store.getBusinessCategories());

		Business refBusiness = businessResource.get(business.getId()).getState();
		business.setCategories(storeCategories);
		patch = new Diff().diff(business, refBusiness);
		business = businessResource.patch(business.getId(), patch).getState();
		store = storeResource.get(store.getId()).getState();

		assertEquals("Expected new categories added to business", storeCategories, business.getCategories());
		assertEquals("Expected categories unasigned from store", 0, store.getStoreCategories().size());
		assertEquals("Expected new business categories still assigned", storeCategories, store.getBusinessCategories());
	}

	@Test
	public void test_manage_business_keywords_categories_and_status() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.full());

		BusinessResource businessResource = this.client.getBusinessResource();
		StoreResource storeResource = this.client.getStoreResource();
		TradeCategory businessCategory = TradeCategory.APPAREL__CHILDREN;
		String businessKeyword = "juice";

		Business business = this.businesses.business();
		business.setStatus(BusinessStatus.ACTIVE);
		business.setCategories(new HashSet<>());
		business = businessResource.create(this.businesses.business()).getState();
		Business reference = businessResource.get(business.getId()).getState();

		Store store = this.stores.store();
		store.setStatus(StoreStatus.ACTIVE);
		store.setBusinessId(business.getId());
		store.setStoreCategories(asSet(businessCategory));
		store.setStoreKeywords(asSet(businessKeyword));
		store = storeResource.create(store).getState();

		String inactiveReason = "suspended";
		business.setStatus(BusinessStatus.INACTIVE);
		business.setStatusReason(inactiveReason);
		business.getCategories().add(businessCategory);
		business.getKeywords().add(businessKeyword);
		Patch patch = new Diff().diff(business, reference);
		businessResource.patch(business.getId(), patch);

		business = businessResource.get(business.getId()).getState();
		store = storeResource.get(store.getId()).getState();

		assertTrue("Expected new business category", business.getCategories().contains(businessCategory));
		assertTrue("Expect store category removed", !store.getStoreCategories().contains(businessCategory));
		assertTrue("Expect store business category", store.getBusinessCategories().contains(businessCategory));

		assertTrue("Expected new business keyword", business.getKeywords().contains(businessKeyword));
		assertTrue("Expect store keyword removed", !store.getStoreKeywords().contains(businessKeyword));
		assertTrue("Expect store business keyword", store.getBusinessKeywords().contains(businessKeyword));

		assertEquals("Expected store status update", StoreStatus.INACTIVE, store.getStatus());
		assertEquals("Expected store status update", inactiveReason, store.getStatusReason());
	}

	@Test
	public void test_unauthorised_business_create() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.none());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getBusinessResource().create(this.businesses.business());
	}

	@Test
	public void test_unauthorised_business_read() {
		this.cmdLoadTestDataSet(this.businesses);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.none());

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getBusinessResource().get(BUSINESS_ID);
	}

	@Test
	public void test_unauthorised_business_update() {
		this.cmdLoadTestDataSet(this.businesses);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(BUSINESS_READ_SCOPE));

		Business business = this.client.getBusinessResource().get(BUSINESS_ID).getState();
		assertNotNull(business);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getBusinessResource().patch(BUSINESS_ID, new Patch(new ArrayList<>()));
	}

	@Test
	public void test_unauthorised_business_update_no_ownership() {
		this.cmdLoadTestDataSet(this.businesses);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(BUSINESS_READ_SCOPE, BUSINESS_WRITE_SCOPE).subjectClaim(Data.ID_5));

		Business business = this.client.getBusinessResource().get(BUSINESS_ID).getState();
		assertNotNull(business);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getBusinessResource().patch(BUSINESS_ID, new Patch(new ArrayList<>()));
	}

	@Test
	public void test_business_sales() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(SALE_READ_SCOPE));

		PageRequest page = PageRequest.with(0, 10).build();
		Page<Sale> sales = this.client.getBusinessResource().sales(BUSINESS_ID, page).getState();
		assertNotNull(sales);
		assertTrue(sales.getItems().size() > 0);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(BUSINESS_READ_SCOPE));

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getBusinessResource().sales(BUSINESS_ID, page);
	}

	@Test
	public void test_business_sales_summary() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(SALE_READ_SCOPE));

		SaleSummary summary = this.client.getBusinessResource().summary(BUSINESS_ID).getState();
		assertNotNull(summary);
		assertTrue(summary.getTotalSales() > 0);
		assertTrue(summary.getTotalContributed().signum() > 0);
		assertTrue(summary.getTotalAmount().signum() > 0);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_READ_SCOPE));

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getBusinessResource().summary(BUSINESS_ID);
	}

	@Test
	public void test_business_sales_summary_filter() {
		this.cmdLoadTestDataSet(this.businesses);
		this.cmdLoadTestDataSet(this.stores);
		this.cmdLoadTestDataSet(this.sales);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(SALE_READ_SCOPE));

		SearchQuery query = SearchQueries
			.q(Sale.Fields.DEMOGRAPHICS_GENDER).eq("MALE")
			.and(Sale.Fields.STORE_ID).eq(Data.ID_4)
			.and(Sale.Fields.SALE_AMOUNT).ge(Data.AMOUNT_10_00)
			.and(Sale.Fields.DEMOGRAPHICS_BIRTH_DATE).ge(Data.T_0.minusYears(25).toLocalDate())
			.getSearchQuery();

		SaleSummary summary = this.client.getBusinessResource().summary(BUSINESS_ID, query).getState();
		assertNotNull(summary);
		assertTrue(summary.getTotalSales() > 0);
		assertTrue(summary.getTotalContributed().signum() > 0);
		assertTrue(summary.getTotalAmount().signum() > 0);

		this.accessTokenProvider.setAccessToken(TestAccessTokens.withScope(STORE_READ_SCOPE));

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getBusinessResource().summary(BUSINESS_ID);
	}

}
