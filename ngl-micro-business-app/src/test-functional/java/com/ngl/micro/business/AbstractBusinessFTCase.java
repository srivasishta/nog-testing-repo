package com.ngl.micro.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.business.client.BusinessClient;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;

@ContextConfiguration(classes = {
	DefaultDatabaseTestConfiguration.class,
	BusinessFTCaseConfiguration.class
})
public abstract class AbstractBusinessFTCase extends AbstractApplicationFTCase {

	@Autowired
	protected BusinessClient client;

	public AbstractBusinessFTCase() {
		super(BusinessApplication.class);
	}
}