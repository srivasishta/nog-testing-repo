package com.ngl.micro.charity.charity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityStatusTypeDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

/**
 * Validate status type mappings.
 *
 * @author Paco Mendes
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StatusTypeITCase extends AbstractDatabaseITCase {

	private JCharityStatusTypeDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JCharityStatusTypeDao(this.sqlContext);
	}

	@Test
	public void test_status_mapping() throws Exception {
		assertEquals(ResourceStatus.ACTIVE.name(), CharityStatus.ACTIVE.name());
		assertEquals(ResourceStatus.INACTIVE.name(), CharityStatus.INACTIVE.name());
		JCharityStatusType status;
		assertEquals("Expect equal status type count.", ResourceStatus.values().length, this.statusDao.count());
		for (ResourceStatus type : ResourceStatus.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
