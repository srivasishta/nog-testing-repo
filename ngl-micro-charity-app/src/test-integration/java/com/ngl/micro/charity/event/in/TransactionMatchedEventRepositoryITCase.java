package com.ngl.micro.charity.event.in;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.event.in.transaction.TransactionMatchedEventHistory;
import com.ngl.micro.charity.event.in.transaction.TransactionMatchedEventRepository;
import com.ngl.micro.charity.test_data.TestTransactionMatchedEvents;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

/**
 * {@link TransactionMatchedEventRepository} tests.
 *
 * @author Willy du Preez
 *
 */
@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class TransactionMatchedEventRepositoryITCase extends AbstractDatabaseITCase {

	private TransactionMatchedEventRepository repo;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new TransactionMatchedEventRepository(this.sqlContext);
	}

	@Test
	public void test_transaction_matched_events() {
		TransactionMatchedEvent event = TestTransactionMatchedEvents.newEvent();

		event = this.repo.addEvent(event);

		Assert.assertEquals(TestTransactionMatchedEvents.newEvent(), event);

		TransactionMatchedEventHistory history = this.repo.getEventHistory(event.getId()).get();

		Assert.assertEquals(event.getId(), history.getId());
		Assert.assertEquals(event.getCreated(), history.getCreated());
//		Assert.assertEquals(event.getDocument().getId(), history.getDocumentId());
	}

}