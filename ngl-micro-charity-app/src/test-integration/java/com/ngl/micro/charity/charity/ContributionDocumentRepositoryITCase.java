package com.ngl.micro.charity.charity;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.contribution.ContributionDocumentRepository;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.micro.charity.test_data.TestContributionAllocations;
import com.ngl.micro.charity.test_data.TestContributionDocuments;
import com.ngl.micro.contribution.ContributionSearchFields;
import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.page.SearchQuery.ComparisonOperator;
import com.ngl.middleware.rest.api.page.SearchQuery.Condition;
import com.ngl.middleware.rest.api.page.SearchQuery.LogicalOperator;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class ContributionDocumentRepositoryITCase extends AbstractDatabaseITCase {

	private ContributionDocumentRepository repo;
	private TestCharities testCharities;
	private TestContributionAllocations testContributionAlloctions;
	private TestContributionDocuments testContributionDocuments;

	@Override
	@Before
	public void before() {
		super.before();
		this.repo = new ContributionDocumentRepository(this.sqlContext);
		this.testCharities = new TestCharities();
		this.testContributionAlloctions = new TestContributionAllocations();
		this.testContributionDocuments = new TestContributionDocuments();
	}

	@Test
	public void test_add_document() {
		this.cmdLoadTestDataSet(this.testCharities);
		ContributionDocument document = TestContributionDocuments.newDocument();

		document = this.repo.saveDocument(document);

		Assert.assertEquals(TestContributionDocuments.newDocument(), document);

		document = this.repo.getDocument(document.getId()).get();

		Assert.assertEquals(TestContributionDocuments.newDocument(), document);
	}

	@Test
	public void test_get_empty_summary_by_category(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = SearchQueries.and(
				ContributionSearchFields.CONTRIBUTION_DATE).gt(Data.T_PLUS_1y).getSearchQuery();

		Map<CharityCategory, BigDecimal> contributions = this.repo.getTotalsByCategory(searchQuery);

		Assert.assertEquals(CharityCategory.values().length, contributions.size());

		Assert.assertEquals(new BigDecimal("0.0"), contributions.get(CharityCategory.EDUCATION));
		Assert.assertEquals(new BigDecimal("0.0"), contributions.get(CharityCategory.INCLUSION));
		Assert.assertEquals(new BigDecimal("0.0"), contributions.get(CharityCategory.HEALTH));
		Assert.assertEquals(new BigDecimal("0.0"), contributions.get(CharityCategory.ENVIRONMENT));
	}

	@Test
	public void test_get_summary_by_category(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = new SearchQuery();

		Map<CharityCategory, BigDecimal> contributions = this.repo.getTotalsByCategory(searchQuery);

		Assert.assertEquals(CharityCategory.values().length, contributions.size());

		Assert.assertEquals(new BigDecimal("42.00000"), contributions.get(CharityCategory.EDUCATION));
		Assert.assertEquals(new BigDecimal("31.00000"), contributions.get(CharityCategory.INCLUSION));
		Assert.assertEquals(new BigDecimal("21.00000"), contributions.get(CharityCategory.HEALTH));
		Assert.assertEquals(new BigDecimal("20.00000"), contributions.get(CharityCategory.ENVIRONMENT));
	}

	@Test
	public void test_get_summary_by_category_filtered(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = new SearchQuery();
		searchQuery.addConditions(new Condition(
				LogicalOperator.AND,
				ContributionSearchFields.CONTRIBUTION_DATE,
				ComparisonOperator.LT,
				"%" + Data.T_MINUS_1d.format(DateTimeFormatter.ISO_ZONED_DATE_TIME) + "%"));

		Map<CharityCategory, BigDecimal> contributions = this.repo.getTotalsByCategory(searchQuery);

		Assert.assertEquals(CharityCategory.values().length, contributions.size());

		Assert.assertEquals(new BigDecimal("22.00000"), contributions.get(CharityCategory.EDUCATION));
		Assert.assertEquals(new BigDecimal("16.00000"), contributions.get(CharityCategory.INCLUSION));
		Assert.assertEquals(new BigDecimal("11.00000"), contributions.get(CharityCategory.HEALTH));
		Assert.assertEquals(new BigDecimal("10.00000"), contributions.get(CharityCategory.ENVIRONMENT));
	}

	@Test
	public void test_get_summary(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = new SearchQuery();

		BigDecimal summaryTotal = this.repo.getTotalContributions(searchQuery);

		Assert.assertEquals(summaryTotal, new BigDecimal("61.00000"));
	}

	@Test
	public void test_get_summary_filtered(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = new SearchQuery();
		searchQuery.addConditions(new Condition(
				LogicalOperator.AND,
				ContributionSearchFields.CONTRIBUTION_DATE,
				ComparisonOperator.LT,
				"%" + Data.T_MINUS_1d.format(DateTimeFormatter.ISO_ZONED_DATE_TIME) + "%"));

		BigDecimal summaryTotal = this.repo.getTotalContributions(searchQuery);

		Assert.assertEquals(summaryTotal, new BigDecimal("31.00000"));
	}

	@Test
	public void test_get_summary_for_charity(){
		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAlloctions);

		SearchQuery searchQuery = new SearchQuery();

		BigDecimal summaryTotal = this.repo.getTotalContributionsByCharityId(Data.ID_4, searchQuery);

		Assert.assertEquals(summaryTotal, new BigDecimal("8.00000"));
	}

}