package com.ngl.micro.charity.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCurrencyDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCurrency;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class CurrencyITCase extends AbstractDatabaseITCase {

	private JCurrencyDao statusDao;

	@Override
	public void before() {
		super.before();
		this.statusDao = new JCurrencyDao(this.sqlContext);
	}

	@Test
	public void test_currency_mapping() throws Exception {
		JCurrency status;
		assertEquals("Expect equal status type count.", Currency.values().length, this.statusDao.count());
		for (Currency type : Currency.values()) {
			status = this.statusDao.fetchOneByJValue(type.name());
			assertEquals("Expect type name equals persisted name", type.name(), status.getValue());
			assertEquals("Expect type id equals persisted id: ", type.getId(), status.getId().longValue());
		}
	}
}
