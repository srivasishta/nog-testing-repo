package com.ngl.micro.charity.charity;

import static com.ngl.micro.charity.Charity.CharityStatus.ACTIVE;
import static com.ngl.micro.charity.Charity.CharityStatus.INACTIVE;
import static com.ngl.micro.charity.test_data.TestCharities.CHARITY_2_PK;
import static com.ngl.middleware.test.data.Data.REASON_ACTIVATED;
import static com.ngl.middleware.test.data.Data.REASON_UPDATED;
import static org.junit.Assert.assertEquals;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class StatusHistoryRepositoryITCase extends AbstractDatabaseITCase {

	private StatusHistoryRepository repo;

	private TestCharities testCharities;

	@Before
	public void setup() {
		this.repo = new StatusHistoryRepository(new DAL(this.sqlContext));
		this.testCharities = new TestCharities();
	}

	@Test
	public void test_get_charity_status() {
		this.cmdLoadTestDataSet(this.testCharities);

		assertEquals(null, this.repo.getByCharityIdAt(TestCharities.CHARITY_2_PK, Data.T_MINUS_5y));
		assertEquals(ACTIVE, this.repo.getByCharityIdAt(TestCharities.CHARITY_2_PK, Data.T_MINUS_1y).getStatus());
		assertEquals(INACTIVE, this.repo.getByCharityIdAt(TestCharities.CHARITY_2_PK, Data.T_MINUS_5M).getStatus());
		assertEquals(ACTIVE, this.repo.getByCharityIdAt(TestCharities.CHARITY_2_PK, Data.T_MINUS_1M).getStatus());
		assertEquals(ACTIVE, this.repo.getByCharityIdAt(TestCharities.CHARITY_2_PK, Data.T_0).getStatus());
	}

	@Test
	public void test_get_null_status() {
		assertEquals(null, this.repo.getByCharityIdAt(Data.toLong(TestCharities.CHARITY_2_ID), Data.T_MINUS_1y));
	}

	@Test
	public void test_update_status() {
		this.cmdLoadTestDataSet(this.testCharities);

		Long charityId = CHARITY_2_PK;
		CharityStatusRecord status = this.repo.getByCharityIdAt(charityId, Data.T_0);
		int records = this.repo.list(charityId).size();

		assertEquals(ACTIVE, status.getStatus());
		assertEquals(Data.REASON_ACTIVATED, status.getStatusReason());

		//Unchanged
		this.repo.setCharityStatus(charityId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByCharityIdAt(charityId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(records, this.repo.list(charityId).size());

		//Status reason update
		this.repo.setCharityStatus(charityId, INACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByCharityIdAt(charityId, ZonedDateTime.now());
		assertEquals(INACTIVE, status.getStatus());
		assertEquals(REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(charityId).size());

		//Status only
		this.repo.setCharityStatus(charityId, ACTIVE, REASON_UPDATED, ZonedDateTime.now());
		status = this.repo.getByCharityIdAt(charityId, ZonedDateTime.now());
		assertEquals("status only update", ACTIVE, status.getStatus());
		assertEquals("status only update", REASON_UPDATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(charityId).size());

		//Reason only
		this.repo.setCharityStatus(charityId, ACTIVE, REASON_ACTIVATED, ZonedDateTime.now());
		status = this.repo.getByCharityIdAt(charityId, ZonedDateTime.now());
		assertEquals(ACTIVE, status.getStatus());
		assertEquals(REASON_ACTIVATED, status.getStatusReason());
		assertEquals(++records, this.repo.list(charityId).size());

		//Empty reason
		this.repo.setCharityStatus(charityId, ACTIVE, "", ZonedDateTime.now());
		status = this.repo.getByCharityIdAt(charityId, ZonedDateTime.now());
		assertEquals("", status.getStatusReason());
		assertEquals(++records, this.repo.list(charityId).size());
	}

}