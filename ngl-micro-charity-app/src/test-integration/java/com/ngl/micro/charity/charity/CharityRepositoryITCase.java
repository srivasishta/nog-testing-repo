package com.ngl.micro.charity.charity;
import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.Country;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class CharityRepositoryITCase extends AbstractDatabaseITCase {

	private CharityRepository repo;
	private TestCharities testCharities;

	@Before
	public void setup() {
		this.testCharities = new TestCharities();
		this.repo = new CharityRepository(new DAL(this.sqlContext));
	}

	@Test
	public void test_create_charity() {
		Charity charity = this.repo.add(this.testCharities.newCharity());

		assertEquals(TestCharities.NEW_CHARITY_ID, charity.getId());
		assertTrue(charity.getReach().getAreas().size() > 0);
		assertTrue(charity.getCategories().size() > 0);
		assertNull(charity.getLogoImages().getOriginal());
		assertNull(charity.getLogoImages().getLarge());
		assertNull(charity.getLogoImages().getMedium());
		assertNull(charity.getLogoImages().getThumbnail());
		assertNotNull(charity.getStatus());
	}

	@Test
	public void test_get_missing_charity() {
		assertNull(this.repo.get(Data.TYPE4_UUID_1));
	}

	@Test
	public void test_update_charity() {
		Charity reference = this.testCharities.newCharity();
		reference.setStatus(CharityStatus.ACTIVE);
		Charity working = this.repo.add(reference);
		working.setName("Updated Name");
		working.setDescription("Updated Description");
		working.setStatus(CharityStatus.INACTIVE);
		working.setReach(new Reach(ReachType.NATIONAL, asSet(new Area(Country.CA, asSet("AB", "BC", "SK")))));
		working.setCategories(asSet(CharityCategory.EDUCATION, CharityCategory.ENVIRONMENT));
		working.setLogoImages(reference.getLogoImages());
		this.repo.update(working);

		assertEquals(working, this.repo.get(working.getId()));
	}

	@Test
	public void test_list_charities() {
		this.cmdLoadTestDataSet(this.testCharities);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5).build());
		Assert.assertEquals(5, charities.getItems().size());
		charities.getItems().stream().forEach(charity -> {
			if (charity.getId().equals(new UUID(0,2))){
				//Check 1 area
				Assert.assertEquals(charity.getReach().getAreas().size(), 1);
				//Check 2 regions in single area
				charity.getReach().getAreas().forEach(area -> {
					Assert.assertEquals(area.getRegions().size(), 2);
					Assert.assertEquals(area.getCountry(), Country.US);
				});
			}
		});
	}

	@Test
	@Ignore("Group by all aggregate columns required on H2. Passes on MYSQL")
	public void test_filter_charities_by_category() {
		this.cmdLoadTestDataSet(this.testCharities);

		SearchQuery query = SearchQueries.q(Charity.Fields.CATEGORIES).eq(CharityCategory.EDUCATION).getSearchQuery();
		Fields fields = new Fields();
		fields.include(Charity.Fields.ID);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(charities.hasItems());
		for (Charity c : charities.getItems()) {
			Set<CharityCategory> charityCategories = this.repo.get(c.getId()).getCategories();
			Assert.assertTrue(charityCategories.contains(CharityCategory.EDUCATION));
		}
	}


	@Test
	public void test_get_charity_statuses_over_time() {
		this.cmdLoadTestDataSet(this.testCharities);

		validateStatus(TestCharities.CHARITY_2_ID, ResourceStatus.ACTIVE, Data.T_MINUS_1y);
		validateStatus(TestCharities.CHARITY_2_ID, ResourceStatus.INACTIVE, Data.T_MINUS_5M);
		validateStatus(TestCharities.CHARITY_2_ID, ResourceStatus.ACTIVE, Data.T_MINUS_1M);
		validateStatus(TestCharities.CHARITY_2_ID, ResourceStatus.ACTIVE, Data.T_0);

		Map<UUID, ResourceStatus> chars = this.repo.getCharityStatusesByResourceIdsAt(Collections.asSet(Data.ID_2, Data.ID_3, Data.ID_4), Data.T_0);
		Assert.assertEquals(3, chars.size());
		Assert.assertTrue(chars.containsKey(Data.ID_2));
		Assert.assertTrue(chars.containsKey(Data.ID_3));
		Assert.assertTrue(chars.containsKey(Data.ID_4));
	}

	private void validateStatus(UUID id, ResourceStatus expected, ZonedDateTime date) {
		Map<UUID, ResourceStatus> chars = this.repo.getCharityStatusesByResourceIdsAt(Collections.asSet(id), date);
		Assert.assertEquals(1, chars.size());
		Assert.assertEquals(expected, chars.get(id));
	}

	@Test
	public void test_filter_charities_by_country() {
		this.cmdLoadTestDataSet(this.testCharities);

		SearchQuery query = SearchQueries.q(Charity.ReachSearchFields.COUNTRY).eq(Country.CA).getSearchQuery();
		Fields fields = new Fields();
		fields.include(Charity.Fields.ID);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(charities.getNumberOfItems() == 2);
		for (Charity c : charities.getItems()) {
			boolean ca = c.getReach().getAreas().stream().anyMatch(a -> Country.CA.equals(a.getCountry()));
			Assert.assertTrue(ca);
		}
	}

	@Test
	@Ignore("Group by all aggregate columns required on H2. Passes on MYSQL")
	public void test_filter_charities_by_region() {
		this.cmdLoadTestDataSet(this.testCharities);

		SearchQuery query = SearchQueries.q(Charity.ReachSearchFields.COUNTRY).eq(Country.US)
				.and(Charity.ReachSearchFields.REGION).eq("CA")
				.getSearchQuery();
		Fields fields = new Fields();
		fields.include(Charity.Fields.ID);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(charities.getNumberOfItems() == 2);
		for (Charity c : charities.getItems()) {
			boolean ca = c.getReach().getAreas().stream().anyMatch(a -> a.getRegions().contains("CA"));
			Assert.assertTrue(ca);
		}
	}

	@Test
	public void test_filter_charities_by_category_and_region() {
		this.cmdLoadTestDataSet(this.testCharities);

		SearchQuery query = SearchQueries.q(Charity.Fields.CATEGORIES).eq(CharityCategoryType.EDUCATION)
				.and(Charity.ReachSearchFields.REGION).in("ON", "AB")
				.getSearchQuery();
		Fields fields = new Fields();
		fields.include(Charity.Fields.ID);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(charities.getNumberOfItems() == 1);
		for (Charity c : charities.getItems()) {
			boolean ca = c.getReach().getAreas().stream().anyMatch(
					a -> a.getRegions().contains("ON") || a.getRegions().contains("AB"));
			Assert.assertTrue(ca);
		}
		for (Charity c : charities.getItems()) {
			Set<CharityCategory> charityCategories = this.repo.get(c.getId()).getCategories();
			Assert.assertTrue(charityCategories.contains(CharityCategory.EDUCATION));
		}
	}

	@Test
	public void test_filter_charities_by_multiple_regions() {
		this.cmdLoadTestDataSet(this.testCharities);

		SearchQuery query = SearchQueries.q(Charity.ReachSearchFields.COUNTRY).eq(Country.CA)
				.and(Charity.ReachSearchFields.REGION).in("ON", "AB")
				.getSearchQuery();
		Fields fields = new Fields();
		fields.include(Charity.Fields.ID);
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5)
				.fields(fields)
				.search(query).build());
		Assert.assertTrue(charities.getNumberOfItems() == 2);
		for (Charity c : charities.getItems()) {
			boolean ca = c.getReach().getAreas().stream().anyMatch(
					a -> a.getRegions().contains("ON") || a.getRegions().contains("AB"));
			Assert.assertTrue(ca);
		}
	}

	@Test
	@Ignore("Group by all aggregate columns required on H2. Passes on MYSQL")
	public void test_filter_charities_by_categories() {
		this.cmdLoadTestDataSet(this.testCharities);

		List<CharityCategory> categoryFilter = Arrays.asList(CharityCategory.EDUCATION, CharityCategory.ENVIRONMENT);

		SearchQuery query = SearchQueries.q(Charity.Fields.CATEGORIES).in(categoryFilter).getSearchQuery();
		Page<Charity> charities = this.repo.get(PageRequest.with(0, 5).search(query).build());
		Assert.assertTrue(charities.hasItems());
		for (Charity c : charities.getItems()) {
			Set<CharityCategory> charityCategories = this.repo.get(c.getId()).getCategories();
			Assert.assertTrue(charityCategories.contains(CharityCategory.EDUCATION) || charityCategories.contains(CharityCategory.ENVIRONMENT));
		}
	}

}
