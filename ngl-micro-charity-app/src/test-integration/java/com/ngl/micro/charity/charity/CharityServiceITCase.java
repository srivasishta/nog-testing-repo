package com.ngl.micro.charity.charity;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity;
import com.ngl.micro.charity.revision.jooq.JVersionControlService;
import com.ngl.micro.charity.test_data.TestAccessTokens;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.oauth2.TestSubjects;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.json.ObjectMapperFactory;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class CharityServiceITCase extends AbstractDatabaseITCase {

	private static ObjectMapper mapper = new ObjectMapperFactory().getInstance();

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private CharityService service;
	private CharityRepository repo;
	private TestCharities testCharities;
	private JVersionControlService<Charity> charityVc;

	@Before
	public void setup() {
		this.testCharities = new TestCharities();
		this.repo = new CharityRepository(new DAL(this.sqlContext));
		this.charityVc= new JVersionControlService<>(
				mapper,
				JNglMicroCharity.NGL_MICRO_CHARITY.getName(),
				"charity_revision",
				new DAL(this.sqlContext));
		this.service = new CharityService(this.repo, CharityValidator.newInstance(), this.charityVc);
	}

	@Test
	public void test_update_charity_logo_as_system() {
		TestSubjects.login(TestAccessTokens.SYSTEM);
		Charity base = this.service.createCharity(this.testCharities.newCharity());
		Charity working = this.service.getCharityById(base.getId());

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.service.updateCharity(base.getId(), patch);
		Charity patched = this.service.getCharityById(base.getId());

		Assert.assertThat(patched, CoreMatchers.is(working));
	}

	@Test
	public void test_fail_create_charity_as_partner() {
		TestSubjects.login(TestAccessTokens.charity_read());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.service.createCharity(this.testCharities.newCharity());
	}

	@Test
	public void test_fail_update_charity_as_partner() {
		TestSubjects.login(TestAccessTokens.charity_write());
		Charity base = this.service.createCharity(this.testCharities.newCharity());

		TestSubjects.login(TestAccessTokens.charity_read());

		Charity working = this.service.getCharityById(base.getId());

		working.getLogoImages().setOriginal("http://cdn.images/original");
		Patch patch = new Diff().diff(working, base);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.service.updateCharity(base.getId(), patch);
	}

}