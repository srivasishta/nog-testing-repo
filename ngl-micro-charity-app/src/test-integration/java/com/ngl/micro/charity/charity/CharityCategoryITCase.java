package com.ngl.micro.charity.charity;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCategoryDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory;
import com.ngl.middleware.database.test.AbstractDatabaseITCase;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;

@ContextConfiguration(classes = DefaultDatabaseTestConfiguration.class)
public class CharityCategoryITCase extends AbstractDatabaseITCase {

	private JCategoryDao categoryDao;

	@Override
	public void before() {
		super.before();
		this.categoryDao = new JCategoryDao(this.sqlContext);
	}

	@Test
	public void test_valid_category_keys() throws Exception {
		List<JCategory> categories = this.categoryDao.findAll();
		Assert.assertEquals(categories.size(), Charity.CharityCategory.values().length);

		for (Charity.CharityCategory key : Charity.CharityCategory.values()) {
			JCategory persistedCategory = this.categoryDao.fetchOneByJValue(key.name());
			Assert.assertNotNull("Expect category: " + key, persistedCategory);
			Assert.assertEquals("Expected matching id:", persistedCategory.getId(), CharityCategoryType.valueOf(persistedCategory.getValue()).getId());
		}
	}
}

