package com.ngl.micro.charity;

import static com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity.NGL_MICRO_CHARITY;

import com.ngl.middleware.microservice.MicroserviceApplication;

/**
 * The charity application entry-point.
 *
 * @author Willy du Preez
 *
 */
public class CharityApplication extends MicroserviceApplication {

	public static void main(String[] args) throws Exception {
		new CharityApplication().development();
	}

	public CharityApplication() {
		super(CharityApplicationConfiguration.class, NGL_MICRO_CHARITY);
	}

}