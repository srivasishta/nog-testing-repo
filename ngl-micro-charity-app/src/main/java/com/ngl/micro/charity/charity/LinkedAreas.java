package com.ngl.micro.charity.charity;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_AREA;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Country;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityAreaDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityArea;
import com.ngl.middleware.dal.repository.AbstractLinkedDataSet;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.util.Collections;

public class LinkedAreas extends AbstractLinkedDataSet<Area, Country, Long> {

	private static final String RESOURCE_ID = "resource_id";
	private static final String COUNTRY = "country";
	private static final String REGION = "region";

	private DAL support;
	private JCharityAreaDao areaDao;
	private JooqQuery query;
	private TableOnConditionStep<?> tables;
	private FieldMap<String> fieldMap;

	public LinkedAreas(DAL dal) {
		this.areaDao = new JCharityAreaDao(dal.config());
		this.support = dal;

		this.fieldMap = new FieldMap<String>()
				.bind(RESOURCE_ID, new MappedField(CHARITY.RESOURCE_ID).searchable(true))
				.bind(COUNTRY, new MappedField(CHARITY_AREA.COUNTRY))
				.bind(REGION, new MappedField(CHARITY_AREA.REGION));

		this.tables = CHARITY.join(CHARITY_AREA).on(CHARITY.ID.eq(CHARITY_AREA.CHARITY_ID));

		this.query = JooqQuery.builder(this.support, this.fieldMap).from(this.tables).build();

	}

	@Override
	public Area add(Long parentId, Area items) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public Area get(Long parentId, Country childId) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public Set<Area> list(Long id) {
		List<JCharityArea> charityAreas = this.areaDao.fetchByJCharityId(id);
		Map<Country, Set<String>> regions = this.countryRegions(charityAreas);
		Set<Area> areas = new HashSet<>();
		for (Country country : regions.keySet()) {
			areas.add(new Area(country, regions.get(country)));
		}
		return areas;
	}

	public Map<UUID, Set<Area>> list(Page<Charity> charityPage) {
		List<Record> reachAreaRecords = this.query.getRecordsWhere(
				CHARITY.RESOURCE_ID.in(charityPage.getItems()
						.stream()
						.map(c -> c.getId())
						.collect(Collectors.toList())));

		return reachAreaRecords.stream().map(this::map).collect(Collectors.groupingBy(ca -> {
			return ca.getCharityId();
		}, Collectors.collectingAndThen(Collectors.groupingBy(ca -> {
			return ca.getCountry();
		}, Collectors.collectingAndThen(Collectors.mapping(ca -> {
			return ca.getRegions();
		}, Collectors.toSet()), this::flatten)), this::toAreaSet)));
	}

	private Set<String> flatten(Set<Set<String>> nested) {
		Set<String> flatSet = new HashSet<String>();
		nested.forEach(strSet -> {
			flatSet.addAll(strSet);
		});
		return flatSet;
	}

	private Set<Area> toAreaSet(Map<Country, Set<String>> map) {
		Set<Area> area = new HashSet<Area>();
		map.forEach((key, value) -> {
			area.add(new Area(key, value));
		});
		return area;
	}

	private CharityArea map(Record record) {
		CharityArea charityArea = new CharityArea();
		charityArea.setCharityId(record.getValue(CHARITY.RESOURCE_ID));
		charityArea.setCountry(Country.valueOf(record.getValue(CHARITY_AREA.COUNTRY)));
		charityArea.setRegions(Collections.asSet(record.getValue(CHARITY_AREA.REGION)));
		return charityArea;
	}

	private Map<Country, Set<String>> countryRegions(List<JCharityArea> areas) {
		Map<Country, Set<String>> countryRegions = new HashMap<>();
		for (JCharityArea area : areas) {
			Country country = Country.valueOf(area.getCountry());
			if (countryRegions.containsKey(country)) {
				countryRegions.get(country).add(area.getRegion());
			} else {
				Set<String> regions = new HashSet<>();
				regions.add(area.getRegion());
				countryRegions.put(country, regions);
			}
		}
		return countryRegions;
	}

	@Override
	public void update(Long parentId, Area item) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void add(Long id, Set<Area> areas) {
		Set<JCharityArea> charityRegions = new HashSet<>();
		for (Area area : areas) {
			Set<JCharityArea> countryRegions = area.getRegions().stream()
					.map(region -> this.map(id, area.getCountry(), region)).collect(Collectors.toSet());
			charityRegions.addAll(countryRegions);
		}
		this.areaDao.insert(charityRegions);
	}

	private JCharityArea map(Long charityId, Country country, String region) {
		JCharityArea mappedArea = new JCharityArea();
		mappedArea.setCharityId(charityId);
		mappedArea.setCountry(country.name());
		mappedArea.setRegion(region);
		return mappedArea;
	}

	@Override
	public void update(Long id, Set<Area> areas) {
		List<JCharityArea> currentAreas = this.areaDao.fetchByJCharityId(id);
		if (!currentAreas.isEmpty()) {
			this.areaDao.delete(currentAreas);
		}
		this.add(id, areas);
	}
}
