package com.ngl.micro.charity.contribution;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_CATEGORIES;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CONTRIBUTION_ALLOCATION;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CONTRIBUTION_CHARITY_CHOICES;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CONTRIBUTION_DOCUMENT;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.impl.DSL;
import org.springframework.transaction.annotation.Transactional;

import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.charity.CharityCategoryType;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JContributionAllocationDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JContributionCharityChoicesDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JContributionDemographicsDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JContributionAllocation;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JContributionCharityChoices;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JContributionDemographics;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JContributionDocumentRecord;
import com.ngl.micro.contribution.ContributionSearchFields;
import com.ngl.micro.shared.contracts.charity.ContributionAllocation;
import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQueries;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.util.Assert;

/**
 * Contribution document repository.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackFor = Exception.class)
public class ContributionDocumentRepository {

	private DAL dal;

	private JContributionAllocationDao allocations;
	private JContributionCharityChoicesDao choices;
	private JContributionDemographicsDao demographics;

	private FieldMap<String> totalsSearchFields;

	public ContributionDocumentRepository(org.jooq.Configuration config) {
		this.dal = new DAL(config);

		this.allocations = new JContributionAllocationDao(config);
		this.choices = new JContributionCharityChoicesDao(config);
		this.demographics = new JContributionDemographicsDao(config);

		this.totalsSearchFields = new FieldMap<String>()
				.bind(ContributionSearchFields.CONTRIBUTION_DATE, CONTRIBUTION_DOCUMENT.CONTRIBUTION_DATE);
	}

	public ContributionDocument saveDocument(ContributionDocument document) {
		Assert.notNull(document);

		JContributionDocumentRecord record = this.getOrCreateDocumentRecord(document.getId());

		record.setResourceId(document.getId());
		record.setCreatedDate(document.getCreatedDate());
		record.setLastModifiedDate(document.getLastModifiedDate());

		record.setBusinessStoreContributionRate(document.getBusinessStoreContributionRate());
		record.setBusinessId(document.getBusinessId());
		record.setBusinessStoreId(document.getBusinessStoreId());

		record.setContributionDate(document.getContributionDate());
		record.setContributionAmount(document.getContributionAmount());

		record.setMembershipId(document.getMembershipId());

		record.setPartnerId(document.getPartnerId());

		record.setTransactionDate(document.getTransactionDate());
		record.setTransactionAmount(document.getTransactionAmount());
		record.setTransactionCurrency(document.getTransactionCurrency().getId());
		record.setTransactionStatus(document.getTransactionStatus().getId());
		record.setTransactionTimeZone(document.getTransactionTimezone().getId());

		record.store();

		Long fk = record.getId();
		this.saveContributionAllocations(fk, document.getContributionAllocations());
		this.saveMembershipCharityChoices(fk, document.getMembershipCharityChoices());
		this.saveMembershipDemographics(fk, document.getMembershipDemographics());

		return this.getDocument(document.getId()).get();
	}

	private JContributionDocumentRecord getOrCreateDocumentRecord(UUID id) {
		JContributionDocumentRecord record = this.dal.sql().fetchOne(
				CONTRIBUTION_DOCUMENT,
				CONTRIBUTION_DOCUMENT.RESOURCE_ID.eq(id));

		if (record == null) {
			record = this.dal.sql().newRecord(CONTRIBUTION_DOCUMENT);
		}
		return record;
	}

	private void saveMembershipDemographics(Long fk, MembershipDemographics demographics) {
		JContributionDemographics record = this.demographics.findById(fk);
		if (record == null) {
			record = new JContributionDemographics();
			record.setContributionDocumentId(fk);
			record.setBirthdate(demographics.getBirthdate().map(d -> Date.valueOf(d)).orElse(null));
			record.setGender(demographics.getGender().getId());
			this.demographics.insert(record);
		}
		else {
			Assert.state(this.mapDemographicsFromRecord(record).equals(demographics));
		}
	}

	private MembershipDemographics mapDemographicsFromRecord(JContributionDemographics record) {
		Gender gender = Gender.forId(record.getGender());
		LocalDate birthDate = record.getBirthdate() == null ? null : record.getBirthdate().toLocalDate();
		return new MembershipDemographics(gender, birthDate);
	}

	private void saveMembershipCharityChoices(Long fk, Set<MembershipCharityChoice> choices) {
		Set<MembershipCharityChoice> existing = this.getCharityChoicesByDocumentPk(fk);
		if (existing.isEmpty() && !choices.isEmpty()) {
			this.choices.insert(choices.stream().map(c ->
				new JContributionCharityChoices(fk, this.getCharityId(c.getCharityId()), c.getAllocation())
			).collect(Collectors.toList()));
		}
		else {
			Assert.state(new HashSet<>(existing).equals(choices));
		}
	}

	private Set<MembershipCharityChoice> getCharityChoicesByDocumentPk(Long id) {
		List<MembershipCharityChoice> existing = this.dal.sql()
				.select(CHARITY.RESOURCE_ID,
						CONTRIBUTION_CHARITY_CHOICES.CHARITY_ALLOCATION)
				.from(CONTRIBUTION_CHARITY_CHOICES)
				.join(CHARITY).on(CHARITY.ID.eq(CONTRIBUTION_CHARITY_CHOICES.CHARITY_ID))
				.where(CONTRIBUTION_CHARITY_CHOICES.CONTRIBUTION_DOCUMENT_ID.eq(id))
				.fetch(r -> new MembershipCharityChoice(r.value1(), r.value2()));

		return existing.isEmpty() ? Collections.emptySet() : new HashSet<>(existing);
	}

	private void saveContributionAllocations(Long fk, Set<ContributionAllocation> allocations) {
		Set<ContributionAllocation> existing = this.getContributionAllocationsByDocumentPk(fk);
		if (existing.isEmpty() && !allocations.isEmpty()) {
			this.allocations.insert(allocations.stream().map(a ->
				new JContributionAllocation(fk, this.getCharityId(a.getCharityId()), a.getAmount(), a.getAllocation())
			).collect(Collectors.toList()));
		}
		else {
			Assert.state(new HashSet<>(existing).equals(allocations));
		}
	}

	private Set<ContributionAllocation> getContributionAllocationsByDocumentPk(Long id) {
		List<ContributionAllocation> existing = this.dal.sql()
				.select(CHARITY.RESOURCE_ID,
						CONTRIBUTION_ALLOCATION.AMOUNT,
						CONTRIBUTION_ALLOCATION.ALLOCATION)
				.from(CONTRIBUTION_ALLOCATION)
				.join(CHARITY).on(CHARITY.ID.eq(CONTRIBUTION_ALLOCATION.CHARITY_ID))
				.where(CONTRIBUTION_ALLOCATION.CONTRIBUTION_DOCUMENT_ID.eq(id))
				.fetch(r -> new ContributionAllocation(r.value1(), r.value2(), r.value3()));

		return existing.isEmpty() ? Collections.emptySet() : new HashSet<>(existing);
	}

	private Long getCharityId(UUID resourceId) {
		Record record = this.dal.sql()
				.select(CHARITY.ID)
				.from(CHARITY)
				.where(CHARITY.RESOURCE_ID.eq(resourceId))
				.fetchOne();

		if (record == null) {
			throw ApplicationError.resourceNotFound()
					.setTitle("Charity Not Found")
					.setDetail("No charity found for ID: " + resourceId)
					.asException();
		}

		return record.getValue(CHARITY.ID);
	}

	public Optional<ContributionDocument> getDocument(UUID id) {
		Assert.notNull(id);

		JContributionDocumentRecord record = this.dal.sql().fetchOne(
				CONTRIBUTION_DOCUMENT,
				CONTRIBUTION_DOCUMENT.RESOURCE_ID.eq(id));

		if (record == null) {
			return Optional.empty();
		}

		ContributionDocument.Builder builder = ContributionDocument.builder();
		builder.id(record.getResourceId());
		builder.createdDate(record.getCreatedDate());
		builder.lastModifiedDate(record.getLastModifiedDate());

		builder.partnerId(record.getPartnerId());

		builder.transactionDate(record.getTransactionDate());
		builder.transactionStatus(TransactionStatus.forId(record.getTransactionStatus()));
		builder.transactionAmount(record.getTransactionAmount());
		builder.transactionTimezone(TimeZone.forId(record.getTransactionTimeZone()));
		builder.transactionCurrency(Currency.forId(record.getTransactionCurrency()));

		builder.businessId(record.getBusinessId());
		builder.businessStoreId(record.getBusinessStoreId());
		builder.businessStoreContributionRate(record.getBusinessStoreContributionRate());

		builder.membershipId(record.getMembershipId());
		builder.membershipCharityChoices(this.getCharityChoicesByDocumentPk(record.getId()));
		builder.membershipDemographics(this.mapDemographicsFromRecord(
				this.demographics.fetchOneByJContributionDocumentId(record.getId())));

		builder.contributionAllocations(this.getContributionAllocationsByDocumentPk(record.getId()));
		if (record.getContributionDate() != null) {
			builder.contributionDate(record.getContributionDate());
		}
		builder.contributionAmount(record.getContributionAmount());

		return Optional.of(builder.build());
	}

	public Map<CharityCategory, BigDecimal> getTotalsByCategory(SearchQuery query) {
		SelectOnConditionStep<Record2<Long, BigDecimal>> select = this.dal.sql()
				.select(CHARITY_CATEGORIES.CATEGORY_ID, DSL.sum(CONTRIBUTION_ALLOCATION.AMOUNT))
				.from(CONTRIBUTION_ALLOCATION)
				.join(CHARITY_CATEGORIES).on(CHARITY_CATEGORIES.CHARITY_ID.eq(CONTRIBUTION_ALLOCATION.CHARITY_ID));

		Result<Record2<Long, BigDecimal>> result = JooqQueries.mapSearch(this.totalsSearchFields, query).map(c ->
				select.join(CONTRIBUTION_DOCUMENT)
				.on(CONTRIBUTION_DOCUMENT.ID.eq(CONTRIBUTION_ALLOCATION.CONTRIBUTION_DOCUMENT_ID))
				.and(c)
		).orElse(select).groupBy(CHARITY_CATEGORIES.CATEGORY_ID).fetch();

		// Make sure that all categories are returned,
		// even if there are no contributions for a
		// category.
		Map<CharityCategory, BigDecimal> totals = this.getTotalsAsZero();
		result.forEach(r -> {
			totals.put(CharityCategory.valueOf(CharityCategoryType.nameForId(r.value1())), r.value2());
		});

		return totals;
	}

	private Map<CharityCategory, BigDecimal> getTotalsAsZero() {
		Map<CharityCategory, BigDecimal> summary = new HashMap<>();
		for (CharityCategory category : CharityCategory.values()) {
			summary.put(category, new BigDecimal("0.0"));
		}
		return summary;
	}

	public BigDecimal getTotalContributionsByCharityId(UUID charityId, SearchQuery query) {
		SelectJoinStep<Record1<BigDecimal>> select = this.getTotalQuery(query)
				.join(CHARITY).on(CHARITY.ID.eq(CONTRIBUTION_ALLOCATION.CHARITY_ID))
				.and(CHARITY.RESOURCE_ID.eq(charityId));
		return Optional.ofNullable(select.fetchOne(CONTRIBUTION_ALLOCATION.AMOUNT))
				.orElse(BigDecimal.ZERO);
	}

	public BigDecimal getTotalContributions(SearchQuery query) {
		return Optional.ofNullable(this.getTotalQuery(query).fetchOne(CONTRIBUTION_ALLOCATION.AMOUNT))
				.orElse(BigDecimal.ZERO);
	}

	private SelectJoinStep<Record1<BigDecimal>> getTotalQuery(SearchQuery query) {
		return JooqQueries.mapSearch(this.totalsSearchFields, query).map(c ->
			(SelectJoinStep<Record1<BigDecimal>>) this.dal.sql()
				.select(DSL.sum(CONTRIBUTION_ALLOCATION.AMOUNT).as(CONTRIBUTION_ALLOCATION.AMOUNT))
				.from(CONTRIBUTION_ALLOCATION)
				.join(CONTRIBUTION_DOCUMENT)
				.on(CONTRIBUTION_DOCUMENT.ID.eq(CONTRIBUTION_ALLOCATION.CONTRIBUTION_DOCUMENT_ID))
				.and(c)
		).orElse(this.dal.sql()
				.select(DSL.sum(CONTRIBUTION_ALLOCATION.AMOUNT).as(CONTRIBUTION_ALLOCATION.AMOUNT))
				.from(CONTRIBUTION_ALLOCATION));
	}

}
