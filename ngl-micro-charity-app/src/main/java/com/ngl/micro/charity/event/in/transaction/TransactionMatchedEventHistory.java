package com.ngl.micro.charity.event.in.transaction;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.messaging.api.event.AbstractEvent;
import com.ngl.middleware.util.Assert;

/**
 * A POJO used to manage transaction matched event history.
 *
 * @author Willy du Preez
 *
 */
public class TransactionMatchedEventHistory extends AbstractEvent<Long> {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "transaction-matched-event";
	private static final String VERSION = "1.0";

	private UUID documentId;

	public TransactionMatchedEventHistory(Long id, ZonedDateTime created, UUID documentId) {
		super(NAME, VERSION, id, created);

		Assert.notNull(documentId);
		this.documentId = documentId;
	}

	public UUID getDocumentId() {
		return this.documentId;
	}

}
