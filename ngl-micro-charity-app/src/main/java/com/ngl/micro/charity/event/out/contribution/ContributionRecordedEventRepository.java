package com.ngl.micro.charity.event.out.contribution;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CONTRIBUTION_RECORDED_EVENT;

import java.util.Optional;

import org.jooq.Record;

import com.ngl.micro.charity.contribution.ContributionDocumentRepository;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JContributionRecordedEventRecord;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.dal.repository.PublishableEventRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.portability.SQLBooleans;

/**
 * Manages {@link ContributionRecordedEvent}s.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEventRepository implements PublishableEventRepository<ContributionRecordedEvent, Long> {

	private DAL dal;
	private ContributionDocumentRepository documents;

	public ContributionRecordedEventRepository(
			org.jooq.Configuration config,
			ContributionDocumentRepository documents) {
		this.dal = new DAL(config);
	}

	@Override
	public ContributionRecordedEvent addEvent(ContributionRecordedEvent event) {
		JContributionRecordedEventRecord record = this.dal.sql().newRecord(CONTRIBUTION_RECORDED_EVENT);
		record.setId(event.getId());
		record.setCreatedDate(event.getCreated());
		record.setContributionDocumentId(event.getDocument().getId());
		record.store();
		return event;
	}

	@Override
	public Optional<ContributionRecordedEvent> getEvent(Long id) {
		Record record = this.dal.sql().select()
				.from(CONTRIBUTION_RECORDED_EVENT)
				.where(CONTRIBUTION_RECORDED_EVENT.ID.eq(id))
				.fetchOne();

		if (record == null) {
			return Optional.empty();
		}

		return this.documents.getDocument(record.getValue(CONTRIBUTION_RECORDED_EVENT.CONTRIBUTION_DOCUMENT_ID))
				.map(document -> new ContributionRecordedEvent(
						record.getValue(CONTRIBUTION_RECORDED_EVENT.ID),
						record.getValue(CONTRIBUTION_RECORDED_EVENT.CREATED_DATE),
						document));
	}

	@Override
	public void markPublished(Long eventId) {
		this.dal.sql().update(CONTRIBUTION_RECORDED_EVENT)
				.set(CONTRIBUTION_RECORDED_EVENT.PUBLISHED, SQLBooleans.toString(true))
				.where(CONTRIBUTION_RECORDED_EVENT.ID.eq(eventId))
				.execute();
	}

}
