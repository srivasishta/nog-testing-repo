package com.ngl.micro.charity.charity;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.revision.VersionControlService;
import com.ngl.micro.shared.contracts.oauth.ApiPolicy;
import com.ngl.middleware.rest.api.error.ApplicationError;
import com.ngl.middleware.rest.api.error.ApplicationException;
import com.ngl.middleware.rest.api.error.ValidationError;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.api.revision.Revision;
import com.ngl.middleware.rest.patch.merge.Merge;
import com.ngl.middleware.rest.server.security.oauth2.util.Subjects;
import com.ngl.middleware.util.EqualsUtils;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.UUIDS;

/**
 * The charity service.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackOn = Exception.class)
public class CharityService {

	private CharityRepository charities;
	private CharityValidator validator;
	private Merge mergeHandler;
	private VersionControlService<Charity, UUID> charityVc;

	public CharityService(CharityRepository charities,
			CharityValidator validator,
			VersionControlService<Charity, UUID> charityVc) {
		this.charities = charities;
		this.validator = validator;
		this.mergeHandler = new Merge();
		this.charityVc = charityVc;
	}

	public Charity createCharity(Charity charity) {
		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			throw ApplicationError.forbidden()
				.setTitle("Unauthorized Charity Create")
				.setDetail("Only the system account may create charities.")
				.asException();
		}

		ZonedDateTime createdDate = Time.utcNow();
		charity.setId(UUIDS.type4Uuid());
		this.validator.validate(charity);

		Charity created = this.charities.add(charity);
		this.charityVc.revise(
				createdDate,
				new Revision(),
				new Charity(),
				created,
				Subjects.getClientId(),
				Subjects.getSubject());

		return created;
	}

	public Charity getCharityById(UUID id) {
		Charity charity = this.charities.get(id);
		if (charity == null) {
			ApplicationError error = ApplicationError.resourceNotFound().build();
			throw new ApplicationException(error, "No charity found with ID: " + id);
		}
		return charity;
	}

	public Page<Charity> getCharitys(PageRequest pageRequest) {
		return this.charities.get(pageRequest);
	}

	public Charity updateCharity(UUID id, Patch patch) {
		UUID partnerId = Subjects.getSubject();
		if (!ApiPolicy.isSystemAccount(partnerId)) {
			throw ApplicationError.forbidden()
					.setTitle("Unauthorized Charity Update")
					.setDetail("Only the system account may update charities.")
					.asException();
		}

		ZonedDateTime updatedDate = Time.utcNow();
		Charity base = this.getCharityById(id);
		Charity working = this.getCharityById(id);
		this.mergeHandler.merge(patch, working);
		working.setId(id);

		this.validator.validate(working);
		this.validateDefaultCharityUpdate(working);

		this.charities.update(working);
		Charity updated = this.getCharityById(id);

		this.charityVc.revise(
				updatedDate,
				new Revision(),
				base,
				updated,
				Subjects.getClientId(),
				Subjects.getSubject());

		return updated;
	}

	private void validateDefaultCharityUpdate(Charity charity) {
		UUID defaultCharityId = this.charities.getDefaultCharityId();
		if (charity.getId().equals(defaultCharityId)) {
			List<ValidationError> errors = new ArrayList<>();
			if (CharityCategory.values().length != charity.getCategories().size()) {
				errors.add(new ValidationError("categories", ValidationErrorCode.INVALID, "The default charity categories must include all categories."));
			}
			if (!EqualsUtils.areEqual(ReachType.GLOBAL, charity.getReach().getType())) {
				errors.add(new ValidationError("categories", ValidationErrorCode.INVALID, "The default charity reach must be GLOBAL."));
			}
			if (!EqualsUtils.areEqual(CharityStatus.ACTIVE, charity.getStatus())) {
				errors.add(new ValidationError("categories", ValidationErrorCode.INVALID, "The default charity status must be ACTIVE."));
			}

			if (!errors.isEmpty()) {
				throw ApplicationError.validationError()
						.setTitle("Validation Error")
						.setDetail("Invalid updates to the default charity.")
						.addValidationErrors(errors)
						.asException();
			}
		}
	}

}
