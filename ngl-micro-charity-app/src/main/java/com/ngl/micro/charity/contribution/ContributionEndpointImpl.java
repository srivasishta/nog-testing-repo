package com.ngl.micro.charity.contribution;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.contribution.NetworkContributions;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;

/**
 * Contribution endpoint implementation.
 *
 * @author Cameron Waldron
 *
 */
public class ContributionEndpointImpl implements ContributionEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private ContributionService contributionService;

	public ContributionEndpointImpl(RequestProcessor processor, ContributionService contributionService) {
		this.contributionService = contributionService;
		this.processor = processor;
	}

	@Override
	public Response summary() {
		SearchQuery query = this.processor.pageRequest(this.msgCtx).getSearchQuery();
		NetworkContributions contributions = this.contributionService.getNetworkContributions(query);
		Resource resource = HAL.resource(contributions).build();
		return Response.ok(resource).build();
	}

}
