/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables.records;


import com.ngl.micro.charity.dal.generated.jooq.tables.JCharityArea;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JCharityAreaRecord extends UpdatableRecordImpl<JCharityAreaRecord> implements Record4<Long, Long, String, String> {

	private static final long serialVersionUID = -482061292;

	/**
	 * Setter for <code>ngl_micro_charity.charity_area.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>ngl_micro_charity.charity_area.id</code>.
	 */
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>ngl_micro_charity.charity_area.charity_id</code>.
	 */
	public void setCharityId(Long value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>ngl_micro_charity.charity_area.charity_id</code>.
	 */
	public Long getCharityId() {
		return (Long) getValue(1);
	}

	/**
	 * Setter for <code>ngl_micro_charity.charity_area.country</code>.
	 */
	public void setCountry(String value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>ngl_micro_charity.charity_area.country</code>.
	 */
	public String getCountry() {
		return (String) getValue(2);
	}

	/**
	 * Setter for <code>ngl_micro_charity.charity_area.region</code>.
	 */
	public void setRegion(String value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>ngl_micro_charity.charity_area.region</code>.
	 */
	public String getRegion() {
		return (String) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, Long, String, String> fieldsRow() {
		return (Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row4<Long, Long, String, String> valuesRow() {
		return (Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return JCharityArea.CHARITY_AREA.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field2() {
		return JCharityArea.CHARITY_AREA.CHARITY_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field3() {
		return JCharityArea.CHARITY_AREA.COUNTRY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field4() {
		return JCharityArea.CHARITY_AREA.REGION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value2() {
		return getCharityId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value3() {
		return getCountry();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value4() {
		return getRegion();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityAreaRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityAreaRecord value2(Long value) {
		setCharityId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityAreaRecord value3(String value) {
		setCountry(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityAreaRecord value4(String value) {
		setRegion(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityAreaRecord values(Long value1, Long value2, String value3, String value4) {
		value1(value1);
		value2(value2);
		value3(value3);
		value4(value4);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JCharityAreaRecord
	 */
	public JCharityAreaRecord() {
		super(JCharityArea.CHARITY_AREA);
	}

	/**
	 * Create a detached, initialised JCharityAreaRecord
	 */
	public JCharityAreaRecord(Long id, Long charityId, String country, String region) {
		super(JCharityArea.CHARITY_AREA);

		setValue(0, id);
		setValue(1, charityId);
		setValue(2, country);
		setValue(3, region);
	}
}
