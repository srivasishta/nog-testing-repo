package com.ngl.micro.charity.charity.constraint;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Country;

/**
 * Validate charity reach rules for country allocation {@link ValidReach}.
 */
public class ReachValidator implements ConstraintValidator<ValidReach, com.ngl.micro.charity.Reach>{

	private static final int NATIONAL_COUNTRY_COUNT = 1;

	@Override
	public void initialize(ValidReach reach) {
		reach.getClass();
	}

	@Override
	public boolean isValid(com.ngl.micro.charity.Reach reach, ConstraintValidatorContext context) {
		if (reach == null) {
			failNull(context);
			return false;
		}
		Set<Area> areas = reach.getAreas();
		if (areas == null) {
			failNullAreas(context);
			return false;
		}

		switch(reach.getType()) {
		case GLOBAL:
			if (areas.size() > 0) {
				failGlobal(context);
				return false;
			}
			break;
		case INTERNATIONAL:
			if (isCountryCount(areas, c -> c <= NATIONAL_COUNTRY_COUNT)) {
				failInternationalCountry(context);
				return false;
			}
			break;
		case NATIONAL:
			if (isCountryCount(areas, c -> c != NATIONAL_COUNTRY_COUNT)) {
				failNationalCountry(context);
				return false;
			}
			break;
		case REGIONAL:
			if (isCountryCount(areas, c -> c != NATIONAL_COUNTRY_COUNT)) {
				failRegionalCountry(context);
				return false;
			}
			break;
		default:
			return false;
		}
		return true;
	}

	private boolean isCountryCount(Set<Area> areas, Predicate<Integer> predicate) {
		Set<Country> countries = new HashSet<>();
		for (Area area : areas) {
			countries.add(area.getCountry());
		}
		return predicate.test(countries.size());
	}

	private void failNull(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("Reach may not be null.").addConstraintViolation();
	}

	private void failNullAreas(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("Areas may not be null.").addConstraintViolation();
	}

	private void failGlobal(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("GLOBAL charities must not reach target areas.").addConstraintViolation();
	}

	private void failInternationalCountry(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("INTERNATIONAL charities must reach more than one country.").addConstraintViolation();
	}

	private void failNationalCountry(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("NATIONAL charities must reach exactly one country.").addConstraintViolation();
	}

	private void failRegionalCountry(ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate("REGIONAL charities must reach exactly one country.").addConstraintViolation();
	}
}