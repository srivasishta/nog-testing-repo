/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables;


import com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity;
import com.ngl.micro.charity.dal.generated.jooq.Keys;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JContributionAllocationRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JContributionAllocation extends TableImpl<JContributionAllocationRecord> {

	private static final long serialVersionUID = -261785607;

	/**
	 * The reference instance of <code>ngl_micro_charity.contribution_allocation</code>
	 */
	public static final JContributionAllocation CONTRIBUTION_ALLOCATION = new JContributionAllocation();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JContributionAllocationRecord> getRecordType() {
		return JContributionAllocationRecord.class;
	}

	/**
	 * The column <code>ngl_micro_charity.contribution_allocation.contribution_document_id</code>.
	 */
	public final TableField<JContributionAllocationRecord, Long> CONTRIBUTION_DOCUMENT_ID = createField("contribution_document_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.contribution_allocation.charity_id</code>.
	 */
	public final TableField<JContributionAllocationRecord, Long> CHARITY_ID = createField("charity_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.contribution_allocation.amount</code>.
	 */
	public final TableField<JContributionAllocationRecord, BigDecimal> AMOUNT = createField("amount", org.jooq.impl.SQLDataType.DECIMAL.precision(15, 5).nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.contribution_allocation.allocation</code>.
	 */
	public final TableField<JContributionAllocationRecord, Integer> ALLOCATION = createField("allocation", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_charity.contribution_allocation</code> table reference
	 */
	public JContributionAllocation() {
		this("contribution_allocation", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_charity.contribution_allocation</code> table reference
	 */
	public JContributionAllocation(String alias) {
		this(alias, CONTRIBUTION_ALLOCATION);
	}

	private JContributionAllocation(String alias, Table<JContributionAllocationRecord> aliased) {
		this(alias, aliased, null);
	}

	private JContributionAllocation(String alias, Table<JContributionAllocationRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroCharity.NGL_MICRO_CHARITY, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JContributionAllocationRecord> getPrimaryKey() {
		return Keys.KEY_CONTRIBUTION_ALLOCATION_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JContributionAllocationRecord>> getKeys() {
		return Arrays.<UniqueKey<JContributionAllocationRecord>>asList(Keys.KEY_CONTRIBUTION_ALLOCATION_PRIMARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JContributionAllocationRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JContributionAllocationRecord, ?>>asList(Keys.CONTRIBUTION_ALLOCATION_IBFK_1, Keys.CONTRIBUTION_ALLOCATION_IBFK_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JContributionAllocation as(String alias) {
		return new JContributionAllocation(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JContributionAllocation rename(String name) {
		return new JContributionAllocation(name, null);
	}
}
