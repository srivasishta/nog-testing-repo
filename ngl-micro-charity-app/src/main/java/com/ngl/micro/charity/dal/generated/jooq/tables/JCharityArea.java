/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables;


import com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity;
import com.ngl.micro.charity.dal.generated.jooq.Keys;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCharityAreaRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JCharityArea extends TableImpl<JCharityAreaRecord> {

	private static final long serialVersionUID = 268877767;

	/**
	 * The reference instance of <code>ngl_micro_charity.charity_area</code>
	 */
	public static final JCharityArea CHARITY_AREA = new JCharityArea();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JCharityAreaRecord> getRecordType() {
		return JCharityAreaRecord.class;
	}

	/**
	 * The column <code>ngl_micro_charity.charity_area.id</code>.
	 */
	public final TableField<JCharityAreaRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.charity_area.charity_id</code>.
	 */
	public final TableField<JCharityAreaRecord, Long> CHARITY_ID = createField("charity_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.charity_area.country</code>.
	 */
	public final TableField<JCharityAreaRecord, String> COUNTRY = createField("country", org.jooq.impl.SQLDataType.VARCHAR.length(2).nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.charity_area.region</code>.
	 */
	public final TableField<JCharityAreaRecord, String> REGION = createField("region", org.jooq.impl.SQLDataType.VARCHAR.length(50), this, "");

	/**
	 * Create a <code>ngl_micro_charity.charity_area</code> table reference
	 */
	public JCharityArea() {
		this("charity_area", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_charity.charity_area</code> table reference
	 */
	public JCharityArea(String alias) {
		this(alias, CHARITY_AREA);
	}

	private JCharityArea(String alias, Table<JCharityAreaRecord> aliased) {
		this(alias, aliased, null);
	}

	private JCharityArea(String alias, Table<JCharityAreaRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroCharity.NGL_MICRO_CHARITY, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<JCharityAreaRecord, Long> getIdentity() {
		return Keys.IDENTITY_CHARITY_AREA;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JCharityAreaRecord> getPrimaryKey() {
		return Keys.KEY_CHARITY_AREA_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JCharityAreaRecord>> getKeys() {
		return Arrays.<UniqueKey<JCharityAreaRecord>>asList(Keys.KEY_CHARITY_AREA_PRIMARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityArea as(String alias) {
		return new JCharityArea(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JCharityArea rename(String name) {
		return new JCharityArea(name, null);
	}
}
