package com.ngl.micro.charity.contribution;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Contribution endpoint.
 *
 * @author Cameron Waldron
 *
 */
@Path("contributions")
public interface ContributionEndpoint extends RestEndpoint {

	@GET
	@Path("summary")
	@RequiresScopes(Scopes.CONTRIBUTION_READ_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response summary();

}
