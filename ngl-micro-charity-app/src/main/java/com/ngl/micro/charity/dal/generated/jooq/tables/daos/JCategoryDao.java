/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables.daos;


import com.ngl.micro.charity.dal.generated.jooq.tables.JCategory;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCategoryRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JCategoryDao extends DAOImpl<JCategoryRecord, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory, Long> {

	/**
	 * Create a new JCategoryDao without any configuration
	 */
	public JCategoryDao() {
		super(JCategory.CATEGORY, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory.class);
	}

	/**
	 * Create a new JCategoryDao with an attached configuration
	 */
	public JCategoryDao(Configuration configuration) {
		super(JCategory.CATEGORY, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory> fetchByJId(Long... values) {
		return fetch(JCategory.CATEGORY.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory fetchOneByJId(Long value) {
		return fetchOne(JCategory.CATEGORY.ID, value);
	}

	/**
	 * Fetch records that have <code>value IN (values)</code>
	 */
	public List<com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory> fetchByJValue(String... values) {
		return fetch(JCategory.CATEGORY.VALUE, values);
	}

	/**
	 * Fetch a unique record that has <code>value = value</code>
	 */
	public com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCategory fetchOneByJValue(String value) {
		return fetchOne(JCategory.CATEGORY.VALUE, value);
	}
}
