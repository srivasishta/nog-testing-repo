package com.ngl.micro.charity;

import java.util.UUID;
import java.util.concurrent.Executors;

import javax.jms.ConnectionFactory;

import org.apache.shiro.realm.Realm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.PlatformTransactionManager;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.charity.charity.CharityEndpoint;
import com.ngl.micro.charity.charity.CharityEndpointImpl;
import com.ngl.micro.charity.charity.CharityRepository;
import com.ngl.micro.charity.charity.CharityService;
import com.ngl.micro.charity.charity.CharityValidator;
import com.ngl.micro.charity.contribution.ContributionDocumentRepository;
import com.ngl.micro.charity.contribution.ContributionEndpoint;
import com.ngl.micro.charity.contribution.ContributionEndpointImpl;
import com.ngl.micro.charity.contribution.ContributionService;
import com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity;
import com.ngl.micro.charity.event.in.token.AccessTokenRevokedEventListener;
import com.ngl.micro.charity.event.in.token.AccessTokenRevokedEventRepository;
import com.ngl.micro.charity.event.in.transaction.TransactionMatchedEventDeserializer;
import com.ngl.micro.charity.event.in.transaction.TransactionMatchedEventListener;
import com.ngl.micro.charity.event.in.transaction.TransactionMatchedEventRepository;
import com.ngl.micro.charity.event.out.contribution.ContributionRecordedEventPublishedListener;
import com.ngl.micro.charity.event.out.contribution.ContributionRecordedEventRepository;
import com.ngl.micro.charity.revision.VersionControlService;
import com.ngl.micro.charity.revision.VersionControlledEndpointDelegate;
import com.ngl.micro.charity.revision.jooq.JVersionControlService;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.micro.shared.contracts.oauth.AccessTokenRevokedEvent;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.messaging.core.bus.RxLocalMessageBus;
import com.ngl.middleware.messaging.core.data.deserialize.LoggingDeserializationErrorHandler;
import com.ngl.middleware.messaging.core.data.serialize.JsonMessageSerializer;
import com.ngl.middleware.messaging.jms.publish.JmsPublisher;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherConfiguration;
import com.ngl.middleware.messaging.jms.publish.JmsPublisherMetrics;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriber;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberConfiguration;
import com.ngl.middleware.messaging.jms.subscribe.SpringJmsSubscriberMetrics;
import com.ngl.middleware.metrics.MetricRegistryFactory;
import com.ngl.middleware.microservice.config.EnableMicroserviceDatabase;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.config.EnableMicroserviceMonitoring;
import com.ngl.middleware.microservice.config.EnableMicroserviceRestServer;
import com.ngl.middleware.microservice.config.MicroserviceConfiguration;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.hal.dsl.HALConfiguration;
import com.ngl.middleware.rest.server.RestEndpoints;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.AccessTokenSecurityRealm;
import com.ngl.middleware.rest.server.security.oauth2.RevocationList;
import com.ngl.middleware.rest.server.security.oauth2.jwt.JwtAccessTokenValidator;
import com.ngl.middleware.rest.server.security.oauth2.jwt.SignedEncryptedJwtFactory;
import com.ngl.middleware.util.Time;
import com.ngl.middleware.util.backoff.FixedBackOff;

/**
 * Configures the charity application.
 *
 * @author Willy du Preez
 * @author Cameron Waldron
 * @author Paco Mendes
 *
 */
@Configuration
@EnableMicroserviceDatabase
@EnableMicroserviceRestServer
@EnableMicroserviceMonitoring
@EnableMicroserviceJmsClient
public class CharityApplicationConfiguration extends MicroserviceConfiguration {

	private static final String CHARITY_SERVICE_ENDPOINT = "charity";

	@Bean
	public RestEndpoints restEndpoints(ObjectMapper mapper, CharityEndpoint charityEndpoint, ContributionEndpoint contributionEndpoint, Realm realm) {
		HALConfiguration config = new HALConfiguration(mapper);
		HAL.configureDefault(config);

		RestEndpoints restEndpoints = new RestEndpoints(CHARITY_SERVICE_ENDPOINT);
		restEndpoints.addEndpoints(charityEndpoint);
		restEndpoints.addEndpoints(contributionEndpoint);
		restEndpoints.setRealm(realm);
		return restEndpoints;
	}

	@Bean
	public MetricRegistry metricRegistry(EnvironmentConfigurationProvider config) {
		return MetricRegistryFactory.fromEnvironment(config);
	}

	/* --------------------------------------------------------
	 *  Charities
	 * -------------------------------------------------------- */

	@Bean
	public CharityRepository charityRepository(DAL dal) {
		return new CharityRepository(dal);
	}

	@Bean
	public CharityValidator charityValidator() {
		return CharityValidator.newInstance();
	}

	@Bean
	public VersionControlService<Charity, UUID> charityVersionControlService(DAL dal, ObjectMapper mapper) {
		return new JVersionControlService<>(
				mapper,
				JNglMicroCharity.NGL_MICRO_CHARITY.getName(),
				"charity_revision",
				dal);
	}

	@Bean
	public CharityService charityService(
			CharityRepository charityRepository,
			CharityValidator charityValidator,
			VersionControlService<Charity, UUID> charityVersionControlService) {
		return new CharityService(charityRepository, charityValidator, charityVersionControlService);
	}

	@Bean
	public CharityEndpoint charityEndpoint(
			RequestProcessor processor,
			CharityService charityService,
			ContributionService contributionService,
			VersionControlService<Charity, UUID> charityVersionControlService) {
		return new CharityEndpointImpl(processor,
										charityService,
										contributionService,
										new VersionControlledEndpointDelegate<>(charityVersionControlService));
	}

	/* --------------------------------------------------------
	 *  Transaction Matched Events
	 * -------------------------------------------------------- */

	@Bean
	@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
	public TransactionMatchedEventRepository transactionMatchedEventRepository(org.jooq.Configuration config) {
		return new TransactionMatchedEventRepository(config);
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber transactionMatchedEventSubscriber(
			ObjectMapper objectMapper,
			ConnectionFactory connectionFactory,
			TransactionMatchedEventRepository events,
			ContributionService contributions,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<TransactionMatchedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(10);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setSubscription("charity-" + TransactionMatchedEvent.NAME + "-subscriber");
		config.setDestination(TransactionMatchedEvent.NAME);
		config.setMessageDeserializer(new TransactionMatchedEventDeserializer(objectMapper));
		config.setMetrics(new SpringJmsSubscriberMetrics(TransactionMatchedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(new TransactionMatchedEventListener(events, contributions));

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

	/* --------------------------------------------------------
	 *  Contributions
	 * -------------------------------------------------------- */

	@Bean
	public ContributionDocumentRepository contributionDocumentRepository(org.jooq.Configuration config) {
		return new ContributionDocumentRepository(config);
	}

	@Bean
	public ContributionService contributionService(
			CharityRepository charities,
			ContributionDocumentRepository documents,
			ContributionRecordedEventRepository events,
			PlatformTransactionManager txManager) {
		return new ContributionService(charities, documents, events, this.contributionRecordedEventBus(), txManager);
	}

	@Bean
	public ContributionEndpoint contributionEndpoint(RequestProcessor processor, ContributionService contributionService) {
		return new ContributionEndpointImpl(processor, contributionService);
	}

	@Bean
	public ContributionRecordedEventRepository contributionRecordedEventRepository(
			org.jooq.Configuration config,
			ContributionDocumentRepository documents) {
		return new ContributionRecordedEventRepository(config, documents);
	}

	@Bean
	public MessageBus contributionRecordedEventBus() {
		return new RxLocalMessageBus("ContributionRecordedEventBus");
	}

	@Bean(destroyMethod = "stop")
	public JmsPublisher contributionRecordedEventPublisher(
			ConnectionFactory connectionFactory,
			ObjectMapper objectMapper,
			ContributionRecordedEventRepository events,
			MetricRegistry metricRegistry) {

		JmsPublisherConfiguration config = new JmsPublisherConfiguration();
		config.setConnectionFactory(connectionFactory);
		config.setDestination(ContributionRecordedEvent.NAME);
		config.setMessageBus(this.contributionRecordedEventBus());
		config.setProducerCount(10);
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setSerializer(new JsonMessageSerializer(objectMapper));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessagePublishedListener(new ContributionRecordedEventPublishedListener(events));
		config.setJmsPublisherMetrics(new JmsPublisherMetrics(ContributionRecordedEvent.NAME, metricRegistry));

		JmsPublisher publisher = new JmsPublisher(config);
		publisher.start();
		return publisher;
	}

	/* --------------------------------------------------------
	 *  Access Tokens
	 * -------------------------------------------------------- */

	@Bean
	public AccessTokenRevokedEventRepository accessTokenRevokedEventRepository(org.jooq.Configuration configuration) {
		return new AccessTokenRevokedEventRepository(configuration);
	}

	@Bean
	public RevocationList revocationList(AccessTokenRevokedEventRepository events) {
		return new RevocationList(events.getByExpirationDateGreaterThan(Time.utcNow()));
	}

	@Bean
	public AccessTokenRevokedEventListener accessTokenEventListener(
			ObjectMapper objectMapper,
			AccessTokenRevokedEventRepository accessTokenRevokedEventRepository,
			RevocationList revocationList) {
		return new AccessTokenRevokedEventListener(objectMapper, accessTokenRevokedEventRepository, revocationList);
	}

	@Bean
	public Realm securityRealm(RevocationList revocationList) {
		return new AccessTokenSecurityRealm(new JwtAccessTokenValidator(
				this.signedEncryptedJwtFactory(),
				revocationList));
	}

	@Bean
	public SignedEncryptedJwtFactory signedEncryptedJwtFactory() {
		return new SignedEncryptedJwtFactory();
	}

	@Bean(destroyMethod = "stop")
	public SpringJmsSubscriber accessTokenEventSubscriber(
			ConnectionFactory connectionFactory,
			AccessTokenRevokedEventListener accessTokenRevokedEventListener,
			MetricRegistry metricRegistry) {

		SpringJmsSubscriberConfiguration<AccessTokenRevokedEvent> config = new SpringJmsSubscriberConfiguration<>();
		config.setConnectionFactory(connectionFactory);
		config.setConsumerCount(1);
		config.setDeserializationErrorHandler(new LoggingDeserializationErrorHandler());
		config.setDestination(AccessTokenRevokedEvent.NAME);
		config.setSubscription("charity-" + AccessTokenRevokedEvent.NAME + "-subscriber");
		config.setMessageDeserializer(accessTokenRevokedEventListener);
		config.setMetrics(new SpringJmsSubscriberMetrics(AccessTokenRevokedEvent.NAME, metricRegistry));
		config.setRetryPolicy(new FixedBackOff(10_000));
		config.setThreadFactory(Executors.defaultThreadFactory());
		config.setMessageListener(accessTokenRevokedEventListener);

		SpringJmsSubscriber subscriber = new SpringJmsSubscriber(config);
		subscriber.start();
		return subscriber;
	}

}
