package com.ngl.micro.charity.charity;

import java.util.Set;
import java.util.UUID;

import com.ngl.micro.charity.Country;

public class CharityArea {

	private UUID charityId;
	private Country country;
	private Set<String> regions;

	public UUID getCharityId() {
		return charityId;
	}

	public void setCharityId(UUID charityId) {
		this.charityId = charityId;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Set<String> getRegions() {
		return regions;
	}

	public void setRegions(Set<String> regions) {
		this.regions = regions;
	}


}
