package com.ngl.micro.charity.event.out.contribution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.middleware.messaging.api.Message;
import com.ngl.middleware.messaging.core.publish.MessagePublishedListener;

/**
 * Listens for {@link ContributionRecordedEvent}.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEventPublishedListener implements MessagePublishedListener {

	private static final Logger log = LoggerFactory.getLogger(ContributionRecordedEventPublishedListener.class);

	private ContributionRecordedEventRepository events;

	public ContributionRecordedEventPublishedListener(ContributionRecordedEventRepository events) {
		this.events = events;
	}

	@Override
	public void onPublished(Message<?> message) {
		if (message instanceof ContributionRecordedEvent) {
			this.events.markPublished(((ContributionRecordedEvent) message).getId());
		}
		else {
			log.warn("Unexpected message type received: {}", message == null ? null : message.getClass().getName());
		}
	}

}
