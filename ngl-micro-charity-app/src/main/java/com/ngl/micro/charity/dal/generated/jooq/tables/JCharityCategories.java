/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables;


import com.ngl.micro.charity.dal.generated.jooq.JNglMicroCharity;
import com.ngl.micro.charity.dal.generated.jooq.Keys;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCharityCategoriesRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JCharityCategories extends TableImpl<JCharityCategoriesRecord> {

	private static final long serialVersionUID = 1975268686;

	/**
	 * The reference instance of <code>ngl_micro_charity.charity_categories</code>
	 */
	public static final JCharityCategories CHARITY_CATEGORIES = new JCharityCategories();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<JCharityCategoriesRecord> getRecordType() {
		return JCharityCategoriesRecord.class;
	}

	/**
	 * The column <code>ngl_micro_charity.charity_categories.charity_id</code>.
	 */
	public final TableField<JCharityCategoriesRecord, Long> CHARITY_ID = createField("charity_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * The column <code>ngl_micro_charity.charity_categories.category_id</code>.
	 */
	public final TableField<JCharityCategoriesRecord, Long> CATEGORY_ID = createField("category_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * Create a <code>ngl_micro_charity.charity_categories</code> table reference
	 */
	public JCharityCategories() {
		this("charity_categories", null);
	}

	/**
	 * Create an aliased <code>ngl_micro_charity.charity_categories</code> table reference
	 */
	public JCharityCategories(String alias) {
		this(alias, CHARITY_CATEGORIES);
	}

	private JCharityCategories(String alias, Table<JCharityCategoriesRecord> aliased) {
		this(alias, aliased, null);
	}

	private JCharityCategories(String alias, Table<JCharityCategoriesRecord> aliased, Field<?>[] parameters) {
		super(alias, JNglMicroCharity.NGL_MICRO_CHARITY, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<JCharityCategoriesRecord> getPrimaryKey() {
		return Keys.KEY_CHARITY_CATEGORIES_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<JCharityCategoriesRecord>> getKeys() {
		return Arrays.<UniqueKey<JCharityCategoriesRecord>>asList(Keys.KEY_CHARITY_CATEGORIES_PRIMARY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<JCharityCategoriesRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<JCharityCategoriesRecord, ?>>asList(Keys.CHARITY_CATEGORIES_IBFK_1, Keys.CHARITY_CATEGORIES_IBFK_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JCharityCategories as(String alias) {
		return new JCharityCategories(alias, this);
	}

	/**
	 * Rename this table
	 */
	public JCharityCategories rename(String name) {
		return new JCharityCategories(name, null);
	}
}
