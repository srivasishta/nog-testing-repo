package com.ngl.micro.charity.charity.constraint;

import org.hibernate.validator.cfg.ConstraintDef;

public class ReachDef extends ConstraintDef<ReachDef, ValidReach> {

	public ReachDef() {
		super(ValidReach.class);
	}
}
