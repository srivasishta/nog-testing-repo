/**
 * This class is generated by jOOQ
 */
package com.ngl.micro.charity.dal.generated.jooq.tables.daos;


import com.ngl.micro.charity.dal.generated.jooq.tables.JCharityStatusType;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCharityStatusTypeRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JCharityStatusTypeDao extends DAOImpl<JCharityStatusTypeRecord, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType, Long> {

	/**
	 * Create a new JCharityStatusTypeDao without any configuration
	 */
	public JCharityStatusTypeDao() {
		super(JCharityStatusType.CHARITY_STATUS_TYPE, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType.class);
	}

	/**
	 * Create a new JCharityStatusTypeDao with an attached configuration
	 */
	public JCharityStatusTypeDao(Configuration configuration) {
		super(JCharityStatusType.CHARITY_STATUS_TYPE, com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType> fetchByJId(Long... values) {
		return fetch(JCharityStatusType.CHARITY_STATUS_TYPE.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType fetchOneByJId(Long value) {
		return fetchOne(JCharityStatusType.CHARITY_STATUS_TYPE.ID, value);
	}

	/**
	 * Fetch records that have <code>value IN (values)</code>
	 */
	public List<com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType> fetchByJValue(String... values) {
		return fetch(JCharityStatusType.CHARITY_STATUS_TYPE.VALUE, values);
	}

	/**
	 * Fetch a unique record that has <code>value = value</code>
	 */
	public com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusType fetchOneByJValue(String value) {
		return fetchOne(JCharityStatusType.CHARITY_STATUS_TYPE.VALUE, value);
	}
}
