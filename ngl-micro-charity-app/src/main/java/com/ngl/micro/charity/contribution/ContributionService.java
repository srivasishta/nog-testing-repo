package com.ngl.micro.charity.contribution;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import com.ngl.micro.charity.charity.CharityRepository;
import com.ngl.micro.charity.event.out.contribution.ContributionRecordedEventRepository;
import com.ngl.micro.contribution.CategoryContributions;
import com.ngl.micro.contribution.CharityContributions;
import com.ngl.micro.contribution.NetworkContributions;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.charity.ContributionAllocation;
import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.charity.ContributionRecordedEvent;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.messaging.core.bus.MessageBus;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;
import com.ngl.middleware.util.Time;

/**
 * Contribution service.
 *
 * @author Willy du Preez
 *
 */
public class ContributionService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ContributionService.class);

	private static final BigDecimal ONE_HUNDRED = new BigDecimal("100.00000");
	private static final Integer TOTAL_ALLOCATION = 100;

	private CharityRepository charities;
	private ContributionDocumentRepository documents;
	private ContributionRecordedEventRepository events;
	private MessageBus eventBus;
	private TransactionTemplate txTemplate;

	public ContributionService(
			CharityRepository charities,
			ContributionDocumentRepository documents,
			ContributionRecordedEventRepository events,
			MessageBus eventBus,
			PlatformTransactionManager txManager) {

		Assert.notNull(charities);
		Assert.notNull(documents);
		Assert.notNull(events);
		Assert.notNull(eventBus);
		Assert.notNull(txManager);

		this.charities = charities;
		this.documents = documents;
		this.events = events;
		this.eventBus = eventBus;
		this.txTemplate = new TransactionTemplate(txManager);
	}

	public void calculate(TransactionMatchedEvent in) {
		// Contribution has already been calculated.
		if (this.documents.getDocument(in.getTransactionGroupId()).isPresent()) {
			return;
		}

		ZonedDateTime now = Time.utcNow();

		ContributionDocument.Builder builder = ContributionDocument.builder()
				.id(in.getTransactionGroupId())
				.createdDate(now)
				.lastModifiedDate(now)
				.businessId(in.getBusinessId())
				.businessStoreId(in.getBusinessStoreId())
				.businessStoreContributionRate(in.getBusinessStoreContributionRate())
				.membershipId(in.getMembershipId())
				.membershipCharityChoices(in.getMembershipCharityChoices())
				.membershipDemographics(in.getMembershipDemographics())
				.partnerId(in.getPartnerId())
				.transactionAmount(in.getTransactionAmount())
				.transactionCurrency(in.getTransactionCurrency())
				.transactionDate(in.getTransactionDate())
				.transactionStatus(in.getTransactionStatus())
				.transactionTimezone(in.getTransactionTimezone());

		BigDecimal contributionAmount = this.calculateContributionAmount(
				in.getTransactionAmount(),
				in.getBusinessStoreContributionRate());

		Set<ContributionAllocation> allocations = this.calculateAllocations(
				contributionAmount,
				in.getTransactionDate(),
				in.getMembershipCharityChoices());

		ContributionDocument document = builder
				.contributionAmount(contributionAmount)
				.contributionAllocations(allocations)
				.contributionDate(now)
				.build();

		ContributionRecordedEvent out = this.txTemplate.execute(status -> {
			ContributionRecordedEvent event = new ContributionRecordedEvent(
					in.getId(),
					in.getCreated(),
					this.documents.saveDocument(document));
			return this.events.addEvent(event);
		});

		if (out != null) {
			this.eventBus.send(out);
		}
	}

	private BigDecimal calculateContributionAmount(BigDecimal transactionAmount, BigDecimal contributionRate) {
		return transactionAmount.multiply(contributionRate);
	}

	private Set<ContributionAllocation> calculateAllocations(
			BigDecimal contributionAmount,
			ZonedDateTime transactionDate,
			Set<MembershipCharityChoice> charityChoices) {

		if (charityChoices.isEmpty()) {
			return defaultCharityAllocation(contributionAmount);
		} else {
			return memberChoiceAllocation(contributionAmount, charityChoices, transactionDate);
		}
	}

	private Set<ContributionAllocation> defaultCharityAllocation(BigDecimal amount) {
		UUID defaultCharity = this.charities.getDefaultCharityId();
		Set<ContributionAllocation> allocations = new HashSet<>();
		allocations.add(new ContributionAllocation(
				defaultCharity,
				amount,
				TOTAL_ALLOCATION));
		return allocations;
	}

	private Set<ContributionAllocation> memberChoiceAllocation(BigDecimal contributionAmount, Set<MembershipCharityChoice> charityChoices, ZonedDateTime transactionDate) {

		Map<UUID, ResourceStatus> statuses = this.getCharityStatusesForMemberChoice(charityChoices, transactionDate);
		Set<ContributionAllocation> allocations = new HashSet<>();

		for (MembershipCharityChoice choice : charityChoices) {

			UUID allocationCharity = choice.getCharityId();
			BigDecimal charityAllocation = new BigDecimal(choice.getAllocation()).divide(ONE_HUNDRED);
			BigDecimal allocationAmount = charityAllocation.multiply(contributionAmount);

			if (!isActiveCharity(allocationCharity, statuses)) {
				allocationCharity = this.charities.getDefaultCharityId();
			}
			ContributionAllocation allocation = new ContributionAllocation(
					allocationCharity,
					allocationAmount,
					choice.getAllocation());

			allocations.add(allocation);
		}
		return allocations;
	}

	private Map<UUID, ResourceStatus> getCharityStatusesForMemberChoice(Set<MembershipCharityChoice> charityChoices, ZonedDateTime updatedDate) {
		Set<UUID> charityIds = charityChoices.stream()
			.map(c -> c.getCharityId())
			.collect(Collectors.toSet());

		return this.charities.getCharityStatusesByResourceIdsAt(charityIds, updatedDate);
	}

	private boolean isActiveCharity(UUID charity, Map<UUID, ResourceStatus> statuses) {
		return EqualsUtils.areEqual(ResourceStatus.ACTIVE, statuses.get(charity));
	}

	public NetworkContributions getNetworkContributions(SearchQuery query) {
		Set<CategoryContributions> categoryContributions = new HashSet<>();
		this.documents.getTotalsByCategory(query).forEach((k, v) -> {
			CategoryContributions cc = new CategoryContributions();
			cc.setCategory(k);
			cc.setTotal(v);
			categoryContributions.add(cc);
		});

		NetworkContributions networkContributions = new NetworkContributions();
		networkContributions.setCategoryContributions(categoryContributions);
		networkContributions.setTotal(this.documents.getTotalContributions(query));
		return networkContributions;
	}

	public CharityContributions getCharityContributions(UUID id, SearchQuery query) {
		CharityContributions contributions = new CharityContributions();
		contributions.setTotal(this.documents.getTotalContributionsByCharityId(id, query));
		return contributions;
	}

}
