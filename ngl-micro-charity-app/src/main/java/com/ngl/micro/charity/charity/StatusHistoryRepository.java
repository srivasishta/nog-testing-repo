package com.ngl.micro.charity.charity;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_STATUS_HISTORY;
import static java.util.stream.Collectors.toList;

import java.time.ZonedDateTime;
import java.util.List;

import org.jooq.Result;

import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCharityStatusHistoryRecord;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.util.Experimental;

/**
 * Tracks charity status history on all updates.
 *
 * @author Paco Mendes
 */
public class StatusHistoryRepository {

	private DAL support;

	public StatusHistoryRepository(DAL dal) {
		this.support = dal;
	}

	public void setCharityStatus(Long charityId, CharityStatus status, String reason, ZonedDateTime updatedDate) {
		JCharityStatusHistoryRecord currentStatus = this.getStatusRecordAt(charityId, updatedDate);
		if (currentStatus == null) {
			this.addStatusRecord(charityId, status, reason, updatedDate);
		} else if (this.updated(currentStatus, status, reason)) {
			this.expireStatusRecord(currentStatus, updatedDate);
			this.addStatusRecord(charityId, status, reason, updatedDate);
		}
	}

	private boolean updated(JCharityStatusHistoryRecord currentStatus, CharityStatus status, String reason) {
		return !currentStatus.getStatusId().equals(ResourceStatus.idFrom(status)) ||
				!currentStatus.getStatusReason().equals(reason);
	}

	private void addStatusRecord(Long MembershipId, CharityStatus status, String reason, ZonedDateTime startDate) {
		JCharityStatusHistoryRecord record =  this.support.sql().newRecord(CHARITY_STATUS_HISTORY);
		record.setCharityId(MembershipId);
		record.setStatusId(ResourceStatus.idFrom(status));
		record.setStatusReason(reason);
		record.setStartDate(startDate);
		record.store();
	}

	private void expireStatusRecord(JCharityStatusHistoryRecord currentStatus, ZonedDateTime updatedDate) {
		currentStatus.setEndDate(updatedDate);
		currentStatus.store();
	}

	public CharityStatusRecord getByCharityIdAt(Long charityId, ZonedDateTime updatedDate) {
		JCharityStatusHistoryRecord record = this.getStatusRecordAt(charityId, updatedDate);
		return record == null ? null : this.map(record);
	}

	public JCharityStatusHistoryRecord getStatusRecordAt(Long charityId, ZonedDateTime updatedDate) {
		return this.support.sql()
				.selectFrom(CHARITY_STATUS_HISTORY)
				.where(CHARITY_STATUS_HISTORY.CHARITY_ID.eq(charityId))
				.and(CHARITY_STATUS_HISTORY.START_DATE.lessOrEqual(updatedDate))
				.and(CHARITY_STATUS_HISTORY.END_DATE.greaterThan(updatedDate)
						.or(CHARITY_STATUS_HISTORY.END_DATE.isNull()))
				.fetchOne();
	}

	@Experimental
	public List<CharityStatusRecord> list(Long charityId) {
		Result<JCharityStatusHistoryRecord> records = this.support.sql()
				.selectFrom(CHARITY_STATUS_HISTORY)
				.where(CHARITY_STATUS_HISTORY.CHARITY_ID.eq(charityId))
				.fetch();

		return records.stream().map(this::map).collect(toList());
	}

	private CharityStatusRecord map(JCharityStatusHistoryRecord record) {
		CharityStatusRecord status = new CharityStatusRecord();
		status.setId(record.getId());
		status.setStatus(CharityStatus.valueOf(ResourceStatus.forId(record.getStatusId()).name()));
		status.setStatusReason(record.getStatusReason());
		status.setStartDate(record.getStartDate());
		status.setEndDate(record.getEndDate());
		return status;
	}
}
