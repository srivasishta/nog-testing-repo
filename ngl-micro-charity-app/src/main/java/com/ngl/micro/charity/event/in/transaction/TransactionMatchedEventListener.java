package com.ngl.micro.charity.event.in.transaction;

import org.springframework.util.Assert;

import com.ngl.micro.charity.contribution.ContributionService;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.messaging.core.subscribe.listener.MessageListener;

/**
 * Listens for {@link TransactionMatchedEvent}s.
 *
 * @author Willy du Preez
 *
 */
public class TransactionMatchedEventListener implements MessageListener<TransactionMatchedEvent> {

	private TransactionMatchedEventRepository events;
	private ContributionService contributions;

	public TransactionMatchedEventListener(
			TransactionMatchedEventRepository events,
			ContributionService contributions) {

		Assert.notNull(events);
		Assert.notNull(contributions);

		this.events = events;
		this.contributions = contributions;
	}

	@Override
	public void onMessage(TransactionMatchedEvent event) {
		this.contributions.calculate(event);
		this.events.addEvent(event);
	}

}
