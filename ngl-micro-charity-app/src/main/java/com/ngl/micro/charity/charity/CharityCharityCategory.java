package com.ngl.micro.charity.charity;

import java.util.UUID;

import com.ngl.micro.charity.Charity.CharityCategory;

public class CharityCharityCategory {
	private UUID charityId;
	private CharityCategory category;

	public UUID getCharityId() {
		return this.charityId;
	}

	public void setCharityId(UUID charityId) {
		this.charityId = charityId;
	}

	public CharityCategory getCategory() {
		return this.category;
	}

	public void setCategory(CharityCategory category) {
		this.category = category;
	}

}
