package com.ngl.micro.charity.charity;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * A persisted charity category type.
 *
 * @author Paco Mendes
 */
public enum CharityCategoryType {

	HEALTH(1L),
	INCLUSION(2L),
	EDUCATION(3L),
	ENVIRONMENT(4L);

	public static CharityCategoryType forId(Long id) {
		Assert.notNull(id);

		for (CharityCategoryType status : CharityCategoryType.values()) {
			if (status.id == id) {
				return status;
			}
		}
		throw new NoSuchElementException("No CharityCategoryType with ID: " + id);
	}

	public static CharityCategoryType forEnum(Enum<?> named) {
		Assert.notNull(named);

		String name = named.name();
		for (CharityCategoryType status : CharityCategoryType.values()) {
			if (status.name().equals(name)) {
				return status;
			}
		}
		throw new NoSuchElementException("No CharityCategoryType for Enum with name: " + name);
	}

	public static String nameForId(Long id) {
		return CharityCategoryType.forId(id).name();
	}

	public static Long idFrom(Enum<?> named) {
		Assert.notNull(named);
		return CharityCategoryType.valueOf(named.name()).getId();
	}

	private Long id;

	private CharityCategoryType(long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

}
