package com.ngl.micro.charity.charity;

import static com.ngl.micro.shared.contracts.oauth.Scopes.CHARITY_READ_SCOPE;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.contribution.ContributionService;
import com.ngl.micro.charity.revision.VersionControlledEndpoint;
import com.ngl.micro.charity.revision.VersionControlledEndpointDelegate;
import com.ngl.micro.contribution.CharityContributions;
import com.ngl.middleware.rest.api.page.Fields;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.hal.Resource;
import com.ngl.middleware.rest.hal.dsl.HAL;
import com.ngl.middleware.rest.server.request.RequestProcessor;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Charity endpoint implementation.
 *
 * @author Willy du Preez
 *
 */
public class CharityEndpointImpl implements CharityEndpoint, VersionControlledEndpoint {

	@Context
	private MessageContext msgCtx;

	private RequestProcessor processor;

	private CharityService charityService;

	private ContributionService contributionService;

	private VersionControlledEndpointDelegate<Charity, UUID> vc;

	public CharityEndpointImpl(RequestProcessor processor,
				CharityService charityService,
				ContributionService contributionService,
				VersionControlledEndpointDelegate<Charity, UUID> vc) {
		this.processor = processor;
		this.charityService = charityService;
		this.contributionService = contributionService;
		this.vc = vc;
	}

	@Override
	public Response get(UUID id) {
		Fields fields = this.processor.fields(this.msgCtx);
		Charity charity = this.charityService.getCharityById(id);

		Resource resource = HAL.resource(charity)
				.fieldFilter(fields)
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response list() {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		Page<Charity> page = this.charityService.getCharitys(pageRequest);

		Resource resource = HAL.resource(page)
				.fieldFilter(pageRequest.getFields())
				.build();

		return Response.ok(resource).build();
	}

	@Override
	public Response create(Charity charity) {
		charity = this.charityService.createCharity(charity);
		Resource resource = HAL.resource(charity).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response update(UUID id, Patch patch) {
		Charity charity = this.charityService.updateCharity(id, patch);
		Resource resource = HAL.resource(charity).build();
		return Response.ok(resource).build();
	}

	@Override
	public Response summary(UUID id) {
		SearchQuery query = this.processor.pageRequest(this.msgCtx).getSearchQuery();
		CharityContributions contributions = this.contributionService.getCharityContributions(id, query);
		Resource resource = HAL.resource(contributions).build();
		return Response.ok(resource).build();
	}

	@Override
	@RequiresScopes(CHARITY_READ_SCOPE)
	public Response getRevisions(UUID resourceId) {
		PageRequest pageRequest = this.processor.pageRequest(this.msgCtx);
		return this.vc.getRevisions(resourceId, pageRequest);
	}

	@Override
	@RequiresScopes(CHARITY_READ_SCOPE)
	public Response getRevision(UUID resourceId, UUID revisionId) {
		return this.vc.getRevision(resourceId, revisionId);
	}

	@Override
	@RequiresScopes(CHARITY_READ_SCOPE)
	public Response getLatestRevision(UUID resourceId) {
		return this.vc.getLatestRevision(resourceId);
	}

}
