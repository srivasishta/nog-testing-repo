package com.ngl.micro.charity.charity;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CATEGORY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_AREA;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_CATEGORIES;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_STATUS_HISTORY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_STATUS_TYPE;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.ImageSet;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.dal.generated.jooq.Tables;
import com.ngl.micro.charity.dal.generated.jooq.tables.JCategory;
import com.ngl.micro.charity.dal.generated.jooq.tables.JCharityArea;
import com.ngl.micro.charity.dal.generated.jooq.tables.records.JCharityRecord;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.dal.repository.PagingRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.database.portability.SQLBooleans;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.util.Strings;
import com.ngl.middleware.util.Time;

/**
 * Charity repository.
 *
 * @author Willy du Preez
 * @author Paco Mendes
 */
public class CharityRepository implements PagingRepository<Charity, UUID> {

	private static final String PRIMARY_KEY = "pk";

	private static final JCategory CATEGORY_ALIAS = CATEGORY.as("category");
	private static final JCharityArea CHARITY_AREA_ALIAS = Tables.CHARITY_AREA.as("charity_area");

	private DAL support;
	private TableOnConditionStep<?> tables;
	private JooqQuery query;
	private FieldMap<String> fieldMap;
	private FieldMap<String> linkedFieldMap;

	private LinkedAreas linkedAreas;
	private LinkedCharityCategories linkedCategories;
	private StatusHistoryRepository statusHistory;

	public CharityRepository(DAL dal) {
		this.support = dal;

		this.linkedAreas = new LinkedAreas(dal);
		this.linkedCategories = new LinkedCharityCategories(dal);
		this.statusHistory = new StatusHistoryRepository(dal);

		this.fieldMap = new FieldMap<String>()
				.bind(Charity.Fields.ID, new MappedField(CHARITY.RESOURCE_ID).searchable(true))
				.bind(Charity.Fields.NAME, new MappedField(CHARITY.NAME).searchable(true).sortable(true))
				.bind(Charity.Fields.DESCRIPTION, new MappedField(CHARITY.DESCRIPTION).searchable(true))
				.bind(Charity.Fields.STATUS, new MappedField(CHARITY_STATUS_TYPE.VALUE).searchable(true))
				.bind(Charity.Fields.STATUS_REASON, new MappedField(CHARITY_STATUS_HISTORY.STATUS_REASON).searchable(true))
				.bind(Charity.Fields.REACH_TYPE, new MappedField(CHARITY.REACH_TYPE).searchable(true))
				.bind(Charity.Fields.LOGO_IMAGES_ORIGINAL, CHARITY.LOGO_ORIGINAL)
				.bind(Charity.Fields.LOGO_IMAGES_THUMBNAIL, CHARITY.LOGO_SMALL)
				.bind(Charity.Fields.LOGO_IMAGES_MEDIUM, CHARITY.LOGO_MEDIUM)
				.bind(Charity.Fields.LOGO_IMAGES_LARGE, CHARITY.LOGO_LARGE)
				.bind(PRIMARY_KEY, CHARITY.ID);

		this.linkedFieldMap = new FieldMap<String>()
				.bindAll(this.fieldMap)
				.bind(Charity.Fields.CATEGORIES, new MappedField(CATEGORY_ALIAS.VALUE).as("category").searchable(true))
				.bind(Charity.ReachSearchFields.COUNTRY, new MappedField(CHARITY_AREA_ALIAS.COUNTRY).as("country").searchable(true))
				.bind(Charity.ReachSearchFields.REGION, new MappedField(CHARITY_AREA_ALIAS.REGION).as("region").searchable(true));

		this.tables = CHARITY
				.join(CHARITY_STATUS_HISTORY).on(CHARITY.ID.eq(CHARITY_STATUS_HISTORY.CHARITY_ID))
				.join(CHARITY_STATUS_TYPE).on(CHARITY_STATUS_HISTORY.STATUS_ID.eq(CHARITY_STATUS_TYPE.ID))
				.and(CHARITY_STATUS_HISTORY.END_DATE.isNull());

		this.query = JooqQuery
				.builder(this.support, this.fieldMap)
				.from(this.tables)
				.build();
	}

	@Override
	public Charity add(Charity charity) {
		JCharityRecord record = this.support.sql().newRecord(CHARITY);
		record.setResourceId(charity.getId());
		record.setName(charity.getName());
		record.setDescription(charity.getDescription());
		record.setReachType(charity.getReach().getType().name());
		record.setDefaultCharity(SQLBooleans.toString(false));
		record.store();

		Long charityId = record.getId();
		ZonedDateTime updatedDate = Time.utcNow();
		this.linkedAreas.add(charityId, charity.getReach().getAreas());
		this.linkedCategories.add(charityId, charity.getCategories());
		this.statusHistory.setCharityStatus(charityId, charity.getStatus(), charity.getStatusReason(), updatedDate);
		return this.get(charity.getId());
	}

	@Override
	public Charity get(UUID id) {
		Record record = this.query.getRecordWhere(CHARITY.RESOURCE_ID.eq(id));
		if (record == null) {
			return null;
		} else {
			Charity charity = this.mapRecord(record);
			Long internalId = record.getValue(CHARITY.ID);
			Set<Area> areas = this.linkedAreas.list(internalId);
			charity.getReach().setAreas(areas);
			charity.setCategories(this.linkedCategories.list(internalId));
			return charity;
		}
	}

	public UUID getDefaultCharityId() {
		Record record = this.query.getRecordWhere(CHARITY.DEFAULT_CHARITY.eq(SQLBooleans.toString(true)));
		if (record == null) {
			return null;
		} else {
			return record.getValue(CHARITY.RESOURCE_ID);
		}
	}

	@Override
	public Page<Charity> get(PageRequest pageRequest) {
		boolean qByCategoryAndArea = pageRequest.getSearchQuery().getConditions()
				.stream()
				.map(c -> c.getProperty())
				.anyMatch(p -> p.equals(Charity.Fields.CATEGORIES)
						|| p.equals(Charity.ReachSearchFields.COUNTRY)
						|| p.equals(Charity.ReachSearchFields.REGION));

		Page<Charity> resultPage;
		if (qByCategoryAndArea) {
			TableOnConditionStep<?> from = this.tables
				.leftOuterJoin(CHARITY_CATEGORIES).on(CHARITY.ID.eq(CHARITY_CATEGORIES.CHARITY_ID))
				.leftOuterJoin(CATEGORY_ALIAS).on(CHARITY_CATEGORIES.CATEGORY_ID.eq(CATEGORY_ALIAS.ID))
				.leftOuterJoin(CHARITY_AREA).on(CHARITY.ID.eq(CHARITY_AREA.CHARITY_ID));

			resultPage = this.query.getPage(
					pageRequest,
					() -> this.linkedFieldMap,
					() -> from,
					q -> {
						q.addGroupBy(CHARITY.ID);
					},
					this::mapRecord);
		}
		else {
			resultPage = this.query.getPage(pageRequest, this::mapRecord);
		}

		this.injectCharityCategories(resultPage);
		this.injectAreas(resultPage);
		return resultPage;
	}

	private void injectCharityCategories(Page<Charity> charityPage) {
		Map<UUID, Set<CharityCategory>> resultPageCategories = this.linkedCategories.list(charityPage);

		charityPage.getItems()
			.stream()
			.forEach(c -> { c.setCategories(resultPageCategories.get(c.getId())); });
	}

	private void injectAreas(Page<Charity> charityPage) {
		Map<UUID, Set<Area>> resultPageAreas = this.linkedAreas.list(charityPage);

		charityPage.getItems()
			.stream()
			.forEach(c -> { c.getReach().setAreas(resultPageAreas.get(c.getId())); });
	}

	private Charity mapRecord(Record record) {
		Charity charity = new Charity();
		charity.setId(record.getValue(CHARITY.RESOURCE_ID));
		charity.setName(record.getValue(CHARITY.NAME));
		charity.setDescription(record.getValue(CHARITY.DESCRIPTION));
		charity.setStatus(CharityStatus.valueOf(record.getValue(CHARITY_STATUS_TYPE.VALUE)));
		charity.setStatusReason(record.getValue(CHARITY_STATUS_HISTORY.STATUS_REASON));
		Reach reach = new Reach();
		reach.setType(ReachType.valueOf(record.getValue(CHARITY.REACH_TYPE)));
		charity.setReach(reach);
		String logoOriginal = record.getValue(CHARITY.LOGO_ORIGINAL);
		String logoSmall = record.getValue(CHARITY.LOGO_SMALL);
		String logoMedium = record.getValue(CHARITY.LOGO_MEDIUM);
		String logoLarge = record.getValue(CHARITY.LOGO_LARGE);
		if (!Strings.isNullOrWhitespace(logoOriginal)
				|| !Strings.isNullOrWhitespace(logoSmall)
				|| !Strings.isNullOrWhitespace(logoMedium)
				|| !Strings.isNullOrWhitespace(logoLarge)) {
			ImageSet logos = new ImageSet();
			logos.setOriginal(logoOriginal);
			logos.setThumbnail(logoSmall);
			logos.setLarge(logoLarge);
			logos.setMedium(logoMedium);
			charity.setLogoImages(logos);
		}
		return charity;
	}

	@Override
	public void update(Charity charity) {
		JCharityRecord record = this.support.sql().fetchOne(CHARITY, CHARITY.RESOURCE_ID.eq(charity.getId()));
		record.setName(charity.getName());
		record.setDescription(charity.getDescription());
		record.setReachType(charity.getReach().getType().name());
		ImageSet logos = charity.getLogoImages() == null ? new ImageSet() : charity.getLogoImages();
		record.setLogoOriginal(logos.getOriginal());
		record.setLogoSmall(logos.getThumbnail());
		record.setLogoMedium(logos.getMedium());
		record.setLogoLarge(logos.getLarge());
		record.store();
		Long charityId = record.getId();
		ZonedDateTime updatedDate = Time.utcNow();
		this.linkedAreas.update(charityId, charity.getReach().getAreas());
		this.linkedCategories.update(charityId, charity.getCategories());
		this.statusHistory.setCharityStatus(charityId, charity.getStatus(), charity.getStatusReason(), updatedDate);
	}

	@Override
	public void delete(UUID id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public long size() {
		return this.query.size();
	}

	public Map<UUID, ResourceStatus> getCharityStatusesByResourceIdsAt(Set<UUID> resourceIds, ZonedDateTime updatedDate) {

		return this.support.sql()
				.select(CHARITY.RESOURCE_ID, CHARITY_STATUS_HISTORY.STATUS_ID)
				.from(CHARITY
				.join(CHARITY_STATUS_HISTORY).on(CHARITY.ID.eq(CHARITY_STATUS_HISTORY.CHARITY_ID)))
				.where(CHARITY.RESOURCE_ID.in(resourceIds))
				.and(CHARITY_STATUS_HISTORY.START_DATE.lessOrEqual(updatedDate))
				.and(CHARITY_STATUS_HISTORY.END_DATE.greaterThan(updatedDate)
						.or(CHARITY_STATUS_HISTORY.END_DATE.isNull()))
				.fetch()
				.stream()
				.collect(Collectors.toMap(
						r -> r.getValue(CHARITY.RESOURCE_ID),
						r -> ResourceStatus.forId(r.getValue(CHARITY_STATUS_HISTORY.STATUS_ID))));
	}

}
