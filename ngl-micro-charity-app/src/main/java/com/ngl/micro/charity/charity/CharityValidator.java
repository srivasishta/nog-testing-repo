package com.ngl.micro.charity.charity;

import static java.lang.annotation.ElementType.FIELD;

import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.defs.NotNullDef;
import org.hibernate.validator.cfg.defs.SizeDef;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.charity.constraint.ReachDef;
import com.ngl.middleware.rest.server.validation.ResourceValidator;

public class CharityValidator extends ResourceValidator<Charity> {

	public static CharityValidator newInstance() {
		return ResourceValidator.configurator(new CharityValidator()).configure();
	}

	private CharityValidator() {
	}

	@Override
	protected void configure(ConstraintMapping mapping) {
		mapping.type(Charity.class)
			.property("name", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(60))
			.property("description", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1).max(255))
			.property("status", FIELD)
				.constraint(new NotNullDef())
			.property("statusReason", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(0).max(50))
			.property("categories", FIELD)
				.constraint(new NotNullDef())
			.property("reach", FIELD)
				.constraint(new ReachDef())
				.valid()
		.type(Reach.class)
			.property("type", FIELD)
				.constraint(new NotNullDef())
			.property("areas", FIELD)
				.constraint(new NotNullDef())
				.valid()
		.type(Area.class)
			.property("country", FIELD)
				.constraint(new NotNullDef())
			.property("regions", FIELD)
				.constraint(new NotNullDef())
				.constraint(new SizeDef().min(1));
	}

}
