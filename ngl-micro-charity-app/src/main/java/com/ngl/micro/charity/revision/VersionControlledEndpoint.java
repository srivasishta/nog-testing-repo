package com.ngl.micro.charity.revision;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.middleware.util.Beta;

@Beta(description =  "To be moved into middleware.")
public interface VersionControlledEndpoint {

	@GET
	@Path("{resourceId}/revisions")
	@Produces(MediaType.APPLICATION_JSON)
	Response getRevisions(@PathParam("resourceId") UUID resourceId);

	@GET
	@Path("{resourceId}/revisions/latest")
	@Produces(MediaType.APPLICATION_JSON)
	Response getLatestRevision(@PathParam("resourceId") UUID resourceId);

	@GET
	@Path("{resourceId}/revisions/{revisionId}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getRevision(@PathParam("resourceId") UUID resourceId, @PathParam("revisionId") UUID revisionId);

}
