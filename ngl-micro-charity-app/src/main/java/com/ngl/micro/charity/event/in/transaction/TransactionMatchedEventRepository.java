package com.ngl.micro.charity.event.in.transaction;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.TRANSACTION_MATCHED_EVENT;

import java.util.Optional;
import java.util.UUID;

import org.jooq.Configuration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ngl.micro.charity.dal.generated.jooq.tables.records.JTransactionMatchedEventRecord;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.dal.repository.EventRepository;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.database.portability.SQLBooleans;

/**
 * Manages {@link TransactionMatchedEvent}s.
 *
 * @author Willy du Preez
 *
 */
@Transactional(rollbackFor = Exception.class)
public class TransactionMatchedEventRepository implements EventRepository<TransactionMatchedEvent, Long> {

	private DAL dal;

	public TransactionMatchedEventRepository(Configuration config) {
		this.dal = new DAL(config);
	}

	@Override
	public TransactionMatchedEvent addEvent(TransactionMatchedEvent event) {
		Assert.notNull(event);

		JTransactionMatchedEventRecord record = this.dal.sql().newRecord(TRANSACTION_MATCHED_EVENT);
		record.setId(event.getId());
		record.setCreatedDate(event.getCreated());
		record.setContributionDocumentId(event.getTransactionGroupId());
		record.setProcessed(SQLBooleans.toString(true));
		record.store();

		return event;
	}

	@Override
	public Optional<TransactionMatchedEvent> getEvent(Long id) {
		throw new UnsupportedOperationException("Use TransactionMatchedEventRepository.getEventHistory()");
	}

	public Optional<TransactionMatchedEventHistory> getEventHistory(Long id) {
		Assert.notNull(id);

		JTransactionMatchedEventRecord record = this.dal.sql()
				.selectFrom(TRANSACTION_MATCHED_EVENT)
				.where(TRANSACTION_MATCHED_EVENT.ID.eq(id))
				.fetchOne();

		if (record == null) {
			return Optional.empty();
		}

		UUID documentId = record.getValue(TRANSACTION_MATCHED_EVENT.CONTRIBUTION_DOCUMENT_ID);

		return Optional.of(new TransactionMatchedEventHistory(
				id,
				record.getValue(TRANSACTION_MATCHED_EVENT.CREATED_DATE),
				documentId));
	}

}
