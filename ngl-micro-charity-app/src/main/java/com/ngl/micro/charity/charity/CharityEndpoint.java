package com.ngl.micro.charity.charity;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.server.RestEndpoint;
import com.ngl.middleware.rest.server.http.PATCH;
import com.ngl.middleware.rest.server.security.oauth2.annotation.RequiresScopes;

/**
 * Charity endpoint.
 *
 * @author Willy du Preez
 *
 */
@Path("charities")
public interface CharityEndpoint extends RestEndpoint {


	@GET
	@Path("{id}/contributions/summary")
	@RequiresScopes({Scopes.CHARITY_READ_SCOPE, Scopes.CONTRIBUTION_READ_SCOPE})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response summary(@PathParam("id") UUID id);

	@POST
	@RequiresScopes(Scopes.SYS_CHARITY_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response create(Charity charity);

	@GET
	@Path("{id}")
	@RequiresScopes(Scopes.CHARITY_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response get(@PathParam("id") UUID id);

	@GET
	@RequiresScopes(Scopes.CHARITY_READ_SCOPE)
	@Produces(MediaType.APPLICATION_JSON)
	Response list();

	@PATCH
	@Path("{id}")
	@RequiresScopes(Scopes.SYS_CHARITY_WRITE_SCOPE)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response update(@PathParam("id") UUID id, Patch patch);

}
