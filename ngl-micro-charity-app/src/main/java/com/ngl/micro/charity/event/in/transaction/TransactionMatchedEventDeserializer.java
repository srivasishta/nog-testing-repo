package com.ngl.micro.charity.event.in.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.messaging.core.data.deserialize.JsonMessageDeserializer;

/**
 * Deserializes transaction matched events.
 *
 * @author Willy du Preez
 *
 */
public class TransactionMatchedEventDeserializer extends JsonMessageDeserializer<TransactionMatchedEvent> {

	public TransactionMatchedEventDeserializer(ObjectMapper objectMapper) {
		super(objectMapper);
	}

	@Override
	protected Class<TransactionMatchedEvent> getEventType(String eventName) {
		return TransactionMatchedEvent.class;
	}

}
