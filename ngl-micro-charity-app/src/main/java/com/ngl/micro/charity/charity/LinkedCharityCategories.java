package com.ngl.micro.charity.charity;

import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY;
import static com.ngl.micro.charity.dal.generated.jooq.Tables.CHARITY_CATEGORIES;
import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jooq.Record;
import org.jooq.TableOnConditionStep;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityCategoriesDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityCategories;
import com.ngl.middleware.dal.vendor.jooq.support.DAL;
import com.ngl.middleware.dal.vendor.jooq.support.FieldMap;
import com.ngl.middleware.dal.vendor.jooq.support.JooqQuery;
import com.ngl.middleware.dal.vendor.jooq.support.MappedField;
import com.ngl.middleware.rest.api.page.Page;

/**
 * Linked charity categories.
 *
 * @author Paco Mendes
 */
public class LinkedCharityCategories {

	private static final String CATEGORY_ID = "category_id";

	private DAL support;
	private JCharityCategoriesDao charityCategoriesDao;
	private JooqQuery query;
	private TableOnConditionStep<?> tables;
	private FieldMap<String> fieldMap;

	public LinkedCharityCategories(DAL dal) {
		this.support = dal;
		this.charityCategoriesDao = new JCharityCategoriesDao(dal.config());

		this.fieldMap = new FieldMap<String>()
				.bind(Charity.Fields.ID, new MappedField(CHARITY.RESOURCE_ID).searchable(true))
				.bind(CATEGORY_ID, new MappedField(CHARITY_CATEGORIES.CATEGORY_ID));

		this.tables = CHARITY
				.join(CHARITY_CATEGORIES).on(CHARITY.ID.eq(CHARITY_CATEGORIES.CHARITY_ID));

		this.query = JooqQuery
				.builder(this.support, this.fieldMap)
				.from(tables)
				.build();

	}

	public void add(Long charityId, Set<CharityCategory> charityCategories) {
		Set<JCharityCategories> categories = new HashSet<>();
		for (CharityCategory category : charityCategories) {
			JCharityCategories charityCategory = new JCharityCategories();
			charityCategory.setCharityId(charityId);
			charityCategory.setCategoryId(CharityCategoryType.forEnum(category).getId());
			categories.add(charityCategory);
		}
		this.charityCategoriesDao.insert(categories);
	}

	public Set<CharityCategory> list(Long charityId) {
			return this.charityCategoriesDao.fetchByJCharityId(charityId)
				.stream()
				.map(c -> map(c))
				.collect(toSet());
	}

	private CharityCategory map(JCharityCategories c) {
		return CharityCategory.valueOf(CharityCategoryType.nameForId(c.getCategoryId()));
	}

	public Map<UUID, Set<CharityCategory>> list(Page<Charity> charityPage) {

		List<Record> charityCategoryRecords = this.query.getRecordsWhere(
				CHARITY.RESOURCE_ID.in(
						charityPage.getItems()
							.stream()
							.map(c -> c.getId())
							.collect(Collectors.toList())
				)
		);

		return charityCategoryRecords.stream()
			.map(this::map)
			.collect(
				Collectors.groupingBy(CharityCharityCategory::getCharityId,
						Collectors.mapping(CharityCharityCategory::getCategory,
						Collectors.toSet())
				)
			);
	}

	private CharityCharityCategory map(Record record) {
		CharityCharityCategory category = new CharityCharityCategory();
		category.setCharityId(record.getValue(CHARITY.RESOURCE_ID));
		category.setCategory(CharityCategory.valueOf(CharityCategoryType.nameForId(record.getValue(CHARITY_CATEGORIES.CATEGORY_ID))));
		return category;
	}

	public void update(Long charityId, Set<CharityCategory> charityCategories) {
		Set<CharityCategory> currentCategories = this.list(charityId);
		if (currentCategories.equals(charityCategories)) {
			return;
		}
		if (!currentCategories.isEmpty()) {
			this.deleteAll(charityId);
		}
		this.add(charityId, charityCategories);
	}

	private void deleteAll(Long charityId) {
		this.support.sql()
				.delete(CHARITY_CATEGORIES)
				.where(CHARITY_CATEGORIES.CHARITY_ID.eq(charityId))
				.execute();
	}
}
