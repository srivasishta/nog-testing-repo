CREATE TABLE `contribution_demographics` (

    -- Columns
    `contribution_document_id`   BIGINT(20) NOT NULL,
    `gender`                     BIGINT(20) NOT NULL,
    `birthdate`                  DATE,

    -- Constraints
    PRIMARY KEY (`contribution_document_id`),
    FOREIGN KEY (`contribution_document_id`) REFERENCES `contribution_document` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
