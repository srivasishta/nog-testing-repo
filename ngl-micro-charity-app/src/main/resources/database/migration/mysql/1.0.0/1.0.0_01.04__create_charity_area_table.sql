CREATE TABLE `charity_area` (

    -- Columns
    `id`                BIGINT(20) NOT NULL AUTO_INCREMENT,
    `charity_id`        BIGINT(20) NOT NULL,
    `country`           VARCHAR(2) NOT NULL,
    `region`            VARCHAR(50),

    -- Constraints
    PRIMARY KEY (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
