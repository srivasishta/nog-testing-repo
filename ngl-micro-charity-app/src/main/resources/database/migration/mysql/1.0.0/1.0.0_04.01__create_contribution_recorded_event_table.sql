CREATE TABLE `contribution_recorded_event` (

    -- Columns
    `id`                             BIGINT(20) NOT NULL,
    `created_date`                   DATETIME NOT NULL,
    `contribution_document_id`       BINARY(16) NOT NULL,
    `published`                      CHAR(1) NOT NULL DEFAULT 'F',

    -- Constraints
    PRIMARY KEY (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_cre_created_date` ON `contribution_recorded_event` (`created_date`);
CREATE INDEX `idx_cre_published` ON `contribution_recorded_event` (`published`);