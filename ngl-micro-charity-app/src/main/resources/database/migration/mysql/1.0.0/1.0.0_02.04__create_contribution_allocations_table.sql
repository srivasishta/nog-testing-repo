CREATE TABLE `contribution_allocation` (

    -- Columns
    `contribution_document_id`           BIGINT(20) NOT NULL,
    `charity_id`                         BIGINT(20) NOT NULL,
    `amount`                             DECIMAL(15,5) NOT NULL,
    `allocation`                         INTEGER NOT NULL,

    -- Constraints
    PRIMARY KEY (`contribution_document_id`, `charity_id`),
    FOREIGN KEY (`contribution_document_id`) REFERENCES `contribution_document` (`id`),
    FOREIGN KEY (`charity_id`) REFERENCES `charity` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
