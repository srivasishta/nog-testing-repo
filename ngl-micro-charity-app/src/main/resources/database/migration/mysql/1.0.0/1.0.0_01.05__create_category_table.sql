CREATE TABLE `category` (

    -- Columns
    `id`                    BIGINT(20) NOT NULL,
    `value`                 VARCHAR(70) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE (`value`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
