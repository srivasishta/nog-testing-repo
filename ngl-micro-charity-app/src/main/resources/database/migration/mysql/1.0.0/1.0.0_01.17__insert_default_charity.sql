INSERT INTO `charity` (
   `id`
  ,`resource_id`
  ,`name`
  ,`description`
  ,`reach_type`
  ,`default_charity`
) VALUES (
   1
  ,X'00000000000000000000000000000001'
  ,'The Network of Giving'
  ,'Default cause for unallocated network contributions.'
  ,'GLOBAL'
  ,'T'
);

INSERT INTO `charity_categories` (`charity_id`, `category_id`)
VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);

INSERT INTO `charity_status_history` (`charity_id`, `status_id`, `status_reason`, `start_date`) VALUES
(1, 1, 'Created Default Charity', NOW());