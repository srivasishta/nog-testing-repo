CREATE TABLE `transaction_matched_event` (

    -- Columns
    `id`                             BIGINT(20) NOT NULL,
    `created_date`                   DATETIME NOT NULL,
    `contribution_document_id`       BINARY(16) NOT NULL,
    `processed`                      CHAR(1) NOT NULL DEFAULT 'F',

    -- Constraints
    PRIMARY KEY (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_tme_created_date` ON `transaction_matched_event` (`created_date`);
CREATE INDEX `idx_tme_processed` ON `transaction_matched_event` (`processed`);