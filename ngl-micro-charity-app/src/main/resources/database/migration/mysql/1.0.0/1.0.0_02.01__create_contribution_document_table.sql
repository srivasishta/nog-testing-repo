CREATE TABLE `contribution_document` (

    -- Columns
    `id`                                   BIGINT(20) NOT NULL AUTO_INCREMENT,
    `resource_id`                          BINARY(16) NOT NULL,
    `created_date`                         DATETIME NOT NULL,
    `last_modified_date`                   DATETIME NOT NULL,

    `contribution_date`                    DATETIME,
    `contribution_amount`                  DECIMAL(15,5) NOT NULL,

    `partner_id`                           BINARY(16) NOT NULL,

    `transaction_date`                     DATETIME NOT NULL,
    `transaction_status`                   BIGINT(20) NOT NULL,
    `transaction_amount`                   DECIMAL(15,5) NOT NULL,
    `transaction_time_zone`                BIGINT(20) NOT NULL,
    `transaction_currency`                 BIGINT(20) NOT NULL,

    `business_id`                          BINARY(16) NOT NULL,
    `business_store_id`                    BINARY(16) NOT NULL,
    `business_store_contribution_rate`     DECIMAL(15,5) NOT NULL,

    `membership_id`                        BINARY(16) NOT NULL,

    -- Constraints
    PRIMARY KEY (`id`),
    UNIQUE KEY(`resource_id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE INDEX `idx_d_created_date` ON `contribution_document` (`created_date`);
