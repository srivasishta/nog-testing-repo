CREATE TABLE `charity_categories` (

	-- Columns
	`charity_id`        BIGINT(20) NOT NULL,
	`category_id`       BIGINT(20) NOT NULL,

	-- Constraints
	PRIMARY KEY (`charity_id`, `category_id`),
	FOREIGN KEY (`charity_id`) REFERENCES `charity` (`id`),
	FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;