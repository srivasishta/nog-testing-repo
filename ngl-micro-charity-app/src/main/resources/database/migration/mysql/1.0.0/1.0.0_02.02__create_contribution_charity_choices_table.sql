CREATE TABLE `contribution_charity_choices` (

    -- Columns
    `contribution_document_id`       BIGINT(20) NOT NULL,
    `charity_id`                     BIGINT(20) NOT NULL,
    `charity_allocation`             INTEGER NOT NULL,

    -- Constraints
    PRIMARY KEY (`contribution_document_id`, `charity_id`),
    FOREIGN KEY (`charity_id`) REFERENCES `charity` (`id`)

) ENGINE = InnoDB DEFAULT CHARSET = utf8;
