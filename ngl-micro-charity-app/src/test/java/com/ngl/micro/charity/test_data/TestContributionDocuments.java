package com.ngl.micro.charity.test_data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JContributionDocumentDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JContributionDocument;
import com.ngl.micro.shared.contracts.charity.ContributionDocument;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

/**
 * {@link ContributionDocument} test data.
 *
 * @author Willy du Preez
 *
 */
public class TestContributionDocuments implements JooqTestDataSet  {

	public static ContributionDocument newDocument() {
		return ContributionDocument.builder()
				.id(Data.ID_7)
				.createdDate(Data.T_0)
				.lastModifiedDate(Data.T_0)
				.businessStoreContributionRate(Data.AMOUNT_0_50)
				.businessId(Data.ID_1)
				.businessStoreId(Data.ID_2)
				.contributionAllocations(java.util.Collections.emptySet())
				.contributionAmount(Data.AMOUNT_0_50)
				.contributionDate(Data.T_0)
				.membershipCharityChoices(Collections.asSet(new MembershipCharityChoice(Data.ID_3, Data.PCT_100)))
				.membershipDemographics(new MembershipDemographics(Gender.MALE, LocalDate.of(2000, 1, 1)))
				.membershipId(Data.ID_5)
				.partnerId(Data.ID_6)
				.transactionAmount(Data.AMOUNT_25_00)
				.transactionCurrency(Currency.USD)
				.transactionDate(Data.T_MINUS_5d)
				.transactionStatus(TransactionStatus.SETTLED)
				.transactionTimezone(TimeZone.AMERICA__EDMONTON)
				.build();
	}

	@Override
	public void loadData(DSLContext sql) {
		JContributionDocumentDao contributionDocumentDao = new JContributionDocumentDao(sql.configuration());
		contributionDocumentDao.insert(
				contributionDocument(
						Data.PK_1, /*Id*/
						Data.ID_1, /*businessId*/
						Data.AMOUNT_0_20, /*businessStoreContributionRate*/
						Data.ID_1, /*businessStoreId*/
						Data.AMOUNT_10_00, /*contributionAmount*/
						Data.T_MINUS_1d, /*contributionDate*/
						Data.T_MINUS_1d, /*createdDate*/
						Data.T_MINUS_1d, /*lastModifiedDate*/
						Data.ID_1, /*membershipId*/
						Data.ID_1, /*partnerId*/
						UUID.randomUUID(), /*resourceId*/
						Data.AMOUNT_50_00, /*transactionAmount*/
						Currency.USD.getId(), /*transactionCurrency*/
						Data.T_MINUS_1d, /*transactionDate*/
						TransactionStatus.SETTLED.getId(), /*transactionStatus*/
						TimeZone.AMERICA__EDMONTON.getId() /*transactionTimeZone*/
						)
				);
		contributionDocumentDao.insert(
				contributionDocument(
						Data.PK_2, /*Id*/
						Data.ID_1, /*businessId*/
						Data.AMOUNT_0_20, /*businessStoreContributionRate*/
						Data.ID_1, /*businessStoreId*/
						Data.AMOUNT_10_00, /*contributionAmount*/
						Data.T_MINUS_10d, /*contributionDate*/
						Data.T_MINUS_10d, /*createdDate*/
						Data.T_MINUS_10d, /*lastModifiedDate*/
						Data.ID_1, /*membershipId*/
						Data.ID_1, /*partnerId*/
						UUID.randomUUID(), /*resourceId*/
						Data.AMOUNT_50_00, /*transactionAmount*/
						Currency.USD.getId(), /*transactionCurrency*/
						Data.T_MINUS_10d, /*transactionDate*/
						TransactionStatus.SETTLED.getId(), /*transactionStatus*/
						TimeZone.AMERICA__EDMONTON.getId() /*transactionTimeZone*/
						)
				);
		contributionDocumentDao.insert(
				contributionDocument(
						Data.PK_3, /*Id*/
						Data.ID_1, /*businessId*/
						Data.AMOUNT_0_20, /*businessStoreContributionRate*/
						Data.ID_1, /*businessStoreId*/
						new BigDecimal("21.00000"), /*contributionAmount*/
						Data.T_MINUS_10d, /*contributionDate*/
						Data.T_MINUS_10d, /*createdDate*/
						Data.T_MINUS_10d, /*lastModifiedDate*/
						Data.ID_1, /*membershipId*/
						Data.ID_1, /*partnerId*/
						UUID.randomUUID(), /*resourceId*/
						new BigDecimal("105.00000"), /*transactionAmount*/
						Currency.USD.getId(), /*transactionCurrency*/
						Data.T_MINUS_10d, /*transactionDate*/
						TransactionStatus.SETTLED.getId(), /*transactionStatus*/
						TimeZone.AMERICA__EDMONTON.getId() /*transactionTimeZone*/
						)
				);
		contributionDocumentDao.insert(
				contributionDocument(
						Data.PK_4, /*Id*/
						Data.ID_1, /*businessId*/
						Data.AMOUNT_0_20, /*businessStoreContributionRate*/
						Data.ID_1, /*businessStoreId*/
						Data.AMOUNT_20_00, /*contributionAmount*/
						Data.T_MINUS_1d, /*contributionDate*/
						Data.T_MINUS_1d, /*createdDate*/
						Data.T_MINUS_1d, /*lastModifiedDate*/
						Data.ID_1, /*membershipId*/
						Data.ID_1, /*partnerId*/
						UUID.randomUUID(), /*resourceId*/
						Data.AMOUNT_100_00, /*transactionAmount*/
						Currency.USD.getId(), /*transactionCurrency*/
						Data.T_MINUS_1d, /*transactionDate*/
						TransactionStatus.SETTLED.getId(), /*transactionStatus*/
						TimeZone.AMERICA__EDMONTON.getId() /*transactionTimeZone*/
						)
				);

	}

	private JContributionDocument contributionDocument(
			Long id,
			UUID businessId,
			BigDecimal businessStoreContributionRate,
			UUID businessStoreId,
			BigDecimal contributionAmount,
			ZonedDateTime contributionDate,
			ZonedDateTime createdDate,
			ZonedDateTime lastModifiedDate,
			UUID membershipId,
			UUID partnerId,
			UUID resourceId,
			BigDecimal transactionAmount,
			Long transactionCurrency,
			ZonedDateTime transactionDate,
			Long transactionStatus,
			Long transactionTimeZone
			){
		JContributionDocument document = new JContributionDocument();
		document.setId(id);
		document.setBusinessId(businessId);
		document.setBusinessStoreContributionRate(businessStoreContributionRate);
		document.setBusinessStoreId(businessStoreId);
		document.setContributionAmount(contributionAmount);
		document.setContributionDate(contributionDate);
		document.setCreatedDate(createdDate);
		document.setLastModifiedDate(lastModifiedDate);
		document.setMembershipId(membershipId);
		document.setPartnerId(partnerId);
		document.setResourceId(resourceId);
		document.setTransactionAmount(transactionAmount);
		document.setTransactionCurrency(transactionCurrency);
		document.setTransactionDate(transactionDate);
		document.setTransactionStatus(transactionStatus);
		document.setTransactionTimeZone(transactionTimeZone);
		return document;
	}

}
