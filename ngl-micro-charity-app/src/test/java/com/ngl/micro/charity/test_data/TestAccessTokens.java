package com.ngl.micro.charity.test_data;

import com.ngl.micro.shared.contracts.oauth.Scopes;
import com.ngl.middleware.microservice.test.oauth2.TestAccessToken;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

public class TestAccessTokens {

	public static final TestAccessToken SYSTEM = TestAccessToken.SYSTEM.scope(Scopes.VALUES);

	public static TestAccessToken charity_write() {
		return TestAccessToken.newBasic()
				.subjectClaim(Data.ID_1)
				.clientIdClaim(Data.ID_1)
				.scope(Collections.asSet(Scopes.SYS_CHARITY_WRITE_SCOPE));
	}

	public static TestAccessToken charity_read() {
		return TestAccessToken.newBasic()
				.subjectClaim(Data.ID_2)
				.clientIdClaim(Data.ID_2)
				.scope(Collections.asSet(Scopes.CHARITY_READ_SCOPE, Scopes.CONTRIBUTION_READ_SCOPE));
	}

}
