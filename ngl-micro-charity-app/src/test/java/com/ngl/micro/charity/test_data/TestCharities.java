package com.ngl.micro.charity.test_data;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.jooq.DSLContext;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.Country;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.charity.CharityCategoryType;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityAreaDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityCategoriesDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JCharityStatusHistoryDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharity;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityArea;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityCategories;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JCharityStatusHistory;
import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

public class TestCharities implements JooqTestDataSet {

	public static final Long CHARITY_2_PK = Data.PK_2;
	public static final UUID CHARITY_2_ID = Data.ID_2;
	public static final UUID NEW_CHARITY_ID = Data.ID_7;

	public Charity newCharity() {
		Charity charity = new Charity();
		charity.setName("North American Red Cross");
		charity.setDescription("United States and Canadian Red Cross");
		charity.setId(NEW_CHARITY_ID);
		charity.setStatus(CharityStatus.ACTIVE);
		charity.setStatusReason(Data.REASON_CREATED);
		charity.setReach(new Reach(ReachType.INTERNATIONAL, Collections.asSet(this.us(), this.ca())));
		charity.setCategories(Collections.asSet(CharityCategory.HEALTH, CharityCategory.INCLUSION));
		charity.getLogoImages().setOriginal("http://cdn.images/orignal.jpg");
		charity.getLogoImages().setLarge("http://cdn.images/large.jpg");
		return charity;

	}

	public Area us() {
		return new Area(Country.US, Collections.asSet("CA", "CO", "DC", "TX", "WA"));
	}

	public Area ca() {
		return new Area(Country.CA, Collections.asSet("AB", "BC", "MB", "NB", "QC", "ON"));
	}

	@Override
	public void loadData(DSLContext sql) {
		JCharityDao charityDao = new JCharityDao(sql.configuration());
		JCharityStatusHistoryDao statusDao = new JCharityStatusHistoryDao(sql.configuration());
		JCharityCategoriesDao catDao = new JCharityCategoriesDao(sql.configuration());
		JCharityAreaDao areaDao = new JCharityAreaDao(sql.configuration());

		charityDao.insert(
			this.charity(CHARITY_2_PK, Data.ID_2, Data.NAME_1, Data.STRING_1, ReachType.GLOBAL),
			this.charity(Data.PK_3,    Data.ID_3, Data.NAME_2, Data.STRING_2, ReachType.GLOBAL),
			this.charity(Data.PK_4,    Data.ID_4, Data.NAME_3, Data.STRING_3, ReachType.INTERNATIONAL),
			this.charity(Data.PK_5,    Data.ID_5, Data.NAME_4, Data.STRING_4, ReachType.NATIONAL),
			this.charity(Data.PK_6,    Data.ID_6, Data.NAME_5, Data.STRING_5, ReachType.REGIONAL));

		statusDao.insert(
				this.status(Data.FK_2,    ResourceStatus.ACTIVE.getId(),   Data.REASON_CREATED,     Data.T_MINUS_1y, Data.T_MINUS_5M),
				this.status(Data.FK_2,    ResourceStatus.INACTIVE.getId(), Data.REASON_DEACTIVATED, Data.T_MINUS_5M, Data.T_MINUS_1M),
				this.status(Data.FK_2,    ResourceStatus.ACTIVE.getId(),   Data.REASON_ACTIVATED,   Data.T_MINUS_1M, null),
				this.status(Data.FK_3,    ResourceStatus.ACTIVE.getId(),   Data.REASON_CREATED,     Data.T_MINUS_1M, null),
				this.status(Data.FK_4,    ResourceStatus.ACTIVE.getId(),   Data.REASON_CREATED,     Data.T_MINUS_1M, null),
				this.status(Data.FK_5,    ResourceStatus.ACTIVE.getId(),   Data.REASON_CREATED,     Data.T_MINUS_1M, null),
				this.status(Data.FK_6,    ResourceStatus.INACTIVE.getId(), Data.REASON_DEACTIVATED, Data.T_MINUS_1M, null));

		catDao.insert(
				this.category(Data.FK_2, CharityCategoryType.EDUCATION),
				this.category(Data.FK_2, CharityCategoryType.ENVIRONMENT),
				this.category(Data.FK_2, CharityCategoryType.INCLUSION),
				this.category(Data.FK_3, CharityCategoryType.EDUCATION),
				this.category(Data.FK_3, CharityCategoryType.HEALTH),
				this.category(Data.FK_4, CharityCategoryType.HEALTH),
				this.category(Data.FK_5, CharityCategoryType.EDUCATION),
				this.category(Data.FK_6, CharityCategoryType.INCLUSION)
			);

		areaDao.insert(
				this.area(Data.PK_1, Data.FK_2, Country.US, "WA"),
				this.area(Data.PK_2, Data.FK_2, Country.US, "CA"),
				this.area(Data.PK_3, Data.FK_3, Country.US, "NY"),
				this.area(Data.PK_4, Data.FK_4, Country.CA, "ON"),
				this.area(Data.PK_5, Data.FK_5, Country.CA, "AB"),
				this.area(Data.PK_6, Data.FK_6, Country.US, "CA"),
				this.area(Data.PK_7, Data.FK_6, Country.US, "CO"),
				this.area(Data.PK_8, Data.FK_6, Country.US, "DC")
			);



	}

	private JCharityCategories category(Long charityId, CharityCategoryType type) {
		return new JCharityCategories(charityId, type.getId());
	}

	private JCharityArea area(Long id, Long charityId, Country country, String region) {
		return new JCharityArea(id, charityId, country.name(), region);
	}

	private JCharity charity(Long id, UUID resourceId, String name, String description, ReachType reach) {
		JCharity charity = new JCharity();
		charity.setId(id);
		charity.setResourceId(resourceId);
		charity.setName(name);
		charity.setDescription(description);
		charity.setReachType(reach.name());
		return charity;
	}

	private JCharityStatusHistory status(Long charityId, Long statusId, String reason, ZonedDateTime startDate, ZonedDateTime endDate) {
		JCharityStatusHistory status =  new JCharityStatusHistory();
		status.setCharityId(charityId);
		status.setStartDate(startDate);
		status.setEndDate(endDate);
		status.setStatusId(statusId);
		status.setStatusReason(reason);
		return status;
	}

}
