package com.ngl.micro.charity.charity;

import static com.ngl.middleware.util.Collections.asSet;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Country;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.error.ApplicationErrorCode;
import com.ngl.middleware.rest.api.error.ValidationErrorCode;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.util.Collections;

public class CharityValidatorTest {

	private CharityValidator validator;
	private TestCharities testCharities;

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	@Before
	public void before() {
		this.validator = CharityValidator.newInstance();
		this.testCharities = new TestCharities();
	}

	@Test
	public void test_validate_reference_charity() {
		this.validator.validate(this.testCharities.newCharity());
	}

	@Test
	public void test_validate_charity_reach() {
		Charity global = this.testCharities.newCharity();
		global.setReach(new Reach(ReachType.GLOBAL, new HashSet<>()));
		this.validator.validate(global);

		Charity international = this.testCharities.newCharity();
		international.setReach(new Reach(ReachType.INTERNATIONAL, asSet(this.testCharities.us(), this.testCharities.ca())));
		this.validator.validate(international);
		Charity national = this.testCharities.newCharity();

		national.setReach(new Reach(ReachType.NATIONAL, asSet(this.testCharities.ca())));
		this.validator.validate(national);

		Charity regional = this.testCharities.newCharity();
		regional.setReach(new Reach(ReachType.REGIONAL, asSet(this.testCharities.ca())));
		this.validator.validate(regional);
	}

	@Test
	public void test_invalid_regions() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("reach.areas.regions", ValidationErrorCode.INVALID);

		Charity charity = this.testCharities.newCharity();
		charity.setReach(new Reach(ReachType.REGIONAL,
			Collections.asSet(new Area(Country.US, new HashSet<>()))));
		this.validator.validate(charity);
	}

	@Test
	public void test_invalid_global_reach() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("reach", ValidationErrorCode.INVALID, "GLOBAL charities must not reach target areas.");

		Charity charity = this.testCharities.newCharity();
		charity.setReach(new Reach(ReachType.GLOBAL, asSet(this.testCharities.us())));

		this.validator.validate(charity);
	}

	@Test
	public void test_invalid_international_reach() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("reach", ValidationErrorCode.INVALID, "INTERNATIONAL charities must reach more than one country.");

		Charity charity = this.testCharities.newCharity();
		charity.setReach(new Reach(ReachType.INTERNATIONAL, asSet(this.testCharities.us())));

		this.validator.validate(charity);
	}

	@Test
	public void test_invalid_national_reach() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("reach", ValidationErrorCode.INVALID, "NATIONAL charities must reach exactly one country.");

		Charity charity = this.testCharities.newCharity();
		charity.setReach(new Reach(ReachType.NATIONAL, asSet(this.testCharities.us(), this.testCharities.ca())));
		this.validator.validate(charity);
	}

	@Test
	public void test_invalid_regional_reach() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("reach", ValidationErrorCode.INVALID, "REGIONAL charities must reach exactly one country.");

		Charity charity = this.testCharities.newCharity();
		charity.setReach(new Reach(ReachType.REGIONAL, asSet(this.testCharities.us(), this.testCharities.ca())));
		this.validator.validate(charity);
	}

	@Test
	public void test_fail_null_fields() {
		this.thrown.expectErrorCode(ApplicationErrorCode.VALIDATION_ERROR)
				.expectStatus(ApplicationStatus.BAD_REQUEST)
				.expectValidationError("name", ValidationErrorCode.INVALID)
				.expectValidationError("description", ValidationErrorCode.INVALID)
				.expectValidationError("status", ValidationErrorCode.INVALID)
				.expectValidationError("statusReason", ValidationErrorCode.INVALID)
				.expectValidationError("reach", ValidationErrorCode.INVALID)
				.expectValidationError("categories", ValidationErrorCode.INVALID);

		Charity charity = new Charity();
		charity.setStatusReason(null);
		charity.setCategories(null);
		this.validator.validate(charity);
	}

}