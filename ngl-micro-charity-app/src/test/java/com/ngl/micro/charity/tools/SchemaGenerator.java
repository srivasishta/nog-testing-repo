package com.ngl.micro.charity.tools;

import com.ngl.middleware.database.DatabaseProperties;
import com.ngl.middleware.database.mapping.DatabaseMappingGenerator;
import com.ngl.middleware.database.mapping.DatabaseMappingGeneratorProperties;

/**
 * Schema generator for charity service tables.
 *
 * @author Willy du Preez
 */
public class SchemaGenerator {

	public static void main(String[] args) {
		DatabaseMappingGeneratorProperties mappingProps = new DatabaseMappingGeneratorProperties();
		mappingProps.setGenerateWithClassPrefix(true);
		mappingProps.setGenerateTargetPackage("com.ngl.micro.charity.dal.generated.jooq");
		mappingProps.setGenerateWithExcludes("");
		mappingProps.setGenerateWithIncludes(".*");

		DatabaseProperties dbProps = new DatabaseProperties();
		dbProps.setUsername("root");
		dbProps.setPassword("Admin123");
		dbProps.setSchema("ngl_micro_charity");

		new DatabaseMappingGenerator().generateFromSchema(mappingProps, dbProps);
	}

}
