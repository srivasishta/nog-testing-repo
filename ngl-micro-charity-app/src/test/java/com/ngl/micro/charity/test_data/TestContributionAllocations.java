package com.ngl.micro.charity.test_data;

import java.math.BigDecimal;

import org.jooq.DSLContext;

import com.ngl.micro.charity.dal.generated.jooq.tables.daos.JContributionAllocationDao;
import com.ngl.micro.charity.dal.generated.jooq.tables.pojos.JContributionAllocation;
import com.ngl.middleware.database.test.JooqTestDataSet;
import com.ngl.middleware.test.data.Data;

public class TestContributionAllocations implements JooqTestDataSet {

	@Override
	public void loadData(DSLContext sql) {
		//Correlated with contribution document test data, entries below should add to 61.00
		JContributionAllocationDao contribAllocDao = new JContributionAllocationDao(sql.configuration());
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_60, Data.AMOUNT_6_00, Data.FK_2, Data.FK_1));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_3, Data.FK_1));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_4, Data.FK_1));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_5, Data.FK_1));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_6, Data.FK_1));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_40, Data.AMOUNT_4_00, Data.FK_2, Data.FK_2));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_2_00, Data.FK_3, Data.FK_2));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_4, Data.FK_2));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_1_00, Data.FK_5, Data.FK_2));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_2_00, Data.FK_6, Data.FK_2));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_30, Data.AMOUNT_6_00, Data.FK_2, Data.FK_3));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_30, Data.AMOUNT_6_00, Data.FK_3, Data.FK_3));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_2_00, Data.FK_4, Data.FK_3));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_10, Data.AMOUNT_3_00, Data.FK_5, Data.FK_3));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_6, Data.FK_3));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_2, Data.FK_4));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_3, Data.FK_4));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_4, Data.FK_4));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_5, Data.FK_4));
		contribAllocDao.insert(this.contributionAllocation(Data.PCT_20, Data.AMOUNT_4_00, Data.FK_6, Data.FK_4));
	}

	private JContributionAllocation contributionAllocation(Integer allocation, BigDecimal amount,
			Long charityId, Long contributionDocumentId){
		JContributionAllocation contribAlloc = new JContributionAllocation();
		contribAlloc.setAllocation(allocation);
		contribAlloc.setAmount(amount);
		contribAlloc.setCharityId(charityId);
		contribAlloc.setContributionDocumentId(contributionDocumentId);
		return contribAlloc;
	}

}
