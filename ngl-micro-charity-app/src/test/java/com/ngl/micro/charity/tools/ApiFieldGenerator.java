package com.ngl.micro.charity.tools;

import java.util.Arrays;

import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.ImageSet;
import com.ngl.micro.charity.Reach;
import com.ngl.middleware.test.tools.ApiFieldsGenerator;

/**
 * Generates typesafe API fields.
 *
 * @author Paco Mendes
 *
 */
public class ApiFieldGenerator {

	public static void main(String[] args) throws Exception {
		new ApiFieldsGenerator(Arrays.asList(Reach.class, ImageSet.class)).generate(Charity.class);
	}

}
