package com.ngl.micro.charity.test_data;

import java.time.LocalDate;

import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.micro.shared.contracts.transaction.TransactionMatchedEvent;
import com.ngl.middleware.test.data.Data;
import com.ngl.middleware.util.Collections;

/**
 * {@link TransactionMatchedEvent} test data.
 *
 * @author Willy du Preez
 *
 */
public class TestTransactionMatchedEvents {

	private static final Long NEW_EVENT_ID = Data.PK_1;

	public static TransactionMatchedEvent newEvent() {
		return TransactionMatchedEvent.builder(NEW_EVENT_ID, Data.T_MINUS_1m)
				.businessStoreContributionRate(Data.AMOUNT_0_50)
				.businessId(Data.ID_1)
				.businessStoreId(Data.ID_2)
				.membershipCharityChoices(Collections.asSet(new MembershipCharityChoice(Data.ID_3, Data.PCT_100)))
				.membershipDemographics(new MembershipDemographics(Gender.MALE, LocalDate.of(2000, 1, 1)))
				.membershipId(Data.ID_5)
				.partnerId(Data.ID_6)
				.transactionId(Data.toUUID(NEW_EVENT_ID))
				.transactionGroupId(Data.ID_7)
				.transactionAmount(Data.AMOUNT_25_00)
				.transactionCurrency(Currency.USD)
				.transactionDate(Data.T_MINUS_5d)
				.transactionStatus(TransactionStatus.SETTLED)
				.transactionTimezone(TimeZone.AMERICA__EDMONTON)
				.build();
	}

}
