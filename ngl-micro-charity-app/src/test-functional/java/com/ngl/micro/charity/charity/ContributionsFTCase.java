package com.ngl.micro.charity.charity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Charity.CharityCategory;
import com.ngl.micro.charity.CharityApplication;
import com.ngl.micro.charity.CharityFTCaseConfiguration;
import com.ngl.micro.charity.client.CharityClient;
import com.ngl.micro.charity.test_data.TestAccessTokens;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.micro.charity.test_data.TestContributionAllocations;
import com.ngl.micro.charity.test_data.TestContributionDocuments;
import com.ngl.micro.contribution.CategoryContributions;
import com.ngl.micro.contribution.CharityContributions;
import com.ngl.micro.contribution.NetworkContributions;
import com.ngl.micro.contribution.ContributionSearchFields;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.SearchQueries;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.test.api.ExpectedApplicationException;
import com.ngl.middleware.test.data.Data;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		CharityFTCaseConfiguration.class
})
public class ContributionsFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestCharities testCharities;
	private TestContributionAllocations testContributionAllocations;
	private TestContributionDocuments testContributionDocuments;

	@Autowired
	private CharityClient client;

	public ContributionsFTCase() {
		super(CharityApplication.class);
	}

	@Override
	protected void before() {
			super.before();
		this.testCharities = new TestCharities();
		this.testContributionAllocations = new TestContributionAllocations();
		this.testContributionDocuments = new TestContributionDocuments();
	}

	@Test
	public void test_charity_contributions() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAllocations);

		CharityContributions charityContributions = this.client.getCharityResource().charityContributions(TestCharities.CHARITY_2_ID).getState();
		Assert.assertEquals("all time", Data.AMOUNT_20_00, charityContributions.getTotal());

		SearchQuery searchQuery = SearchQueries.q(
				ContributionSearchFields.CONTRIBUTION_DATE).ge(Data.T_MINUS_1y)
				.and(ContributionSearchFields.CONTRIBUTION_DATE).le(Data.T_0).getSearchQuery();
		charityContributions = this.client.getCharityResource()
				.charityContributions(TestCharities.CHARITY_2_ID, searchQuery).getState();
		Assert.assertEquals("all time range", Data.AMOUNT_20_00, charityContributions.getTotal());

		searchQuery = SearchQueries.q(
				ContributionSearchFields.CONTRIBUTION_DATE).ge(Data.T_MINUS_1d)
				.and(ContributionSearchFields.CONTRIBUTION_DATE).le(Data.T_0).getSearchQuery();
		charityContributions = this.client.getCharityResource()
				.charityContributions(TestCharities.CHARITY_2_ID, searchQuery).getState();
		Assert.assertEquals("sub time range", Data.AMOUNT_10_00, charityContributions.getTotal());

	}

	@Test
	public void test_network_contributions() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		this.cmdLoadTestDataSet(this.testCharities);
		this.cmdLoadTestDataSet(this.testContributionDocuments);
		this.cmdLoadTestDataSet(this.testContributionAllocations);

		SearchQuery searchQuery = SearchQueries.q(
				ContributionSearchFields.CONTRIBUTION_DATE).ge(Data.T_MINUS_1y)
				.and(ContributionSearchFields.CONTRIBUTION_DATE).le(Data.T_0).getSearchQuery();
		NetworkContributions networkContributions = this.client.getContributionResource()
				.networkContributions(searchQuery).getState();

		Map<CharityCategory, BigDecimal> expected = new HashMap<>();
		expected.put(CharityCategory.EDUCATION, new BigDecimal("42.00000"));
		expected.put(CharityCategory.INCLUSION, new BigDecimal("31.00000"));
		expected.put(CharityCategory.HEALTH, new BigDecimal("21.00000"));
		expected.put(CharityCategory.ENVIRONMENT, new BigDecimal("20.00000"));

		this.assertContributions(expected, networkContributions);

		networkContributions = this.client.getContributionResource().networkContributions(searchQuery).getState();

		this.assertContributions(expected, networkContributions);
		Assert.assertEquals(new BigDecimal("61.00000") ,networkContributions.getTotal());

		searchQuery = SearchQueries.q(
				ContributionSearchFields.CONTRIBUTION_DATE).ge(Data.T_PLUS_1y).getSearchQuery();
		networkContributions = this.client.getContributionResource().networkContributions(searchQuery).getState();

		Map<CharityCategory, BigDecimal> empty = new HashMap<>();
		empty.put(CharityCategory.EDUCATION, new BigDecimal("0.00000"));
		empty.put(CharityCategory.INCLUSION, new BigDecimal("0.00000"));
		empty.put(CharityCategory.HEALTH, new BigDecimal("0.00000"));
		empty.put(CharityCategory.ENVIRONMENT, new BigDecimal("0.00000"));

		this.assertContributions(empty, networkContributions);
		Assert.assertEquals(new BigDecimal("0.00000") ,networkContributions.getTotal());

		searchQuery = SearchQueries.q(
				ContributionSearchFields.CONTRIBUTION_DATE).ge(Data.T_MINUS_1d).getSearchQuery();
		networkContributions = this.client.getContributionResource().networkContributions(searchQuery).getState();

		Map<CharityCategory, BigDecimal> subset = new HashMap<>();
		subset.put(CharityCategory.EDUCATION, new BigDecimal("20.00000"));
		subset.put(CharityCategory.INCLUSION, new BigDecimal("15.00000"));
		subset.put(CharityCategory.HEALTH, new BigDecimal("10.00000"));
		subset.put(CharityCategory.ENVIRONMENT, new BigDecimal("10.00000"));

		this.assertContributions(subset, networkContributions);
		Assert.assertEquals(new BigDecimal("30.00000") ,networkContributions.getTotal());
	}

	private void assertContributions(Map<CharityCategory, BigDecimal> expected, NetworkContributions  actual) {
		Assert.assertEquals(CharityCategory.values().length, actual.getCategoryContributions().size());
		for (CategoryContributions contribution : actual.getCategoryContributions()) {
			Assert.assertEquals(expected.get(contribution.getCategory()), contribution.getTotal());
		}
	}

	@Test
	public void test_unauthorised_network_contributions() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_write());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getContributionResource().networkContributions().getState();

	}

	@Test
	public void test_unauthorised_charity_contributions() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_write());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getCharityResource().charityContributions(TestCharities.CHARITY_2_ID).getState();
	}

}
