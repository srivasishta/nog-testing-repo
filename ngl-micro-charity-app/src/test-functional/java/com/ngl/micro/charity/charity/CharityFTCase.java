package com.ngl.micro.charity.charity;

import static com.ngl.middleware.util.Collections.asSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.ngl.micro.charity.Area;
import com.ngl.micro.charity.Charity;
import com.ngl.micro.charity.Charity.CharityStatus;
import com.ngl.micro.charity.CharityApplication;
import com.ngl.micro.charity.CharityFTCaseConfiguration;
import com.ngl.micro.charity.Country;
import com.ngl.micro.charity.Reach;
import com.ngl.micro.charity.Reach.ReachType;
import com.ngl.micro.charity.client.CharityClient;
import com.ngl.micro.charity.test_data.TestAccessTokens;
import com.ngl.micro.charity.test_data.TestCharities;
import com.ngl.middleware.database.test.DefaultDatabaseTestConfiguration;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCase;
import com.ngl.middleware.rest.api.ApplicationStatus;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.patch.diff.Diff;
import com.ngl.middleware.test.api.ExpectedApplicationException;

@ContextConfiguration(classes = {
		DefaultDatabaseTestConfiguration.class,
		CharityFTCaseConfiguration.class
})
public class CharityFTCase extends AbstractApplicationFTCase {

	@Rule
	public ExpectedApplicationException thrown = ExpectedApplicationException.none();

	private TestCharities testCharities;

	@Autowired
	private CharityClient client;

	public CharityFTCase() {
		super(CharityApplication.class);
	}

	@Override
	protected void before() {
		super.before();
		this.testCharities = new TestCharities();
	}

	@Test
	public void test_charity_create_read_update() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		Charity charity = this.client.getCharityResource()
			.create(this.testCharities.newCharity())
			.getState();

		assertNotNull(charity);

		UUID charityId = charity.getId();
		Charity working = this.client.getCharityResource().get(charity.getId()).getState();
		working.setName("Updated Name");
		working.setDescription("Updated Description");
		Reach updatedReach = new Reach(ReachType.NATIONAL, asSet(new Area(Country.CA, asSet("BC"))));
		working.setReach(updatedReach);
		working.setStatus(CharityStatus.INACTIVE);
		Patch patch = new Diff().diff(working, charity);
		Charity updated = this.client.getCharityResource().patch(charity.getId(), patch).getState();

		assertEquals(charityId, updated.getId());
		assertEquals(CharityStatus.INACTIVE, updated.getStatus());
		assertEquals(updatedReach, updated.getReach());
	}

	@Test
	public void test_charity_page() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.SYSTEM);

		this.client.getCharityResource().create(this.testCharities.newCharity());
		this.client.getCharityResource().create(this.testCharities.newCharity());
		this.client.getCharityResource().create(this.testCharities.newCharity());
		Page<Charity> charities = this.client.getCharityResource().list(PageRequest.with(0, 2).build()).getState();

		assertEquals(2, charities.getNumberOfItems());
		assertEquals(4, charities.getTotalItems());
	}

	@Test
	public void test_unauthorised_charity_read() {
		this.cmdLoadTestDataSet(this.testCharities);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_write());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getCharityResource().get(TestCharities.CHARITY_2_ID);
	}

	@Test
	public void test_unauthorised_charity_list() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_write());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getCharityResource().list(PageRequest.with(0, 1).build());
	}

	@Test
	public void test_unauthorised_charity_create() {
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_read());
		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);

		this.client.getCharityResource().create(this.testCharities.newCharity());
	}

	@Test
	public void test_unauthorised_charity_update() {
		this.cmdLoadTestDataSet(this.testCharities);
		this.accessTokenProvider.setAccessToken(TestAccessTokens.charity_read());

		Charity base = this.client.getCharityResource().get(TestCharities.CHARITY_2_ID).getState();
		assertNotNull(base);

		this.thrown.expectStatus(ApplicationStatus.FORBIDDEN);
		this.client.getCharityResource().patch(TestCharities.CHARITY_2_ID, new Patch(new ArrayList<>()));
	}

}
