package com.ngl.micro.charity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngl.micro.charity.client.CharityClient;
import com.ngl.middleware.config.EnvironmentConfigurationProvider;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerConfiguration;
import com.ngl.middleware.messaging.jms.provider.hornetq.HornetQEmbeddedServerProperties;
import com.ngl.middleware.microservice.config.EnableMicroserviceJmsClient;
import com.ngl.middleware.microservice.test.AbstractApplicationFTCaseConfiguration;
import com.ngl.middleware.rest.api.page.QueryParameterParser;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.HttpClient;

/**
 * @author Paco Mendes
 */
@Configuration
@EnableMicroserviceJmsClient
@Import(HornetQEmbeddedServerConfiguration.class)
public class CharityFTCaseConfiguration extends AbstractApplicationFTCaseConfiguration {

	public static final Long DEFAULT_PARTNER_ID = 2L;

	private static final String CONNECT_SERVICE_URL = "http://localhost:9097/service/charity/";

	@Bean
	public HornetQEmbeddedServerProperties hornetQEmbeddedServerProperties(
			EnvironmentConfigurationProvider config) {
		return config.getConfiguration(
				HornetQEmbeddedServerProperties.DEFAULT_PREFIX,
				HornetQEmbeddedServerProperties.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CharityClient restClient(HttpClient requestFactory, QueryParameterParser parser, ObjectMapper mapper) {
		RestClientConfig config = new RestClientConfig(CONNECT_SERVICE_URL, requestFactory, parser, mapper);
		return new CharityClient(config);
	}

}
