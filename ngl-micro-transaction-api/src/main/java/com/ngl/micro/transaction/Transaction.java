package com.ngl.micro.transaction;

import static com.ngl.middleware.util.EqualsUtils.areEqual;
import static com.ngl.middleware.util.EqualsUtils.hashValue;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import com.ngl.middleware.rest.api.Identifiable;

public class Transaction implements Identifiable<UUID> {

	/** Generated. Do not modify. 2015-10-23T11:53:32.856Z */
	public static final class Fields {
		public static final String AMOUNT = "amount";
		public static final String BUSINESS_ID = "businessId";
		public static final String CREATED_DATE = "createdDate";
		public static final String CURRENCY = "currency";
		public static final String GROUP_ID = "groupId";
		public static final String ID = "id";
		public static final String LAST_MODIFIED_DATE = "lastModifiedDate";
		public static final String LOCAL_AMOUNT = "localAmount";
		public static final String LOCAL_CURRENCY = "localCurrency";
		public static final String MEMBERSHIP_ID = "membershipId";
		public static final String MEMBERSHIP_REF_ID = "membershipRefId";
		public static final String PARTNER_ID = "partnerId";
		public static final String PROCESSING_STATUS = "processingStatus";
		public static final String PROCESSING_STATUS_REASON = "processingStatusReason";
		public static final String REF_ID = "refId";
		public static final String STORE_ID = "storeId";
		public static final String STORE_REF_ID = "storeRefId";
		public static final String TIMEZONE = "timezone";
		public static final String TRACE_ID = "traceId";
		public static final String TRANSACTION_DATE = "transactionDate";
		public static final String TRANSACTION_STATUS = "transactionStatus";
	}

	private UUID id;
	private UUID groupId;
	private String refId;
	private String traceId;
	private UUID partnerId;
	private UUID membershipId;
	private String membershipRefId;
	private UUID businessId;
	private UUID storeId;
	private String storeRefId;
	private String transactionStatus;
	private ZonedDateTime transactionDate;
	private ZoneId timezone;
	private String currency;
	private BigDecimal amount;
	private String localCurrency;
	private BigDecimal localAmount;
	private String processingStatus;
	private String processingStatusReason;

	private ZonedDateTime lastModifiedDate;
	private ZonedDateTime createdDate;

	@Override
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getGroupId() {
		return this.groupId;
	}

	public void setGroupId(UUID groupId) {
		this.groupId = groupId;
	}

	public String getRefId() {
		return this.refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getTraceId() {
		return this.traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public UUID getMembershipId() {
		return this.membershipId;
	}

	public void setMembershipId(UUID membershipId) {
		this.membershipId = membershipId;
	}

	public String getMembershipRefId() {
		return this.membershipRefId;
	}

	public void setMembershipRefId(String membershipRefId) {
		this.membershipRefId = membershipRefId;
	}

	public UUID getStoreId() {
		return this.storeId;
	}

	public void setStoreId(UUID storeId) {
		this.storeId = storeId;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(UUID businessId) {
		this.businessId = businessId;
	}

	public String getStoreRefId() {
		return this.storeRefId;
	}

	public void setStoreRefId(String storeRefId) {
		this.storeRefId = storeRefId;
	}

	public String getTransactionStatus() {
		return this.transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public ZonedDateTime getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(ZonedDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public ZoneId getTimezone() {
		return this.timezone;
	}

	public void setTimezone(ZoneId timezone) {
		this.timezone = timezone;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getLocalCurrency() {
		return this.localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public BigDecimal getLocalAmount() {
		return this.localAmount;
	}

	public void setLocalAmount(BigDecimal localAmount) {
		this.localAmount = localAmount;
	}

	public String getProcessingStatus() {
		return this.processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	public String getProcessingStatusReason() {
		return this.processingStatusReason;
	}

	public void setProcessingStatusReason(String processingStatusReason) {
		this.processingStatusReason = processingStatusReason;
	}

	public ZonedDateTime getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public ZonedDateTime getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += hashValue(this.id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Transaction)) {
			return false;
		}

		Transaction other = (Transaction) obj;

		return  areEqual(this.id, other.id) &&
				areEqual(this.groupId, other.groupId) &&
				areEqual(this.refId, other.refId) &&
				areEqual(this.traceId, other.traceId) &&
				areEqual(this.partnerId, other.partnerId) &&
				areEqual(this.membershipRefId, other.membershipRefId) &&
				areEqual(this.membershipId, other.membershipId) &&
				areEqual(this.storeRefId, other.storeRefId) &&
				areEqual(this.storeId, other.storeId) &&
				areEqual(this.businessId, other.businessId) &&
				areEqual(this.transactionStatus, other.transactionStatus) &&
				areEqual(this.transactionDate, other.transactionDate) &&
				areEqual(this.timezone, other.timezone) &&
				areEqual(this.currency, other.currency) &&
				areEqual(this.amount, other.amount) &&
				areEqual(this.localCurrency, other.localCurrency) &&
				areEqual(this.localAmount, other.localAmount) &&
				areEqual(this.processingStatus, other.processingStatus) &&
				areEqual(this.processingStatusReason, other.processingStatusReason) &&
				areEqual(this.createdDate, other.createdDate) &&
				areEqual(this.lastModifiedDate, other.lastModifiedDate);
	}

	@Override
	public String toString() {
		return String.format("%s %s (%s)", this.currency, this.amount, this.id);
	}

}
