package com.ngl.micro.transaction.client;

import java.util.UUID;

import com.ngl.micro.transaction.Transaction;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;

/**
 * The client of the Transaction service.
 *
 * @author Willy du Preez
 */
public class TransactionClient extends AbstractRestClient {

	public class TransactionResource extends ResourceTemplate<Transaction, UUID> {
		private TransactionResource() {
			super(Transaction.class, "transactions/");
		}
	}

	private TransactionResource transactionResource;

	public TransactionClient(RestClientConfig config) {
		super(config);

		this.transactionResource = new TransactionResource();
	}

	public TransactionResource getTransactionResource() {
		return transactionResource;
	}

}
