package com.ngl.micro.shared.contracts.oauth;

import java.time.ZonedDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.middleware.messaging.api.event.AbstractEvent;
import com.ngl.middleware.util.Assert;

/**
 * Event for revoked access tokens.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class AccessTokenRevokedEvent extends AbstractEvent<Long> {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "access-token-revoked-event";
	private static final String VERSION = "1.0";

	private final UUID tokenId;
	private final ZonedDateTime expirationDate;

	@JsonCreator
	public AccessTokenRevokedEvent(
			@JsonProperty("id") Long id,
			@JsonProperty("created") ZonedDateTime created,
			@JsonProperty("tokenId") UUID tokenId,
			@JsonProperty("expirationDate") ZonedDateTime expirationDate) {
		super(NAME, VERSION, id, created);

		Assert.notNull(tokenId);
		Assert.notNull(expirationDate);

		this.tokenId = tokenId;
		this.expirationDate = expirationDate;
	}

	public UUID getTokenId() {
		return this.tokenId;
	}

	public ZonedDateTime getExpirationDate() {
		return this.expirationDate;
	}

}
