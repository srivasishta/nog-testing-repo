package com.ngl.micro.shared.contracts.common;

import java.math.RoundingMode;
import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Supported Currencies.
 *
 * @author Willy du Preez
 *
 */
public enum Currency {

	USD(1L),
	CAD(2L);

	public static final int SCALE = 5;
	public static final RoundingMode ROUNDING = RoundingMode.HALF_UP;

	public static Currency forId(Long id) {
		Assert.notNull(id);

		for (Currency currency : Currency.values()) {
			if (currency.id == id) {
				return currency;
			}
		}
		throw new NoSuchElementException("No Currency with ID: " + id);
	}

	private long id;

	private Currency(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
