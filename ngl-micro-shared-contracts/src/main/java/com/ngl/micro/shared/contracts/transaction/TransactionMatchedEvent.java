package com.ngl.micro.shared.contracts.transaction;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;
import com.ngl.micro.shared.contracts.charity.TransactionStatus;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.middleware.messaging.api.event.AbstractEvent;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link TransactionMatchedEvent}.
 *
 * @author Willy du Preez
 *
 */
@JsonDeserialize(builder = TransactionMatchedEvent.Builder.class)
public class TransactionMatchedEvent extends AbstractEvent<Long> {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "transaction-matched-event";
	private static final String VERSION = "1.0";

	public static Builder builder(Long id, ZonedDateTime created) {
		return new Builder(id, created);
	}

	public static Builder builder(TransactionMatchedEvent event) {
		return new Builder(event);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class Builder {

		private TransactionMatchedEvent event;

		@JsonCreator
		public Builder(
				@JsonProperty("id") Long id,
				@JsonProperty("created") ZonedDateTime created) {
			this(new TransactionMatchedEvent(id, created));
		}

		private Builder(TransactionMatchedEvent event) {
			Assert.notNull(event);
			this.event = event;
		}

		public TransactionMatchedEvent build() {
			this.validate();

			TransactionMatchedEvent build = new TransactionMatchedEvent(this.event.getId(), this.event.getCreated());

			build.businessStoreContributionRate = this.event.businessStoreContributionRate;
			build.businessId = this.event.businessId;
			build.businessStoreId = this.event.businessStoreId;

			build.membershipCharityChoices = Collections.unmodifiableSet(new HashSet<>(this.event.membershipCharityChoices));
			build.membershipDemographics = this.event.membershipDemographics;
			build.membershipId = this.event.membershipId;

			build.partnerId = this.event.partnerId;

			build.transactionId = this.event.transactionId;
			build.transactionGroupId = this.event.transactionGroupId;
			build.transactionAmount = this.event.transactionAmount;
			build.transactionCurrency = this.event.transactionCurrency;
			build.transactionDate = this.event.transactionDate;
			build.transactionStatus = this.event.transactionStatus;
			build.transactionTimezone = this.event.transactionTimezone;

			return build;
		}

		private void validate() {
			Assert.notNull(this.event.businessStoreContributionRate);
			Assert.notNull(this.event.businessId);
			Assert.notNull(this.event.businessStoreId);

			Assert.notNull(this.event.membershipCharityChoices);
			Assert.notNull(this.event.membershipDemographics);
			Assert.notNull(this.event.membershipId);

			Assert.notNull(this.event.partnerId);

			Assert.notNull(this.event.transactionId);
			Assert.notNull(this.event.transactionGroupId);
			Assert.notNull(this.event.transactionAmount);
			Assert.notNull(this.event.transactionCurrency);
			Assert.notNull(this.event.transactionDate);
			Assert.notNull(this.event.transactionStatus);
			Assert.notNull(this.event.transactionTimezone);
		}

		public Builder partnerId(UUID partnerId) {
			Assert.notNull(partnerId);
			this.event.partnerId = partnerId;
			return this;
		}

		public Builder transactionId(UUID transactionId) {
			Assert.notNull(transactionId);
			this.event.transactionId = transactionId;
			return this;
		}

		public Builder transactionGroupId(UUID transactionGroupId) {
			Assert.notNull(transactionGroupId);
			this.event.transactionGroupId = transactionGroupId;
			return this;
		}

		public Builder transactionDate(ZonedDateTime transactionDate) {
			Assert.notNull(transactionDate);
			this.event.transactionDate = transactionDate;
			return this;
		}

		public Builder transactionStatus(TransactionStatus transactionStatus) {
			Assert.notNull(transactionStatus);
			this.event.transactionStatus = transactionStatus;
			return this;
		}

		public Builder transactionAmount(BigDecimal transactionAmount) {
			Assert.notNull(transactionAmount);
			this.event.transactionAmount = transactionAmount.setScale(Currency.SCALE, Currency.ROUNDING);
			return this;
		}

		public Builder transactionTimezone(TimeZone transactionTimezone) {
			Assert.notNull(transactionTimezone);
			this.event.transactionTimezone = transactionTimezone;
			return this;
		}

		public Builder transactionCurrency(Currency transactionCurrency) {
			Assert.notNull(transactionCurrency);
			this.event.transactionCurrency = transactionCurrency;
			return this;
		}

		public Builder businessId(UUID businessId) {
			Assert.notNull(businessId);
			this.event.businessId = businessId;
			return this;
		}

		public Builder businessStoreId(UUID businessStoreId) {
			Assert.notNull(businessStoreId);
			this.event.businessStoreId = businessStoreId;
			return this;
		}

		public Builder businessStoreContributionRate(BigDecimal businessStoreContributionRate) {
			Assert.notNull(businessStoreContributionRate);
			this.event.businessStoreContributionRate = businessStoreContributionRate;
			return this;
		}

		public Builder membershipId(UUID membershipId) {
			Assert.notNull(membershipId);
			this.event.membershipId = membershipId;
			return this;
		}

		public Builder membershipCharityChoices(Set<MembershipCharityChoice> membershipCharityChoices) {
			Assert.notNull(membershipCharityChoices);
			this.event.membershipCharityChoices = membershipCharityChoices;
			return this;
		}

		public Builder membershipDemographics(MembershipDemographics membershipDemographics) {
			Assert.notNull(membershipDemographics);
			this.event.membershipDemographics = membershipDemographics;
			return this;
		}

	}

	private UUID partnerId;

	private UUID transactionId;
	private UUID transactionGroupId;
	private ZonedDateTime transactionDate;
	private TransactionStatus transactionStatus;
	private BigDecimal transactionAmount;
	private TimeZone transactionTimezone;
	private Currency transactionCurrency;

	private UUID businessId;
	private UUID businessStoreId;
	private BigDecimal businessStoreContributionRate;

	private UUID membershipId;
	private Set<MembershipCharityChoice> membershipCharityChoices = Collections.emptySet();
	private MembershipDemographics membershipDemographics;

	private TransactionMatchedEvent(Long id, ZonedDateTime created) {
		super(NAME, VERSION, id, created);
	}

	public UUID getTransactionId() {
		return this.transactionId;
	}

	public UUID getTransactionGroupId() {
		return this.transactionGroupId;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public ZonedDateTime getTransactionDate() {
		return this.transactionDate;
	}

	public TransactionStatus getTransactionStatus() {
		return this.transactionStatus;
	}

	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}

	public TimeZone getTransactionTimezone() {
		return this.transactionTimezone;
	}

	public Currency getTransactionCurrency() {
		return this.transactionCurrency;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public UUID getBusinessStoreId() {
		return this.businessStoreId;
	}

	public BigDecimal getBusinessStoreContributionRate() {
		return this.businessStoreContributionRate;
	}

	public UUID getMembershipId() {
		return this.membershipId;
	}

	public Set<MembershipCharityChoice> getMembershipCharityChoices() {
		return this.membershipCharityChoices;
	}

	public MembershipDemographics getMembershipDemographics() {
		return this.membershipDemographics;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.getId());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		TransactionMatchedEvent other = (TransactionMatchedEvent) obj;

		return EqualsUtils.areEqual(this.getId(), other.getId())
				&& EqualsUtils.areEqual(this.getCreated(), other.getCreated())
				&& EqualsUtils.areEqual(this.partnerId, other.partnerId)
				&& EqualsUtils.areEqual(this.transactionId, other.transactionId)
				&& EqualsUtils.areEqual(this.transactionGroupId, other.transactionGroupId)
				&& EqualsUtils.areEqual(this.transactionDate, other.transactionDate)
				&& EqualsUtils.areEqual(this.transactionStatus, other.transactionStatus)
				&& EqualsUtils.areEqual(this.transactionCurrency, other.transactionCurrency)
				&& EqualsUtils.areEqual(this.transactionAmount, other.transactionAmount)
				&& EqualsUtils.areEqual(this.transactionTimezone, other.transactionTimezone)
				&& EqualsUtils.areEqual(this.businessId, other.businessId)
				&& EqualsUtils.areEqual(this.businessStoreId, other.businessStoreId)
				&& EqualsUtils.areEqual(this.businessStoreContributionRate, other.businessStoreContributionRate)
				&& EqualsUtils.areEqual(this.membershipId, other.membershipId)
				&& EqualsUtils.areEqual(this.membershipCharityChoices, other.membershipCharityChoices)
				&& EqualsUtils.areEqual(this.membershipDemographics, other.membershipDemographics);
	}

	@Override
	public String toString() {
		return "id=" + this.getId() + "; transactionId=" + this.transactionId;
	}

}
