package com.ngl.micro.shared.contracts.member;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import com.ngl.micro.shared.contracts.ResourceStatus;
import com.ngl.micro.shared.contracts.charity.MembershipCharityChoice;
import com.ngl.micro.shared.contracts.charity.MembershipDemographics;

/**
 * Membership lookup found.
 *
 * @author Willy du Preez
 *
 */
public class MembershipLookupResponse {

	private boolean matched;

	private UUID membershipId;
	private ResourceStatus status;

	private Set<MembershipCharityChoice> charityChoice = Collections.emptySet();
	private MembershipDemographics demographics;

	public boolean isMatched() {
		return this.matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public UUID getMembershipId() {
		return this.membershipId;
	}

	public void setMembershipId(UUID membershipId) {
		this.membershipId = membershipId;
	}

	public ResourceStatus getStatus() {
		return this.status;
	}

	public void setStatus(ResourceStatus status) {
		this.status = status;
	}

	public Set<MembershipCharityChoice> getCharityChoice() {
		return this.charityChoice;
	}

	public void setCharityChoice(Set<MembershipCharityChoice> charityChoice) {
		this.charityChoice = charityChoice;
	}

	public MembershipDemographics getDemographics() {
		return this.demographics;
	}

	public void setDemographics(MembershipDemographics demographics) {
		this.demographics = demographics;
	}

}
