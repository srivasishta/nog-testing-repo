package com.ngl.micro.shared.contracts.charity;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link MembershipCharityChoice}.
 *
 * @author Willy du Preez
 */
public class MembershipCharityChoice {

	private UUID charityId;
	private Integer allocation;

	@JsonCreator
	public MembershipCharityChoice(
			@JsonProperty("charityId") UUID charityId,
			@JsonProperty("allocation") Integer allocation) {
		Assert.notNull(charityId);
		Assert.notNull(allocation);

		this.charityId = charityId;
		this.allocation = allocation;
	}

	public UUID getCharityId() {
		return this.charityId;
	}

	public Integer getAllocation() {
		return this.allocation;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.charityId);
		result += EqualsUtils.hashValue(this.allocation);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		MembershipCharityChoice other = (MembershipCharityChoice) obj;

		return EqualsUtils.areEqual(this.charityId, other.charityId)
				&& EqualsUtils.areEqual(this.allocation, other.allocation);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.charityId, this.allocation);
	}

}
