package com.ngl.micro.shared.contracts;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

public enum ResourceStatus {

	ACTIVE(1L),
	INACTIVE(2L);

	public static ResourceStatus forId(Long id) {
		Assert.notNull(id);

		for (ResourceStatus status : ResourceStatus.values()) {
			if (status.id == id) {
				return status;
			}
		}
		throw new NoSuchElementException("No ResourceStatus with ID: " + id);
	}

	public static ResourceStatus forEnum(Enum<?> named) {
		Assert.notNull(named);

		String name = named.name();
		for (ResourceStatus status : ResourceStatus.values()) {
			if (status.name().equals(name)) {
				return status;
			}
		}
		throw new NoSuchElementException("No ResourceStatus for Enum with name: " + name);
	}

	public static String nameForId(Long id) {
		return ResourceStatus.forId(id).name();
	}

	public static Long idFrom(Enum<?> named) {
		Assert.notNull(named);
		return ResourceStatus.valueOf(named.name()).getId();
	}

	private long id;

	private ResourceStatus(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
