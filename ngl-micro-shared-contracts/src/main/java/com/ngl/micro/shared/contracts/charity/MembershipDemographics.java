package com.ngl.micro.shared.contracts.charity;

import java.time.LocalDate;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.micro.shared.contracts.common.Gender;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link MembershipDemographics}.
 *
 * @author Willy du Preez
 *
 */
public class MembershipDemographics {

	@JsonProperty
	private Gender gender;
	@JsonProperty
	private LocalDate birthdate;

	@JsonCreator
	public MembershipDemographics(@JsonProperty("gender") Gender gender, @JsonProperty("birthdate") LocalDate birthdate) {
		Assert.notNull(gender);

		this.gender = gender;
		this.birthdate = birthdate;
	}

	@JsonIgnore
	public Gender getGender() {
		return this.gender;
	}

	@JsonIgnore
	public Optional<LocalDate> getBirthdate() {
		return Optional.ofNullable(this.birthdate);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result += EqualsUtils.hashValue(this.gender);
		result += EqualsUtils.hashValue(this.birthdate);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		MembershipDemographics other = (MembershipDemographics) obj;

		return EqualsUtils.areEqual(this.gender, other.gender)
				&& EqualsUtils.areEqual(this.birthdate, other.birthdate);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.gender, this.birthdate);
	}

}
