package com.ngl.micro.shared.contracts.business;

import java.time.ZonedDateTime;

/**
 * Request to look up a store.
 *
 * @author Willy du Preez
 *
 */
public class StoreLookupRequest {

	private String storeRefId;
	private ZonedDateTime transactionDate;

	public StoreLookupRequest() {
	}

	public StoreLookupRequest(String storeRefId, ZonedDateTime transactionDate) {
		this.storeRefId = storeRefId;
		this.transactionDate = transactionDate;
	}

	public String getStoreRefId() {
		return this.storeRefId;
	}

	public void setStoreRefId(String storeRefId) {
		this.storeRefId = storeRefId;
	}

	public ZonedDateTime getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(ZonedDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

}
