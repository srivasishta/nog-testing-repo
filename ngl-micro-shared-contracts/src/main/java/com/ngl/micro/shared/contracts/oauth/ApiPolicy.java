package com.ngl.micro.shared.contracts.oauth;

import java.util.UUID;

public final class ApiPolicy {

	public static final UUID SYSTEM_ACCOUNT_ID = UUID.fromString("00000000-0000-0000-0000-000000000001");
	public static final UUID SYSTEM_CLIENT_ID = UUID.fromString("00000000-0000-0000-0000-000000000001");

	public static final boolean isSystemAccount(UUID partnerClaim) {
		return partnerClaim.equals(ApiPolicy.SYSTEM_ACCOUNT_ID);
	}

	public static final boolean isAccessibleByPartner(UUID partnerId, UUID resourcePartnerId) {
		return ((partnerId.equals(resourcePartnerId)) || (partnerId.equals(ApiPolicy.SYSTEM_ACCOUNT_ID)));
	}

	private ApiPolicy() {
	}

}
