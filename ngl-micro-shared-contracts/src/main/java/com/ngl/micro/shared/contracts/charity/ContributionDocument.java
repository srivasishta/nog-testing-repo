package com.ngl.micro.shared.contracts.charity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.micro.shared.contracts.common.TimeZone;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link ContributionDocument}.
 *
 * @author Willy du Preez
 *
 */
@JsonDeserialize(builder = ContributionDocument.Builder.class)
public class ContributionDocument implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Builder builder() {
		return new Builder();
	}

	public static Builder builder(ContributionDocument document) {
		return new Builder(document);
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class Builder {

		private ContributionDocument document;

		public Builder() {
			this(new ContributionDocument());
		}

		private Builder(ContributionDocument document) {
			Assert.notNull(document);
			this.document = document;
		}

		public ContributionDocument build() {
			this.validate();

			ContributionDocument build = new ContributionDocument();

			build.id = this.document.id;
			build.createdDate = this.document.createdDate;
			build.lastModifiedDate = this.document.lastModifiedDate;

			build.businessStoreContributionRate = this.document.businessStoreContributionRate;
			build.businessId = this.document.businessId;
			build.businessStoreId = this.document.businessStoreId;

			build.contributionAllocations = Collections.unmodifiableSet(new HashSet<>(this.document.contributionAllocations));
			build.contributionAmount = this.document.contributionAmount;
			build.contributionDate = this.document.contributionDate;

			build.membershipCharityChoices = Collections.unmodifiableSet(new HashSet<>(this.document.membershipCharityChoices));
			build.membershipDemographics = this.document.membershipDemographics;
			build.membershipId = this.document.membershipId;

			build.partnerId = this.document.partnerId;

			build.transactionAmount = this.document.transactionAmount;
			build.transactionCurrency = this.document.transactionCurrency;
			build.transactionDate = this.document.transactionDate;
			build.transactionStatus = this.document.transactionStatus;
			build.transactionTimezone = this.document.transactionTimezone;

			return build;
		}

		private void validate() {
			Assert.notNull(this.document.id);
			Assert.notNull(this.document.createdDate);
			Assert.notNull(this.document.lastModifiedDate);

			Assert.notNull(this.document.businessStoreContributionRate);
			Assert.notNull(this.document.businessId);
			Assert.notNull(this.document.businessStoreId);

			Assert.notNull(this.document.contributionAllocations);
			Assert.notNull(this.document.contributionAmount);
			Assert.notNull(this.document.contributionDate);

			Assert.notNull(this.document.membershipCharityChoices);
			Assert.notNull(this.document.membershipDemographics);
			Assert.notNull(this.document.membershipId);

			Assert.notNull(this.document.partnerId);

			Assert.notNull(this.document.transactionAmount);
			Assert.notNull(this.document.transactionCurrency);
			Assert.notNull(this.document.transactionDate);
			Assert.notNull(this.document.transactionStatus);
			Assert.notNull(this.document.transactionTimezone);
		}

		public Builder id(UUID id) {
			Assert.notNull(id);
			this.document.id = id;
			return this;
		}

		public Builder createdDate(ZonedDateTime createdDate) {
			Assert.notNull(createdDate);
			this.document.createdDate = createdDate;
			return this;
		}

		public Builder lastModifiedDate(ZonedDateTime lastModifiedDate) {
			Assert.notNull(lastModifiedDate);
			this.document.lastModifiedDate = lastModifiedDate;
			return this;
		}

		public Builder partnerId(UUID partnerId) {
			Assert.notNull(partnerId);
			this.document.partnerId = partnerId;
			return this;
		}

		public Builder transactionDate(ZonedDateTime transactionDate) {
			Assert.notNull(transactionDate);
			this.document.transactionDate = transactionDate;
			return this;
		}

		public Builder transactionStatus(TransactionStatus transactionStatus) {
			Assert.notNull(transactionStatus);
			this.document.transactionStatus = transactionStatus;
			return this;
		}

		public Builder transactionAmount(BigDecimal transactionAmount) {
			Assert.notNull(transactionAmount);
			this.document.transactionAmount = transactionAmount.setScale(Currency.SCALE, Currency.ROUNDING);
			return this;
		}

		public Builder transactionTimezone(TimeZone transactionTimezone) {
			Assert.notNull(transactionTimezone);
			this.document.transactionTimezone = transactionTimezone;
			return this;
		}

		public Builder transactionCurrency(Currency transactionCurrency) {
			Assert.notNull(transactionCurrency);
			this.document.transactionCurrency = transactionCurrency;
			return this;
		}

		public Builder businessId(UUID businessId) {
			Assert.notNull(businessId);
			this.document.businessId = businessId;
			return this;
		}

		public Builder businessStoreId(UUID businessStoreId) {
			Assert.notNull(businessStoreId);
			this.document.businessStoreId = businessStoreId;
			return this;
		}

		public Builder businessStoreContributionRate(BigDecimal businessStoreContributionRate) {
			Assert.notNull(businessStoreContributionRate);
			this.document.businessStoreContributionRate = businessStoreContributionRate;
			return this;
		}

		public Builder membershipId(UUID membershipId) {
			Assert.notNull(membershipId);
			this.document.membershipId = membershipId;
			return this;
		}

		public Builder membershipCharityChoices(Set<MembershipCharityChoice> membershipCharityChoices) {
			Assert.notNull(membershipCharityChoices);
			this.document.membershipCharityChoices = membershipCharityChoices;
			return this;
		}

		public Builder membershipDemographics(MembershipDemographics membershipDemographics) {
			Assert.notNull(membershipDemographics);
			this.document.membershipDemographics = membershipDemographics;
			return this;
		}

		public Builder contributionDate(ZonedDateTime contributionDate) {
			Assert.notNull(contributionDate);
			this.document.contributionDate = contributionDate;
			return this;
		}

		public Builder contributionAmount(BigDecimal contributionAmount) {
			Assert.notNull(contributionAmount);
			this.document.contributionAmount = contributionAmount;
			return this;
		}

		public Builder contributionAllocations(Set<ContributionAllocation> contributionAllocations) {
			Assert.notNull(contributionAllocations);
			this.document.contributionAllocations = contributionAllocations;
			return this;
		}

	}

	private UUID id;
	private ZonedDateTime contributionDate;
	private BigDecimal contributionAmount; // = BigDecimal.ZERO.setScale(Currency.SCALE, Currency.ROUNDING);
	private Set<ContributionAllocation> contributionAllocations; // = Collections.emptySet();

	private ZonedDateTime createdDate;
	private ZonedDateTime lastModifiedDate;

	private UUID partnerId;

	private ZonedDateTime transactionDate;
	private TransactionStatus transactionStatus;
	private BigDecimal transactionAmount;
	private TimeZone transactionTimezone;
	private Currency transactionCurrency;

	private UUID businessId;
	private UUID businessStoreId;
	private BigDecimal businessStoreContributionRate;

	private UUID membershipId;
	private Set<MembershipCharityChoice> membershipCharityChoices = Collections.emptySet();
	private MembershipDemographics membershipDemographics;

	public UUID getId() {
		return this.id;
	}

	public ZonedDateTime getCreatedDate() {
		return this.createdDate;
	}

	public ZonedDateTime getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public ZonedDateTime getTransactionDate() {
		return this.transactionDate;
	}

	public TransactionStatus getTransactionStatus() {
		return this.transactionStatus;
	}

	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}

	public TimeZone getTransactionTimezone() {
		return this.transactionTimezone;
	}

	public Currency getTransactionCurrency() {
		return this.transactionCurrency;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public UUID getBusinessStoreId() {
		return this.businessStoreId;
	}

	public BigDecimal getBusinessStoreContributionRate() {
		return this.businessStoreContributionRate;
	}

	public UUID getMembershipId() {
		return this.membershipId;
	}

	public Set<MembershipCharityChoice> getMembershipCharityChoices() {
		return this.membershipCharityChoices;
	}

	public MembershipDemographics getMembershipDemographics() {
		return this.membershipDemographics;
	}

	public ZonedDateTime getContributionDate() {
		return this.contributionDate;
	}

	public BigDecimal getContributionAmount() {
		return this.contributionAmount;
	}

	public Set<ContributionAllocation> getContributionAllocations() {
		return this.contributionAllocations;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		ContributionDocument other = (ContributionDocument) obj;

		return EqualsUtils.areEqual(this.partnerId, other.partnerId)
				&& EqualsUtils.areEqual(this.id, other.id)
				&& EqualsUtils.areEqual(this.createdDate, other.createdDate)
				&& EqualsUtils.areEqual(this.lastModifiedDate, other.lastModifiedDate)
				&& EqualsUtils.areEqual(this.transactionDate, other.transactionDate)
				&& EqualsUtils.areEqual(this.transactionStatus, other.transactionStatus)
				&& EqualsUtils.areEqual(this.transactionCurrency, other.transactionCurrency)
				&& EqualsUtils.areEqual(this.transactionAmount, other.transactionAmount)
				&& EqualsUtils.areEqual(this.transactionTimezone, other.transactionTimezone)
				&& EqualsUtils.areEqual(this.businessId, other.businessId)
				&& EqualsUtils.areEqual(this.businessStoreId, other.businessStoreId)
				&& EqualsUtils.areEqual(this.businessStoreContributionRate, other.businessStoreContributionRate)
				&& EqualsUtils.areEqual(this.membershipId, other.membershipId)
				&& EqualsUtils.areEqual(this.membershipCharityChoices, other.membershipCharityChoices)
				&& EqualsUtils.areEqual(this.membershipDemographics, other.membershipDemographics)
				&& EqualsUtils.areEqual(this.contributionDate, other.contributionDate)
				&& EqualsUtils.areEqual(this.contributionAmount, other.contributionAmount)
				&& EqualsUtils.areEqual(this.contributionAllocations, other.contributionAllocations);
	}

	@Override
	public String toString() {
		return "id=" + this.id + ";contributionAllocations=" + this.contributionAllocations;
	}

}
