package com.ngl.micro.shared.contracts.member;

import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * Request to look up a membership.
 *
 * @author Willy du Preez
 *
 */
public class MembershipLookupRequest {

	private UUID partnerId;
	private String membershipRefId;
	private ZonedDateTime transactionDate;

	public MembershipLookupRequest() {
	}

	public MembershipLookupRequest(UUID partnerId, String membershipRefId, ZonedDateTime transactionDate) {
		this.partnerId = partnerId;
		this.membershipRefId = membershipRefId;
		this.transactionDate = transactionDate;
	}

	public UUID getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(UUID partnerId) {
		this.partnerId = partnerId;
	}

	public String getMembershipRefId() {
		return this.membershipRefId;
	}

	public void setMembershipRefId(String membershipRefId) {
		this.membershipRefId = membershipRefId;
	}

	public ZonedDateTime getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(ZonedDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

}
