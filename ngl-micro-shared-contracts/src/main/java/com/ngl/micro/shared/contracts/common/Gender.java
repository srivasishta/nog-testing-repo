package com.ngl.micro.shared.contracts.common;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Genders.
 *
 * @author Willy du Preez
 *
 */
public enum Gender {

	MALE(1L),
	FEMALE(2L),
	UNSPECIFIED(3L);

	public static Gender forId(Long id) {
		Assert.notNull(id);

		for (Gender gender : Gender.values()) {
			if (gender.id == id) {
				return gender;
			}
		}
		throw new NoSuchElementException("No Gender with ID: " + id);
	}

	public static Gender forEnum(Enum<?> named) {
		String name = named.name();
		for (Gender gender : Gender.values()) {
			if (gender.name().equals(name)) {
				return gender;
			}
		}
		return UNSPECIFIED;
	}

	private long id;

	private Gender(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
