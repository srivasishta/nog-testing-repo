package com.ngl.micro.shared.contracts.business;

import java.math.BigDecimal;
import java.util.UUID;

import com.ngl.micro.shared.contracts.ResourceStatus;

/**
 * Store lookup found.
 *
 * @author Willy du Preez
 *
 */
public class StoreLookupResponse {

	private boolean matched;

	private UUID businessId;
	private ResourceStatus businessStatus;

	private UUID storeId;
	private ResourceStatus storeStatus;

	private BigDecimal contributionRate;

	public boolean isMatched() {
		return this.matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public UUID getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(UUID businessId) {
		this.businessId = businessId;
	}

	public ResourceStatus getBusinessStatus() {
		return this.businessStatus;
	}

	public void setBusinessStatus(ResourceStatus businessStatus) {
		this.businessStatus = businessStatus;
	}

	public UUID getStoreId() {
		return this.storeId;
	}

	public void setStoreId(UUID storeId) {
		this.storeId = storeId;
	}

	public ResourceStatus getStoreStatus() {
		return this.storeStatus;
	}

	public void setStoreStatus(ResourceStatus storeStatus) {
		this.storeStatus = storeStatus;
	}

	public BigDecimal getContributionRate() {
		return this.contributionRate;
	}

	public void setContributionRate(BigDecimal contributionRate) {
		this.contributionRate = contributionRate;
	}

}
