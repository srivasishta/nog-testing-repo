package com.ngl.micro.shared.contracts.charity;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.middleware.messaging.api.event.AbstractEvent;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link ContributionRecordedEvent}.
 *
 * @author Willy du Preez
 *
 */
public class ContributionRecordedEvent extends AbstractEvent<Long> {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "contribution-recorded-event";
	private static final String VERSION = "1.0";

	private ContributionDocument document;

	@JsonCreator
	public ContributionRecordedEvent(
			@JsonProperty("id") Long id,
			@JsonProperty("created") ZonedDateTime created,
			@JsonProperty("document") ContributionDocument document) {

		super(NAME, VERSION, id, created);

		Assert.notNull(document);
		this.document = document;
	}

	public ContributionDocument getDocument() {
		return this.document;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.getId());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		ContributionRecordedEvent other = (ContributionRecordedEvent) obj;

		return EqualsUtils.areEqual(this.getId(), other.getId())
				&& EqualsUtils.areEqual(this.getCreated(), other.getCreated())
				&& EqualsUtils.areEqual(this.document, other.getDocument());
	}

	@Override
	public String toString() {
		return "id=" + this.getId() + ";document=[" + this.document + "]";
	}

}
