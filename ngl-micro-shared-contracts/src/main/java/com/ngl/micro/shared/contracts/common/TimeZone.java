package com.ngl.micro.shared.contracts.common;

import java.time.ZoneId;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import com.ngl.middleware.util.Assert;

/**
 * Supported time zones.
 *
 * @author Willy du Preez
 *
 */
public enum TimeZone {

	UTC(1L, "UTC"),
	AMERICA__EDMONTON(2L, "America/Edmonton"),
	AMERICA__NEW_YORK(3L, "America/New_York"),
	AMERICA__DETROIT(4L, "America/Detroit"),
	AMERICA__KENTUCKY_LOUISVILLE(5L, "America/Kentucky/Louisville"),
	AMERICA__KENTUCKY_MONTICELLO(6L, "America/Kentucky/Monticello"),
	AMERICA__INDIANA_INDIANAPOLIS(7L, "America/Indiana/Indianapolis"),
	AMERICA__INDIANA_VINCENNES(8L, "America/Indiana/Vincennes"),
	AMERICA__INDIANA_WINAMAC(9L, "America/Indiana/Winamac"),
	AMERICA__INDIANA_MARENGO(10L, "America/Indiana/Marengo"),
	AMERICA__INDIANA_PETERSBURG(11L, "America/Indiana/Petersburg"),
	AMERICA__INDIANA_VEVAY(12L, "America/Indiana/Vevay"),
	AMERICA__CHICAGO(13L, "America/Chicago"),
	AMERICA__INDIANA_TELL_CITY(14L, "America/Indiana/Tell_City"),
	AMERICA__INDIANA_KNOX(15L, "America/Indiana/Knox"),
	AMERICA__MENOMINEE(16L, "America/Menominee"),
	AMERICA__NORTH_DAKOTA_CENTER(17L, "America/North_Dakota/Center"),
	AMERICA__NORTH_DAKOTA_NEW_SALEM(18L, "America/North_Dakota/New_Salem"),
	AMERICA__NORTH_DAKOTA_BEULAH(19L, "America/North_Dakota/Beulah"),
	AMERICA__DENVER(20L, "America/Denver"),
	AMERICA__BOISE(21L, "America/Boise"),
	AMERICA__PHOENIX(22L, "America/Phoenix"),
	AMERICA__LOS_ANGELES(23L, "America/Los_Angeles"),
	AMERICA__METLAKATLA(24L, "America/Metlakatla"),
	AMERICA__ANCHORAGE(25L, "America/Anchorage"),
	AMERICA__JUNEAU(26L, "America/Juneau"),
	AMERICA__SITKA(27L, "America/Sitka"),
	AMERICA__YAKUTAT(28L, "America/Yakutat"),
	AMERICA__NOME(29L, "America/Nome"),
	AMERICA__ADAK(30L, "America/Adak"),
	PACIFIC__HONOLULU(31L, "Pacific/Honolulu"),
	US__ALASKA(32L, "US/Alaska"),
	US__ALEUTIAN(33L, "US/Aleutian"),
	US__ARIZONA(34L, "US/Arizona"),
	US__CENTRAL(35L, "US/Central"),
	US__EAST_INDIANA(36L, "US/East-Indiana"),
	US__EASTERN(37L, "US/Eastern"),
	US__HAWAII(38L, "US/Hawaii"),
	US__INDIANA_STARKE(39L, "US/Indiana-Starke"),
	US__MICHIGAN(40L, "US/Michigan"),
	US__MOUNTAIN(41L, "US/Mountain"),
	US__PACIFIC(42L, "US/Pacific"),
	US__PACIFIC_NEW(43L, "US/Pacific-New"),
	US__SAMOA(44L, "US/Samoa");

	public static String[] ALLOWED_ZONES = getAllowedZoneIds();

	public static TimeZone forId(Long id) {
		Assert.notNull(id);

		for (TimeZone tz : TimeZone.values()) {
			if (tz.id == id) {
				return tz;
			}
		}
		throw new NoSuchElementException("No TimeZone with ID: " + id);
	}

	public static TimeZone forZoneId(ZoneId zoneId) {
		Assert.notNull(zoneId);

		for (TimeZone tz : TimeZone.values()) {
			if (tz.zoneId.equals(zoneId)) {
				return tz;
			}
		}
		throw new NoSuchElementException("No TimeZone with Zone ID: " + zoneId.toString());
	}

	private static String[] getAllowedZoneIds() {
		return Stream.of(TimeZone.values())
				.map(z -> z.getZoneId().getId())
				.toArray(s -> new String[s]);
	}

	private ZoneId zoneId;
	private long id;

	private TimeZone(long id, String zoneId) {
		this.zoneId = ZoneId.of(zoneId);
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public ZoneId getZoneId() {
		return this.zoneId;
	}

}
