package com.ngl.micro.shared.contracts.business;

/**
 * Looks up a store.
 *
 * @author Willy du Preez
 *
 */
public interface StoreLookupService {

	StoreLookupResponse lookup(StoreLookupRequest request);

}