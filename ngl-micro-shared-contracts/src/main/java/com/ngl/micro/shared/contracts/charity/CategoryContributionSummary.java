package com.ngl.micro.shared.contracts.charity;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link CategoryContributionSummary}.
 *
 * @author Cameron Waldron
 */
public class CategoryContributionSummary {
	private Long categoryId;
	private BigDecimal amount;

	@JsonCreator
	public CategoryContributionSummary(
			@JsonProperty("categoryId") Long categoryId,
			@JsonProperty("amount") BigDecimal amount) {
		Assert.notNull(categoryId);
		Assert.notNull(amount);

		this.categoryId = categoryId;
		this.amount = amount;
	}

	public Long getCategoryId() {
		return this.categoryId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.categoryId);
		result += EqualsUtils.hashValue(this.amount);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		CategoryContributionSummary other = (CategoryContributionSummary) obj;

		return EqualsUtils.areEqual(this.categoryId, other.categoryId)
				&& EqualsUtils.areEqual(this.amount, other.amount);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.categoryId, this.amount);
	}
}
