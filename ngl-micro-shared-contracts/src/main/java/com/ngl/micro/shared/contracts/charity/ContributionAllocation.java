package com.ngl.micro.shared.contracts.charity;

import java.math.BigDecimal;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngl.micro.shared.contracts.common.Currency;
import com.ngl.middleware.util.Assert;
import com.ngl.middleware.util.EqualsUtils;

/**
 * Immutable {@link ContributionAllocation}.
 *
 * @author Willy du Preez
 *
 */
public class ContributionAllocation {

	private UUID charityId;
	private BigDecimal amount;
	private Integer allocation;

	@JsonCreator
	public ContributionAllocation(
			@JsonProperty("charityId") UUID charityId,
			@JsonProperty("amount") BigDecimal amount,
			@JsonProperty("allocation") Integer allocation) {

		Assert.notNull(charityId);
		Assert.notNull(amount);
		Assert.notNull(allocation);

		this.charityId = charityId;
		this.amount = amount.setScale(Currency.SCALE, Currency.ROUNDING);
		this.allocation = allocation;
	}

	public UUID getCharityId() {
		return this.charityId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public Integer getAllocation() {
		return this.allocation;
	}

	@Override
	public int hashCode() {
		int result = 13;
		result += EqualsUtils.hashValue(this.charityId);
		result += EqualsUtils.hashValue(this.amount);
		result += EqualsUtils.hashValue(this.allocation);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!EqualsUtils.areEqual(this.getClass(), obj.getClass())) {
			return false;
		}

		ContributionAllocation other = (ContributionAllocation) obj;

		return EqualsUtils.areEqual(this.charityId, other.charityId)
				&& EqualsUtils.areEqual(this.amount, other.amount)
				&& EqualsUtils.areEqual(this.allocation, other.allocation);
	}

	@Override
	public String toString() {
		return String.format("charityId=%s;amount=%s;allocation=%s", this.charityId, this.amount, this.allocation);
	}

}
