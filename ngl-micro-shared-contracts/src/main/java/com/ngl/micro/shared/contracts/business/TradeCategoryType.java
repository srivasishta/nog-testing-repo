package com.ngl.micro.shared.contracts.business;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Trade category keys. Double underscores (__) are used to represent sub
 * categories.
 *
 * @Author Cameron Waldron
 *
 */
public enum TradeCategoryType{

	APPAREL__GENERAL(1),
	APPAREL__MENS(2),
	APPAREL__WOMEN(3),
	APPAREL__CHILDREN(4),
	APPAREL__FOOTWEAR(5),
	APPAREL__SPECIALTY(6),
	APPAREL__CLEANING(7),
	AUTOMOTIVE__AUTO__PARTS(8),
	AUTOMOTIVE__AUTO__REPAIR(9),
	AUTOMOTIVE__AUTO__SALES(10),
	AUTOMOTIVE__FUEL(11),
	AUTOMOTIVE__RENTAL(12),
	DINING__QUICK_SERVICE(13),
	DINING__CASUAL_DINING(14),
	DINING__FINE_DINING(15),
	DINING__SPORTS_BARS(16),
	DINING__CATERING(17),
	ENTERTAINMENT__ATTRACTIONS(18),
	ENTERTAINMENT__ENTERTAINMENT(19),
	TRAVEL__HOTELS(20),
	TRAVEL__TRAVEL(21),
	GENERAL_RETAIL__HOME_SUPPLIES(22),
	GENERAL_RETAIL__CONSUMER_ELECTRONICS(23),
	GENERAL_RETAIL__GENERAL_RETAIL(24),
	GENERAL_RETAIL__OFFICE_SUPPLIES(25),
	GENERAL_RETAIL__TOYS(26),
	GENERAL_RETAIL__BOOKS_MUSIC(27),
	GENERAL_RETAIL__FURNITURE(28),
	GENERAL_RETAIL__JEWELRY(29),
	GENERAL_RETAIL__SPECIALTY(30),
	GROCERY__GENERAL(31),
	GROCERY__SPECIALTY(32),
	HOME__HOME_IMPROVEMENT(33),
	HOME__REPAIR_SERVICES(34),
	HOME__LAWN_GARDEN(35),
	HOME__PAINT_SUPPLIES(36),
	HOME__SPECIALTY(37),
	LUXURY__SPAS_MASSAGE(38),
	LUXURY__SALONS_BARBERSHOPS(39),
	LUXURY__PERSONAL_CARE(40),
	LUXURY__OTHER_RETAIL(41),
	HEALTH__MEDICAL_HEALTH(42),
	HEALTH__PHARMACIES(43),
	HEALTH__GYMS(44),
	GENERAL_SERVICES__GENERAL_REPAIR(45),
	GENERAL_SERVICES__INSURANCE_LEGAL_SERVICES(46),
	GENERAL_SERVICES__PHOTOGRAPHY_PORTRAIT(47),
	GENERAL_SERVICES__ACCOUNTING_TAX_SERVICES(48),
	GENERAL_SERVICES__PEST_CONTROL(49),
	GENERAL_SERVICES__OTHER_SERVICES(50),
	PETS__PET_STORES(51),
	PETS__VETERINARY(52),
	PETS__BOARDING(53),
	PETS__TRAINING(54),
	SPORTS_RECREATION__SPORTING_GOODS(55),
	SPORTS_RECREATION__GOLF_COURSES(56),
	SPORTS_RECREATION__WINTER_SPORTS(57),
	SPORTS_RECREATION__POWER_SPORTS(58),
	SPORTS_RECREATION__CAMPING(59);

	private final long id;

	private TradeCategoryType(long id) {
		this.id = id;
	}

	public static TradeCategoryType forId(Long id) {
		Assert.notNull(id);

		for (TradeCategoryType category : TradeCategoryType.values()) {
			if (category.id == id) {
				return category;
			}
		}
		throw new NoSuchElementException("No TradeCategoryType with ID: " + id);
	}

	public static TradeCategoryType forEnum(Enum<?> named) {
		Assert.notNull(named);

		String name = named.name();
		for (TradeCategoryType category : TradeCategoryType.values()) {
			if (category.name().equals(name)) {
				return category;
			}
		}
		throw new NoSuchElementException("No TradeCategoryType for Enum with name: " + name);
	}

	public static String nameForId(Long id) {
		return TradeCategoryType.forId(id).name();
	}

	public static Long idFrom(Enum<?> named) {
		Assert.notNull(named);
		return TradeCategoryType.valueOf(named.name()).getId();
	}

	public long getId() {
		return this.id;
	}

}
