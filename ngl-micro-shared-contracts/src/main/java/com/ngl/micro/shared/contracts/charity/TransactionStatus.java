package com.ngl.micro.shared.contracts.charity;

import java.util.NoSuchElementException;

import com.ngl.middleware.util.Assert;

/**
 * Transaction status.
 *
 * @author Willy du Preez
 */
public enum TransactionStatus {

	AUTHORIZED(1L),
	SETTLED(2L),
	CANCELLED(3L),
	REVERSED(4L);

	public static TransactionStatus forId(Long id) {
		Assert.notNull(id);

		for (TransactionStatus status : TransactionStatus.values()) {
			if (status.id == id) {
				return status;
			}
		}
		throw new NoSuchElementException("No TransactionStatus with ID: " + id);
	}

	private long id;

	private TransactionStatus(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
