package com.ngl.micro.shared.contracts.member;

/**
 * Looks up a membership.
 *
 * @author Willy du Preez
 *
 */
public interface MembershipLookupService {

	MembershipLookupResponse lookup(MembershipLookupRequest request);

}
