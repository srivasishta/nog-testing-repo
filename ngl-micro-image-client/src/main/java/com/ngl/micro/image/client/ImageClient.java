package com.ngl.micro.image.client;

import static com.ngl.middleware.rest.client.http.Methods.POST;

import java.io.InputStream;
import java.util.UUID;

import com.ngl.micro.image.ImageJob;
import com.ngl.middleware.rest.api.patch.Patch;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Headers;
import com.ngl.middleware.rest.client.http.MediaTypes;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.client.http.mime.Multipart;
import com.ngl.middleware.rest.hal.TypedResource;

/**
 * The Member Service Client.
 *
 * @author Willy du Preez
 */
public class ImageClient extends AbstractRestClient {

	public class ImageResource extends ResourceTemplate<ImageJob, UUID> {

		private ImageResource() {
			super(ImageJob.class, "images/");
		}

		public TypedResource<ImageJob> upload(InputStream content, ImageJob job) {
			String header = MediaTypes.multipartFormData();

			String resourceId = job.getResourceId().toString();
			String resourceType = job.getResourceType().toString();

			Multipart body = Multipart.boundary(MediaTypes.getMultipartBoundary(header))
					.withPart("image", MediaTypes.APPLICATION_OCTET_STREAM, "image.png", content)
					.withPart(ImageJob.Fields.RESOURCE_ID, MediaTypes.APPLICATION_FORM_URLENCODED, resourceId)
					.withPart(ImageJob.Fields.RESOURCE_TYPE, MediaTypes.APPLICATION_FORM_URLENCODED, resourceType);

			if (job.getPartnerId() != null) {
				body.withPart(
						ImageJob.Fields.PARTNER_ID,
						MediaTypes.APPLICATION_FORM_URLENCODED,
						job.getPartnerId().toString());
			}

			Request request = ImageClient.this.http.request(this.actionPath())
					.method(POST)
					.header(Headers.CONTENT_TYPE, header)
					.body(body);

			return ImageClient.this.executeRequest(ImageJob.class, request);
		}

		public TypedResource<ImageJob> status(UUID resourceId) {
			String url = this.actionPath() + resourceId.toString() + "/status";
			Request request = ImageClient.this.http.request(url);
			return ImageClient.this.executeRequest(ImageJob.class, request);
		}

		@Override
		public TypedResource<ImageJob> create(ImageJob resource) {
			throw new UnsupportedOperationException();
		}

		@Override
		public TypedResource<ImageJob> patch(UUID resourceId, Patch patch) {
			throw new UnsupportedOperationException();
		}

	}

	private ImageResource imageResource;


	public ImageClient(RestClientConfig config) {
		super(config);

		this.imageResource = new ImageResource();
	}

	public ImageResource getImageResource() {
		return this.imageResource;
	}

}
