package com.ngl.micro.business.client;

import java.util.UUID;

import com.ngl.micro.business.business.Business;
import com.ngl.micro.business.sale.Sale;
import com.ngl.micro.business.sale.SaleSummary;
import com.ngl.micro.business.store.Store;
import com.ngl.middleware.rest.api.page.Page;
import com.ngl.middleware.rest.api.page.PageRequest;
import com.ngl.middleware.rest.api.page.SearchQuery;
import com.ngl.middleware.rest.client.AbstractRestClient;
import com.ngl.middleware.rest.client.RestClientConfig;
import com.ngl.middleware.rest.client.http.Request;
import com.ngl.middleware.rest.hal.TypedResource;

/**
 * The {@link Business} client for the business micros service.
 *
 * @author Paco Mendes
 * @author Willy du Preez
 */
public class BusinessClient extends AbstractRestClient {

	public class BusinessResource extends ResourceTemplate<Business, UUID> {
		private BusinessResource() {
			super(Business.class, "businesses/");
		}

		public TypedResource<Page<Sale>> sales(UUID businessId, PageRequest page) {
			Request request = BusinessClient.this.http.request(this.actionPath() + businessId + "/sales/")
					.queryParams(BusinessClient.this.qpParser.asQueryParameters(page));

			return BusinessClient.this.executePagedRequest(Sale.class, request);
		}

		public TypedResource<SaleSummary> summary(UUID businessId) {
			return this.summary(businessId, new SearchQuery());
		}

		public TypedResource<SaleSummary> summary(UUID businessId, SearchQuery query) {
			Request request = BusinessClient.this.http
					.request(this.actionPath() + businessId + "/sales/summary")
					.queryParams(BusinessClient.this.qpParser.asQueryParameters(query));

			return BusinessClient.this.executeRequest(SaleSummary.class, request);
		}

	}

	public class StoreResource extends VersionControlledResourceTemplate<Store, UUID> {

		private StoreResource() {
			super(Store.class, "stores/");
		}

	}

	private BusinessResource businessResource;
	private StoreResource storeResource;

	public BusinessClient(RestClientConfig config) {
		super(config);

		this.businessResource = new BusinessResource();
		this.storeResource = new StoreResource();
	}

	public BusinessResource getBusinessResource() {
		return this.businessResource;
	}

	public StoreResource getStoreResource() {
		return this.storeResource;
	}

}
